Feature: Test enable auto-renewal membership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   |
      | 321      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     | 54.99   |
    And property to send IDs to Kafka has value false in db:

  Scenario: Enable auto renewal
    When requested to disable auto renewal for member with memberId 123
    And requested to enable auto renewal for member with memberId 123
    Then member has the auto renewal with status ENABLED
