package com.odigeo.membership;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class AuditMemberAccountTest extends BeanTest<AuditMemberAccount> {

    private static final Long MEMBER_ACCOUNT_ID = 8L;
    private static final Long USER_ID = 91L;
    private static final String NAME = "Eren";
    private static final String LAST_NAME = "Jaeger";

    @Override
    protected AuditMemberAccount getBean() {
        return getAuditMemberAccount();
    }

    private AuditMemberAccount getAuditMemberAccount() {
        return AuditMemberAccount.builder().memberAccountId(MEMBER_ACCOUNT_ID)
            .userId(USER_ID).name(NAME).lastName(LAST_NAME).build();
    }

    @Test
    protected void checkEqual() {
        AuditMemberAccount auditMemberAccount1 = getAuditMemberAccount();
        AuditMemberAccount auditMemberAccount2 = getAuditMemberAccount();
        AuditMemberAccount auditMemberAccount3 = AuditMemberAccount.builder().memberAccountId(MEMBER_ACCOUNT_ID)
            .userId(USER_ID).build();

        assertEquals(auditMemberAccount2, auditMemberAccount1);
        assertEquals(auditMemberAccount1, auditMemberAccount1);
        assertNotEquals(auditMemberAccount3, auditMemberAccount1);
        assertNotEquals(auditMemberAccount1, null);
    }
}
