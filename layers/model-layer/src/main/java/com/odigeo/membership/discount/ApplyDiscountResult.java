package com.odigeo.membership.discount;

import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.NotApplicationReason;

public final class ApplyDiscountResult {

    private final boolean applicable;
    private final Membership applicableMembership;
    private final NotApplicationReason reason;

    private ApplyDiscountResult(boolean applicable, Membership membershipApplicable, NotApplicationReason reason) {
        this.applicable = applicable;
        this.applicableMembership = membershipApplicable;
        this.reason = reason;
    }

    public static ApplyDiscountResult applicable(Membership membership) {
        return new ApplyDiscountResult(true, membership, null);
    }

    public static ApplyDiscountResult notApplicable(NotApplicationReason reason) {
        return new ApplyDiscountResult(false, null, reason);
    }

    public boolean isApplicable() {
        return applicable;
    }

    public Membership getApplicableMembership() {
        return applicableMembership;
    }

    public NotApplicationReason getReason() {
        return reason;
    }
}
