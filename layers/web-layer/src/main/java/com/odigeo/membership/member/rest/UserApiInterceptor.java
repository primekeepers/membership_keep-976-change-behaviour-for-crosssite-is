package com.odigeo.membership.member.rest;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.interception.ClientInterceptor;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@ClientInterceptor
public class UserApiInterceptor implements ClientRequestFilter, ClientResponseFilter {

    protected static final Logger LOGGER = Logger.getLogger(UserApiInterceptor.class);

    @Override
    public void filter(ClientRequestContext clientRequestContext) throws IOException {
        LOGGER.info("PASSO PER AQUÍ");
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext, ClientResponseContext clientResponseContext) throws IOException {
        LOGGER.info("PASSO PER AQUÍ");
    }
}
