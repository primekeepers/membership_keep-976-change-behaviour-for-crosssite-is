package com.odigeo.membership.functionals.config;


import bean.test.BeanTest;

public class DatabaseConfigTest extends BeanTest<DatabaseConfig> {

    @Override
    protected DatabaseConfig getBean() {
        DatabaseConfig databaseConfig = new DatabaseConfig();
        databaseConfig.setPassword("1234");
        databaseConfig.setUrl("http://example");
        databaseConfig.setUser("user");
        return databaseConfig;
    }
}