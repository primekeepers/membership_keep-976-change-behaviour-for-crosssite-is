package com.odigeo.membership.member.update;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.enums.MembershipType;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

public class UpdateMembershipObjectMother {
    private static final String MEMBERSHIP_ID = "9911";
    private static final String RECURRING_ID = "REC123";
    private static final String PLUS_TYPE = "PLUS";

    public static MembershipBuilder anActivatedMembershipBuilder() {
        return new MembershipBuilder()
                .setMembershipType(MembershipType.BASIC)
                .setId(Long.parseLong(MEMBERSHIP_ID))
                .setBalance(BigDecimal.TEN)
                .setMembershipRenewal(MembershipRenewal.ENABLED)
                .setStatus(MemberStatus.ACTIVATED)
                .setRemindMeLater(Boolean.FALSE);
    }

    public static UpdateMembership anUpdateMembership() {
        return UpdateMembership.builder()
                .operation(StringUtils.EMPTY)
                .membershipId(MEMBERSHIP_ID).build();
    }

    public static UpdateMembership anInsertRecurringIdUpdateMembership() {
        return UpdateMembership.builder().membershipId(MEMBERSHIP_ID)
                .recurringId(RECURRING_ID)
                .build();
    }

    public static UpdateMembership anUpdateTypeUpdateMembership() {
        return UpdateMembership.builder().membershipId(MEMBERSHIP_ID)
                .membershipType(PLUS_TYPE)
                .renewalPrice(BigDecimal.TEN)
                .build();
    }

    public static UpdateMembership aDeactivateUpdateMembership() {
        return UpdateMembership.builder().membershipId(MEMBERSHIP_ID)
                .operation(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP.name())
                .build();
    }
}
