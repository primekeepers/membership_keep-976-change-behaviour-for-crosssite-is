package com.odigeo.membership.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.MapBinder;
import com.odigeo.membership.member.discount.ApplyDiscountChecker;
import com.odigeo.membership.member.discount.rule.ApplyDiscountRules;
import com.odigeo.membership.member.discount.rule.PrimeShareRules;
import com.odigeo.membership.member.discount.rule.TravellersRules;
import com.odigeo.membership.member.discount.rule.UserRules;
import com.odigeo.membership.member.discount.rule.WebsiteRules;

import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_PRIME_SHARE;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_TRAVELLERS;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_USER;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_WEBSITE;

public class ApplyDiscountModule extends AbstractModule {

    @Override
    protected void configure() {
        MapBinder<String, ApplyDiscountRules> mapbinder = MapBinder.newMapBinder(binder(), String.class, ApplyDiscountRules.class);
        mapbinder.addBinding(RULES_USER).to(UserRules.class);
        mapbinder.addBinding(RULES_WEBSITE).to(WebsiteRules.class);
        mapbinder.addBinding(RULES_TRAVELLERS).to(PrimeShareRules.class);
        mapbinder.addBinding(RULES_PRIME_SHARE).to(TravellersRules.class);
        requestStaticInjection(ApplyDiscountChecker.class);
    }
}
