package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.MemberAccountUpdateException;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.product.MembershipProduct;
import com.odigeo.membership.product.MembershipProductType;
import com.odigeo.membership.util.Money;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Stateless
@Local(MemberService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MemberServiceBean extends AbstractServiceBean implements MemberService {

    @Override
    public Membership getMembershipById(long membershipId) throws MissingElementException, DataAccessException {
        return getMembership(membershipId);
    }

    private Membership getMembership(long membershipId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipById(dataSource, membershipId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + membershipId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + membershipId, e);
        }
    }

    @Override
    public Membership getMembershipByIdWithMemberAccount(long membershipId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipByIdWithMemberAccount(dataSource, membershipId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + membershipId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + membershipId, e);
        }
    }

    @Override
    public List<Membership> getMembershipsByAccountId(long memberAccountId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipByMemberAccountId(dataSource, memberAccountId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Memberships not found by memberAccountId: " + memberAccountId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load memberships with memberAccountId: " + memberAccountId, e);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Boolean activateMembership(long membershipId, long bookingId, BigDecimal balance) throws DataAccessException, MissingElementException, UserApiException {
        Optional<Membership> activatedMembership;
        MemberAccount memberAccount = getMemberAccount(membershipId);
        getUserService().fixPendingMailConfirmationUserAccount(memberAccount.getUserId())
                .ifPresent(updateMemberAccountUserId(memberAccount));

        try {
            activatedMembership = getMembershipCreationService().activate(membershipId, balance);
        } catch (SQLException | MissingElementException e) {
            throw new DataAccessRollbackException("Cannot activate membership for membershipId " + membershipId + " .Error Status Action ACTIVATION", e);
        }

        activatedMembership.ifPresent(m -> {
            sendActivateMessages(bookingId, m);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.ACTIVATIONS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
        });

        return activatedMembership.isPresent();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long createMembership(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException {
        Long membershipId = getMembershipCreationService().create(membershipCreation);
        getMembershipMessageSendingManager().sendMembershipIdToMembershipReporter(membershipId);
        return membershipId;
    }

    @Override
    public MembershipProduct getProductById(String productId) throws MissingElementException, DataAccessException {
        Optional<Membership> membership = getMembershipNoSQLManager().get(productId);
        MembershipProductType type = MembershipProductType.MEMBERSHIP;
        if (membership.isEmpty()) {
            membership = Optional.of(getMembership(Long.parseLong(productId)));
            type = MembershipProductType.MEMBERSHIP_RENEWAL;
        }
        List<AbstractFee> fees = getMembershipFeesService().retrieveFees(membership.get().getFeeContainerId());
        return MembershipProduct.builder()
            .membershipId(String.valueOf(membership.get().getId()))
            .type(type)
            .money(new Money(membership.get().getTotalPrice(), Currency.getInstance(membership.get().getCurrencyCode())))
            .fees(fees).build();
    }

    private Consumer<Long> updateMemberAccountUserId(MemberAccount memberAccount) {
        return userId -> {
            try {
                getMemberAccountUserIdUpdater().updateMemberAccountUserId(dataSource, memberAccount.getId(), userId);
            } catch (DataAccessException e) {
                throw new MemberAccountUpdateException("There was a problem updating memberAccount " + memberAccount.getId() + "with new userId " + userId, e);
            }
        };
    }

    private MemberAccount getMemberAccount(long membershipId) throws DataAccessRollbackException {
        MembershipStore membershipStore = getMembershipStore();
        MemberAccount memberAccount;

        try {
            memberAccount = Optional.ofNullable(membershipStore.fetchMembershipByIdWithMemberAccount(dataSource, membershipId))
                    .map(Membership::getMemberAccount)
                    .orElseThrow(() -> new MissingElementException("Membership id not found in database: " + membershipId));
        } catch (SQLException | MissingElementException | DataNotFoundException e) {
            throw new DataAccessRollbackException("Could not retrieve member account for membershipId " + membershipId, e);
        }
        return memberAccount;
    }

    private void sendActivateMessages(long bookingId, Membership activatedMembership) {
        MembershipMessageSendingManager membershipMessageSendingManager = getMembershipMessageSendingManager();
        membershipMessageSendingManager.sendMembershipIdToMembershipReporter(activatedMembership.getId());
        membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(activatedMembership, SubscriptionStatus.SUBSCRIBED);
        membershipMessageSendingManager.sendWelcomeToPrimeMessageToMembershipTransactionalTopic(activatedMembership, bookingId, false);
    }

}
