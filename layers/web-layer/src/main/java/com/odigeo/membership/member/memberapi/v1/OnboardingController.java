package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import com.odigeo.membership.v1.OnboardingService;

public class OnboardingController extends AbstractController implements OnboardingService {


    @Override
    public Long saveOnboardingEvent(OnboardingEventRequest request) throws MembershipServiceException {
        try {
            return getOnboardingEventService().createOnboarding(getOnboardingMapper().map(request));
        } catch (DataAccessException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

}
