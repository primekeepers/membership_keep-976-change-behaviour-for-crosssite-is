Feature: Testing update membershipType

  Scenario Outline: Call updateMembership with a valid operation, membershipType and renewalPrice
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 978987          | 137765 | HAN       | SOLO      |
    And the next membership stored in db:
      | memberId | website | status    | membershipType | memberAccountId | activationDate | expirationDate | balance | autoRenewal | renewalPrice          |
      | 99001234 | ES      | ACTIVATED | <currentType>  | 978987          | 2018-06-06     | 2019-06-06     | 44.99   | ENABLED     | <currentRenewalPrice> |
    And property to send IDs to Kafka has value false in db:
    When requested to update membership
      | membershipId | operation              | membershipType | renewalPrice           |
      | 99001234     | UPDATE_MEMBERSHIP_TYPE | <expectedType> | <expectedRenewalPrice> |
    When requested the current membership with userId 137765 and website ES
    Then the retrieved membership has type <expectedType> and renewalPrice <expectedRenewalPrice>
    And the result of the membership update operation is true
    Examples:
      | currentType | currentRenewalPrice | expectedRenewalPrice | expectedType |
      | BASIC       | 54.99               | 64.99                | PLUS         |
      | BASIC       | 54.99               | 54.99                | BASIC        |
      | PLUS        | 64.99               | 54.99                | BASIC        |
      | PLUS        | 64.99               | 64.99                | PLUS         |
