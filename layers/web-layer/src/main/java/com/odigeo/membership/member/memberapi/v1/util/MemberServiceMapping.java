package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.BlacklistedPaymentMethod;
import com.odigeo.membership.product.FreeTrialCandidate;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.container.TravellerParametersContainer;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.membership.response.MembershipRecurring;
import com.odigeo.membership.response.MembershipWarning;
import com.odigeo.util.DateParser;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@SuppressWarnings("PMD.NullAssignment")
@Singleton
public class MemberServiceMapping {

    public List<BlacklistedPaymentMethod> mapToBlackListedPaymentMethods(Long membershipId, List<com.odigeo.membership.response.BlackListedPaymentMethod> blackListedPaymentMethods) {
        List<BlacklistedPaymentMethod> mappedList = new ArrayList<>();
        for (com.odigeo.membership.response.BlackListedPaymentMethod blackListedPaymentMethod : blackListedPaymentMethods) {
            BlacklistedPaymentMethod mapped = new BlacklistedPaymentMethod();
            mapped.setId(blackListedPaymentMethod.getId());
            mapped.setTimestamp(blackListedPaymentMethod.getTimestamp());
            mapped.setErrorType(blackListedPaymentMethod.getErrorType());
            mapped.setErrorMessage(blackListedPaymentMethod.getErrorMessage());
            mapped.setMembershipId(membershipId);
            mappedList.add(mapped);
        }
        return mappedList;
    }

    private void fillMembershipFields(MemberAccount memberAccount, Membership result, com.odigeo.membership.Membership membership) {
        result.setName(memberAccount.getName());
        result.setLastNames(memberAccount.getLastNames());
        result.setMemberId(membership.getId());
        result.setMembershipId(membership.getId());
        result.setMemberAccountId(memberAccount.getId());
        result.setWebsite(membership.getWebsite());
        result.setAutoRenewalStatus(membership.getAutoRenewal().toString());
        result.setMembershipStatus(membership.getStatus().toString());
        result.setBalance(membership.getBalance());
        result.setExpirationDate(DateParser.toIsoDate(membership.getExpirationDate()));
        result.setActivationDate(DateParser.toIsoDate(membership.getActivationDate()));
        result.setMembershipType(membership.getMembershipType().toString());
        result.setSourceType(membership.getSourceType() == null ? null : membership.getSourceType().name());
        result.setRecurringId(membership.getRecurringId());
        result.setMembershipRecurring(createMembershipRecurringResponse(membership.getMembershipRecurring()));
        result.setRecurringCollectionId(membership.getRecurringCollectionId());
        result.setMonthsDuration(membership.getMonthsDuration());
        result.setDurationTimeUnit(membership.getDurationTimeUnit().name());
        result.setDuration(membership.getDuration());
        result.setWarnings(new ArrayList<>());
        if (membership.getBookingLimitReached() != null && membership.getBookingLimitReached()) {
            result.getWarnings().add(MembershipWarning.PERKS_USAGE_EXCEEDED);
        }
    }

    private List<MembershipRecurring> createMembershipRecurringResponse(List<com.odigeo.membership.MembershipRecurring> membershipRecurring) {
        return Optional.ofNullable(membershipRecurring)
                .orElse(Collections.emptyList())
                .stream()
                .map(this::createMembershipRecurring)
                .collect(Collectors.toList());
    }

    private MembershipRecurring createMembershipRecurring(com.odigeo.membership.MembershipRecurring recurringInfo) {
        MembershipRecurring recurringInfoResponse = new MembershipRecurring();
        recurringInfoResponse.setRecurringId(recurringInfo.getRecurringId());
        recurringInfoResponse.setStatus(recurringInfo.getStatus());
        return recurringInfoResponse;
    }

    public Membership asMembershipResponse(MemberAccount memberAccount) {
        Membership result = new Membership();
        com.odigeo.membership.Membership membership = memberAccount.getMemberships().get(0);
        fillMembershipFields(memberAccount, result, membership);
        return result;
    }

    MembershipInfo asMembershipInfoResponse(MemberAccount memberAccount) {
        com.odigeo.membership.Membership membership = memberAccount.getMemberships().get(0);
        MembershipInfo membershipInfo = new MembershipInfo(asMembershipResponse(memberAccount));
        membershipInfo.setTotalPrice(membership.getTotalPrice());
        membershipInfo.setRenewalPrice(membership.getRenewalPrice());
        membershipInfo.setRenewalDuration(membership.getRenewalDuration());
        membershipInfo.setCurrencyCode(membership.getCurrencyCode());
        membershipInfo.setTimestamp(DateParser.toIsoDateTime(membership.getTimestamp()));
        return membershipInfo;
    }

    public MemberOnPassengerListParameter asPassengerList(CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) {
        MemberOnPassengerListParameter result = new MemberOnPassengerListParameter();
        List<TravellerParameter> travellerParameterList = new ArrayList<>();
        for (TravellerParametersContainer tmp : checkMemberOnPassengerListRequest.getTravellerContainerList()) {
            TravellerParameter travellerParameter = new TravellerParameter();
            travellerParameter.setName(tmp.getName());
            travellerParameter.setLastNames(tmp.getLastNames());
            travellerParameterList.add(travellerParameter);
        }
        result.setUserId(checkMemberOnPassengerListRequest.getUserId());
        result.setSite(checkMemberOnPassengerListRequest.getSite());
        result.setTravellerList(travellerParameterList);
        return result;
    }

    public Map<String, Membership> asWebsiteMemberMap(List<MemberAccount> memberAccountList) {
        Map<String, Membership> result = new HashMap<>();
        for (MemberAccount memberAccount : memberAccountList) {
            com.odigeo.membership.Membership membership = memberAccount.getMemberships().get(0);
            result.put(membership.getWebsite(), asMembershipResponse(memberAccount));
        }
        return result;
    }

    public FreeTrialCandidate mapFreeTrialCandidate(FreeTrialCandidateRequest freeTrialCandidateRequest) {
        return FreeTrialCandidate.builder()
            .email(freeTrialCandidateRequest.getEmail())
            .website(freeTrialCandidateRequest.getWebsite())
            .name(freeTrialCandidateRequest.getName())
            .lastName(freeTrialCandidateRequest.getLastNames())
            .build();
    }
}
