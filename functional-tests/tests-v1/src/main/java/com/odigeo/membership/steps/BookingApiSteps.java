package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.bookingapi.mock.v14.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v14.response.BuilderException;
import com.odigeo.bookingapi.mock.v14.response.BuyerBuilder;
import com.odigeo.bookingapi.mock.v14.response.CollectionAttemptBuilder;
import com.odigeo.bookingapi.mock.v14.response.CollectionSummaryBuilder;
import com.odigeo.bookingapi.mock.v14.response.CreditCardBuilder;
import com.odigeo.bookingapi.mock.v14.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v14.response.FeeContainerBuilder;
import com.odigeo.bookingapi.mock.v14.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v14.response.MembershipSubscriptionBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.MovementBuilder;
import com.odigeo.bookingapi.mock.v14.response.ProductCategoryBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.WebsiteBuilder;
import com.odigeo.bookingapi.v14.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.Buyer;
import com.odigeo.bookingapi.v14.responses.CollectionSummary;
import com.odigeo.bookingapi.v14.responses.CreditCard;
import com.odigeo.bookingapi.v14.responses.Fee;
import com.odigeo.bookingapi.v14.responses.FeeContainer;
import com.odigeo.bookingapi.v14.responses.MembershipPerks;
import com.odigeo.bookingapi.v14.responses.ProductCategoryBooking;
import com.odigeo.bookingapi.v14.responses.Website;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.functionals.membership.booking.BookingDetailBuilder;
import com.odigeo.membership.functionals.membership.booking.BookingProductsBuilder;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceMock;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.steps.utils.BookingApiHelper;
import com.odigeo.membership.util.DateConverter;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

@ScenarioScoped
public class BookingApiSteps {

    private static final String SUBSCRIPTION = "subscription";
    private static final String DATE_SPLITTER = "/";
    private static final String BOOKING_ID_LABEL = "bookingId";
    private static final String AVOID_FEES_LABEL = "AVOID_FEES";
    private static final String COST_LABEL = "AVOID_FEES";
    private static final String PERKS_LABEL = "PERKS";
    private static final String AVOID_FEES_SUBCODE = "AB35";
    private static final String COST_SUBCODE = "AB32";
    private static final String PERKS_SUBCODE = "AB34";
    private static final String BOOKING_STATUS_LABEL = "bookingStatus";
    private static final String MEMBERSHIP_ID_LABEL = "membershipId";
    private static final String PRODUCT_TYPE_LABEL = "productType";
    private static final String MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE = "MEMBERSHIP_SUBSCRIPTION";
    private static final String LAST_RECURRING_ID_LABEL = "lastRecurringId";
    private static final String PAID_MOVEMENT_STATUS = "PAID";
    private static final String DIRECTY_PAYMENT_ACTION = "DIRECTY_PAYMENT";
    private final BookingApiServiceWorld bookingApiServiceWorld;
    private final com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder bookingDetailBuilder;
    private final BookingSummaryBuilder bookingSummaryBuilder;
    private final Random random;
    private final FeeBuilder feeBuilder;
    private final MembershipPerksBuilder membershipPerksBuilder;
    private final BookingBasicInfoBuilder bookingBasicInfoBuilder;
    private final CreditCardBuilder creditCardBuilder;
    private final Collection<com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder> bookingDetailBuilderCollection;

    @Inject
    public BookingApiSteps(BookingApiServiceWorld bookingApiServiceWorld) {
        this.bookingApiServiceWorld = bookingApiServiceWorld;
        bookingDetailBuilder = new com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder();
        random = new Random();
        feeBuilder = new FeeBuilder();
        bookingSummaryBuilder = new BookingSummaryBuilder();
        membershipPerksBuilder = new MembershipPerksBuilder();
        bookingBasicInfoBuilder = new BookingBasicInfoBuilder();
        creditCardBuilder = new CreditCardBuilder();
        bookingDetailBuilderCollection = new ArrayList<>();
    }

    @Given("^the next booking detail retrieved by the API:$")
    public void givenBookingDetailById(final List<BookingDetailBuilder> bookingDetails) throws BuilderException {
        for (BookingDetailBuilder bookingDetailParam : bookingDetails) {
            final BookingDetail bookingDetail = new BookingDetail();
            bookingDetail.setBookingBasicInfo(fillBookingBasicInfo(bookingDetailParam));
            bookingDetail.setBookingStatus(bookingDetailParam.getBookingStatus());
            if (bookingDetailParam.hasPerks()) {
                MembershipPerks membershipPerks = new MembershipPerks();
                membershipPerks.setMemberId(bookingDetailParam.getMemberId());
                bookingDetail.setMembershipPerks(membershipPerks);
                bookingDetail.getBookingBasicInfo().setMembershipId(bookingDetailParam.getMemberId());
            }
            bookingDetail.setAllFeeContainers(singletonList(createFeeContainer(bookingDetailParam)));
            if (nonNull(bookingDetailParam.getProductType())) {
                bookingDetail.setBookingProducts(buildBookingProducts(bookingDetailParam.getProductType(), bookingDetailParam.getMemberId()));
            }
            if (nonNull(bookingDetailParam.getEmail())) {
                bookingDetail.setBuyer(buildBookingBuyer(bookingDetailParam.getEmail()));
            }
            bookingDetail.setTestBooking(bookingDetailParam.getTestBooking());
            bookingApiServiceWorld.getBookingApiServiceMock().addBookingDetail(bookingDetail);
        }
    }

    private BookingBasicInfo fillBookingBasicInfo(BookingDetailBuilder builder) throws BuilderException {
        BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
        bookingBasicInfo.setId(builder.getBookingId());
        bookingBasicInfo.setCreationDate(DateConverter.toIsoDate(builder.getCreationDate()).orElse(null));
        if (nonNull(builder.getWebsite())) {
            bookingBasicInfo.setWebsite(buildBookingWebsite(builder.getWebsite()));
        }
        return bookingBasicInfo;
    }

    private FeeContainer createFeeContainer(BookingDetailBuilder builder) {
        FeeContainer container = new FeeContainer();
        final List<Fee> fees = new ArrayList<>();
        if (nonNull(builder.getAvoidFeeAmount())) {
            fees.add(singleFee(AVOID_FEES_LABEL, AVOID_FEES_SUBCODE, builder.getAvoidFeeAmount()));
        }
        if (nonNull(builder.getCostFeeAmount())) {
            fees.add(singleFee(COST_LABEL, COST_SUBCODE, builder.getCostFeeAmount()));
        }
        if (nonNull(builder.getPerksFeeAmount())) {
            fees.add(singleFee(PERKS_LABEL, PERKS_SUBCODE, builder.getPerksFeeAmount()));
        }
        container.setFees(fees);
        return container;
    }

    private Fee singleFee(final String feeLabel, final String feeSubCode, final BigDecimal feeAmount) {
        Fee fee = new Fee();
        fee.setFeeLabel(feeLabel);
        fee.setSubCode(feeSubCode);
        fee.setAmount(feeAmount);
        return fee;
    }

    private List<ProductCategoryBooking> buildBookingProducts(String productType, long membershipId) throws BuilderException {
        MembershipSubscriptionBookingBuilder membershipSubscriptionBookingBuilder = new MembershipSubscriptionBookingBuilder()
                .productType(productType)
                .memberId(membershipId);
        return singletonList(membershipSubscriptionBookingBuilder.build(random));
    }

    private Website buildBookingWebsite(String websiteCode) throws BuilderException {
        return new WebsiteBuilder().code(websiteCode).build(random);
    }

    private Buyer buildBookingBuyer(String email) throws BuilderException {
        return new BuyerBuilder().mail(email).build(random);
    }

    @And("^that booking-api returns the following bookings when performs the search by membershipId$")
    public void retrieveBookingsByMembershipId(DataTable dataTable) throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        List<Map<String, String>> dataTableMapList = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> dataTableMap : dataTableMapList) {
            long membershipId = Long.parseLong(dataTableMap.get("membershipId"));
            long bookingId = Long.parseLong(dataTableMap.get(BOOKING_ID_LABEL));
            String isSubscriptionBooking = dataTableMap.get("isSubscriptionBooking");
            BookingSummary bookingSummary = bookingSummaryBuilder.membershipId(membershipId)
                    .setIsBookingSubscriptionPrime(isSubscriptionBooking)
                    .build(new Random());
            bookingSummary.getBookingBasicInfo().setId(bookingId);
            bookingApiServiceWorld.getBookingSearchApiServiceMock().addBookingSummary(bookingSummary);
        }
    }

    @And("^each prime booking has the following details$")
    public void setContainerFeeForBookings(DataTable dataTable) throws BuilderException {
        List<Map<String, String>> dataTableMapList = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> dataTableMap : dataTableMapList) {
            BookingDetail bookingDetail = buildBookingDetail(dataTableMap);
            bookingApiServiceWorld.getBookingApiServiceMock().addBookingDetail(bookingDetail);
        }
    }

    private BookingDetail buildBookingDetail(Map<String, String> stringMap) throws BuilderException {
        Long bookingId = Long.parseLong(stringMap.get(BOOKING_ID_LABEL));
        Long feeContainerId = Long.parseLong(stringMap.get("containerId"));
        BookingBasicInfoBuilder basicInfoBuilder = bookingBasicInfoBuilder.id(bookingId);
        List<ProductCategoryBookingBuilder> listProductBuilder = createListProductBuilder(stringMap);
        FeeContainerBuilder feeContainerBuilder = createFeeContainerBuilder(stringMap);

        BookingDetail bookingDetail = bookingDetailBuilder
                .membershipPerks(membershipPerksBuilder.feeContainerId(feeContainerId))
                .bookingBasicInfoBuilder(basicInfoBuilder)
                .allFeeContainers(singletonList(feeContainerBuilder))
                .bookingProducts(listProductBuilder)
                .build(random);
        return addLastCreditCard(stringMap, bookingDetail);
    }

    private FeeContainerBuilder createFeeContainerBuilder(Map<String, String> stringMap) {
        FeeContainerBuilder feeContainerBuilder = new FeeContainerBuilder();
        BigDecimal feeTotalAmount = new BigDecimal(stringMap.get("feeTotalAmount"));
        Long feeContainerId = Long.parseLong(stringMap.get("containerId"));
        String currencyCode = stringMap.get("currencyCode");
        List<FeeBuilder> feeBuilders = singletonList(feeBuilder.amount(feeTotalAmount).currency(currencyCode).id(feeContainerId));
        feeContainerBuilder.totalAmount(feeTotalAmount).id(feeContainerId).fees(feeBuilders);
        return feeContainerBuilder;
    }


    private BookingDetail addLastCreditCard(Map<String, String> stringMap, BookingDetail bookingDetail) throws BuilderException {
        CollectionSummary collectionSummary = new CollectionSummary();
        collectionSummary.setLastCreditCard(buildCreditCard(stringMap));
        bookingDetail.setCollectionSummary(collectionSummary);
        return bookingDetail;
    }

    private List<ProductCategoryBookingBuilder> createListProductBuilder(Map<String, String> stringMap) {
        List<ProductCategoryBookingBuilder> listProductBuilder = new ArrayList<>();
        String productCategoryBooking = stringMap.get("productCategoryBooking");
        if (SUBSCRIPTION.equals(productCategoryBooking)) {
            ProductCategoryBookingBuilder membershipSubscriptionBooking = new MembershipSubscriptionBookingBuilder();
            listProductBuilder.add(membershipSubscriptionBooking);
        } else {
            ProductCategoryBookingBuilder itineraryBooking = new ItineraryBookingBuilder();
            listProductBuilder.add(itineraryBooking);
        }
        return listProductBuilder;
    }

    private CreditCard buildCreditCard(Map<String, String> stringMap) throws BuilderException {
        String creditCardNo = stringMap.get("ccNumber");
        String creditCardExpDate = stringMap.get("ccExpirationDate");
        String[] splitDate = creditCardExpDate.split(DATE_SPLITTER);
        return creditCardBuilder.creditCardNumber(creditCardNo)
                .expirationMonth(splitDate[0])
                .expirationYear(splitDate[1])
                .expirationDate(splitDate[0] + splitDate[1])
                .build(random);
    }

    @And("^each booking related to member has the following details$")
    public void setBookingDetailsWithProducts(DataTable dataTable) throws BuilderException {
        List<Map<String, String>> dataTableMapList = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> dataTableMap : dataTableMapList) {
            BookingDetail bookingDetail = buildBookingDetailWithProduct(dataTableMap);
            bookingApiServiceWorld.getBookingApiServiceMock().addBookingDetail(bookingDetail);
        }
    }

    private BookingDetail buildBookingDetailWithProduct(Map<String, String> stringMap) throws BuilderException {
        List<ProductCategoryBookingBuilder> listProductBuilder = createListProductBuilder(stringMap);
        Long bookingId = Long.parseLong(stringMap.get(BOOKING_ID_LABEL));
        BookingBasicInfoBuilder basicInfoBuilder = bookingBasicInfoBuilder.id(bookingId);
        return bookingDetailBuilder.bookingBasicInfoBuilder(basicInfoBuilder).bookingProducts(listProductBuilder).build(random);
    }

    @Given("^BookingAPI getBookingById returns the following information for that booking:$")
    public void setBookingDetails(DataTable dataTable) throws BuilderException {
        List<Map<String, String>> dataTableMap = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> dataTableElement : dataTableMap) {
            BookingDetail bookingDetail = buildBookingDetailToKafkaMessage(dataTableElement);
            bookingApiServiceWorld.getBookingApiServiceMock().addBookingDetail(bookingDetail);
        }
    }

    private BookingDetail buildBookingDetailToKafkaMessage(Map<String, String> stringMap) throws BuilderException {
        Long bookingId = Long.parseLong(stringMap.get(BOOKING_ID_LABEL));
        BookingBasicInfoBuilder basicInfoBuilder = bookingBasicInfoBuilder.id(bookingId).website(new WebsiteBuilder().code(stringMap.get("website")));
        com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder bookingDetailBuilder = this.bookingDetailBuilder.bookingBasicInfoBuilder(basicInfoBuilder).buyer(new BuyerBuilder().mail(stringMap.get("email")));
        if (stringMap.containsKey(BOOKING_STATUS_LABEL)) {
            bookingDetailBuilder = bookingDetailBuilder.bookingStatus(stringMap.get(BOOKING_STATUS_LABEL));
        }
        if (stringMap.containsKey(MEMBERSHIP_ID_LABEL)) {
            String productType = MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE;
            if (stringMap.containsKey(PRODUCT_TYPE_LABEL)) {
                productType = stringMap.get(PRODUCT_TYPE_LABEL);
            }
            final MembershipSubscriptionBookingBuilder membershipSubscriptionBookingBuilder = new MembershipSubscriptionBookingBuilder().productType(productType).memberId(Long.parseLong(stringMap.get(MEMBERSHIP_ID_LABEL)));
            List<ProductCategoryBookingBuilder> products = singletonList(membershipSubscriptionBookingBuilder);
            bookingDetailBuilder = bookingDetailBuilder.bookingProducts(products);
        }
        if (stringMap.containsKey(LAST_RECURRING_ID_LABEL)) {
            final MovementBuilder movementBuilder = new MovementBuilder().status(PAID_MOVEMENT_STATUS).action(DIRECTY_PAYMENT_ACTION);
            final ArrayList<MovementBuilder> validMovements = new ArrayList<>();
            validMovements.add(movementBuilder);
            final CollectionAttemptBuilder collectionAttemptBuilder = new CollectionAttemptBuilder().validMovements(validMovements).recurringId(stringMap.get(LAST_RECURRING_ID_LABEL));
            final ArrayList<CollectionAttemptBuilder> attempts = new ArrayList<>();
            attempts.add(collectionAttemptBuilder);
            CollectionSummaryBuilder collectionSummary = new CollectionSummaryBuilder().attempts(attempts);
            bookingDetailBuilder = bookingDetailBuilder.collectionSummary(collectionSummary);
        }
        return bookingDetailBuilder.build(random);
    }

    @And("^booking-api has BookingDetails:$")
    public void setBookingDetailsGetBookingById(List<BookingDetailBuilder> dataTable) {
        for (BookingDetailBuilder dataTableElement : dataTable) {
            bookingDetailBuilderCollection.add(BookingApiHelper.getBookingDetail(dataTableElement));
        }
    }

    @And("^BookingDetails has BookingProducts:$")
    public void setBookingProducts(List<BookingProductsBuilder> bookingProducts) {
        for (BookingProductsBuilder bookingProduct : bookingProducts) {
            Long bookingId = bookingProduct.getBookingId();
            bookingDetailBuilderCollection.stream().filter(BookingApiHelper.hasSameBookingId(bookingId)).forEach(BookingApiHelper.add(bookingProduct));
        }
    }

    @And("^BookingApi returns BookingDetails$")
    public void setGetBookingById() {
        BookingApiServiceMock bookingApiServiceMock = bookingApiServiceWorld.getBookingApiServiceMock();
        bookingDetailBuilderCollection.forEach(BookingApiHelper.add(bookingApiServiceMock));
    }

}
