package com.odigeo.bookingapi;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.odigeo.bookingapi.v14.BadCredentialsException;
import com.odigeo.bookingapi.v14.BookingApiService;
import com.odigeo.bookingapi.v14.InvalidParametersException;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingsearchapi.BookingSearchApiConfiguration;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class BookingApiManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingApiManager.class);
    private static final String RESPONSE_STATUS_MESSAGE = "BookingApiManager::read bookingId {} error message {}";
    private static final String BOOKING_API_ERROR_MSG = "Error in BookingApi module when executing:";
    private static final String GET_BOOKING_SERVICE_TXT = "getBooking() with bookingID:";

    private final BookingApiService bookingApiService;
    private final BookingApiConfiguration bookingApiConfiguration;

    private final BookingSearchApiService bookingSearchApiService;
    private final BookingSearchApiConfiguration bookingSearchApiConfiguration;

    @Inject
    public BookingApiManager(BookingApiService bookingApiService,
                             BookingApiConfiguration bookingApiConfiguration,
                             BookingSearchApiService bookingSearchApiService,
                             BookingSearchApiConfiguration bookingSearchApiConfiguration) {
        this.bookingApiService = bookingApiService;
        this.bookingApiConfiguration = bookingApiConfiguration;
        this.bookingSearchApiService = bookingSearchApiService;
        this.bookingSearchApiConfiguration = bookingSearchApiConfiguration;
    }

    public BookingDetail getBooking(long bookingId) throws BookingApiException {
        try {
            LOGGER.info("BookingApiManager::getBooking with bookingId {}", bookingId);
            BookingDetail bookingDetail = bookingApiService.getBooking(bookingApiConfiguration.getUser(), bookingApiConfiguration.getPassword(), Locale.getDefault(), bookingId);
            return errorHandle(bookingId, bookingDetail);
        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            LOGGER.error(BOOKING_API_ERROR_MSG + GET_BOOKING_SERVICE_TXT + bookingId, e);
            throw new BookingApiException(BOOKING_API_ERROR_MSG + GET_BOOKING_SERVICE_TXT + bookingId, e);
        }
    }

    public SearchBookingsPageResponse<List<BookingSummary>> searchBookings(SearchBookingsRequest searchBookingsRequest) throws DataAccessException {
        LOGGER.info("BookingApiManager::searchBookings with SearchBookingsRequest {}", searchBookingsRequest);
        try {
            return bookingSearchApiService.searchBookings(bookingSearchApiConfiguration.getUser(), bookingSearchApiConfiguration.getPassword(), Locale.getDefault(), searchBookingsRequest);
        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            LOGGER.error("Error in BookingApi module when executing searchBookings()", e);
            throw new DataAccessException(e);
        }
    }

    private BookingDetail errorHandle(long bookingId, BookingDetail bookingDetail) {
        if (Objects.nonNull(bookingDetail.getErrorMessage())) {
            LOGGER.error(RESPONSE_STATUS_MESSAGE, bookingId, bookingDetail.getErrorMessage());
            return null;
        }
        return bookingDetail;
    }
}
