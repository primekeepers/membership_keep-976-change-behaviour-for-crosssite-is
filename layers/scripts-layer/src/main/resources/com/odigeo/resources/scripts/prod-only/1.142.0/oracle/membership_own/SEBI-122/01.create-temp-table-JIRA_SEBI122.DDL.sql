CREATE TABLE JIRA_SEBI122 (
  MEMBERSHIP_ID   VARCHAR2(25 CHAR) NOT NULL,
  ACTIVATION_DATE DATE,
  PRIMARY KEY (MEMBERSHIP_ID)
);
