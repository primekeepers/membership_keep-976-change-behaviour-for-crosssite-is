package com.odigeo.membership.member.creation.sql;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

public class BasicFreeMembershipService extends MembershipSQLServiceProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicFreeMembershipService.class);
    private static final int BASIC_FREE_MONTHS_DURATION = 6;
    private final UserApiManager userApiManager;
    private final SearchService searchService;
    private final MembershipMessageSendingManager membershipMessageSendingManager;

    @Inject
    public BasicFreeMembershipService(UserApiManager userApiManager, SearchService searchService, MembershipMessageSendingManager membershipMessageSendingManager) {
        this.userApiManager = userApiManager;
        this.searchService = searchService;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
    }

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        try {
            LocalDateTime now = LocalDateTime.now();
            membershipCreation.setActivationDate(now);
            membershipCreation.setMonthsDuration(BASIC_FREE_MONTHS_DURATION);
            membershipCreation.setExpirationDate(now.plusMonths(BASIC_FREE_MONTHS_DURATION));
            membershipCreation.setBalance(BigDecimal.ZERO);
            List<Membership> memberships = retrieveActivatedMembershipByUser(membershipCreation, searchService);
            if (CollectionUtils.isEmpty(memberships)) {
                sendSubscriptionMessage(membershipCreation);
                incrementCreationMetricsCounter(MetricsNames.BASIC_FREE_CREATION);
                Long memberId = getMemberManager().createMember(dataSource, membershipCreation);
                storeMemberStatusAction(dataSource, memberId, membershipCreation.getStatusAction());
                membershipMessageSendingManager.sendMembershipIdToMembershipReporter(memberId);
                return memberId;
            } else {
                throw new DataAccessRollbackException(new ActivatedMembershipException("An activated membership exists for userId " + membershipCreation.getMemberAccountCreation().getUserId() + " and website " + membershipCreation.getWebsite()));
            }
        } catch (DataAccessException e) {
            throw new DataAccessRollbackException("Error creating membership for userId " + membershipCreation.getMemberAccountCreation().getUserId(), e);
        }
    }

    private void sendSubscriptionMessage(MembershipCreation membershipCreation) {
        String email = retrieveEmail(membershipCreation);
        Preconditions.checkNotNull(membershipCreation.getActivationDate());
        Preconditions.checkNotNull(membershipCreation.getExpirationDate());
        Optional.ofNullable(email).ifPresent(mail ->
                membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(mail, membershipCreation, SubscriptionStatus.SUBSCRIBED)
        );
    }

    private String retrieveEmail(MembershipCreation membershipCreation) {
        String email;
        if (nonNull(membershipCreation.getUserCreation()) && isNotBlank(membershipCreation.getUserCreation().getEmail())) {
            email = membershipCreation.getUserCreation().getEmail();
        } else {
            UserInfo userInfo = userApiManager.getUserInfo(membershipCreation.getMemberAccountCreation().getUserId());
            if (isBlank(userInfo.getEmail())) {
                LOGGER.warn("Email not retrieved related to user id received for basic_free creation ({}), no mkt email will be sent",
                        membershipCreation.getMemberAccountCreation().getUserId());
            }
            email = userInfo.getEmail();
        }
        return email;
    }

    private MemberManager getMemberManager() {
        return ConfigurationEngine.getInstance(MemberManager.class);
    }
}
