package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Flight;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberAccountBooking;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MemberUserAreaServiceMapping;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.response.FutureFlight;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.MembershipAccountInfo;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.membership.v1.MemberUserArea;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

public class MemberUserAreaController extends AbstractController implements MemberUserArea {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserAreaController.class);
    private static final String WITH_MEMBERSHIP_ID = " with membership Id ";
    private static final String WITH_ACCOUNT_ID = " with memberAccountId ";

    private final MapperFacade mapper;

    public MemberUserAreaController(MapperFacade mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<MemberSubscriptionDetails> getAllMemberSubscriptionsDetails(Long memberAccountId) {
        try {
            List<com.odigeo.membership.MemberSubscriptionDetails> allMemberSubscriptionDetails = getMemberUserAreaService()
                    .getAllMemberSubscriptionDetails(memberAccountId);
            return mapper.mapAsList(allMemberSubscriptionDetails, MemberSubscriptionDetails.class);
        } catch (DataAccessException | MissingElementException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_ACCOUNT_ID + memberAccountId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipInfo getMembershipInfo(Long membershipId, Boolean skipBooking) throws MembershipServiceException {
        try {
            MemberAccountBooking memberAccountBooking = getMemberUserAreaService().getMembershipInfo(membershipId, skipBooking);
            return MemberUserAreaServiceMapping.asMembershipInfoResponse(memberAccountBooking.getMemberAccount(), memberAccountBooking.getBookingDetailSubscriptionId(), memberAccountBooking.getLastStatusModificationDate());
        } catch (BookingApiException | DataAccessException | MissingElementException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public List<FutureFlight> getFutureFlights(Long membershipId) {
        try {
            Collection<Flight> flights = getMemberUserAreaService().getFlights(membershipId);
            return mapper.mapAsList(flights, FutureFlight.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public long getUserId(Long membershipId) {
        return getMemberAccountByMembershipId(membershipId).getUserId();
    }

    @Override
    public boolean userHasMembershipForBrand(final long userId, final String brandCode) {
        try {
            return getMemberUserAreaService().userHasAnyMembershipForBrand(userId, brandCode);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, " with userId " + userId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public long getMembershipAccountId(final Long membershipId) throws MembershipServiceException {
        try {
            return getMemberService().getMembershipById(membershipId).getMemberAccountId();
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipAccountInfo getMembershipAccount(final Long accountId) throws MembershipServiceException {
        try {
            MemberAccount account = getMemberAccountService().getMemberAccountById(accountId, Boolean.FALSE);
            return MemberUserAreaServiceMapping.asMemberAccountInfoResponse(account);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_ACCOUNT_ID + accountId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    private MemberAccount getMemberAccountByMembershipId(Long membershipId) {
        try {
            return getMemberAccountService().getMemberAccountByMembershipId(membershipId);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }
}
