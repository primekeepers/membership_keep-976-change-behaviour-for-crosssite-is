package com.odigeo.membership.world;

import com.odigeo.membership.functionals.membership.MembershipCreationRequestBuilder;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.response.FutureFlight;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.WebsiteMembership;
import com.odigeo.membership.response.search.MembershipResponse;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.ArrayList;
import java.util.List;

@ScenarioScoped
public class MembershipManagementWorld {

    private final List<Exception> exceptionList = new ArrayList<>();
    private Membership membership;
    private MembershipInfo membershipInfo;
    private List<? extends Membership> membershipList;
    private CreateMembershipSubscriptionRequest membershipCreationRequest;
    private Long createdMemberId;
    private Long userId;
    private UpdateMemberAccountRequest updateMemberAccountRequest;
    private boolean isUpdatedMemberId;
    private MembershipStatus membershipStatus;
    private MembershipRenewal membershipAutoRenewalStatus;
    private List<MemberSubscriptionDetails> subscriptionsDetails = new ArrayList<>();
    private WebsiteMembership websiteMembership;
    private String membershipType;
    private boolean isMembershipSupportedForSite;
    private boolean userHasAnyMembershipForBrand;
    private boolean membershipActivationSucceeded;
    private boolean isMembershipToBeRenewed;
    private Long membershipAccountId;
    private MembershipResponse membershipResponse;
    private boolean operationSuccess;
    private List<FutureFlight> futureFlights;

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public MembershipInfo getMembershipInfo() {
        return membershipInfo;
    }

    public void setMembershipInfo(final MembershipInfo membershipInfo) {
        this.membershipInfo = membershipInfo;
    }

    public List<? extends Membership> getMembershipList() {
        return membershipList;
    }

    public void setMembershipList(List<? extends Membership> membershipList) {
        this.membershipList = membershipList;
    }

    public CreateMembershipSubscriptionRequest getMembershipCreationRequest() {
        return membershipCreationRequest;
    }

    public void setMembershipCreationRequest(List<MembershipCreationRequestBuilder> membershipCreationRequestBuilders) {
        membershipCreationRequest = new CreateMembershipSubscriptionRequest();
        membershipCreationRequest.setUserId(membershipCreationRequestBuilders.get(0).getUserId());
        membershipCreationRequest.setName(membershipCreationRequestBuilders.get(0).getName());
        membershipCreationRequest.setLastNames(membershipCreationRequestBuilders.get(0).getLastNames());
        membershipCreationRequest.setWebsite(membershipCreationRequestBuilders.get(0).getWebsite());
        membershipCreationRequest.setMonthsToRenewal(membershipCreationRequestBuilders.get(0).getMonthsToRenewal());
        membershipCreationRequest.setEmail(membershipCreationRequestBuilders.get(0).getEmail());
        membershipCreationRequest.setMembershipType(membershipCreationRequestBuilders.get(0).getMembershipType());
    }

    public void setCreatedMemberId(Long createdMemberId) {
        this.createdMemberId = createdMemberId;
    }

    public Long getCreatedMemberId() {
        return this.createdMemberId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setMemberAccountUpdateRequest(String memberAccountId, String name, String lastNames) {
        updateMemberAccountRequest = new UpdateMemberAccountRequest();
        updateMemberAccountRequest.setMemberAccountId(memberAccountId);
        updateMemberAccountRequest.setName(name);
        updateMemberAccountRequest.setLastNames(lastNames);
    }

    public UpdateMemberAccountRequest getMemberAccountRequest() {
        return updateMemberAccountRequest;
    }

    public Boolean isMembershipUpdateRequest() {
        return isUpdatedMemberId;
    }

    public void setUpdatedMemberId(Boolean isUpdatedMemberId) {
        this.isUpdatedMemberId = isUpdatedMemberId;
    }

    public MembershipStatus getMembershipStatus() {
        return membershipStatus;
    }

    public MembershipManagementWorld setMembershipStatus(MembershipStatus membershipStatus) {
        this.membershipStatus = membershipStatus;
        return this;
    }

    public List<Exception> getExceptionList() {
        return exceptionList;
    }

    public void addException(Exception e) {
        getExceptionList().add(e);
    }

    public MembershipRenewal getMembershipAutoRenewalStatus() {
        return membershipAutoRenewalStatus;
    }

    public void setMembershipAutoRenewalStatus(MembershipRenewal membershipAutoRenewalStatus) {
        this.membershipAutoRenewalStatus = membershipAutoRenewalStatus;
    }

    public List<MemberSubscriptionDetails> getSubscriptionsDetails() {
        return subscriptionsDetails;
    }

    public void setSubscriptionsDetails(List<MemberSubscriptionDetails> subscriptionsDetails) {
        this.subscriptionsDetails = subscriptionsDetails;
    }

    public MemberSubscriptionDetails getSubscriptionDetailsByMembershipId(long membershipId) {
        return subscriptionsDetails.stream()
                .filter(subDetail -> subDetail.getMembershipId().equals(membershipId))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No subscription details found for membershipId: " + membershipId));
    }

    public String getMembershipType() {
        return membershipType;
    }

    public WebsiteMembership getWebsiteMembership() {
        return websiteMembership;
    }

    public void setWebsiteMembership(WebsiteMembership websiteMembership) {
        this.websiteMembership = websiteMembership;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public boolean isMembershipSupportedForSite() {
        return isMembershipSupportedForSite;
    }

    public void setMembershipSupportedForSite(final boolean membershipSupportedForSite) {
        isMembershipSupportedForSite = membershipSupportedForSite;
    }

    public boolean isUserHasAnyMembershipForBrand() {
        return userHasAnyMembershipForBrand;
    }

    public void setUserHasAnyMembershipForBrand(final boolean userHasAnyMembershipForBrand) {
        this.userHasAnyMembershipForBrand = userHasAnyMembershipForBrand;
    }

    public boolean isMembershipActivationSucceeded() {
        return membershipActivationSucceeded;
    }

    public void setMembershipActivationSucceeded(boolean isSuccess) {
        this.membershipActivationSucceeded = isSuccess;
    }

    public Long getMembershipAccountId() {
        return membershipAccountId;
    }

    public void setMembershipAccountId(final Long membershipAccountId) {
        this.membershipAccountId = membershipAccountId;
    }

    public boolean isMembershipToBeRenewed() {
        return isMembershipToBeRenewed;
    }

    public void setMembershipToBeRenewed(boolean membershipToBeRenewed) {
        isMembershipToBeRenewed = membershipToBeRenewed;
    }

    public MembershipResponse getMembershipResponse() {
        return membershipResponse;
    }

    public void setMembershipResponse(MembershipResponse membershipResponse) {
        this.membershipResponse = membershipResponse;
    }

    public boolean isOperationSuccess() {
        return operationSuccess;
    }

    public void setOperationSuccess(boolean operationSuccess) {
        this.operationSuccess = operationSuccess;
    }

    public List<FutureFlight> getFutureFlights() {
        return futureFlights;
    }

    public void setFutureFlights(List<FutureFlight> futureFlights) {
        this.futureFlights = futureFlights;
    }
}
