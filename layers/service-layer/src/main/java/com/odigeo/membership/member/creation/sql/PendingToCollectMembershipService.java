package com.odigeo.membership.member.creation.sql;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.product.MembershipSubscriptionFeeStore;
import com.odigeo.membership.recurring.MembershipRecurringStore;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Predicate;

import static com.odigeo.membership.product.MembershipProductType.MEMBERSHIP_RENEWAL;
import static java.util.Objects.nonNull;

public class PendingToCollectMembershipService extends MembershipSQLServiceProvider {

    private MemberAccountService memberAccountService;
    private MembershipFeesService membershipFeesService;

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException, MissingElementException {
        MemberAccount memberAccount = getMemberAccountService().getMemberAccountById(membershipCreation.getMemberAccountId(), true);
        Optional<Long> membershipId = memberAccount.getMemberships().stream()
                .filter(getPendingToCollectWithSameWebsitePredicate(membershipCreation))
                .findAny()
                .map(Membership::getId);
        return membershipId.isPresent() ? membershipId.get() : createNewMembershipInPendingToCollect(dataSource, membershipCreation);
    }

    private Long createNewMembershipInPendingToCollect(DataSource dataSource, MembershipCreation membershipCreation)
            throws DataAccessException {
        membershipCreation.setProductStatus(ProductStatus.INIT.toString());
        createFeeContainer(membershipCreation).ifPresent(membershipCreation::setFeeContainerId);
        long membershipId = createMembershipForAccount(dataSource, membershipCreation);
        storeRecurringIdIfNotNull(dataSource, membershipCreation, membershipId);
        storeMembershipRenewalFee(dataSource, membershipId, membershipCreation);
        storeMemberStatusAction(dataSource, membershipId, StatusAction.CREATION);
        incrementCreationMetricsCounter(MetricsNames.PENDING_TO_COLLECT_CREATION);
        return membershipId;
    }

    private Optional<Long> createFeeContainer(MembershipCreation membershipCreation) {
        return getMembershipFeesService().requestFeeContainerCreation(membershipCreation.getSubscriptionPrice(), membershipCreation.getCurrencyCode());
    }

    private void storeRecurringIdIfNotNull(DataSource dataSource, MembershipCreation membershipCreation,
                                           long membershipId) throws DataAccessException {
        if (nonNull(membershipCreation.getRecurringId())) {
            try {
                getMembershipRecurringStore().insertMembershipRecurring(dataSource, membershipId, membershipCreation.getRecurringId());
            } catch (SQLException e) {
                throw new DataAccessRollbackException("Error inserting recurringId for userId " + membershipCreation.getMemberAccountCreation().getUserId(), e);
            }
        }
    }

    private void storeMembershipRenewalFee(DataSource dataSource, long membershipId, MembershipCreation membershipCreation) throws DataAccessException {
        try {
            getMembershipSubscriptionFeeStore().createMembershipFee(dataSource, String.valueOf(membershipId),
                    membershipCreation.getSubscriptionPrice(), membershipCreation.getCurrencyCode(), MEMBERSHIP_RENEWAL.name());
        } catch (SQLException e) {
            throw new DataAccessRollbackException("There was an error trying to insert Membership Fee" + membershipId, e);
        }
    }

    private Long createMembershipForAccount(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        try {
            return getMembershipStore().createMember(dataSource, membershipCreation);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("There was an error in membership creation for userId "
                    + membershipCreation.getMemberAccountCreation().getUserId(), e);
        }
    }

    private Predicate<Membership> getPendingToCollectWithSameWebsitePredicate(MembershipCreation membershipCreation) {
        return membership -> MemberStatus.PENDING_TO_COLLECT.equals(membership.getStatus())
                && membership.getWebsite().equals(membershipCreation.getWebsite());
    }

    private MembershipStore getMembershipStore() {
        return ConfigurationEngine.getInstance(MembershipStore.class);
    }

    private MembershipRecurringStore getMembershipRecurringStore() {
        return ConfigurationEngine.getInstance(MembershipRecurringStore.class);
    }

    private MembershipSubscriptionFeeStore getMembershipSubscriptionFeeStore() {
        return ConfigurationEngine.getInstance(MembershipSubscriptionFeeStore.class);
    }

    private MemberAccountService getMemberAccountService() {
        return Optional.ofNullable(memberAccountService).orElseGet(() -> {
            memberAccountService = ConfigurationEngine.getInstance(MemberAccountService.class);
            return memberAccountService;
        });
    }

    private MembershipFeesService getMembershipFeesService() {
        return Optional.ofNullable(membershipFeesService).orElseGet(() -> {
            membershipFeesService = ConfigurationEngine.getInstance(MembershipFeesService.class);
            return membershipFeesService;
        });
    }
}
