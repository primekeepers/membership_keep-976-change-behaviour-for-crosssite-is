package com.odigeo.membership.functionals;

import com.google.inject.Inject;
import com.odigeo.membership.functionals.database.DatabaseInstallerWorld;
import com.odigeo.membership.functionals.jboss.JbossServerWorld;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.mocks.feesapi.FeesApiWorld;
import com.odigeo.membership.mocks.kafka.MessageControllerWorld;
import com.odigeo.membership.mocks.recurringcollectionresource.RecurringCollectionResourceWorld;
import com.odigeo.membership.mocks.userapi.UserApiWorld;
import com.odigeo.membership.mocks.visitengine.VisitEngineServiceWorld;
import com.odigeo.membership.server.ServerStopException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;

@ScenarioScoped
public class InstallerScenarioEnvironment {

    private static final Logger LOGGER = Logger.getLogger(InstallerScenarioEnvironment.class);

    @Inject
    private JbossServerWorld jbossServerWorld;
    @Inject
    private DatabaseInstallerWorld databaseInstallerWorld;
    @Inject
    private BookingApiServiceWorld bookingApiServiceWorld;
    @Inject
    private MessageControllerWorld messageControllerWorld;
    @Inject
    private VisitEngineServiceWorld visitEngineServiceWorld;
    @Inject
    private UserApiWorld userApiWorld;
    @Inject
    private FeesApiWorld feesApiWorld;
    @Inject
    private RecurringCollectionResourceWorld recurringCollectionResourceWorld;

    @Before
    public void install() throws InterruptedException, SQLException, ClassNotFoundException, IOException, ServerStopException {
        LOGGER.info("Start install environment for a scenario");
        databaseInstallerWorld.clearData();
        jbossServerWorld.installScenario();
        bookingApiServiceWorld.install();
        visitEngineServiceWorld.install();
        userApiWorld.install();
        feesApiWorld.install();
        recurringCollectionResourceWorld.install();
        LOGGER.info("End install environment for a scenario");
    }

    @After
    public void uninstall() {
        LOGGER.info("Start uninstall environment for a scenario");
        bookingApiServiceWorld.uninstall();
        visitEngineServiceWorld.uninstall();
        userApiWorld.uninstall();
        messageControllerWorld.uninstall();
        feesApiWorld.uninstall();
        recurringCollectionResourceWorld.uninstall();
        LOGGER.info("End uninstall environment for a scenario");
    }
}
