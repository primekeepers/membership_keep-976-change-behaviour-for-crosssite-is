package com.odigeo.membership.steps;

import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import com.odigeo.membership.functionals.membership.MembershipProductBookingApiVerifier;
import com.odigeo.membership.functionals.membership.MembershipProductFeeBuilder;
import com.odigeo.membership.functionals.membership.MembershipProductVerifier;
import com.odigeo.membership.functionals.membership.StatusVerifier;
import com.odigeo.membership.functionals.membership.StatusVerifierWrapper;
import com.odigeo.membership.world.MembershipProductManagementWorld;
import com.odigeo.product.v2.exception.ProductException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

@ScenarioScoped
public class MembershipProductSteps extends CommonSteps {

    private final MembershipProductManagementWorld world;
    private final DatabaseWorld databaseWorld;

    @Inject
    public MembershipProductSteps(MembershipProductManagementWorld world, ServerConfiguration serverConfiguration, DatabaseWorld databaseWorld) {
        super(serverConfiguration);
        this.world = world;
        this.databaseWorld = databaseWorld;
    }

    @When("^the product information is requested for the product id (\\w+)$")
    public void theProductInformationIsRequestedForTheProductIdProductId(String productId) throws ProductException {
        world.setProduct(membershipProductServiceV2.getProduct(productId));
    }

    @When("^the product information details is requested for the product id (\\w+)$")
    public void theProductInformationDetailsIsRequestedForTheProductIdProductId(String productId) throws ProductException {
        world.setProduct(membershipProductServiceV2.getProductDetails(productId));
    }

    @Then("^the response to the membership product request is:$")
    public void theResponseToTheMembershipProductRequestIs(List<MembershipProductVerifier> verifiers) {
        for (MembershipProductVerifier membershipProductVerifier : verifiers) {
            membershipProductVerifier.verifyMembershipProduct(world.getProduct());
        }
    }

    @And("^the next membership product fee stored in db:$")
    public void theNextMembershipProductFeeStoredInDb(List<MembershipProductFeeBuilder> membershipProductFeeBuilders) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addMembershipProductFees(membershipProductFeeBuilders);
    }

    @When("^the product BookingApi information is requested for the product id (\\w+)$")
    public void theProductBookingApiInformationIsRequestedForTheProductId(String productId) throws ProductException {
        world.setProductBookingApiInfo(membershipProductServiceV2.getBookingApiProductInfo(productId));
    }

    @Then("^the response to the membership product Booking Api request is:$")
    public void theResponseToTheMembershipBookingApiProductRequestIs(List<MembershipProductBookingApiVerifier> verifiers) {
        for (MembershipProductBookingApiVerifier membershipProductBookingApiVerifier : verifiers) {
            membershipProductBookingApiVerifier.verifyMembershipProduct(world.getBookingApiMembershipInfo());
        }
    }

    @When("^save partial information for product id (\\w+)$")
    public void savePartialInformationProductId(String productId) throws ProductException {
        membershipProductServiceV2.saveProduct(productId, Boolean.TRUE);
    }

    @When("^save total information for product id (\\w+)$")
    public void saveTotalInformationProductId(String productId) throws ProductException {
        membershipProductServiceV2.saveProduct(productId, Boolean.FALSE);
    }

    @When("^commit transaction for product id (\\w+)$")
    public void commitTransactionProductId(String productId) throws ProductException {
        membershipProductServiceV2.commitTransactionProduct(productId, productId);
    }

    @Then("^the product and membership status for product id (\\w+) are:$")
    public void theProductAndMembershipStatusAre(String productId, List<StatusVerifier> verifiers) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilderDB = databaseWorld.getMembershipById(Long.valueOf(productId));
        for (StatusVerifier statusVerifier : verifiers) {
            StatusVerifierWrapper statusVerifierWrapper = new StatusVerifierWrapper(membershipBuilderDB.getProductStatus(), membershipBuilderDB.getStatus(),
                membershipBuilderDB.getBalance(), membershipBuilderDB.getActivationDate(), membershipBuilderDB.getExpirationDate());
            statusVerifier.verifyStatus(statusVerifierWrapper);
        }
    }
}
