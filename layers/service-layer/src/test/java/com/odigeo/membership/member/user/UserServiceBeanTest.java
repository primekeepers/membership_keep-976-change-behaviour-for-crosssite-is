package com.odigeo.membership.member.user;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserDTO;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v2.UserApiService;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.requests.ThirdAppRequest;
import com.odigeo.userprofiles.api.v2.model.responses.UserRegisteredResponse;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidFindException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.StatusUserException;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class UserServiceBeanTest {

    private static final String BRAND = "ED";
    private static final Brand BRAND_FROM_ENUM = Brand.ED;
    private static final String WEBSITE = "ES";
    private static final String LOCALE = "es_ES";
    private static final Integer TRAFFIC_INTERFACE_ID = 1;
    private static final String EMAIL = "kenny.mccormic@mail.com";
    private static final long SOME_USER_ID = 1234L;
    private static final long SOME_OTHER_USER_ID = 4321L;

    @Mock
    private UserApiService userApiService;

    @Mock
    private UserApiManager userApiManager;

    @Mock
    private UserProfileApiSecurityConfiguration userApiSecurityConfig;

    @InjectMocks
    private UserServiceBean userServiceBean = new UserServiceBean();

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @BeforeMethod
    public void setup() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(UserApiService.class).toInstance(userApiService);
        binder.bind(UserApiManager.class).toInstance(userApiManager);
    }

    @Test
    public void saveNewUser() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserNotRegisteredResponse());
        when(userApiService.saveUser(any(User.class))).thenReturn(getUserWithId());

        userServiceBean.saveUser(getValidUserCreation());

        verify(userApiService).saveUser(userArgumentCaptor.capture());
        assertEquals(userArgumentCaptor.getValue().getEmail(), EMAIL);
        assertEquals(userArgumentCaptor.getValue().getBrand().name(), BRAND);
        assertEquals(userArgumentCaptor.getValue().getLocale(), LOCALE);
        assertEquals(userArgumentCaptor.getValue().getWebsite(), WEBSITE);
        assertEquals(userArgumentCaptor.getValue().getTrafficInterface().getId(), TRAFFIC_INTERFACE_ID.intValue());
    }

    @Test
    public void saveUserWithExistingUserParams() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserRegisteredResponse(Status.PENDING_LOGIN));
        userServiceBean.saveUser(getValidUserCreation());
        verify(userApiService, never()).saveUser(any(User.class));
    }

    @Test
    public void fixAccountWithStatusOne() throws UserApiException, DataAccessException, StatusUserException, InvalidCredentialsException, RemoteException, InvalidValidationException {
        when(userApiManager.getUser(anyLong())).thenReturn(createNewUserWithStatus(Status.PENDING_MAIL_CONFIRMATION));
        when(userApiManager.deletePendingMailConfirmationUserAccount(EMAIL, BRAND_FROM_ENUM)).thenReturn(Boolean.TRUE);
        when(userApiService.saveUser(any(User.class))).thenReturn(createReturnedUserServiceUser());
        Optional<Long> expectedNewUserId = userServiceBean.fixPendingMailConfirmationUserAccount(SOME_USER_ID);
        assertEquals(expectedNewUserId.get().longValue(), SOME_OTHER_USER_ID);
    }

    @Test
    public void fixAccountWithStatusSix() throws StatusUserException, InvalidCredentialsException, RemoteException, InvalidValidationException, UserApiException, DataAccessException {
        when(userApiManager.getUser(anyLong())).thenReturn(createNewUserWithStatus(Status.PENDING_LOGIN));
        userServiceBean.fixPendingMailConfirmationUserAccount(SOME_USER_ID);
        verify(userApiService, never()).saveUser(any(User.class));
    }

    @Test(expectedExceptions = {UserApiException.class}, expectedExceptionsMessageRegExp = ".*Could not delete user with user Id.*")
    public void failToDeletePendingMailConfirmationUserAccount() throws UserApiException, DataAccessException {
        when(userApiManager.getUser(anyLong())).thenReturn(createNewUserWithStatus(Status.PENDING_MAIL_CONFIRMATION));
        when(userApiManager.deletePendingMailConfirmationUserAccount(EMAIL, BRAND_FROM_ENUM)).thenReturn(Boolean.FALSE);
        userServiceBean.fixPendingMailConfirmationUserAccount(SOME_USER_ID);
    }

    @Test(expectedExceptions = {DataAccessException.class}, expectedExceptionsMessageRegExp = ".*Problem checking isUserRegistered.*")
    public void saveUserWithExceptionInIsRegisteredCall() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenThrow(new InvalidCredentialsException("bad username or password"));
        userServiceBean.saveUser(getValidUserCreation());
    }

    @Test(expectedExceptions = {DataAccessException.class}, expectedExceptionsMessageRegExp = ".*Problem occurred saving user*.")
    public void saveUserWithExceptionInUserProfileSaveUser() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserNotRegisteredResponse());
        when(userApiService.saveUser(any(User.class))).thenThrow(new InvalidCredentialsException("bad credentials"));
        userServiceBean.saveUser(getValidUserCreation());
    }

    @Test
    public void getUserIdByEmailAndWebSite_UserIsRegisteredAndActive() throws InvalidCredentialsException, InvalidFindException, RemoteException, InvalidValidationException, UserAccountNotFoundException, DataAccessException {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserRegisteredResponse(Status.ACTIVE));
        assertNotNull(userServiceBean.getUserIdBy(EMAIL, WEBSITE));
    }

    @Test(expectedExceptions = {UserAccountNotFoundException.class}, expectedExceptionsMessageRegExp = ".*User account was not found with email(?s).*")
    public void getUserIdByEmailAndWebSite_UserIsNotRegistered() throws InvalidCredentialsException, InvalidFindException, RemoteException, InvalidValidationException, UserAccountNotFoundException, DataAccessException {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserNotRegisteredResponse());
        userServiceBean.getUserIdBy(EMAIL, WEBSITE);
    }

    @Test(expectedExceptions = {UserAccountNotFoundException.class}, expectedExceptionsMessageRegExp = ".*User account was not found with email(?s).*")
    public void getUserIdByEmailAndWebSite_UserWasDeleted() throws InvalidCredentialsException, InvalidFindException, RemoteException, InvalidValidationException, UserAccountNotFoundException, DataAccessException {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserRegisteredResponse(Status.DELETED));
        userServiceBean.getUserIdBy(EMAIL, WEBSITE);
    }

    private static Optional<UserDTO> createNewUserWithStatus(Status status) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(SOME_USER_ID);
        userDTO.setStatus(status);
        userDTO.setEmail(EMAIL);
        userDTO.setBrand(BRAND_FROM_ENUM);
        return Optional.of(userDTO);
    }

    private static User createReturnedUserServiceUser() {
        User user = new User();
        user.setId(SOME_OTHER_USER_ID);
        return user;
    }

    private static User getUserWithId() {
        User user = new User();
        user.setId(17L);
        return user;
    }

    private static UserRegisteredResponse getUserNotRegisteredResponse() {
        UserRegisteredResponse response = new UserRegisteredResponse();
        response.setRegistered(false);
        response.setUserStatus(Status.UNREGISTERED);
        return response;
    }

    private static UserRegisteredResponse getUserRegisteredResponse(Status status) {
        UserRegisteredResponse response = new UserRegisteredResponse();
        response.setRegistered(true);
        response.setUserStatus(status);
        response.setId(39L);
        return response;
    }

    private UserCreation getValidUserCreation() {
        return new UserCreation.Builder()
                .withLocale(LOCALE)
                .withEmail(EMAIL)
                .withWebsite(WEBSITE)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .build();
    }
}
