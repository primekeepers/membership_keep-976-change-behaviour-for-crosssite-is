package com.odigeo.membership.product;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.exception.DataNotFoundException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class MembershipSubscriptionFeeStoreTest {

    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    private MembershipSubscriptionFeeStore store;

    private static final String MEMBERSHIP_ID = "123";
    private static final BigDecimal AMOUNT = BigDecimal.TEN;
    private static final String CURRENCY = "EUR";
    private static final String FEE_TYPE = "RENEWAL";
    private static final Long FEE_ID = 1L;

    private static final int ONCE = 1;
    private static final int FOUR = 4;

    private static final MembershipFee MEMBERSHIP_FEE = new MembershipFee(MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE);

    @BeforeMethod
    public void before() throws Exception {
        openMocks(this);
        mockDatabase();

        ConfigurationEngine.init();
        store = ConfigurationEngine.getInstance(MembershipSubscriptionFeeStore.class);
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    private void setUpFeeSequence() throws SQLException{
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong(any())).thenReturn(FEE_ID);
    }

    @Test
    public void testCreateMembershipFee() throws Exception {
        setUpFeeSequence();
        store.createMembershipFee(dataSource, MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE);
        verify(preparedStatement, times(ONCE)).execute();
        verify(preparedStatement, times(FOUR)).setString(anyInt(), any());
        verify(preparedStatement, times(ONCE)).setBigDecimal(anyInt(), any());
    }

    @Test
    public void testGetMembershipFee() throws Exception {
        setUpFee();
        List<MembershipFee> membershipFeeList = store.getMembershipFee(dataSource, MEMBERSHIP_ID);
        assertEquals(MEMBERSHIP_FEE, membershipFeeList.get(0));
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMembershipFeeNoResults() throws Exception {
        setUpNoFee();
        store.getMembershipFee(dataSource, MEMBERSHIP_ID);
    }

    private void setUpNoFee() throws SQLException {
        when(resultSet.next()).thenReturn(false);
    }

    private void setUpFee() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        when(resultSet.getBigDecimal("AMOUNT")).thenReturn(AMOUNT);
        when(resultSet.getString("CURRENCY")).thenReturn(CURRENCY);
        when(resultSet.getString("MEMBERSHIP_ID")).thenReturn(MEMBERSHIP_ID);
        when(resultSet.getString("FEE_TYPE")).thenReturn(FEE_TYPE);
    }
}