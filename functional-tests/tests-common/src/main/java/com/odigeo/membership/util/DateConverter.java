package com.odigeo.membership.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.isNull;

public final class DateConverter {

    private DateConverter() {
    }

    public static Optional<Date> toIsoDate(final String stringDate) {
        if (isNull(stringDate)) {
            return Optional.empty();
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            return Optional.ofNullable(simpleDateFormat.parse(stringDate));
        } catch (ParseException e) {
            return Optional.empty();
        }
    }

    public static java.sql.Date toSqlDate(final String dateStr) {
        final Long transformedDate = toIsoDate(dateStr).map(Date::getTime).orElse(null);
        if (Objects.nonNull(transformedDate)) {
            return new java.sql.Date(transformedDate);
        }
        return null;
    }
}
