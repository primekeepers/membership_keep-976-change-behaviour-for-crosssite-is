package com.odigeo.membership.member.update.operation.autorenewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;

public class EnableAutoRenewalOperationTest extends AutoRenewalChangeOperationTest {

    private EnableAutoRenewalOperation enableAutoRenewalOperation;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        enableAutoRenewalOperation = new EnableAutoRenewalOperation(dataSource, membershipStore, membershipMessageSendingManager, memberService);
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setMembershipRenewal(MembershipRenewal.DISABLED)
                .build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.updateAutoRenewal(eq(dataSource), eq(membership.getId()), eq(AutoRenewalOperation.ENABLE_AUTO_RENEW)))
                .thenReturn(Boolean.TRUE);
        assertTrue(enableAutoRenewalOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
    }

    @Test
    public void testUpdateStoreAutoRenewalFail() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setMembershipRenewal(MembershipRenewal.DISABLED)
                .build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.updateAutoRenewal(eq(dataSource), eq(membership.getId()), eq(AutoRenewalOperation.ENABLE_AUTO_RENEW)))
                .thenReturn(Boolean.FALSE);
        assertFalse(enableAutoRenewalOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verifyZeroInteractions(membershipMessageSendingManager);
    }

    @Test
    public void testUpdateAlreadyEnabled() throws ExistingRecurringException, DataAccessException, MissingElementException {
        configureOperation(MembershipRenewal.ENABLED);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        assertTrue(enableAutoRenewalOperation.update(updateMembership));
        verifyZeroInteractions(membershipMessageSendingManager, membershipStore);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateWithException() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        configureOperationException(MembershipRenewal.DISABLED, AutoRenewalOperation.ENABLE_AUTO_RENEW);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        enableAutoRenewalOperation.update(updateMembership);
    }

}