package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.exception.MembershipUnauthorizedException;
import com.odigeo.membership.member.rest.utils.AuthenticationChecker;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.v1.MembershipService;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.interception.PostMatchContainerRequestContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipAuthenticationInterceptorTest {

    private static final String HASH_HEADER_NAME = "hash";
    private static final String CLIENT_HEADER_NAME = "client";
    private static final String HASH_HEADERS = "asdfqwerty";
    private static final String CLIENT_HEADERS = "one-front";
    private static final String MODULE_NAME_PREFIX = "/membership";
    private static final String UNAUTHENTICATED_PATH = "/membership/v1/website/{siteId}";
    private static final String AUTHENTICATED_PATH = "/membership/v1//activate/{membershipId}";

    @Mock
    private UriInfo uriInfo;
    @Mock
    private PostMatchContainerRequestContext httpRequest;
    @Mock
    private MultivaluedHashMap<String, String> httpHeaders;
    @Mock
    private AuthenticationChecker authenticationChecker;
    @Mock
    private ResourceMethodInvoker resourceMethodInvoker;
    private MembershipAuthenticationInterceptor membershipAuthenticationInterceptor;
    private Method method;

    @BeforeMethod
    public void setUp() throws NoSuchMethodException {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipAuthenticationInterceptor = new MembershipAuthenticationInterceptor();
    }

    @Test
    public void testExecuteInterceptorAuthorized() throws IOException, NoSuchMethodException {
        fillHttpHeaders();
        mockUnauthenticatedCall();
        when(authenticationChecker.check(CLIENT_HEADERS, HASH_HEADERS, MODULE_NAME_PREFIX + UNAUTHENTICATED_PATH)).thenReturn(Boolean.TRUE);
        membershipAuthenticationInterceptor.filter(httpRequest);
    }

    @Test(expectedExceptions = MembershipUnauthorizedException.class)
    public void testExecuteInterceptorNotAuthorized() throws IOException, NoSuchMethodException {
        mockAuthenticatedCall();
        when(authenticationChecker.check(CLIENT_HEADERS, HASH_HEADERS, MODULE_NAME_PREFIX + AUTHENTICATED_PATH)).thenReturn(Boolean.FALSE);
        membershipAuthenticationInterceptor.filter(httpRequest);
    }

    @Test(expectedExceptions = MembershipUnauthorizedException.class)
    public void testExecuteInterceptorMissingHeaders() throws IOException, NoSuchMethodException {
        when(httpRequest.getHeaders()).thenReturn(new MultivaluedHashMap<>());
        mockAuthenticatedCall();
        membershipAuthenticationInterceptor.filter(httpRequest);
    }

    @Test(expectedExceptions = MembershipUnauthorizedException.class)
    public void testExecuteInterceptorNullHeaders() throws IOException, NoSuchMethodException {
        when(httpRequest.getHeaders()).thenReturn(null);
        mockAuthenticatedCall();
        membershipAuthenticationInterceptor.filter(httpRequest);
    }

    @Test
    public void testAccept() throws NoSuchMethodException, IOException {
        fillHttpHeaders();
        mockAuthenticatedCall();
        when(authenticationChecker.check(eq(CLIENT_HEADERS), eq(HASH_HEADERS), anyString())).thenReturn(Boolean.TRUE);
        membershipAuthenticationInterceptor.filter(httpRequest);
        mockUnauthenticatedCall();
        membershipAuthenticationInterceptor.filter(httpRequest);
    }

    private void fillHttpHeaders() {
        httpHeaders = new MultivaluedHashMap<>();
        httpHeaders.add(HASH_HEADER_NAME, HASH_HEADERS);
        httpHeaders.add(CLIENT_HEADER_NAME, CLIENT_HEADERS);
        when(httpRequest.getHeaders()).thenReturn(httpHeaders);
    }

    private void mockAuthenticatedCall() throws NoSuchMethodException {
        method = MembershipService.class.getMethod("activateMembership", Long.TYPE, ActivationRequest.class);
        when(httpRequest.getResourceMethod()).thenReturn(resourceMethodInvoker);
        when(httpRequest.getResourceMethod().getMethod()).thenReturn(method);
        when(httpRequest.getUriInfo()).thenReturn(uriInfo);
        when(httpRequest.getUriInfo().getPath()).thenReturn(AUTHENTICATED_PATH);
    }

    private void mockUnauthenticatedCall() throws NoSuchMethodException {
        method = MembershipService.class.getMethod("isMembershipPerksActiveOn", String.class);
        when(httpRequest.getResourceMethod()).thenReturn(resourceMethodInvoker);
        when(httpRequest.getResourceMethod().getMethod()).thenReturn(method);
        when(httpRequest.getUriInfo()).thenReturn(uriInfo);
        when(httpRequest.getUriInfo().getPath()).thenReturn(UNAUTHENTICATED_PATH);
    }

    private void configure(Binder binder) {
        binder.bind(AuthenticationChecker.class).toProvider(() -> authenticationChecker);
        binder.bind(ContainerRequestContext.class).toInstance(httpRequest);
    }
}