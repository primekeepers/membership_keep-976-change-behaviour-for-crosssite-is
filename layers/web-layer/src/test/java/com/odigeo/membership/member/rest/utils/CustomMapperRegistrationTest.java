package com.odigeo.membership.member.rest.utils;

import com.odigeo.bookingapi.v14.responses.CreditCard;
import com.odigeo.bookingapi.v14.responses.CreditCardType;
import com.odigeo.membership.Flight;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.response.FutureFlight;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CustomMapperRegistrationTest {
    private static final String CCNUMBER = "1234567890123456";
    private static final String CCOWNER = "Mario Rossi";
    private MapperFacade mapperFacade;

    @BeforeMethod
    public void setUp() throws Exception {
        MapperFactory mapperFactory = new GeneralMapperCreator().getMapperFactory();
        CustomMapperRegistration.registerCreditCardMapper(mapperFactory);
        CustomMapperRegistration.registerCreditCardTypeMapper(mapperFactory);
        CustomMapperRegistration.registerMemberSubscriptionDetailsMapper(mapperFactory);
        CustomMapperRegistration.registerFlightMapper(mapperFactory);
        mapperFacade = mapperFactory.getMapperFacade();
    }

    @Test
    public void testCustomMapper() throws Exception {
        CreditCard creditCard = initCreditCard();
        com.odigeo.membership.response.CreditCard creditCardMembership = new com.odigeo.membership.response.CreditCard();
        mapperFacade.map(creditCard, creditCardMembership);
        assertEquals(creditCardMembership.getExpirationDateMonth(), creditCard.getExpirationMonth());
        assertEquals(creditCardMembership.getExpirationDateYear(), creditCard.getExpirationYear());
        assertEquals(creditCardMembership.getExpirationDateYearFormated(), creditCard.getExpirationDate());
        assertEquals(creditCardMembership.getCreditCardType().getCreditCardCode(), creditCard.getCreditCardType().getCode());
        assertEquals(creditCardMembership.getCreditCardType().getCreditCardName(), creditCard.getCreditCardType().getName());
        MemberSubscriptionDetails memberSubscriptionDetails = new MemberSubscriptionDetails();
        memberSubscriptionDetails.setSubscriptionPaymentMethodType("CREDITCARD");
        memberSubscriptionDetails.setSubscriptionPaymentMethod(creditCard);
        memberSubscriptionDetails.setMembershipId(1L);
        com.odigeo.membership.response.MemberSubscriptionDetails memberApiSubscriptiondetails = new com.odigeo.membership.response.MemberSubscriptionDetails();
        mapperFacade.map(memberSubscriptionDetails, memberApiSubscriptiondetails);
        assertEquals(memberSubscriptionDetails.getSubscriptionPaymentMethodType(), memberApiSubscriptiondetails.getSubscriptionPaymentMethod().getCreditCardType().getCreditCardType());

    }

    private CreditCard initCreditCard() {
        CreditCard creditCard = new CreditCard();
        creditCard.setCreditCardNumber(CCNUMBER);
        creditCard.setOwner(CCOWNER);
        creditCard.setId(1234L);
        creditCard.setExpirationYear("20");
        creditCard.setExpirationMonth("12");
        creditCard.setExpirationDate("1220");
        CreditCardType ccType = new CreditCardType();
        ccType.setCode("VI");
        ccType.setName("VISA CREDIT");
        creditCard.setCreditCardType(ccType);
        return creditCard;
    }

    @Test
    public void testRegisterFlightMapper() {
        Flight flight = new Flight(1L, ZonedDateTime.parse("2020-12-31T00:00Z"),"iMADestination");
        FutureFlight res = mapperFacade.map(flight, FutureFlight.class);
        assertNotNull(res);
        assertEquals(res.getDepartureDate(), "2020-12-31T00:00Z");
        assertEquals(res.getDestination(), "iMADestination");
    }
}
