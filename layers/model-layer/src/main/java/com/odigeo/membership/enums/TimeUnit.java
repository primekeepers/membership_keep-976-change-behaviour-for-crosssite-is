package com.odigeo.membership.enums;

import org.apache.commons.lang.StringUtils;

public enum TimeUnit {
    DAYS,
    MONTHS;

    public static TimeUnit getFromNullableValue(String name) {
        if (StringUtils.isEmpty(name)) {
            return TimeUnit.MONTHS;
        }
        return valueOf(name);
    }
}
