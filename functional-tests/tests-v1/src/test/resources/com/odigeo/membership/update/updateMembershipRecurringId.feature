Feature: Testing update membership to insert recurring ID into the ddbb

  Scenario: Call updateMembership with a valid recurring ID
    Given the next memberAccount stored in db:
      | memberAccountId   | userId    | firstName   | lastNames   |
      | 1234              | 4321      | HAN         | SOLO        |
    And the next membership stored in db:
      | memberId    | website   | status    | memberAccountId   | activationDate    | expirationDate    | balance       | autoRenewal   |
      | 789         | ES        | ACTIVATED | 1234              | 2018-06-06        | 2019-06-06        | 44.99         | ENABLED       |
    And property to send IDs to Kafka has value false in db:
    When requested to update membership
      | membershipId    | operation             | recurringId   |
      | 789             | INSERT_RECURRING_ID   | 1234          |
    Then the following membership recurring information is stored in db:
      | userId | membershipId | recurringId   | website |
      | 4321   | 789          | 1234          | ES      |
    And the result of the membership update operation is true


  Scenario: Call updateMembership without recurring ID
    Given the next memberAccount stored in db:
      | memberAccountId   | userId    | firstName   | lastNames   |
      | 1234              | 4321      | HAN         | SOLO        |
    And the next membership stored in db:
      | memberId    | website   | status    | memberAccountId   | activationDate    | expirationDate    | balance       | autoRenewal   |
      | 789         | ES        | ACTIVATED | 1234              | 2018-06-06        | 2019-06-06        | 44.99         | ENABLED       |
    And property to send IDs to Kafka has value false in db:
    When requested to update membership
      | membershipId    | operation             |
      | 789             | INSERT_RECURRING_ID   |
    Then Exceptions occurs

  Scenario: Call updateMembership for an non existing membership ID:
    When requested to update membership
      | membershipId    | operation             | recurringId   |
      | 789             | INSERT_RECURRING_ID   | 123456        |
    Then Exceptions occurs
