package com.odigeo.membership.product;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MembershipProductTypeTest {

    @DataProvider(name = "membershipProductTypes")
    public Object[][] getMembershipProductTypes() {
        return new Object[][] {
            {MembershipProductType.MEMBERSHIP, "AE11"},
            {MembershipProductType.MEMBERSHIP_RENEWAL, "AE10"},
        };
    }

    @Test(dataProvider = "membershipProductTypes")
    public void testMembershipProductTypes(MembershipProductType membershipProductType,
                                           String subCodeFee) {
        assertEquals(membershipProductType.getFeeSubCode(), subCodeFee);
    }
}