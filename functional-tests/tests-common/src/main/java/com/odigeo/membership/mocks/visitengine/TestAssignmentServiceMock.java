package com.odigeo.membership.mocks.visitengine;

import com.google.inject.Inject;
import com.odigeo.membership.mocks.visitengine.request.TestAssignmentRequestTest;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentBuilder;
import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.multitest.MultitestException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignment;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentNotFoundException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TestAssignmentServiceMock implements TestAssignmentService {

    private final Random random;
    private final Map<String, TestAssignment> testAssignmentBuilderMap;

    @Inject
    public TestAssignmentServiceMock(Random random) {
        this.random = random;
        this.testAssignmentBuilderMap = new HashMap<>();
    }

    @Override
    public TestAssignment getTestAssignment(String testName) throws VisitEngineException, TestAssignmentNotFoundException {
        return testAssignmentBuilderMap
                .computeIfAbsent(testName, testAssignment -> new TestAssignmentBuilder(random)
                        .testName(testName).enabled(false).build());
    }

    @Override
    public List<TestAssignment> findAllTestAssignment() throws MultitestException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> findAllTestAssignmentTestName() throws MultitestException {
        throw new UnsupportedOperationException();
    }

    public void addTestAssignmentBuilders(List<TestAssignmentRequestTest> testAssignmentRequestTests) {
        for (TestAssignmentRequestTest testAssignmentRequestTest : testAssignmentRequestTests) {
            testAssignmentBuilderMap.put(testAssignmentRequestTest.getTestName(), convertTestAssignmentRequest(testAssignmentRequestTest));
        }
    }

    private TestAssignment convertTestAssignmentRequest(TestAssignmentRequestTest testAssignmentRequestTest) {
        return new TestAssignmentBuilder(random)
                .testName(testAssignmentRequestTest.getTestName())
                .assignedTestLabel(testAssignmentRequestTest.getAssignedTestLabel())
                .winnerPartition(testAssignmentRequestTest.getWinnerPartition())
                .enabled(testAssignmentRequestTest.isEnabled()).build();
    }
}
