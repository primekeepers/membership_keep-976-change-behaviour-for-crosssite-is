package com.odigeo.membership.member.creation;

import com.odigeo.membership.Membership;
import com.odigeo.membership.enums.TimeUnit;

import java.time.LocalDateTime;

public enum MembershipDateCalculator {

    INSTANCE;

    public LocalDateTime getExpirationDate(Membership membership) {
        if (TimeUnit.DAYS.equals(membership.getDurationTimeUnit())) {
            return LocalDateTime.now().plusDays(membership.getDuration());
        } else {
            return LocalDateTime.now().plusMonths(membership.getMonthsDuration());
        }
    }
}
