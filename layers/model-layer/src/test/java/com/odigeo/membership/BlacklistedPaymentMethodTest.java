package com.odigeo.membership;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

public class BlacklistedPaymentMethodTest extends BeanTest<BlacklistedPaymentMethod> {

    private static final String ERROR_TYPE = "error_type";
    private static final String ERROR_MSG = "error_msg";
    private static final Date TIMESTAMP = new Date();
    private static final Long ID_PAYMENT_METHOD = 123L;
    private static final Long MEMBERSHIP_ID = 12345L;

    @BeforeMethod
    public void setUp() {
    }

    @Override
    protected BlacklistedPaymentMethod getBean() {
        return createBlackListedPaymentMethod();
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(BlacklistedPaymentMethod.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    private BlacklistedPaymentMethod createBlackListedPaymentMethod() {
        BlacklistedPaymentMethod blackListedPaymentMethod = new BlacklistedPaymentMethod();
        blackListedPaymentMethod.setErrorType(ERROR_TYPE);
        blackListedPaymentMethod.setErrorMessage(ERROR_MSG);
        blackListedPaymentMethod.setTimestamp(TIMESTAMP);
        blackListedPaymentMethod.setId(ID_PAYMENT_METHOD);
        blackListedPaymentMethod.setMembershipId(MEMBERSHIP_ID);
        return blackListedPaymentMethod;
    }
}