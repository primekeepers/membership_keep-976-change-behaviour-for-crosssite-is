package com.odigeo.membership.parameters;



import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class UserCreation {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private String email;

    private String locale;

    private Integer trafficInterfaceId;

    private String website;

    UserCreation() {
    }

    UserCreation(Builder builder) {
        this.email = builder.getEmail();
        this.locale = builder.getLocale();
        this.trafficInterfaceId = builder.getTrafficInterfaceId();
        this.website = builder.getWebsite();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Integer getTrafficInterfaceId() {
        return trafficInterfaceId;
    }

    public void setTrafficInterfaceId(Integer trafficInterfaceId) {
        this.trafficInterfaceId = trafficInterfaceId;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public static UserCreation fromString(String json) throws IOException {
        return OBJECT_MAPPER.readValue(json, UserCreation.class);
    }

    public static class Builder {

        private String email;
        private String locale;
        private Integer trafficInterfaceId;
        private String website;

        public String getEmail() {
            return email;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public String getLocale() {
            return locale;
        }

        public Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public Integer getTrafficInterfaceId() {
            return trafficInterfaceId;
        }

        public Builder withTrafficInterfaceId(Integer trafficInterfaceId) {
            this.trafficInterfaceId = trafficInterfaceId;
            return this;
        }

        public String getWebsite() {
            return website;
        }

        public Builder withWebsite(String website) {
            this.website = website;
            return this;
        }

        public UserCreation build() {
            return new UserCreation(this);
        }
    }
}
