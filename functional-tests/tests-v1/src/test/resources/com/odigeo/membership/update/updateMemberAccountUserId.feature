Feature: Update userId with updateMemberAccount service

  Scenario Outline: The user wants to update the userId but has no active or PTC memberships
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website | status      | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES      | DEACTIVATED | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId      | status | email              | brand | token | tokenType        |
      | 4321        | ACTIVE | <currentUserEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId> | ACTIVE | <newUserEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | email          | operation   | website |
      | 123             | <newUserEmail> | <operation> | ED      |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 0
    Examples:
      | newUserId | operation      | newUserEmail        | currentUserEmail  |
      | 257       | UPDATE_USER_ID | registered.newuser1@odigeo.com | joseg1@odigeo.com |

  Scenario Outline: The user wants to update the userId and has either an active or PTC membership
    Given the next memberAccount stored in db:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES          | <membershipStatus> | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email               | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUsersEmail> | ED    | null  | REQUEST_PASSWORD |
      | 257             | ACTIVE | <newUsersEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | email           | operation      | website |
      | 123             | <newUsersEmail> | UPDATE_USER_ID | ED      |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 257    | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 2
    And membershipSubscriptionMessage is correctly sent to kafka queue as <newUsersSubscriptionStatus> and email will be sent to <newUsersEmail>
    And membershipSubscriptionMessage is correctly sent to kafka queue as UNSUBSCRIBED and email will be sent to <currentUsersEmail>
    Examples:
      | membershipStatus   | currentUserId | currentUsersEmail | newUsersEmail                  | newUsersSubscriptionStatus |
      | ACTIVATED          | 4321          | joseg1@odigeo.com | registered.newuser1@odigeo.com | SUBSCRIBED                 |
      | PENDING_TO_COLLECT | 8841          | joseg2@odigeo.com | registered.newuser2@odigeo.com | PENDING                    |

  Scenario Outline: Old userId has 2 member accounts with active memberships in the same website
    Given the next memberAccount stored in db:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
      | 643             | <currentUserId> | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES          | ACTIVATED          | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3685     | ES          | ACTIVATED          | ENABLED     | 643             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email               | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUsersEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId>     | ACTIVE | <newUsersEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | email           | operation      | website |
      | 123             | <newUsersEmail> | UPDATE_USER_ID | ED      |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId      | firstName | lastNames |
      | 123             | <newUserId> | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 1
    And membershipSubscriptionMessage is correctly sent to kafka queue as SUBSCRIBED and email will be sent to <newUsersEmail>
    Examples:
      | currentUserId | newUserId | currentUsersEmail | newUsersEmail                  |
      | 4466          | 257       | joseg3@odigeo.com | registered.newuser3@odigeo.com |

  Scenario Outline: Old userId has 1 member account with active membership in the same website than the new userId does.
    Given the next memberAccount stored in db:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
      | 643             | <newUserId>     | CARLOS    | GARCIA    |
    And the next membership stored in db:
      | memberId | website        | status    | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | <oldSite>      | ACTIVATED | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3685     | <newSite>      | ACTIVATED | ENABLED     | 643             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email              | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUserEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId>     | ACTIVE | <newUserEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | email          | operation      | website |
      | 123             | <newUserEmail> | UPDATE_USER_ID | ED      |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
    And no message to trigger a WelcomeToPrime transactional email is sent
    Examples:
      | currentUserId | newUserId | currentUserEmail  | newUserEmail                   | oldSite | newSite |
      | 4466          | 257       | joseg3@odigeo.com | registered.newuser3@odigeo.com | ES      | ES      |
      | 4321          | 257       | joseg3@odigeo.com | registered.newuser3@odigeo.com | DE      | DE      |

  Scenario Outline: Update member account to a new user that has a membership with status other than Active or different website from the one to be assigned
    Given the next memberAccount stored in db:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
      | 643             | <newUserId>     | CARLOS    | GARCIA    |
    And the next membership stored in db:
      | memberId | website   | status             | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | <oldSite> | ACTIVATED          | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3685     | <newSite> | <membershipStatus> | ENABLED     | 643             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email              | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUserEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId>     | ACTIVE | <newUserEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | email          | operation      | website |
      | 123             | <newUserEmail> | UPDATE_USER_ID | ED      |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId      | firstName | lastNames |
      | 123             | <newUserId> | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 2
    And membershipSubscriptionMessage is correctly sent to kafka queue as SUBSCRIBED and email will be sent to <newUserEmail>
    Examples:
      | currentUserId | newUserId | currentUserEmail  | newUserEmail                   | membershipStatus   | oldSite | newSite |
      | 4466          | 257       | joseg3@odigeo.com | registered.newuser3@odigeo.com | DEACTIVATED        | ES      | ES      |
      | 4466          | 257       | joseg3@odigeo.com | registered.newuser3@odigeo.com | PENDING_TO_COLLECT | ES      | ES      |
      | 4466          | 257       | joseg3@odigeo.com | registered.newuser3@odigeo.com | ACTIVATED          | DE      | ES      |

  Scenario Outline: Cannot update member account if the new user does not exists or was deleted
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName   | lastNames |
      | 123             | 4321   | <firstName> | GARCIA    |
    And the next membership stored in db:
      | memberId | website | status      | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES      | DEACTIVATED | ENABLED     | 123             | 2020-07-06     | 2021-07-06     |
    And the user-api has the following user info
      | userId | status | email              | brand | token | tokenType        |
      | 4321   | ACTIVE | <currentUserEmail> | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | email          | operation      | website |
      | 123             | <newUserEmail> | UPDATE_USER_ID | ED      |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId | firstName   | lastNames |
      | 123             | 4321   | <firstName> | GARCIA    |
    And the number of membership subscription messages sent is 0
    Examples:
      | firstName | currentUserEmail  | newUserEmail                  |
      | JOSE      | joseg1@odigeo.com | newuser1@odigeo.com           |
      | ANA       | ana@odigeo.com    | deleted.ana.garcia@odigeo.com |