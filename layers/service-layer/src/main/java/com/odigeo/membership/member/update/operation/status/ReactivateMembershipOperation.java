package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;

public class ReactivateMembershipOperation extends MembershipStatusChangeOperation {

    private final MemberService memberService;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        ReactivateMembershipOperation createOperation(DataSource dataSource);
    }

    @Inject
    public ReactivateMembershipOperation(@Assisted DataSource dataSource,
                                         MemberService memberService,
                                         MembershipMessageSendingManager membershipMessageSendingManager,
                                         MembershipStore membershipStore,
                                         MemberStatusActionStore memberStatusActionStore) {
        super(dataSource, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
        this.memberService = memberService;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        return MemberStatus.ACTIVATED.name().equals(reactivateMembership(updateMembership));
    }

    public String reactivateMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        if (membership.getStatus() == MemberStatus.DEACTIVATED) {
            return changeStatusMembership(membership, MemberStatus.ACTIVATED, StatusAction.ACTIVATION);
        }
        return membership.getStatus().toString();
    }
}
