package com.odigeo.membership.member.rest.utils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.Set;

public final class ValidatorRequestAdapter {

    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    public static void applyValidation(Object... request) {
        Set<ConstraintViolation<?>> constraintViolations = new HashSet<>(VALIDATOR.validate(request));
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }
}
