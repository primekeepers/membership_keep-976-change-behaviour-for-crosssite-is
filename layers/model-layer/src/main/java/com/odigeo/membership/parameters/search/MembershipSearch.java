package com.odigeo.membership.parameters.search;

import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;

public class MembershipSearch extends SearchQueries {

    private final String website;
    private final String status;
    private final String autoRenewal;
    private final String fromExpirationDate;
    private final String toExpirationDate;
    private final String fromActivationDate;
    private final String toActivationDate;
    private final String fromCreationDate;
    private final String toCreationDate;
    private final String membershipType;
    private final BigDecimal minBalance;
    private final BigDecimal maxBalance;
    private final Integer monthsDuration;
    private final String durationTimeUnit;
    private final Integer duration;
    private final String productStatus;
    private final BigDecimal totalPrice;
    private final BigDecimal renewalPrice;
    private final Integer renewalDuration;
    private final String currencyCode;
    private final String sourceType;
    private final String email;
    private final Long memberAccountId;
    private final boolean withStatusActions;
    private final boolean withMemberAccount;
    private final MemberAccountSearch memberAccountSearch;
    private final SortingDTO sorting;
    private final PaginationDTO pagination;
    private final String query;
    private final List<FilterCondition> filterConditions;

    protected MembershipSearch(MembershipSearchBuilder builder) {
        website = builder.website;
        status = builder.status;
        autoRenewal = builder.autoRenewal;
        fromExpirationDate = builder.fromExpirationDate;
        toExpirationDate = builder.toExpirationDate;
        fromActivationDate = builder.fromActivationDate;
        toActivationDate = builder.toActivationDate;
        fromCreationDate = builder.fromCreationDate;
        toCreationDate = builder.toCreationDate;
        membershipType = builder.membershipType;
        minBalance = builder.minBalance;
        maxBalance = builder.maxBalance;
        productStatus = builder.productStatus;
        totalPrice = builder.totalPrice;
        renewalPrice = builder.renewalPrice;
        sourceType = builder.sourceType;
        monthsDuration = builder.monthsDuration;
        durationTimeUnit = builder.durationTimeUnit;
        duration = builder.duration;
        currencyCode = builder.currencyCode;
        memberAccountId = builder.memberAccountId;
        withStatusActions = builder.withStatusActions;
        withMemberAccount = builder.withMemberAccount;
        memberAccountSearch = builder.memberAccountSearch;
        renewalDuration = builder.renewalDuration;
        email = builder.email;
        filterConditions = builder.filterConditions;
        sorting = builder.sorting;
        pagination = builder.pagination;
        query = queryCompiler(filterConditions);
    }

    public boolean isWithStatusActions() {
        return withStatusActions;
    }

    public boolean isWithMemberAccount() {
        return withMemberAccount;
    }

    public static MembershipSearchBuilder builder() {
        return new MembershipSearchBuilder();
    }

    public static MembershipSearchBuilder builder(MembershipSearch copy) {
        return builder()
                .website(copy.website)
                .status(copy.status)
                .autoRenewal(copy.autoRenewal)
                .fromExpirationDate(copy.fromExpirationDate)
                .toExpirationDate(copy.toExpirationDate)
                .fromActivationDate(copy.fromActivationDate)
                .toActivationDate(copy.toActivationDate)
                .fromCreationDate(copy.fromCreationDate)
                .toCreationDate(copy.toCreationDate)
                .membershipType(copy.membershipType)
                .minBalance(copy.minBalance)
                .maxBalance(copy.maxBalance)
                .monthsDuration(copy.monthsDuration)
                .durationTimeUnit(copy.durationTimeUnit)
                .duration(copy.duration)
                .productStatus(copy.productStatus)
                .totalPrice(copy.totalPrice)
                .renewalPrice(copy.renewalPrice)
                .renewalDuration(copy.renewalDuration)
                .currencyCode(copy.currencyCode)
                .sourceType(copy.sourceType)
                .email(copy.email)
                .memberAccountId(copy.memberAccountId)
                .withStatusActions(copy.withStatusActions)
                .withMemberAccount(copy.withMemberAccount)
                .memberAccountSearch(copy.memberAccountSearch)
                .sorting(copy.sorting)
                .pagination(copy.pagination);
    }

    public String getQueryString() {
        return query;
    }

    private String queryCompiler(List<FilterCondition> filterConditions) {
        String queryString;
        if (withMemberAccount) {
            queryString = withStatusActions ? SEARCH_MEMBERSHIPS_WITH_STATUS_ACTIONS_AND_ACCOUNT : SEARCH_MEMBERSHIPS_WITH_ACCOUNT;
        } else {
            queryString = withStatusActions ? SEARCH_MEMBERSHIPS_WITH_STATUS_ACTIONS : SEARCH_MEMBERSHIPS;
        }
        StringBuilder queryBuilder = new StringBuilder(queryString);
        queryBuilder.append(SqlKeywords.WHERE)
                .append(filterConditions.stream()
                        .map(FilterCondition::toString)
                        .collect(joining(SqlKeywords.AND.toString())));
        appendAccountSearchConditions(filterConditions, queryBuilder);
        Optional.ofNullable(this.sorting).ifPresent(queryBuilder::append);
        Optional.ofNullable(this.pagination).ifPresent(queryBuilder::append);
        return queryBuilder.toString();
    }

    private void appendAccountSearchConditions(List<FilterCondition> filterConditions, StringBuilder queryBuilder) {
        if (withMemberAccount && nonNull(memberAccountSearch)) {
            if (CollectionUtils.isNotEmpty(filterConditions)) {
                queryBuilder.append(SqlKeywords.AND);
            }
            queryBuilder.append(memberAccountSearch.getFilterConditions().stream()
                    .map(FilterCondition::toString)
                    .collect(joining(SqlKeywords.AND.toString())));
        }
    }

    public List<Object> getValues() {
        List<Object> objects = filterConditions.stream()
                .map(FilterCondition::getValue)
                .collect(Collectors.toList());
        if (withMemberAccount && nonNull(memberAccountSearch)) {
            objects.addAll(memberAccountSearch.getValues());
        }
        return objects;
    }

    @Override
    public boolean isEmpty() {
        Object[] fieldArray = {website, status, autoRenewal, fromExpirationDate, toExpirationDate, fromActivationDate, toActivationDate, fromCreationDate, toCreationDate, membershipType, minBalance, maxBalance, monthsDuration, duration, productStatus, totalPrice, renewalPrice, renewalDuration, currencyCode, sourceType, memberAccountId};
        boolean thisIsEmpty = Arrays.stream(fieldArray).noneMatch(Objects::nonNull);
        if (thisIsEmpty && nonNull(memberAccountSearch)) {
            return memberAccountSearch.isEmpty();
        }
        return thisIsEmpty;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public MemberAccountSearch getMemberAccountSearch() {
        return memberAccountSearch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipSearch that = (MembershipSearch) o;
        return withStatusActions == that.withStatusActions && withMemberAccount == that.withMemberAccount
                && Objects.equals(website, that.website) && Objects.equals(status, that.status)
                && Objects.equals(autoRenewal, that.autoRenewal) && Objects.equals(fromExpirationDate, that.fromExpirationDate)
                && Objects.equals(toExpirationDate, that.toExpirationDate) && Objects.equals(fromActivationDate, that.fromActivationDate)
                && Objects.equals(toActivationDate, that.toActivationDate) && Objects.equals(fromCreationDate, that.fromCreationDate)
                && Objects.equals(toCreationDate, that.toCreationDate) && Objects.equals(membershipType, that.membershipType)
                && Objects.equals(minBalance, that.minBalance) && Objects.equals(maxBalance, that.maxBalance) && Objects.equals(monthsDuration, that.monthsDuration)
                && Objects.equals(durationTimeUnit, that.durationTimeUnit) && Objects.equals(duration, that.duration) && Objects.equals(productStatus, that.productStatus)
                && Objects.equals(totalPrice, that.totalPrice) && Objects.equals(renewalPrice, that.renewalPrice) && Objects.equals(renewalDuration, that.renewalDuration)
                && Objects.equals(currencyCode, that.currencyCode) && Objects.equals(sourceType, that.sourceType) && Objects.equals(email, that.email)
                && Objects.equals(memberAccountId, that.memberAccountId) && Objects.equals(memberAccountSearch, that.memberAccountSearch)
                && Objects.equals(sorting, that.sorting) && Objects.equals(query, that.query) && Objects.equals(filterConditions, that.filterConditions)
                && Objects.equals(pagination, that.pagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(website, status, autoRenewal, fromExpirationDate, toExpirationDate, fromActivationDate, toActivationDate, fromCreationDate, toCreationDate, membershipType, minBalance, maxBalance, monthsDuration, durationTimeUnit, duration, productStatus, totalPrice, renewalPrice, renewalDuration, currencyCode, sourceType, email, memberAccountId, withStatusActions, withMemberAccount, memberAccountSearch, sorting, query, filterConditions, pagination);
    }
}
