package com.odigeo.membership.member.bootstrap;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;

public class TestAssignmentServiceModule extends AbstractRestUtilsModule {

    public TestAssignmentServiceModule(ServiceNotificator... serviceNotificators) {
        super(TestAssignmentService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpDefault(TestAssignmentService.class);
    }
}
