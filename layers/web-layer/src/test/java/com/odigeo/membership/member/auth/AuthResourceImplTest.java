package com.odigeo.membership.member.auth;

import com.google.inject.AbstractModule;
import com.odigeo.membership.auth.AuthorizationRequest;
import com.odigeo.membership.auth.LoginRequest;
import com.odigeo.membership.auth.LogoutRequest;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;
import com.odigeo.membership.auth.usecases.AuthenticationLoginUseCase;
import com.odigeo.membership.auth.usecases.AuthenticationLogoutUseCase;
import com.odigeo.membership.auth.usecases.AuthorizationPermissionsUseCase;
import com.odigeo.membership.member.auth.exception.MembershipAuthenticationException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import static com.edreams.configuration.ConfigurationEngine.init;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AuthResourceImplTest {
    private static final String USERNAME = "username";
    private static final String ERROR = "ERROR";

    private AuthenticationLoginUseCase authenticationLoginUseCase;
    private AuthenticationLogoutUseCase authenticationLogoutUseCase;
    private AuthorizationPermissionsUseCase authorizationPermissionsUseCase;
    private Request request;

    private AuthResource authResource;

    @BeforeMethod
    public void setUp() {

        authenticationLoginUseCase = mock(AuthenticationLoginUseCase.class);
        authenticationLogoutUseCase = mock(AuthenticationLogoutUseCase.class);
        authorizationPermissionsUseCase = mock(AuthorizationPermissionsUseCase.class);

        request = mock(Request.class);

        init(new AbstractModule() {

            @Override
            protected void configure() {

                bind(AuthenticationLoginUseCase.class).toInstance(authenticationLoginUseCase);
                bind(AuthenticationLogoutUseCase.class).toInstance(authenticationLogoutUseCase);
                bind(AuthorizationPermissionsUseCase.class).toInstance(authorizationPermissionsUseCase);
            }
        });

        authResource = new AuthResourceImpl();
    }

    @Test
    public void testLogin() {
        final Response response = authResource.login(request, USERNAME, createLoginRequest());

        checkRightResponse(response);
    }

    @Test(expectedExceptions = MembershipAuthenticationException.class)
    public void testLoginError() throws MembershipAuthServiceException {
        doThrow(new MembershipAuthServiceException(ERROR)).when(authenticationLoginUseCase).login(anyString(), anyString());

        final Response response = authResource.login(request, USERNAME, createLoginRequest());

        checkErrorResponse(response);
    }

    @Test
    public void testLogout() {
        final Response response = authResource.logout(request, createLogoutRequest());

        checkRightResponse(response);
    }

    @Test(expectedExceptions = MembershipAuthenticationException.class)
    public void testLogoutError() throws MembershipAuthServiceException {
        doThrow(new MembershipAuthServiceException(ERROR)).when(authenticationLogoutUseCase).logout(anyString());

        final Response response = authResource.logout(request, createLogoutRequest());

        checkErrorResponse(response);
    }


    @Test
    public void testGetTokenPermissions() {
        final Response response = authResource.getTokenPermissions(request, createAuthorizationRequest());

        checkRightResponse(response);
    }

    @Test(expectedExceptions = MembershipAuthenticationException.class)
    public void testGetTokenPermissionsError() throws MembershipAuthServiceException {
        doThrow(new MembershipAuthServiceException(ERROR)).when(authorizationPermissionsUseCase).getTokenPermissions(anyString());

        final Response response = authResource.getTokenPermissions(request, createAuthorizationRequest());

        checkErrorResponse(response);
    }

    private void checkRightResponse(final Response response) {
        assertNotNull(response);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
    }

    private void checkErrorResponse(final Response response) {
        assertNotNull(response);
        assertEquals(response.getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    private LoginRequest createLoginRequest() {
        final LoginRequest loginRequest = mock(LoginRequest.class);
        when(loginRequest.getPassword()).thenReturn("password");
        return loginRequest;
    }

    private LogoutRequest createLogoutRequest() {
        final LogoutRequest logoutRequest = mock(LogoutRequest.class);
        when(logoutRequest.getToken()).thenReturn("token");
        return logoutRequest;
    }

    private AuthorizationRequest createAuthorizationRequest() {
        final AuthorizationRequest authorizationRequest = mock(AuthorizationRequest.class);
        when(authorizationRequest.getToken()).thenReturn("token");
        return authorizationRequest;
    }
}