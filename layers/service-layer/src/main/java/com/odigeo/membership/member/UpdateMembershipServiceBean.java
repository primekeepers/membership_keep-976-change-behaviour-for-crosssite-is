package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.renewal.MembershipRenewalService;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.membership.member.update.operation.autorenewal.DisableAutoRenewalOperation;
import com.odigeo.membership.member.update.operation.autorenewal.EnableAutoRenewalOperation;
import com.odigeo.membership.member.update.operation.status.DeactivateMembershipOperation;
import com.odigeo.membership.member.update.operation.status.DiscardMembershipOperation;
import com.odigeo.membership.member.update.operation.status.ExpireMembershipOperation;
import com.odigeo.membership.member.update.operation.status.ReactivateMembershipOperation;
import com.odigeo.membership.tracking.autorenewal.MembershipAutorenewalTracker;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.security.InvalidParameterException;
import java.util.Map;
import java.util.Optional;

import static com.odigeo.membership.UpdateMembershipAction.DEACTIVATE_MEMBERSHIP;
import static com.odigeo.membership.UpdateMembershipAction.DISCARD_MEMBERSHIP;
import static com.odigeo.membership.UpdateMembershipAction.EXPIRE_MEMBERSHIP;
import static com.odigeo.membership.UpdateMembershipAction.REACTIVATE_MEMBERSHIP;

@Stateless
@Local(UpdateMembershipService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UpdateMembershipServiceBean extends AbstractServiceBean implements UpdateMembershipService {
    private MembershipRenewalService membershipRenewalService;
    private Map<UpdateMembershipAction, MembershipUpdateOperationFactory> updateOperations;

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public Boolean updateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, ExistingRecurringException {
        return Optional.ofNullable(getOperations()
                .get(UpdateMembershipAction.valueOf(updateMembership.getOperation())))
                .orElseThrow(() -> new InvalidParameterException("Unknown operation parameter: " + updateMembership.getOperation()))
                .createOperation(dataSource)
                .update(updateMembership);
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String deactivateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, ExistingRecurringException {
        return getOperation(DEACTIVATE_MEMBERSHIP, DeactivateMembershipOperation.class).deactivateMembership(updateMembership);
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String reactivateMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        return getOperation(REACTIVATE_MEMBERSHIP, ReactivateMembershipOperation.class).reactivateMembership(updateMembership);
    }

    @Override
    public String expireMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        return getOperation(EXPIRE_MEMBERSHIP, ExpireMembershipOperation.class).expireMembership(updateMembership);
    }

    @Override
    public String discardMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        return getOperation(DISCARD_MEMBERSHIP, DiscardMembershipOperation.class).discardMembership(updateMembership);
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String enableAutoRenewal(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        return getOperation(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, EnableAutoRenewalOperation.class).enableAutoRenewal(updateMembership);
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String disableAutoRenewal(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        return getOperation(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, DisableAutoRenewalOperation.class).disableAutoRenewal(updateMembership);
    }

    @Override
    public boolean activateRenewalPendingToCollect(Membership membership) throws DataAccessException {
        return getMembershipRenewalService().activatePendingToCollect(dataSource, membership);
    }

    private <T> T getOperation(UpdateMembershipAction action, Class<T> operationClass) {
        return operationClass.cast(getOperations().get(action).createOperation(dataSource));
    }

    private MembershipRenewalService getMembershipRenewalService() {
        return Optional.ofNullable(membershipRenewalService).orElseGet(() -> {
            membershipRenewalService = ConfigurationEngine.getInstance(MembershipRenewalService.class);
            return membershipRenewalService;
        });
    }

    private Map<UpdateMembershipAction, MembershipUpdateOperationFactory> getOperations() {
        return Optional.ofNullable(updateOperations).orElseGet(() -> {
            updateOperations = ConfigurationEngine.getInstance(Key.get(new ActionOperationTypeLiteral()));
            return updateOperations;
        });
    }

    @VisibleForTesting
    protected void setOperations(Map<UpdateMembershipAction, MembershipUpdateOperationFactory> operations) {
        this.updateOperations = operations;
    }

    @VisibleForTesting
    protected void setMembershipRenewalService(MembershipRenewalService membershipRenewalService) {
        this.membershipRenewalService = membershipRenewalService;
    }

    private static class ActionOperationTypeLiteral extends TypeLiteral<Map<UpdateMembershipAction, MembershipUpdateOperationFactory>> {
    }
}
