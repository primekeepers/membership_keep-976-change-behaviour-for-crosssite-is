package com.odigeo.membership.fees;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.fees.exception.FeesServiceException;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeContainer;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.fees.rest.FeesService;
import com.odigeo.membership.product.MembershipProductType;
import com.odigeo.util.FeeMapper;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MembershipFeesServiceBeanTest {

    private static final String CURRENCY_CODE = "USD";
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final Long FEE_CONTAINER_ID = 222L;

    @Mock
    FeesService feesService;
    @Mock
    FeeMapper feeMapper;

    MembershipFeesServiceBean membershipFeesServiceBean;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        membershipFeesServiceBean = new MembershipFeesServiceBean();
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(FeesService.class).toInstance(feesService);
        binder.bind(FeeMapper.class).toInstance(feeMapper);
    }

    @Test
    public void testRequestFeeContainerCreation() throws FeesServiceException {
        when(feesService.createContainer(any())).thenReturn(mockFeeContainer());
        Optional<Long> feeContainerId = membershipFeesServiceBean.requestFeeContainerCreation(SUBSCRIPTION_PRICE, CURRENCY_CODE);
        assertTrue(feeContainerId.isPresent());
        assertEquals(feeContainerId.get(), FEE_CONTAINER_ID);
    }

    @Test
    public void testRetrieveFees() throws FeesServiceException {
        when(feesService.findFeeContainer(anyLong())).thenReturn(mockFeeContainer());
        List<AbstractFee> fees = membershipFeesServiceBean.retrieveFees(FEE_CONTAINER_ID);
        FixFee fee = (FixFee) fees.get(0);
        assertEquals(fees.size(), 1);
        assertEquals(fee.getAmount(), SUBSCRIPTION_PRICE);
        assertEquals(fee.getCurrency(), Currency.getInstance(CURRENCY_CODE));
    }

    private FeeContainer mockFeeContainer() {
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setId(FEE_CONTAINER_ID);
        feeContainer.setFeeMap(new FeeMapper().membershipFeeToFeeLabelMap(SUBSCRIPTION_PRICE, CURRENCY_CODE));
        return feeContainer;
    }

    @Test
    public void testFillProductFee() {
        FixFee fee = membershipFeesServiceBean.fillProductFee(BigDecimal.TEN, CURRENCY_CODE);
        assertEquals(fee.getAmount(), BigDecimal.TEN);
        assertEquals(fee.getCurrency(), Currency.getInstance(CURRENCY_CODE));
        assertEquals(fee.getFeeLabel(), FeeLabel.MARKUP_TAX);
        assertEquals(fee.getSubCode(), MembershipProductType.MEMBERSHIP_RENEWAL.getFeeSubCode());
        assertNotNull(fee.getCreationDate());
    }
}
