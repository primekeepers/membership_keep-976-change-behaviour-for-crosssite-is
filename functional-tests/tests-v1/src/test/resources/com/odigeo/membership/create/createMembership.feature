Feature: Test createMembership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 1234            | 4321   | JOSE      | GARCIA    |
    And the user-api has the following user info
      | userId | email                 | brand | status | token |
      | 4321   | basicFree@edreams.com | ED    | ACTIVE | null  |
    And property to send IDs to Kafka has value false in db:

  Scenario: create membership pendingToCollect
    Given the next membership stored in db:
      | memberId | status  | balance | activationDate | expirationDate | memberAccountId | autoRenewal |
      | 44698    | EXPIRED | 44.99   | 2018-05-04     | 2019-05-04     | 1234            | ENABLED     |
    And the next createMembershipPendingToCollectRequest:
      | website | monthsToRenewal | sourceType     | membershipType | memberAccountId | expirationDate          | currencyCode | recurringId | subscriptionPrice |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 1234            | 2023-02-04T00:00:00.000 | EUR          | xyz         | 49.99             |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_COLLECT and a valid fee container ID
    And all information for a pending to collect is filled in the membership of BASIC type
    And the recurringId xyz has been inserted for the new membership
    And the membership fee has been inserted with right information

  Scenario: create membership pendingToCollect PLUS
    Given the next membership stored in db:
      | memberId | status  | balance | activationDate | expirationDate | memberAccountId | autoRenewal |
      | 44698    | EXPIRED | 44.99   | 2018-05-04     | 2019-05-04     | 1234            | ENABLED     |
    And the next createMembershipPendingToCollectRequest:
      | website | monthsToRenewal | sourceType     | membershipType | memberAccountId | expirationDate          | currencyCode | recurringId | subscriptionPrice |
      | ES      | 12              | FUNNEL_BOOKING | PLUS           | 1234            | 2023-02-04T00:00:00.000 | EUR          | xyz         | 49.99             |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_COLLECT and a valid fee container ID
    And all information for a pending to collect is filled in the membership of PLUS type
    And the recurringId xyz has been inserted for the new membership
    And the membership fee has been inserted with right information

  Scenario: create membership pendingToCollect returns the membershipId that already exists for the same website
    Given the next membership stored in db:
      | memberId | website | status             | balance | activationDate | expirationDate | memberAccountId | autoRenewal |
      | 44698    | ES      | EXPIRED            | 44.99   | 2018-05-04     | 2019-05-04     | 1234            | ENABLED     |
      | 48579    | ES      | PENDING_TO_COLLECT | 44.99   | 2019-05-04     | 2020-05-04     | 1234            | ENABLED     |
    And the next createMembershipPendingToCollectRequest:
      | website | monthsToRenewal | sourceType     | membershipType | memberAccountId | expirationDate          | currencyCode | recurringId | subscriptionPrice |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 1234            | 2023-02-04T00:00:00.000 | EUR          | xyz         | 49.99             |
    When the client request createMembership to membership API
    Then no new membership in pending to collect was created for memberAccount 1234 and website ES
    And the membershipId returned is 48579

  Scenario: CreateNewMembershipRequest with existing userId creates a pending to activate membership, legacy
    Given the following createNewMembershipRequest:
      | website | monthsToRenewal | sourceType     | membershipType | userId | name  | lastNames |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 4321   | Emiri | Kaya      |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_ACTIVATE
    And the membership belongs to a user with id 4321

  Scenario Outline: CreateNewMembershipRequest with existing userId creates a pending to activate membership
    Given the following createNewMembershipRequest:
      | website   | monthsToRenewal   | duration          | durationTimeUnit          | sourceType   | membershipType   | userId   | name   | lastNames   |
      | <website> | <monthsToRenewal> | <durationRequest> | <durationTimeUnitRequest> | <sourceType> | <membershipType> | <userId> | <name> | <lastNames> |
    When the client request createMembership to membership API
    Then a membership is created with the values:
      | status              | monthsDuration   | duration           | durationTimeUnit           | sourceType   | membershipType   |
      | PENDING_TO_ACTIVATE | <monthsDuration> | <durationResponse> | <durationTimeUnitResponse> | <sourceType> | <membershipType> |
    And the membership belongs to a user with id <userId>
    Examples:
      | userId | website | monthsToRenewal | durationRequest | durationTimeUnitRequest | sourceType     | membershipType | userId   | name  | lastNames | monthsDuration | durationResponse | durationTimeUnitResponse |
      | 4321   | ES      | 0               | 7               | DAYS                    | FUNNEL_BOOKING | BASIC          | <userId> | Emiri | Kaya      | 0              | 7                | DAYS                     |
      | 4321   | ES      | 0               | 6               | MONTHS                  | FUNNEL_BOOKING | BASIC          | <userId> | Emiri | Kaya      | 6              | 6                | MONTHS                   |
      | 4321   | ES      | 12              |                 | NULL                    | FUNNEL_BOOKING | BASIC          | <userId> | Emiri | Kaya      | 12             | 12               | MONTHS                   |
      | 4321   | ES      | 12              |                 | MONTHS                  | FUNNEL_BOOKING | BASIC          | <userId> | Emiri | Kaya      | 12             | 12               | MONTHS                   |
      | 4321   | ES      | 1               | 1               | MONTHS                  | FUNNEL_BOOKING | PLUS           | <userId> | Emiri | Kaya      | 1              | 1                | MONTHS                   |

  Scenario: CreateNewMembershipRequest with user creation info
    Given the following createNewMembershipRequest:
      | website | monthsToRenewal | sourceType     | membershipType | userId | name | lastNames | email                 | locale | trafficInterfaceId |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 4321   | JOSE | GARCIA    | basicFree@edreams.com | tr_TR  | 2                  |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 4321   | JOSE | GARCIA    | basicFree@edreams.com | tr_TR  | 1                  |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_ACTIVATE

  Scenario: In CreateNewMembershipRequest userId has priority over user creation info
    Given the following createNewMembershipRequest:
      | website | monthsToRenewal | sourceType     | membershipType | name  | lastNames | email       | locale | trafficInterfaceId | userId |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | Talat | Tata      | any@thy.com | tr_TR  | 2                  | 4321   |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_ACTIVATE
    And the membership belongs to a user with id 4321

  Scenario Outline: Create Basic FREE membership with existing user
    Given the following createBasicFreeMembershipRequest:
      | website | monthsToRenewal | sourceType   | membershipType | currencyCode | name | lastNames | userId   | channel   |
      | ES      | 6               | POST_BOOKING | BASIC_FREE     | EUR          | Juan | Sanchez   | <userId> | <channel> |
    When the client request createMembership to membership API
    Then a membership is created with the status ACTIVATED
    And membership is created with autorenewal disabled
    And the membership belongs to a user with id <userId>

    Examples:
      | userId | channel      |
      | 4321   | LANDING_PAGE |
      | 4321   | ANDROID      |

  Scenario: Create Basic FREE membership with user creation info
    Given the following createBasicFreeMembershipRequest:
      | website | monthsToRenewal | sourceType   | membershipType | currencyCode | name | lastNames | userId | email                 | locale | trafficInterfaceId | channel |
      | ES      | 6               | POST_BOOKING | BASIC_FREE     | EUR          | Paul | Winter    | 4321   | basicFree@edreams.com | es_ES  | 1                  | IOS     |
    When the client request createMembership to membership API
    Then a membership is created with the status ACTIVATED
    And membership is created with autorenewal disabled


    Scenario: Create membership for shopping basket
      Given the following createNewMembershipRequest:
        | isShoppingBasketProduct | website | monthsToRenewal | sourceType     | membershipType | userId | name | lastNames | email                                | locale | trafficInterfaceId |
        | true                    | ES      | 12              | FUNNEL_BOOKING | BASIC          | 4321   | JOSE | GARCIA    | shoppingBasketMembership@edreams.com | es_ES  | 1                  |

      When the client request createMembership to membership API

      Then an id is generated for the new membership
      And the RecurringCollectionResource generated a new RecurringCollectionId
      And no membership has been stored in db for generated membershipId
