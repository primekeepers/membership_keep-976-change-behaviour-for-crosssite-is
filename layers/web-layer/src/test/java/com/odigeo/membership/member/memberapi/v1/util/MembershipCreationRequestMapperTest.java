package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.request.product.creation.CreateNewMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.request.product.creation.UserCreationInfo;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Locale;

import static org.testng.Assert.assertEquals;

public class MembershipCreationRequestMapperTest {

    private static final String USER_ID = "123";
    private static final Long MEMBER_ACCOUNT_ID = 343L;
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now();
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.valueOf(54.99);
    private static final BigDecimal RENEWAL_PRICE = BigDecimal.valueOf(514.99);
    private static final String NAME = "Test";
    private static final String LAST_NAMES = "Last Name";
    private static final String WEBSITE = "ES";
    private static final int MONTHS_TO_RENEWAL = 12;
    private static final String CURRENCY_CODE = "EUR";
    private static final String SOURCE_TYPE = SourceType.FUNNEL_BOOKING.toString();
    private static final String BASIC_MEMBERSHIP_TYPE = MembershipType.BASIC.toString();
    private static final String BASIC_FREE_MEMBERSHIP_TYPE = MembershipType.BASIC_FREE.toString();
    private static final String EMAIL = "TestLastName@mail.com";
    private static final CreateNewMembershipRequest.Builder COMMON_NEW_SUBS_REQUEST_BUILDER = new CreateNewMembershipRequest.Builder()
            .withWebsite(WEBSITE)
            .withMonthsToRenewal(MONTHS_TO_RENEWAL)
            .withSourceType(SOURCE_TYPE)
            .withRenewalPrice(RENEWAL_PRICE)
            .withCurrencyCode(CURRENCY_CODE)
            .withName(NAME)
            .withLastNames(LAST_NAMES);

    @Test
    public void testGetMembershipCreationFromGenericRequestNewSubscription() {
        CreateNewMembershipRequest request = COMMON_NEW_SUBS_REQUEST_BUILDER
                .withMembershipType(BASIC_MEMBERSHIP_TYPE)
                .withUserId(USER_ID)
                .build();
        MembershipCreation membershipCreation = MembershipCreationRequestMapper
                .getMembershipCreationFromRequestFields(request.getAsMap());
        assertEquals(membershipCreation.getMemberStatus(), MemberStatus.PENDING_TO_ACTIVATE);
        checkMandatoryFields(membershipCreation);
        MemberAccountCreation memberAccountCreation = membershipCreation.getMemberAccountCreation();
        assertEquals(memberAccountCreation.getUserId(), Long.valueOf(USER_ID));
        assertEquals(memberAccountCreation.getName(), NAME);
        assertEquals(memberAccountCreation.getLastNames(), LAST_NAMES);
    }

    @Test
    public void testGetMembershipCreationFromGenericRequestPendingToCollect() {
        CreatePendingToCollectRequest request = new CreatePendingToCollectRequest.Builder()
                .withWebsite(WEBSITE)
                .withMonthsToRenewal(MONTHS_TO_RENEWAL)
                .withSourceType(SOURCE_TYPE)
                .withMembershipType(BASIC_MEMBERSHIP_TYPE)
                .withMemberAccountId(MEMBER_ACCOUNT_ID)
                .withExpirationDate(EXPIRATION_DATE.toString())
                .withSubscriptionPrice(SUBSCRIPTION_PRICE)
                .withRenewalPrice(RENEWAL_PRICE)
                .withCurrencyCode(CURRENCY_CODE)
                .build();
        MembershipCreation membershipCreation = MembershipCreationRequestMapper
                .getMembershipCreationFromRequestFields(request.getAsMap());
        assertEquals(membershipCreation.getMemberStatus(), MemberStatus.PENDING_TO_COLLECT);
        checkMandatoryFields(membershipCreation);
        assertEquals(membershipCreation.getMemberAccountId(), MEMBER_ACCOUNT_ID);
        assertEquals(membershipCreation.getExpirationDate(), EXPIRATION_DATE);
        assertEquals(membershipCreation.getSubscriptionPrice(), SUBSCRIPTION_PRICE);
        assertEquals(membershipCreation.getCurrencyCode(), CURRENCY_CODE);
    }

    @Test
    public void testGetMembershipCreationAndUserCreationFromGenericRequest() {
        CreateNewMembershipRequest request = COMMON_NEW_SUBS_REQUEST_BUILDER.withMembershipType(BASIC_MEMBERSHIP_TYPE)
                .withUserCreationInfo(new UserCreationInfo.Builder()
                        .withEmail(EMAIL)
                        .withLocale(Locale.ENGLISH.toString())
                        .withTrafficInterfaceId(1)
                        .build())
                .build();
        MembershipCreation membershipCreation = MembershipCreationRequestMapper
                .getMembershipCreationFromRequestFields(request.getAsMap());
        assertEquals(membershipCreation.getMemberStatus(), MemberStatus.PENDING_TO_ACTIVATE);
        checkMandatoryFields(membershipCreation);
        MemberAccountCreation memberAccountCreation = membershipCreation.getMemberAccountCreation();
        assertEquals(memberAccountCreation.getName(), NAME);
        assertEquals(memberAccountCreation.getLastNames(), LAST_NAMES);
    }

    @Test
    public void testGetMembershipBasicFreeCreationFromGenericRequest() {
        CreateNewMembershipRequest request = COMMON_NEW_SUBS_REQUEST_BUILDER
                .withMembershipType(BASIC_FREE_MEMBERSHIP_TYPE)
                .withUserId(USER_ID)
                .build();
        MembershipCreation membershipCreation = MembershipCreationRequestMapper
                .getMembershipCreationFromRequestFields(request.getAsMap());
        assertEquals(membershipCreation.getMemberStatus(), MemberStatus.ACTIVATED);
        assertEquals(membershipCreation.getWebsite(), WEBSITE);
        assertEquals(membershipCreation.getMonthsDuration(), MONTHS_TO_RENEWAL);
        assertEquals(membershipCreation.getSourceType(), SourceType.valueOf(SOURCE_TYPE));
        assertEquals(membershipCreation.getMembershipType(), MembershipType.valueOf(BASIC_FREE_MEMBERSHIP_TYPE));
        MemberAccountCreation memberAccountCreation = membershipCreation.getMemberAccountCreation();
        assertEquals(memberAccountCreation.getName(), NAME);
        assertEquals(memberAccountCreation.getLastNames(), LAST_NAMES);
    }

    private void checkMandatoryFields(MembershipCreation membershipCreation) {
        assertEquals(membershipCreation.getWebsite(), WEBSITE);
        assertEquals(membershipCreation.getMonthsDuration(), MONTHS_TO_RENEWAL);
        assertEquals(membershipCreation.getSourceType(), SourceType.valueOf(SOURCE_TYPE));
        assertEquals(membershipCreation.getMembershipType(), MembershipType.valueOf(BASIC_MEMBERSHIP_TYPE));
    }
}
