package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.UpdateMemberAccountOperation;
import com.odigeo.membership.exception.AlreadyHasPrimeMembershipException;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.odigeo.membership.UpdateMemberAccountOperation.UPDATE_NAMES;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Stateless
@Local(MemberAccountService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MemberAccountServiceBean extends AbstractServiceBean implements MemberAccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberAccountServiceBean.class);
    private static final Predicate<MemberStatus> IS_ACTIVATED = status -> status.is(MemberStatus.ACTIVATED);
    private static final Predicate<MemberStatus> IS_PENDING_TO_COLLECT = status -> status.is(MemberStatus.PENDING_TO_COLLECT);
    private static final Predicate<Membership> IS_ACTIVE_OR_PTC = membership -> IS_ACTIVATED.or(IS_PENDING_TO_COLLECT).test(membership.getStatus());

    @Override
    public MemberAccount getMembershipAccount(long userId, String website) throws DataAccessException {
        MemberAccount memberAccount = getMemberWithActivatedMemberships(userId, website);
        setBookingLimitReached(memberAccount);
        return memberAccount;
    }

    @Override
    public List<MemberAccount> getActiveMembersByUserId(long userId) throws DataAccessException {
        return getMemberManager().getMembersWithActivatedMembershipsByUserId(userId, dataSource);
    }

    @Override
    public MemberAccount getMemberAccountByMembershipId(long membershipId) throws MissingElementException, DataAccessException {
        Membership membership = getMemberManager().getMembershipById(dataSource, membershipId);
        MemberAccount memberAccount = getMemberManager()
                .getMemberAccountsByMemberAccountId(membership.getMemberAccountId(), false, dataSource);
        memberAccount.setMemberships(Collections.singletonList(membership));
        return memberAccount;
    }

    @Override
    public MemberAccount getMemberWithActivatedMemberships(long userId, String website) throws DataAccessException {
        Optional<MemberAccount> memberAccount = getMemberManager()
                .getMembersWithActivatedMembershipsByUserId(userId, dataSource)
                .stream()
                .filter(MemberServiceUtils.websiteCheck(website))
                .findFirst();
        if (memberAccount.isEmpty()) {
            LOGGER.info("UserId {} doesn't have active prime subscription in {} ", userId, website);
        }
        return memberAccount.orElse(null);
    }

    @Override
    public Boolean updateMemberAccount(UpdateMemberAccount updateMemberAccount) throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccountOperation updateMemberAccountOperation = Optional.ofNullable(updateMemberAccount.getOperation()).
                map(UpdateMemberAccountOperation::valueOf)
                .orElse(UPDATE_NAMES);
        switch (updateMemberAccountOperation) {
        case UPDATE_USER_ID:
            return updateMemberAccountUserId(updateMemberAccount);
        case UPDATE_NAMES:
            return updateMemberAccountNames(updateMemberAccount);
        default:
            throw new InvalidParameterException("Unknown operation parameter: " + updateMemberAccount.getOperation());
        }
    }

    @Override
    public Set<String> getActiveMembershipSitesByUserId(final long userId) throws DataAccessException {
        return getMemberManager().getMembersWithActivatedMembershipsByUserId(userId, dataSource).stream()
                .map(memberAccount -> memberAccount.getMemberships().get(0))
                .map(Membership::getWebsite)
                .map(String::toUpperCase)
                .collect(Collectors.toSet());
    }

    @Override
    public MemberAccount getMemberAccountById(final long accountId, boolean withMemberships) throws MissingElementException, DataAccessException {
        return getMemberManager().getMemberAccountsByMemberAccountId(accountId, withMemberships, dataSource);
    }

    private Boolean updateMemberAccountUserId(final UpdateMemberAccount updateMemberAccount) throws DataAccessException, MissingElementException, UserAccountNotFoundException, AlreadyHasPrimeMembershipException {
        getMemberAccountUserIdUpdater().validateParametersForUpdateUserIdOperation(updateMemberAccount);
        Long newUserId = getNewUserId(updateMemberAccount);
        final MemberAccount memberAccountWithMemberships = getMemberAccountById(
                Long.parseLong(updateMemberAccount.getMemberAccountId()), true);
        final long oldUserId = memberAccountWithMemberships.getUserId();
        final List<Membership> activeAndPtcMemberships = getActiveOrPtcMembership(memberAccountWithMemberships);

        boolean userIdUpdated = false;
        if (isNotEmpty(activeAndPtcMemberships)) {
            hasActiveMembershipForTheSameWebsite(newUserId, activeAndPtcMemberships);
            userIdUpdated = getMemberAccountUserIdUpdater().updateMemberAccountUserId(dataSource, Long.parseLong(updateMemberAccount.getMemberAccountId()), newUserId);
            if (userIdUpdated) {
                getMembershipTransferMarketingHandler().subscribeNewUserAndUnsubscribeOldUser(
                        oldUserId, newUserId,
                        activeAndPtcMemberships, getActiveMembershipSitesByUserId(oldUserId));
            }
        }
        return userIdUpdated;
    }

    private Long getNewUserId(UpdateMemberAccount updateMemberAccount) throws UserAccountNotFoundException, DataAccessException {
        return nonNull(updateMemberAccount.getUserId()) ? updateMemberAccount.getUserId()
                : getUserService().getUserIdBy(updateMemberAccount.getEmail(), updateMemberAccount.getWebsite());
    }

    private void hasActiveMembershipForTheSameWebsite(Long userId, List<Membership> oldUserActiveAndPtcMemberships) throws DataAccessException, AlreadyHasPrimeMembershipException {
        Set<String> newUserMemberAccountWebsites = getActiveMembershipSitesByUserId(userId);
        Optional<String> websiteOp = oldUserActiveAndPtcMemberships.stream()
                .map(Membership::getWebsite)
                .filter(newUserMemberAccountWebsites::contains)
                .findAny();
        if (websiteOp.isPresent()) {
            throw new AlreadyHasPrimeMembershipException("The user already has an active membership for the website " + websiteOp.get());
        }
    }

    private List<Membership> getActiveOrPtcMembership(MemberAccount memberAccountWithMemberships) {
        return memberAccountWithMemberships.getMemberships().stream()
                .filter(IS_ACTIVE_OR_PTC)
                .collect(Collectors.toList());
    }

    private Boolean updateMemberAccountNames(UpdateMemberAccount updateMemberAccount) throws DataAccessException {
        if (nonNull(updateMemberAccount.getMemberAccountId()) && MemberServiceUtils.isValidPersonName(updateMemberAccount.getName())
                && MemberServiceUtils.isValidPersonName(updateMemberAccount.getLastNames())) {
            return getMemberManager().updateMemberAccountNames(dataSource, Long.valueOf(updateMemberAccount.getMemberAccountId()),
                    updateMemberAccount.getName(), updateMemberAccount.getLastNames());
        }
        return Boolean.FALSE;
    }


    private void setBookingLimitReached(MemberAccount memberAccount) throws DataAccessException {
        Optional<Membership> membership = Optional.ofNullable(memberAccount)
                .flatMap(account -> account.getMemberships().stream().findFirst());
        if (membership.isPresent()) {
            Boolean isBookingLimitReached = getBookingTrackingService().isBookingLimitReached(membership.get().getId());
            membership.get().setBookingLimitReached(isBookingLimitReached);
        }
    }

}
