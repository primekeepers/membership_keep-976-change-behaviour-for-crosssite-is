package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.AuditMemberAccount;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MemberManagerTest {

    private static final Long USER_ID = 123L;
    private static final Long MEMBER_ID = 123L;
    private static final Long MEMBER_ACCOUNT_ID = 1L;
    private static final String NAME = "Name";
    private static final String LAST_NAME = "LastName";
    private static final String WEBSITE = "ES";
    private static final String EXCEPTION_TEST = "Exception Test";
    private static final SQLException SQL_EXCEPTION = new SQLException();
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final DataNotFoundException DATA_NOT_FOUND_EXCEPTION = new DataNotFoundException(EXCEPTION_TEST);
    private static final MembershipCreation MEMBERSHIP_CREATION_PENDING = new MembershipCreationBuilder()
            .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                    .userId(USER_ID)
                    .name(NAME)
                    .lastNames(LAST_NAME))
            .withWebsite(WEBSITE).withMembershipType(MEMBERSHIP_TYPE).withSourceType(SOURCE_TYPE).withSubscriptionPrice(BigDecimal.TEN).build();

    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private AuditMemberAccountManager auditMemberAccountManager;

    @Captor
    private ArgumentCaptor<MembershipCreation> membershipCreationArgumentCaptor;

    private final MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID, 123L, "JOHN", "WICK");
    private final Membership membership = new MembershipBuilder().setId(MEMBER_ID).setWebsite("ES").setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID).setMembershipType(MEMBERSHIP_TYPE).setProductStatus(ProductStatus.CONTRACT).build();
    private final List<Membership> membershipList = Collections.singletonList(membership);
    private final List<MemberAccount> memberAccountList = Collections.singletonList(memberAccount);
    private final List<MemberAccount> memberAccountListResult = Collections.singletonList(memberAccount.setMemberships(membershipList));

    private MemberManager manager;

    @BeforeMethod
    public void init() {
        openMocks(this);
        manager = new MemberManager(membershipStore, memberAccountStore, auditMemberAccountManager);
    }

    @Test
    public void testGetMembersWithActivatedMembershipsByUserIdFromDB() throws SQLException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsActivated(eq(dataSource), anyLong())).thenReturn(memberAccountList);
        assertEquals(manager.getMembersWithActivatedMembershipsByUserId(USER_ID, dataSource), memberAccountListResult);
    }

    @Test
    public void testGetMembersWithActivatedMembershipsByUserIdReturnEmptyList() throws SQLException, DataAccessException {
        List<MemberAccount> memberAccounts = new ArrayList<>();
        when(memberAccountStore.getMemberAccountAndMembershipsActivated(eq(dataSource), anyLong())).thenReturn(memberAccounts);
        assertEquals(manager.getMembersWithActivatedMembershipsByUserId(USER_ID, dataSource), memberAccounts);
    }

    @Test
    public void testGetMembersWithEmptyActivatedMembershipsByUserIdFromDB() throws SQLException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsActivated(eq(dataSource), anyLong())).thenReturn(Collections.emptyList());
        assertEquals(manager.getMembersWithActivatedMembershipsByUserId(USER_ID, dataSource), Collections.emptyList());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetActivatedMembershipsByUserIdFromDBSQLException() throws SQLException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsActivated(eq(dataSource), anyLong())).thenThrow(SQL_EXCEPTION);
        assertEquals(manager.getMembersWithActivatedMembershipsByUserId(USER_ID, dataSource), memberAccountListResult);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMembershipsByIdMissingInDB() throws SQLException, MissingElementException, DataAccessException {
        when(membershipStore.fetchMembershipById(eq(dataSource), anyLong())).thenThrow(DATA_NOT_FOUND_EXCEPTION);
        manager.getMembershipById(dataSource, MEMBER_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipsByIdFailedToLoad() throws SQLException, MissingElementException, DataAccessException {
        when(membershipStore.fetchMembershipById(eq(dataSource), anyLong())).thenThrow(SQL_EXCEPTION);
        manager.getMembershipById(dataSource, MEMBER_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipsByIdSQLException() throws SQLException, MissingElementException, DataAccessException {
        when(membershipStore.fetchMembershipById(eq(dataSource), anyLong())).thenThrow(new SQLException());
        manager.getMembershipById(dataSource, MEMBER_ID);
    }

    @Test
    public void testGetMembershipByIdPresentInDB() throws SQLException, MissingElementException, DataAccessException {
        when(membershipStore.fetchMembershipById(eq(dataSource), anyLong())).thenReturn(membership);
        assertEquals(manager.getMembershipById(dataSource, MEMBER_ID), membership);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testCreateMemberThrowException() throws SQLException, DataAccessException {
        when(memberAccountStore.createMemberAccount(eq(dataSource), anyLong(), anyString(), anyString())).thenThrow(SQL_EXCEPTION);
        manager.createMember(dataSource, MEMBERSHIP_CREATION_PENDING);
    }

    @Test
    public void testCreateMemberAndAudit() throws SQLException, DataAccessException {
        long memberAccountId = 10L;
        when(membershipStore.createMember(eq(dataSource), any())).thenReturn(1L);
        when(memberAccountStore.createMemberAccount(eq(dataSource), anyLong(), anyString(), anyString())).thenReturn(memberAccountId);
        assertEquals(1L, manager.createMember(dataSource, MEMBERSHIP_CREATION_PENDING));
        verify(auditMemberAccountManager).auditNewMemberAccount(dataSource, AuditMemberAccount.builder().memberAccountId(memberAccountId)
                .userId(USER_ID).name(NAME).lastName(LAST_NAME).build());
        verify(membershipStore, times(1)).createMember(dataSource, MEMBERSHIP_CREATION_PENDING);
    }

    @Test
    public void testCreateMemberPendingWithoutExpDate() throws SQLException, DataAccessException {
        when(membershipStore.createMember(eq(dataSource), any())).thenReturn(1L);
        when(memberAccountStore.createMemberAccount(eq(dataSource), anyLong(), anyString(), anyString())).thenReturn(10L);
        assertEquals(1L, manager.createMember(dataSource, MEMBERSHIP_CREATION_PENDING));
        verify(memberAccountStore, times(1)).createMemberAccount(eq(dataSource), anyLong(), anyString(), anyString());
        verify(membershipStore, times(1)).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        assertEquals(MemberStatus.PENDING_TO_ACTIVATE, membershipCreationArgumentCaptor.getValue().getMemberStatus());
        assertNull(membershipCreationArgumentCaptor.getValue().getExpirationDate());
    }

    @Test
    public void testCreateMemberActivatedWithExpDate() throws SQLException, DataAccessException {
        when(membershipStore.createMember(eq(dataSource), any())).thenReturn(1L);
        when(memberAccountStore.createMemberAccount(eq(dataSource), anyLong(), anyString(), anyString())).thenReturn(10L);
        MembershipCreation membershipCreation = new MembershipCreationBuilder().withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                .userId(USER_ID).name(NAME).lastNames(LAST_NAME))
                .withWebsite(WEBSITE).withMembershipType(MEMBERSHIP_TYPE).withSourceType(SOURCE_TYPE).withMemberStatus(MemberStatus.ACTIVATED)
                .withExpirationDate(LocalDateTime.now().plusMonths(6)).withSubscriptionPrice(BigDecimal.TEN).build();
        assertEquals(1L, manager.createMember(dataSource, membershipCreation));
        verify(memberAccountStore, times(1)).createMemberAccount(eq(dataSource), anyLong(), anyString(), anyString());
        verify(membershipStore, times(1)).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        assertEquals(MemberStatus.ACTIVATED, membershipCreationArgumentCaptor.getValue().getMemberStatus());
        assertNotNull(membershipCreationArgumentCaptor.getValue().getExpirationDate());
    }

    @Test
    public void testUpdateMemberNamesAndAudit() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountNames(eq(dataSource), anyLong(), anyString(), anyString())).thenReturn(true);
        assertTrue(manager.updateMemberAccountNames(dataSource, MEMBER_ACCOUNT_ID, NAME, LAST_NAME));
        InOrder verifyOrder = inOrder(memberAccountStore, auditMemberAccountManager);
        verifyOrder.verify(memberAccountStore, times(1)).updateMemberAccountNames(eq(dataSource), anyLong(), anyString(), anyString());
        verifyOrder.verify(auditMemberAccountManager).auditUpdatedMemberAccount(dataSource, MEMBER_ACCOUNT_ID);
    }

    @Test
    public void testUpdateMemberNamesNoAuditIfNothingUpdated() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountNames(eq(dataSource), anyLong(), anyString(), anyString())).thenReturn(false);
        manager.updateMemberAccountNames(dataSource, MEMBER_ACCOUNT_ID, NAME, LAST_NAME);
        verifyNoInteractions(auditMemberAccountManager);
    }

    @Test
    public void testUpdateMemberUserIdNoAuditIfNothingUpdated() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong())).thenReturn(false);
        manager.updateMemberAccountNames(dataSource, MEMBER_ACCOUNT_ID, NAME, LAST_NAME);
        verifyNoInteractions(auditMemberAccountManager);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateMemberNamesThrowsException() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountNames(eq(dataSource), anyLong(), anyString(), anyString()))
                .thenThrow(SQL_EXCEPTION);
        manager.updateMemberAccountNames(dataSource, MEMBER_ACCOUNT_ID, NAME, LAST_NAME);
    }

    @Test
    public void testGetMemberAccountsByMembershipId() throws MissingElementException,
            SQLException, DataAccessException {
        when(memberAccountStore.getMemberAccountById(any(DataSource.class), anyLong())).thenReturn(memberAccount);
        assertEquals(manager.getMemberAccountsByMemberAccountId(MEMBER_ACCOUNT_ID, false, dataSource), memberAccount);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMemberAccountsByMemberAccountIdDataNotFoundException() throws MissingElementException,
            SQLException, DataAccessException {
        when(memberAccountStore.getMemberAccountById(any(DataSource.class), anyLong()))
                .thenThrow(DATA_NOT_FOUND_EXCEPTION);
        manager.getMemberAccountsByMemberAccountId(MEMBER_ACCOUNT_ID, false, dataSource);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMemberAccountsByMemberAccountIdDataAccessException() throws MissingElementException,
            SQLException, DataAccessException {
        when(memberAccountStore.getMemberAccountById(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        manager.getMemberAccountsByMemberAccountId(MEMBER_ACCOUNT_ID, false, dataSource);
    }
}
