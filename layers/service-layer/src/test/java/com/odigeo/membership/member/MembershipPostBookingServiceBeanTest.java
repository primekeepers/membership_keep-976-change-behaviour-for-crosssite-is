package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v14.response.BuilderException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.PostBookingPageInfo;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.creation.MembershipCreationService;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.util.CrmCipher;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;

import static com.odigeo.membership.member.PostBookingParser.TOKEN;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class MembershipPostBookingServiceBeanTest {

    private static final long MEMBERSHIP_ID = 112L;
    @Mock
    private DataSource dataSource;
    @Mock
    private MemberService memberService;
    @Mock
    private MemberManager memberManager;
    @Mock
    private MemberUserAreaService memberUserAreaService;
    @Mock
    private MembershipCreationService membershipCreationService;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private CrmCipher crmCipher;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManagerMock;

    @Captor
    private ArgumentCaptor<MembershipCreation> membershipCreationArgumentCaptor;

    @InjectMocks
    private MembershipPostBookingServiceBean postBookingProcessServiceBean = new MembershipPostBookingServiceBean();

    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final String EMAIL = "optimus.prime_123@edreams.com";
    private static final String WRONG_EMAIL = "optimus@.com";
    private static final String USER_ID = "42156";
    private static final String NAME = "Tést";
    private static final String LAST_NAMES = "PöstBoòking";
    private static final String WEBSITE_FR = "FR";
    private static final String CSV_HEADER = "NAME;LAST_NAME;WEBSITE;EMAIL";
    private static final String CSV = CSV_HEADER + "\r\n" + new StringJoiner(TOKEN).add(NAME).add(LAST_NAMES).add(WEBSITE_FR).add(EMAIL);
    private static final String WEBSITE_GB = "GB";
    private static final String MEMBERSHIP_BASIC_TYPE = MembershipType.BASIC.toString();
    private static final String POST_BOOKING_TOKEN = "@bookingId=123456&email=user@mail.com";
    private static final String POST_BOOKING_BAD_TOKEN = "@bookingIds=123456&email=user@mail.com";
    private static final long BOOKING_ID = 123456L;
    private static final String PB_EMAIL = "user@mail.com";

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configureBinders);
    }

    private void configureBinders(Binder binder) {
        binder.bind(MemberService.class).toInstance(memberService);
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(MemberUserAreaService.class).toInstance(memberUserAreaService);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
        binder.bind(MemberAccountStore.class).toInstance(memberAccountStore);
        binder.bind(CrmCipher.class).toInstance(crmCipher);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManagerMock);
        binder.bind(MembershipCreationService.class).toInstance(membershipCreationService);
    }

    @Test
    public void testParseInfoAndCreateAccountNullParam() {
        List<String> errors = postBookingProcessServiceBean.parseInfoAndCreateAccounts(null);
        assertFalse(errors.isEmpty());
        assertEquals(errors.get(0), "No content in the file");
    }

    @Test
    public void testParseInfoAndCreateAccountLessColumnsExpected() {
        String wrongCsv = "USER_ID;NAME;lastName;WEBSITE_FR;monthsToRenewal;\r\n112;test;tést;";
        assertFalse(postBookingProcessServiceBean.parseInfoAndCreateAccounts(wrongCsv).isEmpty());
    }

    @Test
    public void testParseInfoAndCreateAccountInternalEmployeeOk() throws DataAccessException, MissingElementException {
        parseInfoAndCreateInternalEmployeeAccount();
        verify(membershipCreationService).create(membershipCreationArgumentCaptor.capture());
        MemberAccountCreation memberAccountCreation = membershipCreationArgumentCaptor.getValue().getMemberAccountCreation();
        assertEquals(memberAccountCreation.getName(), NAME);
        assertEquals(memberAccountCreation.getLastNames(), LAST_NAMES);
        assertEquals(membershipCreationArgumentCaptor.getValue().getWebsite(), WEBSITE_FR);
        assertEquals(membershipCreationArgumentCaptor.getValue().getMembershipType().toString(), MembershipType.EMPLOYEE.toString());
    }

    @Test
    public void testCreateMembershipSubscriptionPostBookingOK()
            throws DataAccessException, SQLException, BuilderException, ActivatedMembershipException, BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(true);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembership(membershipCreation, EMAIL);
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingManagerMock).sendSubscriptionMessageToCRMTopic(eq(EMAIL), eq(membershipCreation), eq(SubscriptionStatus.SUBSCRIBED));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
    }

    @Test
    public void testCreatePostBookingMembershipBasicFree() throws DataAccessException, ActivatedMembershipException {
        MembershipCreation membershipCreation = getMembershipCreation(true);
        membershipCreation.setMembershipType(MembershipType.BASIC_FREE);
        postBookingProcessServiceBean.createPostBookingMembership(membershipCreation, EMAIL);
        verify(memberManager).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        assertEquals(membershipCreationArgumentCaptor.getValue().getAutoRenewal(), MembershipRenewal.DISABLED);
    }

    @Test(expectedExceptions = ActivatedMembershipException.class, expectedExceptionsMessageRegExp = "An activated membership exists for userId 333 and website ES")
    public void testCreateMembershipSubscriptionPostBookingUserAccountKO()
            throws SQLException, ActivatedMembershipException, DataAccessException {
        MemberAccount memberAccount = getMemberAccountWithMembership();
        List<MemberAccount> memberAccountList = Collections.singletonList(memberAccount);
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenReturn(memberAccountList);
        postBookingProcessServiceBean.createPostBookingMembership(getMembershipCreation(true), EMAIL);
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error creating membership for userId 333")
    public void testCreateMembershipSubscriptionPostBookingSQLExceptionKO()
            throws SQLException, ActivatedMembershipException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenThrow(new SQLException(StringUtils.EMPTY));
        postBookingProcessServiceBean.createPostBookingMembership(getMembershipCreation(true), EMAIL);
    }

    @Test
    public void testCreatePostBookingMembershipPending() throws DataAccessException, BuilderException,
            ActivatedMembershipException, SQLException, BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(false);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembershipPending(membershipCreation);
        verify(memberStatusActionStore, times(1)).createMemberStatusAction(eq(dataSource), eq(444L), eq(StatusAction.PB_PHONE_CREATION));
    }

    @Test(expectedExceptions = ActivatedMembershipException.class, expectedExceptionsMessageRegExp = "An activated membership exists for userId 333 and website ES")
    public void testCreatePendingMembershipSubscriptionPostBookingAlreadyActivatedKO() throws SQLException, ActivatedMembershipException, DataAccessException {
        List<MemberAccount> memberAccountList = Collections.singletonList(getMemberAccountWithMembership());
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenReturn(memberAccountList);
        postBookingProcessServiceBean.createPostBookingMembershipPending(getMembershipCreation(false));
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error creating membership for userId 333")
    public void testCreatePendingMembershipSubscriptionPostBookingSQLExceptionKO() throws SQLException, ActivatedMembershipException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenThrow(new SQLException(StringUtils.EMPTY));
        postBookingProcessServiceBean.createPostBookingMembershipPending(getMembershipCreation(false));
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error creating memberStatusAction PB_PHONE_CREATION for membershipId 0")
    public void testCreatePendingMembershipSubscriptionPostBookingStatusActionSQLExceptionKO() throws SQLException, ActivatedMembershipException, DataAccessException {
        doThrow(new SQLException(StringUtils.EMPTY)).when(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), eq(StatusAction.PB_PHONE_CREATION));
        postBookingProcessServiceBean.createPostBookingMembershipPending(getMembershipCreation(false));
    }

    @Test
    public void testGetPostBookingPageInfo() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenReturn(POST_BOOKING_TOKEN);
        PostBookingPageInfo postBookingPageInfo = postBookingProcessServiceBean.getPostBookingPageInfo("token");
        PostBookingPageInfo idealPostBookingPageInfo = new PostBookingPageInfo(BOOKING_ID, PB_EMAIL);
        assertEquals(postBookingPageInfo, idealPostBookingPageInfo);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = PostBookingPageInfo.EXCEPTION_MESSAGE)
    public void testGetPostBookingPageInfoShouldThrowExceptionWithWrongToken() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenReturn(POST_BOOKING_BAD_TOKEN);
        postBookingProcessServiceBean.getPostBookingPageInfo("token");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testGetPostBookingPageInfoWithException() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenThrow(new IllegalArgumentException());
        postBookingProcessServiceBean.getPostBookingPageInfo("badToken");
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = PostBookingPageInfo.EXCEPTION_MESSAGE)
    public void testGetPostBookingPageInfoWithEmptyToken() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenReturn("NOT_READABLE");
        postBookingProcessServiceBean.getPostBookingPageInfo("");
    }

    @Test
    public void testCreateMembershipSubscriptionPostBooking_SendCorrectIdToReporter()
            throws DataAccessException, BuilderException, ActivatedMembershipException,
            BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(true);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembership(membershipCreation, EMAIL);
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(444L);
    }

    @Test
    public void testCreatePostBookingMembershipPending_SendCorrectIdToReporter() throws DataAccessException, BuilderException,
            ActivatedMembershipException, BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(false);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembershipPending(membershipCreation);
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(444L);
    }

    private MemberAccount getMemberAccountWithMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, 333L, "juan", "carlos");
        Membership membership = new MembershipBuilder().setId(555L).setWebsite("ES").setStatus(MemberStatus.ACTIVATED).setMemberAccountId(123L).setProductStatus(ProductStatus.CONTRACT).build();
        memberAccount.setMemberships(Collections.singletonList(membership));
        return memberAccount;
    }

    private MembershipCreation getMembershipCreation(boolean withExpirationDate) {
        MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder()
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                        .userId(333L)
                        .name("test")
                        .lastNames("postbooking"))
                .withWebsite("ES")
                .withMembershipType(MembershipType.BASIC)
                .withSourceType(SourceType.POST_BOOKING)
                .withSubscriptionPrice(BigDecimal.ZERO)
                .withStatusAction(StatusAction.PB_PHONE_CREATION);

        if (withExpirationDate) {
            membershipCreationBuilder.withActivationDate(LocalDateTime.now())
                    .withExpirationDate(LocalDateTime.now().plusMonths(12));
        }
        return membershipCreationBuilder.build();
    }

    private MembershipCreation buildEmployeeMembershipCreation() {
        return new MembershipCreationBuilder()
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                        .userId(Long.parseLong(USER_ID))
                        .name(NAME)
                        .lastNames(LAST_NAMES))
                .withWebsite(WEBSITE_FR)
                .withMembershipType(MembershipType.BASIC)
                .withSourceType(SourceType.POST_BOOKING)
                .withSubscriptionPrice(BigDecimal.ZERO)
                .build();
    }

    private void parseInfoAndCreateInternalEmployeeAccount() throws DataAccessException, MissingElementException {
        MembershipCreation membershipCreation = buildEmployeeMembershipCreation();
        when(membershipCreationService.create(membershipCreation)).thenReturn(MEMBERSHIP_ID);
        postBookingProcessServiceBean.parseInfoAndCreateAccounts(CSV);
    }
}
