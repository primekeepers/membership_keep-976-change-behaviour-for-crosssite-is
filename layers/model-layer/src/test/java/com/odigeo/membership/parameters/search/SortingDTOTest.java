package com.odigeo.membership.parameters.search;

import bean.test.BeanTest;
import com.odigeo.membership.enums.db.MembershipField;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SortingDTOTest extends BeanTest<SortingDTO> {

    @Override
    protected SortingDTO getBean() {
        return SortingDTO.builder()
                .sortBy(MembershipField.ACTIVATION_DATE.name())
                .sortCriteria("ASC")
                .build();
    }

    @Test
    public void testTimestampSwitch() {
        assertEquals(SortingDTO.builder().sortBy(MembershipField.TIMESTAMP.name())
                .sortCriteria("ASC").build().getSortBy(), "MEMBERSHIP_TIMESTAMP");
    }
}