package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.AbstractServiceBean;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

@Stateless
@Local(BookingTrackingService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BookingTrackingServiceBean extends AbstractServiceBean implements BookingTrackingService {

    private static final Predicate<BigDecimal> IS_NOT_ZERO = bd -> BigDecimal.ZERO.compareTo(bd) != 0;

    @Override
    public boolean isBookingLimitReached(Long memberId) throws DataAccessException {
        try {
            return getBookingTrackingStore().isBookingLimitReached(dataSource, memberId);
        } catch (SQLException e) {
            throw new DataAccessException("Cannot search for tracked bookings for member " + memberId, e);
        }
    }

    @Override
    public BookingTracking getMembershipBookingTracking(long bookingId) throws DataAccessException {
        try {
            return getBookingTrackingStore().getBookingTracked(dataSource, bookingId);
        } catch (SQLException e) {
            throw new DataAccessException("Exception retrieving booking tracking: " + bookingId, e);
        }
    }

    @Override
    public BookingTracking insertMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException {
        try {
            getBookingTrackingStore().trackBooking(dataSource, bookingTracking);
            Membership membership = getMembershipStore().fetchMembershipById(dataSource, bookingTracking.getMembershipId());
            if (IS_NOT_ZERO.test(bookingTracking.getAvoidFeeAmount())) {
                BigDecimal newBalance = membership.getBalance().add(bookingTracking.getAvoidFeeAmount());
                getMembershipStore().updateMembershipBalance(dataSource, membership.getId(), newBalance);
            }
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKED_BOOKINGS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
        } catch (SQLException | DataNotFoundException e) {
            throw new DataAccessRollbackException("Exception inserting tracking of: membership " + bookingTracking.getMembershipId()
                    + " booking " + bookingTracking.getBookingId(), e);
        }
        return getMembershipBookingTracking(bookingTracking.getBookingId());
    }

    @Override
    public void deleteMembershipBookingTracking(long bookingId) throws DataAccessException {
        try {
            Optional<BookingTracking> bookingTracked = Optional.ofNullable(getBookingTrackingStore().getBookingTracked(dataSource, bookingId));
            if (bookingTracked.isPresent()) {
                getBookingTrackingStore().removeTrackedBooking(dataSource, bookingId);
                Membership membership = getMembershipStore().fetchMembershipById(dataSource, bookingTracked.get().getMembershipId());
                BigDecimal newBalance = membership.getBalance().subtract(bookingTracked.get().getAvoidFeeAmount());
                getMembershipStore().updateMembershipBalance(dataSource, membership.getId(), newBalance);
                MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKED_BOOKINGS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME, -1L);
            }
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Exception deleting tracking of booking " + bookingId, e);
        }
    }

    @Override
    public Collection<BookingTracking> getBookingTrackedByMembershipId(Long membershipId) throws DataAccessException {
        try {
            return getBookingTrackingStore().getBookingTrackedByMembershipId(dataSource, membershipId);
        } catch (SQLException e) {
            throw new DataAccessException("Exception retrieving booking tracking by membershipId: " + membershipId, e);
        }
    }

    private BookingTrackingStore getBookingTrackingStore() {
        return ConfigurationEngine.getInstance(BookingTrackingStore.class);
    }

}
