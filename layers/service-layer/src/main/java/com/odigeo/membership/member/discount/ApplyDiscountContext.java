package com.odigeo.membership.member.discount;

import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ApplyDiscountContext {

    private final TestTokenUtils testTokenMatcher;

    private final Collection<Membership> membershipsEligible;
    private final List<NotApplicationReason> notApplicationReason;

    private final Map<String, Collection<Membership>> ruleMembershipsEligible;
    private final Map<String, List<NotApplicationReason>> ruleErrors;

    private final Map<String, Boolean> ruleResults;

    public ApplyDiscountContext(TestTokenUtils testTokenMatcher, Collection<Membership> memberships) {
        this.testTokenMatcher = testTokenMatcher;
        this.membershipsEligible = memberships.stream().distinct().collect(Collectors.toList());
        this.notApplicationReason = new LinkedList<>();
        this.ruleMembershipsEligible = new HashMap<>();
        this.ruleErrors = new LinkedHashMap<>();
        this.ruleResults = new LinkedHashMap<>();
    }

    public List<NotApplicationReason> getNotApplicationReason() {
        return notApplicationReason;
    }

    public void addNotApplicationReason(NotApplicationReason notApplicationReason) {
        this.notApplicationReason.add(notApplicationReason);
    }

    public Collection<Membership> getMembershipsEligible() {
        return membershipsEligible;
    }

    public Collection<Membership> updateMembershipsEligible(Collection<Membership> memberships) {
        membershipsEligible.retainAll(memberships);
        return membershipsEligible;
    }

    public TestTokenUtils getTestTokenMatcher() {
        return testTokenMatcher;
    }

    public Collection<Membership> getRuleMembershipsEligible(String rule) {
        return ruleMembershipsEligible.computeIfAbsent(rule, key -> membershipsEligible);
    }

    public void updateRuleMembershipsEligible(String rule, Collection<Membership> memberships) {
        getRuleMembershipsEligible(rule).retainAll(memberships);
    }

    public List<NotApplicationReason> getRuleErrors(String rule) {
        return ruleErrors.computeIfAbsent(rule, key -> new ArrayList<>());
    }

    public void addRuleError(String rule, NotApplicationReason error) {
        getRuleErrors(rule).add(error);
    }

    public Boolean getRuleResult(String rule) {
        return ruleResults.get(rule);
    }

    public void addRuleResult(String rule, Boolean result) {
        ruleResults.put(rule, result);
    }

}
