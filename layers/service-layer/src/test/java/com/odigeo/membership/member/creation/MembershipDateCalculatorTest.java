package com.odigeo.membership.member.creation;

import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.enums.TimeUnit;
import org.testng.annotations.Test;


import java.time.LocalDateTime;

import static org.testng.AssertJUnit.assertEquals;

public class MembershipDateCalculatorTest {

    private static final Integer DURATION_DAYS = 7;
    private static final Integer DURATION_MONTHS = 6;

    @Test
    public void testGetExpirationDateByDays() {
        MembershipDateCalculator membershipDateCalculator = MembershipDateCalculator.INSTANCE;
        LocalDateTime expected = LocalDateTime.now().plusDays(DURATION_DAYS);
        LocalDateTime actual = membershipDateCalculator.getExpirationDate(getTestMembershipByDays());

        assertEquals(expected.getYear(), actual.getYear());
        assertEquals(expected.getMonth(), actual.getMonth());
        assertEquals(expected.getDayOfYear(), actual.getDayOfYear());
        assertEquals(expected.getHour(), actual.getHour());
    }

    @Test
    public void testGetExpirationDateByMonths() {
        MembershipDateCalculator membershipDateCalculator = MembershipDateCalculator.INSTANCE;
        LocalDateTime expected = LocalDateTime.now().plusMonths(DURATION_MONTHS);
        LocalDateTime actual = membershipDateCalculator.getExpirationDate(getTestMembershipByMonths());

        assertEquals(expected.getYear(), actual.getYear());
        assertEquals(expected.getMonth(), actual.getMonth());
        assertEquals(expected.getDayOfYear(), actual.getDayOfYear());
        assertEquals(expected.getHour(), actual.getHour());

    }

    public Membership getTestMembershipByDays() {
        return new MembershipBuilder().setDurationTimeUnit(TimeUnit.DAYS)
                .setDuration(DURATION_DAYS)
                .build();
    }

    public Membership getTestMembershipByMonths() {
        return new MembershipBuilder().setDurationTimeUnit(TimeUnit.MONTHS)
                .setMonthsDuration(DURATION_MONTHS)
                .build();
    }
}