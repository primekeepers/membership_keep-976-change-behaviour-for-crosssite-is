package com.odigeo.membership.member.nosql;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class MembershipNoSQLManagerImplTest {

    private static final long MEMBERSHIP_ID = 8L;
    private static final String BOOKING_ID = "1991";
    private static final int SECONDS_TO_EXPIRE = 3600;
    private static final String ERROR = "error";
    private static final String WEBSITE = "ES";
    private static final long MEMBER_ACCOUNT_ID = 123L;
    private static final LocalDateTime TODAY = LocalDateTime.now();
    private static final LocalDateTime NEXT_YEAR = TODAY.plusYears(1);
    private static final String CURRENCY_CODE = "USD";
    private static final Membership MEMBERSHIP = new MembershipBuilder()
        .setId(MEMBERSHIP_ID).setWebsite(WEBSITE)
        .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED)
        .setActivationDate(TODAY).setExpirationDate(NEXT_YEAR)
        .setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(com.odigeo.membership.ProductStatus.CONTRACT)
        .setMembershipPricesBuilder(MembershipPrices.builder().totalPrice(BigDecimal.ONE)
            .currencyCode(CURRENCY_CODE)).build();

    @Mock
    private MembershipNoSQLRepository membershipNoSQLRepository;

    private MembershipNoSQLManagerImpl membershipNoSQLManager;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipNoSQLManager = new MembershipNoSQLManagerImpl(membershipNoSQLRepository);
    }

    @Test
    public void testGetBookingIdFromCacheAlreadyStoredInCache() throws DataAccessException {
        when(membershipNoSQLRepository.get(String.valueOf(MEMBERSHIP_ID))).thenReturn(BOOKING_ID);
        Optional<Long> bookingId = membershipNoSQLManager.getBookingIdFromCache(MEMBERSHIP_ID);
        assertEquals(bookingId, Optional.of(Long.valueOf(BOOKING_ID)));
    }

    @Test
    public void testGetBookingIdFromCacheNotStoredInCache() throws DataAccessException {
        when(membershipNoSQLRepository.get(String.valueOf(MEMBERSHIP_ID)))
                .thenThrow(new DataAccessException(ERROR));
        Optional<Long> bookingId = membershipNoSQLManager.getBookingIdFromCache(MEMBERSHIP_ID);
        assertEquals(bookingId, Optional.empty());
    }

    @Test
    public void testStoreInCache() throws DataAccessException {
        membershipNoSQLManager.store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
        verify(membershipNoSQLRepository).store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
    }

    @Test
    public void testStoreInCacheWithFailureWithTheConnection() throws DataAccessException {
        doThrow(new DataAccessException(ERROR)).when(membershipNoSQLRepository)
                .store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
        membershipNoSQLManager.store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
        verify(membershipNoSQLRepository).store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
    }

    @Test
    public void testGetMembership() throws DataAccessException {
        when(membershipNoSQLRepository.get(String.valueOf(MEMBERSHIP_ID))).thenReturn(MEMBERSHIP.toString());
        Optional<Membership> membership = membershipNoSQLManager.get(String.valueOf(MEMBERSHIP_ID));
        assertEquals(membership, Optional.of(MEMBERSHIP));
    }

    @Test
    public void testGetMembershipException() throws DataAccessException {
        when(membershipNoSQLRepository.get(String.valueOf(MEMBERSHIP_ID))).thenThrow(new DataAccessException(ERROR));
        Optional<Membership> membership = membershipNoSQLManager.get(String.valueOf(MEMBERSHIP_ID));
        assertEquals(membership, Optional.empty());
    }
}