package com.odigeo.membership.member.memberapi.converter;

import com.odigeo.membership.request.product.creation.CreateMembershipRequest;

import java.util.HashMap;
import java.util.Map;

public class CreateMembershipConverter extends AbstractEventDataConverter {

    private static final String REQUEST_TYPE = "requestType";

    @Override
    public Object convertArgumentsToJson(Object... args) {
        CreateMembershipRequest request = (CreateMembershipRequest) args[0];
        Map<String, String> map = new HashMap<>();
        map.put(REQUEST_TYPE, request.getClass().getSimpleName());
        return gson.toJson(map);
    }

}
