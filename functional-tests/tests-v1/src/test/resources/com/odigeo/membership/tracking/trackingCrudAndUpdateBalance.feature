Feature: Booking tracking CRUD and update balance

  Scenario Outline: retrieve membership booking tracking

    Given the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount   | costFeeAmount   | perksFeeAmount   |
      | <bookingId> | <memberId> | <bookingDate>  | <avoidFeeAmount> | <costFeeAmount> | <perksFeeAmount> |
    Then getBookingTracking with bookingId <bookingId> is not empty

    Examples:
      | bookingId | memberId | bookingDate | avoidFeeAmount | costFeeAmount | perksFeeAmount |
      | 66330     | 55440    | 2018-01-01  | -18.5          | 0             | -10            |
      | 66331     | 55441    | 2018-02-02  | -10.8          | -8.5          | -15            |

  Scenario Outline: Update balance and track booking, add

    Given the next membership stored in db:
      | memberId   | status             | balance         | activationDate   | expirationDate | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | <activationDate> | 2019-10-10     | 1               | ENABLED     |
    When addMembershipBookingTracking with following request is called
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    |
      | <bookingId> | <memberId> | <creationDate> | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> |
    Then the tracked booking exists in db
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    |
      | <bookingId> | <memberId> | <creationDate> | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> |
    And the stored membership with ID <memberId> now has <balanceAfter> as balance

    Examples:
      | bookingId | memberId | membershipStatus | activationDate | creationDate | balanceBefore | bookingAvoidFee | bookingCostFee | bookingPerksFee | balanceAfter |
      | 66330     | 55440    | ACTIVATED        | 2019-05-24     | 2019-07-01   | 60.0          | -18.5           | 0              | -10.0           | 41.5         |
      | 66331     | 55441    | ACTIVATED        | 2019-05-24     | 2019-07-01   | 10.0          | -10.0           | -8.5           | -10.0           | 0.0          |

  Scenario Outline: Update balance and track booking, delete

    Given the next membership activated today is stored in db:
      | memberId   | status             | balance         | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | 1               | ENABLED     |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount   | costFeeAmount | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | <avoidFeeAmount> | -12.5         | -10            |
    When deleteMembershipBookingTracking with bookingId <bookingId> is called
    Then the tracked booking with the booking ID <bookingId> does not exist
    And the stored membership with ID <memberId> now has <balanceAfter> as balance

    Examples:
      | bookingId | memberId | membershipStatus | bookingDate | balanceBefore | avoidFeeAmount | balanceAfter |
      | 91234     | 92990    | ACTIVATED        | 2018-01-01  | 60.2          | -2.1           | 62.3         |
      | 91245     | 92991    | ACTIVATED        | 2018-02-02  | -21.5         | -54.99         | 33.49        |
      | 91256     | 92992    | ACTIVATED        | 2018-03-03  | 40.3          | -10.3          | 50.6         |
      | 91267     | 92993    | ACTIVATED        | 2018-04-04  | -10.8         | -2.1           | -8.7         |
      | 92055     | 92884    | DEACTIVATED      | 2018-05-15  | 60.2          | -2.1           | 62.3         |
      | 92056     | 92885    | DEACTIVATED      | 2018-06-16  | 60.2          | -60.2          | 120.4        |
      | 92057     | 92886    | DEACTIVATED      | 2018-06-17  | 40.3          | -10.3          | 50.6         |