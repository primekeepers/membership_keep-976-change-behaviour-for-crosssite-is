package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.MembershipField;

import java.util.StringJoiner;

public class SortingDTO {
    private static final String SQL_KEYWORD = "ORDER BY";
    private final String sortBy;
    private final String sortCriteria;

    public String getSortBy() {
        return sortBy;
    }

    public String getSortCriteria() {
        return sortCriteria;
    }

    private SortingDTO(Builder builder) {
        sortBy = builder.sortBy;
        sortCriteria = builder.sortCriteria;
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private static final String TIMESTAMP = "TIMESTAMP";
        private static final String MEMBERSHIP_TIMESTAMP = "MEMBERSHIP_TIMESTAMP";
        private String sortBy;
        private String sortCriteria;

        public Builder sortBy(String sortBy) {
            if (TIMESTAMP.equals(sortBy)) {
                this.sortBy = MEMBERSHIP_TIMESTAMP;
            } else {
                this.sortBy = MembershipField.valueOf(sortBy).withTableAlias();
            }
            return this;
        }

        public Builder sortCriteria(String sortCriteria) {
            this.sortCriteria = sortCriteria;
            return this;
        }

        public SortingDTO build() {
            return new SortingDTO(this);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(" ")
                .add(SQL_KEYWORD)
                .add(sortBy)
                .add(sortCriteria)
                .toString();
    }
}

