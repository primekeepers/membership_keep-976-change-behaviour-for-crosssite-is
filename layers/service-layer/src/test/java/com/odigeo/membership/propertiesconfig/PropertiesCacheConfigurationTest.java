package com.odigeo.membership.propertiesconfig;


import com.edreams.persistance.cache.Cache;
import com.edreams.persistance.cache.impl.SimpleMemoryOnlyCache;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class PropertiesCacheConfigurationTest {

    private PropertiesCacheConfiguration configuration;
    private final Cache sendingIdsToKafkaActiveCacheMock = new SimpleMemoryOnlyCache("isSendingIdsToKafkaActiveCacheMock", "sendIdsKafkaActive", Boolean.class);
    private final Cache transactionalWelcomeEmailActive = new SimpleMemoryOnlyCache("isTransactionalWelcomeEmailActiveCache","sendTransactionalWelcomeEmailActive", Boolean.class);

    @BeforeClass
    public void setUp() {
       sendingIdsToKafkaActiveCacheMock.addEntry("sendIdsKafkaActive", true);
       transactionalWelcomeEmailActive.addEntry("sendTransactionalWelcomeEmailActive", true);
       configuration = new PropertiesCacheConfiguration();
       configuration.setIsTransactionalWelcomeEmailActiveCache(transactionalWelcomeEmailActive);
       configuration.setIsSendingIdsToKafkaActiveCache(sendingIdsToKafkaActiveCacheMock);
    }

    @Test
    public void testSendIdsKafkaActiveProperties() {
        assertEquals(configuration.getIsSendingIdsToKafkaActiveCache().getCacheSize(), 1);
    }

    @Test
    public void testSendTransactionalWelcomeEmailActive() {
        assertEquals(configuration.getIsSendingIdsToKafkaActiveCache().getCacheSize(), 1);
    }


}