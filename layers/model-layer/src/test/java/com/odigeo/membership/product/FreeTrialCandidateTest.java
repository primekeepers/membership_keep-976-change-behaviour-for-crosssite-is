package com.odigeo.membership.product;

import bean.test.BeanTest;

public class FreeTrialCandidateTest extends BeanTest<FreeTrialCandidate> {

    private static final String EMAIL = "email@edreamsodigeo.com";
    private static final String WEBSITE = "IT";
    private static final String NAME = "Cloud";
    private static final String LAST_NAME = "Strife";

    @Override
    protected FreeTrialCandidate getBean() {
        return FreeTrialCandidate.builder()
            .email(EMAIL).website(WEBSITE).name(NAME)
            .lastName(LAST_NAME).build();
    }
}