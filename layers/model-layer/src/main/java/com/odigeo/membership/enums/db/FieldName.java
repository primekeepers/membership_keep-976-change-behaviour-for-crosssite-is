package com.odigeo.membership.enums.db;

public interface FieldName {
    enum TableAlias {
        MEMBER_ACCOUNT_ALIAS("a"), MEMBERSHIP_ALIAS("m"), MEMBERSHIP_RECURRING_ALIAS("gmr"), MEMBERSHIP_RECURRING_COLLECTION_ALIAS("gmrc"), MEMBER_STATUS_ACTION_ALIAS("gmsa");

        private String alias;

        TableAlias(String alias) {
            this.alias = alias;
        }

        @Override
        public String toString() {
            return this.alias;
        }
    }

    String withTableAlias();

    String filteringName();
}
