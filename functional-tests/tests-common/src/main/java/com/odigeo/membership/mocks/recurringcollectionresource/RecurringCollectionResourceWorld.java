package com.odigeo.membership.mocks.recurringcollectionresource;

import com.google.inject.Inject;
import com.odigeo.membership.server.ServerStopException;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;
import java.util.UUID;

@ScenarioScoped
public class RecurringCollectionResourceWorld {
    private final RecurringCollectionResourceHttpServer recurringCollectionResourceHttpServer;
    private final RecurringCollectionResourceMock recurringCollectionResourceMock;

    @Inject
    public RecurringCollectionResourceWorld(RecurringCollectionResourceHttpServer recurringCollectionResourceHttpServer, RecurringCollectionResourceMock recurringCollectionResourceMock) {
        this.recurringCollectionResourceHttpServer = recurringCollectionResourceHttpServer;
        this.recurringCollectionResourceMock = recurringCollectionResourceMock;
    }

    public void install() throws ServerStopException {
        if (recurringCollectionResourceHttpServer.serverNotCreated()) {
            recurringCollectionResourceHttpServer.startServer();
        }
        recurringCollectionResourceHttpServer.addService(recurringCollectionResourceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(recurringCollectionResourceHttpServer)) {
            recurringCollectionResourceHttpServer.clearServices();
        }
    }

    public UUID getGeneratedUUID() {
        return recurringCollectionResourceMock.getGeneratedUUID();
    }
}
