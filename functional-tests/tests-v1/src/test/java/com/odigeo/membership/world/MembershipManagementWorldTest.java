package com.odigeo.membership.world;

import bean.test.BeanTest;
import com.odigeo.membership.functionals.membership.MembershipCreationRequestBuilder;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import org.mockito.Mock;

import java.util.Collections;

import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipManagementWorldTest extends BeanTest<MembershipManagementWorld> {

    private static final long CREATED_MEMBER_ID = 1234L;
    private static final String LAST_NAME = "last-names";
    private static final String NAME = "name";
    private static final String MEMBER_ACCOUNT_ID = "1234";
    private static final String MEMBERSHIP_TYPE = "TYPE";
    @Mock
    private Membership membership;
    @Mock
    private MembershipRenewal membershipRenewalStatus;
    @Mock
    private MembershipCreationRequestBuilder membershipCreationRequestBuilder;
    @Mock
    private MembershipStatus membershipStatus;
    @Mock
    private MemberSubscriptionDetails memberSubscriptionDetails;

    @Override
    protected MembershipManagementWorld getBean() {
        openMocks(this);
        MembershipManagementWorld bean = new MembershipManagementWorld();
        return createBean(bean);
    }

    private MembershipManagementWorld createBean(MembershipManagementWorld bean) {
        bean.setMembership(this.membership);
        bean.setMembershipAutoRenewalStatus(membershipRenewalStatus);
        bean.setMembershipList(Collections.singletonList(membership));
        bean.setMembershipCreationRequest(Collections.singletonList(membershipCreationRequestBuilder));
        bean.setCreatedMemberId(CREATED_MEMBER_ID);
        bean.setMemberAccountUpdateRequest(MEMBER_ACCOUNT_ID, NAME, LAST_NAME);
        bean.setUpdatedMemberId(Boolean.TRUE);
        bean.setMembershipStatus(membershipStatus);
        bean.addException(new Exception());
        bean.setSubscriptionsDetails(Collections.singletonList(memberSubscriptionDetails));
        bean.setMembershipType(MEMBERSHIP_TYPE);
        return bean;
    }
}