package com.odigeo.membership;

import java.util.Date;

public class MemberAccountBooking {

    private final MemberAccount memberAccount;
    private final Long bookingDetailSubscriptionId;
    private final Date lastStatusModificationDate;

    public MemberAccountBooking(final MemberAccount memberAccount, final Long bookingDetailSubscriptionId, final Date lastStatusModificationDate) {
        this.memberAccount = memberAccount;
        this.bookingDetailSubscriptionId = bookingDetailSubscriptionId;
        this.lastStatusModificationDate = date(lastStatusModificationDate);
    }

    public MemberAccount getMemberAccount() {
        return memberAccount;
    }

    public Long getBookingDetailSubscriptionId() {
        return bookingDetailSubscriptionId;
    }

    public Date getLastStatusModificationDate() {
        return date(lastStatusModificationDate);
    }

    private Date date(final Date lastStatusModificationDate) {
        return lastStatusModificationDate == null ? null : new Date(lastStatusModificationDate.getTime());
    }
}
