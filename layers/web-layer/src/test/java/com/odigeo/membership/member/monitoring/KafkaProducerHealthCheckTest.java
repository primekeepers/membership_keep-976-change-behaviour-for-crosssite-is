package com.odigeo.membership.member.monitoring;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.robots.activator.MembershipActivationProducerConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

public class KafkaProducerHealthCheckTest {

    private static final String TEST_RESOURCE = "test";


    private static final String BROCKER_LIST = "kafkahost1:2181,kafkahost2:2181,kafkahost3:2181";

    private KafkaProducerHealthCheck kafkaProducerHealthCheck;

    private MembershipActivationProducerConfiguration membershipActivationProducerConfiguration;


    @BeforeMethod
    public void setUp() throws Exception {
        membershipActivationProducerConfiguration = new MembershipActivationProducerConfiguration();
        ConfigurationEngine.init(this::configure);
        kafkaProducerHealthCheck = new KafkaProducerHealthCheck();
    }

    private void configure(Binder binder) {
        binder.bind(MembershipActivationProducerConfiguration.class).toInstance(membershipActivationProducerConfiguration);
    }

    @Test
    public void testCheckReturnsFalseWithInconsistentData() {
        membershipActivationProducerConfiguration.setBrokerList("inconsistentData");
        assertFalse(kafkaProducerHealthCheck.execute().isHealthy());
    }

    @Test
    public void testCheckReturnsFalseWhenMembershipActivationProducerConfigurationAvailableWithWrongResource() {
        membershipActivationProducerConfiguration.setBrokerList(BROCKER_LIST);
        assertFalse(kafkaProducerHealthCheck.execute().isHealthy());
    }

    @Test
    public void testCheckReturnsFalse() {
        membershipActivationProducerConfiguration.setBrokerList("");
        assertFalse(kafkaProducerHealthCheck.execute().isHealthy());
    }
}
