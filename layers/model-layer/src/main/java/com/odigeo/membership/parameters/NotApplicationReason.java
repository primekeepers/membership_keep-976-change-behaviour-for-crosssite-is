package com.odigeo.membership.parameters;

public enum NotApplicationReason {

    WEBSITE_INVALID("Website not applicable"),
    ORIGIN_INVALID("Origin not applicable"),
    INTERFACE_INVALID("Interface not applicable"),
    TRAVELLERS_INVALID("Travellers not applicable"),
    MEMBERSHIP_INVALID("Membership not applicable"),
    MEMBERSHIP_UNKNOWN("Membership not found"),
    USER_UNKNOWN("User not found"),
    USER_INVALID("User not applicable"),
    UNKNOWN("Unknown");

    private final String literal;

    NotApplicationReason(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }
}
