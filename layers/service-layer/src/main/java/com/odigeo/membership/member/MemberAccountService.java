package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.exception.AlreadyHasPrimeMembershipException;
import com.odigeo.membership.exception.UserAccountNotFoundException;

import java.util.List;
import java.util.Set;

public interface MemberAccountService {

    MemberAccount getMembershipAccount(long userId, String website) throws DataAccessException;

    List<MemberAccount> getActiveMembersByUserId(long userId) throws DataAccessException;

    MemberAccount getMemberAccountByMembershipId(long membershipId) throws MissingElementException, DataAccessException;

    MemberAccount getMemberWithActivatedMemberships(long userId, String website) throws DataAccessException;

    Boolean updateMemberAccount(UpdateMemberAccount updateMemberAccount) throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException;

    Set<String> getActiveMembershipSitesByUserId(long userId) throws DataAccessException;

    MemberAccount getMemberAccountById(long accountId, boolean withMemberships) throws MissingElementException, DataAccessException;

}
