package com.odigeo.messaging.kafka;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.messaging.MembershipProductClosureService;
import com.odigeo.shoppingbasket.checkout.messages.ProductClosureRequested;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MembershipProductClosureProcessor implements MessageProcessor<DomainEvent<ProductClosureRequested>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipProductClosureProcessor.class);
    private static final String MEMBERSHIP_TYPE = "MEMBERSHIP";
    private final MembershipProductClosureService controller;

    @Inject
    public MembershipProductClosureProcessor(MembershipProductClosureService controller) {
        this.controller = controller;
    }

    @Override
    public void onMessage(DomainEvent<ProductClosureRequested> domainEvent) {
        ProductClosureRequested productClosureRequested = domainEvent.getPayload();
        if (MEMBERSHIP_TYPE.equals(productClosureRequested.getProductReference().getType())) {
            try {
                controller.closeMembership(productClosureRequested);
            } catch (MissingElementException e) {
                LOGGER.info("Could not find membership reference: " + productClosureRequested.getProductReference().getReference(), e);
            } catch (DataAccessException e) {
                LOGGER.error("Could not close membership reference: " + productClosureRequested.getProductReference().getReference(), e);
            }
        }
    }
}
