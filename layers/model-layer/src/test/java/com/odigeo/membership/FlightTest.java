package com.odigeo.membership;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;

public class FlightTest extends BeanTest<Flight> {
    @Override
    protected Flight getBean() {
        return new Flight(1L, ZonedDateTime.now(), StringUtils.EMPTY);
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(Flight.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
