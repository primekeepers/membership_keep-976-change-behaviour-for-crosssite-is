package com.odigeo.userapi;

import com.google.common.collect.ImmutableMap;
import com.odigeo.userprofiles.api.v2.model.Brand;

import java.util.Locale;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.EMPTY;

public final class BrandMapper {

    private static final String ED_PREFIX = "ED";
    private static final String GO_PREFIX = "GO";
    private static final String OP_PREFIX = "OP";
    private static final String TL_PREFIX = "TL";
    private static final ImmutableMap<String, Optional<Brand>> BRAND_MAP = ImmutableMap.of(
            EMPTY, Optional.of(Brand.ED),
            ED_PREFIX, Optional.of(Brand.ED),
            GO_PREFIX, Optional.of(Brand.GV),
            OP_PREFIX, Optional.of(Brand.OP),
            TL_PREFIX, Optional.of(Brand.TL)
    );

    private BrandMapper() {
    }

    public static Optional<Brand> map(String website) {
        if (nonNull(website)) {
            String formattedWebsite = website.trim().toUpperCase(Locale.getDefault());
            if (formattedWebsite.length() == 2) {
                return Optional.of(Brand.ED);
            }
            if (formattedWebsite.length() == 4) {
                return findBrandForWebsite(formattedWebsite);
            }
        }
        return Optional.empty();
    }

    private static Optional<Brand> findBrandForWebsite(String formattedWebsite) {
        String prefix = formattedWebsite.substring(0, 2);
        return BRAND_MAP.getOrDefault(prefix, Optional.empty());
    }
}
