package com.odigeo.membership.steps;

import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.mocks.kafka.MessageControllerWorld;
import com.odigeo.membership.mocks.kafka.consumers.MembershipSubscriptionMessageConsumer;
import com.odigeo.membership.mocks.kafka.processors.MembershipSubscriptionMessageProcessor;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class KafkaStepsTest {

    private static final String EMAIL_TEST = "test@test.com";

    @Mock
    private MembershipSubscriptionMessageConsumer membershipSubscriptionMessageConsumer;

    @Mock
    private MembershipSubscriptionMessageProcessor membershipSubscriptionMessageProcessor;

    @Mock
    private DatabaseWorld databaseWorld;

    @Mock
    private MessageControllerWorld messageControllerWorld;

    @Mock
    private MembershipSubscriptionMessage membershipSubscriptionMessage;

    private KafkaSteps instance;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        this.instance = new KafkaSteps(this.databaseWorld, this.messageControllerWorld);
        List<MembershipSubscriptionMessage> membershipSubscriptionMessages = createSubscriptionMessagesList();
        when(messageControllerWorld.getMembershipSubscriptionMessageConsumer()).thenReturn(membershipSubscriptionMessageConsumer);
        when(membershipSubscriptionMessageConsumer.getMessageProcessor()).thenReturn(membershipSubscriptionMessageProcessor);
        when(membershipSubscriptionMessageProcessor.getReceivedMessages()).thenReturn(membershipSubscriptionMessages);
    }

    @Test
    public void testReadingMessagesFromKafka() {
        this.instance.readMembershipSubscriptionMessageFromKafka(EMAIL_TEST);
        verify(this.membershipSubscriptionMessageProcessor).getReceivedMessages();
        verify(this.membershipSubscriptionMessage, times(2)).getSubscriptionStatus();
        verify(this.membershipSubscriptionMessage, times(2)).getEmail();
    }

    private List<MembershipSubscriptionMessage> createSubscriptionMessagesList() {
        when(this.membershipSubscriptionMessage.getSubscriptionStatus()).thenReturn(SubscriptionStatus.SUBSCRIBED);
        when(this.membershipSubscriptionMessage.getEmail()).thenReturn(EMAIL_TEST);
        return Collections.singletonList(this.membershipSubscriptionMessage);
    }
}
