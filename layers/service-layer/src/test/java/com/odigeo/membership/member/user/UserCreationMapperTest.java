package com.odigeo.membership.member.user;

import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userprofiles.api.v2.model.User;
import org.testng.annotations.Test;

import java.security.InvalidParameterException;

import static org.testng.Assert.assertEquals;

public class UserCreationMapperTest {

    private static final String BRAND = "ED";
    private static final String EMAIL = "galjina@mail.com";
    private static final String LOCALE = "ru_RU";
    private static final Integer TRAFFIC_INTERFACE_ID = 1;
    private static final String WEBSITE = "ES";

    @Test
    public void mapToUserWithValidCreation() {
        UserCreation userCreation = getValidCreation();
        User user = UserCreationMapper.mapToUser(userCreation);
        assertEquals(user.getBrand().name(), BRAND);
        assertEquals(user.getEmail(), EMAIL);
        assertEquals(user.getTrafficInterface().getId(), TRAFFIC_INTERFACE_ID.intValue());
        assertEquals(user.getLocale(), LOCALE);
        assertEquals(user.getWebsite(), WEBSITE);
    }

    @Test(expectedExceptions = {InvalidParameterException.class}, expectedExceptionsMessageRegExp = ".*traffic interface id.*")
    public void mapToUserWithInvalidTrafficInterface() {
        UserCreation userCreation = getValidCreation();
        userCreation.setTrafficInterfaceId(-1);
        UserCreationMapper.mapToUser(userCreation);
    }

    private UserCreation getValidCreation() {
        return new UserCreation.Builder()
                .withEmail(EMAIL)
                .withLocale(LOCALE)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .withWebsite(WEBSITE)
                .build();
    }
}
