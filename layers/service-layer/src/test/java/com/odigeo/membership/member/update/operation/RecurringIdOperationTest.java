package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import static com.odigeo.membership.member.update.UpdateMembershipObjectMother.anActivatedMembershipBuilder;
import static java.util.Objects.nonNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class RecurringIdOperationTest {

    @Mock
    private MembershipStore membershipStore;
    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipRecurringStore membershipRecurringStore;
    private RecurringIdOperation recurringIdOperation;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        recurringIdOperation = new RecurringIdOperation(dataSource, membershipRecurringStore, membershipStore);
    }

    @DataProvider
    Object[][] exceptionsProvider() {
        return new Object[][]{
                {anActivatedMembershipBuilder().build(), new DataNotFoundException(StringUtils.EMPTY), MissingElementException.class},
                {anActivatedMembershipBuilder().build(), new SQLException(), DataAccessException.class},
                {anActivatedMembershipBuilder().setRecurringId("123").build(), null, ExistingRecurringException.class},
        };
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anInsertRecurringIdUpdateMembership();
        Membership membership = anActivatedMembershipBuilder().build();
        long membershipId = Long.parseLong(updateMembership.getMembershipId());
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(membershipId))).thenReturn(membership);
        recurringIdOperation.update(updateMembership);
        verify(membershipRecurringStore).insertMembershipRecurring(eq(dataSource), eq(membershipId), eq(updateMembership.getRecurringId()));
    }

    @Test(dataProvider = "exceptionsProvider")
    public void testUpdateStoreExceptions(Membership membership, Throwable storeException, Class<Throwable> expectedExceptionClass) throws DataNotFoundException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anInsertRecurringIdUpdateMembership();
        long membershipId = Long.parseLong(updateMembership.getMembershipId());
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(membershipId))).thenReturn(membership);
        if (nonNull(storeException)) {
            when(membershipStore.fetchMembershipById(eq(dataSource), eq(membershipId))).thenThrow(storeException);
        }
        try {
            recurringIdOperation.update(updateMembership);
            fail();
        } catch (ExistingRecurringException | DataAccessException | MissingElementException e) {
            assertEquals(e.getClass(), expectedExceptionClass);
        }
    }

    @Test(expectedExceptions = InvalidParameterException.class)
    public void testUpdateWrongOperationParameter() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        Membership membership = anActivatedMembershipBuilder().build();
        long membershipId = Long.parseLong(updateMembership.getMembershipId());
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(membershipId))).thenReturn(membership);
        recurringIdOperation.update(updateMembership);
    }
}