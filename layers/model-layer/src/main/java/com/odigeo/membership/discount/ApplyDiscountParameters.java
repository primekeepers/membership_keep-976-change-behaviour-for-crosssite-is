package com.odigeo.membership.discount;

import com.odigeo.membership.enums.Interface;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.membership.parameters.UserIdentification;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Collection;
import java.util.Map;

public class ApplyDiscountParameters {

    private Map<String, Integer> testDimensions;
    private UserIdentification user;
    private String website;
    private Collection<TravellerParameter> travellers;
    private Boolean metasearch;
    private Interface clientInterface;

    public Map<String, Integer> getTestDimensions() {
        return testDimensions;
    }

    public void setTestDimensions(Map<String, Integer> testDimensions) {
        this.testDimensions = testDimensions;
    }

    public UserIdentification getUser() {
        return user;
    }

    public void setUser(UserIdentification user) {
        this.user = user;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Collection<TravellerParameter> getTravellers() {
        return travellers;
    }

    public void setTravellers(Collection<TravellerParameter> travellers) {
        this.travellers = travellers;
    }

    public Boolean getMetasearch() {
        return metasearch;
    }

    public boolean isMetasearch() {
        return BooleanUtils.isTrue(metasearch);
    }

    public void setMetasearch(Boolean metasearch) {
        this.metasearch = metasearch;
    }

    public Interface getClientInterface() {
        return clientInterface;
    }

    public void setClientInterface(Interface clientInterface) {
        this.clientInterface = clientInterface;
    }
}
