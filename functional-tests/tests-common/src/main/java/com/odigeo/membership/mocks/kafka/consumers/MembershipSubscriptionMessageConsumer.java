package com.odigeo.membership.mocks.kafka.consumers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.mocks.kafka.processors.MembershipSubscriptionMessageProcessor;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;

@Singleton
public class MembershipSubscriptionMessageConsumer extends AbstractKafkaMessageConsumer<MembershipSubscriptionMessage> {
    private static final String TOPIC_NAME = "MEMBERSHIP_ACTIVATIONS_v4";
    private static final String GROUP_ID = "SubscriptionMessageFunctionalTest";
    private final MembershipSubscriptionMessageProcessor messageProcessor;

    @Inject
    public MembershipSubscriptionMessageConsumer(MembershipSubscriptionMessageProcessor membershipSubscriptionMessageProcessor) {
        super(membershipSubscriptionMessageProcessor, MembershipSubscriptionMessage.class, TOPIC_NAME, GROUP_ID);
        this.messageProcessor = membershipSubscriptionMessageProcessor;
    }

    @Override
    public MembershipSubscriptionMessageProcessor getMessageProcessor() {
        return messageProcessor;
    }
}
