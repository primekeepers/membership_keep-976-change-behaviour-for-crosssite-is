Feature: Test site related logic for memberships

  Scenario Outline: Should return false when membership perks not active for the site
    When check if website <site> supports membership
    Then website does not support membership
    Examples:
    | site |
    | OPIT |
    | JP   |
    | ID   |
    | IS   |
    | DE   |


  Scenario Outline: Should return true when membership perks active for the site
    When check if website <site> supports membership
    Then website supports membership
    Examples:
    | site |
    | FR   |
    | ES   |
    | GOFR |
    | OPFR |
    | IT   |
    | OPDE |
    | UK   |
    | OPUK |
    | US   |
