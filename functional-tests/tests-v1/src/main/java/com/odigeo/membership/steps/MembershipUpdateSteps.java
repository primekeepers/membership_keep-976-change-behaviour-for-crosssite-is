package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;

@ScenarioScoped
public class MembershipUpdateSteps extends CommonSteps {

    private final MembershipManagementWorld world;

    @Inject
    public MembershipUpdateSteps(MembershipManagementWorld world, final ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
        this.world = world;
    }

    @When("^requested to update membership$")
    public void requestedToUpdateMembership(List<UpdateMembershipRequest> updateMembershipRequests) {
        try {
            world.setOperationSuccess(membershipService.updateMembership(updateMembershipRequests.iterator().next()));
        } catch (MembershipInternalServerErrorException | UnsupportedOperationException e) {
            world.addException(e);
        }
    }
}
