package com.odigeo.membership.member.update.operation.autorenewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.membership.member.update.operation.RemindMeLaterOperation;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;

public class DisableAutoRenewalOperation extends AutoRenewalChangeOperation {
    private final RemindMeLaterOperation remindMeLaterOperation;

    public interface Factory extends MembershipUpdateOperationFactory {
        DisableAutoRenewalOperation createOperation(DataSource dataSource);
    }

    @Inject
    public DisableAutoRenewalOperation(@Assisted DataSource dataSource,
                                       RemindMeLaterOperation remindMeLaterOperation,
                                       MemberService membershipService,
                                       MembershipStore membershipStore,
                                       MembershipMessageSendingManager membershipMessageSendingManager) {
        super(membershipStore, membershipMessageSendingManager, membershipService, dataSource);
        this.remindMeLaterOperation = remindMeLaterOperation;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        return MembershipRenewal.DISABLED.name().equals(disableAutoRenewal(updateMembership));
    }

    public String disableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        String autoRenewalChanged = changeAutoRenewal(Long.parseLong(updateMembership.getMembershipId()), MembershipRenewal.DISABLED, MembershipRenewal.ENABLED, AutoRenewalOperation.DISABLE_AUTO_RENEW);
        Membership membership = membershipService.getMembershipById(Long.parseLong(updateMembership.getMembershipId()));
        if (MembershipRenewal.DISABLED.toString().equals(autoRenewalChanged) && membership.getRemindMeLater()) {
            remindMeLaterOperation.setRemindMeLater(UpdateMembership.builderCloneOf(updateMembership)
                    .remindMeLater(Boolean.FALSE)
                    .build());
        }
        return autoRenewalChanged;
    }
}
