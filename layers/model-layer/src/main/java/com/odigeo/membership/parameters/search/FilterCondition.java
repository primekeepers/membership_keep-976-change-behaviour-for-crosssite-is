package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.Condition;
import com.odigeo.membership.enums.db.FieldName;

import java.util.Objects;

public class FilterCondition {
    private final FieldName fieldName;
    private final Condition condition;
    private final Object value;

    public FilterCondition(FieldName fieldName, Condition condition, Object value) {
        this.fieldName = fieldName;
        this.condition = condition;
        this.value = value;
    }

    public FilterCondition(FieldName fieldName, Object value) {
        this(fieldName, Condition.EQ, value);
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return fieldName.filteringName() + condition.getQueryChunk();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FilterCondition that = (FilterCondition) o;
        return Objects.equals(fieldName, that.fieldName) && condition == that.condition && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName, condition, value);
    }
}
