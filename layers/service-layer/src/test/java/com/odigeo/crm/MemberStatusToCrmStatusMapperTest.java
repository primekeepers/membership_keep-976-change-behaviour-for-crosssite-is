package com.odigeo.crm;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MemberStatusToCrmStatusMapperTest {

    private MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;

    @BeforeMethod
    public void before() {
        memberStatusToCrmStatusMapper = new MemberStatusToCrmStatusMapper();
    }

    @Test(dataProvider = "testMapData")
    public void testMapForUpdatedMembership(MemberStatus memberStatus, MembershipRenewal membershipRenewal, MemberStatus newStatus, SubscriptionStatus expectedSubscriptionStatus) {
        final Membership membership = getMembership(memberStatus, membershipRenewal);
        assertEquals(memberStatusToCrmStatusMapper.mapForUpdatedMembership(membership, newStatus), expectedSubscriptionStatus);
    }

    @DataProvider(name = "testMapData")
    public Object[][] getData() {
        return new Object[][] {
                {MemberStatus.DEACTIVATED, MembershipRenewal.ENABLED, MemberStatus.ACTIVATED, SubscriptionStatus.SUBSCRIBED},
                {MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MemberStatus.EXPIRED, SubscriptionStatus.PENDING},
                {MemberStatus.ACTIVATED, MembershipRenewal.DISABLED, MemberStatus.EXPIRED, SubscriptionStatus.UNSUBSCRIBED},
                {MemberStatus.DEACTIVATED, MembershipRenewal.ENABLED, MemberStatus.EXPIRED, SubscriptionStatus.UNSUBSCRIBED},
                {MemberStatus.PENDING_TO_ACTIVATE, MembershipRenewal.ENABLED, MemberStatus.EXPIRED, SubscriptionStatus.UNSUBSCRIBED},
                {MemberStatus.PENDING_TO_COLLECT, MembershipRenewal.ENABLED, MemberStatus.EXPIRED, SubscriptionStatus.UNSUBSCRIBED},
                {MemberStatus.PENDING_TO_COLLECT, MembershipRenewal.DISABLED, MemberStatus.DISCARDED, SubscriptionStatus.UNSUBSCRIBED}
        };
    }

    @Test
    public void testMap() {
        assertEquals(memberStatusToCrmStatusMapper.map(MemberStatus.ACTIVATED), SubscriptionStatus.SUBSCRIBED);
        assertEquals(memberStatusToCrmStatusMapper.map(MemberStatus.EXPIRED), SubscriptionStatus.UNSUBSCRIBED);
        assertEquals(memberStatusToCrmStatusMapper.map(MemberStatus.DEACTIVATED), SubscriptionStatus.UNSUBSCRIBED);
        assertEquals(memberStatusToCrmStatusMapper.map(MemberStatus.PENDING_TO_COLLECT), SubscriptionStatus.PENDING);
    }

    private Membership getMembership(MemberStatus memberStatus, MembershipRenewal membershipRenewal) {
        return new MembershipBuilder()
                .setStatus(memberStatus)
                .setMembershipRenewal(membershipRenewal)
                .build();
    }
}