package com.odigeo.membership.booking;

import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;

import java.util.Calendar;

public class SearchBookingsRequestBuilder {

    private Calendar initialBookingDate;
    private Calendar finalBookingDate;
    private String locator;
    private String buyerEmail;
    private String travellerName;
    private String clientBookingReferenceId;
    private Boolean includeOrders;
    private String buyerPhoneNumber;
    private Calendar initialLastArrivalDate;
    private Calendar finalLastArrivalDate;
    private String creditCard;
    private Integer offsetPage;
    private Integer maxNumBookingsByPage;
    private String websiteCode;
    private String brandCode;
    private Long membershipId;
    private Long memberAccountId;
    private String bookingSubscriptionPrime;


    public SearchBookingsRequest build() {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequest();
        searchBookingsRequest.setInitialBookingDate(this.getInitialBookingDate());
        searchBookingsRequest.setFinalBookingDate(this.getFinalBookingDate());
        searchBookingsRequest.setLocator(this.getLocator());
        searchBookingsRequest.setBuyerEmail(this.getBuyerEmail());
        searchBookingsRequest.setTravellerName(this.getTravellerName());
        searchBookingsRequest.setClientBookingReferenceId(this.getClientBookingReferenceId());
        searchBookingsRequest.setIncludeOrders(this.getIncludeOrders());
        searchBookingsRequest.setBuyerPhoneNumber(this.getBuyerPhoneNumber());
        searchBookingsRequest.setInitialLastArrivalDate(this.getInitialLastArrivalDate());
        searchBookingsRequest.setFinalLastArrivalDate(this.getFinalLastArrivalDate());
        searchBookingsRequest.setCreditCard(this.getCreditCard());
        searchBookingsRequest.setOffsetPage(this.getOffsetPage());
        searchBookingsRequest.setMaxNumBookingsByPage(this.getMaxNumBookingsByPage());
        searchBookingsRequest.setWebsiteCode(this.getWebsiteCode());
        searchBookingsRequest.setBrandCode(this.getBrandCode());
        searchBookingsRequest.setMembershipId(this.getMembershipId());
        searchBookingsRequest.setMemberAccountId(this.getMemberAccountId());
        searchBookingsRequest.setIsBookingSubscriptionPrime(this.bookingSubscriptionPrime);
        return searchBookingsRequest;
    }

    public Calendar getInitialBookingDate() {
        return initialBookingDate;
    }

    public SearchBookingsRequestBuilder setInitialBookingDate(Calendar initialBookingDate) {
        this.initialBookingDate = initialBookingDate;
        return this;
    }

    public Calendar getFinalBookingDate() {
        return finalBookingDate;
    }

    public SearchBookingsRequestBuilder setFinalBookingDate(Calendar finalBookingDate) {
        this.finalBookingDate = finalBookingDate;
        return this;
    }

    public String getLocator() {
        return locator;
    }

    public SearchBookingsRequestBuilder setLocator(String locator) {
        this.locator = locator;
        return this;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public SearchBookingsRequestBuilder setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
        return this;
    }

    public String getTravellerName() {
        return travellerName;
    }

    public SearchBookingsRequestBuilder setTravellerName(String travellerName) {
        this.travellerName = travellerName;
        return this;
    }

    public String getClientBookingReferenceId() {
        return clientBookingReferenceId;
    }

    public SearchBookingsRequestBuilder setClientBookingReferenceId(String clientBookingReferenceId) {
        this.clientBookingReferenceId = clientBookingReferenceId;
        return this;
    }

    public Boolean getIncludeOrders() {
        return includeOrders;
    }

    public SearchBookingsRequestBuilder setIncludeOrders(Boolean includeOrders) {
        this.includeOrders = includeOrders;
        return this;
    }

    public String getBuyerPhoneNumber() {
        return buyerPhoneNumber;
    }

    public SearchBookingsRequestBuilder setBuyerPhoneNumber(String buyerPhoneNumber) {
        this.buyerPhoneNumber = buyerPhoneNumber;
        return this;
    }

    public Calendar getInitialLastArrivalDate() {
        return initialLastArrivalDate;
    }

    public SearchBookingsRequestBuilder setInitialLastArrivalDate(Calendar initialLastArrivalDate) {
        this.initialLastArrivalDate = initialLastArrivalDate;
        return this;
    }

    public Calendar getFinalLastArrivalDate() {
        return finalLastArrivalDate;
    }

    public SearchBookingsRequestBuilder setFinalLastArrivalDate(Calendar finalLastArrivalDate) {
        this.finalLastArrivalDate = finalLastArrivalDate;
        return this;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public SearchBookingsRequestBuilder setCreditCard(String creditCard) {
        this.creditCard = creditCard;
        return this;
    }

    public Integer getOffsetPage() {
        return offsetPage;
    }

    public SearchBookingsRequestBuilder setOffsetPage(Integer offsetPage) {
        this.offsetPage = offsetPage;
        return this;
    }

    public Integer getMaxNumBookingsByPage() {
        return maxNumBookingsByPage;
    }

    public SearchBookingsRequestBuilder setMaxNumBookingsByPage(Integer maxNumBookingsByPage) {
        this.maxNumBookingsByPage = maxNumBookingsByPage;
        return this;
    }

    public String getWebsiteCode() {
        return websiteCode;
    }

    public SearchBookingsRequestBuilder setWebsiteCode(String websiteCode) {
        this.websiteCode = websiteCode;
        return this;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public SearchBookingsRequestBuilder setBrandCode(String brandCode) {
        this.brandCode = brandCode;
        return this;
    }

    public Long getMembershipId() {
        return membershipId;
    }

    public SearchBookingsRequestBuilder setMembershipId(Long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public SearchBookingsRequestBuilder setMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public String getBookingSubscriptionPrime() {
        return bookingSubscriptionPrime;
    }

    public SearchBookingsRequestBuilder setBookingSubscriptionPrime(String bookingSubscriptionPrime) {
        this.bookingSubscriptionPrime = bookingSubscriptionPrime;
        return this;
    }
}
