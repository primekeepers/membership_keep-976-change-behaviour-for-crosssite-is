package com.odigeo.membership.parameters;


public class UserIdentification {

    private Long userId;
    private BrandedEmail userBrandedEmail;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public BrandedEmail getUserBrandedEmail() {
        return userBrandedEmail;
    }

    public void setUserBrandedEmail(BrandedEmail userBrandedEmail) {
        this.userBrandedEmail = userBrandedEmail;
    }
}
