package com.odigeo.messaging.kafka.messageconsumers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.Consumer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.commons.messaging.domain.events.DomainEventDeserializer;
import com.odigeo.messaging.kafka.MembershipProductClosureProcessor;
import com.odigeo.shoppingbasket.checkout.messages.ProductClosureRequested;
import org.apache.log4j.Logger;

import static java.util.Objects.nonNull;

@Singleton
public class MembershipProductClosureConsumer {

    private static final Logger LOGGER = Logger.getLogger(MembershipProductClosureConsumer.class);
    public static final String PRODUCT_CLOSURE_TOPIC = "PRODUCT_CLOSURE_v1";
    public static final int THREADS_COUNT = 1;

    private final MembershipProductClosureProcessor membershipProductClosureProcessor;
    private final Consumer<DomainEvent<ProductClosureRequested>> consumer;

    @Inject
    public MembershipProductClosureConsumer(MembershipProductClosureProcessor membershipProductClosureProcessor) {
        this.membershipProductClosureProcessor = membershipProductClosureProcessor;
        consumer = new KafkaConsumer<>(
                new DomainEventDeserializer<>(ProductClosureRequested.getClassSchema()), PRODUCT_CLOSURE_TOPIC);
    }

    public void launch() {
        consumer.start(membershipProductClosureProcessor, THREADS_COUNT);
    }

    public void shutdown() {
        if (nonNull(consumer)) {
            try {
                consumer.shutdown();
            } catch (InterruptedException e) {
                LOGGER.error("MembershipProductClosureConsumer shutdown interrupted", e);
                Thread.currentThread().interrupt();
            }
        }
    }
}

