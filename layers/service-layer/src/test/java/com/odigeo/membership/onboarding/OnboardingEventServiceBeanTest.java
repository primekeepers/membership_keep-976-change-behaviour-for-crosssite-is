package com.odigeo.membership.onboarding;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.DataAccessRollbackException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class OnboardingEventServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private OnboardingStore onboardingStore;

    @InjectMocks
    private OnboardingEventServiceBean onboardingEventServiceBean = new OnboardingEventServiceBean();

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(binder ->
                binder.bind(OnboardingStore.class).toInstance(onboardingStore)
        );
    }

    @Test
    public void testCreateOnboardingSuccessful() throws DataAccessException, SQLException {
        //Given
        long expectedOnboardingId = 742L;
        long memberAccountId = 8234L;
        OnboardingEvent event = OnboardingEvent.COMPLETED;
        OnboardingDevice device = OnboardingDevice.DESKTOP;
        Onboarding onboarding = new Onboarding.Builder()
                .withMemberAccountId(memberAccountId)
                .withDevice(device)
                .withEvent(event)
                .build();
        given(onboardingStore.createOnboarding(dataSource, memberAccountId, event, device)).willReturn(expectedOnboardingId);
        //When
        long createdOnboardingId = onboardingEventServiceBean.createOnboarding(onboarding);
        //Then
        assertEquals(createdOnboardingId, expectedOnboardingId);
    }

    @Test(expectedExceptions = DataAccessRollbackException.class, expectedExceptionsMessageRegExp = "There was an error trying to create a new onboarding.*")
    public void testCreateOnboardingFailed() throws SQLException, DataAccessException {
        //Given
        long memberAccountId = 492L;
        OnboardingEvent event = OnboardingEvent.COMPLETED;
        OnboardingDevice device = OnboardingDevice.APP;
        Onboarding onboarding = new Onboarding.Builder()
                .withMemberAccountId(memberAccountId)
                .withDevice(device)
                .withEvent(event)
                .build();
        given(onboardingStore.createOnboarding(dataSource, memberAccountId, event, device)).willThrow(new SQLException("expected exception"));
        //When
        onboardingEventServiceBean.createOnboarding(onboarding);
    }
}