package com.odigeo.membership;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.util.JsonDateDeserializer;
import com.odigeo.membership.util.JsonDateSerializer;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Membership implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Membership.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Long id;
    private final String website;
    private final MemberStatus status;
    private final MembershipRenewal autoRenewal;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    private final LocalDateTime activationDate;

    private final Long feeContainerId;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    private LocalDateTime expirationDate;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    private final LocalDateTime timestamp;

    private Boolean isBookingLimitReached;
    private final long memberAccountId;
    private BigDecimal balance;
    private final int monthsDuration;
    private final TimeUnit durationTimeUnit;
    private final Integer duration;
    private final MembershipType membershipType;
    private final SourceType sourceType;
    private final ProductStatus productStatus;
    private final BigDecimal totalPrice;
    private final BigDecimal renewalPrice;
    private final Integer renewalDuration;
    private final String currencyCode;
    private final String recurringId;
    private final List<MemberStatusAction> memberStatusActions;
    private final List<MembershipRecurring> membershipRecurring;
    private final UUID recurringCollectionId;
    private final MemberAccount memberAccount;
    private final Boolean remindMeLater;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    private final LocalDateTime remindMeLaterLastUpdate;

    @JsonIgnore
    public Membership(MembershipBuilder membershipBuilder) {
        this.id = membershipBuilder.getId();
        this.website = membershipBuilder.getWebsite();
        this.status = membershipBuilder.getStatus();
        this.autoRenewal = membershipBuilder.getMembershipRenewal();
        this.activationDate = membershipBuilder.getActivationDate();
        this.expirationDate = membershipBuilder.getExpirationDate();
        this.memberAccountId = membershipBuilder.getMemberAccountId();
        this.membershipType = membershipBuilder.getMembershipType();
        this.monthsDuration = membershipBuilder.getMonthsDuration();
        this.durationTimeUnit = membershipBuilder.getDurationTimeUnit();
        this.duration = membershipBuilder.getDuration();
        this.balance = membershipBuilder.getBalance();
        this.sourceType = membershipBuilder.getSourceType();
        this.productStatus = membershipBuilder.getProductStatus();
        MembershipPrices membershipPrices = membershipBuilder.getMembershipPricesBuilder().build();
        this.totalPrice = membershipPrices.getTotalPrice();
        this.renewalPrice = membershipPrices.getRenewalPrice();
        this.renewalDuration = membershipBuilder.getRenewalDuration();
        this.currencyCode = membershipPrices.getCurrencyCode();
        this.recurringId = membershipBuilder.getRecurringId();
        this.recurringCollectionId = membershipBuilder.getRecurringCollectionId();
        this.timestamp = membershipBuilder.getTimestamp();
        this.memberStatusActions = Optional.ofNullable(membershipBuilder.getMemberStatusActions()).orElseGet(ArrayList::new);
        this.membershipRecurring = Optional.ofNullable(membershipBuilder.getMembershipRecurring()).orElseGet(ArrayList::new);
        this.memberAccount = membershipBuilder.getMemberAccount();
        this.feeContainerId = membershipBuilder.getFeeContainerId();
        this.remindMeLater = membershipBuilder.getRemindMeLater();
        this.remindMeLaterLastUpdate = membershipBuilder.getRemindMeLaterLastUpdate();
    }

    public static MembershipBuilder builder() {
        return new MembershipBuilder();
    }

    @SuppressWarnings("PMD.ExcessiveParameterList")
    @JsonCreator
    private Membership(@JsonProperty("id") Long id,
                       @JsonProperty("website") String website,
                       @JsonProperty("status") MemberStatus status,
                       @JsonProperty("autoRenewal") MembershipRenewal autoRenewal,
                       @JsonProperty("activationDate") LocalDateTime activationDate,
                       @JsonProperty("feeContainerId") Long feeContainerId,
                       @JsonProperty("expirationDate") LocalDateTime expirationDate,
                       @JsonProperty("timestamp") LocalDateTime timestamp,
                       @JsonProperty("isBookingLimitReached") Boolean isBookingLimitReached,
                       @JsonProperty("memberAccountId") long memberAccountId,
                       @JsonProperty("balance") BigDecimal balance,
                       @JsonProperty("monthsDuration") int monthsDuration,
                       @JsonProperty("durationTimeUnit") TimeUnit durationTimeUnit,
                       @JsonProperty("duration") Integer duration,
                       @JsonProperty("membershipType") MembershipType membershipType,
                       @JsonProperty("sourceType") SourceType sourceType,
                       @JsonProperty("productStatus") ProductStatus productStatus,
                       @JsonProperty("totalPrice") BigDecimal totalPrice,
                       @JsonProperty("renewalPrice") BigDecimal renewalPrice,
                       @JsonProperty("renewalDuration") Integer renewalDuration,
                       @JsonProperty("currencyCode") String currencyCode,
                       @JsonProperty("recurringId") String recurringId,
                       @JsonProperty("recurringCollectionId") UUID recurringCollectionId,
                       @JsonProperty("memberStatusActions") List<MemberStatusAction> memberStatusActions,
                       @JsonProperty("membershipRecurring") List<MembershipRecurring> membershipRecurring,
                       @JsonProperty("memberAccount") MemberAccount memberAccount,
                       @JsonProperty("remindMeLater") Boolean remindMeLater,
                       @JsonProperty("remindMeLaterLastUpdate") LocalDateTime remindMeLaterLastUpdate) {
        this.id = id;
        this.website = website;
        this.status = status;
        this.autoRenewal = autoRenewal;
        this.activationDate = activationDate;
        this.expirationDate = expirationDate;
        this.memberAccountId = memberAccountId;
        this.membershipType = membershipType;
        this.monthsDuration = monthsDuration;
        this.durationTimeUnit = durationTimeUnit;
        this.duration = duration;
        this.balance = balance;
        this.sourceType = sourceType;
        this.productStatus = productStatus;
        this.totalPrice = totalPrice;
        this.renewalPrice = renewalPrice;
        this.renewalDuration = renewalDuration;
        this.currencyCode = currencyCode;
        this.recurringId = recurringId;
        this.recurringCollectionId = recurringCollectionId;
        this.timestamp = timestamp;
        this.isBookingLimitReached = isBookingLimitReached;
        this.memberStatusActions = Optional.ofNullable(memberStatusActions).orElseGet(ArrayList::new);
        this.membershipRecurring = Optional.ofNullable(membershipRecurring).orElseGet(ArrayList::new);
        this.memberAccount = memberAccount;
        this.feeContainerId = feeContainerId;
        this.remindMeLater = remindMeLater;
        this.remindMeLaterLastUpdate = remindMeLaterLastUpdate;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public Integer getRenewalDuration() {
        return renewalDuration;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public ProductStatus getProductStatus() {
        return this.productStatus;
    }

    public Boolean getIsActive() {
        return (status == MemberStatus.ACTIVATED);
    }

    public Boolean getBookingLimitReached() {
        return isBookingLimitReached;
    }

    public Membership setBookingLimitReached(Boolean bookingLimitReached) {
        isBookingLimitReached = bookingLimitReached;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getWebsite() {
        return website;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public MembershipRenewal getAutoRenewal() {
        return autoRenewal;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public TimeUnit getDurationTimeUnit() {
        return durationTimeUnit;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public UUID getRecurringCollectionId() {
        return recurringCollectionId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<MemberStatusAction> getMemberStatusActions() {
        return memberStatusActions;
    }

    public List<MembershipRecurring> getMembershipRecurring() {
        return membershipRecurring;
    }

    public MemberAccount getMemberAccount() {
        return memberAccount;
    }

    public Long getFeeContainerId() {
        return feeContainerId;
    }

    public Boolean getRemindMeLater() {
        return remindMeLater;
    }

    public LocalDateTime getRemindMeLaterLastUpdate() {
        return remindMeLaterLastUpdate;
    }

    public static Membership valueOf(String jsonQueryParam) {
        try {
            return OBJECT_MAPPER.readValue(jsonQueryParam, Membership.class);
        } catch (IOException e) {
            LOGGER.error("Invalid jsonQueryParameter : {}", jsonQueryParam, e);
            throw new RuntimeJsonMappingException("Invalid Membership jsonQueryParameter : {}",
                    new JsonMappingException("Impossible parsing Membership to jsonQueryParameter", e));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Membership) || getClass() != o.getClass()) {
            return false;
        }

        Membership that = (Membership) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id)
                .toHashCode();
    }

    @Override
    public String toString() {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (IOException e) {
            LOGGER.error("Impossible convert to jsonQueryParameter", e);
            throw new RuntimeJsonMappingException("Error Parsing Membership",
                    new JsonMappingException("Impossible parsing Membership to jsonQueryParameter", e));
        }
    }

    public boolean isRenewable() {
        return MemberStatus.ACTIVATED.equals(this.getStatus()) && MembershipRenewal.ENABLED.equals(this.getAutoRenewal());
    }
}
