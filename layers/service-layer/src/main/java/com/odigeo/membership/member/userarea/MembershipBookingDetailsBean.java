package com.odigeo.membership.member.userarea;

import com.google.inject.Inject;
import com.odigeo.bookingapi.BookingApiDAO;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.responses.BookingBasicInfo;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

class MembershipBookingDetailsBean {

    private static final Logger LOGGER = Logger.getLogger(MembershipBookingDetailsBean.class);

    private final BookingApiDAO bookingApiDAO;
    private final MembershipNoSQLManager membershipNoSQLManager;

    @Inject
    MembershipBookingDetailsBean(final BookingApiDAO bookingApiDAO,
                                 final MembershipNoSQLManager membershipNoSQLManager) {
        this.bookingApiDAO = bookingApiDAO;
        this.membershipNoSQLManager = membershipNoSQLManager;
    }

    Optional<BookingDetail> getBookingDetailSubscriptionFromMembershipId(long membershipId) throws BookingApiException {
        Optional<Long> subscriptionBookingIdByMembership = getBookingId(membershipId);
        if (subscriptionBookingIdByMembership.isPresent()) {
            return Optional.ofNullable(bookingApiDAO.getBooking(subscriptionBookingIdByMembership.get()));
        }
        return Optional.empty();
    }

    private Optional<Long> getBookingId(long membershipId) {
        MetricsUtils.startTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_FETCH_REDIS), MetricsNames.METRICS_REGISTRY_NAME);
        Optional<Long> bookingId = membershipNoSQLManager.getBookingIdFromCache(membershipId);
        MetricsUtils.stopTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_FETCH_REDIS), MetricsNames.METRICS_REGISTRY_NAME);
        if (bookingId.isPresent()) {
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.MEMORYSTORE_HIT_CACHE_SUCCESS), MetricsNames.METRICS_REGISTRY_NAME);
        } else {
            List<BookingSummary> primeBookings = bookingApiDAO.searchPrimeBookingsByMembershipId(membershipId);
            bookingId = getFirstBookingIdFromBookings(primeBookings);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.MEMORYSTORE_HIT_CACHE_FAILURE), MetricsNames.METRICS_REGISTRY_NAME);
        }
        return bookingId;
    }

    List<BookingDetail> getBookingDetailsByMembershipId(final long membershipId) {
        return bookingApiDAO.searchBookingsByMembershipId(membershipId).stream()
                .map(BookingSummary::getBookingBasicInfo)
                .map(BookingBasicInfo::getId)
                .map(bookingDetailFromBookingId())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }

    private Function<Long, Optional<BookingDetail>> bookingDetailFromBookingId() {
        return bookingId -> {
            try {
                return Optional.ofNullable(bookingApiDAO.getBooking(bookingId));
            } catch (BookingApiException e) {
                LOGGER.error("Cannot retrieve booking details for booking id: " + bookingId, e);
            }
            return Optional.empty();
        };
    }

    private Optional<Long> getFirstBookingIdFromBookings(final List<BookingSummary> bookings) {
        return bookings.stream()
                .findFirst()
                .map(BookingSummary::getBookingBasicInfo)
                .map(BookingBasicInfo::getId);
    }
}
