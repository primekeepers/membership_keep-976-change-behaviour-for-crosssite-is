package com.odigeo.messaging.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.propertiesconfig.PropertiesCacheService;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.kafka.messagepublishers.MembershipSubscriptionPublisher;
import com.odigeo.messaging.kafka.messagepublishers.MembershipUpdatePublisher;
import com.odigeo.messaging.kafka.messagepublishers.WelcomeToPrimePublisher;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import com.odigeo.visitengineapi.v1.multitest.TestDimensionPartitionService;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class MembershipKafkaSenderBeanTest {

    private static final String EMAIL = "test@gmail.com";
    private static final VerificationMode TWO_TIMES = times(2);

    @Mock
    private MembershipSubscriptionMessage membershipSubscriptionMessage;
    @Mock
    private MembershipUpdateMessage membershipUpdateMessage;
    @Mock
    private MembershipMailerMessage membershipMailerMessage;
    @Mock
    private MembershipSubscriptionPublisher membershipSubscriptionPublisher;
    @Mock
    private MembershipUpdatePublisher membershipUpdatePublisher;
    @Mock
    private WelcomeToPrimePublisher welcomeToPrimePublisher;
    @Mock
    private Appender mockAppender;
    @Mock
    private TestDimensionPartitionService TestDimensionPartitionServiceMock;
    @Mock
    private TestAssignmentService testAssignmentServiceMock;
    @Mock
    private PropertiesCacheService propertiesCacheServiceMock;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    private MembershipKafkaSenderBean membershipKafkaSenderBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        membershipKafkaSenderBean = new MembershipKafkaSenderBean(membershipSubscriptionPublisher, membershipUpdatePublisher, welcomeToPrimePublisher, propertiesCacheServiceMock);
        ConfigurationEngine.init(
                binder -> binder.bind(TestDimensionPartitionService.class).toInstance(TestDimensionPartitionServiceMock),
                binder -> binder.bind(TestAssignmentService.class).toInstance(testAssignmentServiceMock));
    }

    @AfterMethod
    public void tearDown() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    @Test
    public void testSendMembershipSubscriptionMessageToKafka() throws PublishMessageException {
        membershipKafkaSenderBean.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
        membershipKafkaSenderBean.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
        verify(membershipSubscriptionPublisher, TWO_TIMES).publishMembershipSubscriptionMessage(eq(membershipSubscriptionMessage), any());
    }

    @Test(expectedExceptions = PublishMessageException.class)
    public void testSendMembershipSubscriptionMessageToKafkaException() throws PublishMessageException {
        doThrow(new PublishMessageException(StringUtils.EMPTY, new Exception())).when(membershipSubscriptionPublisher).publishMembershipSubscriptionMessage(eq(membershipSubscriptionMessage), any(Callback.class));
        membershipKafkaSenderBean.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
    }

    @Test
    public void testSendMembershipUpdateMessageToKafkaNotSentWhenNotActive() {
        when(propertiesCacheServiceMock.isSendingIdsToKafkaActive()).thenReturn(false);
        membershipKafkaSenderBean.sendMembershipUpdateMessageToKafka(membershipUpdateMessage);
        verifyNoInteractions(membershipUpdatePublisher);
    }

    @Test
    public void testSendMembershipUpdateMessageToKafkaSentWhenActive() {
        when(propertiesCacheServiceMock.isSendingIdsToKafkaActive()).thenReturn(true);
        membershipKafkaSenderBean.sendMembershipUpdateMessageToKafka(membershipUpdateMessage);
        verify(membershipUpdatePublisher).publishMembershipUpdateMessage(eq(membershipUpdateMessage), any());
    }

    @Test
    public void testSendMembershipMailerMessageToKafkaNotSentWhenNotActive() {
        when(propertiesCacheServiceMock.isTransactionalWelcomeEmailActive()).thenReturn(false);
        membershipKafkaSenderBean.sendMembershipMailerMessageToKafka(membershipMailerMessage, false);
        verifyNoInteractions(welcomeToPrimePublisher);
    }

    @Test
    public void testSendMembershipMailerMessageToKafkaSentWhenActive() {
        when(propertiesCacheServiceMock.isTransactionalWelcomeEmailActive()).thenReturn(true);
        membershipKafkaSenderBean.sendMembershipMailerMessageToKafka(membershipMailerMessage, false);
        verify(welcomeToPrimePublisher).publishMembershipMailerMessage(any(MembershipMailerMessage.class), any());
    }

    @Test
    public void testSendMembershipMailerMessageToKafkaSentWhenInactiveAndForced() {
        when(propertiesCacheServiceMock.isTransactionalWelcomeEmailActive()).thenReturn(false);
        membershipKafkaSenderBean.sendMembershipMailerMessageToKafka(membershipMailerMessage, true);
        verify(welcomeToPrimePublisher).publishMembershipMailerMessage(any(MembershipMailerMessage.class), any());
    }

    @Test
    public void testPublishMessageCallbackProcessorNonNullException() {
        String exceptionMessage = "impossible to send message";
        Exception e = new Exception(exceptionMessage);
        membershipKafkaSenderBean.publishMessageCallbackProcessor(MembershipKafkaSenderBean.SUBSCRIPTION_MESSAGE_SENT_OK, EMAIL, MetricsNames.FAILED_SUBSCRIPTION_MSG, e);
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertEquals(exceptionMessage, loggingEvent.getMessage());
        assertEquals(Level.ERROR, loggingEvent.getLevel());
    }

    @Test
    public void testPublishMessageCallbackProcessorNullException() {
        membershipKafkaSenderBean.publishMessageCallbackProcessor(MembershipKafkaSenderBean.SUBSCRIPTION_MESSAGE_SENT_OK, EMAIL, MetricsNames.SUCCESS_SUBSCRIPTION_MSG, null);
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertEquals(loggingEvent.getMessage(), MembershipKafkaSenderBean.SUBSCRIPTION_MESSAGE_SENT_OK.replace("{}", EMAIL));
        assertEquals(Level.INFO, loggingEvent.getLevel());
    }
}
