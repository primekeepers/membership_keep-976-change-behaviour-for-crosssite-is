package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.member.AbstractServiceBean;
import com.odigeo.membership.member.MemberServiceUtils;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.parameters.MembershipCreation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;


@Stateless
@Local(MembershipCreationService.class)
@TransactionAttribute
public class MembershipCreationServiceBean extends AbstractServiceBean implements MembershipCreationService {
    private MembershipCreationFactoryProvider membershipCreationFactoryProvider;
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipCreationServiceBean.class);

    @Override
    public Optional<Membership> activate(long membershipId, BigDecimal balance) throws DataAccessException, SQLException, MissingElementException {
        MembershipStore membershipStore = getMembershipStore();
        Membership membership = Optional.ofNullable(membershipStore.fetchMembershipByIdWithMemberAccount(dataSource, membershipId))
                .orElseThrow(() -> new MissingElementException("Membership id not found in database: " + membershipId));
        final boolean isMembershipPendingToActivate = MemberStatus.PENDING_TO_ACTIVATE.equals(membership.getStatus());
        final boolean userHaveActiveMembershipForWebsite = userHaveActiveMembershipForWebsite(membership);
        if (!isMembershipPendingToActivate || userHaveActiveMembershipForWebsite) {
            return Optional.empty();
        }

        LocalDateTime activationDate = LocalDateTime.now();
        boolean success = membershipStore.activateMember(dataSource, membership.getId(), activationDate, MembershipDateCalculator.INSTANCE.getExpirationDate(membership), balance);
        if (!success) {
            return Optional.empty();
        }
        LOGGER.info("Membership activated for membershipId {}", membership.getId());
        getMemberStatusActionStore().createMemberStatusAction(dataSource, membership.getId(), StatusAction.ACTIVATION);
        Membership membershipActivated = membershipStore.fetchMembershipByIdWithMemberAccount(dataSource, membershipId);
        return Optional.of(membershipActivated);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Long create(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException {
        if (membershipCreation.getMemberAccountCreation().getUserId() == null && membershipCreation.getMemberAccountId() == null) {
            membershipCreation.getMemberAccountCreation().setUserId(getUserService().saveUser(membershipCreation.getUserCreation()));
        }
        MembershipCreationServiceProvider membershipCreationFactory = getMembershipCreationFactoryProvider().getInstance(membershipCreation);
        return membershipCreationFactory.createMembership(dataSource, membershipCreation);
    }

    private boolean userHaveActiveMembershipForWebsite(Membership membership) throws DataAccessException {
        return getMemberAccountService().getActiveMembersByUserId(membership.getMemberAccount().getUserId()).stream()
                .filter(MemberServiceUtils.websiteCheck(membership.getWebsite()))
                .flatMap(memberAccount -> memberAccount.getMemberships().stream())
                .anyMatch(Membership::getIsActive);
    }

    private MembershipCreationFactoryProvider getMembershipCreationFactoryProvider() {
        return Optional.ofNullable(membershipCreationFactoryProvider).orElseGet(() -> {
            membershipCreationFactoryProvider = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class);
            return membershipCreationFactoryProvider;
        });
    }

}
