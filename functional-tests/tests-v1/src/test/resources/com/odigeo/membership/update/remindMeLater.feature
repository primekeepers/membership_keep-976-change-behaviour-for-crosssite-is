Feature: Test setting remind me later flag

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   |
      | 321      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     | 54.99   |
    And property to send IDs to Kafka has value true in db:

  Scenario Outline: Set remind me later flag for an activated membership
    Given the user-api has the following user info
      | userId   | status       | email       | brand | token                    | tokenType        |
      | 4321     | ACTIVE       | foo@bar.com | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When a request to remind me later is received for membershipId 123 set to <rmlFlag>
    Then the membershipId 123 has the remind me later flag set to <rmlFlag>
    And membershipSubscriptionMessage is correctly sent to kafka queue with remind me later set to <rmlFlag>
    Examples:
      | rmlFlag |
      | true    |
      | false   |

  Scenario: Attempt to set remind me later flag for a non-activated membership
    Given the user-api has the following user info
      | userId   | status       | email       | brand | token                    | tokenType        |
      | 1234     | ACTIVE       | foo@bar.com | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When a request to remind me later is received for membershipId 321 set to true
    Then the membershipId 321 has the remind me later flag not set
    And no membershipSubscriptionMessage is sent to kafka
