package com.odigeo.membership.member.bootstrap;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Module;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.membership.member.bootstrap.warmup.MembershipControllersWarmUp;
import com.odigeo.membership.member.monitoring.redis.RedisHealthCheck;
import com.odigeo.messaging.kafka.messageconsumers.MembershipProductClosureConsumer;
import org.apache.log4j.PropertyConfigurator;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class ContextListenerTest {

    private static final int EXPECTED_MODULES_NUMBER = 17;
    private ContextListener contextListener;

    @Mock
    private ServletContextEvent servletContextEventMock;
    @Mock
    private ServletContext servletContext;
    @Mock
    private MembershipProductClosureConsumer mockMembershipProductClosureConsumer;
    @Mock
    private HealthCheck healthCheck;
    @Mock
    private MembershipControllersWarmUp membershipControllersWarmUp;

    private MockedStatic<ConfigurationEngine> configurationEngine;
    private MockedStatic<PropertyConfigurator> propertyConfigurator;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        configurationEngineMock();
        when(servletContextEventMock.getServletContext()).thenReturn(servletContext);
        contextListener = new ContextListener();
    }

    private void configurationEngineMock() {
        configurationEngine.when(() -> ConfigurationEngine.getInstance(RedisHealthCheck.class)).thenReturn(healthCheck);
        configurationEngine.when(() -> ConfigurationEngine.getInstance(MembershipProductClosureConsumer.class)).thenReturn(mockMembershipProductClosureConsumer);
        configurationEngine.when(() -> ConfigurationEngine.getInstance(MembershipControllersWarmUp.class)).thenReturn(membershipControllersWarmUp);
        propertyConfigurator.when(() -> PropertyConfigurator.configureAndWatch(anyString(), anyLong())).then(a -> null);
//        configurationEngine.when(() -> ConfigurationEngine.getInstance(SearchService.class)).thenReturn(searchService);
    }

    @BeforeClass
    public void beforeClass() {
        configurationEngine = Mockito.mockStatic(ConfigurationEngine.class);
        propertyConfigurator = Mockito.mockStatic(PropertyConfigurator.class);
    }

    @AfterClass
    public void afterClass() {
        configurationEngine.close();
        propertyConfigurator.close();
    }

    @Test
    public void testContextDestroyed() {
        try {
            this.contextListener.contextDestroyed(servletContextEventMock);
        } catch (final Exception e) {
            fail();
        }
    }

    @Test
    public void testAssembleModulesList() {
        final List<Module> receivedList = contextListener.assembleModulesList(mock(AutoHealthCheckRegister.class));
        assertEquals(receivedList.size(), EXPECTED_MODULES_NUMBER);
    }

    @Test
    public void testContextInitialized() {
        contextListener.contextInitialized(servletContextEventMock);
        verify(servletContext).setAttribute(anyString(), any(HealthCheckRegistry.class));
        verify(servletContext).setAttribute(anyString(), any(DumpStateRegistry.class));
        verify(servletContext, times(5)).log(anyString());
        verifyNoMoreInteractions(servletContext);
    }
}
