package com.odigeo.messaging;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import com.odigeo.shoppingbasket.checkout.messages.ProductClosureRequested;
import com.odigeo.shoppingbasket.checkout.messages.ProductReference;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipProductClosureServiceTest {

    @Mock
    private MembershipNoSQLManager membershipNoSQLManager;

    @Mock
    private MembershipRecurringStore recurringStore;

    @Mock
    private MembershipStore membershipStore;

    @InjectMocks
    private MembershipProductClosureService service = new MembershipProductClosureService();

    private static long MEMBERSHIP_ID = 1L;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
    }

    @Test
    public void successfullyCloseMembership() throws SQLException, DataAccessException, MissingElementException {
        Membership ptcMembership = createPendingMembership();
        noSqlManagerReturnsAMembership(ptcMembership);
        Long membershipNewId = 3L;
        when(membershipStore.createMember(any(), any())).thenReturn(membershipNewId);
        ProductClosureRequested productClosureRequested = ProductClosureRequested.newBuilder()
                .setProductReference(ProductReference.newBuilder().setReference(String.valueOf(MEMBERSHIP_ID)).setType("MEMBERSHIP").build()).build();
        service.closeMembership(productClosureRequested);
        verify(membershipNoSQLManager).get(String.valueOf(MEMBERSHIP_ID));
        verify(membershipStore).createMember(any(), any());
        verify(recurringStore).insertMembershipRecurringCollectionId(any(), eq(membershipNewId), eq(ptcMembership.getRecurringCollectionId()));
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void membershipNotFound() throws DataAccessException, MissingElementException {
        when(membershipNoSQLManager.get(String.valueOf(MEMBERSHIP_ID))).thenReturn(Optional.empty());
        ProductClosureRequested productClosureRequested = ProductClosureRequested.newBuilder()
                .setProductReference(ProductReference.newBuilder().setReference(String.valueOf(2L)).setType("MEMBERSHIP").build()).build();
        service.closeMembership(productClosureRequested);
        verifyNoInteractions(membershipStore);
        verifyNoInteractions(recurringStore);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void simulatingDBIsDown() throws SQLException, DataAccessException, MissingElementException {
        Membership ptcMembership = createPendingMembership();
        noSqlManagerReturnsAMembership(ptcMembership);
        when(membershipStore.createMember(any(), any())).thenThrow(new SQLException());
        ProductClosureRequested productClosureRequested = ProductClosureRequested.newBuilder()
                .setProductReference(ProductReference.newBuilder().setReference(String.valueOf(MEMBERSHIP_ID)).setType("MEMBERSHIP").build()).build();
        service.closeMembership(productClosureRequested);
        verify(membershipNoSQLManager).get(String.valueOf(MEMBERSHIP_ID));
        verify(membershipStore).createMember(any(), any());
        verifyNoInteractions(recurringStore);
    }

    private void noSqlManagerReturnsAMembership(Membership membership) {
        when(membershipNoSQLManager.get(String.valueOf(MEMBERSHIP_ID))).thenReturn(Optional.of(membership));
    }

    private static Membership createPendingMembership() {
        BigDecimal price = new BigDecimal("52.00");
        return new MembershipBuilder().setId(MEMBERSHIP_ID)
                .setWebsite("ES")
                .setMemberAccountId(2L)
                .setMemberAccount(new MemberAccount(2L,
                       243L, "John", "Smith"))
                .setStatus(MemberStatus.PENDING_TO_ACTIVATE)
                .setMembershipRenewal(MembershipRenewal.ENABLED)
                .setExpirationDate(LocalDateTime.now().plusYears(1))
                .setBalance(price)
                .setProductStatus(ProductStatus.INIT)
                .setSourceType(SourceType.FUNNEL_BOOKING)
                .setMembershipType(MembershipType.BASIC)
                .setMonthsDuration(12)
                .setDuration(12)
                .setDurationTimeUnit(TimeUnit.MONTHS)
                .setMembershipPricesBuilder(MembershipPrices.builder()
                        .totalPrice(price)
                        .renewalPrice(price)
                        .currencyCode("EUR"))
                .setRecurringCollectionId(UUID.randomUUID())
                .setFeeContainerId(238L).build();
    }
}