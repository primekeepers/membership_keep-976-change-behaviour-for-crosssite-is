package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.creation.sql.BasicFreeMembershipService;
import com.odigeo.membership.member.creation.sql.NewMembershipSubscriptionService;
import com.odigeo.membership.member.creation.sql.PendingToCollectMembershipService;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

public class MembershipCreationServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private MembershipCreationFactoryProvider membershipCreationFactoryProviderMock;
    @Mock
    private NewMembershipSubscriptionService newMembershipSubscriptionCreationMock;
    @Mock
    private PendingToCollectMembershipService pendingToCollectMembershipCreationMock;
    @Mock
    private BasicFreeMembershipService basicFreeMembershipServiceCreationMock;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManagerMock;
    @Mock
    private UserService userService;

    @InjectMocks
    private final MembershipCreationServiceBean membershipCreationServiceBean = new MembershipCreationServiceBean();

    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final int DEFAULT_DURATION = 12;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final long USER_ID = 123L;
    private static final long MEMBER_ID1 = 10L;
    private static final long MEMBER_ID2 = 11L;
    private static final long MEMBER_ID4 = 13L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long BOOKING_ID = 800L;
    private static final String WEBSITE1 = "ES";
    private static final String MEMBER_NAME1 = "JOSE";
    private static final String MEMBER_SURNAME1 = "LUIS";
    private static final String TEST_EXCEPTION = "TestException";
    private static final String TEST_EMAIL = "test@edreams.com";

    private final MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
    private final Membership activeMembership1 = getMembership(MEMBER_ID1, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, null);
    private final Membership pendingMemberWithAccount = getMembership(MEMBER_ID4, WEBSITE1, MemberStatus.PENDING_TO_ACTIVATE, MembershipRenewal.DISABLED, MEMBER_ACCOUNT_ID1, memberAccount);
    private final Membership activeMembership1WithAccount = getMembership(MEMBER_ID1, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, memberAccount);
    private final MembershipCreation membershipCreation = new MembershipCreationBuilder()
            .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                    .userId(USER_ID)
                    .lastNames(MEMBER_SURNAME1)
                    .name(MEMBER_NAME1))
            .withWebsite(WEBSITE1).withMembershipType(MEMBERSHIP_TYPE).withSourceType(SourceType.POST_BOOKING).withSubscriptionPrice(BigDecimal.TEN)
            .withMemberAccountId(MEMBER_ACCOUNT_ID1).build();

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        when(membershipCreationFactoryProviderMock.getInstance(any(MembershipCreation.class))).thenCallRealMethod();
    }

    private void configure(Binder binder) {
        binder.bind(MemberAccountService.class).toInstance(memberAccountService);
        binder.bind(NewMembershipSubscriptionService.class).toInstance(newMembershipSubscriptionCreationMock);
        binder.bind(PendingToCollectMembershipService.class).toInstance(pendingToCollectMembershipCreationMock);
        binder.bind(BasicFreeMembershipService.class).toInstance(basicFreeMembershipServiceCreationMock);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManagerMock);
    }



    @Test
    public void testCreateMembershipNewSubscription() throws DataAccessException, MissingElementException {
        when(newMembershipSubscriptionCreationMock.createMembership(dataSource, membershipCreation)).thenReturn(MEMBER_ID1);
        long newMembershipId = membershipCreationServiceBean.create(membershipCreation);
        assertEquals(newMembershipId, MEMBER_ID1);
        verify(newMembershipSubscriptionCreationMock).createMembership(dataSource, membershipCreation);
    }

    @Test
    public void testCreateMembershipPendingToCollect() throws DataAccessException, MissingElementException {
        MembershipCreation createMembershipPendingToCollect = new MembershipCreationBuilder()
                .withMemberStatus(MemberStatus.PENDING_TO_COLLECT)
                .withMemberAccountId(MEMBER_ACCOUNT_ID1)
                .build();
        when(pendingToCollectMembershipCreationMock.createMembership(dataSource, createMembershipPendingToCollect)).thenReturn(MEMBER_ID2);
        long newMembershipId = membershipCreationServiceBean.create(createMembershipPendingToCollect);
        assertEquals(newMembershipId, MEMBER_ID2);
    }

    @Test
    public void testCreateMembershipBasicFree() throws DataAccessException, MissingElementException {
        MembershipCreation createMembershipBasicFree = new MembershipCreationBuilder()
                .withMembershipType(MembershipType.BASIC_FREE)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID))
                .build();
        when(basicFreeMembershipServiceCreationMock.createMembership(eq(dataSource), eq(createMembershipBasicFree))).thenReturn(MEMBER_ID2);
        long newMembershipId = membershipCreationServiceBean.create(createMembershipBasicFree);
        assertEquals(newMembershipId, MEMBER_ID2);
        verifyNoInteractions(userService);
    }

    @Test
    public void testCreateMembershipAndUserBasicFree() throws DataAccessException, MissingElementException {
        UserCreation userCreation = new UserCreation.Builder()
                .withEmail(TEST_EMAIL)
                .withLocale(Locale.ENGLISH.toString())
                .withTrafficInterfaceId(1)
                .build();
        MembershipCreation createMembershipBasicFree = new MembershipCreationBuilder()
                .withMembershipType(MembershipType.BASIC_FREE)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withUserCreation(userCreation)
                .build();
        when(basicFreeMembershipServiceCreationMock.createMembership(eq(dataSource), eq(createMembershipBasicFree))).thenReturn(MEMBER_ID2);
        long newMembershipId = membershipCreationServiceBean.create(createMembershipBasicFree);
        assertEquals(newMembershipId, MEMBER_ID2);
        verify(userService).saveUser(userCreation);
    }

    @Test
    public void testActivateMembership() throws DataAccessException, SQLException, MissingElementException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        assertNotNull(membershipCreationServiceBean.activate(MEMBER_ID1, BigDecimal.TEN));
        verify(memberStatusActionStore).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
    }

    @Test
    public void testNotActivateMembership() throws DataAccessException, SQLException, MissingElementException {
        //GIVEN
        setUpActivateMembershipStoreResponse(Boolean.FALSE);
        //WHEN
        Optional<Membership> result = membershipCreationServiceBean.activate(MEMBER_ID1, BigDecimal.TEN);
        //THEN
        assertFalse(result.isPresent());
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testActivateMemberSQLException() throws DataAccessException, SQLException, MissingElementException {
        setUpActivateMembershipStoreResponseException();
        membershipCreationServiceBean.activate(MEMBER_ID1, BigDecimal.TEN);
    }

    @Test
    public void testActivateMembershipSubscription() throws SQLException, DataAccessException, MissingElementException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        assertNotNull(membershipCreationServiceBean.activate(MEMBER_ID4, BigDecimal.TEN));

    }

    @Test
    public void testActivateMembership_AlreadyHasAnActiveMembership() throws DataAccessException, SQLException, MissingElementException {
        // GIVEN
        when(memberAccountService.getActiveMembersByUserId(anyLong())).thenReturn(Collections.singletonList(assembleMemberAcccountWithActiveMemberInES()));
        when(membershipStore.fetchMembershipByIdWithMemberAccount(eq(dataSource), anyLong())).thenReturn(pendingMemberWithAccount);
        // WHEN
        final Optional<Membership> membershipActivated = membershipCreationServiceBean.activate(MEMBER_ID1, BigDecimal.TEN);
        // THEN
        assertFalse(membershipActivated.isPresent());
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingManagerMock, never()).sendMembershipIdToMembershipReporter(anyLong());
        verify(membershipStore).fetchMembershipByIdWithMemberAccount(eq(dataSource), anyLong());
        verify(membershipMessageSendingManagerMock, never()).sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class));
        verify(membershipMessageSendingManagerMock, never()).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(any(Membership.class), anyLong(), eq(false));
    }

    private MemberAccount assembleMemberAcccountWithActiveMemberInES() {
        MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
        memberAccount.setMemberships(Collections.singletonList(activeMembership1));
        return memberAccount;
    }

    private void setUpActivateMembershipStoreResponse(final boolean isUpdated) throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipByIdWithMemberAccount(eq(dataSource), anyLong())).thenReturn(pendingMemberWithAccount).thenReturn(activeMembership1WithAccount);
        when(membershipStore.activateMember(any(DataSource.class), anyLong(), any(LocalDateTime.class), any(LocalDateTime.class), any(BigDecimal.class))).thenReturn(isUpdated);
    }

    private void setUpActivateMembershipStoreResponseException() throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipById(any(DataSource.class), anyLong())).thenThrow(new SQLException(TEST_EXCEPTION));
        when(membershipStore.activateMember(any(DataSource.class), anyLong(), any(LocalDateTime.class), any(LocalDateTime.class), any(BigDecimal.class))).thenThrow(new SQLException(TEST_EXCEPTION));
    }

    private static Membership getMembership(long memberId, String website, MemberStatus status, MembershipRenewal renewal, long memberAccountId, MemberAccount memberAccount) {
        MembershipBuilder builder = new MembershipBuilder().setId(memberId).setWebsite(website).setStatus(status)
                .setMembershipRenewal(renewal).setActivationDate(NOW).setMemberAccountId(memberAccountId)
                .setSourceType(SourceType.FUNNEL_BOOKING).setMemberAccount(memberAccount).setMonthsDuration(DEFAULT_DURATION);
        if (MemberStatus.ACTIVATED == status) {
            builder.setExpirationDate(NOW.plusMonths(DEFAULT_DURATION));
        }
        return builder.build();
    }
}
