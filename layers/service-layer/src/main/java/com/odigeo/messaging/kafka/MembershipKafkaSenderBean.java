package com.odigeo.messaging.kafka;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.propertiesconfig.PropertiesCacheService;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.kafka.messagepublishers.MembershipSubscriptionPublisher;
import com.odigeo.messaging.kafka.messagepublishers.MembershipUpdatePublisher;
import com.odigeo.messaging.kafka.messagepublishers.WelcomeToPrimePublisher;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.producer.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Singleton
public class MembershipKafkaSenderBean implements MembershipKafkaSender {

    @VisibleForTesting
    static final String SUBSCRIPTION_MESSAGE_SENT_OK = "MembershipKafkaSenderBean: MembershipSubscriptionMessage sent to marketing with email = {}";
    private static final String UPDATE_MESSAGE_SENT_OK = "MembershipKafkaSenderBean: MembershipUpdateMessage sent with membershipId = {}";
    private static final String WELCOME_TO_PRIME_MESSAGE_SENT_OK = "MembershipKafkaSenderBean: WelcomeToPrime MembershipMailerMessage sent with membershipId = {}";
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipKafkaSenderBean.class);

    private final MembershipSubscriptionPublisher membershipSubscriptionPublisher;
    private final MembershipUpdatePublisher membershipUpdatePublisher;
    private final WelcomeToPrimePublisher welcomeToPrimePublisher;
    private final PropertiesCacheService propertiesCacheService;

    @Inject
    public MembershipKafkaSenderBean(MembershipSubscriptionPublisher membershipSubscriptionPublisher, MembershipUpdatePublisher membershipUpdatePublisher, WelcomeToPrimePublisher welcomeToPrimePublisher,
                                     PropertiesCacheService propertiesCacheService) {
        this.membershipSubscriptionPublisher = membershipSubscriptionPublisher;
        this.membershipUpdatePublisher = membershipUpdatePublisher;
        this.welcomeToPrimePublisher = welcomeToPrimePublisher;
        this.propertiesCacheService = propertiesCacheService;
    }

    @Override
    public void sendMembershipSubscriptionMessageToKafka(MembershipSubscriptionMessage membershipSubscriptionMessage) throws PublishMessageException {
        LOGGER.info("sending MembershipSubscriptionMessage to Kafka for email = {}, website = {}", membershipSubscriptionMessage.getEmail(), membershipSubscriptionMessage.getWebsite());

        Callback membershipSubscriptionCallback = (recordMetadata, e) -> publishMessageCallbackProcessor(SUBSCRIPTION_MESSAGE_SENT_OK, membershipSubscriptionMessage.getEmail(),
                isNull(e) ? MetricsNames.SUCCESS_SUBSCRIPTION_MSG : MetricsNames.FAILED_SUBSCRIPTION_MSG, e);

        membershipSubscriptionPublisher.publishMembershipSubscriptionMessage(membershipSubscriptionMessage, membershipSubscriptionCallback);
    }

    @Override
    public void sendMembershipUpdateMessageToKafka(MembershipUpdateMessage membershipUpdateMessage) {
        if (propertiesCacheService.isSendingIdsToKafkaActive()) {
            LOGGER.info("sending MembershipUpdateMessage to Kafka for membershipId {}", membershipUpdateMessage.getMembershipId());
            Callback updateMembershipCallback = (recordMetadata, e) -> publishMessageCallbackProcessor(UPDATE_MESSAGE_SENT_OK, membershipUpdateMessage.getKey(),
                    isNull(e) ? MetricsNames.SUCCESS_UPDATE_MSG : MetricsNames.FAILED_UPDATE_MSG, e);

            membershipUpdatePublisher.publishMembershipUpdateMessage(membershipUpdateMessage, updateMembershipCallback);
        }
    }

    @Override
    public void sendMembershipMailerMessageToKafka(MembershipMailerMessage membershipMailerMessage, boolean force) {
        if (force || propertiesCacheService.isTransactionalWelcomeEmailActive()) {
            LOGGER.info("sending{} MembershipMailerMessage to Kafka to trigger WelcomeToPrime email for membershipId {}", force ? " forced" : StringUtils.EMPTY, membershipMailerMessage.getMembershipId());
            Callback welcomeToPrimeCallback = (recordMetadata, e) -> publishMessageCallbackProcessor(WELCOME_TO_PRIME_MESSAGE_SENT_OK,
                    membershipMailerMessage.getMembershipId().toString(),
                    isNull(e) ? MetricsNames.SUCCESS_WELCOME_TO_PRIME_MSG : MetricsNames.FAILED_WELCOME_TO_PRIME_MSG, e);

            welcomeToPrimePublisher.publishMembershipMailerMessage(membershipMailerMessage, welcomeToPrimeCallback);
        }
    }

    void publishMessageCallbackProcessor(String successMessage, String key, String metricName, Exception e) {
        if (nonNull(e)) {
            LOGGER.error(e.getMessage());
        } else {
            LOGGER.info(successMessage, key);
        }
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(metricName), MetricsNames.METRICS_REGISTRY_NAME);
    }
}
