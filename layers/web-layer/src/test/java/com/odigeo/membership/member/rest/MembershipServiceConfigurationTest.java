package com.odigeo.membership.member.rest;

import com.odigeo.membership.member.rest.exception.mapper.BaseExceptionMapper;
import org.testng.annotations.Test;

import java.util.Set;

import static org.testng.Assert.assertTrue;

public class MembershipServiceConfigurationTest {

    @Test
    public void testGetMembershipApiExceptionMappers() {
        Set<Object> mappers = MembershipServiceConfiguration.getMembershipApiExceptionMappers();

        for (Object mapper : mappers) {
            assertTrue(mapper instanceof BaseExceptionMapper);
        }
    }
}
