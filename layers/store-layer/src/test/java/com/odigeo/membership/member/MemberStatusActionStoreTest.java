package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.StatusAction;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class MemberStatusActionStoreTest {

    private static final long TEST_MEMBER_ID = 1L;
    private static final StatusAction TEST_STATUS_ACTION = StatusAction.ACTIVATION;
    private static final long TEST_ACTION_ID = 3L;
    private static final int MEMBER_ID = 0;

    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    private MemberStatusActionStore memberStatusActionStore;

    @BeforeMethod
    public void setUp() throws SQLException {
        MockitoAnnotations.openMocks(this);
        mockDatabase();
        ConfigurationEngine.init();
        memberStatusActionStore = ConfigurationEngine.getInstance(MemberStatusActionStore.class);
    }

    @Test
    public void createMemberStatusAction() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getLong("ID")).thenReturn(TEST_ACTION_ID);
        memberStatusActionStore.createMemberStatusAction(dataSource, TEST_MEMBER_ID, TEST_STATUS_ACTION);
        verify(preparedStatement).setString(1, String.valueOf(0L));
        verify(preparedStatement).setString(2, String.valueOf(TEST_MEMBER_ID));
        verify(preparedStatement).setString(3, TEST_STATUS_ACTION.toString());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testCreateMemberStatusActionThrowsNPEWithNullDBData() throws Exception {
        this.memberStatusActionStore.createMemberStatusAction(null, MEMBER_ID, null);
    }

    @Test(expectedExceptions = SQLException.class)
    public void testCreateMemberStatusActionConnectsButThrowsExceptionWithInconsistentData() throws Exception {
        this.memberStatusActionStore.createMemberStatusAction(dataSource, MEMBER_ID, StatusAction.CREATION);
    }

    @Test
    public void testPrepareAndExecuteCreationStatement() throws Exception {
        memberStatusActionStore.prepareAndExecuteCreationStatement(TEST_MEMBER_ID, TEST_STATUS_ACTION,
                preparedStatement, TEST_ACTION_ID);
        verify(preparedStatement).setString(1, String.valueOf(TEST_ACTION_ID));
        verify(preparedStatement).setString(2, String.valueOf(TEST_MEMBER_ID));
        verify(preparedStatement).setString(3, TEST_STATUS_ACTION.toString());
    }

    @Test
    public void testFoundMemberStatusActionIsReturnedWhenExistsInDB() throws Exception {
        //Given
        final Long memberId = 324L;
        final Long memberStatusId = 89L;
        java.util.Date statusChangeDate = Date.from(Instant.parse("2007-12-03T10:15:30.00Z"));
        configureSelectLastStatusAction(memberId, memberStatusId,
            statusChangeDate, StatusAction.ACTIVATION.name());
        //When
        Optional<MemberStatusAction> memberStatusAction = memberStatusActionStore.selectLastStatusActionByMembershipId(dataSource, memberId);
        //Then
        if (memberStatusAction.isPresent()) {
            assertEquals(memberStatusAction.get(), new MemberStatusAction(memberStatusId, memberId, StatusAction.ACTIVATION, statusChangeDate));
        } else {
            Assert.fail("FAIL: memberStatusAction is empty when it shouldn't.");
        }
    }

    private void configureSelectLastStatusAction(Long memberId, Long memberStatusId,
                                                 Date statusChangeDate, String statusAction) throws SQLException {
        when(connection.prepareStatement(getSelectLastStatusModificationQuery(memberId))).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getLong("ID")).thenReturn(memberStatusId);
        when(resultSet.getLong("MEMBER_ID")).thenReturn(memberId);
        when(resultSet.getString("ACTION_TYPE")).thenReturn(statusAction);
        when(resultSet.getDate("ACTION_DATE")).thenReturn(new java.sql.Date(statusChangeDate.getTime()));
    }

    @Test
    public void testFoundMemberStatusActionWithUnknown() throws Exception {
        //Given
        final Long memberId = 324L;
        final Long memberStatusId = 89L;
        java.util.Date statusChangeDate = Date.from(Instant.parse("2007-12-03T10:15:30.00Z"));
        configureSelectLastStatusAction(memberId, memberStatusId,
            statusChangeDate, "no found");
        //When
        Optional<MemberStatusAction> memberStatusAction = memberStatusActionStore.selectLastStatusActionByMembershipId(dataSource, memberId);
        //Then
        if (memberStatusAction.isPresent()) {
            assertEquals(memberStatusAction.get(), new MemberStatusAction(memberStatusId, memberId, StatusAction.UNKNOWN, statusChangeDate));
        } else {
            Assert.fail("FAIL: memberStatusAction is empty when it shouldn't.");
        }
    }

    @Test
    public void testNoMemberStatusActionReturnedWhenNotFoundInDB() throws Exception {
        //Given
        final Long memberId = 9932587L;
        when(connection.prepareStatement(getSelectLastStatusModificationQuery(memberId))).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(false);
        //When
        Optional<MemberStatusAction> memberStatusAction = memberStatusActionStore.selectLastStatusActionByMembershipId(dataSource, memberId);
        //Then
        assertFalse(memberStatusAction.isPresent());
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    private String getSelectLastStatusModificationQuery(final Long memberId) {
        return "SELECT msa.ID, msa.MEMBER_ID, msa.ACTION_TYPE, msa.ACTION_DATE FROM GE_MEMBER_STATUS_ACTION msa WHERE msa.MEMBER_ID = " + memberId + " ORDER BY msa.ACTION_DATE DESC ";
    }
}
