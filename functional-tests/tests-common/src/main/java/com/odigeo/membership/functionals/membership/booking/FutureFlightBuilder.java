package com.odigeo.membership.functionals.membership.booking;

import com.odigeo.membership.util.ZonedDateTimeISOConverter;
import cucumber.deps.com.thoughtworks.xstream.annotations.XStreamConverter;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class FutureFlightBuilder {

    private String destination;
    @XStreamConverter(ZonedDateTimeISOConverter.class)
    private OffsetDateTime departureDate;

    public String getDepartureDate() {
        return departureDate != null ? departureDate.format(DateTimeFormatter.ISO_DATE_TIME) : null;
    }

    public void setDepartureDate(OffsetDateTime departureDate) {
        this.departureDate = departureDate;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
