package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.db.DbUtils;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.util.MembershipEntityBuilder;
import com.odigeo.membership.member.util.MembershipRecurringEntityBuilder;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.search.MembershipSearch;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.odigeo.db.DbUtils.setLongOrNull;
import static com.odigeo.db.DbUtils.setStringOrNull;
import static com.odigeo.membership.member.util.MembershipEntityBuilder.AUTO_RENEWAL_DISABLED;
import static com.odigeo.membership.member.util.MembershipEntityBuilder.AUTO_RENEWAL_ENABLED;

@Singleton
public class MembershipStore {

    private static final Logger logger = Logger.getLogger(MembershipStore.class);
    private static final String MEMBERSHIP_FIELDS = " m.ID, m.WEBSITE, m.STATUS, m.AUTO_RENEWAL, m.ACTIVATION_DATE, m.EXPIRATION_DATE, m.MEMBER_ACCOUNT_ID, m.BALANCE, m.MEMBERSHIP_TYPE, m.MONTHS_DURATION, m.DURATION_TIME_UNIT, m.DURATION, m.SOURCE_TYPE, m.PRODUCT_STATUS, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, m.CURRENCY_CODE, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.FEE_CONTAINER_ID ";
    static final String DEFAULT_COLUMNS_MEMBERSHIP = "SELECT" + MEMBERSHIP_FIELDS
            + ", gmr.RECURRING_ID, gmrc.RECURRING_COLLECTION_ID, gmrml.REMIND_ME_LATER_FLAG, gmrml.REMIND_ME_LATER_LAST_UPDATE  ";
    private static final String GET_MEMBERSHIP_BY_ID_SQL = DEFAULT_COLUMNS_MEMBERSHIP + " FROM GE_MEMBERSHIP m "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr on m.id = gmr.membership_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc on m.id = gmrc.membership_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml on m.id = gmrml.membership_id "
            + "WHERE m.ID = ? ";
    private static final String GET_MEMBERSHIP_BY_MEMBER_ACCOUNT_ID_SQL = DEFAULT_COLUMNS_MEMBERSHIP + " FROM GE_MEMBERSHIP m "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr on m.id = gmr.membership_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc on m.id = gmrc.membership_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml on m.id = gmrml.membership_id "
            + "WHERE m.MEMBER_ACCOUNT_ID = ? ";
    private static final String GET_MEMBERSHIP_BY_ID_WITH_MEMBER_ACCOUNT_SQL = DEFAULT_COLUMNS_MEMBERSHIP
            + ", " + MemberAccountStore.MEMBER_ACCOUNT_FIELDS + " FROM GE_MEMBERSHIP m "
            + "JOIN GE_MEMBER_ACCOUNT a on a.id = m.member_account_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr on m.id = gmr.membership_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc on m.id = gmrc.membership_id "
            + "LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml on m.id = gmrml.membership_id "
            + "WHERE m.ID = ? ";
    private static final String SELECT_NEXT_MEMBER_ID = "SELECT SEQ_GE_MEMBERSHIP_ID.nextval FROM dual ";
    private static final String ADD_MEMBERSHIP_SQL = "INSERT INTO GE_MEMBERSHIP(ID, WEBSITE, MEMBER_ACCOUNT_ID, STATUS, AUTO_RENEWAL, ACTIVATION_DATE, EXPIRATION_DATE, TOTAL_PRICE, RENEWAL_PRICE, PRODUCT_STATUS, MEMBERSHIP_TYPE, BALANCE, MONTHS_DURATION, DURATION_TIME_UNIT, DURATION, SOURCE_TYPE, CURRENCY_CODE, MONTHS_RENEWAL_DURATION, FEE_CONTAINER_ID) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
    private static final String UPDATE_MEMBERSHIP_BALANCE_SQL = "UPDATE GE_MEMBERSHIP SET BALANCE = ? WHERE ID = ?";
    private static final String UPDATE_STATUS_SQL = "UPDATE GE_MEMBERSHIP SET STATUS = ?, AUTO_RENEWAL = ? WHERE ID = ?";
    private static final String UPDATE_RENEWAL_SQL = "UPDATE GE_MEMBERSHIP SET AUTO_RENEWAL = ? WHERE ID = ?";
    private static final String UPDATE_TYPE_SQL = "UPDATE GE_MEMBERSHIP SET MEMBERSHIP_TYPE = ?, RENEWAL_PRICE = ? WHERE ID = ?";
    private static final String ACTIVATE_PENDING_TO_COLLECT_SQL = "UPDATE GE_MEMBERSHIP SET PRODUCT_STATUS = 'CONTRACT', STATUS = '" + MemberStatus.ACTIVATED + "', BALANCE = ?, ACTIVATION_DATE = ? WHERE ID = ?";
    private static final String ACTIVATE_MEMBERSHIP_SQL = "UPDATE GE_MEMBERSHIP SET STATUS = '" + MemberStatus.ACTIVATED + "', " + "ACTIVATION_DATE = ?, EXPIRATION_DATE = ?, BALANCE = ? WHERE ID = ?";
    private static final String MISSING_MEMBERSHIP_MESSAGE = "Missing membership with membershipId: ";
    private static final String ACTIVATE_PENDING_TO_COLLECT_MONTHLY_SUBSCRIPTION_SQL = "UPDATE GE_MEMBERSHIP SET PRODUCT_STATUS = 'CONTRACT', STATUS = '" + MemberStatus.ACTIVATED + "', BALANCE = ?, ACTIVATION_DATE = ?, EXPIRATION_DATE = ?  WHERE ID = ?";

    private static final String REMIND_ME_LATER_SELECT = "SELECT * FROM GE_MEMBERSHIP_REMIND_ME_LATER WHERE MEMBERSHIP_ID = ?";
    private static final String REMIND_ME_LATER_INSERT = "INSERT INTO GE_MEMBERSHIP_REMIND_ME_LATER (REMIND_ME_LATER_FLAG, REMIND_ME_LATER_LAST_UPDATE, MEMBERSHIP_ID) VALUES (?, SYSDATE, ?)";
    private static final String REMIND_ME_LATER_UPDATE = "UPDATE GE_MEMBERSHIP_REMIND_ME_LATER SET REMIND_ME_LATER_FLAG = ?, REMIND_ME_LATER_LAST_UPDATE = SYSDATE WHERE MEMBERSHIP_ID = ?";

    public boolean activateMember(DataSource dataSource, long membershipId, LocalDateTime activationDate, LocalDateTime expirationDate, BigDecimal balance) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ACTIVATE_MEMBERSHIP_SQL)) {
            preparedStatement.setTimestamp(1, Timestamp.valueOf(activationDate));
            preparedStatement.setTimestamp(2, Timestamp.valueOf(expirationDate));
            preparedStatement.setBigDecimal(3, balance);
            preparedStatement.setString(4, String.valueOf(membershipId));
            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean activatePendingToCollect(DataSource dataSource, Membership membership) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ACTIVATE_PENDING_TO_COLLECT_SQL)) {
            preparedStatement.setBigDecimal(1, membership.getTotalPrice());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            preparedStatement.setString(3, String.valueOf(membership.getId()));
            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean activatePendingToCollectMonthlySubscription(DataSource dataSource, Membership membership) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ACTIVATE_PENDING_TO_COLLECT_MONTHLY_SUBSCRIPTION_SQL)) {
            int position = 1;
            preparedStatement.setBigDecimal(position++, membership.getTotalPrice());
            preparedStatement.setTimestamp(position++, Timestamp.valueOf(LocalDateTime.now()));
            preparedStatement.setTimestamp(position++, Timestamp.valueOf(membership.getExpirationDate()));
            preparedStatement.setString(position, String.valueOf(membership.getId()));
            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean updateStatusAndAutorenewal(DataSource dataSource, Long membershipId, MemberStatus newStatus, MembershipRenewal newAutoRenewal) throws DataAccessException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STATUS_SQL)) {
            preparedStatement.setString(1, newStatus.toString());
            preparedStatement.setString(2, newAutoRenewal.toString());
            preparedStatement.setString(3, String.valueOf(membershipId));
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Cannot change membership status for membershipId " + membershipId, e);
        }
    }

    public boolean updateAutoRenewal(DataSource dataSource, Long membershipId, AutoRenewalOperation operation) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_RENEWAL_SQL)) {
            String newAutoRenewalStatus = operation == AutoRenewalOperation.DISABLE_AUTO_RENEW
                    ? AUTO_RENEWAL_DISABLED : AUTO_RENEWAL_ENABLED;
            preparedStatement.setString(1, newAutoRenewalStatus);
            preparedStatement.setString(2, String.valueOf(membershipId));
            int result = preparedStatement.executeUpdate();
            return result > 0;
        }
    }

    public boolean updateTypeAndRenewalPrice(DataSource dataSource, Long membershipId, MembershipType type, BigDecimal renewalPrice) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TYPE_SQL)) {
            preparedStatement.setString(1, type.name());
            preparedStatement.setBigDecimal(2, renewalPrice);
            preparedStatement.setString(3, String.valueOf(membershipId));
            int result = preparedStatement.executeUpdate();
            return result > 0;
        }
    }

    public List<Membership> fetchMembershipByMemberAccountId(DataSource dataSource, long memberAccountId) throws SQLException, DataNotFoundException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBERSHIP_BY_MEMBER_ACCOUNT_ID_SQL)) {
            preparedStatement.setLong(1, memberAccountId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<Membership> membershipList = getMembershipEntityBuilder().buildListFromResultSet(rs);
                if (membershipList.isEmpty()) {
                    logger.warn("Missing membership for memberAccount: " + memberAccountId);
                    throw new DataNotFoundException("Missing membership for memberAccount: " + memberAccountId);
                }
                return membershipList;
            }
        }
    }

    public Membership fetchMembershipById(DataSource dataSource, long membershipId) throws SQLException, DataNotFoundException {
        return fetchMembershipById(dataSource, membershipId, false).orElse(null);
    }

    public Optional<Membership> fetchMembershipById(DataSource dataSource, long membershipId, boolean checkingForExistence) throws SQLException, DataNotFoundException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBERSHIP_BY_ID_SQL)) {
            preparedStatement.setString(1, String.valueOf(membershipId));
            try (ResultSet rs = preparedStatement.executeQuery()) {
                Membership membership = extractMembershipFromResultSet(rs, membershipId, false);
                return checkMembershipForExistence(membership, checkingForExistence, MISSING_MEMBERSHIP_MESSAGE + membershipId);
            }
        }
    }

    public Membership fetchMembershipByIdWithMemberAccount(DataSource dataSource, long membershipId) throws SQLException, DataNotFoundException {
        Membership membership;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBERSHIP_BY_ID_WITH_MEMBER_ACCOUNT_SQL)) {
            preparedStatement.setString(1, String.valueOf(membershipId));
            try (ResultSet rs = preparedStatement.executeQuery()) {
                membership = extractMembershipFromResultSet(rs, membershipId, true);
                if (Objects.isNull(membership)) {
                    logger.warn(MISSING_MEMBERSHIP_MESSAGE + membershipId);
                    throw new DataNotFoundException(MISSING_MEMBERSHIP_MESSAGE + membershipId);
                }
            }
        }
        return membership;
    }

    public long createMember(DataSource dataSource, MembershipCreation membershipCreation) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_MEMBERSHIP_SQL)) {
            long nextMemberId = getNextSequenceMemberId(connection);
            int idx = 1;
            preparedStatement.setString(idx++, String.valueOf(nextMemberId));
            preparedStatement.setString(idx++, membershipCreation.getWebsite());
            preparedStatement.setLong(idx++, membershipCreation.getMemberAccountId());
            preparedStatement.setString(idx++, membershipCreation.getMemberStatus().toString());
            preparedStatement.setString(idx++, membershipCreation.getAutoRenewal().toString());
            setDateOrNull(preparedStatement, idx++, membershipCreation.getActivationDate());
            setDateOrNull(preparedStatement, idx++, membershipCreation.getExpirationDate());
            preparedStatement.setBigDecimal(idx++, membershipCreation.getSubscriptionPrice());
            preparedStatement.setBigDecimal(idx++, membershipCreation.getRenewalPrice());
            setStringOrNull(preparedStatement, idx++, membershipCreation.getProductStatus());
            preparedStatement.setString(idx++, membershipCreation.getMembershipType().toString());
            preparedStatement.setBigDecimal(idx++, membershipCreation.getBalance());
            preparedStatement.setInt(idx++, membershipCreation.getMonthsDuration());
            preparedStatement.setString(idx++, membershipCreation.getDurationTimeUnit().name());
            preparedStatement.setInt(idx++, membershipCreation.getDuration());
            setStringOrNull(preparedStatement, idx++, getStringOrNull(membershipCreation.getSourceType()));
            setStringOrNull(preparedStatement, idx++, membershipCreation.getCurrencyCode());
            setIntegerOrNull(preparedStatement, idx++, membershipCreation.getRenewalDuration());
            setLongOrNull(preparedStatement, idx, membershipCreation.getFeeContainerId());
            preparedStatement.execute();
            return nextMemberId;
        }
    }

    private String getStringOrNull(Object value) {
        return Optional.ofNullable(value).map(Object::toString).orElse(null);
    }

    private Long getNextSequenceMemberId(Connection connection) throws SQLException {
        return DbUtils.nextSequence(connection.prepareStatement(SELECT_NEXT_MEMBER_ID));
    }

    //TODO OPRIME-1919 --> Do not use SEQ VAL and migrate to UUID
    public long getNextMembershipId(DataSource dataSource) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            return getNextSequenceMemberId(connection);
        }
    }

    private void setDateOrNull(PreparedStatement preparedStatement, int idx, LocalDateTime localDateTime) throws SQLException {
        if (Objects.isNull(localDateTime)) {
            preparedStatement.setNull(idx, Types.DATE);
        } else {
            preparedStatement.setTimestamp(idx, Timestamp.valueOf(localDateTime));
        }
    }

    private void setIntegerOrNull(PreparedStatement preparedStatement, int idx, Integer value) throws SQLException {
        if (Objects.isNull(value)) {
            preparedStatement.setNull(idx, Types.INTEGER);
        } else {
            preparedStatement.setInt(idx, value);
        }
    }

    public boolean updateMembershipBalance(DataSource dataSource, Long membershipId, BigDecimal balance) throws SQLException {
        try (Connection sqlConnection = dataSource.getConnection();
             PreparedStatement preparedStatement = sqlConnection.prepareStatement(UPDATE_MEMBERSHIP_BALANCE_SQL)) {
            preparedStatement.setBigDecimal(1, balance);
            preparedStatement.setString(2, String.valueOf(membershipId));
            int result = preparedStatement.executeUpdate();
            return result > 0;
        }
    }

    private void extractRecurringFromResultSetToMembership(ResultSet resultSet, Membership membership) throws SQLException {
        getMembershipRecurringEntityBuilder().build(resultSet)
                .ifPresent(membership.getMembershipRecurring()::add);
    }

    @SuppressWarnings("SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING")
    public List<Membership> searchMemberships(DataSource dataSource, MembershipSearch membershipSearch) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(membershipSearch.getQueryString())) {
            int preparedIndex = 1;
            for (Object value : membershipSearch.getValues()) {
                preparedStatement.setObject(preparedIndex++, value);
            }
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return getMembershipEntityBuilder().buildListFromSearch(membershipSearch, resultSet);
            }
        }
    }

    public boolean setRemindMeLater(DataSource dataSource, Long membershipId, Boolean value) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(REMIND_ME_LATER_SELECT)) {
            preparedStatement.setString(1, String.valueOf(membershipId));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return executeUpsert(connection, membershipId, value, resultSet);
            }
        }

    }

    public boolean executeUpsert(Connection connection, Long membershipId, Boolean value, ResultSet resultSet) throws SQLException {

        try (PreparedStatement preparedStatement = connection.prepareStatement(resultSet.next() ? REMIND_ME_LATER_UPDATE : REMIND_ME_LATER_INSERT)) {
            preparedStatement.setString(1, value ? "Y" : "N");
            preparedStatement.setLong(2, membershipId);
            return preparedStatement.executeUpdate() == 1;
        }

    }

    private Membership extractMembershipFromResultSet(ResultSet rs, long membershipId, boolean withAccount) throws SQLException {
        Map<Long, Membership> membershipMap = new LinkedHashMap<>();
        while (rs.next()) {
            membershipMap.putIfAbsent(membershipId, getMembershipEntityBuilder().build(rs, withAccount));
            Membership membership = membershipMap.get(membershipId);
            extractRecurringFromResultSetToMembership(rs, membership);
        }
        return membershipMap.get(membershipId);
    }

    private Optional<Membership> checkMembershipForExistence(Membership membership, boolean checkingForExistence, String message) throws DataNotFoundException {
        if (Objects.nonNull(membership) || checkingForExistence) {
            return Optional.ofNullable(membership);
        }
        logger.warn(message);
        throw new DataNotFoundException(message);
    }

    private MembershipEntityBuilder getMembershipEntityBuilder() {
        return ConfigurationEngine.getInstance(MembershipEntityBuilder.class);
    }

    private MembershipRecurringEntityBuilder getMembershipRecurringEntityBuilder() {
        return ConfigurationEngine.getInstance(MembershipRecurringEntityBuilder.class);
    }
}
