package com.odigeo.membership.member.discount.rule;

import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.List;

import static com.odigeo.membership.parameters.NotApplicationReason.MEMBERSHIP_UNKNOWN;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class UserRulesTest extends ApplyDiscountRulesTest {

    private UserRules userRules;

    @Mock
    private TestTokenUtils testTokenUtils;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        userRules = new UserRules();
    }

    @DataProvider
    public static Object[][] memberships() {
        Membership membership1 = Membership.builder().build();
        Membership membership2 = Membership.builder().build();
        return new Object[][]{
                {List.of(membership1), TRUE, null},
                {List.of(membership1, membership2), TRUE, null},
                {List.of(), FALSE, MEMBERSHIP_UNKNOWN}
        };
    }

    @Test(dataProvider = "memberships")
    public void testTest1(Collection<Membership> memberships, boolean expected, NotApplicationReason reason) {

        ApplyDiscountParameters parameters = new ApplyDiscountParameters();
        ApplyDiscountContext context = new ApplyDiscountContext(testTokenUtils, memberships);

        //WHEN
        boolean result = userRules.test(parameters, context);

        //THEN
        assertEquals(result, expected);
        checkContext(context, result, reason);
    }

}