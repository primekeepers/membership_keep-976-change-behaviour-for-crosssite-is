package com.odigeo.membership.member.engineering.refresh;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertNotNull;

public class RefreshServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private PrintWriter printWriter;

    private RefreshServlet refreshServlet;

    @BeforeMethod
    public void beforeClass() throws IOException {
        openMocks(this);
        refreshServlet = new RefreshServlet();
    }

    @AfterMethod
    public void tearDown() {
        Mockito.verifyNoMoreInteractions(request, response, printWriter);
    }

    @Test
    public void testConstructorReturnsNonNullInstance() throws Exception {
        assertNotNull(refreshServlet);
    }

    @Test
    public void testDoGet() throws IOException {
        when(response.getWriter()).thenReturn(printWriter);

        refreshServlet.doGet(request, response);

        verify(response).getWriter();
        verify(printWriter).print(anyString());
        verify(response).setStatus(anyInt());
    }

    @Test
    public void testDoPost() throws IOException {
        when(response.getWriter()).thenReturn(printWriter);

        refreshServlet.doPost(request, response);

        verify(response).getWriter();
        verify(printWriter).print(anyString());
        verify(response).setStatus(anyInt());
    }
}
