package com.odigeo.membership.member.memberapi.mapper;

import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.enums.Interface;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.membership.request.membership.ApplyItineraryDiscountRequest;
import com.odigeo.membership.response.membership.ApplyItineraryDiscountResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;

@Mapper
public interface MembershipMapper {

    ApplyDiscountParameters toApplyDiscountParameters(ApplyItineraryDiscountRequest applyItineraryDiscountRequest);

    @ValueMapping(target = "UNKNOWN", source = MappingConstants.ANY_REMAINING)
    Interface toInterface(com.odigeo.membership.request.Interface anInterface);

    @ValueMapping(target = "UNKNOWN", source = MappingConstants.ANY_REMAINING)
    com.odigeo.membership.response.membership.NotApplicationReason toNotApplicationReason(NotApplicationReason notApplicationReason);

    @Mapping(target = "detail", source = "reason")
    ApplyItineraryDiscountResponse toApplyItineraryDiscountResponse(ApplyDiscountResult applyDiscountResult);

}
