package com.odigeo.messaging.kafka.messagepublishers;

import com.google.inject.Singleton;
import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.MessagePublisher;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import org.apache.kafka.clients.producer.Callback;
import org.apache.log4j.Logger;

@Singleton
public class MembershipUpdatePublisher {
    private static final Logger LOGGER = Logger.getLogger(MembershipUpdatePublisher.class);

    @MessagePublisher(topic = "MEMBERSHIP_UPDATES_V2")
    private KafkaPublisher<Message> publisher;

    public void publishMembershipUpdateMessage(MembershipUpdateMessage membershipUpdateMessage, Callback callback) {
        try {
            publisher.publish(membershipUpdateMessage, callback);
            LOGGER.info(String.format("MembershipUpdateMessage published - membershipId:%s",
                    membershipUpdateMessage.getMembershipId()));
        } catch (PublishMessageException e) {
            LOGGER.error(String.format("Error publishing MembershipUpdateMessage: membershipId:%s",
                    membershipUpdateMessage.getMembershipId()), e);
        }
    }
}
