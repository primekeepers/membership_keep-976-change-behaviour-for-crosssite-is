package com.odigeo.membership.functionals.membership;

public class MemberStatusActionBuilder {

    private String id;
    private String memberId;
    private String actionType;
    private String actionDate;

    public String getId() {
        return id;
    }

    public String getMemberId() {
        return memberId;
    }

    public String getActionType() {
        return actionType;
    }

    public String getActionDate() {
        return actionDate;
    }

    public MemberStatusActionBuilder setId(final String id) {
        this.id = id;
        return this;
    }

    public MemberStatusActionBuilder setMemberId(final String memberId) {
        this.memberId = memberId;
        return this;
    }

    public MemberStatusActionBuilder setActionType(final String actionType) {
        this.actionType = actionType;
        return this;
    }

    public MemberStatusActionBuilder setActionDate(final String actionDate) {
        this.actionDate = actionDate;
        return this;
    }
}
