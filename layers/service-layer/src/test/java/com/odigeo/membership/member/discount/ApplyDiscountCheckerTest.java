package com.odigeo.membership.member.discount;

import com.google.inject.Provider;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.enums.Interface;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.member.discount.rule.ApplyDiscountRules;
import com.odigeo.membership.member.discount.rule.PrimeShareRules;
import com.odigeo.membership.member.discount.rule.TravellersRules;
import com.odigeo.membership.member.discount.rule.UserRules;
import com.odigeo.membership.member.discount.rule.WebsiteRules;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.util.TestTokenUtils;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_PRIME_SHARE;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_TRAVELLERS;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_USER;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_WEBSITE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class ApplyDiscountCheckerTest {

    private static final String WEBSITE_USER = "ES";
    private static final String WEBSITE_OTHER = "COM";

    private static final String USER_NAME = "IMAUserName";
    private static final String USER_LAST_NAMES = "IMAUserLastName";

    private static final TravellerParameter USER = new TravellerParameter(USER_NAME, USER_LAST_NAMES);
    private static final TravellerParameter OTHER = new TravellerParameter("Foo", "Fuu");

    private MockedStatic<TestTokenUtils> testTokenMatcherUtilsMockedStatic;

    @Mock
    private TestTokenUtils testTokenUtils;
    @Mock
    private Map<String, Provider<ApplyDiscountRules>> rulesMap;

    @BeforeClass
    public void beforeClass() {
        testTokenMatcherUtilsMockedStatic = Mockito.mockStatic(TestTokenUtils.class);
    }

    @AfterClass
    public void afterClass() {
        testTokenMatcherUtilsMockedStatic.close();
    }

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        testTokenMatcherUtilsMockedStatic.when(() -> TestTokenUtils.forTestDimensions(anyMap())).thenReturn(testTokenUtils);
        ApplyDiscountChecker.rulesProvider = rulesMap;
        when(rulesMap.get(RULES_USER)).thenReturn(UserRules::new);
        when(rulesMap.get(RULES_WEBSITE)).thenReturn(WebsiteRules::new);
        when(rulesMap.get(RULES_PRIME_SHARE)).thenReturn(PrimeShareRules::new);
        when(rulesMap.get(RULES_TRAVELLERS)).thenReturn(TravellersRules::new);
    }

    @DataProvider
    public static Object[][] tests() {
        return new Object[][]{
                {List.of(), List.of(), Interface.UNKNOWN, 0, 0, 0, FALSE, WEBSITE_OTHER, FALSE},
                {List.of(MembershipType.BASIC), List.of(USER), Interface.UNKNOWN, 1, 1, 1, FALSE, WEBSITE_USER, TRUE},
                {List.of(MembershipType.BASIC), List.of(), Interface.UNKNOWN, 1, 2, 1, FALSE, WEBSITE_USER, TRUE},
                {List.of(MembershipType.BASIC), List.of(), Interface.UNKNOWN, 1, 3, 1, FALSE, WEBSITE_USER, FALSE},
                {List.of(MembershipType.PLUS), List.of(), Interface.UNKNOWN, 1, 3, 1, FALSE, WEBSITE_USER, TRUE},

                {List.of(MembershipType.BASIC), List.of(USER), Interface.ONE_FRONT_SMARTPHONE, 2, 2, 1, FALSE, WEBSITE_USER, TRUE},
                {List.of(MembershipType.BASIC_FREE), List.of(OTHER), Interface.NATIVE_IOS_EDREAMS, 1, 3, 1, TRUE, WEBSITE_USER, FALSE},
                {List.of(MembershipType.BASIC_FREE), List.of(USER), Interface.ONE_FRONT_DESKTOP, 1, 1, 2, FALSE, WEBSITE_OTHER, FALSE},
                {List.of(MembershipType.BASIC_FREE), List.of(OTHER), Interface.ONE_FRONT_SMARTPHONE, 2, 1, 1, TRUE, WEBSITE_USER, FALSE},
                {List.of(MembershipType.BASIC_FREE), List.of(USER), Interface.NATIVE_ANDROID_GOV, 3, 2, 2, FALSE, WEBSITE_USER, TRUE},
                {List.of(MembershipType.BUSINESS), List.of(USER), Interface.NATIVE_ANDROID_GOV, 1, 2, 2, FALSE, WEBSITE_USER, TRUE},
                {List.of(MembershipType.BUSINESS), List.of(OTHER), Interface.ONE_FRONT_DESKTOP, 2, 3, 1, TRUE, WEBSITE_USER, FALSE},
                {List.of(MembershipType.BUSINESS), List.of(USER), Interface.ONE_FRONT_SMARTPHONE, 3, 1, 1, FALSE, WEBSITE_OTHER, FALSE},
                {List.of(MembershipType.EMPLOYEE), List.of(USER), Interface.ONE_FRONT_DESKTOP, 2, 2, 1, TRUE, WEBSITE_OTHER, FALSE},
                {List.of(MembershipType.EMPLOYEE), List.of(OTHER), Interface.ONE_FRONT_DESKTOP, 3, 3, 2, FALSE, WEBSITE_USER, FALSE}
        };
    }

    @Test(dataProvider = "tests")
    public void testTest1(Collection<MembershipType> membershipsType, Collection<TravellerParameter> travellers, Interface clientInterface, Integer abNative, Integer abPrimeSharing, Integer abMeta, boolean isMeta, String website, boolean expected) {

        //GIVEN
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_FOR_ALL)).thenReturn(abPrimeSharing.equals(2));
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_NATIVE_FOR_ALL)).thenReturn(abNative.equals(2));
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_ONLY_PLUS)).thenReturn(abPrimeSharing.equals(3));
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_NATIVE_ONLY_PLUS)).thenReturn(abNative.equals(3));
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_META_BLOCKED)).thenReturn(abMeta.equals(2));

        ApplyDiscountParameters applyDiscountParameters = buildApplyDiscountParameters(travellers, clientInterface, isMeta, website);

        Collection<Membership> memberships = getMemberships(membershipsType);

        ApplyDiscountChecker applyDiscountChecker = ApplyDiscountChecker.of(applyDiscountParameters).withMemberships(memberships);

        //WHEN
        ApplyDiscountResult result = applyDiscountChecker.testAndGetResult();

        //THEN
        assertEquals(result.isApplicable(), expected, Objects.toString(result.getReason()));
        if (result.isApplicable()) {
            assertNotNull(result.getApplicableMembership());
            assertNull(result.getReason());
        } else {
            assertNotNull(result.getReason());
            assertNull(result.getApplicableMembership());
        }

    }

    private Collection<Membership> getMemberships(Collection<MembershipType> membershipsType) {
        MemberAccount membeAccount = new MemberAccount(1, 100, USER_NAME, USER_LAST_NAMES);
        return membershipsType.stream().map(membershipType -> Membership.builder()
                .setMembershipType(membershipType)
                .setWebsite(WEBSITE_USER)
                .setMemberAccount(membeAccount)
                .build()).collect(Collectors.toList());
    }

    private ApplyDiscountParameters buildApplyDiscountParameters(Collection<TravellerParameter> travellers, Interface clientInterface, Boolean isMeta, String website) {
        ApplyDiscountParameters parameters = new ApplyDiscountParameters();
        parameters.setTravellers(travellers);
        parameters.setClientInterface(clientInterface);
        parameters.setMetasearch(isMeta);
        parameters.setWebsite(website);
        parameters.setTestDimensions(Map.of());
        return parameters;
    }
}