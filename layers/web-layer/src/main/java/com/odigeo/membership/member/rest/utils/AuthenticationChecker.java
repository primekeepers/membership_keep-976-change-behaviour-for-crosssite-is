package com.odigeo.membership.member.rest.utils;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Singleton;
import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.config.files.PropertiesLoader;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Singleton
public class AuthenticationChecker {
    private static final String MEMBERSHIP_AUTHENTICATION_PROPERTIES_PATH = "/com/odigeo/membership/configuration/authentication/MembershipAuthentication.properties";
    private static final String MAC_ALGORITHM = "HmacSHA256";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    private static final ZoneId ZONE_MAD = ZoneId.of("Europe/Madrid");
    private final Mac mac;
    private final Properties authenticationProperties;

    public AuthenticationChecker() {
        try {
            mac = Mac.getInstance(MAC_ALGORITHM);
            URL configurationFileUrl = new ConfigurationFilesManager().getConfigurationFileUrl(MEMBERSHIP_AUTHENTICATION_PROPERTIES_PATH);
            authenticationProperties = new PropertiesLoader().loadPropertiesFromDisk(configurationFileUrl);
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
    }

    @VisibleForTesting
    protected AuthenticationChecker(Properties properties) throws NoSuchAlgorithmException {
        mac = Mac.getInstance(MAC_ALGORITHM);
        this.authenticationProperties = properties;
    }

    public boolean check(String client, String hash, String path) {
        ZonedDateTime now = ZonedDateTime.now(ZONE_MAD);
        String toHashCurrentMinute = client + path + DATE_TIME_FORMATTER.format(now);
        String toHashPreviousMinute = client + path + DATE_TIME_FORMATTER.format(now.minusMinutes(1L));
        String key = authenticationProperties.getProperty(client);
        return isNotEmpty(key) && StringUtils.equalsAny(hash, calculateHmac(key, toHashCurrentMinute), calculateHmac(key, toHashPreviousMinute));
    }

    private String calculateHmac(String key, String input) {
        try {
            mac.init(new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), MAC_ALGORITHM));
        } catch (InvalidKeyException e) {
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
        return Hex.encodeHexString(mac.doFinal(input.getBytes(StandardCharsets.UTF_8)));
    }
}
