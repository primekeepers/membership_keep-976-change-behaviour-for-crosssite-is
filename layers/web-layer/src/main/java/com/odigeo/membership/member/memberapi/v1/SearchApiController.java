package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.v1.MembershipSearchApi;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class SearchApiController extends AbstractController implements MembershipSearchApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchApiController.class);
    private static final String SEARCH_BAD_REQUEST_MESSAGE = "To perform a search at least one search parameter should be set!";

    private final MapperFacade mapperFacade;
    private final Function<Membership, MembershipResponse> membershipToMembershipResponse;

    public SearchApiController() {
        this.mapperFacade = new GeneralMapperCreator().getMapper();
        this.membershipToMembershipResponse = membership -> mapperFacade.map(membership, MembershipResponse.class);
    }

    @Override
    public MemberAccountResponse getMemberAccount(Long memberAccountId, boolean withMemberships) {
        try {
            return Optional.of(getMemberAccountService().getMemberAccountById(memberAccountId, withMemberships))
                    .map(memberAccount -> mapperFacade.map(memberAccount, MemberAccountResponse.class))
                    .orElse(null);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipResponse getMembership(Long membershipId, boolean withMemberAccount) {
        try {
            return Optional.of(getMembershipById(membershipId, withMemberAccount))
                    .map(membershipToMembershipResponse)
                    .orElse(null);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    private Membership getMembershipById(Long membershipId, boolean withMemberAccount) throws MissingElementException, DataAccessException {
        if (withMemberAccount) {
            return getMembershipService().getMembershipByIdWithMemberAccount(membershipId);
        }
        return getMembershipService().getMembershipById(membershipId);
    }

    @Override
    public List<MemberAccountResponse> searchAccounts(MemberAccountSearchRequest searchRequest) {
        MemberAccountSearch memberAccountSearch = SearchMapper.INSTANCE.toMemberAccountSearch(searchRequest, false);
        if (memberAccountSearch.isEmpty()) {
            throw new MembershipBadRequestException(SEARCH_BAD_REQUEST_MESSAGE);
        }
        try {
            return Optional.of(getSearchService().searchMemberAccounts(memberAccountSearch))
                    .map(accounts -> mapperFacade.mapAsList(accounts, MemberAccountResponse.class))
                    .orElseGet(Collections::emptyList);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public List<MembershipResponse> searchMemberships(MembershipSearchRequest searchRequest) {
        MembershipSearch membershipSearch = SearchMapper.INSTANCE.toMembershipSearch(searchRequest);

        if (membershipSearch.isEmpty()) {
            throw new MembershipBadRequestException(SEARCH_BAD_REQUEST_MESSAGE);
        }
        try {
            return Optional.of(getSearchService().searchMemberships(membershipSearch))
                    .map(memberships -> mapperFacade.mapAsList(memberships, MembershipResponse.class))
                    .orElseGet(Collections::emptyList);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

}

