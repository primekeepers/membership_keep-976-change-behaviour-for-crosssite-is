package com.odigeo.membership;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.test.docker.Artifact;
import com.odigeo.commons.test.docker.ContainerInfoBuilderFactory;
import com.odigeo.commons.test.docker.LogFilesTransfer;
import com.odigeo.membership.functionals.InstallerTestSuiteEnvironment;
import com.odigeo.membership.functionals.config.redis.RedisConfig;
import com.odigeo.technology.docker.ContainerComposer;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.DockerModule;
import com.odigeo.technology.docker.ImageController;
import com.odigeo.technology.docker.ImageException;
import com.odigeo.technology.docker.SocketPingChecker;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;

import java.io.IOException;
import java.time.ZoneOffset;
import java.util.TimeZone;

@CucumberOptions(plugin = {"pretty", "json:target/cucumber/cucumber.json", "html:target/cucumber/"},
        strict = true)
@Guice(modules = DockerModule.class)
public class BaseMembershipDockerTest extends AbstractTestNGCucumberTests {
    private static final Logger LOGGER = Logger.getLogger(BaseMembershipDockerTest.class);
    private static final String GROUP_ID = "com.odigeo.membership";
    private static final String MODULE_NAME = "membership";

    private final Artifact artifact = new Artifact(GROUP_ID, MODULE_NAME, System.getProperty("project.parent.version"));
    @Inject
    private ContainerComposer containerComposer;
    @Inject
    private CustomContainerComposer customContainerComposer;
    @Inject
    private ContainerInfoBuilderFactory containerInfoBuilderFactory;
    @Inject
    private ImageController imageController;
    @Inject
    private LogFilesTransfer logFilesTransfer;

    private InstallerTestSuiteEnvironment installerTestSuiteEnvironment;

    @BeforeClass
    public void setUp() throws IOException, ImageException, ContainerException {
        try {
            ConfigurationEngine.init();
            installerTestSuiteEnvironment = ConfigurationEngine.getInstance(InstallerTestSuiteEnvironment.class);
            ContainerInfo zookeeperKafka = CustomContainerInfoBuilderFactory.newZookeeperKafka().build();
            ContainerInfo redisContainer = installRedis();
            ContainerInfo kafkaInitializerSchemaRegistry = newKafkaInitializerSchemaRegistry();
            ContainerInfo moduleContainer = containerInfoBuilderFactory.newModuleWithEmptyOracle(artifact)
                    .addDependency(kafkaInitializerSchemaRegistry)
                    .addDependency(redisContainer)
                    .build();

            containerComposer.addServiceAndDependencies(zookeeperKafka, 200, 5000).composeUp(true);
            Thread.sleep(90000l); //delay to allow kafka become responsive to the kafka-initializer
            customContainerComposer.addServiceAndDependencies(moduleContainer, 200, 5000).composeUp(true);

            installerTestSuiteEnvironment.install();
            LOGGER.info("--------------- Starting integration tests ---------------");
        } catch (ContainerException | ImageException e) {
            LOGGER.error("error:", e);
            logFilesTransfer.copyToExternalFolder(artifact);
            throw e;
        } catch (IllegalArgumentException e) {
            LOGGER.error("error:", e);
            throw e;
        } catch (InterruptedException e) {
            e.printStackTrace();
            LOGGER.error("error: sleep interrupted", e);
        }
        TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC));
    }

    private ContainerInfo newKafkaInitializerSchemaRegistry() throws ContainerException {
        return CustomContainerInfoBuilderFactory.newKafkaInitializerSchemaRegistry(artifact, "MEMBERSHIP_ACTIVATIONS_v4","MEMBERSHIP_UPDATES_V2", "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1").build();
    }

    private ContainerInfo installRedis() throws ContainerException {
        RedisConfig redisConfig = ConfigurationEngine.getInstance(RedisConfig.class);
        SocketPingChecker socketPingChecker = new SocketPingChecker(redisConfig.getLocalIp(), redisConfig.getPort());
        return new ContainerInfoBuilder("redis:5.0.7-alpine")
                .setName("redis")
                .addPortMapping(redisConfig.getPort(), redisConfig.getPort())
                .setPingChecker(socketPingChecker)
                .build();
    }

    @AfterClass
    public void tearDown() throws IOException, ContainerException, ImageException {
        installerTestSuiteEnvironment.uninstall();
        logFilesTransfer.copyToExternalFolder(artifact);
        customContainerComposer.composeDown();
        containerComposer.composeDown();
        imageController.removeFuncionalTestsImage(artifact.getArtifactId());
    }
}
