package com.odigeo.membership.mocks.userapi;

import com.odigeo.userprofiles.api.v2.UserApiServiceInternal;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.HashCode;
import com.odigeo.userprofiles.api.v2.model.HashType;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import com.odigeo.userprofiles.api.v2.model.requests.GenerateHashCodeRequest;
import com.odigeo.userprofiles.api.v2.model.requests.SendEmailRequest;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidFindException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.StatusUserException;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserApiInternalMock implements UserApiServiceInternal {

    private static final String JSON_CONTENT_TYPE = "application/json;charset=UTF-8";
    private static final String MOCK_NOT_IMPLEMENTED = "Mock not implemented";
    private final List<UserBasicInfo> mockedUserBasicInfos;
    private final Map<Long, HashCode> mockedUserHashCodes;

    public UserApiInternalMock() {
        this.mockedUserBasicInfos = new ArrayList<>();
        this.mockedUserHashCodes = new HashMap<>();
    }

    public void addMockUserBasicInfo(UserBasicInfo userBasicInfo) {
        mockedUserBasicInfos.add(userBasicInfo);
    }

    public void addMockHashCodeToUser(HashCode hashCode, long userId) {
        mockedUserHashCodes.put(userId, hashCode);
    }

    @Override
    public Response ping(String s) {
        return okResponse("Mock user-api running");
    }

    @Override
    public Response getUserBasicInfo(String auth, long userId) {
        return mockedUserBasicInfos.stream()
                .filter(user -> user.getId().equals(userId))
                .findFirst()
                .map(this::okResponse)
                .orElse(null);
    }

    @Override
    public Response getUserBasicInfo(String auth, String email, Brand brand) {
        return mockedUserBasicInfos.stream()
                .filter(user -> user.getEmail().equals(email) && user.getBrand().equals(brand))
                .findFirst()
                .map(this::okResponse)
                .orElse(null);
    }

    @Override
    public Response getDevices(String s, long l) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response getAirlinePreferences(String s, long l) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response getAirportPreferences(String s, long l) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response getDestinationPreferences(String s, long l) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response getCreditCards(String s, long l) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response sendEmail(String s, long l, SendEmailRequest sendEmailRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response deletePendingUserAccount(String s, String s1, Brand brand) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response saveUser(String s, User user) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public Response generateHashCode(String s, GenerateHashCodeRequest generateHashCodeRequest) throws RemoteException, InvalidValidationException, InvalidCredentialsException, InvalidFindException, StatusUserException {
        return null;
    }

    @Override
    public Response getHashCodes(String auth, long userId, HashType hashType) {
        Optional<HashCode> hashCode = Optional.ofNullable(mockedUserHashCodes.get(userId));
        if (hashCode.isPresent()) {
            if (hashCode.get().getHashType().equals(hashType)) {
                return okListResponse(hashCode.get());
            }
        }
        return emptyListResponse();
    }

    private Response okResponse(Object object) {
        return Response.ok(object).type(JSON_CONTENT_TYPE).build();
    }

    private Response emptyListResponse() {
        return Response.noContent().type(JSON_CONTENT_TYPE).build();
    }

    private Response okListResponse(Object object) {
        return okResponse(Collections.singletonList(object));
    }
}
