package com.odigeo.messaging;

import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class MembershipMailerMessageServiceTest {

    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long USER_ID = 343L;
    private static final Long BOOKING_ID = 200L;
    private static final Membership ACTIVE_MEMBERSHIP = new MembershipBuilder()
            .setId(MEMBERSHIP_ID)
            .setStatus(MemberStatus.ACTIVATED)
            .setMemberAccount(new MemberAccount(404L, USER_ID, "Test", "User")).build();

    @Mock
    private MembershipKafkaSender membershipKafkaSender;

    private MembershipMailerMessageService membershipMailerMessageService;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipMailerMessageService = new MembershipMailerMessageService(membershipKafkaSender);
    }

    @Test
    public void testSendWelcomeToPrimeMessageToMembershipTransactionalTopic_HappyPath() {
        ArgumentCaptor<MembershipMailerMessage> argument = ArgumentCaptor.forClass(MembershipMailerMessage.class);
        membershipMailerMessageService.sendWelcomeToPrimeMessageToMembershipTransactionalTopic(ACTIVE_MEMBERSHIP, BOOKING_ID, false);

        verify(membershipKafkaSender).sendMembershipMailerMessageToKafka(argument.capture(), eq(false));
        assertEquals(argument.getValue().getMembershipId(), MEMBERSHIP_ID);
        assertEquals(argument.getValue().getBookingId(), BOOKING_ID);
        assertEquals(argument.getValue().getUserId(), USER_ID);
        assertEquals(argument.getValue().getMessageType(), WELCOME_TO_PRIME);
    }
}
