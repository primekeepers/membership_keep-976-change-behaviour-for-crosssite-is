package com.odigeo.membership.member.memberapi.v1;

import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.request.search.SortCriteria;
import com.odigeo.membership.request.search.Sorting;
import com.odigeo.membership.request.search.SortingField;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class SearchMapperTest {


    private SearchMapper searchMapper;

    @BeforeMethod
    public void setUp() {
        searchMapper = SearchMapper.INSTANCE;
    }

    @Test
    public void nullMappings() {
        assertNull(searchMapper.toMembershipSearch(null));
        assertNull(searchMapper.toMemberAccountSearch(null, true));
        assertNull(searchMapper.toMembershipSearch(MembershipSearchRequest.builder().build()).getMemberAccountSearch());
    }

    @Test
    public void nestedSortingMapper(){
        MembershipSearchRequest searchRequest = MembershipSearchRequest.builder()
                .website("ES")
                .memberAccountSearchRequest(new MemberAccountSearchRequest.Builder().userId(1L).build())
                .sorting(Sorting.builder(SortingField.TIMESTAMP, SortCriteria.DESC).build()).build();
        MembershipSearch membershipSearch = searchMapper.toMembershipSearch(searchRequest);
        assertTrue(membershipSearch.getQueryString().contains(" ORDER BY MEMBERSHIP_TIMESTAMP DESC"));
    }

    @Test
    public void testToMemberAccountSearchRequest(){
        MemberAccountSearchRequest memberAccountSearchRequest = new MemberAccountSearchRequest.Builder().userId(1L).withMemberships(true).build();
        assertTrue(searchMapper.toMemberAccountSearch(memberAccountSearchRequest, false).isWithMemberships());
    }
}
