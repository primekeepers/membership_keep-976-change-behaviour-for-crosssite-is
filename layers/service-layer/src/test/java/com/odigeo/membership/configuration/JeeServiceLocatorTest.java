package com.odigeo.membership.configuration;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.naming.NamingException;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class JeeServiceLocatorTest extends AbstractContextFactory {

    public static final String MY_APP_JNDI_NAME = "myApp";

    protected ServiceNameResolver serviceNameResolver;

    @BeforeClass
    public void before() throws NamingException {
        openMocks(this);
        super.before();
        serviceNameResolver = ServiceNameResolver.getInstance();
        when(context.lookup(JeeServiceLocator.JNDI_APP_NAME)).thenReturn(MY_APP_JNDI_NAME);
    }

    @Test(enabled = false)
    public void testJndiAppName() throws NamingException {
        assertEquals(JeeServiceLocator.getInstance().getApplicationName(), MY_APP_JNDI_NAME);
    }

    @Test(enabled = false)
    public void testNotFoundService() throws NamingException, UnavailableServiceException {
        when(context.lookup(serviceNameResolver.resolveServiceName(String.class, serviceNameResolver.resolveServiceContext(MY_APP_JNDI_NAME)))).thenThrow(new NamingException());
        JeeServiceLocator.getInstance().getService(String.class);
    }
}
