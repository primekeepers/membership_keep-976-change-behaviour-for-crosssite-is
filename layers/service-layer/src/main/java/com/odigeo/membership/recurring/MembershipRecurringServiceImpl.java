package com.odigeo.membership.recurring;

import com.edreamsodigeo.payments.recurringcollection.contract.RecurringCollectionResource;
import com.edreamsodigeo.payments.recurringcollection.contract.model.ProductId;
import com.edreamsodigeo.payments.recurringcollection.contract.model.UserId;
import com.edreamsodigeo.payments.recurringcollection.contract.model.Website;
import com.edreamsodigeo.payments.recurringcollection.contract.request.CreateRecurringCollectionRequest;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.UUID;

@Singleton
public class MembershipRecurringServiceImpl implements MembershipRecurringService {

    private final RecurringCollectionResource recurringCollectionResource;

    @Inject
    public MembershipRecurringServiceImpl(RecurringCollectionResource recurringCollectionResource) {
        this.recurringCollectionResource = recurringCollectionResource;
    }

    @Override
    public UUID createMembershipRecurringCollection(MembershipRecurringCreation membershipRecurringCreation) {
        return recurringCollectionResource
                .create(buildCreateRecurringCollectionRequest(membershipRecurringCreation)).getId();
    }

    private CreateRecurringCollectionRequest buildCreateRecurringCollectionRequest(MembershipRecurringCreation membershipRecurringCreation) {
        ProductId productId = new ProductId(membershipRecurringCreation.getMembershipId(),
                membershipRecurringCreation.getType());
        Website website = new Website(membershipRecurringCreation.getWebsite());
        UserId userId = new UserId(membershipRecurringCreation.getUserId());
        return new CreateRecurringCollectionRequest(productId, website, userId);
    }
}
