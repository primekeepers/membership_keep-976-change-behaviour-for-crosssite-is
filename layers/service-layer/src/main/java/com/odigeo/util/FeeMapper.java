package com.odigeo.util;

import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.product.MembershipProductType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeeMapper {

    private AbstractFee createAbstractFee(BigDecimal subscriptionPrice, String currencyCode) {
        FixFee fixFee = new FixFee();
        fixFee.setAmount(subscriptionPrice);
        fixFee.setSubCode(MembershipProductType.MEMBERSHIP_RENEWAL.getFeeSubCode());
        fixFee.setCurrency(Currency.getInstance(currencyCode));
        fixFee.setCreationDate(new Date());
        fixFee.setFeeLabel(FeeLabel.MARKUP_TAX);
        return fixFee;
    }

    public Map<FeeLabel, List<AbstractFee>> membershipFeeToFeeLabelMap(BigDecimal subscriptionPrice, String currencyCode) {
        Map<FeeLabel, List<AbstractFee>> feeLabelListHashMap = new HashMap<>();
        ArrayList<AbstractFee> abstractFees = new ArrayList<>();
        abstractFees.add(createAbstractFee(subscriptionPrice, currencyCode));
        feeLabelListHashMap.put(FeeLabel.MARKUP_TAX, abstractFees);
        return feeLabelListHashMap;
    }

}
