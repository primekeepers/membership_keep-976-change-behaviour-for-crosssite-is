package com.odigeo.membership.exception.userapi;

import com.edreams.base.BaseException;

public class UserApiException extends BaseException {

    public UserApiException(String message) {
        super(message);
    }
}
