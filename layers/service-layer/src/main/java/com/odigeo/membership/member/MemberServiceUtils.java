package com.odigeo.membership.member;

import com.odigeo.bookingapi.v14.InvalidParametersException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.TravellerParameter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.text.Normalizer;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public final class MemberServiceUtils {

    private static final Pattern VALID_NAME_PATTERN = Pattern.compile("^([\\p{L}\\'\\.-]*(\\s))*[\\p{L}\\'\\.-]*$");
    private static final String INVALID_NAMES_ERROR_MSG = "The names are not correct.";

    private MemberServiceUtils() {
    }

    static Optional<Membership> getMemberOnList(List<MemberAccount> memberAccounts, String site, List<TravellerParameter> travellerList) {
        return memberAccounts.stream()
                .filter(websiteCheck(site).and(memberAccount -> isMemberOnList(memberAccount, travellerList)))
                .flatMap(memberAccount -> memberAccount.getMemberships().stream())
                .max(Comparator.comparing(Membership::getExpirationDate, Comparator.nullsLast(Comparator.naturalOrder())));
    }

    public static Boolean isMemberOnList(MemberAccount memberAccount, Collection<TravellerParameter> travellerList) {
        return Objects.nonNull(memberAccount) && CollectionUtils.isNotEmpty(travellerList)
                && travellerList.stream().anyMatch(getTravellerNameCheck(memberAccount));
    }

    static Boolean isValidPersonName(String name) {
        if (StringUtils.isBlank(name) || !VALID_NAME_PATTERN.matcher(name).matches()) {
            throw new InvalidParametersException(INVALID_NAMES_ERROR_MSG);
        }
        return Boolean.TRUE;
    }

    private static Predicate<TravellerParameter> getTravellerNameCheck(MemberAccount member) {
        return traveller -> isNormalizedNameMatch(traveller, member);
    }

    public static Predicate<MemberAccount> websiteCheck(String website) {
        return memberAccount -> memberAccount.getMemberships().stream()
                .anyMatch(membership -> website.equalsIgnoreCase(membership.getWebsite()));
    }

    public static boolean isExpirationDateInPast(Membership membership) {
        return Objects.nonNull(membership.getExpirationDate()) && LocalDateTime.now().isAfter(membership.getExpirationDate());
    }


    private static boolean isNormalizedNameMatch(TravellerParameter traveller, MemberAccount member) {
        String memberName = normalizeName(member.getName());
        String name = normalizeName(traveller.getName());
        String memberLastNames = normalizeName(member.getLastNames());
        String lastNames = normalizeName(traveller.getLastNames());

        return name.equalsIgnoreCase(memberName) && lastNames.equalsIgnoreCase(memberLastNames);
    }

    private static String normalizeName(String name) {
        return Normalizer.normalize(name.trim(), Normalizer.Form.NFKD)
                .replaceAll("^\\p{InCombiningDiacriticalMarks}+", "")
                .replaceAll("\\p{M}", "");
    }


}
