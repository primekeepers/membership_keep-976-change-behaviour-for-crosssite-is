package com.odigeo.membership.parameters.search;

import java.util.Objects;
import java.util.StringJoiner;

import static java.util.Objects.nonNull;

public class PaginationDTO {
    private final Integer offset;
    private final Integer limit;

    private PaginationDTO(Builder builder) {
        offset = builder.offset;
        limit = builder.limit;
    }

    public static Builder builder() {
        return new Builder();
    }


    public static Builder builder(PaginationDTO copy) {
        Builder builder = new Builder();
        builder.offset = copy.getOffset();
        builder.limit = copy.getLimit();
        return builder;
    }

    private Integer getLimit() {
        return limit;
    }

    private Integer getOffset() {
        return offset;
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private Integer offset;
        private Integer limit;

        public Builder offset(Integer offset) {
            this.offset = offset;
            return this;
        }

        public Builder limit(Integer limit) {
            this.limit = limit;
            return this;
        }

        public PaginationDTO build() {
            return new PaginationDTO(this);
        }

    }

    @Override
    public String toString() {
        StringJoiner paginationQueryChunk = new StringJoiner(" ");
        if (nonNull(offset)) {
            paginationQueryChunk.add(" OFFSET").add(String.valueOf(offset)).add("ROWS");
        }
        if (nonNull(limit)) {
            paginationQueryChunk.add(" FETCH NEXT").add(String.valueOf(limit)).add("ROWS ONLY");
        }
        return paginationQueryChunk.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaginationDTO that = (PaginationDTO) o;
        return Objects.equals(offset, that.offset) && Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(offset, limit);
    }
}
