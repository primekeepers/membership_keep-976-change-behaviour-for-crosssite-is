package com.odigeo.membership.configuration.services;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.DefaultRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v2.UserApiServiceInternal;
import org.apache.http.auth.UsernamePasswordCredentials;

public class UserProfileApiServiceModule extends DefaultRestUtilsModule<UserApiServiceInternal> {

    public UserProfileApiServiceModule(ServiceNotificator... serviceNotificators) {
        super(UserApiServiceInternal.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<UserApiServiceInternal> getServiceConfiguration(Class userApiService) {
        UserProfileApiSecurityConfiguration configuration = ConfigurationEngine.getInstance(UserProfileApiSecurityConfiguration.class);
        return ServiceConfigurationBuilder.setUpCustomWithCredentials(UserApiServiceInternal.class,
            new UsernamePasswordCredentials(configuration.getUser(), configuration.getPassword()));
    }
}
