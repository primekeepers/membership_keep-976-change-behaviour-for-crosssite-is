package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.BookingTracking;

import java.util.Collection;

public interface BookingTrackingService {

    boolean isBookingLimitReached(Long memberId) throws DataAccessException;

    BookingTracking getMembershipBookingTracking(long bookingId) throws DataAccessException;

    BookingTracking insertMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException;

    void deleteMembershipBookingTracking(long bookingId) throws DataAccessException;

    Collection<BookingTracking> getBookingTrackedByMembershipId(Long membershipId) throws DataAccessException;
}
