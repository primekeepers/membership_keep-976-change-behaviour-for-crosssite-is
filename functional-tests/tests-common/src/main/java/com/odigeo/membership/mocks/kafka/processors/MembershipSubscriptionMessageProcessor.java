package com.odigeo.membership.mocks.kafka.processors;

import com.google.inject.Singleton;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class MembershipSubscriptionMessageProcessor extends AbstractKafkaMessageProcessor<MembershipSubscriptionMessage> {

    private static final Logger LOGGER = Logger.getLogger(MembershipSubscriptionMessageProcessor.class);
    private final List<MembershipSubscriptionMessage> receivedMessages = new ArrayList<>();

    @Override
    public void onMessage(MembershipSubscriptionMessage message) {
        LOGGER.info("Received new MembershipSubscription message from kafka for email: " + message.getEmail());
        receivedMessages.add(message);
    }

    @Override
    public List<MembershipSubscriptionMessage> getReceivedMessages() {
        return receivedMessages;
    }

    @Override
    public void resetMessageList() {
        receivedMessages.clear();
    }
}
