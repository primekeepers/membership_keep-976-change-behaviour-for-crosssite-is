package com.odigeo.bookingapi;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.booking.SearchBookingsRequestBuilder;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BookingApiDAOTest {

    @Mock
    private BookingApiManager bookingApiManagerMock;
    private BookingApiDAO bookingApiDAO;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(getAbstractModule());
        bookingApiDAO = new BookingApiDAO();
    }

    @Test(expectedExceptions = BookingApiException.class, expectedExceptionsMessageRegExp = "booking api exception when retrieving booking")
    public void testGetBookingThrowsExceptionWhenDataAccessException() throws Exception {
        //Given
        final long bookingId = 4538L;
        given(bookingApiManagerMock.getBooking(bookingId)).willThrow(new BookingApiException("booking api exception when retrieving booking"));
        //When
        bookingApiDAO.getBooking(bookingId);
    }

    @Test
    public void testGetBookingReturnsBookingDetailWhenFoundByApi() throws Exception {
        //Given
        final long bookingId = 9345L;
        final BookingDetail expectedBookingDetail = new BookingDetail();
        expectedBookingDetail.setBookingStatus("expected booking");
        given(bookingApiManagerMock.getBooking(bookingId)).willReturn(expectedBookingDetail);
        //When
        final BookingDetail booking = bookingApiDAO.getBooking(bookingId);
        //Then
        assertEquals(booking, expectedBookingDetail);
    }

    @Test
    public void testSearchPrimeBookingsByMembershipIdReturnsEmptyListWhenApiThrowsException() throws Exception {
        //Given
        final long membershipId = 773L;
        final SearchBookingsRequest searchRequest = searchBookingRequest(membershipId, TRUE);
        given(bookingApiManagerMock.searchBookings(searchRequest)).willThrow(new DataAccessException("data access exception when searching bookings"));
        //When
        final List<BookingSummary> bookingSummaries = bookingApiDAO.searchPrimeBookingsByMembershipId(membershipId);
        //Then
        assertTrue(bookingSummaries.isEmpty());
    }

    @Test
    public void testSearchPrimeBookingsByMembershipIdReturnsEmptyListWhenApiReturnsResponseWithNullBookings() throws Exception {
        //Given
        final long membershipId = 773835L;
        final SearchBookingsRequest searchRequest = searchBookingRequest(membershipId, TRUE);
        given(bookingApiManagerMock.searchBookings(searchRequest)).willReturn(new SearchBookingsPageResponse<>(null, 0));
        //When
        final List<BookingSummary> bookingSummaries = bookingApiDAO.searchPrimeBookingsByMembershipId(membershipId);
        //Then
        assertTrue(bookingSummaries.isEmpty());
    }

    @Test
    public void testSearchPrimeBookingsByMembershipIdReturnsFoundBookingsByApi() throws Exception {
        //Given
        final long membershipId = 9332L;
        final SearchBookingsRequest searchRequest = searchBookingRequest(membershipId, TRUE);
        List<BookingSummary> expectedBookingSummaries = asList(randomBookingSummary(), randomBookingSummary());
        given(bookingApiManagerMock.searchBookings(searchRequest)).willReturn(new SearchBookingsPageResponse<>(expectedBookingSummaries, expectedBookingSummaries.size()));
        //When
        final List<BookingSummary> bookingSummaries = bookingApiDAO.searchPrimeBookingsByMembershipId(membershipId);
        //Then
        assertEquals(bookingSummaries, expectedBookingSummaries);
    }

    @Test
    public void testSearchBookingsByMembershipIdReturnsEmptyListWhenApiThrowsException() throws Exception {
        //Given
        final long membershipId = 5573L;
        final SearchBookingsRequest searchRequest = searchBookingRequest(membershipId, FALSE);
        given(bookingApiManagerMock.searchBookings(searchRequest)).willThrow(new DataAccessException("data access exception when searching bookings"));
        //When
        final List<BookingSummary> bookingSummaries = bookingApiDAO.searchBookingsByMembershipId(membershipId);
        //Then
        assertTrue(bookingSummaries.isEmpty());
    }

    @Test
    public void testSearchBookingsByMembershipIdReturnsEmptyListWhenApiReturnsResponseWithNullBookings() throws Exception {
        //Given
        final long membershipId = 13835L;
        final SearchBookingsRequest searchRequest = searchBookingRequest(membershipId, FALSE);
        given(bookingApiManagerMock.searchBookings(searchRequest)).willReturn(new SearchBookingsPageResponse<>(null, 0));
        //When
        final List<BookingSummary> bookingSummaries = bookingApiDAO.searchBookingsByMembershipId(membershipId);
        //Then
        assertTrue(bookingSummaries.isEmpty());
    }

    @Test
    public void testSearchBookingsByMembershipIdReturnsFoundBookingsByApi() throws Exception {
        //Given
        final long membershipId = 5532L;
        final SearchBookingsRequest searchRequest = searchBookingRequest(membershipId, FALSE);
        List<BookingSummary> expectedBookingSummaries = asList(randomBookingSummary(), randomBookingSummary());
        given(bookingApiManagerMock.searchBookings(searchRequest)).willReturn(new SearchBookingsPageResponse<>(expectedBookingSummaries, expectedBookingSummaries.size()));
        //When
        final List<BookingSummary> bookingSummaries = bookingApiDAO.searchBookingsByMembershipId(membershipId);
        //Then
        assertEquals(bookingSummaries, expectedBookingSummaries);
    }

    private SearchBookingsRequest searchBookingRequest(final long membershipId, final boolean isPrime) {
        return new SearchBookingsRequestBuilder()
                .setMembershipId(membershipId)
                .setBookingSubscriptionPrime(isPrime ? "true" : null)
                .build();
    }

    private BookingSummary randomBookingSummary() {
        BookingSummary bookingSummary = new BookingSummary();
        bookingSummary.setStatus(randomAlphanumeric(10));
        return bookingSummary;
    }

    private AbstractModule getAbstractModule() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(BookingApiManager.class).toProvider(() -> bookingApiManagerMock);
            }
        };
    }
}
