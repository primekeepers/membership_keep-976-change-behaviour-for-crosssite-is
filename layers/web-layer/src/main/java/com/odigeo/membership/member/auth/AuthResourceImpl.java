package com.odigeo.membership.member.auth;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.auth.AuthorizationRequest;
import com.odigeo.membership.auth.LoginRequest;
import com.odigeo.membership.auth.LogoutRequest;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;
import com.odigeo.membership.auth.usecases.AuthenticationLoginUseCase;
import com.odigeo.membership.auth.usecases.AuthenticationLogoutUseCase;
import com.odigeo.membership.auth.usecases.AuthorizationPermissionsUseCase;
import com.odigeo.membership.member.auth.exception.MembershipAuthenticationException;
import com.odigeo.membership.member.rest.utils.ResponseBuilder;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

public class AuthResourceImpl implements AuthResource {

    private static final Logger LOGGER = Logger.getLogger(AuthResourceImpl.class);
    private static final String LOGIN_LOG = "Requesting login for user %s";
    private static final String LOGOUT_LOG = "Requesting a logout";
    private static final String AUTHORIZATION_LOG = "Requesting authorization for given token";
    private static final String ERROR_LOGIN_LOG = "ERROR login for user %s";
    private static final String ERROR_LOGOUT_LOG = "ERROR Requesting a logout";
    private static final String ERROR_AUTHORIZATION_LOG = "ERROR Requesting authorization for given token";

    public Response login(Request request, String userName, LoginRequest loginRequest) {
        LOGGER.info(String.format(LOGIN_LOG, userName));
        try {
            return ResponseBuilder.buildResponse(ConfigurationEngine.getInstance(AuthenticationLoginUseCase.class).login(userName, loginRequest.getPassword()), request);
        } catch (MembershipAuthServiceException e) {
            LOGGER.error(String.format(ERROR_LOGIN_LOG, userName), e);
            throw new MembershipAuthenticationException(e.getMessage(), e);
        }
    }

    public Response logout(@Context final Request request, final LogoutRequest logoutRequest) {
        LOGGER.info(LOGOUT_LOG);
        try {
            return ResponseBuilder.buildResponse(ConfigurationEngine.getInstance(AuthenticationLogoutUseCase.class).logout(logoutRequest.getToken()), request);
        } catch (MembershipAuthServiceException e) {
            LOGGER.error(ERROR_LOGOUT_LOG, e);
            throw new MembershipAuthenticationException(e.getMessage(), e);
        }
    }

    public Response getTokenPermissions(@Context final Request request, final AuthorizationRequest authorizationRequest) {
        LOGGER.info(AUTHORIZATION_LOG);
        try {
            return ResponseBuilder.buildResponse(ConfigurationEngine.getInstance(AuthorizationPermissionsUseCase.class).getTokenPermissions(authorizationRequest.getToken()), request);
        } catch (MembershipAuthServiceException e) {
            LOGGER.error(ERROR_AUTHORIZATION_LOG, e);
            throw new MembershipAuthenticationException(e.getMessage(), e);
        }
    }
}
