package com.odigeo.membership;

import java.math.BigDecimal;
import java.util.Objects;

public class UpdateMembership {
    private final String membershipId;
    private final String operation;
    private final String membershipStatus;
    private final String membershipRenewal;
    private final Long userCreditCardId;
    private final String recurringId;
    private final Boolean remindMeLater;
    private final String membershipType;
    private final BigDecimal renewalPrice;

    private UpdateMembership(Builder builder) {
        membershipId = builder.membershipId;
        operation = builder.operation;
        membershipStatus = builder.membershipStatus;
        membershipRenewal = builder.membershipRenewal;
        userCreditCardId = builder.userCreditCardId;
        recurringId = builder.recurringId;
        remindMeLater = builder.remindMeLater;
        membershipType = builder.membershipType;
        renewalPrice = builder.renewalPrice;
    }
    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static Builder builder() {
        return new Builder();
    }

    public static Builder builderCloneOf(UpdateMembership updateMembership) {
        return builder()
                .membershipId(updateMembership.membershipId)
                .operation(updateMembership.operation)
                .membershipStatus(updateMembership.membershipStatus)
                .membershipRenewal(updateMembership.membershipRenewal)
                .userCreditCardId(updateMembership.userCreditCardId)
                .recurringId(updateMembership.recurringId)
                .remindMeLater(updateMembership.remindMeLater)
                .membershipType(updateMembership.membershipType)
                .renewalPrice(updateMembership.renewalPrice);
    }

    public Boolean getRemindMeLater() {
        return remindMeLater;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public String getOperation() {
        return operation;
    }

    public String getMembershipStatus() {
        return membershipStatus;
    }

    public String getMembershipRenewal() {
        return membershipRenewal;
    }

    public Long getUserCreditCardId() {
        return userCreditCardId;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private String membershipId;
        private String operation;
        private String membershipStatus;
        private String membershipRenewal;
        private Long userCreditCardId;
        private String recurringId;
        private Boolean remindMeLater;
        private String membershipType;
        private BigDecimal renewalPrice;

        private Builder() {

        }

        public Builder membershipId(String membershipId) {
            this.membershipId = membershipId;
            return this;
        }

        public Builder operation(String operation) {
            this.operation = operation;
            return this;
        }

        public Builder membershipStatus(String membershipStatus) {
            this.membershipStatus = membershipStatus;
            return this;
        }

        public Builder membershipRenewal(String membershipRenewal) {
            this.membershipRenewal = membershipRenewal;
            return this;
        }

        public Builder userCreditCardId(Long userCreditCardId) {
            this.userCreditCardId = userCreditCardId;
            return this;
        }

        public Builder recurringId(String recurringId) {
            this.recurringId = recurringId;
            return this;
        }

        public Builder remindMeLater(Boolean remindMeLater) {
            this.remindMeLater = remindMeLater;
            return this;
        }

        public Builder membershipType(String membershipType) {
            this.membershipType = membershipType;
            return this;
        }

        public Builder renewalPrice(BigDecimal renewalPrice) {
            this.renewalPrice = renewalPrice;
            return this;
        }

        public UpdateMembership build() {
            return new UpdateMembership(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UpdateMembership that = (UpdateMembership) o;
        return Objects.equals(membershipId, that.membershipId) && Objects.equals(operation, that.operation) && Objects.equals(membershipStatus, that.membershipStatus) && Objects.equals(membershipRenewal, that.membershipRenewal) && Objects.equals(userCreditCardId, that.userCreditCardId) && Objects.equals(recurringId, that.recurringId) && Objects.equals(remindMeLater, that.remindMeLater) && Objects.equals(membershipType, that.membershipType) && Objects.equals(renewalPrice, that.renewalPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(membershipId, operation, membershipStatus, membershipRenewal, userCreditCardId, recurringId, remindMeLater, membershipType, renewalPrice);
    }
}
