package com.odigeo.membership.member.creation.sql;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.parameters.MembershipCreation;

import javax.sql.DataSource;

public class NewMembershipSubscriptionService extends MembershipSQLServiceProvider {

    private final MemberManager memberManager;

    @Inject
    public NewMembershipSubscriptionService(MemberManager memberManager) {
        this.memberManager = memberManager;
    }

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        membershipCreation.setSourceType(SourceType.FUNNEL_BOOKING);
        long membershipId = memberManager.createMember(dataSource, membershipCreation);
        storeMemberStatusAction(dataSource, membershipId, StatusAction.CREATION);
        incrementCreationMetricsCounter(MetricsNames.BASIC_CREATION);
        return membershipId;
    }
}
