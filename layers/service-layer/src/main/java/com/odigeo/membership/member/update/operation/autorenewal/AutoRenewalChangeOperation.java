package com.odigeo.membership.member.update.operation.autorenewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.FunctionWithException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperation;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;
import java.sql.SQLException;

public abstract class AutoRenewalChangeOperation implements MembershipUpdateOperation {
    private final MembershipStore membershipStore;
    private final MembershipMessageSendingManager membershipMessageSendingManager;
    private final DataSource dataSource;
    protected final MemberService membershipService;

    public AutoRenewalChangeOperation(MembershipStore membershipStore,
                                      MembershipMessageSendingManager membershipMessageSendingManager,
                                      MemberService membershipService,
                                      DataSource dataSource) {
        this.dataSource = dataSource;
        this.membershipStore = membershipStore;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
        this.membershipService = membershipService;
    }

    protected String changeAutoRenewal(Long memberId, MembershipRenewal desiredValue, MembershipRenewal oldValue, AutoRenewalOperation autoRenewalOperation) throws MissingElementException, DataAccessException {
        MembershipRenewal membershipRenewal = membershipService.getMembershipById(memberId).getAutoRenewal();
        if (membershipRenewal.equals(oldValue) && getUpdateAutoRenewal(autoRenewalOperation).apply(memberId)) {
            membershipMessageSendingManager.sendMembershipIdToMembershipReporter(memberId);
            membershipRenewal = desiredValue;
        }
        return membershipRenewal.toString();
    }

    private FunctionWithException<Long, Boolean, DataAccessException> getUpdateAutoRenewal(AutoRenewalOperation operation) {
        return memberId -> {
            try {
                return membershipStore.updateAutoRenewal(dataSource, memberId, operation);
            } catch (SQLException e) {
                throw new DataAccessRollbackException("Cannot " + operation + " for memberId " + memberId, e);
            }
        };
    }
}
