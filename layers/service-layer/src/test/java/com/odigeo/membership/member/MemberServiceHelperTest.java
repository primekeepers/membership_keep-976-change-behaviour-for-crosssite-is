package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingapi.v14.InvalidParametersException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.membership.tracking.BookingTrackingService;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MemberServiceHelperTest {

    @Mock
    private BookingTrackingService bookingTrackingService;
    @Mock
    private MemberAccount memberAccount;
    @Mock
    private TravellerParameter travellerParameter;

    private MemberAccount member_account;
    private final String WEBSITE1 = "ES";
    private static final String MEMBER_NAME = "JOSE";
    private static final String MEMBER_LASTNAME = "GOMEZ";
    private static final String MEMBER_NO_VALID_NAME = "JOSE 123";
    private static final String MEMBER_NAME_WITH_ACCENTS = "JOSÉ's güçò'l.c-garcía";
    private static final long MEMBERSHIP_ID1 = 1L;
    private static final long MEMBERSHIP_ID2 = 2L;
    private static final long MEMBER_ACCOUNT_ID = 1L;
    private static final long USER_ID = 123L;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;

    @BeforeClass
    public void setUp() {
        member_account = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, MEMBER_NAME, MEMBER_LASTNAME);
        openMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            protected void configure() {
                bind(BookingTrackingService.class).toInstance(bookingTrackingService);
            }
        });
    }

    @Test
    public void tesIsMemberOnBoardEmptyTravellersList() {
        assertFalse(MemberServiceUtils.isMemberOnList(memberAccount, List.of()));
    }

    @Test
    public void testIsMemberOnBoard() {
        setupMocks(Boolean.TRUE);
        assertTrue(MemberServiceUtils.isMemberOnList(memberAccount, Collections.singletonList(travellerParameter)));
    }

    @Test
    public void testMemberIsNotOnBoard() {
        setupMocks(Boolean.FALSE);
        assertFalse(MemberServiceUtils.isMemberOnList(memberAccount, Collections.singletonList(travellerParameter)));
    }

    @Test
    public void testNullMemberIsNotOnBoard() {
        setupMocks(Boolean.FALSE);
        assertFalse(MemberServiceUtils.isMemberOnList(null, Collections.singletonList(travellerParameter)));
    }

    @Test
    public void testNullTravellersIsNotOnBoard() {
        setupMocks(Boolean.FALSE);
        assertFalse(MemberServiceUtils.isMemberOnList(memberAccount, null));
    }

    private void setupMocks(boolean isOnBoard) {
        when(memberAccount.getName()).thenReturn(MEMBER_NAME);
        when(memberAccount.getLastNames()).thenReturn(MEMBER_LASTNAME);
        if (isOnBoard) {
            when(travellerParameter.getName()).thenReturn(MEMBER_NAME);
            when(travellerParameter.getLastNames()).thenReturn(MEMBER_LASTNAME);
        } else {
            when(travellerParameter.getName()).thenReturn(MEMBER_NAME);
            when(travellerParameter.getLastNames()).thenReturn(MEMBER_NAME);
        }
    }

    @Test
    public void testCheckValidPersonName() throws InvalidParametersException {
        assertTrue(MemberServiceUtils.isValidPersonName(MEMBER_NAME));
    }

    @Test
    public void testCheckValidPersonNameWithAccents() throws InvalidParametersException {
        assertTrue(MemberServiceUtils.isValidPersonName(MEMBER_NAME_WITH_ACCENTS));
    }

    @Test(dataProvider = "notValidNames", expectedExceptions = InvalidParametersException.class)
    public void testCheckValidPersonNameInvalidName(String name) throws InvalidParametersException {
        assertFalse(MemberServiceUtils.isValidPersonName(name));
    }

    @DataProvider(name = "notValidNames")
    private static Object[][] notValidNames() {
        return new Object[][] {
                new Object[] { MEMBER_NO_VALID_NAME },
                new Object[] { null },
                new Object[] { StringUtils.EMPTY }
        };
    }

    @Test
    public void testIsExpirationDateInPastWithNullExpirationDate() {
        assertFalse(MemberServiceUtils.isExpirationDateInPast(new MembershipBuilder().build()));
    }

    @Test
    public void testIsExpirationDateInPastWithFutureExpirationDate() {
        Membership membership = new MembershipBuilder().setExpirationDate(LocalDateTime.now().plusDays(1)).build();
        assertFalse(MemberServiceUtils.isExpirationDateInPast(membership));
    }

    @Test
    public void testIsExpirationDateInPastWithOldExpirationDate() {
        Membership membership = new MembershipBuilder().setExpirationDate(LocalDateTime.now().minusDays(1)).build();
        assertTrue(MemberServiceUtils.isExpirationDateInPast(membership));
    }

    private List<TravellerParameter> createTravellerList() {
        TravellerParameter travellerParameter = new TravellerParameter();
        travellerParameter.setName(MEMBER_NAME);
        travellerParameter.setLastNames(MEMBER_LASTNAME);
        return Collections.singletonList(travellerParameter);
    }

    @Test
    public void testIsMemberOnListIsOnList() {
        TravellerParameter traveller = new TravellerParameter();
        traveller.setName(getName());
        traveller.setLastNames(getLastnames());
        List<TravellerParameter> travellerList = new ArrayList<>();
        travellerList.add(traveller);
        MemberAccount memberWithAccents = new MemberAccount(0, 0, getNameWithAccents(), getLastnamesWithAccents());
        MemberAccount sameMember = new MemberAccount(0, 0, getName(), getLastnames());

        assertTrue(MemberServiceUtils.isMemberOnList(sameMember, travellerList));
        assertTrue(MemberServiceUtils.isMemberOnList(memberWithAccents, travellerList));
    }

    @Test
    public void testIsMemberOnListNotOnList() {
        String name = "Mario";
        String lastName = "Bros Sanchez";
        TravellerParameter traveller1 = new TravellerParameter();
        traveller1.setLastNames(name.substring(1));
        traveller1.setName(lastName);
        TravellerParameter traveller2 = new TravellerParameter();
        traveller2.setLastNames(name);
        traveller2.setName(lastName.substring(1));
        List<TravellerParameter> travellers = new ArrayList<>();
        travellers.add(traveller1);
        travellers.add(traveller2);
        MemberAccount member = new MemberAccount(0, 0, name, lastName);

        assertFalse(MemberServiceUtils.isMemberOnList(member, travellers));
    }

    private String getNameWithAccents() {
        String lettersWithAccents = "ĂăȘșÀÈÉÌÒÙàèéìòù";
        String spanishLettersWithAccents = "áéíóúüÁÉÍÓÚÜÑñ";
        return lettersWithAccents + spanishLettersWithAccents;
    }

    private String getLastnamesWithAccents() {
        String frenchLettersWithAccents = "ÀàÂâÇçÉéÈèÊêËëÎîÏïÔôÙùÛûÜüŸÿ";
        String italianLettersWithAccents = "ÀáÈÉéÌíìÒóòÙúùü";
        return frenchLettersWithAccents + italianLettersWithAccents;
    }

    private String getName() {
        return "AaSsAEEIOUaeeiouaeiouuAEIOUUNn";
    }

    private String getLastnames() {
        return "AaAaCcEeEeEeEeIiiiooUUUUuuyYaaeeeiiiooouuuu";
    }
}
