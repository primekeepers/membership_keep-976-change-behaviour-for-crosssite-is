package com.odigeo.membership.member.memberapi;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.member.BackOfficeService;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipPostBookingService;
import com.odigeo.membership.member.MembershipValidationService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.membership.member.memberapi.mapper.MembershipMapper;
import com.odigeo.membership.member.memberapi.mapper.UpdateMembershipMapper;
import com.odigeo.membership.member.memberapi.v1.util.MemberServiceMapping;
import com.odigeo.membership.member.memberapi.v1.util.OnboardingMapper;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.onboarding.OnboardingEventService;
import com.odigeo.membership.product.BlacklistPaymentService;
import com.odigeo.membership.propertiesconfig.PropertiesConfigurationService;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.util.CrmCipher;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractController {

    private final Map<Class<?>, Object> mappers;
    private final Map<Class<?>, Object> services;

    protected AbstractController() {
        mappers = new HashMap<>();
        services = new HashMap<>();
    }

    //Services

    protected MembershipValidationService getMembershipValidationService() {
        return getService(MembershipValidationService.class);
    }

    protected MemberService getMemberService() {
        return getService(MemberService.class);
    }

    protected UpdateMembershipService getUpdateMembershipService() {
        return getService(UpdateMembershipService.class);
    }

    protected MemberAccountService getMemberAccountService() {
        return getService(MemberAccountService.class);
    }

    protected BlacklistPaymentService getBlacklistPaymentService() {
        return getService(BlacklistPaymentService.class);
    }

    protected BookingTrackingService getBookingTrackingService() {
        return getService(BookingTrackingService.class);
    }

    protected CrmCipher getCipher() {
        return getService(CrmCipher.class);
    }

    protected BackOfficeService getBackOfficeService() {
        return getService(BackOfficeService.class);
    }

    protected MemberUserAreaService getMemberUserAreaService() {
        return getService(MemberUserAreaService.class);
    }

    protected OnboardingEventService getOnboardingEventService() {
        return getService(OnboardingEventService.class);
    }

    protected MembershipPostBookingService getMembershipPostBookingService() {
        return getService(MembershipPostBookingService.class);
    }

    protected SearchService getSearchService() {
        return getService(SearchService.class);
    }

    protected MemberService getMembershipService() {
        return getService(MemberService.class);
    }

    protected PropertiesConfigurationService getPropertiesConfigurationService() {
        return getService(PropertiesConfigurationService.class);
    }

    //Mappers

    protected MembershipMapper getMembershipMapper() {
        return getMapper(MembershipMapper.class);
    }

    protected MemberServiceMapping getMembershipServiceMapper() {
        return getMapper(MemberServiceMapping.class);
    }

    protected UpdateMembershipMapper getUpdateMembershipMapper() {
        return getMapper(UpdateMembershipMapper.class);
    }

    protected OnboardingMapper getOnboardingMapper() {
        return getMapper(OnboardingMapper.class);
    }


    private <T> T getMapper(Class<T> mapperClass) {
        @SuppressWarnings("unchecked")
        T mapper = (T) mappers.computeIfAbsent(mapperClass, ConfigurationEngine::getInstance);
        return mapper;
    }

    private <T> T getService(Class<T> serviceClass) {
        @SuppressWarnings("unchecked")
        T s = (T) services.computeIfAbsent(serviceClass, ConfigurationEngine::getInstance);
        return s;
    }

}
