package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.request.search.Pagination;
import com.odigeo.membership.request.search.SortCriteria;
import com.odigeo.membership.request.search.Sorting;
import com.odigeo.membership.request.search.SortingField;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.world.MembershipSearchManagementWorld;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@SuppressWarnings("PMD.NullAssignment")
@ScenarioScoped
public class MembershipSearchApiSteps extends CommonSteps {
    private static final BiFunction<Map<String, String>, String, String> NULL_AS_STRING = (searchMap, searchParameter) -> "null".equalsIgnoreCase(searchMap.get(searchParameter)) ? null : searchMap.get(searchParameter);
    private final Function<String, BigDecimal> toBigDecimal = string -> Optional.ofNullable(string).map(BigDecimal::new).orElse(null);
    private final Function<String, Long> toLong = string -> Optional.ofNullable(string).map(Long::valueOf).orElse(null);
    private final Function<String, Integer> toInteger = string -> Optional.ofNullable(string).map(Integer::valueOf).orElse(null);
    private final Function<String, List<Long>> parseId = stringIds -> Arrays.stream(stringIds.split(";")).filter(StringUtils::isNotEmpty).map(Long::valueOf).collect(toList());

    private final MembershipSearchManagementWorld membershipSearchManagementWorld;

    @Inject
    public MembershipSearchApiSteps(ServerConfiguration serverConfiguration, MembershipSearchManagementWorld membershipSearchManagementWorld) {
        super(serverConfiguration);
        this.membershipSearchManagementWorld = membershipSearchManagementWorld;
    }

    @When("^a search membership is requested with following parameters:$")
    public void aSearchMembershipIsRequestedWithFollowingParameters(DataTable searchDataTable) {
        MembershipSearchRequest membershipSearchRequest = convertToSearchRequest(searchDataTable);
        membershipSearchManagementWorld.setMembershipSearchResponse(membershipSearchApi.searchMemberships(membershipSearchRequest));
    }

    @When("^a search member account is requested with following parameters:$")
    public void aSearchMemberAccountIsRequestedWithFollowingParameters(DataTable searchDataTable) {
        MemberAccountSearchRequest memberAccountSearchRequest = convertToAccountSearchRequest(searchDataTable);
        membershipSearchManagementWorld.setMemberAccountResponses(membershipSearchApi.searchAccounts(memberAccountSearchRequest));
    }


    @Then("^the returned memberships by the search should include the memberships with the following ids (.*?)$")
    public void theReturnedMembershipsByTheSearchShouldInclude(String membershipIds) {
        List<Long> memberships = parseId.apply(membershipIds);
        List<MembershipResponse> membershipSearchResponse = membershipSearchManagementWorld.getMembershipSearchResponse();
        assertEquals(membershipSearchResponse.size(), memberships.size());
        assertTrue(membershipSearchResponse.stream().mapToLong(MembershipResponse::getId).allMatch(memberships::contains));
    }

    @Then("^the returned memberships by the search should include the ordered memberships as following (.*?)$")
    public void theReturnedMembershipsByTheSearchShouldIncludeOrdered(String membershipIds) {
        List<Long> memberships = parseId.apply(membershipIds);
        List<MembershipResponse> membershipSearchResponse = membershipSearchManagementWorld.getMembershipSearchResponse();
        assertEquals(membershipSearchResponse.size(), memberships.size());
        assertEquals(membershipIds, membershipSearchResponse.stream().mapToLong(MembershipResponse::getId)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(";")));
    }

    @Then("^the returned memberships by the search (.*?) include status actions in the response$")
    public void theReturnedMembershipsByTheSearchIncludeStatusActionsInTheResponse(String should) {
        List<MembershipResponse> membershipSearchResponse = membershipSearchManagementWorld.getMembershipSearchResponse();
        if ("should".equalsIgnoreCase(should)) {
            assertTrue(membershipSearchResponse.stream().allMatch(membership -> CollectionUtils.isNotEmpty(membership.getMemberStatusActions())));
        } else if ("should not".equalsIgnoreCase(should)) {
            assertTrue(membershipSearchResponse.stream().allMatch(membership -> CollectionUtils.isEmpty(membership.getMemberStatusActions())));
        }
    }

    @Then("^the returned memberships by the search (.*?) include member account in the response$")
    public void theReturnedMembershipsByTheSearchShouldIncludeMemberAccountInTheResponse(String should) {
        List<MembershipResponse> membershipSearchResponse = membershipSearchManagementWorld.getMembershipSearchResponse();
        if ("should".equalsIgnoreCase(should)) {
            assertTrue(membershipSearchResponse.stream().allMatch(membership -> nonNull(membership.getMemberAccount())));
        } else if ("should not".equalsIgnoreCase(should)) {
            assertTrue(membershipSearchResponse.stream().allMatch(membership -> Objects.isNull(membership.getMemberAccount())));
        }
    }

    @Then("^the returned accounts by the search should include the member accounts with the following ids (.*?), and the following memberships (.*?)$")
    public void searchIncludesMemberAccountsAndMembershipIds(String accountIds, String membershipIds) {
        List<Long> memberships = parseId.apply(membershipIds);
        List<Long> memberAccounts = parseId.apply(accountIds);
        List<MemberAccountResponse> memberAccountResponses = membershipSearchManagementWorld.getMemberAccountResponses();
        assertEquals(memberAccountResponses.size(), memberAccounts.size());
        assertTrue(memberAccountResponses.stream().mapToLong(MemberAccountResponse::getId).allMatch(memberAccounts::contains));
        assertTrue(memberAccountResponses.stream()
                .map(MemberAccountResponse::getMemberships)
                .flatMap(List::stream)
                .mapToLong(MembershipResponse::getId)
                .allMatch(memberships::contains));
    }


    private MembershipSearchRequest convertToSearchRequest(DataTable searchDataTable) {
        Map<String, String> searchParameters = searchDataTable.asMaps(String.class, String.class).get(0);
        String sortBy = NULL_AS_STRING.apply(searchParameters, "sortBy");
        String sortCriteria = NULL_AS_STRING.apply(searchParameters, "sortCriteria");
        String limit = NULL_AS_STRING.apply(searchParameters, "limit");
        String offset = NULL_AS_STRING.apply(searchParameters, "offset");
        MembershipSearchRequest.Builder searchRequestBuilder = new MembershipSearchRequest.Builder()
                .autoRenewal(NULL_AS_STRING.apply(searchParameters, "autoRenewal"))
                .fromActivationDate(NULL_AS_STRING.apply(searchParameters, "fromActivationDate"))
                .toActivationDate(NULL_AS_STRING.apply(searchParameters, "toActivationDate"))
                .fromExpirationDate(NULL_AS_STRING.apply(searchParameters, "fromExpirationDate"))
                .toExpirationDate(NULL_AS_STRING.apply(searchParameters, "toExpirationDate"))
                .fromCreationDate(NULL_AS_STRING.apply(searchParameters, "fromCreationDate"))
                .toCreationDate(NULL_AS_STRING.apply(searchParameters, "toCreationDate"))
                .maxBalance(NULL_AS_STRING.andThen(toBigDecimal).apply(searchParameters, "maxBalance"))
                .minBalance(NULL_AS_STRING.andThen(toBigDecimal).apply(searchParameters, "minBalance"))
                .membershipType(NULL_AS_STRING.apply(searchParameters, "membershipType"))
                .monthsDuration(NULL_AS_STRING.andThen(toInteger).apply(searchParameters, "monthsDuration"))
                .productStatus(NULL_AS_STRING.apply(searchParameters, "productStatus"))
                .currencyCode(NULL_AS_STRING.apply(searchParameters, "currencyCode"))
                .status(NULL_AS_STRING.apply(searchParameters, "status"))
                .sourceType(NULL_AS_STRING.apply(searchParameters, "sourceType"))
                .website(NULL_AS_STRING.apply(searchParameters, "website"))
                .memberAccountId(NULL_AS_STRING.andThen(toLong).apply(searchParameters, "memberAccountId"))
                .totalPrice(NULL_AS_STRING.andThen(toBigDecimal).apply(searchParameters, "totalPrice"))
                .email(NULL_AS_STRING.apply(searchParameters, "email"))
                .withMemberAccount(NULL_AS_STRING.andThen(BooleanUtils::toBoolean).apply(searchParameters, "withMemberAccount"))
                .withStatusActions(NULL_AS_STRING.andThen(BooleanUtils::toBoolean).apply(searchParameters, "withStatusActions"));
        if (StringUtils.isNoneBlank(sortBy, sortCriteria)) {
            searchRequestBuilder.sorting(Sorting.builder(SortingField.valueOf(sortBy), SortCriteria.valueOf(sortCriteria)).build());
        }
        if (StringUtils.isNotBlank(limit)) {
            var paginationBuilder = Pagination.builder(Integer.valueOf(limit));
            if (StringUtils.isNotBlank(offset)) {
                paginationBuilder.offset(Integer.valueOf(offset));
            }
            searchRequestBuilder.pagination(paginationBuilder.build());
        }
        MemberAccountSearchRequest memberAccountSearchRequest = convertToAccountSearchRequest(searchDataTable);
        if (Arrays.stream(new Object[]{memberAccountSearchRequest.getFirstName(), memberAccountSearchRequest.getLastName(), memberAccountSearchRequest.getUserId()}).anyMatch(Objects::nonNull)) {
            searchRequestBuilder.memberAccountSearchRequest(memberAccountSearchRequest);
        }
        return searchRequestBuilder.build();
    }

    private MemberAccountSearchRequest convertToAccountSearchRequest(DataTable searchDataTable) {
        Map<String, String> searchParameters = searchDataTable.asMaps(String.class, String.class).get(0);
        return new MemberAccountSearchRequest.Builder()
                .firstName(NULL_AS_STRING.apply(searchParameters, "firstName"))
                .lastName(NULL_AS_STRING.apply(searchParameters, "lastName"))
                .userId(NULL_AS_STRING.andThen(toLong).apply(searchParameters, "userId"))
                .withMemberships(NULL_AS_STRING.andThen(BooleanUtils::toBoolean).apply(searchParameters, "withMemberships"))
                .build();
    }

}
