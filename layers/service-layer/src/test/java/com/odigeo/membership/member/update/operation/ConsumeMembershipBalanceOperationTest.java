package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ConsumeMembershipBalanceOperationTest {
    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipStore membershipStore;
    private ConsumeMembershipBalanceOperation consumeMembershipBalanceOperation;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        consumeMembershipBalanceOperation = new ConsumeMembershipBalanceOperation(dataSource, membershipStore);
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder().build();
        consumeMembershipBalanceOperation.update(updateMembership);
        verify(membershipStore).updateMembershipBalance(dataSource, membership.getId(), BigDecimal.ZERO);
    }

    @Test(expectedExceptions = DataAccessRollbackException.class)
    public void testUpdateWithExceptions() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        when(membershipStore.updateMembershipBalance(eq(dataSource), eq(Long.parseLong(updateMembership.getMembershipId())), eq(BigDecimal.ZERO)))
                .thenThrow(new SQLException());
        consumeMembershipBalanceOperation.update(updateMembership);
    }
}