package com.odigeo.membership.auth.usecases;

import com.google.inject.Inject;
import com.odigeo.authapi.AuthContract;
import com.odigeo.membership.auth.AuthenticationResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public class AuthenticationLogoutUseCaseImpl implements AuthenticationLogoutUseCase {

    @Inject
    private AuthContract authContract;

    @Override
    public AuthenticationResponse logout(String token) throws MembershipAuthServiceException {
        return authContract.logout(token);
    }
}
