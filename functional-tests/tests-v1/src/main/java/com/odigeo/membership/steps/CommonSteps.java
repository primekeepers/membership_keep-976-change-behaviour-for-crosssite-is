package com.odigeo.membership.steps;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.api.client.re3.configuration.AuthenticationClientInterceptor;
import com.odigeo.membership.api.client.re3.configuration.MembershipAuthentication;
import com.odigeo.membership.functionals.config.MembershipRobotsAuthentication;
import com.odigeo.membership.v1.CRMDecryptionApi;
import com.odigeo.membership.v1.MemberUserArea;
import com.odigeo.membership.v1.MembershipBackOfficeService;
import com.odigeo.membership.v1.MembershipPropertiesConfigService;
import com.odigeo.membership.v1.MembershipSearchApi;
import com.odigeo.membership.v1.MembershipService;
import com.odigeo.membership.v1.OnboardingService;
import com.odigeo.membership.v1.PostBookingMemberService;
import com.odigeo.product.MembershipProductServiceV2;

import java.util.Optional;

public class CommonSteps {
    private static final Integer SERVICE_CONNECTION_TIMEOUT_MILLIS = 45000;
    private static final Integer SERVICE_SOCKET_TIMEOUT_MILLIS = 45000;
    private static final String MEMBERSHIP_CONTEXT = "/membership/";
    private static final String MEMBERSHIP_PRODUCT_CONTEXT = "/membership/product";
    final MemberUserArea userAreaService;
    final MembershipService membershipService;
    final MembershipService membershipAuthenticatedService;
    final PostBookingMemberService postBookingService;
    final MembershipBackOfficeService membershipBackOfficeService;
    final MembershipProductServiceV2 membershipProductServiceV2;
    final MembershipSearchApi membershipSearchApi;
    final CRMDecryptionApi crmDecryptionApi;
    final OnboardingService onboardingService;
    final MembershipPropertiesConfigService membershipPropertiesConfigService;

    public CommonSteps(ServerConfiguration serverConfiguration) {
        this.membershipAuthenticatedService = createMembershipAuthenticatedService(serverConfiguration);
        this.membershipService = createService(serverConfiguration, MembershipService.class, MEMBERSHIP_CONTEXT);
        this.userAreaService = createService(serverConfiguration, MemberUserArea.class, MEMBERSHIP_CONTEXT);
        this.postBookingService = createService(serverConfiguration, PostBookingMemberService.class, MEMBERSHIP_CONTEXT);
        this.membershipBackOfficeService = createService(serverConfiguration, MembershipBackOfficeService.class, MEMBERSHIP_CONTEXT);
        this.membershipProductServiceV2 = createService(serverConfiguration, MembershipProductServiceV2.class, MEMBERSHIP_PRODUCT_CONTEXT);
        this.crmDecryptionApi = createService(serverConfiguration, CRMDecryptionApi.class, MEMBERSHIP_CONTEXT);
        this.onboardingService = createService(serverConfiguration, OnboardingService.class, MEMBERSHIP_CONTEXT);
        this.membershipSearchApi = createService(serverConfiguration, MembershipSearchApi.class, MEMBERSHIP_CONTEXT);
        this.membershipPropertiesConfigService = createService(serverConfiguration, MembershipPropertiesConfigService.class, MEMBERSHIP_CONTEXT);
    }

    private MembershipService createMembershipAuthenticatedService(ServerConfiguration serverConfiguration) {
        final ServiceBuilder<MembershipService> serviceBuilder = new ServiceBuilder<>(MembershipService.class, "http://" + serverConfiguration.getMembershipServer() + MEMBERSHIP_CONTEXT, new SimpleRestErrorsHandler(MembershipService.class));
        serviceBuilder.withConnectionConfiguration(getConnectionConfiguration());
        InterceptorConfiguration<MembershipService> interceptorConfiguration = new InterceptorConfiguration<>();
        Optional.ofNullable(ConfigurationEngine.getInstance(MembershipRobotsAuthentication.class))
                .map(properties -> new MembershipAuthentication(properties.getClient(), properties.getSecretKey()))
                .map(AuthenticationClientInterceptor::new)
                .map(interceptorConfiguration::addInterceptor)
                .map(serviceBuilder::withInterceptorConfiguration);
        return serviceBuilder.build();
    }

    private ConnectionConfiguration getConnectionConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(SERVICE_CONNECTION_TIMEOUT_MILLIS)
                .socketTimeoutInMillis(SERVICE_SOCKET_TIMEOUT_MILLIS)
                .build();
    }

    private <T> T createService(ServerConfiguration serverConfiguration, Class<T> serviceClass, String context) {
        String url = "http://" + serverConfiguration.getMembershipServer() + context;
        return new ServiceBuilder<>(serviceClass, url, new SimpleRestErrorsHandler(serviceClass))
                .withConnectionConfiguration(getConnectionConfiguration())
                .build();
    }
}
