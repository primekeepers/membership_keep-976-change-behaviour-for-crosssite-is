package com.odigeo.membership.member.nosql;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.redis.RedisClient;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.params.SetParams;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipNoSQLStoreTest {

    private static final String KEY = "key";
    private static final String VALUE = "value";
    private static final String ERROR = "error";
    private static final int SECONDS_TO_EXPIRE = 7200;

    @Mock
    private RedisClient redisClient;
    @Mock
    private Jedis jedis;

    private MembershipNoSQLStore membershipNoSQLStore;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipNoSQLStore = new MembershipNoSQLStore(redisClient);
    }

    @Test
    public void testStoreOK() throws DataAccessException {
        when(redisClient.getJedisConnection()).thenReturn(jedis);
        membershipNoSQLStore.store(KEY, VALUE, SECONDS_TO_EXPIRE);
        verify(jedis).set(anyString(), anyString(), any(SetParams.class));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testStoreFail() throws DataAccessException {
        when(redisClient.getJedisConnection()).thenThrow(new JedisConnectionException(ERROR));
        membershipNoSQLStore.store(KEY, VALUE, SECONDS_TO_EXPIRE);
    }

    @Test
    public void testGetOK() throws DataAccessException {
        when(redisClient.getJedisConnection()).thenReturn(jedis);
        membershipNoSQLStore.get(KEY);
        verify(jedis).get(KEY);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetFail() throws DataAccessException {
        when(redisClient.getJedisConnection()).thenThrow(new JedisConnectionException(ERROR));
        membershipNoSQLStore.get(KEY);
    }
}