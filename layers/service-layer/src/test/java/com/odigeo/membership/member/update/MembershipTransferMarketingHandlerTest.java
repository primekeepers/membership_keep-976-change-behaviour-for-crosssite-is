package com.odigeo.membership.member.update;

import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipTransferMarketingHandlerTest {

    private static final long OLD_USER_ID = 123L;
    private static final long NEW_USER_ID = 456L;
    private static final String WEBSITE_ES = "ES";
    private static final String WEBSITE_IT = "IT";
    private static final Long MEMBERSHIP_ID1 = 325L;
    private static final Long MEMBERSHIP_ID2 = 778L;
    private static final String OLD_USER_EMAIL = "old_user@odigeo.com";
    private static final String NEW_USER_EMAIL = "new_user@odigeo.com";
    private static final Membership activeMembershipES = new MembershipBuilder().setId(MEMBERSHIP_ID1).setWebsite(WEBSITE_ES).setStatus(MemberStatus.ACTIVATED).build();
    private static final Membership ptcMembershipIT = new MembershipBuilder().setId(MEMBERSHIP_ID2).setWebsite(WEBSITE_IT).setStatus(MemberStatus.PENDING_TO_COLLECT).build();

    private static final UserInfo oldUserInfo = UserInfo.fromUserBasicInfo(getUserBasicInfo(OLD_USER_EMAIL, OLD_USER_ID));
    private static final UserInfo newUserInfo = UserInfo.fromUserBasicInfo(getUserBasicInfo(NEW_USER_EMAIL, NEW_USER_ID));

    @Mock
    private UserApiManager userApiManager;
    @Mock
    private MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;

    private MembershipTransferMarketingHandler membershipTransferMarketingHandler;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        doCallRealMethod().when(memberStatusToCrmStatusMapper).map(any(MemberStatus.class));
        membershipTransferMarketingHandler = new MembershipTransferMarketingHandler(userApiManager, memberStatusToCrmStatusMapper, membershipMessageSendingManager);
    }

    @Test(dataProvider = "onlyOneActiveOrPTcMembership")
    public void testSubscribeNewUserAndUnsubscribeOldUser_OnlyOneActiveOrPtcMembership(Membership membership, SubscriptionStatus newUsersSubscriptionStatus) {
        List<Membership> newUsersActiveAndPtcMemberships = Collections.singletonList(membership);
        Set<String> oldUsersActiveWebsites = Collections.emptySet();
        mockUserInfoResponses();
        membershipTransferMarketingHandler.subscribeNewUserAndUnsubscribeOldUser(OLD_USER_ID, NEW_USER_ID, newUsersActiveAndPtcMemberships, oldUsersActiveWebsites);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(newUserInfo, membership, newUsersSubscriptionStatus);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(oldUserInfo, membership, SubscriptionStatus.UNSUBSCRIBED);
    }

    @DataProvider(name = "onlyOneActiveOrPTcMembership")
    private static Object[][] onlyOneActiveOrPTcMembership() {
        return new Object[][] {
                new Object[] { ptcMembershipIT, SubscriptionStatus.PENDING },
                new Object[] { activeMembershipES, SubscriptionStatus.SUBSCRIBED }
        };
    }

    @Test
    public void testSubscribeNewUserAndUnsubscribeOldUser_OldUserHasAnotherActiveMembership() {
        List<Membership> newUsersActiveAndPtcMemberships = Collections.singletonList(activeMembershipES);
        Set<String> oldUsersActiveWebsites = Collections.singleton(WEBSITE_ES);
        mockUserInfoResponses();
        membershipTransferMarketingHandler.subscribeNewUserAndUnsubscribeOldUser(OLD_USER_ID, NEW_USER_ID, newUsersActiveAndPtcMemberships, oldUsersActiveWebsites);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(newUserInfo, activeMembershipES, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManager, never()).sendSubscriptionMessageToCRMTopic(oldUserInfo, activeMembershipES, SubscriptionStatus.UNSUBSCRIBED);
    }

    @Test
    public void testSubscribeNewUserAndUnsubscribeOldUser_multipleMemberships() {
        List<Membership> newUsersActiveAndPtcMemberships = Arrays.asList(activeMembershipES, ptcMembershipIT);
        Set<String> oldUsersActiveWebsites = Collections.singleton(WEBSITE_ES);
        mockUserInfoResponses();
        membershipTransferMarketingHandler.subscribeNewUserAndUnsubscribeOldUser(OLD_USER_ID, NEW_USER_ID, newUsersActiveAndPtcMemberships, oldUsersActiveWebsites);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(newUserInfo, activeMembershipES, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(newUserInfo, ptcMembershipIT, SubscriptionStatus.PENDING);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(oldUserInfo, ptcMembershipIT, SubscriptionStatus.UNSUBSCRIBED);
        verify(membershipMessageSendingManager, never()).sendSubscriptionMessageToCRMTopic(oldUserInfo, activeMembershipES, SubscriptionStatus.UNSUBSCRIBED);
    }

    private void mockUserInfoResponses() {
        when(userApiManager.getUserInfo(OLD_USER_ID)).thenReturn(oldUserInfo);
        when(userApiManager.getUserInfo(NEW_USER_ID)).thenReturn(newUserInfo);
    }

    private static UserBasicInfo getUserBasicInfo(String email, long userId) {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setEmail(email);
        userBasicInfo.setId(userId);
        return userBasicInfo;
    }
}