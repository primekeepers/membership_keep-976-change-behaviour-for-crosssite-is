package com.odigeo.membership.tracking;

import com.google.inject.Singleton;
import com.odigeo.membership.BookingTracking;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Singleton
public class BookingTrackingStore {

    private static final String INSERT_BOOKING_TRACKING = "INSERT INTO GE_MEMBERSHIP_BOOKING_TRACKING (BOOKING_ID, MEMBERSHIP_ID, BOOKING_DATE, AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_FEE_AMOUNT, TIMESTAMP) "
            + " VALUES (?, ?, ?, ?, ?, ?, SYSDATE) ";

    private static final String DELETE_BOOKING_TRACKING = "DELETE FROM GE_MEMBERSHIP_BOOKING_TRACKING WHERE BOOKING_ID = ?";

    private static final String GET_TRACKED_BOOKING = "SELECT BOOKING_ID, MEMBERSHIP_ID, BOOKING_DATE, AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_FEE_AMOUNT FROM GE_MEMBERSHIP_BOOKING_TRACKING WHERE BOOKING_ID = ?";
    private static final String GET_TRACKED_BOOKING_BY_MEMBERSHIP_ID = "SELECT BOOKING_ID, MEMBERSHIP_ID, BOOKING_DATE, AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_FEE_AMOUNT FROM GE_MEMBERSHIP_BOOKING_TRACKING WHERE MEMBERSHIP_ID = ?";

    private static final String GET_MEMBERSHIP_RULES = "SELECT NUM_BOOKINGS, NUM_DAYS, IS_ACTIVE FROM MS_MEMBERSHIP_RULES";

    private static final String GET_TOTAL_TRACKED_BOOKINGS = "SELECT COUNT(*) AS TOTAL_BOOKINGS FROM GE_MEMBERSHIP_BOOKING_TRACKING WHERE MEMBERSHIP_ID = ? AND BOOKING_DATE > SYSDATE - ?";

    public boolean trackBooking(DataSource dataSource, BookingTracking bookingTracking) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT_BOOKING_TRACKING)) {
            ps.setLong(1, bookingTracking.getBookingId());
            ps.setLong(2, bookingTracking.getMembershipId());
            ps.setTimestamp(3, Timestamp.valueOf(bookingTracking.getBookingDate()));
            ps.setBigDecimal(4, bookingTracking.getAvoidFeeAmount());
            ps.setBigDecimal(5, bookingTracking.getCostFeeAmount());
            ps.setBigDecimal(6, bookingTracking.getPerksAmount());
            return ps.executeUpdate() > 0;
        }
    }

    public boolean removeTrackedBooking(DataSource dataSource, Long bookingId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(DELETE_BOOKING_TRACKING)) {
            ps.setLong(1, bookingId);
            return ps.executeUpdate() > 0;
        }
    }

    public boolean isBookingTracked(DataSource dataSource, Long bookingId) throws SQLException {
        return Objects.nonNull(getBookingTracked(dataSource, bookingId));
    }

    public BookingTracking getBookingTracked(DataSource dataSource, Long bookingId) throws SQLException {
        BookingTracking bookingTracking = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(GET_TRACKED_BOOKING)) {
            ps.setLong(1, bookingId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    bookingTracking = createBookingTracking(rs);
                }
            }
        }
        return bookingTracking;
    }

    public Collection<BookingTracking> getBookingTrackedByMembershipId(DataSource dataSource, Long membershipId) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(GET_TRACKED_BOOKING_BY_MEMBERSHIP_ID)) {
            ps.setLong(1, membershipId);
            try (ResultSet rs = ps.executeQuery()) {
                Collection<BookingTracking> bookingTracking = new ArrayList<>();
                while (rs.next()) {
                    bookingTracking.add(createBookingTracking(rs));
                }
                return bookingTracking;
            }
        }
    }


    private BookingTracking createBookingTracking(ResultSet rs) throws SQLException {
        BookingTracking bookingTracking = new BookingTracking();
        bookingTracking.setMembershipId(rs.getLong("MEMBERSHIP_ID"));
        bookingTracking.setBookingId(rs.getLong("BOOKING_ID"));
        bookingTracking.setBookingDate(rs.getTimestamp("BOOKING_DATE").toLocalDateTime());
        bookingTracking.setAvoidFeeAmount(rs.getBigDecimal("AVOID_FEE_AMOUNT"));
        bookingTracking.setCostFeeAmount(rs.getBigDecimal("COST_FEE_AMOUNT"));
        bookingTracking.setPerksAmount(rs.getBigDecimal("PERKS_FEE_AMOUNT"));
        return bookingTracking;
    }

    public boolean isBookingLimitReached(DataSource dataSource, Long memberId) throws SQLException {
        long bookingLimit = 1;
        int daysLimit = 1;
        int ruleActive = 0;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(GET_MEMBERSHIP_RULES);
             ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                bookingLimit = rs.getLong("NUM_BOOKINGS");
                daysLimit = rs.getInt("NUM_DAYS");
                ruleActive = rs.getInt("IS_ACTIVE");
            }
        }

        if (ruleActive > 0) {
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement ps = connection.prepareStatement(GET_TOTAL_TRACKED_BOOKINGS)) {
                ps.setLong(1, memberId);
                ps.setInt(2, daysLimit);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        int totalBookings = rs.getInt("TOTAL_BOOKINGS");
                        return bookingLimit <= totalBookings;
                    }
                    return false;
                }
            }
        }
        return false;
    }

}
