package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.BlacklistedPaymentMethod;
import com.odigeo.membership.exception.DataAccessRollbackException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.sql.Statement.EXECUTE_FAILED;
import static java.sql.Statement.SUCCESS_NO_INFO;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PaymentMethodsBlacklistStoreTest {

    private static final String ERROR_TYPE = "error_type";
    public static final String ERROR_MSG = "ERROR_MSG";
    private static final Long ID_PAYMENT_METHOD = 123L;
    private static final Long MEMBERSHIP_ID = 12345L;
    private static final Date TIMESTAMP = new Date();
    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;
    private List<BlacklistedPaymentMethod> blacklistedPaymentMethods;

    private PaymentMethodsBlacklistStore paymentMethodsBlacklistStore;

    @BeforeMethod
    public void setUp() throws SQLException {
        openMocks(this);
        mockDatabase();
        blacklistedPaymentMethods = Collections.singletonList(createBlackListedPaymentMethods());
        paymentMethodsBlacklistStore = new PaymentMethodsBlacklistStore();
    }

    @Test
    public void testFetchBlackListedPaymentMethods() throws DataAccessException, SQLException {
        mockSuccessfulExecution();
        List<BlacklistedPaymentMethod> blacklistedPaymentMethods = paymentMethodsBlacklistStore.fetchBlackListedPaymentMethods(dataSource, MEMBERSHIP_ID);
        final BlacklistedPaymentMethod blacklistedPaymentMethodFromDb = blacklistedPaymentMethods.get(0);
        final BlacklistedPaymentMethod blacklistedPaymentMethodExpected = this.blacklistedPaymentMethods.get(0);
        assertEquals(blacklistedPaymentMethodFromDb, blacklistedPaymentMethodExpected);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testFetchBlackListedPaymentMethodsException() throws DataAccessException, SQLException {
        when(resultSet.next()).thenThrow(new SQLException());
        paymentMethodsBlacklistStore.fetchBlackListedPaymentMethods(dataSource, MEMBERSHIP_ID);
    }

    @Test
    public void testAddToBlackList() throws DataAccessException, SQLException {
        mockSuccessfulExecution();
        Boolean blackListAdded = paymentMethodsBlacklistStore.addToBlackList(dataSource, blacklistedPaymentMethods);
        assertTrue(blackListAdded);
    }

    @Test
    public void testAddToBlackListFailure() throws SQLException, DataAccessException {
        mockFailureExecution();
        Boolean blackListAdded = paymentMethodsBlacklistStore.addToBlackList(dataSource, blacklistedPaymentMethods);
        assertFalse(blackListAdded);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testAddToBlackListThrowDataAccessException() throws SQLException, DataAccessException {
        when(preparedStatement.executeBatch()).thenThrow(SQLException.class);
        paymentMethodsBlacklistStore.addToBlackList(dataSource, blacklistedPaymentMethods);
    }

    @Test
    public void testAddToBlackListSuccessNoInfo() throws DataAccessException, SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeBatch()).thenReturn(new int[] {(EXECUTE_FAILED)});
        Boolean blackListAdded = paymentMethodsBlacklistStore.addToBlackList(dataSource, blacklistedPaymentMethods);
        assertFalse(blackListAdded);
    }

    @Test(expectedExceptions = DataAccessRollbackException.class)
    public void testAddToBlackListException() throws DataAccessException, SQLException {
        when(dataSource.getConnection()).thenThrow(new SQLException());
        paymentMethodsBlacklistStore.addToBlackList(dataSource, blacklistedPaymentMethods);
    }

    private BlacklistedPaymentMethod createBlackListedPaymentMethods() {
        BlacklistedPaymentMethod blackListedPaymentMethod = new BlacklistedPaymentMethod();
        blackListedPaymentMethod.setErrorType(ERROR_TYPE);
        blackListedPaymentMethod.setErrorMessage(ERROR_MSG);
        blackListedPaymentMethod.setTimestamp(TIMESTAMP);
        blackListedPaymentMethod.setId(ID_PAYMENT_METHOD);
        blackListedPaymentMethod.setMembershipId(MEMBERSHIP_ID);
        return blackListedPaymentMethod;
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    private void mockSuccessfulExecution() throws SQLException {
        when(preparedStatement.executeBatch()).thenReturn(new int[]{SUCCESS_NO_INFO});
        when(resultSet.next()).thenReturn(Boolean.TRUE).thenReturn(Boolean.FALSE);
        when(resultSet.getString("ERROR_MESSAGE")).thenReturn(ERROR_MSG);
        when(resultSet.getString(eq("ERROR_TYPE"))).thenReturn(ERROR_TYPE);
        when(resultSet.getTimestamp(eq("TIMESTAMP"))).thenReturn(new Timestamp(TIMESTAMP.getTime()));
        when(resultSet.getLong(eq("ID"))).thenReturn(ID_PAYMENT_METHOD);
    }

    private void mockFailureExecution() throws SQLException {
        when(preparedStatement.executeBatch()).thenReturn(new int[]{EXECUTE_FAILED});
    }
}