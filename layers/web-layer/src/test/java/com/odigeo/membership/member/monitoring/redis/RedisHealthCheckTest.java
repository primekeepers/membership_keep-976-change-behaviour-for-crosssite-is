package com.odigeo.membership.member.monitoring.redis;

import com.codahale.metrics.health.HealthCheck;
import com.odigeo.membership.redis.RedisClient;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class RedisHealthCheckTest {

    private static final String UP_MESSAGE = "Redis is up. URL: ";
    private static final String DOWN_MESSAGE = "Redis is down. URL: ";
    private static final String PONG = "PONG";
    private static final String HOST = "host";
    private static final int PORT = 6379;
    private static final String ERROR = "ERROR";

    @Mock
    private RedisClient redisClient;
    @Mock
    private Jedis jedis;

    private RedisHealthCheck redisHealthCheck;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        this.redisHealthCheck = new RedisHealthCheck(redisClient);
    }

    @Test
    public void testCheckPingOk() {
        setUpRedisClient(Boolean.TRUE);
        HealthCheck.Result result = redisHealthCheck.check();
        assertEquals(result.getMessage(), HealthCheck.Result.healthy(UP_MESSAGE)
                .getMessage() + HOST + ":" + PORT);
    }

    private void setUpRedisClient(final boolean answerIsPong) {
        when(redisClient.getJedisConnection()).thenReturn(jedis);
        when(redisClient.getRedisClientConfiguration()).thenReturn(HOST + ":" + PORT);
        if (answerIsPong) {
            when(jedis.ping()).thenReturn(PONG);
        } else {
            when(jedis.ping()).thenReturn(StringUtils.EMPTY);
        }
    }

    @Test
    public void testCheckPingFail() {
        setUpRedisClient(Boolean.FALSE);
        HealthCheck.Result result = redisHealthCheck.check();
        assertEquals(result.getMessage(), HealthCheck.Result.healthy(DOWN_MESSAGE)
                .getMessage() + HOST + ":" + PORT);
    }

    @Test
    public void testCheckFail() {
        setUpRedisClientException();
        when(redisClient.getRedisClientConfiguration()).thenReturn(HOST + ":" + PORT);
        HealthCheck.Result result = redisHealthCheck.check();
        assertEquals(result.getMessage(), HealthCheck.Result.healthy(DOWN_MESSAGE)
                .getMessage() + HOST + ":" + PORT);
    }

    private void setUpRedisClientException() {
        when(redisClient.getJedisConnection()).thenThrow(new JedisConnectionException(ERROR));
    }
}