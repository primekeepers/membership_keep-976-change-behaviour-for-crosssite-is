package com.odigeo.membership.member.statusaction;

import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.member.MemberStatusActionStore;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;

public class MemberStatusActionServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberStatusActionStore memberStatusActionStoreMock;
    @InjectMocks
    private MemberStatusActionServiceBean memberStatusActionServiceBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
    }

    @Test
    public void testMostRecentStatusIsReturnedWhenFindLastStatusAction() throws SQLException {
        //Given
        final Long membershipId = 4324L;
        final Optional<MemberStatusAction> newMemberStatusAction = sampleMemberStatusAction(1L, membershipId, "2019-10-10");
        final Optional<MemberStatusAction> oldMemberStatusAction = sampleMemberStatusAction(2L, membershipId, "2018-10-10");
        given(memberStatusActionStoreMock.selectLastStatusActionByMembershipId(eq(dataSource), eq(membershipId))).willReturn(newMemberStatusAction).willReturn(oldMemberStatusAction);
        //When
        Optional<MemberStatusAction> memberStatusAction = memberStatusActionServiceBean.lastStatusActionByMembershipId(membershipId);
        //Then
        Assert.assertEquals(memberStatusAction, newMemberStatusAction);
    }

    @Test
    public void testEmptyReturnedWhenFindLastStatusActionDoesNotReturnAnything() throws SQLException {
        //Given
        final Long membershipId = 663L;
        given(memberStatusActionStoreMock.selectLastStatusActionByMembershipId(eq(dataSource), eq(membershipId))).willReturn(Optional.empty());
        //When
        Optional<MemberStatusAction> memberStatusAction = memberStatusActionServiceBean.lastStatusActionByMembershipId(membershipId);
        //Then
        assertFalse(memberStatusAction.isPresent());
    }

    @Test
    public void testEmptyReturnedWhenFindLastStatusActionFails() throws SQLException {
        //Given
        final Long membershipId = 993L;
        given(memberStatusActionStoreMock.selectLastStatusActionByMembershipId(any(DataSource.class), eq(membershipId))).willThrow(new SQLException("expected exception"));
        //When
        Optional<MemberStatusAction> memberStatusAction = memberStatusActionServiceBean.lastStatusActionByMembershipId(membershipId);
        //Then
        assertFalse(memberStatusAction.isPresent());
    }

    private Optional<MemberStatusAction> sampleMemberStatusAction(final Long memberStatusActionId, final Long membershipId, final String actionDate) {
        return Optional.of(new MemberStatusAction(memberStatusActionId, membershipId, randomStatusAction(), Date.from(Instant.parse(actionDate.concat("T00:00:00.00Z")))));
    }

    private StatusAction randomStatusAction() {
        List<StatusAction> allStatusActions = Arrays.asList(StatusAction.values());
        Collections.shuffle(allStatusActions);
        return allStatusActions.stream().findFirst().orElse(StatusAction.ACTIVATION);
    }
}