package com.odigeo.membership.member.memberapi.util;

import com.edreams.base.DataAccessException;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class LogUtilsTest {

    private static final String TEST_MESSAGE = "Test message";
    private static final String SEPARATOR = "\n\t";
    private static final String EXCEPTION_MESSAGE = "Arg invalid!";
    private Logger logger = LoggerFactory.getLogger(LogUtilsTest.class);
    @Mock
    private Appender mockAppender;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        org.apache.log4j.Logger.getRootLogger().addAppender(mockAppender);

    }

    @Test
    public void testSmallLog() {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException(EXCEPTION_MESSAGE);
        String smallLogMessage = TEST_MESSAGE + SEPARATOR + illegalArgumentException.getMessage();
        LogUtils.smallLog(logger, TEST_MESSAGE, illegalArgumentException);
        verifyLoggerMessageAndLevel(smallLogMessage);
    }

    @Test
    public void testDefaultLoggerError() {
        DataAccessException dataAccessException = new DataAccessException(EXCEPTION_MESSAGE);
        String defaultErrorMessage = "DataAccessException : testDefaultLoggerError " + TEST_MESSAGE;
        LogUtils.defaultLoggerError(logger, dataAccessException, TEST_MESSAGE);
        verifyLoggerMessageAndLevel(defaultErrorMessage);
    }

    @Test
    public void testDefaultLoggerErrorWithoutMessage() {
        DataAccessException dataAccessException = new DataAccessException(EXCEPTION_MESSAGE);
        String defaultErrorMessage = "DataAccessException : testDefaultLoggerErrorWithoutMessage ";
        LogUtils.defaultLoggerError(logger, dataAccessException);
        verifyLoggerMessageAndLevel(defaultErrorMessage);
    }

    private void verifyLoggerMessageAndLevel(String defaultErrorMessage) {
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertEquals(loggingEvent.getMessage(), defaultErrorMessage);
        assertEquals(Level.ERROR, loggingEvent.getLevel());
    }
}
