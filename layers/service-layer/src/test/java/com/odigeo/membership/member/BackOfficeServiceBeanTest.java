package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertTrue;

public class BackOfficeServiceBeanTest {

    private static final String SITE_ES = "ES";
    private static final String EMAIL = "test@edreams.com";
    private static final Long USER_ID = 765L;
    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long MEMBER_ACCOUNT_ID = 443L;
    private static final Long BOOKING_ID = 982L;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now().plusMonths(6);
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, "", "");
    private static final String MEMBERSHIP_NOT_FOUND = "Membership not found";
    private static final String DATA_ACCESS_EXCEPTION = "Data access exception";

    @Mock
    private Membership membershipMock;
    @Mock
    private MemberService memberServiceMock;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManagerMock;

    @InjectMocks
    private BackOfficeServiceBean backOfficeServiceBean = new BackOfficeServiceBean();

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(MemberService.class).toInstance(memberServiceMock);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManagerMock);
    }

    @Test
    public void testUpdateMembershipMarketingInfoMemberActivated() throws MissingElementException, DataAccessException {
        // Given
        mockMembership(MemberStatus.ACTIVATED);
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(membershipMock);
        // When
        backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
        // Then
        verify(membershipMessageSendingManagerMock).sendSubscriptionMessageToCRMTopic(eq(membershipMock), eq(SubscriptionStatus.SUBSCRIBED));
    }

    @Test
    public void testUpdateMembershipMarketingInfoMemberDeactivated() throws MissingElementException, DataAccessException {
        // Given
        mockMembership(MemberStatus.DEACTIVATED);
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(membershipMock);
        // When
        backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
        // Then
        verify(membershipMessageSendingManagerMock).sendSubscriptionMessageToCRMTopic(eq(membershipMock), eq(SubscriptionStatus.UNSUBSCRIBED));
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testUpdateMembershipMarketingInfoMemberNotFound() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(null);
        // When
        backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testUpdateMembershipMarketingInfoMemberServiceThrowsMissingElementException() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenThrow(new MissingElementException(MEMBERSHIP_NOT_FOUND));
        // When
        backOfficeServiceBean.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateMembershipMarketingInfoMemberServiceThrowsDataAccessException() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenThrow(new DataAccessException(DATA_ACCESS_EXCEPTION));
        // When
        backOfficeServiceBean.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID);
    }

    @Test
    public void testSendWelcomeEmailSuccess() throws MissingElementException, DataAccessException {
        // Given
        mockMembership(MemberStatus.DEACTIVATED);
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(membershipMock);

        // When
        assertTrue(backOfficeServiceBean.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID));
        verify(membershipMessageSendingManagerMock).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(membershipMock, BOOKING_ID, true);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testSendWelcomeEmailMembershipNotFound() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(null);
        // When
        backOfficeServiceBean.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testSendWelcomeEmailMembershipServiceThrowsMissingElementException() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenThrow(new MissingElementException(MEMBERSHIP_NOT_FOUND));
        // When
        backOfficeServiceBean.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSendWelcomeEmailMembershipServiceThrowsDataAccessException() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenThrow(new DataAccessException(DATA_ACCESS_EXCEPTION));
        // When
        backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
    }

    private void mockMembership(MemberStatus status) {
        when(membershipMock.getStatus()).thenReturn(status);
        when(membershipMock.getWebsite()).thenReturn(SITE_ES);
        when(membershipMock.getActivationDate()).thenReturn(ACTIVATION_DATE);
        when(membershipMock.getExpirationDate()).thenReturn(EXPIRATION_DATE);
        when(membershipMock.getMemberAccount()).thenReturn(MEMBER_ACCOUNT);
    }
}
