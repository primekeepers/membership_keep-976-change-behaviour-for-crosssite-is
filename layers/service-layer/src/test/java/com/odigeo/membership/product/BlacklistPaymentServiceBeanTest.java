package com.odigeo.membership.product;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.BlacklistedPaymentMethod;
import com.odigeo.membership.member.PaymentMethodsBlacklistStore;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BlacklistPaymentServiceBeanTest {

    private static final Long TEST_ID = 1L;
    private static final DataAccessException DATA_ACCESS_EX = new DataAccessException(new Exception());

    @Mock
    private DataSource dataSource;
    @Mock
    private PaymentMethodsBlacklistStore paymentMethodsBlacklistStoreMock;
    private BlacklistedPaymentMethod blacklistedPaymentMethod;
    @InjectMocks
    private BlacklistPaymentService blackListPaymentService = new BlacklistPaymentServiceBean();

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        blacklistedPaymentMethod = new BlacklistedPaymentMethod();
    }

    private void configure(Binder binder) {
        binder.bind(PaymentMethodsBlacklistStore.class).toInstance(paymentMethodsBlacklistStoreMock);
    }

    @Test
    public void testGetBlacklistedPaymentMethods() throws DataAccessException {
        when(paymentMethodsBlacklistStoreMock.fetchBlackListedPaymentMethods(eq(dataSource), eq(TEST_ID)))
                .thenReturn(Collections.singletonList(blacklistedPaymentMethod));
        List<BlacklistedPaymentMethod> blacklistedPaymentMethods = blackListPaymentService.getBlacklistedPaymentMethods(TEST_ID);
        assertEquals(blacklistedPaymentMethods.get(0), blacklistedPaymentMethod);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetBlacklistedPaymentMethodsThrowsException() throws DataAccessException {
        when(paymentMethodsBlacklistStoreMock.fetchBlackListedPaymentMethods(eq(dataSource), eq(TEST_ID)))
                .thenThrow(DATA_ACCESS_EX);
        blackListPaymentService.getBlacklistedPaymentMethods(TEST_ID);
    }

    @Test
    public void testAddToBlackList() throws DataAccessException {
        when(paymentMethodsBlacklistStoreMock.addToBlackList(eq(dataSource), anyList()))
                .thenReturn(Boolean.TRUE);
        assertTrue(blackListPaymentService.addToBlackList(Collections.singletonList(blacklistedPaymentMethod)));
        when(paymentMethodsBlacklistStoreMock.addToBlackList(eq(dataSource), anyList()))
                .thenReturn(Boolean.FALSE);
        assertFalse(blackListPaymentService.addToBlackList(Collections.singletonList(blacklistedPaymentMethod)));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testAddToBlackListThrowsException() throws DataAccessException {
        when(paymentMethodsBlacklistStoreMock.addToBlackList(eq(dataSource), anyList()))
                .thenThrow(DATA_ACCESS_EX);
        blackListPaymentService.addToBlackList(Collections.singletonList(blacklistedPaymentMethod));
    }
}
