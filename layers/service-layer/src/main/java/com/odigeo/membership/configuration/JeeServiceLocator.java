package com.odigeo.membership.configuration;

import org.apache.log4j.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * This class is a locator for services implemented as EJB, based on JNDI searches in
 * the JEE environment.
 */
public final class JeeServiceLocator implements ServiceLocator {

    public static final String JNDI_APP_NAME = "java:app/AppName";

    private InitialContext jndiContext;

    private static final Logger logger = Logger.getLogger(JeeServiceLocator.class);
    private static final JeeServiceLocator THE_INSTANCE = new JeeServiceLocator();

    private String servicesContextWithSlashes;

    ServiceNameResolver serviceNameResolver = ServiceNameResolver.getInstance();

    private JeeServiceLocator() {
        try {
            initContext();
        } catch (NamingException nex) {
            logger.error("Unable to find initial JNDI context.", nex);
        }
    }

    public static JeeServiceLocator getInstance() {
        return THE_INSTANCE;
    }

    private void initContext() throws NamingException {
        if (jndiContext == null) {
            jndiContext = new InitialContext();
            String applicationName = getApplicationName();
            servicesContextWithSlashes = serviceNameResolver.resolveServiceContext(applicationName);
        }
    }

    public String getApplicationName() throws NamingException {
        return (String) jndiContext.lookup(JNDI_APP_NAME);
    }

    private Object getServiceByName(final String name) throws NamingException {
        initContext();
        return jndiContext.lookup(name);
    }

    public <T> T getService(final Class<T> serviceType) throws UnavailableServiceException {
        String jndiServiceName = serviceNameResolver.resolveServiceName(serviceType, servicesContextWithSlashes);
        try {
            return (T) getServiceByName(jndiServiceName);
        } catch (NamingException nex) {
            throw new UnavailableServiceException(serviceType, nex);
        }
    }

}
