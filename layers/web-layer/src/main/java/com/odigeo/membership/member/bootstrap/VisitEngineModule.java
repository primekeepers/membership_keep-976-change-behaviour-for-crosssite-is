package com.odigeo.membership.member.bootstrap;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.DefaultRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;
import com.odigeo.visitengineapi.v1.VisitEngine;

public class VisitEngineModule extends DefaultRestUtilsModule<VisitEngine> {

    public VisitEngineModule(ServiceNotificator... serviceNotificators) {
        super(VisitEngine.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<VisitEngine> getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpDefault(VisitEngine.class);
    }
}
