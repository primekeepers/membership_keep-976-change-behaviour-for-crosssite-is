package com.odigeo.membership.member.memberapi.v1.util;

import com.google.common.collect.ImmutableMap;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.odigeo.membership.enums.MembershipType.BASIC_FREE;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public final class MembershipCreationRequestMapper {

    private static final String WEBSITE = "website";
    private static final String MONTHS_TO_RENEWAL = "monthsToRenewal";
    private static final String DURATION_TIME_UNIT = "durationTimeUnit";
    private static final String DURATION = "duration";
    private static final String USER_ID = "userId";
    private static final String NAME = "name";
    private static final String LAST_NAMES = "lastNames";
    private static final String SOURCE_TYPE = "sourceType";
    private static final String MEMBERSHIP_TYPE = "membershipType";
    private static final String SUBSCRIPTION_PRICE = "subscriptionPrice";
    private static final String RENEWAL_PRICE = "renewalPrice";
    private static final String RENEWAL_DURATION = "renewalDuration";
    private static final String MEMBER_ACCOUNT_ID = "memberAccountId";
    private static final String EXPIRATION_DATE = "expirationDate";
    private static final String CURRENCY_CODE = "currencyCode";
    private static final String RECURRING_ID = "recurringId";
    private static final String CHANNEL = "channel";
    private static final Map<String, StatusAction> STATUS_ACTIONS = ImmutableMap.of(
            "PHONE", StatusAction.PB_PHONE_CREATION,
            "EMAIL", StatusAction.PB_EMAIL_CREATION,
            "ANDROID", StatusAction.PB_ANDROID_CREATION,
            "IOS", StatusAction.PB_IOS_CREATION,
            "LANDING_PAGE", StatusAction.PB_LANDING_PAGE);
    private static final String SHOPPING_BASKET_PRODUCT = "shoppingBasketProduct";

    private MembershipCreationRequestMapper() {
    }

    public static MembershipCreation getMembershipCreationFromRequestFields(Map<String, String> requestFields) {
        Optional<Long> userId = getOptionalValue(requestFields.get(USER_ID), Long::valueOf);
        Optional<Long> memberAccountId = getOptionalValue(requestFields.get(MEMBER_ACCOUNT_ID), Long::valueOf);
        Optional<Integer> monthsDuration = getOptionalValue(requestFields.get(MONTHS_TO_RENEWAL), Integer::valueOf);
        Optional<TimeUnit> durationTimeUnit = getOptionalValue(requestFields.get(DURATION_TIME_UNIT), TimeUnit::valueOf);
        Optional<Integer> duration = getOptionalValue(requestFields.get(DURATION), Integer::valueOf);
        Optional<LocalDateTime> expirationDate = getOptionalValue(requestFields.get(EXPIRATION_DATE), LocalDateTime::parse);
        Optional<BigDecimal> subscriptionPrice = getOptionalValue(requestFields.get(SUBSCRIPTION_PRICE), BigDecimal::new);
        Optional<BigDecimal> renewalPrice = getOptionalValue(requestFields.get(RENEWAL_PRICE), BigDecimal::new);
        Optional<Integer> renewalDuration = getOptionalValue(requestFields.get(RENEWAL_DURATION), Integer::new);
        Optional<StatusAction> statusAction = getOptionalValue(requestFields.get(CHANNEL), STATUS_ACTIONS::get);
        Optional<Boolean> shoppingBasketProduct = getOptionalValue(requestFields.get(SHOPPING_BASKET_PRODUCT), Boolean::valueOf);

        UserCreation userCreation = null;
        if (!existingUserParametersPassed(requestFields)) {
            userCreation = UserCreationRequestMapper.getMandatoryUserCreation(requestFields);
        }
        MembershipCreationBuilder builder = new MembershipCreationBuilder()
                .withWebsite(requestFields.get(WEBSITE))
                .withMonthsDuration(String.valueOf(monthsDuration.orElse(null)))
                .withDurationTimeUnit(durationTimeUnit.orElse(null))
                .withDuration(duration.orElse(null))
                .withSourceType(SourceType.valueOf(requestFields.get(SOURCE_TYPE)))
                .withMembershipType(MembershipType.valueOf(requestFields.get(MEMBERSHIP_TYPE)))
                .withAutoRenewal(getAutoRenewal(requestFields))
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                        .userId(userId.orElse(null))
                        .name(requestFields.get(NAME))
                        .lastNames(requestFields.get(LAST_NAMES))
                )
                .withSubscriptionPrice(subscriptionPrice.orElse(null))
                .withRenewalPrice(renewalPrice.orElse(null))
                .withRenewalDuration(renewalDuration.orElse(null))
                .withMemberAccountId(memberAccountId.orElse(null))
                .withExpirationDate(expirationDate.orElse(null))
                .withCurrencyCode(requestFields.get(CURRENCY_CODE))
                .withRecurringId(requestFields.get(RECURRING_ID))
                .withMemberStatus(getMemberStatus(requestFields))
                .withStatusAction(statusAction.orElse(null))
                .withUserCreation(userCreation)
                .withShoppingBasketProduct(shoppingBasketProduct.orElse(false));
        return builder.build();
    }

    private static MembershipRenewal getAutoRenewal(Map<String, String> requestFields) {
        return MembershipType.valueOf(requestFields.get(MEMBERSHIP_TYPE)).equals(BASIC_FREE) ? MembershipRenewal.DISABLED : MembershipRenewal.ENABLED;
    }

    private static boolean existingUserParametersPassed(Map<String, String> requestFields) {
        return isNotBlank(requestFields.get(USER_ID)) || isNotBlank(requestFields.get(MEMBER_ACCOUNT_ID));
    }

    private static <T> Optional<T> getOptionalValue(String value, Function<String, T> func) {
        return nonNull(value) ? Optional.of(func.apply(value)) : Optional.empty();
    }

    private static MemberStatus getMemberStatus(Map<String, String> requestFields) {
        if (BASIC_FREE.name().equalsIgnoreCase(requestFields.get(MEMBERSHIP_TYPE))) {
            return MemberStatus.ACTIVATED;
        } else {
            return nonNull(requestFields.get(MEMBER_ACCOUNT_ID)) ? MemberStatus.PENDING_TO_COLLECT : MemberStatus.PENDING_TO_ACTIVATE;
        }
    }
}
