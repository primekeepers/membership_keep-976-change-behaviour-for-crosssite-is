package com.odigeo.membership.member.memberapi.mapper;

import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.request.membership.ApplyItineraryDiscountRequest;
import com.odigeo.membership.response.membership.NotApplicationReason;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.assertNotNull;

public class MembershipMapperTest extends MapperTest<MembershipMapper> {

    @Override
    protected MembershipMapper getMapper() {
        return new MembershipMapperImpl();
    }

    @Test
    public void testToApplyDiscountParameters() {
        test(ApplyItineraryDiscountRequest.class, getMapper()::toApplyDiscountParameters);
    }

    @Test
    public void testToApplyItineraryDiscountResponse() {
        test(ApplyDiscountResult.class, getMapper()::toApplyItineraryDiscountResponse);
    }

    @Test(dataProvider = "notApplicationReason")
    public void testToNotApplicationReason(com.odigeo.membership.parameters.NotApplicationReason notApplicationReason) {
        NotApplicationReason response = getMapper().toNotApplicationReason(notApplicationReason);
        assertNotNull(response);
    }

    @DataProvider
    public static Object[][] notApplicationReason() {
        com.odigeo.membership.parameters.NotApplicationReason nullValue = com.odigeo.membership.parameters.NotApplicationReason.UNKNOWN;
        return Arrays.stream(com.odigeo.membership.parameters.NotApplicationReason.values()).filter(value -> !nullValue.equals(value)).map(value -> new Object[]{value}).toArray(Object[][]::new);
    }

}