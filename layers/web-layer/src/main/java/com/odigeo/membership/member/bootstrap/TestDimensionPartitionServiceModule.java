package com.odigeo.membership.member.bootstrap;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;
import com.odigeo.visitengineapi.v1.multitest.TestDimensionPartitionService;

public class TestDimensionPartitionServiceModule extends AbstractRestUtilsModule {

    public TestDimensionPartitionServiceModule(ServiceNotificator... serviceNotificators) {
        super(TestDimensionPartitionService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpDefault(TestDimensionPartitionService.class);
    }
}
