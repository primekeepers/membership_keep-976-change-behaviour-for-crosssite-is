package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.io.CharSource;
import com.google.inject.AbstractModule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.fail;

public class LoggingInterceptorTest {

    @Mock
    private LoggingInterceptor loggingInterceptorMock;

    @Mock
    private ContainerRequestContext requestContext;

    @Mock
    private InputStream inputStreamMock;

    private LoggingInterceptor loggingInterceptor;

    @BeforeMethod
    public void setUp() throws Exception {
        openMocks(this);
        this.loggingInterceptorMock = mock(LoggingInterceptor.class);
        ConfigurationEngine.init(new AbstractModule() {
            protected void configure() {
                bind(LoggingInterceptor.class).toInstance(loggingInterceptorMock);
                bind(ContainerRequestContext.class).toInstance(requestContext);
                bind(InputStream.class).toProvider(() -> inputStreamMock);
            }
        });
    }

    @Test
    public void testFilter() throws IOException {
        UriInfo uriInfo = Mockito.mock(UriInfo.class);
        when(requestContext.getUriInfo()).thenReturn(uriInfo);
        when(uriInfo.getPath()).thenReturn("http://localhost:8080/test");
        when(uriInfo.getQueryParameters()).thenReturn(new MultivaluedHashMap<>());
        when(requestContext.getEntityStream()).thenReturn(CharSource.wrap("/example/test/123?bye=ciao")
                .asByteSource(StandardCharsets.UTF_8).openStream());
        new LoggingInterceptor().filter(requestContext);
        verify(requestContext).setEntityStream(any());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testWriteThrowsNpeIfInterceptorDidNotInitProperly() throws Exception {
        final WriterInterceptorContext messageBodyWriterContext = mock(WriterInterceptorContext.class);
        doCallRealMethod().when(loggingInterceptorMock).aroundWriteTo(messageBodyWriterContext);
        this.loggingInterceptorMock.aroundWriteTo(messageBodyWriterContext);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testPreProcessThrowsNpeIfInvalidInputStream() throws IOException {
        new LoggingInterceptor().filter(requestContext);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testWriteThrowsNpeIfInvalidInputStream() {
        try {
            WriterInterceptorContext messageBodyWriterContextMock = mock(WriterInterceptorContext.class);
            loggingInterceptor = new LoggingInterceptor();
            loggingInterceptor.aroundWriteTo(messageBodyWriterContextMock);
        } catch (final IOException e) {
            fail();
        }
    }
}
