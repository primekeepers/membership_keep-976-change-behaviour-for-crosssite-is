package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.configuration.JeeResourceLocator;
import com.odigeo.membership.member.creation.MembershipCreationService;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.member.update.MemberAccountUserIdUpdater;
import com.odigeo.membership.member.update.MembershipTransferMarketingHandler;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.product.MembershipSubscriptionConfiguration;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.util.CrmCipher;
import com.odigeo.visitengineapi.v1.client.multitest.TestTokenMatcherFactory;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Objects;
import java.util.Optional;

public abstract class AbstractServiceBean {

    @Resource(mappedName = JeeResourceLocator.DEFAULT_DATASOURCE_NAME)
    protected DataSource dataSource;

    private MembershipRecurringStore membershipRecurringStore;
    private MemberManager memberManager;
    private MembershipStore membershipStore;
    private MemberStatusActionStore memberStatusActionStore;
    private MemberAccountStore memberAccountStore;
    private MembershipMessageSendingManager membershipMessageSendingManager;
    private MembershipNoSQLManager membershipNoSQLManager;
    private UserApiManager userApiManager;
    private SearchService searchService;
    private TestTokenMatcherFactory testTokenMatcherFactory;
    private BookingTrackingService bookingTrackingService;
    private UserService userService;
    private MembershipCreationService membershipCreationService;
    private MemberAccountUserIdUpdater memberAccountUserIdUpdater;
    private MembershipFeesService membershipFeesService;
    private MembershipTransferMarketingHandler membershipTransferMarketingHandler;
    private MemberAccountService memberAccountService;
    private CrmCipher crmCipher;
    private MembershipSubscriptionConfiguration membershipSubscriptionConfiguration;

    protected MemberManager getMemberManager() {
        return Optional.ofNullable(memberManager).orElseGet(() -> {
            memberManager = ConfigurationEngine.getInstance(MemberManager.class);
            return memberManager;
        });
    }

    protected MembershipRecurringStore getMembershipRecurringStore() {
        return Optional.ofNullable(membershipRecurringStore).orElseGet(() -> {
            membershipRecurringStore = ConfigurationEngine.getInstance(MembershipRecurringStore.class);
            return membershipRecurringStore;
        });
    }

    protected MembershipStore getMembershipStore() {
        return Optional.ofNullable(membershipStore).orElseGet(() -> {
            membershipStore = ConfigurationEngine.getInstance(MembershipStore.class);
            return membershipStore;
        });
    }

    protected MemberStatusActionStore getMemberStatusActionStore() {
        return Optional.ofNullable(memberStatusActionStore).orElseGet(() -> {
            memberStatusActionStore = ConfigurationEngine.getInstance(MemberStatusActionStore.class);
            return memberStatusActionStore;
        });
    }

    protected MemberAccountStore getMemberAccountStore() {
        return Optional.ofNullable(memberAccountStore).orElseGet(() -> {
            memberAccountStore = ConfigurationEngine.getInstance(MemberAccountStore.class);
            return memberAccountStore;
        });
    }

    protected MembershipMessageSendingManager getMembershipMessageSendingManager() {
        return Optional.ofNullable(membershipMessageSendingManager).orElseGet(() -> {
            membershipMessageSendingManager = ConfigurationEngine.getInstance(MembershipMessageSendingManager.class);
            return membershipMessageSendingManager;
        });
    }

    protected MembershipNoSQLManager getMembershipNoSQLManager() {
        return Optional.ofNullable(membershipNoSQLManager).orElseGet(() -> {
            membershipNoSQLManager = ConfigurationEngine.getInstance(MembershipNoSQLManager.class);
            return membershipNoSQLManager;
        });
    }

    protected UserApiManager getUserApiManager() {
        return Optional.ofNullable(userApiManager).orElseGet(() -> {
            userApiManager = ConfigurationEngine.getInstance(UserApiManager.class);
            return userApiManager;
        });
    }

    protected SearchService getSearchService() {
        return Optional.ofNullable(searchService).orElseGet(() -> {
            searchService = ConfigurationEngine.getInstance(SearchService.class);
            return searchService;
        });
    }

    protected TestTokenMatcherFactory getTestTokenMatcherFactory() {
        return Optional.ofNullable(testTokenMatcherFactory).orElseGet(() -> {
            testTokenMatcherFactory = ConfigurationEngine.getInstance(TestTokenMatcherFactory.class);
            return testTokenMatcherFactory;
        });
    }

    protected BookingTrackingService getBookingTrackingService() {
        return Optional.ofNullable(bookingTrackingService).orElseGet(() -> {
            bookingTrackingService = ConfigurationEngine.getInstance(BookingTrackingService.class);
            return bookingTrackingService;
        });
    }

    protected UserService getUserService() {
        return Optional.ofNullable(userService).orElseGet(() -> {
            userService = ConfigurationEngine.getInstance(UserService.class);
            return userService;
        });
    }

    protected MembershipCreationService getMembershipCreationService() {
        return Optional.ofNullable(membershipCreationService).orElseGet(() -> {
            membershipCreationService = ConfigurationEngine.getInstance(MembershipCreationService.class);
            return membershipCreationService;
        });
    }

    protected MemberAccountUserIdUpdater getMemberAccountUserIdUpdater() {
        return Optional.ofNullable(memberAccountUserIdUpdater).orElseGet(() -> {
            memberAccountUserIdUpdater = ConfigurationEngine.getInstance(MemberAccountUserIdUpdater.class);
            return memberAccountUserIdUpdater;
        });
    }

    protected MembershipFeesService getMembershipFeesService() {
        return Optional.ofNullable(membershipFeesService).orElseGet(() -> {
            membershipFeesService = ConfigurationEngine.getInstance(MembershipFeesService.class);
            return membershipFeesService;
        });
    }

    protected MembershipTransferMarketingHandler getMembershipTransferMarketingHandler() {
        return Optional.ofNullable(membershipTransferMarketingHandler).orElseGet(() -> {
            membershipTransferMarketingHandler = ConfigurationEngine.getInstance(MembershipTransferMarketingHandler.class);
            return membershipTransferMarketingHandler;
        });
    }

    protected MemberAccountService getMemberAccountService() {
        return Optional.ofNullable(memberAccountService).orElseGet(() -> {
            memberAccountService = ConfigurationEngine.getInstance(MemberAccountService.class);
            return memberAccountService;
        });
    }

    protected CrmCipher getCrmCipher() {
        return Optional.ofNullable(crmCipher).orElseGet(() -> {
            crmCipher = ConfigurationEngine.getInstance(CrmCipher.class);
            return crmCipher;
        });
    }

    protected MembershipSubscriptionConfiguration getMembershipSubscriptionConfiguration() {
        if (Objects.isNull(membershipSubscriptionConfiguration)) {
            membershipSubscriptionConfiguration = ConfigurationEngine.getInstance(MembershipSubscriptionConfiguration.class);
        }
        return membershipSubscriptionConfiguration;
    }


}
