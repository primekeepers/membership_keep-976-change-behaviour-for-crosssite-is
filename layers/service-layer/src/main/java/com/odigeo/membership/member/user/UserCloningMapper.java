package com.odigeo.membership.member.user;

import com.odigeo.userapi.BrandMapper;
import com.odigeo.userapi.UserDTO;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;

public final class UserCloningMapper {

    private UserCloningMapper() {
    }

    public static User mapToUser(UserDTO userDTO) {

        User newUser = new User();
        newUser.setId(userDTO.getId());
        newUser.setEmail(userDTO.getEmail());
        newUser.setLocale(userDTO.getLocale());
        newUser.setWebsite(userDTO.getWebsite());
        newUser.setBrand(BrandMapper.map(userDTO.getWebsite()).orElse(null));
        newUser.setTrafficInterface(userDTO.getTrafficInterface());
        newUser.setSource(userDTO.getSource());
        newUser.setMarketingPortal(userDTO.getMarketingPortal());

        newUser.setFlightStatusId(userDTO.getFlightStatusId());
        newUser.setStatus(userDTO.getStatus());
        newUser.setTrafficInterface(userDTO.getTrafficInterface());

        return newUser;
    }

    public static UserDTO mapToUserApiUser(UserBasicInfo userBasicInfo) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userBasicInfo.getId());
        userDTO.setBrand(userBasicInfo.getBrand());
        userDTO.setEmail(userBasicInfo.getEmail());
        userDTO.setSource(userBasicInfo.getSource());
        userDTO.setWebsite(userBasicInfo.getWebsite());
        userDTO.setLocale(userBasicInfo.getLocale());
        userDTO.setMarketingPortal(userBasicInfo.getMarketingPortal());
        userDTO.setFlightStatusId(userBasicInfo.getFlightStatusId());
        userDTO.setStatus(userBasicInfo.getStatus());
        userDTO.setTrafficInterface(userBasicInfo.getTrafficInterface());
        return userDTO;
    }
}
