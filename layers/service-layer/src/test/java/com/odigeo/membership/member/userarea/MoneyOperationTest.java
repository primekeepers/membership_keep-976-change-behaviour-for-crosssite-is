package com.odigeo.membership.member.userarea;

import com.odigeo.bookingapi.v14.responses.Money;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;

public class MoneyOperationTest {

    @Test
    public void testMoneySumWithFirstNullAmountReturnsSecond() {
        //Given
        final Money firstAmount = moneyWithAmount(null);
        final Money secondAmount = moneyWithAmount(TEN);
        //When
        Money moneySum = MoneyOperation.moneySum(firstAmount, secondAmount);
        //Then
        assertEquals(moneySum.getAmount(), secondAmount.getAmount());
    }

    @Test
    public void testMoneySumWithSecondNullReturnsFirst() {
        //Given
        final Money firstAmount = moneyWithAmount(ONE);
        final Money secondAmount = moneyWithAmount(null);
        //When
        Money moneySum = MoneyOperation.moneySum(firstAmount, secondAmount);
        //Then
        assertEquals(moneySum.getAmount(), firstAmount.getAmount());
    }

    @Test
    public void testMoneySumReturnsAddedNumbers() {
        //Given
        final Money firstAmount = moneyWithAmount(BigDecimal.valueOf(11.4));
        final Money secondAmount = moneyWithAmount(TEN);
        //When
        Money moneySum = MoneyOperation.moneySum(firstAmount, secondAmount);
        //Then
        assertEquals(moneySum.getAmount(), BigDecimal.valueOf(21.4));
    }

    @Test
    public void testMoneyIsBuiltWithAmountAndCurrency() {
        //Given
        final BigDecimal amount = BigDecimal.valueOf(12.3);
        final String currency = "GBP";
        //When
        Money builtMoney = MoneyOperation.buildMoney(amount, currency);
        //Then
        assertEquals(builtMoney.getAmount(), amount);
        assertEquals(builtMoney.getCurrency(), currency);
    }

    @Test
    public void testZeroMoneyHasDefaultAmountAndCurrencyValues() {
        //Given
        Money zeroMoney = MoneyOperation.zero();
        //Then
        assertEquals(zeroMoney.getAmount(), ZERO);
        assertEquals(zeroMoney.getCurrency(), EMPTY);
    }

    private Money moneyWithAmount(final BigDecimal amount) {
        final Money money = new Money();
        money.setAmount(amount);
        return money;
    }
}
