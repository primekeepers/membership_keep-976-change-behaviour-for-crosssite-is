package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.memberapi.v1.util.OnboardingMapper;
import com.odigeo.membership.onboarding.Onboarding;
import com.odigeo.membership.onboarding.OnboardingEventService;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class OnboardingControllerTest {

    @Mock
    private OnboardingEventService onboardingEventService;
    @Mock
    private OnboardingMapper mapper;

    @Mock
    private OnboardingEventRequest onboardingEventRequest;

    private OnboardingController controller;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        configureModules();
        controller = new OnboardingController();
    }

    @Test
    public void testOnboardingEventCorrectlySaved() throws DataAccessException {
        //Given
        Onboarding onboardingToSave = mock(Onboarding.class);
        Long expectedSavedOnboardingId = 8348L;

        given(mapper.map(onboardingEventRequest)).willReturn(onboardingToSave);
        given(onboardingEventService.createOnboarding(onboardingToSave)).willReturn(expectedSavedOnboardingId);

        //When
        Long savedOnboardingId = controller.saveOnboardingEvent(onboardingEventRequest);

        //Then
        assertEquals(savedOnboardingId, expectedSavedOnboardingId);
    }

    @Test(expectedExceptions = MembershipServiceException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testExceptionWhenSavingOnboardingEvent() throws DataAccessException {
        //Given
        Onboarding onboardingToSave = mock(Onboarding.class);
        given(mapper.map(onboardingEventRequest)).willReturn(onboardingToSave);
        given(onboardingEventService.createOnboarding(onboardingToSave)).willThrow(new DataAccessException("Expected exception"));
        //When
        controller.saveOnboardingEvent(onboardingEventRequest);
    }

    private void configureModules() {
        ConfigurationEngine.init(binder -> {
            binder.bind(OnboardingEventService.class).toInstance(onboardingEventService);
            binder.bind(OnboardingMapper.class).toInstance(mapper);
        });
    }
}