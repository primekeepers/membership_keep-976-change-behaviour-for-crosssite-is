package com.odigeo.membership.mocks.kafka.consumers;

import com.odigeo.commons.messaging.Consumer;
import com.odigeo.commons.messaging.Jackson2JsonDeserializer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.commons.messaging.Message;
import com.odigeo.membership.mocks.kafka.processors.AbstractKafkaMessageProcessor;

public abstract class AbstractKafkaMessageConsumer<T extends Message> {
    private static final int NUMBER_OF_THREADS = 1;

    AbstractKafkaMessageConsumer(AbstractKafkaMessageProcessor<T> messageProcessor, Class<T> messageClass, String topic, String groupId) {
        Consumer<T> consumer = new KafkaConsumer<>(new Jackson2JsonDeserializer<>(messageClass), topic, groupId);
        consumer.start(messageProcessor, NUMBER_OF_THREADS);
    }

    public abstract AbstractKafkaMessageProcessor<T> getMessageProcessor();
}
