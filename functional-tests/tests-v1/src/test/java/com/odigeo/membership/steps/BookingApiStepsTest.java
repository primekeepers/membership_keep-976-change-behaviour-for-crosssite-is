package com.odigeo.membership.steps;

import com.odigeo.bookingapi.mock.v14.response.BuilderException;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.FeeContainer;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceHttpServer;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceMock;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.mocks.bookingsearchapiservice.BookingSearchApiServiceHttpServer;
import com.odigeo.membership.mocks.bookingsearchapiservice.BookingSearchApiServiceMock;
import cucumber.api.DataTable;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BookingApiStepsTest {

    private static final String BOOKING_ID = "5678";
    private static final String CONTAINER_ID = "111";
    private static final String FEE_AMOUNT = "100";
    private static final String CURRENCY = "EUR";
    private static final String CC_NUMBER = "1234123412346969";
    private static final String CC_EXPIRATION_YEAR = "21";
    private static final String CC_EXPIRATION_MONTH = "10";
    private static final String SUBSCRIPTION_CATEGORY = "subscription";
    private static final String MEMBERSHIP_ID = "1234";
    private static final String ITINERARY_PRODUCT = "ITINERARY";
    private static final String EMAIL = "qa@qa.com";
    private static final String WEBSITE = "ED_ES";
    private static final String MEMBERSHIP_PRODUCT = "MEMBERSHIP_SUBSCRIPTION";

    @Mock
    private DataTable dataTable;
    @Mock
    private BookingApiServiceMock bookingApiServiceMock;
    @Mock
    private BookingSearchApiServiceMock bookingSearchApiServiceMock;
    @Captor
    private ArgumentCaptor<BookingSummary> captorBookingSummary;
    @Captor
    private ArgumentCaptor<BookingDetail> captorBookingDetail;
    private BookingApiServiceWorld bookingApiServiceWorld;
    private BookingApiSteps instance;
    private List<Map<String, String>> rawData;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        initializeDependencies();
        when(dataTable.asMaps(String.class, String.class)).thenReturn(rawData);
        instance = new BookingApiSteps(bookingApiServiceWorld);
    }

    @Test
    public void testRetrieveWithMembershipID() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        addMembershipBookingParameters();
        instance.retrieveBookingsByMembershipId(dataTable);
        verify(bookingSearchApiServiceMock).addBookingSummary(captorBookingSummary.capture());
        BookingSummary bookingSummary = captorBookingSummary.getValue();
        checkIIsMembershipSubscriptionParameters(bookingSummary);
        checkCorrectBookingId(bookingSummary.getBookingBasicInfo().getId());
    }

    @Test
    public void testSetContainerFee() throws BuilderException {
        addCreditCardParameters();
        addFeesContainerParameters();
        instance.setContainerFeeForBookings(dataTable);
        verify(bookingApiServiceMock).addBookingDetail(captorBookingDetail.capture());
        BookingDetail bookingDetail = captorBookingDetail.getValue();
        FeeContainer feeContainer = bookingDetail.getAllFeeContainers().get(0);
        checkCorrectBookingId(bookingDetail.getBookingBasicInfo().getId());
        checkFeeContainerParameters(bookingDetail, feeContainer);
        checkCreditCardParameters(bookingDetail);
    }

    @Test
    public void testBookingDetailsWithMembershipProduct() throws BuilderException {
        BookingDetail bookingDetail = prepareAndTestProduct(SUBSCRIPTION_CATEGORY);
        assertEquals(bookingDetail.getBookingProducts().get(0).getProductType(), MEMBERSHIP_PRODUCT);
    }

    @Test
    public void testBookingDetailsWithItineraryProduct() throws BuilderException {
        BookingDetail bookingDetail = prepareAndTestProduct(ITINERARY_PRODUCT);
        assertEquals(bookingDetail.getBookingProducts().get(0).getProductType(), ITINERARY_PRODUCT);
    }

    @Test
    public void testBookingDetailsForKafka() {
        addKafkaParameters();
        verify(bookingApiServiceMock).addBookingDetail(captorBookingDetail.capture());
        BookingDetail bookingDetail = captorBookingDetail.getValue();
        checkKafkaInformationParameters(bookingDetail);
    }

    private void addKafkaParameters() {
        rawData.get(0).put("email", EMAIL);
        rawData.get(0).put("website", WEBSITE);
    }

    private void addProductParameters(String productType) {
        rawData.get(0).put("productCategoryBooking", productType);
    }

    private void addCreditCardParameters() {
        rawData.get(0).put("currencyCode", CURRENCY);
        rawData.get(0).put("ccNumber", CC_NUMBER);
        rawData.get(0).put("ccExpirationDate", CC_EXPIRATION_MONTH + "/" + CC_EXPIRATION_YEAR);
    }

    private void addFeesContainerParameters() {
        rawData.get(0).put("containerId", CONTAINER_ID);
        rawData.get(0).put("feeTotalAmount", FEE_AMOUNT);
    }

    private void addMembershipBookingParameters() {
        rawData.get(0).put("membershipId", MEMBERSHIP_ID);
        rawData.get(0).put("isSubscriptionBooking", Boolean.TRUE.toString());
    }

    private void initializeDependencies() {
        this.bookingApiServiceWorld = new BookingApiServiceWorld(bookingApiServiceMock,
                new BookingApiServiceHttpServer(), bookingSearchApiServiceMock, new BookingSearchApiServiceHttpServer());
        this.rawData = initializeBasicRawData();
    }

    private List<Map<String, String>> initializeBasicRawData() {
        List<Map<String, String>> rawData = new ArrayList<>();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("bookingId", BOOKING_ID);
        rawData.add(parameters);
        return rawData;
    }

    private void checkCorrectBookingId(long bookingId) {
        assertEquals(bookingId, Long.parseLong(BOOKING_ID));
    }

    private void checkIIsMembershipSubscriptionParameters(BookingSummary bookingSummary) {
        assertEquals(bookingSummary.getMembershipId(), new Long(Long.parseLong(MEMBERSHIP_ID)));
        assertTrue(Boolean.parseBoolean(bookingSummary.getIsBookingSubscriptionPrime()));
    }

    private void checkFeeContainerParameters(BookingDetail bookingDetail, FeeContainer feeContainer) {
        assertEquals(bookingDetail.getMembershipPerks().getFeeContainerId(), new Long(Long.parseLong(CONTAINER_ID)));
        assertEquals(feeContainer.getId(), new Long(Long.parseLong(CONTAINER_ID)));
        assertEquals(feeContainer.getTotalAmount(), new BigDecimal(FEE_AMOUNT));
        assertEquals(feeContainer.getFees().get(0).getCurrency(), CURRENCY);
    }

    private void checkCreditCardParameters(BookingDetail bookingDetail) {
        assertEquals(bookingDetail.getCollectionSummary().getLastCreditCard().getCreditCardNumber(), CC_NUMBER);
        assertEquals(bookingDetail.getCollectionSummary().getLastCreditCard().getExpirationYear(), CC_EXPIRATION_YEAR);
        assertEquals(bookingDetail.getCollectionSummary().getLastCreditCard().getExpirationMonth(), CC_EXPIRATION_MONTH);
    }

    private BookingDetail prepareAndTestProduct(String product) throws BuilderException {
        addProductParameters(product);
        ArgumentCaptor<BookingDetail> argument = ArgumentCaptor.forClass(BookingDetail.class);
        instance.setBookingDetailsWithProducts(dataTable);
        verify(bookingApiServiceMock).addBookingDetail(argument.capture());
        return argument.getValue();
    }

    private void checkKafkaInformationParameters(BookingDetail bookingDetail) {
        assertEquals(bookingDetail.getBuyer().getMail(), EMAIL);
        assertEquals(bookingDetail.getBookingBasicInfo().getWebsite().getCode(), WEBSITE);
    }

}
