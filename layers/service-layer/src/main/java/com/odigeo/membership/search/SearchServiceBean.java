package com.odigeo.membership.search;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.member.AbstractServiceBean;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.parameters.search.MembershipSearchBuilder;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Stateless
@Local(SearchService.class)
public class SearchServiceBean extends AbstractServiceBean implements SearchService {

    @Override
    public List<MemberAccount> searchMemberAccounts(MemberAccountSearch memberAccountSearch) throws DataAccessException {
        try {
            return getMemberAccountStore().searchMemberAccounts(dataSource, memberAccountSearch);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error searching accounts: ", e);
        }
    }

    @Override
    public List<Membership> searchMemberships(MembershipSearch membershipSearch) throws DataAccessException {
        try {
            return getMembershipStore().searchMemberships(dataSource, convertToUserIdSearchByEmailAndWebsite(membershipSearch));
        } catch (SQLException e) {
            throw new DataAccessException("There was an error searching memberships: ", e);
        } catch (UserAccountNotFoundException e) {
            return Collections.emptyList();
        }
    }

    private MembershipSearch convertToUserIdSearchByEmailAndWebsite(MembershipSearch membershipSearch) throws UserAccountNotFoundException, DataAccessException {
        if (StringUtils.isNoneBlank(membershipSearch.getEmail(), membershipSearch.getWebsite())) {
            Long userId = getUserService().getUserIdBy(membershipSearch.getEmail(), membershipSearch.getWebsite());
            return MembershipSearch.builder(membershipSearch)
                    .withMemberAccount(true)
                    .memberAccountSearch(MemberAccountSearch.builder(membershipSearch.getMemberAccountSearch())
                            .userId(userId)
                            .build())
                    .build();
        }
        return membershipSearch;
    }

    @Override
    public Optional<Membership> getCurrentMembership(Long userId, String website) throws DataAccessException {
        Optional<Membership> currentMembership = getMembership(createCurrentMembershipSearch(userId, website, MemberStatus.ACTIVATED));
        if (currentMembership.isEmpty()) {
            currentMembership = getMembership(createCurrentMembershipSearch(userId, website, MemberStatus.PENDING_TO_COLLECT));
        }
        return currentMembership;
    }


    private Optional<Membership> getMembership(MembershipSearch membershipSearch) throws DataAccessException {
        return searchMemberships(membershipSearch).stream().findFirst();
    }

    private MembershipSearch createCurrentMembershipSearch(Long userId, String website, MemberStatus memberStatus) {
        return new MembershipSearchBuilder()
                .website(website)
                .memberAccountSearch(new MemberAccountSearch.Builder()
                        .userId(userId)
                        .build())
                .withMemberAccount(true)
                .status(memberStatus.name())
                .build();
    }

}
