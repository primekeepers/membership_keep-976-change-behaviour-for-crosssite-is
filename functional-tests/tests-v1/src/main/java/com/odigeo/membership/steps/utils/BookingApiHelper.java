package com.odigeo.membership.steps.utils;

import com.odigeo.bookingapi.mock.v14.response.AdditionalProductBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v14.response.BookingItineraryBuilder;
import com.odigeo.bookingapi.mock.v14.response.BuilderException;
import com.odigeo.bookingapi.mock.v14.response.InsuranceBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.LocationBuilder;
import com.odigeo.bookingapi.mock.v14.response.MembershipSubscriptionBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.MonitoringAlertBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.PostsellServiceOptionBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.ProductCategoryBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.SmsNotificationBookingBuilder;
import com.odigeo.membership.functionals.membership.booking.BookingProductsBuilder;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceMock;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;

public final class BookingApiHelper {

    private static final Logger LOGGER = Logger.getLogger(BookingApiHelper.class);

    private BookingApiHelper() {
    }

    public static BookingDetailBuilder getBookingDetail(com.odigeo.membership.functionals.membership.booking.BookingDetailBuilder dataTableElement) {
        BookingDetailBuilder builder = new BookingDetailBuilder();
        return builder.bookingBasicInfoBuilder(getBookingBasicInfo(dataTableElement));
    }

    private static BookingBasicInfoBuilder getBookingBasicInfo(com.odigeo.membership.functionals.membership.booking.BookingDetailBuilder dataTableElement) {
        BookingBasicInfoBuilder builder = new BookingBasicInfoBuilder();
        return builder.id(dataTableElement.getBookingId());
    }

    public static Predicate<BookingDetailBuilder> hasSameBookingId(Long bookingId) {
        return b -> Objects.equals(b.getBookingBasicInfoBuilder().getId(), bookingId);
    }

    public static Consumer<BookingDetailBuilder> add(BookingProductsBuilder bookingProductsBuilder) {
        return builder -> {

            if (builder.getBookingProducts() == null) {
                builder.bookingProducts(new ArrayList<>());
            }

            builder.getBookingProducts().add(getBookingProducts(bookingProductsBuilder));

        };
    }

    public static ProductCategoryBookingBuilder<?> getBookingProducts(BookingProductsBuilder bookingProductsBuilder) {
        ProductCategoryBookingBuilder<?> productCategoryBookingBuilder;
        switch (bookingProductsBuilder.getProductCategoryBooking()) {
        case ITINERARY_BOOKING:
            ItineraryBookingBuilder itineraryBookingBuilder = new ItineraryBookingBuilder();
            itineraryBookingBuilder.bookingItinerary(getBookingItinerary(bookingProductsBuilder));
            productCategoryBookingBuilder = itineraryBookingBuilder;
            break;
        case INSURANCE_BOOKING:
            productCategoryBookingBuilder = new InsuranceBookingBuilder();
            break;
        case ADDITIONAL_PRODUCT_BOOKING:
            productCategoryBookingBuilder = new AdditionalProductBookingBuilder();
            break;
        case MONITORING_ALERT_BOOKING:
            productCategoryBookingBuilder = new MonitoringAlertBookingBuilder();
            break;
        case POSTSELL_SERVICE_OPTION_BOOKING:
            productCategoryBookingBuilder = new PostsellServiceOptionBookingBuilder();
            break;
        case SMS_NOTIFICATION_BOOKING:
            productCategoryBookingBuilder = new SmsNotificationBookingBuilder();
            break;
        case MEMBERSHIP_SUBSCRIPTION_BOOKING:
            productCategoryBookingBuilder = new MembershipSubscriptionBookingBuilder();
            break;
        default:
            productCategoryBookingBuilder  = new ItineraryBookingBuilder();
            break;
        }

        return productCategoryBookingBuilder;
    }

    public static BookingItineraryBuilder getBookingItinerary(BookingProductsBuilder stringMap) {
        BookingItineraryBuilder bookingItineraryBuilder = new BookingItineraryBuilder();
        return bookingItineraryBuilder
                .arrival(getLocation(stringMap))
                .departureDate(stringMap.getDepartureDate());
    }

    public static LocationBuilder getLocation(BookingProductsBuilder stringMap) {
        LocationBuilder locationBuilder = new LocationBuilder();
        return locationBuilder.name(stringMap.getArrivalName());
    }

    public static Consumer<BookingDetailBuilder> add(BookingApiServiceMock bookingApiServiceMock) {
        return b -> {
            try {
                bookingApiServiceMock.addBookingDetail(b.build(new Random()));
            } catch (BuilderException e) {
                LOGGER.error(e.getLocalizedMessage());
            }
        };
    }
}
