package com.odigeo.membership.member.update;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.member.update.operation.ConsumeMembershipBalanceOperation;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.membership.member.update.operation.RecurringIdOperation;
import com.odigeo.membership.member.update.operation.RemindMeLaterOperation;
import com.odigeo.membership.member.update.operation.UpdateMembershipTypeOperation;
import com.odigeo.membership.member.update.operation.autorenewal.DisableAutoRenewalOperation;
import com.odigeo.membership.member.update.operation.autorenewal.EnableAutoRenewalOperation;
import com.odigeo.membership.member.update.operation.status.DeactivateMembershipOperation;
import com.odigeo.membership.member.update.operation.status.DiscardMembershipOperation;
import com.odigeo.membership.member.update.operation.status.ExpireMembershipOperation;
import com.odigeo.membership.member.update.operation.status.ReactivateMembershipOperation;

import java.util.Map;

public class MembershipUpdaterModule extends AbstractModule {
    private static final  Map<UpdateMembershipAction, Class<? extends MembershipUpdateOperationFactory>> OPERATION_FACTORIES_REGISTRY = Maps.immutableEnumMap(
            ImmutableMap.<UpdateMembershipAction, Class<? extends MembershipUpdateOperationFactory>>builder()
                    .put(UpdateMembershipAction.EXPIRE_MEMBERSHIP, ExpireMembershipOperation.Factory.class)
                    .put(UpdateMembershipAction.REACTIVATE_MEMBERSHIP, ReactivateMembershipOperation.Factory.class)
                    .put(UpdateMembershipAction.SET_REMIND_ME_LATER, RemindMeLaterOperation.Factory.class)
                    .put(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP, DeactivateMembershipOperation.Factory.class)
                    .put(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, EnableAutoRenewalOperation.Factory.class)
                    .put(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, DisableAutoRenewalOperation.Factory.class)
                    .put(UpdateMembershipAction.DISCARD_MEMBERSHIP, DiscardMembershipOperation.Factory.class)
                    .put(UpdateMembershipAction.INSERT_RECURRING_ID, RecurringIdOperation.Factory.class)
                    .put(UpdateMembershipAction.UPDATE_MEMBERSHIP_TYPE, UpdateMembershipTypeOperation.Factory.class)
                    .put(UpdateMembershipAction.CONSUME_MEMBERSHIP_REMNANT_BALANCE, ConsumeMembershipBalanceOperation.Factory.class)
                    .build()
    );

    @Override
    public void configure() {
        MapBinder<UpdateMembershipAction, MembershipUpdateOperationFactory> updaterBinder = MapBinder
                .newMapBinder(binder(), UpdateMembershipAction.class, MembershipUpdateOperationFactory.class);
        OPERATION_FACTORIES_REGISTRY.forEach((action, factory) -> {
            install(new FactoryModuleBuilder().build(factory));
            updaterBinder.addBinding(action).to(factory);
        });
    }
}
