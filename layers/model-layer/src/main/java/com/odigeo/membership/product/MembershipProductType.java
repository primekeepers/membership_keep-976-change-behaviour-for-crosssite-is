package com.odigeo.membership.product;

public enum MembershipProductType {

    MEMBERSHIP("AE11"),
    MEMBERSHIP_RENEWAL("AE10");

    private final String feeSubCode;

    MembershipProductType(String feeSubCode) {
        this.feeSubCode = feeSubCode;
    }

    public String getFeeSubCode() {
        return feeSubCode;
    }
}
