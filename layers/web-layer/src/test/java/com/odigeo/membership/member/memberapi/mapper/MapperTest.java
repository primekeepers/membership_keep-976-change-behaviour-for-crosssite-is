package com.odigeo.membership.member.memberapi.mapper;

import org.apache.commons.beanutils.PropertyUtils;
import org.jeasy.random.EasyRandom;
import org.testng.Assert;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.function.Function;

public abstract class MapperTest<T> {

    protected static final EasyRandom easyRandom = new EasyRandom();

    protected static <S> void test(Class<S> source, Function<S, ?> function) {
        easyRandom.objects(source, 100).map(function).map(MapperTest::getValues).forEach(Assert::assertNotNull);
    }

    protected abstract T getMapper();

    protected static Collection<?> getValues(Object result) {
        try {
            return PropertyUtils.describe(result).values();
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

}
