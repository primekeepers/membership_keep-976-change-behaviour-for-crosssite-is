package com.odigeo.membership.member.discount.rule;

import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.util.TestTokenUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.List;

import static com.odigeo.membership.parameters.NotApplicationReason.TRAVELLERS_INVALID;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class TravellersRulesTest extends ApplyDiscountRulesTest {

    private static final String USER_NAME = "IMAUserName";
    private static final String USER_LAST_NAMES = "IMAUserLastName";

    private static final TravellerParameter USER = new TravellerParameter(USER_NAME, USER_LAST_NAMES);
    private static final TravellerParameter OTHER = new TravellerParameter("Foo", "Fuu");

    private TravellersRules travellersRules;

    @Mock
    private TestTokenUtils testTokenUtils;
    @Mock
    private Membership membership1;
    @Mock
    private Membership membership2;
    @Mock
    private MemberAccount memberAccount;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        travellersRules = new TravellersRules();

        when(membership1.getMemberAccount()).thenReturn(memberAccount);
        when(membership2.getMemberAccount()).thenReturn(memberAccount);
    }

    @DataProvider
    public static Object[][] travellers() {
        return new Object[][]{
                {List.of(USER), TRUE, null},
                {List.of(OTHER, USER), TRUE, null},
                {List.of(OTHER), FALSE, TRAVELLERS_INVALID}
        };
    }

    @Test(dataProvider = "travellers")
    public void testTest1(Collection<TravellerParameter> travellers, boolean expected, NotApplicationReason reason) {

        //GIVEN
        when(memberAccount.getName()).thenReturn(USER_NAME);
        when(memberAccount.getLastNames()).thenReturn(USER_LAST_NAMES);

        ApplyDiscountParameters parameters = buildParameters(travellers);
        ApplyDiscountContext context = buildContext();

        //WHEN
        boolean result = travellersRules.test(parameters, context);

        //THEN
        assertEquals(result, expected);
        checkContext(context, result, reason);
    }

    private ApplyDiscountParameters buildParameters(Collection<TravellerParameter> travellers) {
        ApplyDiscountParameters parameters = new ApplyDiscountParameters();
        parameters.setTravellers(travellers);
        return parameters;
    }

    private ApplyDiscountContext buildContext() {
        return new ApplyDiscountContext(testTokenUtils, List.of(membership1, membership2));
    }
}