package com.odigeo.userapi;

import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import org.testng.annotations.Test;

import java.util.Optional;

import static com.odigeo.userprofiles.api.v2.model.Status.ACTIVE;
import static com.odigeo.userprofiles.api.v2.model.Status.PENDING_LOGIN;
import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class UserInfoTest {

    private static final Long USER_ID = 8383L;
    private static final String EMAIL = "EMAIL";

    @Test
    public void testGetUserIdNullWhenDefaultObject() {
        assertEquals(UserInfo.defaultUserInfo().getUserId(), Optional.empty());
    }

    @Test
    public void testGetUserIdHasValueWhenBuildingFromUserBasicInfo() {
        UserInfo userInfo = UserInfo.fromUserBasicInfo(sampleUserBasicInfo(ACTIVE));
        assertTrue(userInfo.getUserId().isPresent());
        assertEquals(userInfo.getUserId().get(), USER_ID);
        assertEquals(userInfo.getEmail(), EMAIL);
    }

    @Test
    public void testShouldSetPasswordFalseWhenDefaultObject() {
        assertFalse(UserInfo.defaultUserInfo().shouldSetPassword());
    }

    @Test
    public void testShouldSetPasswordFalseWhenNotPendingLoginUserInfo() {
        UserInfo userInfo = UserInfo.fromUserBasicInfo(sampleUserBasicInfo(ACTIVE));
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testShouldSetPasswordTrueWhenPendingLoginUserInfo() {
        UserInfo userInfo = UserInfo.fromUserBasicInfo(sampleUserBasicInfo(PENDING_LOGIN));
        assertTrue(userInfo.shouldSetPassword());
    }

    private UserBasicInfo sampleUserBasicInfo(Status status) {
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setId(USER_ID);
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setStatus(status);
        return userBasicInfo;
    }
}