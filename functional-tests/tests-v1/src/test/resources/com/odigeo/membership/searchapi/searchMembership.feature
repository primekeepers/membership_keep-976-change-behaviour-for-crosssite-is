Feature: Test search memberships

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 100             | 1000   | JOSE      | GOMEZ     |
      | 101             | 1100   | ANA       | GOMEZ     |
      | 102             | 1200   | JOHN      | WICK      |
      | 103             | 1300   | MICK      | JAGGER    |
      | 104             | 1400   | MICK      | BURTON    |
      | 105             | 257    | MAX       | BIAGGI    |
      | 106             | 257    | MAX       | BIAGGI    |

    And the next membership stored in db:
      | memberId | website | status              | autoRenewal | memberAccountId | activationDate | expirationDate | monthsDuration | balance | productStatus | totalPrice | sourceType     | membershipType |
      | 99000    | ES      | ACTIVATED           | ENABLED     | 100             | 2017-07-06     | 2018-07-06     | 12             | 54.99   | null          | 54.99      | FUNNEL_BOOKING | BASIC          |
      | 99001    | ES      | PENDING_TO_ACTIVATE | ENABLED     | 100             | null           | null           | 1              | 0       | null          | 34.99      | FUNNEL_BOOKING | BASIC          |
      | 99010    | ES      | DEACTIVATED         | ENABLED     | 101             | 2019-01-01     | 2020-01-01     | 12             | 36.30   | CONTRACT      | 54.99      | FUNNEL_BOOKING | BASIC          |
      | 99020    | OPDE    | EXPIRED             | ENABLED     | 102             | 2017-11-15     | 2018-11-15     | 12             | 0       | null          | 74.99      | FUNNEL_BOOKING | BASIC          |
      | 99021    | OPDE    | ACTIVATED           | DISABLED    | 102             | 2018-11-15     | 2019-11-15     | 12             | 18.19   | null          | 36.99      | FUNNEL_BOOKING | BASIC          |
      | 99030    | GOFR    | ACTIVATED           | ENABLED     | 103             | 2019-06-24     | 2019-09-24     | 3              | 42.50   | CONTRACT      | 0          | POST_BOOKING   | BASIC          |
      | 99040    | FR      | EXPIRED             | ENABLED     | 104             | 2019-01-10     | 2020-01-10     | 12             | 11.99   | CONTRACT      | 39.99      | FUNNEL_BOOKING | BASIC          |
      | 99041    | FR      | PENDING_TO_COLLECT  | ENABLED     | 104             | null           | 2021-01-10     | 6              | 54.99   | INIT          | 54.99      | FUNNEL_BOOKING | BUSINESS       |
      | 99050    | IT      | PENDING_TO_ACTIVATE | ENABLED     | 105             | null           | 2018-08-05     | 3              | 54.99   | null          | 0          | POST_BOOKING   | BUSINESS       |
      | 99051    | IT      | EXPIRED             | ENABLED     | 106             | 2018-08-05     | 2019-08-05     | 12             | 0.09    | CONTRACT      | 99.99      | FUNNEL_BOOKING | BUSINESS       |
      | 99052    | IT      | ACTIVATED           | ENABLED     | 106             | 2019-08-05     | 2020-08-05     | 12             | 54.99   | null          | 74.99      | FUNNEL_BOOKING | BUSINESS       |

    And the next member status action stored in db:
      | id | memberId | actionType | actionDate |
      | 1  | 99000    | CREATION   | 2017-06-06 |
      | 2  | 99000    | ACTIVATION | 2017-07-06 |
      | 3  | 99001    | CREATION   | 2017-07-05 |
      | 4  | 99020    | CREATION   | 2017-11-15 |
      | 5  | 99020    | ACTIVATION | 2017-11-15 |
      | 6  | 99020    | EXPIRATION | 2018-11-15 |


  Scenario Outline: membership search performed filtering membership fields without account or status actions (website,status,autoRenewal)

    When a search membership is requested with following parameters:
      | website   | status   | autoRenewal   | memberAccountId   |
      | <website> | <status> | <autoRenewal> | <memberAccountId> |

    Then the returned memberships by the search should include the memberships with the following ids <searchResult>

    Examples:
      | website | status             | autoRenewal | memberAccountId | searchResult      |
      | IT      | null               | null        | null            | 99050;99051;99052 |
      | GOFR    | ACTIVATED          | ENABLED     | null            | 99030             |
      | null    | ACTIVATED          | ENABLED     | null            | 99000;99030;99052 |
      | null    | null               | DISABLED    | null            | 99021             |
      | OPDE    | null               | ENABLED     | null            | 99020             |
      | null    | PENDING_TO_COLLECT | null        | null            | 99041             |
      | null    | PENDING_TO_COLLECT | DISABLED    | null            |                   |
      | null    | null               | null        | 106             | 99051;99052       |
      | null    | ACTIVATED          | ENABLED     | 100             | 99000             |

  Scenario Outline: membership search performed filtering membership fields without account or status actions (monthsDuration,fromActivationDate,toActivationDate,fromExpirationDate,toExpirationDate)

    When a search membership is requested with following parameters:
      | monthsDuration   | fromActivationDate   | toActivationDate   | fromExpirationDate   | toExpirationDate   | fromCreationDate   | toCreationDate   |
      | <monthsDuration> | <fromActivationDate> | <toActivationDate> | <fromExpirationDate> | <toExpirationDate> | <fromCreationDate> | <toCreationDate> |

    Then the returned memberships by the search should include the memberships with the following ids <searchResult>

    Examples:
      | monthsDuration | fromActivationDate | toActivationDate | fromExpirationDate | toExpirationDate | fromCreationDate | toCreationDate | searchResult                  |
      | null           | 2018-01-01         | 2018-12-01       | null               | null             | null             | null           | 99051;99021                   |
      | null           | null               | 2019-01-01       | null               | null             | null             | null           | 99000;99010;99020;99021;99051 |
      | null           | 2020-01-01         | null             | null               | null             | null             | null           |                               |
      | null           | null               | null             | 2020-02-01         | 2020-09-01       | null             | null           | 99052                         |
      | null           | null               | null             | 2020-02-01         | null             | null             | null           | 99041;99052                   |
      | 3              | null               | null             | null               | null             | null             | null           | 99050;99030                   |
      | 18             | 2018-01-01         | 2018-12-01       | null               | null             | null             | null           |                               |
      | null           | null               | null             | null               | null             | 2020-01-01       | 2020-02-01     |                               |


  Scenario Outline: membership search performed filtering membership fields without account or status actions (minBalance,maxBalance,productStatus,sourceType,membershipType,totalPrice)

    When a search membership is requested with following parameters:
      | minBalance   | maxBalance   | productStatus   | sourceType   | membershipType   | totalPrice   |
      | <minBalance> | <maxBalance> | <productStatus> | <sourceType> | <membershipType> | <totalPrice> |

    Then the returned memberships by the search should include the memberships with the following ids <searchResult>

    Examples:
      | minBalance | maxBalance | productStatus | sourceType   | membershipType | totalPrice | searchResult                  |
      | 37         | null       | null          | null         | null           | null       | 99000;99030;99041;99050;99052 |
      | null       | 0.10       | null          | null         | null           | null       | 99001;99020;99051             |
      | 10         | 20         | null          | null         | null           | null       | 99021;99040                   |
      | null       | null       | INIT          | null         | null           | null       | 99041                         |
      | null       | 40         | CONTRACT      | null         | null           | null       | 99010;99040;99051             |
      | null       | null       | null          | POST_BOOKING | null           | null       | 99030;99050                   |
      | null       | null       | null          | POST_BOOKING | BUSINESS       | 0          | 99050                         |
      | 50         | null       | null          | null         | null           | 0          | 99050                         |
      | 150        | null       | null          | null         | null           | 0          |                               |

  Scenario Outline: membership search with MemberStatusAction

    When a search membership is requested with following parameters:
      | website   | status   | autoRenewal   | withStatusActions   |
      | <website> | <status> | <autoRenewal> | <withStatusActions> |

    Then the returned memberships by the search <should> include status actions in the response

    Examples:
      | website | status             | autoRenewal | withStatusActions | should     |
      | IT      | null               | null        | true              | should     |
      | GOFR    | ACTIVATED          | ENABLED     | false             | should not |
      | OPDE    | null               | ENABLED     | true              | should     |
      | null    | PENDING_TO_COLLECT | null        | false             | should not |

  Scenario Outline: membership search with MemberAccount

    When a search membership is requested with following parameters:
      | website   | status   | autoRenewal   | withMemberAccount   |
      | <website> | <status> | <autoRenewal> | <withMemberAccount> |

    Then the returned memberships by the search <should> include member account in the response

    Examples:
      | website | status             | autoRenewal | withMemberAccount | should     |
      | IT      | null               | null        | true              | should     |
      | GOFR    | ACTIVATED          | ENABLED     | false             | should not |
      | OPDE    | null               | ENABLED     | true              | should     |
      | null    | PENDING_TO_COLLECT | null        | false             | should not |

  Scenario Outline: membership search with MemberAccount filter by account properties
    When a search membership is requested with following parameters:
      | firstName   | lastName   | userId   | withMemberAccount   |
      | <firstName> | <lastName> | <userId> | <withMemberAccount> |

    Then the returned memberships by the search should include the memberships with the following ids <searchResult>

    And the returned memberships by the search should include member account in the response

    Examples:
      | firstName | lastName | userId | withMemberAccount | searchResult      |
      | JOSE      | null     | null   | true              | 99000;99001       |
      | null      | GOMEZ    | null   | true              | 99000;99001;99010 |
      | null      | null     | 257    | true              | 99050;99051;99052 |
      | ANA       | GOMEZ    | null   | true              | 99010             |
      | null      | GOMEZ    | 1000   | true              | 99000;99001       |
      | MICK      | null     | 1400   | true              | 99040;99041       |

  Scenario Outline: membership search with ordering and pagination
    When a search membership is requested with following parameters:
      | autoRenewal | status   | sortBy   | sortCriteria | limit   | offset   |
      | <autorenew> | <status> | <sortBy> | <criteria>   | <limit> | <offset> |

    Then the returned memberships by the search should include the ordered memberships as following <searchResult>

    Examples:
      | autorenew | status    | sortBy          | criteria | limit | offset | searchResult                                                |
      | null      | ACTIVATED | ACTIVATION_DATE | ASC      | null  | null   | 99000;99021;99030;99052                                     |
      | null      | ACTIVATED | ACTIVATION_DATE | DESC     | null  | null   | 99052;99030;99021;99000                                     |
      | ENABLED   | null      | ID              | ASC      | null  | null   | 99000;99001;99010;99020;99030;99040;99041;99050;99051;99052 |
      | ENABLED   | null      | ID              | DESC     | null  | null   | 99052;99051;99050;99041;99040;99030;99020;99010;99001;99000 |
      | ENABLED   | null      | ID              | ASC      | 2     | null   | 99000;99001                                                 |
      | ENABLED   | null      | ID              | DESC     | 2     | 2      | 99050;99041                                                 |
      | null      | ACTIVATED | ACTIVATION_DATE | DESC     | 3     | null   | 99052;99030;99021                                           |

  Scenario Outline: membership search with email
    When a search membership is requested with following parameters:
      | email   | website   |
      | <email> | <website> |
    Then the returned memberships by the search should include the memberships with the following ids <searchResult>

    Examples:
      | email                         | website | searchResult      |
      | registeredMaxBiaggi@email.com | IT      | 99050;99051;99052 |
      | mahatZastred@email.com        | IT      |                   |
      | registeredMaxBiaggi@email.com | ES      |                   |
      



