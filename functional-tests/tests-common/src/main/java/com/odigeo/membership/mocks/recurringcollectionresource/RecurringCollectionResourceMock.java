package com.odigeo.membership.mocks.recurringcollectionresource;

import com.edreamsodigeo.payments.recurringcollection.contract.RecurringCollectionResource;
import com.edreamsodigeo.payments.recurringcollection.contract.model.recurringcollection.RecurringCollectionId;
import com.edreamsodigeo.payments.recurringcollection.contract.model.recurringcollection.RecurringOrderConditions;
import com.edreamsodigeo.payments.recurringcollection.contract.request.CalculateRecurringCollectionConditionsRequest;
import com.edreamsodigeo.payments.recurringcollection.contract.request.CreateRecurringCollectionRequest;

import java.util.UUID;

public class RecurringCollectionResourceMock implements RecurringCollectionResource {

    private UUID generatedUUID;

    @Override
    public RecurringCollectionId create(CreateRecurringCollectionRequest createRecurringCollectionRequest) {
        generatedUUID = UUID.randomUUID();
        return new RecurringCollectionId(generatedUUID);
    }

    @Override
    public RecurringOrderConditions calculateRecurringOrderConditions(CalculateRecurringCollectionConditionsRequest calculateRecurringCollectionConditionsRequest) {
        throw new UnsupportedOperationException("RecurringCollectionResource calculateRecurringOrderConditions mock endpoint not implemented");
    }

    public UUID getGeneratedUUID() {
        return generatedUUID;
    }
}
