package com.odigeo.membership.member.checkoutapi;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.checkout.checkoutapi.v1.exception.ProductException;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.PaymentOptions;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.Product;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.payment.RepricingStatus;
import com.edreamsodigeo.checkout.checkoutapi.v1.request.ProductPaymentSpecIdRequest;
import com.edreamsodigeo.shoppingbasket.membershipcheckoutapi.v1.MembershipCheckoutService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.checkoutapi.mapper.ProductMapper;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class MembershipCheckoutApiController implements MembershipCheckoutService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipCheckoutApiController.class);
    private static final String ERROR = "Product membership error with product id: ";
    private MemberService membershipService;

    @Override
    public Product getProduct(String productId) throws ProductException {
        try {
            return ProductMapper.INSTANCE
                .map(getMembershipService().getProductById(productId));
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, "Error retrieving MembershipProduct with id " + productId);
            throw new ProductException(ERROR + productId, e);
        }
    }

    @Override
    public PaymentOptions getPaymentOptions(String paymentId, String paymentType) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RepricingStatus setProductPaymentSpecId(String transactionId, ProductPaymentSpecIdRequest productPaymentSpecIdRequest) throws ProductException {
        throw new UnsupportedOperationException();
    }

    private MemberService getMembershipService() {
        return Optional.ofNullable(membershipService).orElseGet(() -> {
            membershipService = ConfigurationEngine.getInstance(MemberService.class);
            return membershipService;
        });
    }
}
