package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@ScenarioScoped
public class MembershipValidationSteps extends CommonSteps {

    private final MembershipManagementWorld world;

    @Inject
    public MembershipValidationSteps(MembershipManagementWorld world, ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
        this.world = world;
    }

    @When("^check if website (\\w+) supports membership$")
    public void checkIfWebsiteSiteSupportsMembership(final String site) throws InvalidParametersException {
        world.setMembershipSupportedForSite(membershipService.isMembershipPerksActiveOn(site));
    }

    @Then("^website does not support membership$")
    public void websiteDoesNotSupportMembership() {
        assertFalse(world.isMembershipSupportedForSite());
    }

    @Then("^website supports membership$")
    public void websiteSupportsMembership() {
        assertTrue(world.isMembershipSupportedForSite());
    }

    @When("^check if (\\d+) needs to be renewed$")
    public void membershipIsActiveAndPastDue(Long membershipId) {
        world.setMembershipToBeRenewed(membershipService.isMembershipToBeRenewed(membershipId));
    }

    @Then("^the member is to be renewed$")
    public void theMemberIsToBeRenewed() {
        assertTrue(world.isMembershipToBeRenewed());
    }

    @Then("^the member is not to be renewed$")
    public void theMemberIsNotToBeRenewed() {
        assertFalse(world.isMembershipToBeRenewed());
    }
}
