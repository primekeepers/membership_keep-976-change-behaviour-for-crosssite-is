package com.odigeo.product.membership.rest;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ServiceApplicationTest {

    private ServiceApplication serviceApplication;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        serviceApplication = new ServiceApplication();
    }

    @Test
    public void testGetInstance() {
        assertNotNull(serviceApplication);
    }

    @Test
    public void testGetSingletons() {
        serviceApplication = new ServiceApplication();
        assertNotNull(serviceApplication.getSingletons());
        assertEquals(serviceApplication.getSingletons().size(), 1);
    }
}
