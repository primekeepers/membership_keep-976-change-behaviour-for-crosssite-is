Feature: Test deactivate membership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 1        | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3        | ES          | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |
    And property to send IDs to Kafka has value false in db:

  Scenario Outline: Deactivate active member
    Given the user-api has the following user info
      | userId   | status       | email   | brand | token                    | tokenType        |
      | <userId> | <userStatus> | <email> | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When requested to disable member with memberId 1
    Then the userId 4321 and website ES is DEACTIVATED
    And member with memberId 1 has the auto renewal with status DISABLED
    And membershipSubscriptionMessage is correctly sent to kafka queue as <subscriptionStatus> and email will be sent to <email>
    And the message with shouldSetPassword <shouldSetPassword> and passwordToken <passwordToken> is sent to kafka
    And the membershipId 1 has the remind me later flag not set
    And membershipSubscriptionMessage is correctly sent to kafka queue with remind me later set to false

    Examples:
      | userId  | email                        | userStatus    | passwordTokenGenerated | shouldSetPassword | passwordToken  | subscriptionStatus |
      | 4321    | neji.hyuga@edreamsodigeo.com | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA | UNSUBSCRIBED       |
      | 4321    | neji.hyuga@edreamsodigeo.com | ACTIVE        | passwordTokenB         | false             | null           | UNSUBSCRIBED       |
      | 4321    | neji.hyuga@edreamsodigeo.com | PENDING_LOGIN | null                   | false             | null           | UNSUBSCRIBED       |

  Scenario: Deactivate non-active member
    When requested to disable member with memberId 3
    Then the member is not disabled
    And the number of membership subscription messages sent is 0
    And the membershipId 1 has the remind me later flag not set
