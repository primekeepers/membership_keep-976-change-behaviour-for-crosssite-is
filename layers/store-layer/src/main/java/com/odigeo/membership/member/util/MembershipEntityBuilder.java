package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.db.MembershipField;
import com.odigeo.membership.parameters.search.MembershipSearch;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.NullAssignment")
@Singleton
public class MembershipEntityBuilder {

    public static final String AUTO_RENEWAL_DISABLED = MembershipRenewal.DISABLED.toString();
    public static final String AUTO_RENEWAL_ENABLED = MembershipRenewal.ENABLED.toString();

    public Membership build(ResultSet rs, boolean withAccount) throws SQLException {
        MembershipBuilder membershipBuilder = new MembershipBuilder();
        fillCommonFields(rs, membershipBuilder, withAccount);
        return membershipBuilder.build();
    }

    public List<Membership> buildListFromSearch(MembershipSearch membershipSearch, ResultSet resultSet) throws SQLException {
        Map<Long, Membership> membershipMap = new LinkedHashMap<>();
        while (resultSet.next()) {
            Long membershipId = resultSet.getLong(MembershipField.ID.name());
            membershipMap.putIfAbsent(membershipId, build(resultSet, membershipSearch.isWithMemberAccount()));
            Membership membership = membershipMap.get(membershipId);
            extractRecurringFromResultSetToMembership(resultSet, membership);
            if (membershipSearch.isWithStatusActions()) {
                membership.getMemberStatusActions()
                        .add(getStatusActionEntityBuilder().build(resultSet));
            }
        }
        return new ArrayList<>(membershipMap.values());
    }

    private void extractRecurringFromResultSetToMembership(ResultSet resultSet, Membership membership) throws SQLException {
        getMembershipRecurringEntityBuilder().build(resultSet)
                .ifPresent(membership.getMembershipRecurring()::add);
    }

    public List<Membership> buildListFromResultSet(ResultSet rs) throws SQLException {
        Map<Long, Membership> membershipMap = new LinkedHashMap<>();
        while (rs.next()) {
            Long membershipId = rs.getLong(MembershipField.ID.name());
            membershipMap.putIfAbsent(membershipId, build(rs, false));
            Membership membership = membershipMap.get(membershipId);
            extractRecurringFromResultSetToMembership(rs, membership);
        }
        return membershipMap.values().stream().collect(Collectors.toList());
    }

    private void fillCommonFields(ResultSet rs, MembershipBuilder membershipBuilder, boolean withAccount) throws SQLException {
        Long membershipId = rs.getLong("ID");
        String website = rs.getString("WEBSITE");
        MemberStatus status = MemberStatus.valueOf(rs.getString("STATUS"));
        MembershipRenewal membershipRenewal = MembershipRenewal.valueOf(rs.getString("AUTO_RENEWAL"));
        long memberAccountId = rs.getLong("MEMBER_ACCOUNT_ID");
        Timestamp activationDate = rs.getTimestamp("ACTIVATION_DATE");
        Timestamp expirationDate = rs.getTimestamp("EXPIRATION_DATE");
        BigDecimal balance = rs.getBigDecimal("BALANCE");
        MembershipType membershipType = MembershipType.valueOf(rs.getString("MEMBERSHIP_TYPE"));
        SourceType sourceType = SourceType.getNullableValue(rs.getString("SOURCE_TYPE"));
        int monthsDuration = rs.getInt("MONTHS_DURATION");
        String prStatus = rs.getString("PRODUCT_STATUS");
        ProductStatus productStatus = prStatus == null ? null : ProductStatus.valueOf(prStatus);
        String recurringId = rs.getString("RECURRING_ID");
        Timestamp timestamp = rs.getTimestamp("MEMBERSHIP_TIMESTAMP");
        String currencyCode = rs.getString("CURRENCY_CODE");
        BigDecimal totalPrice = rs.getBigDecimal("TOTAL_PRICE");
        BigDecimal renewalPrice = rs.getBigDecimal("RENEWAL_PRICE");
        Integer renewalDuration = rs.getObject("MONTHS_RENEWAL_DURATION") != null ? rs.getInt("MONTHS_RENEWAL_DURATION") : null;
        TimeUnit timeUnit = TimeUnit.getFromNullableValue(rs.getString("DURATION_TIME_UNIT"));
        Integer duration = rs.getObject("DURATION") != null ? rs.getInt("DURATION") : null;
        Long feeContainerId = rs.getObject("FEE_CONTAINER_ID") != null ? rs.getLong("FEE_CONTAINER_ID") : null;
        MemberAccount memberAccount = null;
        if (withAccount) {
            memberAccount = getMemberAccountEntityBuilder().build(rs, "MEMBER_ACCOUNT_ID");
        }
        Boolean rmlFlag = "Y".equals(rs.getString("REMIND_ME_LATER_FLAG"));
        Timestamp rmlLastUpdate = rs.getTimestamp("REMIND_ME_LATER_LAST_UPDATE");

        membershipBuilder.setId(membershipId).setWebsite(website).setStatus(status)
                .setMembershipRenewal(membershipRenewal).setMemberAccountId(memberAccountId)
                .setBalance(balance).setMembershipType(membershipType)
                .setMonthsDuration(monthsDuration)
                .setDurationTimeUnit(timeUnit)
                .setDuration(duration)
                .setSourceType(sourceType)
                .setProductStatus(productStatus)
                .setRecurringId(recurringId)
                .setRenewalDuration(renewalDuration)
                .setMembershipPricesBuilder(MembershipPrices.builder()
                        .currencyCode(currencyCode)
                        .totalPrice(totalPrice)
                        .renewalPrice(renewalPrice))
                .setMemberStatusActions(new ArrayList<>())
                .setMembershipRecurring(new ArrayList<>())
                .setFeeContainerId(feeContainerId)
                .setMemberAccount(memberAccount)
                .setRemindMeLater(rmlFlag);
        setRecurringCollectionId(rs.getString("RECURRING_COLLECTION_ID"), membershipBuilder);
        setTimestamp(timestamp, membershipBuilder);
        setRMLLastUpdate(rmlLastUpdate, membershipBuilder);
        setActivationDate(activationDate, membershipBuilder);
        setExpirationDate(expirationDate, membershipBuilder);
    }

    private void setRecurringCollectionId(String recurringCollectionId, MembershipBuilder membershipBuilder) {
        if (StringUtils.isNotEmpty(recurringCollectionId)) {
            membershipBuilder.setRecurringCollectionId(UUID.fromString(recurringCollectionId));
        }
    }

    private static void setActivationDate(Timestamp activationDate, MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(activationDate)) {
            membershipBuilder.setActivationDate(activationDate.toLocalDateTime());
        }
    }

    private static void setExpirationDate(Timestamp expirationDate, MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(expirationDate)) {
            membershipBuilder.setExpirationDate(expirationDate.toLocalDateTime());
        }
    }

    private static void setTimestamp(final Timestamp timestamp, final MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(timestamp)) {
            membershipBuilder.setTimestamp(timestamp.toLocalDateTime());
        }
    }

    private static void setRMLLastUpdate(final Timestamp timestamp, final MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(timestamp)) {
            membershipBuilder.setRemindMeLaterLastUpdate(timestamp.toLocalDateTime());
        }
    }

    private MemberAccountEntityBuilder getMemberAccountEntityBuilder() {
        return ConfigurationEngine.getInstance(MemberAccountEntityBuilder.class);
    }

    private StatusActionEntityBuilder getStatusActionEntityBuilder() {
        return ConfigurationEngine.getInstance(StatusActionEntityBuilder.class);
    }

    private MembershipRecurringEntityBuilder getMembershipRecurringEntityBuilder() {
        return ConfigurationEngine.getInstance(MembershipRecurringEntityBuilder.class);
    }
}
