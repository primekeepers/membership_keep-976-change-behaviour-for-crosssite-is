package com.odigeo.product.membership.rest;

import com.odigeo.commons.rest.error.SimpleExceptionBean;
import com.odigeo.product.response.exception.MembershipProductServiceException;
import com.odigeo.product.v2.exception.ProductException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ProductNotFoundExceptionMapperV2 implements ExceptionMapper<ProductException> {

    public final Response toResponse(ProductException exception) {
        int responseStatus = Response.Status.BAD_REQUEST.getStatusCode();
        if (exception instanceof MembershipProductServiceException) {
            MembershipProductServiceException membershipProductServiceException = (MembershipProductServiceException) exception;
            responseStatus = membershipProductServiceException.getMembershipProductServiceExceptionType().getHttpCode();
        }

        return Response.status(responseStatus).type(this.mediaTypeToSend()).entity(new SimpleExceptionBean(exception, this.sendExceptionCause())).build();
    }

    protected String mediaTypeToSend() {
        return MediaType.APPLICATION_JSON;
    }

    public boolean sendExceptionCause() {
        return true;
    }

}
