package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.PostBookingPageInfo;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.exception.MembershipForbiddenException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipPreconditionFailedException;
import com.odigeo.membership.member.MembershipPostBookingService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.request.postbooking.PostBookingPageInfoRequest;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.response.PostBookingPageInfoResponse;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.odigeo.membership.member.PostBookingParser.DEFAULT_MONTHS_TO_RENEWAL;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNull;

public class PostBookingMemberServiceControllerTest {

    private static final String EMAIL = "carlos.marquez@edreams.com";
    @Mock
    private MembershipPostBookingService membershipPostBookingService;

    @Mock
    private PostBookingPageInfoRequest postBookingPageInfoRequest;

    @Mock
    private MapperFacade mapper;

    @Captor
    private ArgumentCaptor<MembershipCreation> postBookingCreationArgumentCaptor;

    private PostBookingMemberServiceController postBookingMemberServiceController;

    private final String USER_ID = "333";

    @BeforeMethod
    public void init() {
        openMocks(this);
        postBookingMemberServiceController = new PostBookingMemberServiceController(mapper);
        ConfigurationEngine.init(this::configureBinders);
        postBookingPageInfoRequest = new PostBookingPageInfoRequest();
        postBookingPageInfoRequest.setToken("token");
    }

    private void configureBinders(Binder binder) {
        binder.bind(MembershipPostBookingService.class).toInstance(membershipPostBookingService);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class, expectedExceptionsMessageRegExp = "Error retrieving file: .*")
    public void testProcessPostBookingCreation() throws IOException {
        MultipartFormDataInput multipartFormDataInput = mockMultipartFormDataInput(true);
        postBookingMemberServiceController.processPostBookingCreation(multipartFormDataInput);
    }

    @Test
    public void testProcessPostBookingCreationErrors() throws IOException {
        MultipartFormDataInput multipartFormDataInput = mockMultipartFormDataInput(false);
        when(membershipPostBookingService.parseInfoAndCreateAccounts(anyString())).thenReturn(Collections.emptyList());
        postBookingMemberServiceController.processPostBookingCreation(multipartFormDataInput);
    }

    private MultipartFormDataInput mockMultipartFormDataInput(boolean withErrorRetrievingBody) throws IOException {
        MultipartFormDataInput multipartFormDataInput = mock(MultipartFormDataInput.class);
        Map<String, List<InputPart>> formDataMap = mock(Map.class);
        List<InputPart> inputParts = mock(List.class);
        InputPart inputPart = mock(InputPart.class);

        when(multipartFormDataInput.getFormDataMap()).thenReturn(formDataMap);
        when(formDataMap.get(any())).thenReturn(inputParts);
        when(inputParts.get(anyInt())).thenReturn(inputPart);
        if (withErrorRetrievingBody) {
            when(inputPart.getBody(any(), any())).thenThrow(new IOException());
        } else {
            when(inputPart.getBody(any(), any())).thenReturn("test;".getBytes());
        }
        return multipartFormDataInput;
    }

    @Test(expectedExceptions = MembershipPreconditionFailedException.class, expectedExceptionsMessageRegExp = "Duplicated account")
    public void testCreatePostBookingMembershipExceptionExpected()
            throws DataAccessException, ActivatedMembershipException {
        when(membershipPostBookingService.createPostBookingMembership(any(MembershipCreation.class), anyString())).thenThrow(new ActivatedMembershipException("Duplicated account"));
        postBookingMemberServiceController.createPostBookingMembership(getPostBookingRequest());
    }

    @Test
    public void testCreatePostBookingMembershipDefaultMonthsToRenewal() throws DataAccessException, ActivatedMembershipException {
        when(membershipPostBookingService.createPostBookingMembership(any(MembershipCreation.class), anyString())).thenReturn(333L);
        postBookingMemberServiceController.createPostBookingMembership(getPostBookingRequestWithMonthsToRenewal(StringUtils.EMPTY));

        verify(membershipPostBookingService).createPostBookingMembership(postBookingCreationArgumentCaptor.capture(), eq(EMAIL));
        assertEquals(Duration.between(postBookingCreationArgumentCaptor.getValue().getExpirationDate(),
                LocalDateTime.now().plusMonths(DEFAULT_MONTHS_TO_RENEWAL)).toMinutes(), 0);
    }

    @Test(expectedExceptions = MembershipForbiddenException.class, expectedExceptionsMessageRegExp = "Maximum number of months to renewal exceeded")
    public void testCreatePostBookingMembershipNotAllowedMonthsToRenewal() {
        postBookingMemberServiceController.createPostBookingMembership(getPostBookingRequestWithMonthsToRenewal("14"));
    }

    @Test(expectedExceptions = MembershipBadRequestException.class, expectedExceptionsMessageRegExp = "Months to renewal cannot be 0 or negative")
    public void testCreatePostBookingMembershipZeroMonthsToRenewal() {
        postBookingMemberServiceController.createPostBookingMembership(getPostBookingRequestWithMonthsToRenewal("0"));
    }

    @Test(expectedExceptions = MembershipBadRequestException.class, expectedExceptionsMessageRegExp = "Number of months to renewal with non numerical format")
    public void testCreatePostBookingMembershipWrongFormatMonthsToRenewal() {
        postBookingMemberServiceController.createPostBookingMembership(getPostBookingRequestWithMonthsToRenewal("3."));
    }

    @Test(expectedExceptions = MembershipBadRequestException.class, expectedExceptionsMessageRegExp = "User id missing or with non numerical format")
    public void testCreatePostBookingMembershipMissingUserId() {
        CreateMembershipSubscriptionRequest request = getPostBookingRequest(StringUtils.EMPTY, Integer.toString(DEFAULT_MONTHS_TO_RENEWAL));
        postBookingMemberServiceController.createPostBookingMembership(request);
    }

    @Test
    public void testCreatePostBookingPendingMembership() throws DataAccessException, ActivatedMembershipException, InvalidParametersException {
        when(membershipPostBookingService.createPostBookingMembershipPending(any(MembershipCreation.class))).thenReturn(333L);
        postBookingMemberServiceController.createPostBookingPendingMembership(getPostBookingRequest());
        verify(membershipPostBookingService).createPostBookingMembershipPending(postBookingCreationArgumentCaptor.capture());
        assertEquals(postBookingCreationArgumentCaptor.getValue().getMemberStatus(), MemberStatus.PENDING_TO_ACTIVATE);
        assertNull(postBookingCreationArgumentCaptor.getValue().getExpirationDate());
    }

    @Test(expectedExceptions = MembershipPreconditionFailedException.class, expectedExceptionsMessageRegExp = "Activated already exists")
    public void testCreatePostBookingPendingMembershipException() throws DataAccessException, ActivatedMembershipException, InvalidParametersException {
        when(membershipPostBookingService.createPostBookingMembershipPending(any(MembershipCreation.class))).thenThrow(new ActivatedMembershipException("Activated already exists"));
        postBookingMemberServiceController.createPostBookingPendingMembership(getPostBookingRequest());
    }

    @Test(expectedExceptions = MembershipBadRequestException.class, expectedExceptionsMessageRegExp = "Months to renewal cannot be 0 or negative")
    public void testCreatePostBookingMembershipPendingZeroMonthsToRenewal() throws InvalidParametersException {
        postBookingMemberServiceController.createPostBookingPendingMembership(getPostBookingRequestWithMonthsToRenewal("0"));
    }

    @Test(expectedExceptions = MembershipBadRequestException.class, expectedExceptionsMessageRegExp = "Number of months to renewal with non numerical format")
    public void testCreatePostBookingMembershipPendingWrongFormatMonthsToRenewal() throws InvalidParametersException {
        postBookingMemberServiceController.createPostBookingPendingMembership(getPostBookingRequestWithMonthsToRenewal("ciao"));
    }

    @Test
    public void testGetPostBookingPageInfo() throws Exception {
        PostBookingPageInfo postBookingPageInfo = new PostBookingPageInfo().setBookingId(1234L).setEmail("user@mail.com");
        when(membershipPostBookingService.getPostBookingPageInfo(anyString())).thenReturn(postBookingPageInfo);
        PostBookingPageInfoResponse postBookingPageInfoResponse = postBookingMemberServiceController.getPostBookingPageInfo(postBookingPageInfoRequest);
        assertEquals(postBookingPageInfoResponse, mapper.map(postBookingPageInfo, PostBookingPageInfoResponse.class));
    }

    @Test(expectedExceptions = MembershipBadRequestException.class, expectedExceptionsMessageRegExp = PostBookingPageInfo.EXCEPTION_MESSAGE)
    public void testGetPostBookingPageInfoShouldThrowMembershipServiceExceptionWhenDecryptionFails() throws Exception {
        when(membershipPostBookingService.getPostBookingPageInfo(anyString())).
                thenThrow(new IllegalArgumentException());
        postBookingMemberServiceController.getPostBookingPageInfo(postBookingPageInfoRequest);
    }

    private CreateMembershipSubscriptionRequest getPostBookingRequestWithMonthsToRenewal(String monthsToRenewal) {
        return getPostBookingRequest(USER_ID, monthsToRenewal);
    }

    private CreateMembershipSubscriptionRequest getPostBookingRequest() {
        return getPostBookingRequest(USER_ID, Integer.toString(DEFAULT_MONTHS_TO_RENEWAL));
    }

    private CreateMembershipSubscriptionRequest getPostBookingRequest(String userId, String monthsToRenewal) {
        CreateMembershipSubscriptionRequest request = new CreateMembershipSubscriptionRequest();
        request.setUserId(userId);
        request.setEmail(EMAIL);
        request.setName(StringUtils.EMPTY);
        request.setLastNames(StringUtils.EMPTY);
        request.setWebsite("ES");
        request.setMonthsToRenewal(monthsToRenewal);
        return request;
    }
}
