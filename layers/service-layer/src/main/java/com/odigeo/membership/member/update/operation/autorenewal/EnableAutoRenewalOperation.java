package com.odigeo.membership.member.update.operation.autorenewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;

public class EnableAutoRenewalOperation extends AutoRenewalChangeOperation {

    public interface Factory extends MembershipUpdateOperationFactory {
        EnableAutoRenewalOperation createOperation(DataSource dataSource);
    }

    @Inject
    public EnableAutoRenewalOperation(@Assisted DataSource dataSource,
                                      MembershipStore membershipStore,
                                      MembershipMessageSendingManager membershipMessageSendingManager,
                                      MemberService membershipService) {
        super(membershipStore, membershipMessageSendingManager, membershipService, dataSource);
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        return MembershipRenewal.ENABLED.name().equals(enableAutoRenewal(updateMembership));
    }

    public String enableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        return changeAutoRenewal(Long.parseLong(updateMembership.getMembershipId()), MembershipRenewal.ENABLED, MembershipRenewal.DISABLED, AutoRenewalOperation.ENABLE_AUTO_RENEW);
    }
}
