package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MembershipStore;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;

public class ConsumeMembershipBalanceOperation implements MembershipUpdateOperation {
    private final DataSource dataSource;
    private final MembershipStore membershipStore;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        ConsumeMembershipBalanceOperation createOperation(DataSource dataSource);
    }

    @Inject
    public ConsumeMembershipBalanceOperation(@Assisted DataSource dataSource,
                                             MembershipStore membershipStore) {
        this.dataSource = dataSource;
        this.membershipStore = membershipStore;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        try {
            return membershipStore.updateMembershipBalance(dataSource, Long.parseLong(updateMembership.getMembershipId()), BigDecimal.ZERO);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to reset membership balance to zero for membershipId " + updateMembership.getMembershipId(), e);
        }
    }
}
