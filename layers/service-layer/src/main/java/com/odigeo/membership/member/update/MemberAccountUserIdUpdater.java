package com.odigeo.membership.member.update;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.AuditMemberAccountManager;
import com.odigeo.membership.member.MemberAccountStore;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import static java.util.Objects.isNull;

@Singleton
public class MemberAccountUserIdUpdater {

    private final MemberAccountStore memberAccountStore;
    private final AuditMemberAccountManager auditMemberAccountManager;

    @Inject
    public MemberAccountUserIdUpdater(MemberAccountStore memberAccountStore, AuditMemberAccountManager auditMemberAccountManager) {
        this.memberAccountStore = memberAccountStore;
        this.auditMemberAccountManager = auditMemberAccountManager;
    }
    public boolean updateMemberAccountUserId(DataSource dataSource, UpdateMemberAccount updateMemberAccount) throws DataAccessRollbackException {
        return updateUserId(dataSource, Long.parseLong(updateMemberAccount.getMemberAccountId()), updateMemberAccount.getUserId());
    }
    public boolean updateMemberAccountUserId(DataSource dataSource, long memberAccountId, long newUserId) throws DataAccessException {
        return updateUserId(dataSource, memberAccountId, newUserId);
    }

    public void validateParametersForUpdateUserIdOperation(UpdateMemberAccount updateMemberAccount) {
        if (isNull(updateMemberAccount.getMemberAccountId())
                || isNull(updateMemberAccount.getUserId())
                && (isNull(updateMemberAccount.getEmail()) || isNull(updateMemberAccount.getWebsite()))) {
            throw new InvalidParameterException("MemberAccountId, the new user email and website or userId must be informed to update the userId");
        }
    }
    private boolean updateUserId(DataSource dataSource, long memberAccountId, long newUserId) throws DataAccessRollbackException {
        try {
            boolean hasBeenSuccessfullyUpdated = memberAccountStore.updateMemberAccountUserId(dataSource, memberAccountId, newUserId);
            if (hasBeenSuccessfullyUpdated) {
                auditMemberAccountManager.auditUpdatedMemberAccount(dataSource, memberAccountId);
            }
            return hasBeenSuccessfullyUpdated;
        } catch (SQLException | DataNotFoundException e) {
            throw new DataAccessRollbackException("There was an error trying to update memberAccountId " + memberAccountId + " with userId " + newUserId, e);
        }
    }
}
