package com.odigeo.membership.redis;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class RedisClientTest {

    private static final String HOST = "host";
    private static final int PORT = 6379;

    @Mock
    private RedisClientConfiguration redisClientConfiguration;
    @Mock
    private JedisPool jedisPool;
    @Mock
    private Jedis jedis;

    private RedisClient redisClient;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        setUpRedisClientConfiguration();
        redisClient = new RedisClient(redisClientConfiguration);
    }

    private void setUpRedisClientConfiguration() {
        when(redisClientConfiguration.getHost()).thenReturn(HOST);
        when(redisClientConfiguration.getPort()).thenReturn(PORT);
    }

    @Test
    public void testJedisConnection() {
        setUpCreateJedisPool();
        redisClient.setJedisPool(jedisPool);
        Jedis jedis = redisClient.getJedisConnection();
        assertNotNull(jedis);
    }

    private void setUpCreateJedisPool() {
        when(jedisPool.getResource()).thenReturn(jedis);
    }

    @Test
    public void testRedisClientConfiguration() {
        setUpCreateJedisPool();
        redisClient.setJedisPool(jedisPool);
        assertEquals(redisClient.getRedisClientConfiguration(), HOST + ":" + PORT);
    }

    @Test(expectedExceptions = JedisConnectionException.class)
    public void testJedisConnectionWrongHostPort() throws JedisConnectionException {
        setUpCreateJedisPool();
        Jedis jedis = redisClient.getJedisConnection();
    }

}