## Membership ##

[![Build Status](http://bcn-jenkins-01.odigeo.org/jenkins/buildStatus/icon?job=membership)](http://bcn-jenkins-01.odigeo.org/jenkins/buildStatus/icon?job=membership)
![Coverage](http://qualitysonar.odigeo.org:8090/api/project_badges/measure?project=com.odigeo.membership%3Amembership-parent&metric=coverage)

## Deploying the module in docker local and QA database ##

- `` mvn clean install -P build-container-image, dev ``

- `` make -f generate-docker-compose-file ``
- `` docker-compose -f generated-docker-compose.yml up ``

- Check: ``http://localhost:8080/membership/engineering/ping``
    
## Launching functional tests in Docker ##
### Launching all the tests ###
1. Select these Profiles:
    - build-container-image
    - dev
    - get-local-ip (only in Windows machines)
    - run-automated-test
2. Execute clean + install from Membership (root)
3. The resultant reports of the test execution can be found in: 
    - \functional-tests\tests-v1\target\surefire-reports
    - \functional-tests\tests-v1\target\cucumber

(The console command is: ``mvn clean install -P build-container-image, dev, run-automated-test``)

### Launching one single feature: ###
1. Create a Run/debug configuration of the class BaseMembershipDockerTest    
2. Remove build in Before launch section
3. In VM Options: -ea -Dproject.parent.version=\<module version\> -Ddocker.image.env=dev -Dcucumber.options="classpath:<path-to-feature>/<feature-name>.feature"
4. Make sure that in the Use classpath of module you have the option 'tests-v1' selected
5. Add the next maven goal to launch section in functional-test working directory:
   
    ``clean install -P run-automated-test -Dmaven.test.skip.exec=true -pl functional-tests``   


## Database versioning with Scripts-Layer: ##

### Scripts structure and location ###

- The base version of the model is located in scripts-layer\src\main\resources\com\odigeo\resources\scripts\schema\0\oracle\membership_own:
    - 00_log_table.sql
    - 01_sequences.sql
    - 02_tables_sql
    - 03_indexes.sql
    
1. Place the DDL scripts in:
    - scripts-layer\src\main\resources\com\odigeo\resources\scripts\schema\next\oracle\membership_own\<JIRA_TICKET>
    - The scripts must be numbered in order of execution and have a significant name such as:
        - 01_Add_column_balance_membership_table_ddl.sql
        - 02_Remove_column_quantity_fees_table_ddl.sql
        - 03_Add_trigger_insert_new_member_ddl.plsql

    - In scripts-layer\src\main\resources\com\odigeo\resources\scripts\schema\next\oracle\membership_own place an empty sql file:
        - doNotRemoveThisFile.sql    
        
2. Place the DML needed for QA in:
    - scripts-layer\src\main\resources\com\odigeo\resources\scripts\dataset\<RELEASE_VERSION>\oracle\membership_own\qa
        - 01_insert_qa_memberships.sql
        - 02_update_qa_membership_fees.sql
        - 03_delete_qa_membership_.sql
        
3. Place the DML related to the release in:
    - scripts-layer\src\main\resources\com\odigeo\resources\scripts\prod-only\next\oracle\membership_own\<JIRA_TICKET>
        - 01_insert.sql
        - 02_update.sql
        - 03_delete.sql
    - Also, for every ticket with changes in the database, a "rollback" folder must be created with the opposite changes 
    to get database to its original status. Create it just inside the <JIRA_TICKET> folder. Rollback scripts need to have prefix R_ 
    according to DIM naming rules: https://jira.odigeo.com/wiki/display/DIM/JIRA+SQL+-+Edreams+Odigeo+standard+for+SQL+tickets 
    - Also, place an empty sql file in scripts-layer\src\main\resources\com\odigeo\resources\scripts\prod-only\next\oracle\membership_own:
        - doNotRemoveThisFile.sql

### Creating a database change release ###

Release of database changes is created as part of Jenkins job execution, that creates membership image of prod fork http://bcn-jenkins-01.odigeo.org/jenkins/job/rm-docker-release-generator/.
It uploads a zip with scripts to nexus and they can be executed on DEV or QA database with Scripts Deploy Center:
    https://scripts-deploy-center.services.odigeo.com/scripts-deploy-center/ providing groupId, artifactId and version
More details on applying scripts in Confluence.
