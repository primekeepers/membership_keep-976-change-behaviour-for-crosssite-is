package com.odigeo.membership.enums.db;

import static com.odigeo.membership.enums.db.FieldName.TableAlias.MEMBERSHIP_RECURRING_COLLECTION_ALIAS;

public enum MembershipRecurringCollectionField implements FieldName {
    MEMBERSHIP_ID,
    RECURRING_COLLECTION_ID,
    TIMESTAMP;

    @Override
    public String withTableAlias() {
        return MEMBERSHIP_RECURRING_COLLECTION_ALIAS + "." + name();
    }

    @Override
    public String filteringName() {
        return withTableAlias();
    }
}
