package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.member.discount.ApplyDiscountChecker;
import com.odigeo.membership.parameters.BrandedEmail;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.membership.parameters.UserIdentification;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.product.FreeTrialCandidate;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Stateless
@Local(MembershipValidationService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MembershipValidationServiceBean extends AbstractServiceBean implements MembershipValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipValidationServiceBean.class);
    private static final String ACTIVATED = "ACTIVATED";
    public static final Set<MembershipType> FREE_TRIAL_TYPES = Set.of(MembershipType.BASIC, MembershipType.PLUS);
    private static final Predicate<Membership> IS_FREE_TRIAL_MEMBERSHIP_TYPE = membership -> FREE_TRIAL_TYPES.contains(membership.getMembershipType());
    private static final Predicate<Membership> IS_FUNNEL_SOURCE = membership -> SourceType.FUNNEL_BOOKING.equals(membership.getSourceType());
    private static final Predicate<Membership> IS_DURATION_OF_A_FREE_TRIAL = membership -> (membership.getMonthsDuration() == 1 || TimeUnit.DAYS.equals(membership.getDurationTimeUnit()));
    private static final Predicate<Membership> IS_AUTO_RENEWAL_DISABLED = membership -> membership.getAutoRenewal().equals(MembershipRenewal.DISABLED);
    private static final Predicate<Membership> PROHIBITS_FREE_TRIAL = IS_FREE_TRIAL_MEMBERSHIP_TYPE
            .and(IS_FUNNEL_SOURCE)
            .and(IS_DURATION_OF_A_FREE_TRIAL)
            .and(IS_AUTO_RENEWAL_DISABLED);


    /**
     * @deprecated Use {@link MembershipValidationServiceBean#applyMembership(ApplyDiscountParameters)}
     */
    @Deprecated
    @Override
    public Boolean applyMembership(MemberOnPassengerListParameter memberOnPassengerListParameter) throws DataAccessException {
        Long userId = memberOnPassengerListParameter.getUserId();
        List<MemberAccount> memberList = getMemberManager().getMembersWithActivatedMembershipsByUserId(userId, dataSource);
        return applyMembership(memberList, memberOnPassengerListParameter.getSite(), memberOnPassengerListParameter.getTravellerList());
    }

    @Override
    public ApplyDiscountResult applyMembership(ApplyDiscountParameters applyDiscountParameters) throws DataAccessException {


        Optional<Long> userId = getUserId(applyDiscountParameters.getUser());
        if (userId.isEmpty()) {
            return ApplyDiscountResult.notApplicable(NotApplicationReason.USER_UNKNOWN);
        }

        List<Membership> memberships = getMembershipsActivated(userId.get());
        ApplyDiscountChecker checker = ApplyDiscountChecker.of(applyDiscountParameters).withMemberships(memberships);

        return checker.testAndGetResult();
    }

    private List<Membership> getMembershipsActivated(Long userId) throws DataAccessException {

        MembershipSearch membershipSearch = MembershipSearch.builder()
                .status(ACTIVATED)
                .withMemberAccount(true)
                .memberAccountSearch(MemberAccountSearch.builder()
                        .userId(userId)
                        .build())
                .build();
        return getSearchService().searchMemberships(membershipSearch);
    }


    @Override
    public Boolean isMembershipActiveOnWebsite(String siteId) {
        List<String> activeSites = getMembershipSubscriptionConfiguration().getActiveSitesList();
        LOGGER.info("Prime markets are {}", activeSites);
        return activeSites.contains(siteId);
    }

    @Override
    public Boolean isMembershipToBeRenewed(Long membershipId) throws MissingElementException, DataAccessException {
        LOGGER.info("Membership to be renewed is {}", membershipId);
        Membership membership = getMemberManager().getMembershipById(dataSource, membershipId);
        return membership.getIsActive() && MemberServiceUtils.isExpirationDateInPast(membership);
    }

    @Override
    public boolean isEligibleForFreeTrial(FreeTrialCandidate freeTrialCandidate) throws DataAccessException {
        String requestWebsite = freeTrialCandidate.getWebsite();
        boolean freeTrialMembershipPermitted = false;
        if (isMembershipActiveOnWebsite(requestWebsite)) {
            UserInfo userInfo = getUserApiManager().getUserInfo(freeTrialCandidate.getEmail(), requestWebsite);
            freeTrialMembershipPermitted = hasNoPreviousMembershipsPreventingFreeTrial(userInfo);
        }
        MetricsUtils.incrementCounter(MetricsBuilder.buildFreeTrialEligibleByWebsite(freeTrialMembershipPermitted, requestWebsite), MetricsNames.METRICS_REGISTRY_NAME);
        return freeTrialMembershipPermitted;
    }


    private boolean hasNoPreviousMembershipsPreventingFreeTrial(UserInfo userInfo) throws DataAccessException {
        Optional<Long> optUserId = userInfo.getUserId();
        return optUserId.isPresent() ? isFreeTrialAbuser(optUserId.get()) : Boolean.TRUE;
    }

    private boolean isFreeTrialAbuser(Long userId) throws DataAccessException {
        MemberAccountSearch memberAccountSearch = new MemberAccountSearch.Builder().userId(userId).withMembership(true).build();
        return getSearchService().searchMemberAccounts(memberAccountSearch).stream()
                .flatMap(memberAccount -> memberAccount.getMemberships().stream())
                .noneMatch(PROHIBITS_FREE_TRIAL);
    }


    private boolean applyMembership(List<MemberAccount> memberList, String site, List<TravellerParameter> travellerList) throws DataAccessException {
        Optional<Membership> membership = MemberServiceUtils.getMemberOnList(memberList, site, travellerList);
        return membership.isPresent() && !getBookingTrackingService().isBookingLimitReached(membership.get().getId());
    }

    private Optional<Long> getUserId(UserIdentification userIdentification) {
        Long userId = userIdentification.getUserId();
        return userId != null ? Optional.of(userId) : getUserId(userIdentification.getUserBrandedEmail());
    }

    private Optional<Long> getUserId(BrandedEmail brandedEmail) {
        UserInfo userInfo = getUserApiManager().getUserInfo(brandedEmail.getEmail(), brandedEmail.getBrand());
        return userInfo.getUserId();
    }


}
