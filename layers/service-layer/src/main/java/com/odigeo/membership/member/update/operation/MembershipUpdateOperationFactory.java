package com.odigeo.membership.member.update.operation;

import javax.sql.DataSource;

public interface MembershipUpdateOperationFactory {
    MembershipUpdateOperation createOperation(DataSource dataSource);
}
