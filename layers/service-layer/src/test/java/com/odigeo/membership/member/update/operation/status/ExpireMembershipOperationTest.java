package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static com.odigeo.membership.MemberStatus.ACTIVATED;
import static com.odigeo.membership.MemberStatus.DISCARDED;
import static com.odigeo.membership.MemberStatus.EXPIRED;
import static java.lang.Boolean.TRUE;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ExpireMembershipOperationTest extends MembershipStatusChangeOperationTest{
    private ExpireMembershipOperation expireMembershipOperation;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        expireMembershipOperation = new ExpireMembershipOperation(dataSource, memberService, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
    }

    @Test
    public void testUpdate() throws MissingElementException, DataAccessException, ExistingRecurringException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId()))
                .thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(EXPIRED), eq(membership.getAutoRenewal())))
                .thenReturn(TRUE);
        assertTrue(expireMembershipOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verifySuccessfulExpired(membership);
    }

    @Test
    public void testUpdateOnlyAllowed() throws MissingElementException, DataAccessException, ExistingRecurringException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setStatus(DISCARDED)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId()))
                .thenReturn(membership);
        assertFalse(expireMembershipOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verifyZeroInteractions(membershipMessageSendingManager, memberStatusActionStore, membershipStore);
    }

    private void verifySuccessfulExpired(Membership membership) throws SQLException {
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopicByRule(eq(ACTIVATED), eq(EXPIRED), eq(membership));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), eq(membership.getId()), eq(StatusAction.EXPIRATION));
    }
}