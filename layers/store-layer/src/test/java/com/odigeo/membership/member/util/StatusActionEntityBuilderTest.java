package com.odigeo.membership.member.util;

import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.StatusAction;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class StatusActionEntityBuilderTest {

    private static final String WRONG_ACTION = "WRONG_ACTION";
    private static final Date utilDate = new Date();
    private static final java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
    private static final Long MEMBER_ID = 11L;
    private static final Long ID = 9L;
    private static final StatusAction ACTION_TYPE = StatusAction.ACTIVATION;

    @Mock
    private ResultSet resultSet;

    private StatusActionEntityBuilder statusActionEntityBuilder;

    @BeforeMethod
    public void before() {
        openMocks(this);
        statusActionEntityBuilder = new StatusActionEntityBuilder();
    }

    @Test
    public void testBuild() throws SQLException {
        mockResultSet();
        final MemberStatusAction memberStatusAction = statusActionEntityBuilder.build(resultSet);
        assertEquals(memberStatusAction.getTimestamp(), sqlDate);
        assertEquals(memberStatusAction.getAction(), ACTION_TYPE);
        assertEquals(memberStatusAction.getMemberId(), MEMBER_ID);
        assertEquals(memberStatusAction.getId(), ID);
    }

    @Test(dataProvider = "testGetActionDate")
    public void testGetActionDate(java.sql.Date actionDate, Date expectedDate) {
        Date date = StatusActionEntityBuilder.getActionDate(actionDate);
        assertEquals(date, expectedDate);
    }

    @DataProvider(name = "testGetActionDate")
    public static Object[][] getActionDateValues() {
        return new Object[][] {{null, null}, {sqlDate, utilDate}};
    }

    @Test(dataProvider = "testGetStatusActionFromString")
    public void testGetStatusActionFromString(String actionType, StatusAction expectedStatusAction) {
        StatusAction statusAction = StatusActionEntityBuilder.getStatusActionFromString(actionType);
        assertEquals(statusAction, expectedStatusAction);
    }

    @DataProvider(name = "testGetStatusActionFromString")
    public static Object[][] getStatusActionFromString() {
        return new Object[][] {{StatusAction.ACTIVATION.toString(), StatusAction.ACTIVATION}, {WRONG_ACTION, StatusAction.UNKNOWN}, {null, StatusAction.UNKNOWN}};
    }

    private void mockResultSet() throws SQLException {
        when(resultSet.getDate("ACTION_DATE")).thenReturn(sqlDate);
        when(resultSet.getString("ACTION_TYPE")).thenReturn(ACTION_TYPE.toString());
        when(resultSet.getLong("MEMBER_STATUS_ACTION_ID")).thenReturn(ID);
        when(resultSet.getLong("ID")).thenReturn(MEMBER_ID);
    }
}