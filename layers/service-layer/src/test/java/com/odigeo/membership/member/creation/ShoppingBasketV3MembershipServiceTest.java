package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.member.MemberAccountStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.recurring.MembershipRecurringCreation;
import com.odigeo.membership.recurring.MembershipRecurringService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ShoppingBasketV3MembershipServiceTest {

    private static final long USER_ID = 123L;
    private static final long MEMBERSHIP_ID = 10L;
    private static final long MEMBER_ACCOUNT_ID = 1L;
    private static final UUID MEMBERSHIP_RECURRING_UUID = UUID.randomUUID();
    private static final String WEBSITE = "ES";
    private static final String NAME = "JOSE";
    private static final String LAST_NAME = "LUIS";

    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipNoSQLManager membershipNoSQLManager;
    @Mock
    private MembershipRecurringService membershipRecurringService;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberAccountStore memberAccountStore;

    private final MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder()
            .withWebsite(WEBSITE)
            .withMemberAccountCreationBuilder(new MemberAccountCreation.MemberAccountCreationBuilder()
                    .userId(USER_ID)
                    .name(NAME)
                    .lastNames(LAST_NAME))
            .withShoppingBasketProduct(true);

    private MembershipCreationServiceProvider membershipCreationFactory;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipCreationFactory = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class)
                .getInstance(membershipCreationBuilder.build());
    }

    private void configure(Binder binder) {
        binder.bind(MembershipNoSQLManager.class).toInstance(membershipNoSQLManager);
        binder.bind(MembershipRecurringService.class).toInstance(membershipRecurringService);
        binder.bind(MembershipStore.class).toInstance(membershipStore);
        binder.bind(MemberAccountStore.class).toInstance(memberAccountStore);
    }

    @Test
    public void checkRightFactoryReturnedTest() {
        assertTrue(membershipCreationFactory instanceof ShoppingBasketV3MembershipService);
    }

    @Test
    public void createMembershipTest() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.getNextMembershipId(dataSource)).thenReturn(MEMBERSHIP_ID);
        when(memberAccountStore.getNextMemberAccountId(dataSource)).thenReturn(MEMBER_ACCOUNT_ID);
        when(membershipRecurringService.createMembershipRecurringCollection(any(MembershipRecurringCreation.class)))
                .thenReturn(MEMBERSHIP_RECURRING_UUID);
        long newMembershipId = membershipCreationFactory.createMembership(dataSource, membershipCreationBuilder.build());
        verify(membershipStore).getNextMembershipId(dataSource);
        verify(memberAccountStore).getNextMemberAccountId(dataSource);
        verify(membershipRecurringService).createMembershipRecurringCollection(any(MembershipRecurringCreation.class));
        assertEquals(newMembershipId, MEMBERSHIP_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void createMembershipSQLException() throws MissingElementException, DataAccessException, SQLException {
        doThrow(new SQLException()).when(membershipStore)
                .getNextMembershipId(dataSource);
        membershipCreationFactory.createMembership(dataSource, membershipCreationBuilder.build());
    }
}