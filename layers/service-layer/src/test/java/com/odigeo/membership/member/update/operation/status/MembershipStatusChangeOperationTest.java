package com.odigeo.membership.member.update.operation.status;

import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.RemindMeLaterOperation;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.Mock;

import javax.sql.DataSource;

public abstract class MembershipStatusChangeOperationTest {
    @Mock
    protected DataSource dataSource;
    @Mock
    protected MemberStatusActionStore memberStatusActionStore;
    @Mock
    protected MembershipStore membershipStore;
    @Mock
    protected MembershipMessageSendingManager membershipMessageSendingManager;
    @Mock
    protected MemberService memberService;
    @Mock
    protected RemindMeLaterOperation remindMeLaterOperation;


}
