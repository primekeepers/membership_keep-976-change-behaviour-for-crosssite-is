package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.request.product.creation.CreateNewMembershipRequest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.fail;

public class MembershipCreationRequestValidatorTest {

    private static final int MONTHS_TO_RENEWAL = 12;
    private static final int DURATION_DAYS = 7;

    private MembershipCreationRequestValidator membershipCreationRequestValidator;

    @BeforeClass
    public void beforeClass() {
        membershipCreationRequestValidator = new MembershipCreationRequestValidator();
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testValidateRequestFieldsWhenDurationAndMonthsToRenewDoesNotExist() {
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder().build();
        membershipCreationRequestValidator.validateMembershipRequestParameters(request);
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testValidateRequestFieldsWhenMonthsToRenewIsInformedAndTimeUnitIsDays() {
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withMonthsToRenewal(MONTHS_TO_RENEWAL)
                .withDurationTimeUnit(TimeUnit.DAYS.name()).build();
        membershipCreationRequestValidator.validateMembershipRequestParameters(request);
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testValidateRequestFieldsWhenDurationAndMonthsToRenewDoesNotMatch() {
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withMonthsToRenewal(MONTHS_TO_RENEWAL)
                .withDuration(DURATION_DAYS)
                .withDurationTimeUnit(TimeUnit.MONTHS.name()).build();
        membershipCreationRequestValidator.validateMembershipRequestParameters(request);
    }

    @Test
    public void testValidateRequestFieldsWhenOnlyInformMonthsDuration() {
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withMonthsToRenewal(MONTHS_TO_RENEWAL).build();
        try{
            membershipCreationRequestValidator.validateMembershipRequestParameters(request);
        } catch (Exception e) {
            fail("Should not have thrown any exception, the request is valid");
        }
    }

}
