package com.odigeo.membership.member.memberapi.v1;

import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SearchMapper {
    SearchMapper INSTANCE = Mappers.getMapper(SearchMapper.class);


    @Mapping(target = "timeUnit", source = "durationTimeUnit")
    @Mapping(target = "memberAccountSearch.name", source = "memberAccountSearchRequest.firstName")
    @Mapping(target = "memberAccountSearch.lastNames", source = "memberAccountSearchRequest.lastName")
    @Mapping(target = "memberAccountSearch.withMembership", constant = "false")
    MembershipSearch toMembershipSearch(MembershipSearchRequest membershipSearchRequest);

    @Mapping(target = "name", source = "memberAccountSearchRequest.firstName")
    @Mapping(target = "lastNames", source = "memberAccountSearchRequest.lastName")
    @Mapping(target = "withMembership", expression = "java(isWithMemberships(memberAccountSearchRequest.isWithMemberships(), excludeMemberships))")
    MemberAccountSearch toMemberAccountSearch(MemberAccountSearchRequest memberAccountSearchRequest, boolean excludeMemberships);


    default boolean isWithMemberships(boolean isRequestWithMembership, boolean forceExclusions) {
        return !forceExclusions && isRequestWithMembership;
    }
}
