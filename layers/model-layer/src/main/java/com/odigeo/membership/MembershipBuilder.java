package com.odigeo.membership;

import com.odigeo.membership.MembershipPrices.MembershipPricesBuilder;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class MembershipBuilder {

    private Long id;
    private String website;
    private MemberStatus status;
    private MembershipRenewal membershipRenewal;
    private LocalDateTime activationDate;
    private LocalDateTime expirationDate;
    private long memberAccountId;
    private BigDecimal balance;
    private MembershipType membershipType;
    private int monthsDuration;
    private TimeUnit durationTimeUnit;
    private Integer duration;
    private Integer renewalDuration;
    private SourceType sourceType;
    private ProductStatus productStatus;
    private MembershipPricesBuilder membershipPricesBuilder;
    private String recurringId;
    private LocalDateTime timestamp;
    private List<MemberStatusAction> memberStatusActions;
    private List<MembershipRecurring> membershipRecurring;
    private UUID recurringCollectionId;
    private MemberAccount memberAccount;
    private Long feeContainerId;
    private Boolean remindMeLater;
    private LocalDateTime remindMeLaterLastUpdate;

    public Boolean getRemindMeLater() {
        return remindMeLater;
    }

    public MembershipBuilder setRemindMeLater(Boolean remindMeLater) {
        this.remindMeLater = remindMeLater;
        return this;
    }

    public LocalDateTime getRemindMeLaterLastUpdate() {
        return remindMeLaterLastUpdate;
    }

    public MembershipBuilder setRemindMeLaterLastUpdate(LocalDateTime remindMeLaterLastUpdate) {
        this.remindMeLaterLastUpdate = remindMeLaterLastUpdate;
        return this;
    }

    public ProductStatus getProductStatus() {
        return productStatus;
    }

    public Integer getRenewalDuration() {
        return renewalDuration;
    }

    public MembershipBuilder setRenewalDuration(Integer renewalDuration) {
        this.renewalDuration = renewalDuration;
        return this;
    }

    public MembershipBuilder setProductStatus(ProductStatus productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public Long getId() {
        return id;
    }

    public MembershipBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public MembershipBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public MembershipBuilder setStatus(MemberStatus status) {
        this.status = status;
        return this;
    }

    public MembershipRenewal getMembershipRenewal() {
        return membershipRenewal;
    }

    public MembershipBuilder setMembershipRenewal(MembershipRenewal membershipRenewal) {
        this.membershipRenewal = membershipRenewal;
        return this;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public MembershipBuilder setActivationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public MembershipBuilder setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipBuilder setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public BigDecimal getBalance() {
        return Optional.ofNullable(balance).orElse(BigDecimal.ZERO);
    }

    public MembershipBuilder setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public MembershipType getMembershipType() {
        return Optional.ofNullable(membershipType).orElse(MembershipType.BASIC);
    }

    public MembershipBuilder setMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public MembershipBuilder setMonthsDuration(int monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public MembershipBuilder setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public MembershipBuilder setRecurringId(String recurringId) {
        this.recurringId = recurringId;
        return this;
    }

    public UUID getRecurringCollectionId() {
        return recurringCollectionId;
    }

    public MembershipBuilder setRecurringCollectionId(UUID recurringCollectionId) {
        this.recurringCollectionId = recurringCollectionId;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public MembershipBuilder setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public List<MemberStatusAction> getMemberStatusActions() {
        return memberStatusActions;
    }

    public MembershipBuilder setMemberStatusActions(List<MemberStatusAction> memberStatusActions) {
        this.memberStatusActions = memberStatusActions;
        return this;
    }

    public List<MembershipRecurring> getMembershipRecurring() {
        return membershipRecurring;
    }

    public MembershipBuilder setMembershipRecurring(List<MembershipRecurring> membershipRecurring) {
        this.membershipRecurring = membershipRecurring;
        return this;
    }

    public MembershipPricesBuilder getMembershipPricesBuilder() {
        return Optional.ofNullable(membershipPricesBuilder).orElseGet(MembershipPrices::builder);
    }

    public MembershipBuilder setMembershipPricesBuilder(MembershipPricesBuilder membershipPricesBuilder) {
        this.membershipPricesBuilder = membershipPricesBuilder;
        return this;
    }

    public MemberAccount getMemberAccount() {
        return memberAccount;
    }

    public MembershipBuilder setMemberAccount(MemberAccount memberAccount) {
        this.memberAccount = memberAccount;
        return this;
    }

    public MembershipBuilder setFeeContainerId(Long feeContainerId) {
        this.feeContainerId = feeContainerId;
        return this;
    }

    public Long getFeeContainerId() {
        return feeContainerId;
    }

    public TimeUnit getDurationTimeUnit() {
        return Objects.isNull(durationTimeUnit) ? TimeUnit.MONTHS : durationTimeUnit;
    }

    public MembershipBuilder setDurationTimeUnit(TimeUnit durationTimeUnit) {
        this.durationTimeUnit = durationTimeUnit;
        return this;
    }

    public Integer getDuration() {
        return Objects.isNull(duration) ? Integer.valueOf(getMonthsDuration()) : duration;
    }

    public MembershipBuilder setDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public Membership build() {
        return new Membership(this);
    }


}
