package com.odigeo.membership.util;

import cucumber.deps.com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class ZonedDateTimeISOConverter extends AbstractSingleValueConverter {


    @Override
    public boolean canConvert(Class type) {
        return type.equals(OffsetDateTime.class);
    }

    @Override
    public OffsetDateTime fromString(String dtz) {
        if (dtz.length() > 10) {
            return OffsetDateTime.parse(dtz, DateTimeFormatter.ISO_DATE_TIME);
        } else {
            return LocalDate.parse(dtz, DateTimeFormatter.ISO_DATE).atStartOfDay(ZoneId.systemDefault()).toOffsetDateTime();
        }

    }
}
