package com.odigeo.membership;

import com.google.inject.Inject;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerRunner;
import com.odigeo.technology.docker.ContainerRunnerProvider;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.Network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public class CustomContainerComposer {

    private final ContainerRunnerProvider containerRunnerProvider;
    private final DockerClient dockerClient;

    private final Map<ContainerInfo, CustomContainerComposer.BootstrapInfo> containerInfoMap;
    private final List<ContainerInfo> containerInfosInRunningOrder;
    private final List<ContainerRunner> containerRunnerList;

    private String networkId;

    @Inject
    public CustomContainerComposer(DockerClient dockerClient, ContainerRunnerProvider containerRunnerProvider) {
        this.containerRunnerProvider = containerRunnerProvider;
        this.dockerClient = dockerClient;
        this.containerInfoMap = new HashMap<>();
        this.containerRunnerList = new ArrayList<>();
        this.containerInfosInRunningOrder = new ArrayList<>();
    }

    public CustomContainerComposer addServiceAndDependencies(ContainerInfo containerInfo, long maxRetries, long retrySleep) {
        for (ContainerInfo dependency : containerInfo.getDependencies()) {
            addServiceAndDependencies(dependency, maxRetries, retrySleep);
        }
        return addService(containerInfo, maxRetries, retrySleep);
    }

    public CustomContainerComposer addService(ContainerInfo containerInfo, long maxRetries, long retrySleep) {
        if (containerInfo.getName() == null) {
            throw new IllegalArgumentException("Cannot add service with name null");
        }
        containerInfoMap.put(containerInfo, new CustomContainerComposer.BootstrapInfo(maxRetries, retrySleep));
        return this;
    }

    public void composeUp(boolean removeContainers) throws ContainerException {
        try {
            defineRunningOrder();

            if (removeContainers) {
                removeExistingContainers();
            }

            networkId = getNetworkId();

            for (ContainerInfo containerInfo : containerInfosInRunningOrder) {
                containerInfo.setNetwork(networkId);
                ContainerRunner containerRunner = containerRunnerProvider.get();
                containerRunnerList.add(containerRunner);

                BootstrapInfo bootstrapInfo = containerInfoMap.get(containerInfo);
                containerRunner.start(containerInfo).waitBootstrap(bootstrapInfo.getMaxRetries(), bootstrapInfo.getRetrySleep());
            }
        } catch (InterruptedException | DockerException e) {
            throw new ContainerException(e);
        }
    }

    public void composeDown() throws ContainerException {
        ListIterator<ContainerRunner> li = containerRunnerList.listIterator(containerRunnerList.size());
        while (li.hasPrevious()) {
            li.previous().destroy();
        }
    }

    private String getNetworkId() throws DockerException, InterruptedException {
        List<Network> networksByName = dockerClient.listNetworks(DockerClient.ListNetworksParam.byNetworkName("network_"));
        return networksByName.isEmpty() ? "NO_NETWORK_FOUND" : networksByName.get(0).id();
    }

    private void defineRunningOrder() {
        boolean changes;
        Set<ContainerInfo> containerInfoSet = containerInfoMap.keySet();
        while (containerInfosInRunningOrder.size() < containerInfoSet.size()) {
            changes = false;
            for (ContainerInfo containerInfo : containerInfoSet) {
                if (notAlreadyAdded(containerInfo) && (hasNoDependencies(containerInfo) || dependenciesAlreadyInOrder(containerInfo))) {
                    containerInfosInRunningOrder.add(containerInfo);
                    changes = true;
                }
            }
            if (!changes) {
                throw new IllegalStateException("Invalid compose dependencies definition, posible loop of dependencies!");
            }
        }
    }

    private boolean notAlreadyAdded(ContainerInfo containerInfo) {
        return !containerInfosInRunningOrder.contains(containerInfo);
    }

    private boolean dependenciesAlreadyInOrder(ContainerInfo containerInfo) {
        return containerInfosInRunningOrder.containsAll(containerInfo.getDependencies());
    }

    private boolean hasNoDependencies(ContainerInfo containerInfo) {
        return containerInfo.getDependencies().isEmpty();
    }

    static class BootstrapInfo {
        private final long maxRetries;
        private final long retrySleep;

        BootstrapInfo(long maxRetries, long retrySleep) {
            this.maxRetries = maxRetries;
            this.retrySleep = retrySleep;
        }

        public long getMaxRetries() {
            return maxRetries;
        }

        public long getRetrySleep() {
            return retrySleep;
        }
    }

    private void removeExistingContainers() throws DockerException, InterruptedException {
        Set<String> names = extractNames(containerInfoMap.keySet());
        List<Container> containerList = dockerClient.listContainers(DockerClient.ListContainersParam.allContainers(true));
        for (Container container : containerList) {
            for (String conflictName : names) {
                if (container.names().contains(conflictName)) {
                    if ("running".equals(container.state())) {
                        dockerClient.killContainer(container.id());
                    }
                    dockerClient.removeContainer(container.id());
                }
            }
        }
    }

    private Set<String> extractNames(Set<ContainerInfo> containerInfoSet) {
        Set<String> result = new HashSet<>();
        for (ContainerInfo containerInfo : containerInfoSet) {
            result.add("/" + containerInfo.getName());
        }
        return result;
    }
}
