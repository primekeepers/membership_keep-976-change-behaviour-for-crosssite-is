package com.odigeo.membership.product;

import bean.test.BeanTest;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.util.Money;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;

public class MembershipProductTest extends BeanTest<MembershipProduct> {

    private static final String MEMBERSHIP_ID = "8";
    private static final Money MONEY = new Money(BigDecimal.TEN, Currency.getInstance("USD"));
    private static final String SUB_CODE = "AE10";
    private static final AbstractFee FEE = new FixFee(1L, FeeLabel.MARKUP_TAX, SUB_CODE,
        Currency.getInstance("USD"), null, BigDecimal.TEN, new Date());

    @Override
    protected MembershipProduct getBean() {
        return MembershipProduct.builder().membershipId(MEMBERSHIP_ID)
            .type(MembershipProductType.MEMBERSHIP).money(MONEY)
            .fees(Collections.singletonList(FEE)).build();
    }
}