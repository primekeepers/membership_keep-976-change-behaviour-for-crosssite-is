package com.odigeo.membership.member.bootstrap;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;
import com.odigeo.userprofiles.api.v2.UserApiService;

public class UserApiServiceModule extends AbstractRestUtilsModule<UserApiService> {

    public UserApiServiceModule(ServiceNotificator... serviceNotificators) {
        super(UserApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<UserApiService> getServiceConfiguration(Class userApiService) {
        return ServiceConfigurationBuilder.setUpDefault(UserApiService.class);
    }
}
