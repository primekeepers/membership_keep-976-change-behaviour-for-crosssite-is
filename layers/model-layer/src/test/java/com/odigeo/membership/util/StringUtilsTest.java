package com.odigeo.membership.util;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.ParseException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class StringUtilsTest {
    private StringUtils stringUtils;

    @BeforeMethod
    public void openMocks() throws ParseException {
        ConfigurationEngine.init();
        stringUtils = ConfigurationEngine.getInstance(StringUtils.class);
    }

    @Test
    public void testNormalizeString() {
        //Given
        String text = "Some text    with   spaces";
        String textNormalized = "SOME TEXT WITH SPACES";
        //Then
        Assert.assertEquals(stringUtils.normalizeString(text), textNormalized);
    }


    @Test
    public void testNormalizeStringWhenNull() {
        //Given
        String text = null;
        //Then
        Assert.assertNull(stringUtils.normalizeString(text));
    }

    @Test
    public void testConcatenateString() {
        //Given
        String firstPart = "first part, ";
        String secondPart = "second part";
        //When
        String wholeSentence = stringUtils.concatenateString(firstPart, secondPart);
        //Then
        Assert.assertEquals(wholeSentence, "first part, second part");
    }

    @Test
    public void testConcatenateStringFirstNull() {
        //Given
        String firstPart = null;
        String secondPart = "second part";
        //When
        String wholeSentence = stringUtils.concatenateString(firstPart, secondPart);
        //Then
        Assert.assertEquals(wholeSentence, "second part");
    }

    @Test
    public void testConcatenateStringSecondNull() {
        //Given
        String firstPart = "first part, ";
        String secondPart = null;
        //When
        String wholeSentence = stringUtils.concatenateString(firstPart, secondPart);
        //Then
        Assert.assertEquals(wholeSentence, "first part, ");
    }

    @Test
    public void testNormalizeAndRemoveNonAlphaFrom() {
        String someString = "some Par98t of the sTRIng";
        String nomalizedString = stringUtils.normalizeAndRemoveNonAlphaFrom(someString);
        Assert.assertEquals(nomalizedString, "SOMEPAR98TOFTHESTRING");
    }

    @Test
    public void testReplaceNewLineOrTabByWhiteSpace() {
        //Given
        String text = "Some text with \t tab \n and new line.";
        //When
        String replacedText = stringUtils.replaceNewLineOrTabByWhiteSpace(text);
        //Then
        assertEquals(replacedText, "Some text with   tab   and new line.");
    }

    @Test
    public void testRemoveUTF8BomHeader() {
        //Given
        String text = "\ufeffSome text with bom.";
        //When
        String replacedText = stringUtils.removeUTF8BomHeader(text);
        //Then
        assertEquals(replacedText, "Some text with bom.");
    }

    @Test
    public void testRemoveUTF8BomHeaderWithNullValueReturnsNull() {
        assertNull(stringUtils.removeUTF8BomHeader(null));
    }
}
