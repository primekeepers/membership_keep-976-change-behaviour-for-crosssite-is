package com.odigeo.userapi;

import com.odigeo.userprofiles.api.v2.model.Brand;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Optional;

import static com.odigeo.userprofiles.api.v2.model.Brand.ED;
import static com.odigeo.userprofiles.api.v2.model.Brand.GV;
import static com.odigeo.userprofiles.api.v2.model.Brand.OP;
import static com.odigeo.userprofiles.api.v2.model.Brand.TL;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BrandMapperTest {

    @Test
    public void testEmptyWhenNullWebsite() {
        //When
        Optional<Brand> brand = BrandMapper.map(null);
        //Then
        assertFalse(brand.isPresent());
    }

    @Test
    public void testEmptyWhenUnknownWebsite() {
        //When
        Optional<Brand> brand = BrandMapper.map("JLER");
        //Then
        assertFalse(brand.isPresent());
    }

    @Test(dataProvider = "wrongWebsites")
    public void testEmptyWhenWrongWebsites(String website) {
        //When
        Optional<Brand> brand = BrandMapper.map(website);
        //Then
        assertFalse(brand.isPresent());
    }

    @Test(dataProvider = "validWebsitesForBrands")
    public void testValidWebsites(String website, Brand expectedBrand) {
        //When
        Optional<Brand> brand = BrandMapper.map(website);
        //Then
        assertTrue(brand.isPresent());
        assertEquals(brand.get(), expectedBrand);
    }

    @DataProvider(name = "wrongWebsites")
    public static Object[][] wrongWebsites() {
        return new Object[][] {
                {"a"}, {"abc"}, {"jj773"}, {"OPITq"}, {" opeed "}};
    }

    @DataProvider(name = "validWebsitesForBrands")
    public static Object[][] validWebsitesForBrands() {
        return new Object[][] {
                {"DE", ED},
                {"eS", ED},
                {"EDES", ED},
                {"GOFR", GV},
                {"gOit", GV},
                {"OPIT", OP},
                {"OPes", OP},
                {" TLES ", TL},
                {"TLES", TL}};
    }
}