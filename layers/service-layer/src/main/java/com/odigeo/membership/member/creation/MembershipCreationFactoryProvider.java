package com.odigeo.membership.member.creation;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.member.creation.sql.BasicFreeMembershipService;
import com.odigeo.membership.member.creation.sql.EmployeeMembershipService;
import com.odigeo.membership.member.creation.sql.NewMembershipSubscriptionService;
import com.odigeo.membership.member.creation.sql.PendingToCollectMembershipService;
import com.odigeo.membership.parameters.MembershipCreation;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.function.Function;

@Singleton
public class MembershipCreationFactoryProvider {
    private static final String SEPARATOR = "-";
    private static final String SHOPPING_BASKET = "shoppingBasket";
    private static final String PENDING_TO_COLLECT_TYPE_AND_STATUS = MembershipType.BASIC + SEPARATOR + MemberStatus.PENDING_TO_COLLECT;
    private static final String PENDING_TO_COLLECT_BUSINESS_TYPE_AND_STATUS = MembershipType.BUSINESS + SEPARATOR + MemberStatus.PENDING_TO_COLLECT;
    private static final String PENDING_TO_COLLECT_PLUS_TYPE_AND_STATUS = MembershipType.PLUS + SEPARATOR + MemberStatus.PENDING_TO_COLLECT;
    private static final String BASIC_FREE_TYPE_AND_STATUS = MembershipType.BASIC_FREE + SEPARATOR + MemberStatus.ACTIVATED;
    private static final String EMPLOYEE_TYPE_AND_STATUS = MembershipType.EMPLOYEE + SEPARATOR + MemberStatus.ACTIVATED;

    private static final Function<MembershipCreation, String> TYPE_AND_STATUS = membershipCreation -> membershipCreation.getMembershipType() + SEPARATOR + membershipCreation.getMemberStatus();
    private static final Function<MembershipCreation, String> SHOPPING_TECHNOLOGY = membershipCreation -> membershipCreation.isShoppingBasketProduct() ? SHOPPING_BASKET : StringUtils.EMPTY;

    private static final Map<String, Class<? extends MembershipCreationServiceProvider>> CREATION_INSTANCE = new ImmutableMap.Builder<String, Class<? extends MembershipCreationServiceProvider>>()
            .put(PENDING_TO_COLLECT_TYPE_AND_STATUS, PendingToCollectMembershipService.class)
            .put(PENDING_TO_COLLECT_BUSINESS_TYPE_AND_STATUS, PendingToCollectMembershipService.class)
            .put(PENDING_TO_COLLECT_PLUS_TYPE_AND_STATUS, PendingToCollectMembershipService.class)
            .put(BASIC_FREE_TYPE_AND_STATUS, BasicFreeMembershipService.class)
            .put(EMPLOYEE_TYPE_AND_STATUS, EmployeeMembershipService.class)
            .put(SHOPPING_BASKET, ShoppingBasketV3MembershipService.class)
            .build();

    public MembershipCreationServiceProvider getInstance(MembershipCreation membershipCreation) {
        return ConfigurationEngine.getInstance(CREATION_INSTANCE.getOrDefault(TYPE_AND_STATUS.apply(membershipCreation),
                CREATION_INSTANCE.getOrDefault(SHOPPING_TECHNOLOGY.apply(membershipCreation), NewMembershipSubscriptionService.class)));
    }

}
