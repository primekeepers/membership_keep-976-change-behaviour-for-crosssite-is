package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.membership.member.creation.sql.EmployeeMembershipService;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.search.SearchService;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

import static com.odigeo.membership.v4.messages.SubscriptionStatus.SUBSCRIBED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class EmployeeMembershipServiceCreationTest {

    private static final String WEBSITE = "ES";
    private static final long USER_ID = 123L;
    private static final String NAME = "jo";
    private static final String LAST_NAMES = "ko";
    private static final long MEMBERSHIP_ID = 111L;
    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private SearchService searchService;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private UpdateMembershipService updateMembershipService;

    private final MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder()
            .withWebsite(WEBSITE)
            .withMemberAccountCreationBuilder(new MemberAccountCreation.MemberAccountCreationBuilder()
                    .userId(USER_ID)
                    .name(NAME)
                    .lastNames(LAST_NAMES))
            .withMembershipType(MembershipType.EMPLOYEE)
            .withMemberStatus(MemberStatus.ACTIVATED)
            .withUserCreation(new UserCreation.Builder().withEmail("abc@eo.com").build());

    private MembershipCreationServiceProvider membershipCreationFactory;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipCreationFactory = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class)
                .getInstance(membershipCreationBuilder.build());
    }

    private void configure(Binder binder) {
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(SearchService.class).toInstance(searchService);
        binder.bind(UpdateMembershipService.class).toInstance(updateMembershipService);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManager);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
    }

    @Test
    public void checkRightFactoryReturnedTest() {
        assertTrue(membershipCreationFactory instanceof EmployeeMembershipService);
    }

    @Test
    public void employeeMembershipCreation() throws MissingElementException, DataAccessException, SQLException, ExistingRecurringException {
        MembershipCreation membershipCreation = membershipCreationBuilder.build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        long membership = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        verifyCreation(membershipCreation, membership);
        verify(updateMembershipService, never()).updateMembership(any(UpdateMembership.class));
    }

    @Test
    public void employeeMembershipCreationAlreadyExists() throws MissingElementException, DataAccessException, SQLException, ExistingRecurringException {
        MembershipCreation membershipCreationUserId = new MembershipCreation(membershipCreationBuilder.withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID)));
        when(memberManager.createMember(dataSource, membershipCreationUserId)).thenReturn(MEMBERSHIP_ID);
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenReturn(Collections.singletonList(new MembershipBuilder().build()));
        Long membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreationUserId);
        verifyCreation(membershipCreationUserId, membershipId);
        verify(updateMembershipService).updateMembership(any(UpdateMembership.class));
    }

    @Test(expectedExceptions = DataAccessRollbackException.class)
    public void testCreateMemberThrowsException() throws DataAccessException, MissingElementException {
        MembershipCreation membershipCreation = membershipCreationBuilder.build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenThrow(new DataAccessException(StringUtils.EMPTY));
        membershipCreationFactory.createMembership(dataSource, membershipCreation);
    }

    @Test
    public void testNotCreateMemberWhenThrowsException() throws SQLException {
        MembershipCreation membershipCreation = membershipCreationBuilder.build();
        Long membershipId = null;
        try {
            when(memberManager.createMember(dataSource, membershipCreation)).thenThrow(new DataAccessException(StringUtils.EMPTY));
            membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        } catch (MissingElementException | DataAccessException e) {
            assertNotNull(e);
        }
        assertNull(membershipId);
        verifyNoInteractions(membershipMessageSendingManager);
        verify(memberStatusActionStore, never()).createMemberStatusAction(eq(dataSource), eq(MEMBERSHIP_ID), eq(StatusAction.INTERNAL_CREATION));
    }

    private void verifyCreation(MembershipCreation membershipCreation, long membership) throws SQLException {
        assertEquals(membership, MEMBERSHIP_ID);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(MEMBERSHIP_ID));
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(eq(membershipCreation.getUserCreation().getEmail()), eq(membershipCreation), eq(SUBSCRIBED));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), eq(MEMBERSHIP_ID), eq(StatusAction.INTERNAL_CREATION));
        assertEquals(ChronoUnit.MONTHS.between(membershipCreation.getActivationDate(), membershipCreation.getExpirationDate()), 360);
    }
}

