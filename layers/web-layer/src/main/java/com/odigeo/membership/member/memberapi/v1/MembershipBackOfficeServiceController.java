package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.request.backoffice.WelcomeEmailRequest;
import com.odigeo.membership.v1.MembershipBackOfficeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MembershipBackOfficeServiceController extends AbstractController implements MembershipBackOfficeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipBackOfficeServiceController.class);

    @Override
    public Boolean updateMembershipMarketingInfo(Long membershipId, String email) {
        try {
            getBackOfficeService().updateMembershipMarketingInfo(membershipId);
            return true;
        } catch (MissingElementException | PublishMessageException e) {
            LogUtils.smallLog(LOGGER, e.getClass().getSimpleName() + " trying to send marketing message", e);
        } catch (DataAccessException e) {
            LogUtils.smallLog(LOGGER, "DataAccessException trying to send marketing message", e);
            throw MembershipExceptionMapper.map(e);
        }
        return false;
    }

    @Override
    public Boolean sendWelcomeEmail(Long membershipId, WelcomeEmailRequest welcomeEmailRequest) {
        boolean result = false;
        try {
            result = getBackOfficeService().sendWelcomeEmail(membershipId, welcomeEmailRequest.getBookingId());
        } catch (MissingElementException e) {
            LogUtils.smallLog(LOGGER, "MissingElementException at sending welcome email", e);
        } catch (DataAccessException e) {
            LogUtils.smallLog(LOGGER, "DataAccessException at sending welcome email", e);
            throw MembershipExceptionMapper.map(e);
        }
        return result;
    }

}
