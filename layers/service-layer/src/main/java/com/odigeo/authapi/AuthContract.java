package com.odigeo.authapi;

import com.odigeo.membership.auth.AuthenticationResponse;
import com.odigeo.membership.auth.PermissionResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public interface AuthContract {
    AuthenticationResponse login(final String userName, final String password) throws MembershipAuthServiceException;

    AuthenticationResponse logout(final String token) throws MembershipAuthServiceException;

    PermissionResponse getTokenPermissions(final String token) throws MembershipAuthServiceException;
}
