package com.odigeo.membership.member.checkoutapi.mapper;

import com.edreamsodigeo.checkout.checkoutapi.v1.model.Fee;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.Price;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.Product;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.product.MembershipProduct;
import com.odigeo.membership.util.Money;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Mapping(source = "membershipId", target = "id")
    @Mapping(target = "type", expression = "java(membershipProduct.getType().name())")
    @Mapping(target = "sellingPrice", expression = "java(map(membershipProduct.getMoney()))")
    @Mapping(target = "merchantPrice", expression = "java(map(membershipProduct.getMoney()))")
    @Mapping(target = "providerProducts", expression = "java(java.util.Collections.emptyList())")
    @Mapping(source = "rolledBackAllowed", target = "canBeRolledback")
    @Mapping(target = "productDependencies", expression = "java(java.util.Collections.emptyList())")
    Product map(MembershipProduct membershipProduct);

    Price map(Money money);

    default List<Fee> map(List<AbstractFee> fees) {
        return Objects.nonNull(fees) ? fees.stream().map(fee -> map((FixFee) fee))
            .collect(Collectors.toList()) : Collections.emptyList();
    }

    @Mapping(target = "price", expression = "java(new Price(fee.getAmount(), fee.getCurrency()))")
    @Mapping(target = "label", expression = "java(fee.getFeeLabel().name())")
    @Mapping(target = "subCode", expression = "java(fee.getSubCode())")
    Fee map(FixFee fee);
}
