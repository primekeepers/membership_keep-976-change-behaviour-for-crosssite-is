Feature: Test getFutureFlights service

  Background:
    Given the next tracked booking stored in db:
      | bookingId | memberId | bookingDateIso | avoidFeeAmount | costFeeAmount | perksFeeAmount |
      | 10        | 1        | 2018-01-01     | -18.5          | 0             | -10            |
      | 11        | 1        | 2018-01-01     | -18.5          | 0             | -10            |
      | 12        | 1        | 2018-01-01     | -18.5          | 0             | -10            |
      | 20        | 2        | 2018-01-01     | -18.5          | 0             | -10            |
      | 30        | 3        | 2018-01-01     | -18.5          | 0             | -10            |
      | 40        | 3        | 2018-01-01     | -18.5          | 0             | -10            |
    And booking-api has BookingDetails:
      | bookingId |
      | 10        |
      | 11        |
      | 12        |
      | 20        |
      | 30        |
      | 50        |
    And BookingDetails has BookingProducts:
      | bookingId | productCategoryBooking | departureDate | arrivalName  |
      | 10        | ITINERARY_BOOKING      | 2000-12-31    | destination1 |
      | 10        | ITINERARY_BOOKING      | 3000-11-30    | destination1 |
      | 10        | ITINERARY_BOOKING      | 3000-12-31    | destination1 |
      | 11        | ITINERARY_BOOKING      | 3000-12-30    | destination3 |
      | 12        | ITINERARY_BOOKING      | 3000-01-29    | destination4 |
      | 20        | ITINERARY_BOOKING      | 3000-01-28    | destination5 |
      | 30        | ITINERARY_BOOKING      | 2000-01-28    | destination5 |
    And BookingApi returns BookingDetails

  Scenario: Check FutureFlights
    When the client calls getFutureFlights to membership Api with membershipId 1
    Then the futureFlights contains only:
      | destination  | departureDate    |
      | destination1 | 3000-11-30 |
      | destination1 | 3000-12-31 |
      | destination3 | 3000-12-30 |
      | destination4 | 3000-01-29 |

  Scenario Outline: Check no FutureFlights
    When the client calls getFutureFlights to membership Api with membershipId <membershipId>
    Then the futureFlights contains only:
      | destination | departureDate |
    Examples:
      | membershipId |
      | 0            |
      | 30           |