package com.odigeo.membership;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.odigeo.membership.util.JsonDateDeserializer;
import com.odigeo.membership.util.JsonDateSerializer;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberAccount implements Serializable {

    private final long id;
    private final long userId;
    private final String name;
    private final String lastNames;
    private List<Membership> memberships;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    private LocalDateTime timestamp;

    @JsonIgnore
    public MemberAccount(long id, long userId, String name, String lastNames) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.lastNames = lastNames;
        memberships = new ArrayList<>();
    }

    @JsonCreator
    private MemberAccount(@JsonProperty("id") long id,
                          @JsonProperty("userId") long userId,
                          @JsonProperty("name") String name,
                          @JsonProperty("lastNames") String lastNames,
                          @JsonProperty("memberships") List<Membership> memberships,
                          @JsonProperty("timestamp") LocalDateTime timestamp) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.lastNames = lastNames;
        this.memberships = memberships;
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public MemberAccount setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public MemberAccount setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MemberAccount) || getClass() != o.getClass()) {
            return false;
        }

        MemberAccount that = (MemberAccount) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).toHashCode();
    }
}
