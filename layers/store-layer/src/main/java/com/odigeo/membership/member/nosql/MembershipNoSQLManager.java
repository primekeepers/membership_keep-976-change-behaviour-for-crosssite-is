package com.odigeo.membership.member.nosql;

import com.odigeo.membership.Membership;

import java.util.Optional;

public interface MembershipNoSQLManager {

    Optional<Long> getBookingIdFromCache(long membershipId);

    Optional<Membership> get(String key);

    void store(String key, String value, int secondsToExpire);
}
