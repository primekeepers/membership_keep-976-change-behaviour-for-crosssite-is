package com.odigeo.membership.member.userarea;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.common.collect.Lists;
import com.odigeo.bookingapi.BookingApiDAO;
import com.odigeo.bookingapi.v14.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.BookingItinerary;
import com.odigeo.bookingapi.v14.responses.ItineraryBooking;
import com.odigeo.bookingapi.v14.responses.Location;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Flight;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberAccountBooking;
import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.member.nosql.MembershipNoSQLRepository;
import com.odigeo.membership.member.statusaction.MemberStatusActionService;
import com.odigeo.membership.tracking.BookingTrackingService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.odigeo.membership.StatusAction.ACTIVATION;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MemberUserAreaServiceBeanTest {

    private static final int SECONDS_TO_EXPIRE = 43200;
    private static final String ERROR = "ERROR";

    private static final Long MEMBERSHIP_ID = 1L;

    @Mock
    private MemberSubscriptionDetailsBean memberSubscriptionDetailsBeanMock;
    @Mock
    private MemberService memberServiceMock;
    @Mock
    private UserHasAnyMembershipForBrandPredicate userHasAnyMembershipForBrandPredicateMock;
    @Mock
    private MemberStatusActionService memberStatusActionService;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private MembershipNoSQLManager membershipNoSQLManager;
    @Mock
    private MembershipNoSQLRepository membershipNoSQLRepository;
    @Mock
    private BookingTrackingService bookingTrackingService;
    @Mock
    private BookingApiDAO bookingApiDAO;

    private MemberUserAreaServiceBean memberUserAreaServiceBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        memberUserAreaServiceBean = new MemberUserAreaServiceBean(memberSubscriptionDetailsBeanMock,
                memberServiceMock, userHasAnyMembershipForBrandPredicateMock,
                memberStatusActionService, memberAccountService, membershipNoSQLManager,
                membershipNoSQLRepository, bookingTrackingService, bookingApiDAO);
    }

    @Test
    public void testAllMemberSubscriptionDetailsIsEmptyWhenNoMembershipsFoundByMemberAccountId() throws Exception {
        //Given
        final long memberAccountId = 4532L;
        given(memberServiceMock.getMembershipsByAccountId(memberAccountId)).willReturn(emptyList());
        //When
        List<MemberSubscriptionDetails> allMemberSubscriptionDetails = memberUserAreaServiceBean.getAllMemberSubscriptionDetails(memberAccountId);
        //Then
        assertEquals(allMemberSubscriptionDetails.size(), 0);
    }

    @Test
    public void testAllMemberSubscriptionDetailsReturnsAllAccountMemberships() throws Exception {
        //Given
        final long memberAccountId = 545L;
        final long firstMembershipId = 1L;
        final long secondMembershipId = 2L;
        final Membership firstMembership = new MembershipBuilder().setId(firstMembershipId).build();
        final Membership secondMembership = new MembershipBuilder().setId(secondMembershipId).build();
        final MemberSubscriptionDetails firstExpectedMemberSubDetails = new MemberSubscriptionDetails().setMembershipId(firstMembershipId);
        final MemberSubscriptionDetails secondExpectedMemberSubDetails = new MemberSubscriptionDetails().setMembershipId(secondMembershipId);
        given(memberServiceMock.getMembershipsByAccountId(memberAccountId)).willReturn(asList(firstMembership, secondMembership));
        given(memberSubscriptionDetailsBeanMock.getMemberSubscriptionDetails(firstMembershipId)).willReturn(firstExpectedMemberSubDetails);
        given(memberSubscriptionDetailsBeanMock.getMemberSubscriptionDetails(secondMembershipId)).willReturn(secondExpectedMemberSubDetails);
        //When
        List<MemberSubscriptionDetails> allMemberSubscriptionDetails = memberUserAreaServiceBean.getAllMemberSubscriptionDetails(memberAccountId);
        //Then
        assertEquals(allMemberSubscriptionDetails.size(), 2);
        assertTrue(allMemberSubscriptionDetails.containsAll(asList(firstExpectedMemberSubDetails, secondExpectedMemberSubDetails)));
    }

    @Test
    public void testGetMemberSubscriptionDetailsReturnsFoundDetailsByMembershipId() {
        //Given
        final long membershipId = 43253L;
        final MemberSubscriptionDetails expectedMemberSubscriptionDetails = new MemberSubscriptionDetails().setMembershipId(membershipId);
        given(memberSubscriptionDetailsBeanMock.getMemberSubscriptionDetails(membershipId)).willReturn(expectedMemberSubscriptionDetails);
        //When
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(membershipId);
        //Then
        assertEquals(memberSubscriptionDetails, expectedMemberSubscriptionDetails);
    }

    @Test
    public void testBookingDetailSubscriptionReturnsNullWhenNoDetailFoundByGivenMemberId() throws Exception {
        //Given
        final long membershipId = 4534L;
        given(memberSubscriptionDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willReturn(Optional.empty());
        //When
        BookingDetail bookingDetailSubscription = memberUserAreaServiceBean.getBookingDetailSubscription(membershipId);
        //Then
        assertNull(bookingDetailSubscription);
    }

    @Test
    public void testBookingDetailSubscriptionReturnsDetailWhenDetailFoundByGivenMemberId() throws Exception {
        //Given
        final long membershipId = 322L;
        final BookingDetail expectedBookingDetail = new BookingDetail();
        given(memberSubscriptionDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willReturn(Optional.of(expectedBookingDetail));
        //When
        BookingDetail bookingDetailSubscription = memberUserAreaServiceBean.getBookingDetailSubscription(membershipId);
        //Then
        assertEquals(bookingDetailSubscription, expectedBookingDetail);
    }

    @Test
    public void testUserHasAnyMembershipForBrandFalse() throws Exception {
        //Given
        final long userId = 932L;
        final String brand = "OP";
        given(userHasAnyMembershipForBrandPredicateMock.testAnyMembershipForBrand(userId, brand)).willReturn(FALSE);
        //When
        boolean userHasAnyMembership = memberUserAreaServiceBean.userHasAnyMembershipForBrand(userId, brand);
        //Then
        assertFalse(userHasAnyMembership);
    }

    @Test
    public void testUserHasAnyMembershipForBrandTrue() throws Exception {
        //Given
        final long userId = 637L;
        final String brand = "ED";
        given(userHasAnyMembershipForBrandPredicateMock.testAnyMembershipForBrand(userId, brand)).willReturn(TRUE);
        //When
        boolean userHasAnyMembership = memberUserAreaServiceBean.userHasAnyMembershipForBrand(userId, brand);
        //Then
        assertTrue(userHasAnyMembership);
    }

    @Test
    public void testGetMembershipInfoWithBooking() throws Exception {
        //Given
        long membershipId = 123L;
        Date lastStatusModificationDate = new Date();
        final long bookingId = 723L;
        final MemberAccount memberAccount = new MemberAccount(1L, 1L, "", "");

        mockMemberStatusActionWithDate(membershipId, lastStatusModificationDate);
        mockMemberAccount(membershipId, memberAccount);
        mockBookingDetailSubscription(membershipId, bookingId);
        mockMemoryFetchCache(membershipId, bookingId, Boolean.FALSE);

        final MemberAccountBooking expectedMemberAccountBooking = new MemberAccountBooking(memberAccount, bookingId, lastStatusModificationDate);
        //When
        MemberAccountBooking memberAccountBooking = memberUserAreaServiceBean.getMembershipInfo(membershipId, false);
        //Then
        assertEqualsMemberAccountBooking(expectedMemberAccountBooking, memberAccountBooking);
    }

    @Test
    public void testGetMembershipInfoSkippingBooking() throws Exception {
        //Given
        final long membershipId = 832L;
        final MemberAccount memberAccount = new MemberAccount(1L, 1L, "", "");
        final Date lastStatusModificationDate = new Date();

        mockMemberStatusActionWithDate(membershipId, lastStatusModificationDate);
        mockMemberAccount(membershipId, memberAccount);

        final MemberAccountBooking expectedMemberAccountBooking = new MemberAccountBooking(memberAccount, null, lastStatusModificationDate);
        //When
        MemberAccountBooking memberAccountBooking = memberUserAreaServiceBean.getMembershipInfo(membershipId, true);
        //Then
        assertEqualsMemberAccountBooking(expectedMemberAccountBooking, memberAccountBooking);
    }

    @Test(expectedExceptions = MissingElementException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipInfoThrowsMissingElementException() throws MissingElementException, DataAccessException, BookingApiException {
        //Given
        final long membershipId = 832L;
        mockMemberStatusActionWithDate(membershipId, new Date());
        given(memberAccountService.getMemberAccountByMembershipId(membershipId)).willThrow(new MissingElementException("Expected exception"));
        //When
        memberUserAreaServiceBean.getMembershipInfo(membershipId, true);
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipInfoThrowsDataAccessException() throws MissingElementException, DataAccessException, BookingApiException {
        //Given
        final long membershipId = 832L;
        mockMemberStatusActionWithDate(membershipId, new Date());
        given(memberAccountService.getMemberAccountByMembershipId(membershipId)).willThrow(new DataAccessException("Expected exception"));
        //When
        memberUserAreaServiceBean.getMembershipInfo(membershipId, true);
    }

    @Test(expectedExceptions = BookingApiException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipInfoThrowsBookingApiException() throws MissingElementException, DataAccessException, BookingApiException {
        //Given
        final long membershipId = 832L;
        final MemberAccount memberAccount = new MemberAccount(1L, 1L, "", "");
        mockMemberStatusActionWithDate(membershipId, new Date());
        mockMemberAccount(membershipId, memberAccount);
        when(membershipNoSQLManager.getBookingIdFromCache(anyLong())).thenReturn(Optional.empty());
        given(memberSubscriptionDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willThrow(new BookingApiException("Expected exception"));
        //When
        memberUserAreaServiceBean.getMembershipInfo(membershipId, false);
    }

    private void mockBookingDetailSubscription(final Long membershipId, final long bookingId) throws BookingApiException {
        final BookingDetail bookingDetail = new BookingDetail();
        BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
        bookingBasicInfo.setId(bookingId);
        bookingDetail.setBookingBasicInfo(bookingBasicInfo);
        given(memberSubscriptionDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willReturn(Optional.of(bookingDetail));
    }

    private void mockMemberAccount(final long membershipId, final MemberAccount memberAccount) throws MissingElementException, DataAccessException {
        given(memberAccountService.getMemberAccountByMembershipId(membershipId)).willReturn(memberAccount);
    }

    private void mockMemberStatusActionWithDate(final Long membershipId, final Date expectedResult) {
        MemberStatusAction memberStatusAction = new MemberStatusAction(1L, membershipId, ACTIVATION, expectedResult);
        given(memberStatusActionService.lastStatusActionByMembershipId(membershipId)).willReturn(Optional.of(memberStatusAction));
    }

    private void assertEqualsMemberAccountBooking(final MemberAccountBooking expectedMemberAccountBooking, final MemberAccountBooking memberAccountBooking) {
        assertEquals(memberAccountBooking.getBookingDetailSubscriptionId(), expectedMemberAccountBooking.getBookingDetailSubscriptionId());
        assertEquals(memberAccountBooking.getLastStatusModificationDate(), expectedMemberAccountBooking.getLastStatusModificationDate());
        assertEquals(memberAccountBooking.getMemberAccount(), expectedMemberAccountBooking.getMemberAccount());
    }

    @Test
    public void testGetMembershipInfoWithBookingIdFromCache() throws Exception {
        //Given
        long membershipId = 91L;
        Date lastStatusModificationDate = new Date();
        final long bookingId = 1723L;
        final MemberAccount memberAccount = new MemberAccount(1L, 1L, "", "");

        mockMemberStatusActionWithDate(membershipId, lastStatusModificationDate);
        mockMemberAccount(membershipId, memberAccount);
        mockMemoryFetchCache(membershipId, bookingId, Boolean.TRUE);

        final MemberAccountBooking expectedMemberAccountBooking = new MemberAccountBooking(memberAccount, bookingId, lastStatusModificationDate);
        //When
        MemberAccountBooking memberAccountBooking = memberUserAreaServiceBean.getMembershipInfo(membershipId, false);
        //Then
        assertEqualsMemberAccountBooking(expectedMemberAccountBooking, memberAccountBooking);
    }

    private void mockMemoryFetchCache(final long membershipId, final long bookingId, final boolean isValueCached) {
        if (isValueCached) {
            when(membershipNoSQLManager.getBookingIdFromCache(membershipId)).thenReturn(Optional.of(bookingId));
        } else {
            when(membershipNoSQLManager.getBookingIdFromCache(membershipId)).thenReturn(Optional.empty());
        }
    }

    @Test
    public void testGetMembershipInfoWithBookingIdFromCacheWithFailureInCache() throws Exception {
        //Given
        long membershipId = 8L;
        Date lastStatusModificationDate = new Date();
        final long bookingId = 2020L;
        final MemberAccount memberAccount = new MemberAccount(1L, 1L, "", "");

        mockMemberStatusActionWithDate(membershipId, lastStatusModificationDate);
        mockMemberAccount(membershipId, memberAccount);
        mockMemoryFetchCache(membershipId, bookingId, Boolean.FALSE);
        mockBookingDetailSubscription(membershipId, bookingId);

        final MemberAccountBooking expectedMemberAccountBooking = new MemberAccountBooking(memberAccount, bookingId, lastStatusModificationDate);
        //When
        MemberAccountBooking memberAccountBooking = memberUserAreaServiceBean.getMembershipInfo(membershipId, false);
        //Then
        assertEqualsMemberAccountBooking(expectedMemberAccountBooking, memberAccountBooking);
    }

    @Test
    public void testGetMembershipInfoWithBookingIdFromCacheWithFailureInStoring() throws Exception {
        //Given
        long membershipId = 10L;
        Date lastStatusModificationDate = new Date();
        final long bookingId = 1991L;
        final MemberAccount memberAccount = new MemberAccount(1L, 1L, "", "");

        mockMemberStatusActionWithDate(membershipId, lastStatusModificationDate);
        mockMemberAccount(membershipId, memberAccount);
        mockMemoryFetchCache(membershipId, bookingId, Boolean.FALSE);
        mockMemoryStoreCache(membershipId, bookingId);
        mockBookingDetailSubscription(membershipId, bookingId);

        final MemberAccountBooking expectedMemberAccountBooking = new MemberAccountBooking(memberAccount, bookingId, lastStatusModificationDate);
        //When
        MemberAccountBooking memberAccountBooking = memberUserAreaServiceBean.getMembershipInfo(membershipId, false);
        //Then
        assertEqualsMemberAccountBooking(expectedMemberAccountBooking, memberAccountBooking);
    }

    private void mockMemoryStoreCache(final long membershipId, final long bookingId) throws Exception {
        doThrow(new DataAccessException(ERROR)).when(membershipNoSQLRepository).store(String.valueOf(membershipId), String.valueOf(bookingId), SECONDS_TO_EXPIRE);
    }

    @Test
    public void testGetFlights() throws DataAccessException, BookingApiException {
        //Given
        BookingTracking b = getBookingTracking();
        given(bookingTrackingService.getBookingTrackedByMembershipId(MEMBERSHIP_ID)).willReturn(Lists.newArrayList(b));
        BookingDetail bd = getBookingDetail();
        given(bookingApiDAO.getBooking(10L)).willReturn(bd);
        //When
        Collection<Flight> r = memberUserAreaServiceBean.getFlights(MEMBERSHIP_ID, LocalDateTime.now().minusMonths(1L));
        //Then
        assertNotNull(r);
        Flight r0 = r.iterator().next();
        assertNotNull(r0);
        assertNotNull(r0.getBookingId());
        assertNotNull(r0.getDeparture());
        assertNotNull(r0.getDestination());
    }

    private BookingDetail getBookingDetail() {
        BookingDetail bd = new BookingDetail();
        BookingBasicInfo bbi = new BookingBasicInfo();
        bbi.setId(10L);
        bd.setBookingBasicInfo(bbi);

        BookingItinerary bookItinerary = new BookingItinerary();
        Location arrival = new Location();
        arrival.setName("iMAnArrivalName");
        bookItinerary.setArrival(arrival);
        bookItinerary.setDepartureDate(Calendar.getInstance());
        ItineraryBooking bookingProducts = new ItineraryBooking();
        bookingProducts.setBookingItinerary(bookItinerary);
        bd.setBookingProducts(Collections.singletonList(bookingProducts));
        return bd;
    }

    @Test
    public void testGetFlightsEmpty() throws DataAccessException, BookingApiException {
        //Given
        BookingTracking b = getBookingTracking();
        given(bookingTrackingService.getBookingTrackedByMembershipId(MEMBERSHIP_ID)).willReturn(Lists.newArrayList(b));
        BookingDetail bd = getBookingDetail();
        given(bookingApiDAO.getBooking(10L)).willReturn(bd);
        //When
        Collection<Flight> r = memberUserAreaServiceBean.getFlights(0L, LocalDateTime.now().minusMonths(1L));
        //Then
        assertNotNull(r);
        assertTrue(r.isEmpty());
    }


    private BookingTracking getBookingTracking() {
        BookingTracking b = new BookingTracking();
        b.setMembershipId(MEMBERSHIP_ID);
        b.setBookingId(10L);
        return b;
    }


}
