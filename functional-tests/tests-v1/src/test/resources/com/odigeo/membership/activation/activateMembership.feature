Feature: Test activate membership authenticated

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
      | 456             | 1111   | JUAN      | PEREZ     |
    And the user-api has the following user info
      | userId | email                 | brand | status | token |
      | 4321   | basicFree@edreams.com | ED    | ACTIVE | null  |
      | 1234   | basicFree@edreams.com | ED    | ACTIVE | null  |
      | 1111   | basicFree@edreams.com | ED    | ACTIVE | null  |
    And the next membership stored in db:
      | memberId | website | status              | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 1        | ES      | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 2        | ES      | PENDING_TO_ACTIVATE | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3        | ES      | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |
      | 4        | ES      | EXPIRED             | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |


  Scenario Outline: Activate member only if status is PENDING_TO_ACTIVATE and doesn't have other activated memberships
    When requested to activate membership <membershipId> with bookingId 404 and set balance equals to 50 with authentication
    Then membership response has status <expectedStatus> and balance <expectedBalance>

    Examples:
      | membershipId | expectedStatus      | expectedBalance |
      | 2            | PENDING_TO_ACTIVATE | 0               |
      | 3            | ACTIVATED           | 50              |
      | 4            | EXPIRED             | 0               |


  Scenario: unauthorized
    When requested to activate membership without authentication unauthorized exception is thrown


  Scenario: No WelcomeToPrime trigger message sent when TRANSACTIONAL_EMAILS property is disabled.
    Given the configuration property TRANSACTIONAL_EMAILS stored in db with the value false

    When requested to activate membership 3 with bookingId 404 and set balance equals to 50 with authentication

    Then membership response has status ACTIVATED and balance 50
    And no message to trigger a WelcomeToPrime transactional email is sent


  Scenario: WelcomeToPrime trigger message sent when TRANSACTIONAL_EMAILS property is enabled.
    Given the configuration property TRANSACTIONAL_EMAILS stored in db with the value true

    When requested to activate membership 3 with bookingId 200 and set balance equals to 50 with authentication

    And 1 message to trigger a WelcomeToPrime transactional email is sent
    And a message to trigger a WelcomeToPrime transactional email is sent to kafka with these properties:
      | membershipId | 3    |
      | userId       | 1234 |
      | bookingId    | 200  |

  Scenario: ActivateMembership endpoint in Membership Service fails if required property is missing from ActivationRequest
    Given the configuration property TRANSACTIONAL_EMAILS stored in db with the value true

    When activateMembership endpoint of MembershipService is called with null bookingId in ActivationRequest an exception is thrown
    And activateMembership endpoint of MembershipService is called with null balance in ActivationRequest an exception is thrown
