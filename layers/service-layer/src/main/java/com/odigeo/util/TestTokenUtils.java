package com.odigeo.util;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.visitengineapi.v1.client.multitest.TestTokenMatcher;
import com.odigeo.visitengineapi.v1.client.multitest.TestTokenMatcherFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public final class TestTokenUtils {

    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_SHARE_FOR_ALL = factory -> factory.createTestTokenMatcherFromTestAssignment("OPR_OPRIME1465", 2);
    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_SHARE_ONLY_PLUS = factory -> factory.createTestTokenMatcherFromTestAssignment("OPR_OPRIME1465", 3);
    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_SHARE_NATIVE_FOR_ALL = factory -> factory.createTestTokenMatcherFromTestAssignment("PTS_AB1058", 2);
    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_SHARE_NATIVE_ONLY_PLUS = factory -> factory.createTestTokenMatcherFromTestAssignment("PTS_AB1058", 3);
    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_SHARE_META_BLOCKED = factory -> factory.createTestTokenMatcherFromTestAssignment("PRIME_OPRIME1650", 2);
    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_CROSS_SITE_DESKTOP = factory -> factory.createTestTokenMatcherFromTestAssignment("PRIME_KEEP974D", 2);
    public static final Function<TestTokenMatcherFactory, TestTokenMatcher> PRIME_CROSS_SITE_MOBILE = factory -> factory.createTestTokenMatcherFromTestAssignment("PRIME_KEEP974M", 2);
    private final Map<Function<TestTokenMatcherFactory, TestTokenMatcher>, TestTokenMatcher> testTokenMatchers;

    private TestTokenMatcherFactory testTokenMatcherFactory;
    private final Map<String, Integer> testDimensions;

    private TestTokenUtils(Map<String, Integer> testDimensions) {
        this.testTokenMatcherFactory = getTestTokenMatcherFactory();
        this.testDimensions = testDimensions;
        testTokenMatchers = new HashMap<>();
    }

    public static TestTokenUtils forTestDimensions(Map<String, Integer> testDimensions) {
        return new TestTokenUtils(testDimensions);
    }

    public boolean matches(Function<TestTokenMatcherFactory, TestTokenMatcher> generateTestTokenMatcherFunction) {
        return testTokenMatchers.computeIfAbsent(generateTestTokenMatcherFunction, generationFunction -> generationFunction.apply(testTokenMatcherFactory)).matches(testDimensions);
    }

    private TestTokenMatcherFactory getTestTokenMatcherFactory() {
        return Optional.ofNullable(testTokenMatcherFactory).orElseGet(() -> {
            testTokenMatcherFactory = ConfigurationEngine.getInstance(TestTokenMatcherFactory.class);
            return testTokenMatcherFactory;
        });
    }

}
