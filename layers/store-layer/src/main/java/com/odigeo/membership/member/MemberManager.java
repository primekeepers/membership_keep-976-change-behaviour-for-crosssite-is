package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.AuditMemberAccount;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@Singleton
public class MemberManager {

    private final MembershipStore membershipStore;
    private final MemberAccountStore memberAccountStore;
    private final AuditMemberAccountManager auditMemberAccountManager;
    private static final Logger LOGGER = LoggerFactory.getLogger(MemberManager.class);

    @Inject
    public MemberManager(MembershipStore membershipStore, MemberAccountStore memberAccountStore, final AuditMemberAccountManager auditMemberAccountManager) {
        this.membershipStore = membershipStore;
        this.memberAccountStore = memberAccountStore;
        this.auditMemberAccountManager = auditMemberAccountManager;
    }

    public Membership getMembershipById(DataSource dataSource, long memberId) throws MissingElementException, DataAccessException {
        try {
            return membershipStore.fetchMembershipById(dataSource, memberId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + memberId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + memberId, e);
        }
    }

    public long createMember(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        MemberAccountCreation memberAccountCreation = membershipCreation.getMemberAccountCreation();
        try {
            long memberAccountId = memberAccountStore.createMemberAccount(dataSource, memberAccountCreation.getUserId(),
                    memberAccountCreation.getName(), memberAccountCreation.getLastNames());
            membershipCreation.setMemberAccountId(memberAccountId);
            auditMemberAccountManager.auditNewMemberAccount(dataSource, auditableMembershipCreationData(membershipCreation));
            return membershipStore.createMember(dataSource, membershipCreation);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("There was an error trying to create memberSubscription: " + memberAccountCreation.getUserId(), e);
        }
    }

    public Boolean updateMemberAccountNames(DataSource dataSource, Long memberAccountId, String name, String lastNames) throws DataAccessException {
        try {
            Boolean successfullyUpdated = memberAccountStore.updateMemberAccountNames(dataSource, memberAccountId, name, lastNames);
            if (successfullyUpdated) {
                auditMemberAccountManager.auditUpdatedMemberAccount(dataSource, memberAccountId);
            }
            return successfullyUpdated;
        } catch (SQLException e) {
            throw new DataAccessRollbackException("There was an error trying to create memberSubscription: " + memberAccountId, e);
        }
    }

    public List<MemberAccount> getMembersWithActivatedMembershipsByUserId(long userId, DataSource dataSource) throws DataAccessException {
        try {
            List<MemberAccount> memberAccountAndMembershipsActivated = memberAccountStore.getMemberAccountAndMembershipsActivated(dataSource, userId);
            if (CollectionUtils.isEmpty(memberAccountAndMembershipsActivated)) {
                LOGGER.info("User id {} doesn't have active subscriptions", userId);
            }
            return memberAccountAndMembershipsActivated;
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with userId: " + userId, e);
        }
    }

    public MemberAccount getMemberAccountsByMemberAccountId(long memberAccountId, boolean withMemberships, DataSource dataSource) throws MissingElementException, DataAccessException {
        try {
            return withMemberships ? memberAccountStore.getMemberAccountWithMembershipById(dataSource, memberAccountId)
                    : memberAccountStore.getMemberAccountById(dataSource, memberAccountId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by accountId " + memberAccountId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with accountId: " + memberAccountId, e);
        }
    }

    private AuditMemberAccount auditableMembershipCreationData(final MembershipCreation membershipCreation) {
        MemberAccountCreation memberAccountCreation = membershipCreation.getMemberAccountCreation();
        return AuditMemberAccount.builder()
                .memberAccountId(membershipCreation.getMemberAccountId())
                .userId(memberAccountCreation.getUserId())
                .name(memberAccountCreation.getName())
                .lastName(memberAccountCreation.getLastNames())
                .build();
    }
}
