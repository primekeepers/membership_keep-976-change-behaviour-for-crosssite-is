package com.odigeo.membership.parameters.search;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PaginationDTOTest extends BeanTest<PaginationDTO> {

    @Override
    protected PaginationDTO getBean() {
        return PaginationDTO.builder()
                .limit(1)
                .offset(1)
                .build();
    }

    @Test
    public void testBuilderCopy() {
        PaginationDTO paginationDTO = getBean();
        assertEquals(PaginationDTO.builder(paginationDTO).build(), paginationDTO);
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(PaginationDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
