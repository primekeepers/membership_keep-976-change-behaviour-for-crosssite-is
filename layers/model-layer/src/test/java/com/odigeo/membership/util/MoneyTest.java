package com.odigeo.membership.util;

import bean.test.BeanTest;

import java.math.BigDecimal;
import java.util.Currency;

public class MoneyTest extends BeanTest<Money> {

    private static final String CURRENCY_CODE = "USD";

    @Override
    protected Money getBean() {
        return new Money(BigDecimal.ONE,
            Currency.getInstance(CURRENCY_CODE));
    }
}
