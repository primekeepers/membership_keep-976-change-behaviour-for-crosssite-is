package com.odigeo.util;

import com.odigeo.bookingapi.v14.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.BookingItinerary;
import com.odigeo.bookingapi.v14.responses.ItineraryBooking;
import com.odigeo.bookingapi.v14.responses.Location;
import com.odigeo.membership.Flight;

import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class FlightUtils {

    private static final Function<Calendar, ZonedDateTime> TO_DATE_TIME = calendar -> ZonedDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId());

    private FlightUtils() { }


    public static Collection<Flight> toFlights(BookingDetail bookingDetail) {
        Optional<BookingDetail> bookingDetailOptional = Optional.ofNullable(bookingDetail);
        if (bookingDetailOptional.isEmpty()) {
            return null;
        }

        Long bookingId = bookingDetailOptional.map(BookingDetail::getBookingBasicInfo).map(BookingBasicInfo::getId).orElse(null);

        return bookingDetailOptional.map(BookingDetail::getBookingProducts).orElse(Collections.emptyList())
                .stream().filter(ItineraryBooking.class::isInstance).map(ItineraryBooking.class::cast)
                .map(ItineraryBooking::getBookingItinerary)
                .map(toFlight(bookingId)).collect(Collectors.toSet());

    }

    private static Function<BookingItinerary, Flight> toFlight(Long bookingId) {
        return bookingItinerary -> {
            Optional<BookingItinerary> bi = Optional.of(bookingItinerary);
            ZonedDateTime departure = bi.map(BookingItinerary::getDepartureDate).map(TO_DATE_TIME).orElse(null);
            String destination = bi.map(BookingItinerary::getArrival).map(Location::getName).orElse(null);
            return new Flight(bookingId, departure, destination);
        };
    }
}
