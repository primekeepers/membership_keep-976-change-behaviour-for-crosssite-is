package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.mocks.visitengine.VisitEngineServiceWorld;
import com.odigeo.membership.mocks.visitengine.request.TestAssignmentRequestTest;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;

@ScenarioScoped
public class VisitEngineSteps {

    private final VisitEngineServiceWorld visitEngineServiceWorld;

    @Inject
    public VisitEngineSteps(VisitEngineServiceWorld visitEngineServiceWorld) {
        this.visitEngineServiceWorld = visitEngineServiceWorld;
    }

    @Given("^Visit Engine responds the following test assignment like:$")
    public void createTestAssignment(List<TestAssignmentRequestTest> testAssignmentRequestTests) {
        visitEngineServiceWorld.addTestAssignmentBuilders(testAssignmentRequestTests);
    }

}
