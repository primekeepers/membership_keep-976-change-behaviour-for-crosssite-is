package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.world.CrmDecryptionWorld;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@ScenarioScoped
public class CrmDecryptionSteps extends CommonSteps {
    private final CrmDecryptionWorld world;

    @Inject
    public CrmDecryptionSteps(ServerConfiguration serverConfiguration, CrmDecryptionWorld world) {
        super(serverConfiguration);
        this.world = world;
    }

    @When("^Request CRM token decryption with token (.*)$")
    public void requestCRMTokenDecryptionWithToken(String token) {
        try {
            world.setDecryptedToken(crmDecryptionApi.getCrmDecryption(token));
        } catch (MembershipInternalServerErrorException e) {
            world.setCrmException(e);
        }
    }

    @Then("^Result matches the expected string (.*)$")
    public void resultMatchesTheExpectedEmailAndName(String decryptedToken) {
        assertEquals(world.getDecryptedToken(), decryptedToken);
    }

    @Then("^Exception occured$")
    public void exceptionOccured() {
        assertNotNull(world.getCrmException());
    }
}
