package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.member.MemberAccountStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.recurring.MembershipRecurringCreation;
import com.odigeo.membership.recurring.MembershipRecurringService;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public class ShoppingBasketV3MembershipService implements MembershipCreationServiceProvider {

    private static final String MEMBERSHIP_SUBSCRIPTION = "MEMBERSHIP_SUBSCRIPTION";
    private static final int SHOPPING_BASKET_TTL_6_MONTHS_IN_SECONDS = 15811200;

    protected final MembershipNoSQLManager membershipNoSQLManager;
    private final MembershipRecurringService membershipRecurringService;

    //TODO OPRIME-1919 --> Do not use SEQ VAL and migrate to UUID
    private final MembershipStore membershipStore;
    private final MemberAccountStore memberAccountStore;

    @Inject
    public ShoppingBasketV3MembershipService(MembershipNoSQLManager membershipNoSQLManager,
                                             MembershipRecurringService membershipRecurringService,
                                             MembershipStore membershipStore,
                                             MemberAccountStore memberAccountStore) {
        this.membershipNoSQLManager = membershipNoSQLManager;
        this.membershipRecurringService = membershipRecurringService;
        this.membershipStore = membershipStore;
        this.memberAccountStore = memberAccountStore;
    }

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation)
            throws MissingElementException, DataAccessException {
        try {
            final long newMembershipId = membershipStore.getNextMembershipId(dataSource);
            final long newMemberAccountId = Optional.ofNullable(membershipCreation.getMemberAccountId()).orElse(memberAccountStore.getNextMemberAccountId(dataSource));
            final UUID membershipRecurringCollectionId = membershipRecurringService
                    .createMembershipRecurringCollection(MembershipRecurringCreation
                            .builder().membershipId(String.valueOf(newMembershipId)).type(MEMBERSHIP_SUBSCRIPTION)
                            .website(membershipCreation.getWebsite()).userId(membershipCreation
                                    .getMemberAccountCreation().getUserId().toString()).build());
            final Membership membership = buildMembership(newMembershipId, newMemberAccountId, membershipRecurringCollectionId, membershipCreation);
            membershipNoSQLManager.store(String.valueOf(newMembershipId),
                    membership.toString(), SHOPPING_BASKET_TTL_6_MONTHS_IN_SECONDS);
            return newMembershipId;
        } catch (SQLException e) {
            throw new DataAccessException("Error retrieving the next Membership SEQ VAL", e);
        }
    }

    private Membership buildMembership(long newMembershipId, long newMemberAccountId,
                                       UUID membershipRecurringCollectionId, MembershipCreation membershipCreation) {
        return new MembershipBuilder().setId(newMembershipId)
                .setWebsite(membershipCreation.getWebsite())
                .setMemberAccountId(newMemberAccountId)
                .setMemberAccount(new MemberAccount(newMemberAccountId,
                        membershipCreation.getMemberAccountCreation().getUserId(),
                        membershipCreation.getMemberAccountCreation().getName(),
                        membershipCreation.getMemberAccountCreation().getLastNames()))
                .setStatus(MemberStatus.PENDING_TO_ACTIVATE)
                .setMembershipRenewal(MembershipRenewal.ENABLED)
                .setExpirationDate(membershipCreation.getExpirationDate())
                .setBalance(membershipCreation.getBalance())
                .setProductStatus(ProductStatus.INIT)
                .setSourceType(membershipCreation.getSourceType())
                .setMembershipType(membershipCreation.getMembershipType())
                .setMonthsDuration(membershipCreation.getMonthsDuration())
                .setDuration(membershipCreation.getDuration())
                .setDurationTimeUnit(membershipCreation.getDurationTimeUnit())
                .setMembershipPricesBuilder(MembershipPrices.builder()
                        .totalPrice(membershipCreation.getSubscriptionPrice())
                        .renewalPrice(membershipCreation.getRenewalPrice())
                        .currencyCode(membershipCreation.getCurrencyCode()))
                .setRecurringCollectionId(membershipRecurringCollectionId)
                .setFeeContainerId(membershipCreation.getFeeContainerId()).build();
    }

}
