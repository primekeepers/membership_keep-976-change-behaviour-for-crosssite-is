package com.odigeo.membership.enums;

public enum Interface {

    ONE_FRONT_DESKTOP(false),
    ONE_FRONT_SMARTPHONE(false),
    NATIVE_ANDROID_EDREAMS(true),
    NATIVE_ANDROID_OPODO(true),
    NATIVE_ANDROID_GOV(true),
    NATIVE_ANDROID_TRAVELLINK(true),
    NATIVE_IOS_EDREAMS(true),
    NATIVE_IOS_OPODO(true),
    NATIVE_IOS_GOV(true),
    NATIVE_IOS_TRAVELLINK(true),
    UNKNOWN(false);

    private final boolean nativeInterface;

    Interface(boolean nativeInterface) {
        this.nativeInterface = nativeInterface;
    }

    public boolean isNative() {
        return nativeInterface;
    }
}
