package com.odigeo.membership.mocks.bookingapiservice;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class BookingApiServiceHttpServer extends JaxRsServiceHttpServer {

    private static final String BOOKING_API_PATH = "/engine";
    private static final int BOOKING_API_PORT = 58654;

    @Inject
    public BookingApiServiceHttpServer() {
        super(BOOKING_API_PATH, BOOKING_API_PORT);
    }
}
