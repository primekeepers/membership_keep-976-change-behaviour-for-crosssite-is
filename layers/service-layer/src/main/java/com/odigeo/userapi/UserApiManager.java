package com.odigeo.userapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.user.UserCloningMapper;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v2.UserApiServiceInternal;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.HashCode;
import com.odigeo.userprofiles.api.v2.model.HashType;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InternalServerException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidFindException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import com.odigeo.userprofiles.api.v2.utils.BasicAuth;
import org.jboss.resteasy.client.core.BaseClientResponse;
import org.jboss.resteasy.util.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Singleton
public class UserApiManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiManager.class);
    private final UserApiServiceInternal userApiServiceInternal;
    private final String basicAuth;

    @Inject
    public UserApiManager(UserApiServiceInternal userApiServiceInternal,
                          UserProfileApiSecurityConfiguration configuration) {
        this.userApiServiceInternal = userApiServiceInternal;
        basicAuth = new BasicAuth(configuration.getUser(), configuration.getPassword()).getAuthorizationHeader();
    }

    public Optional<String> getForgetPasswordToken(long userId) {
        try (Response response = userApiServiceInternal
            .getHashCodes(basicAuth, userId, HashType.REQUEST_PASSWORD)) {
            List<HashCode> hashCodes = hasContent(response)
                ? (List<HashCode>) ((BaseClientResponse) response).getEntity(new HashCodeListType()) : Collections.emptyList();
            return hashCodes
                    .stream()
                    .findFirst()
                    .map(HashCode::getCode);
        } catch (InvalidFindException | RemoteException | InvalidCredentialsException | InternalServerException e) {
            LOGGER.error("Error while retrieving REQUEST_PASSWORD HashCodes for userId: " + userId, e);
        }
        return Optional.empty();
    }

    private boolean hasContent(Response response) {
        return response.getStatus() != Response.Status.NO_CONTENT.getStatusCode();
    }

    static class HashCodeListType extends GenericType<List<HashCode>> {
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public UserInfo getUserInfo(long userId) {
        try (Response response = userApiServiceInternal
            .getUserBasicInfo(basicAuth, userId)) {
            UserBasicInfo userBasicInfo = hasContent(response) ? (UserBasicInfo) ((BaseClientResponse) response).getEntity(UserBasicInfo.class) : null;
            return Optional.ofNullable(userBasicInfo)
                    .map(UserInfo::fromUserBasicInfo)
                    .orElse(UserInfo.defaultUserInfo());
        } catch (RemoteException | InternalServerException | InvalidCredentialsException | InvalidFindException e) {
            LOGGER.error("Error while retrieving UserBasicInfo for userId: " + userId, e);
        } catch (Exception e) {
            LOGGER.error("Generic error while retrieving UserBasicInfo for userId: " + userId, e);
        }
        return UserInfo.defaultUserInfo();
    }

    public UserInfo getUserInfo(String email, String website) {
        Optional<Brand> brand = BrandMapper.map(website);
        if (brand.isPresent()) {
            return getUserInfo(email, brand.get());
        }
        LOGGER.warn("Error while retrieving brand from email: {}, website: {}", email, website);
        return UserInfo.defaultUserInfo();
    }

    private UserInfo getUserInfo(String email, Brand brand) {
        try (Response response = userApiServiceInternal
            .getUserBasicInfo(basicAuth, email, brand)) {
            UserBasicInfo userBasicInfo = hasContent(response)
                ? (UserBasicInfo) ((BaseClientResponse) response).getEntity(UserBasicInfo.class) : null;
            return Optional.ofNullable(userBasicInfo)
                .map(UserInfo::fromUserBasicInfo)
                .orElse(UserInfo.defaultUserInfo());
        } catch (InvalidFindException | RemoteException | InvalidCredentialsException | InternalServerException | InvalidValidationException e) {
            LOGGER.error("Error while retrieving UserBasicInfo for email: " + email + "and brand: " + brand, e);
        }
        return UserInfo.defaultUserInfo();
    }

    public boolean deletePendingMailConfirmationUserAccount(String email, Brand brand) {
        try (Response response = userApiServiceInternal
            .deletePendingUserAccount(basicAuth, email, brand)) {
            return (Boolean) ((BaseClientResponse) response).getEntity(Boolean.class);
        } catch (InvalidCredentialsException | InternalServerException e) {
            return false;
        }
    }

    public Optional<UserDTO> getUser(long userId) {
        try (Response response = userApiServiceInternal
            .getUserBasicInfo(basicAuth, userId)) {
            UserBasicInfo userBasicInfo = (UserBasicInfo) ((BaseClientResponse) response).getEntity(UserBasicInfo.class);
            return Optional.ofNullable(userBasicInfo)
                    .map(UserCloningMapper::mapToUserApiUser);
        } catch (RemoteException | InternalServerException | InvalidCredentialsException e) {
            LOGGER.error("Error while retrieving UserBasicInfo for userId: " + userId, e);
        } catch (InvalidFindException e) {
            LOGGER.warn("Could not find user with Id: " + userId, e);
        }
        return Optional.empty();
    }

    public String getEmail(long userId) throws DataNotFoundException {
        return Optional.ofNullable(getUserInfo(userId).getEmail())
                .orElseThrow(() -> new DataNotFoundException("User api returned email null for userId " + userId));
    }
}
