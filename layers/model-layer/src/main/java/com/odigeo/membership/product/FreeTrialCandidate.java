package com.odigeo.membership.product;

public class FreeTrialCandidate {

    private final String email;
    private final String website;
    private final String name;
    private final String lastNames;

    FreeTrialCandidate(Builder builder) {
        this.email = builder.email;
        this.website = builder.website;
        this.name = builder.name;
        this.lastNames = builder.lastNames;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public String getName() {
        return name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String email;
        private String website;
        private String name;
        private String lastNames;

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastName(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public FreeTrialCandidate build() {
            return new FreeTrialCandidate(this);
        }
    }

}
