package com.odigeo.membership.member.discount.rule;

import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public abstract class ApplyDiscountRulesTest {

    protected void checkContext(ApplyDiscountContext context, boolean result, NotApplicationReason reason) {
        if (result) {
            assertNotNull(context.getMembershipsEligible());
            assertFalse(context.getMembershipsEligible().isEmpty());
            assertTrue(context.getNotApplicationReason().isEmpty());
        } else {
            assertNotNull(context.getNotApplicationReason());
            assertEquals(context.getNotApplicationReason().get(0), reason);
        }
    }

}