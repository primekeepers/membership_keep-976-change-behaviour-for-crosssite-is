package com.odigeo.membership.member.memberapi.v1.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRecurring;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.product.FreeTrialCandidate;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.container.TravellerParametersContainer;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.MembershipInfo;
import ma.glasnost.orika.MapperFacade;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MemberServiceMappingTest {

    private static final String WEBSITE = "ES";
    private static final String CURRENCY_CODE = "EUR";
    private static final String NAME = "name";
    private static final String LAST_NAME = "lastName";
    private static final Long MEMBER_ACCOUNT_ID = 1L;
    private static final Long USER_ID = 123L;
    private static final Long MEMBERSHIP_ID = 1L;
    private static final BigDecimal TOTAL_PRICE = BigDecimal.valueOf(12.45);
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now().plusYears(1);
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final String TIMESTAMP_STR = "2016-06-16T16:16:16";
    private static final LocalDateTime TIMESTAMP = LocalDateTime.parse(TIMESTAMP_STR);
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME);
    private static final Membership MEMBERSHIP = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
        .setMembershipPricesBuilder(MembershipPrices.builder()
            .totalPrice(TOTAL_PRICE)
            .currencyCode(CURRENCY_CODE))
        .setTimestamp(TIMESTAMP)
        .setMembershipRenewal(MembershipRenewal.ENABLED).setExpirationDate(EXPIRATION_DATE).setMemberAccountId(MEMBER_ACCOUNT_ID).setSourceType(SOURCE_TYPE).setProductStatus(ProductStatus.CONTRACT).build();
    private static final String RECURRING_ID = "A123456";
    private static final MembershipRecurring MEMBERSHIP_RECURRING = MembershipRecurring.builder()
        .recurringId(RECURRING_ID)
        .build();
    private static final Membership MEMBERSHIP_WITH_RECURRING = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipPricesBuilder(MembershipPrices.builder()
                    .totalPrice(TOTAL_PRICE)
                    .currencyCode(CURRENCY_CODE))
            .setTimestamp(TIMESTAMP)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setExpirationDate(EXPIRATION_DATE).setMemberAccountId(MEMBER_ACCOUNT_ID).setSourceType(SOURCE_TYPE).setProductStatus(ProductStatus.CONTRACT).setMembershipRecurring(Collections.singletonList(MEMBERSHIP_RECURRING)).build();
    private static final UUID RECURRING_COLLECTION_UUID = UUID.fromString("bd50c770-5a1b-46be-93c1-6a9b1e6d62c7");
    private static final Membership MEMBERSHIP_WITH_RECURRING_COLLECTION_ID = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipPricesBuilder(MembershipPrices.builder()
                    .totalPrice(TOTAL_PRICE)
                    .currencyCode(CURRENCY_CODE))
            .setTimestamp(TIMESTAMP).setMembershipRenewal(MembershipRenewal.ENABLED).setExpirationDate(EXPIRATION_DATE)
            .setMemberAccountId(MEMBER_ACCOUNT_ID).setSourceType(SOURCE_TYPE).setProductStatus(ProductStatus.CONTRACT)
            .setRecurringCollectionId(RECURRING_COLLECTION_UUID).build();
    private static final Membership EXPIRED_MEMBERSHIP = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setExpirationDate(LocalDateTime.now()).setMemberAccountId(MEMBER_ACCOUNT_ID).setSourceType(SOURCE_TYPE).setProductStatus(ProductStatus.CONTRACT).build();
    private static final String EMAIL = "cloud@odigeo.com";

    @Mock
    private MapperFacade mapperFacadeMock;

    @Mock
    private FreeTrialCandidateRequest freeTrialCandidateRequest;

    private final MemberServiceMapping memberServiceMapping = new MemberServiceMapping();

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(MapperFacade.class).toProvider(() -> mapperFacadeMock);
            }
        });
    }

    @Test
    public void testAsMembershipInfoWithoutRecurringResponse() {
        setMemberAccountWithCustomMembership(MEMBERSHIP);
        MembershipInfo membershipInfo = ConfigurationEngine.getInstance(MemberServiceMapping.class).asMembershipInfoResponse(MEMBER_ACCOUNT);
        assertEquals(membershipInfo.getTotalPrice(), TOTAL_PRICE);
        assertEquals(membershipInfo.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(membershipInfo.getTimestamp(), TIMESTAMP_STR);
        assertNotNull(membershipInfo.getMembershipRecurring());
        assertEquals(membershipInfo.getMembershipRecurring().size(), 0);
    }

    @Test
    public void testAsMembershipInfoWithRecurringResponse() {
        setMemberAccountWithCustomMembership(MEMBERSHIP_WITH_RECURRING);
        MembershipInfo membershipInfo = ConfigurationEngine.getInstance(MemberServiceMapping.class).asMembershipInfoResponse(MEMBER_ACCOUNT);
        assertEquals(membershipInfo.getTotalPrice(), TOTAL_PRICE);
        assertEquals(membershipInfo.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(membershipInfo.getTimestamp(), TIMESTAMP_STR);
        assertNotNull(membershipInfo.getMembershipRecurring());
        List<MembershipRecurring> membershipRecurringExpected = MEMBERSHIP_WITH_RECURRING.getMembershipRecurring();
        List<com.odigeo.membership.response.MembershipRecurring> membershipRecurringActual = membershipInfo.getMembershipRecurring();
        assertEquals(membershipRecurringActual.size(), membershipRecurringExpected.size());
        IntStream.range(0, membershipRecurringExpected.size())
            .forEach(i -> testMembershipRecurring(membershipRecurringActual.get(i), membershipRecurringExpected.get(i)));

    }

    @Test
    public void testAsMembershipInfoWithRecurringCollectionIdResponse() {
        setMemberAccountWithCustomMembership(MEMBERSHIP_WITH_RECURRING_COLLECTION_ID);
        MembershipInfo membershipInfo = memberServiceMapping.asMembershipInfoResponse(MEMBER_ACCOUNT);
        assertEquals(membershipInfo.getTotalPrice(), TOTAL_PRICE);
        assertEquals(membershipInfo.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(membershipInfo.getTimestamp(), TIMESTAMP_STR);
        assertNotNull(membershipInfo.getMembershipRecurring());
        assertEquals(membershipInfo.getMembershipRecurring().size(), 0);
        assertEquals(membershipInfo.getRecurringCollectionId(), MEMBERSHIP_WITH_RECURRING_COLLECTION_ID.getRecurringCollectionId());
    }

    private void testMembershipRecurring(com.odigeo.membership.response.MembershipRecurring recurringInfoActual, MembershipRecurring recurringInfoExpected) {
        assertEquals(recurringInfoActual.getRecurringId(), recurringInfoExpected.getRecurringId());
        assertEquals(recurringInfoActual.getStatus(), recurringInfoExpected.getStatus());
    }

    @Test
    public void testAsMembershipWithoutExpirationDate() {
        setMemberAccountWithCustomMembership(MEMBERSHIP);
        assertConsistentMembership();
    }

    @Test
    public void testAsMembershipWithExpirationDate() {
        setMemberAccountWithCustomMembership(EXPIRED_MEMBERSHIP);
        assertConsistentMembership();
    }

    private void assertConsistentMembership() {
        final com.odigeo.membership.response.Membership membership = ConfigurationEngine.getInstance(MemberServiceMapping.class).asMembershipResponse(MEMBER_ACCOUNT);
        assertConsistentData(membership);
    }

    private void assertConsistentData(com.odigeo.membership.response.Membership membership) {
        assertEquals(membership.getAutoRenewalStatus(), MembershipRenewal.ENABLED.toString());
        assertEquals(membership.getName(), NAME);
        assertEquals(membership.getLastNames(), LAST_NAME);
        assertEquals(membership.getWebsite(), WEBSITE);
        assertEquals(membership.getSourceType(), SOURCE_TYPE.name());
        assertEquals(membership.getWarnings().size(), 1);
    }

    @Test
    public void testMapToBlackListedPaymentMethods() {
        List<BlackListedPaymentMethod> blackListedMethods = getBlackListedMethods();
        assertNotNull(ConfigurationEngine.getInstance(MemberServiceMapping.class).mapToBlackListedPaymentMethods(MEMBERSHIP_ID, blackListedMethods));
        assertEquals(ConfigurationEngine.getInstance(MemberServiceMapping.class).mapToBlackListedPaymentMethods(MEMBERSHIP_ID, blackListedMethods).size(), 1);
    }

    private List<BlackListedPaymentMethod> getBlackListedMethods() {
        List<BlackListedPaymentMethod> blackListedMethods = new ArrayList<>();
        BlackListedPaymentMethod blacklistedPaymentMethod = new BlackListedPaymentMethod();
        blacklistedPaymentMethod.setErrorMessage("Error Message");
        blacklistedPaymentMethod.setErrorType("1");
        blacklistedPaymentMethod.setId(1L);
        blacklistedPaymentMethod.setTimestamp(new Date());
        blackListedMethods.add(blacklistedPaymentMethod);
        return blackListedMethods;
    }

    @Test
    public void testAsPassengerList() {
        CheckMemberOnPassengerListRequest request = getCheckMemberOnPassengerListRequest();
        assertNotNull((ConfigurationEngine.getInstance(MemberServiceMapping.class).asPassengerList(request)));
        assertEquals((ConfigurationEngine.getInstance(MemberServiceMapping.class).asPassengerList(request).getClass()), MemberOnPassengerListParameter.class);
        assertEquals((ConfigurationEngine.getInstance(MemberServiceMapping.class).asPassengerList(request).getUserId()), USER_ID);
    }

    @Test
    public void testAsWebsiteMemberMap() {
        MEMBER_ACCOUNT.getMemberships().add(MEMBERSHIP);
        List<MemberAccount> memberAccountList = Collections.singletonList(MEMBER_ACCOUNT);
        assertNotNull((ConfigurationEngine.getInstance(MemberServiceMapping.class).asWebsiteMemberMap(memberAccountList)));
        assertEquals(ConfigurationEngine.getInstance(MemberServiceMapping.class).asWebsiteMemberMap(memberAccountList).get(WEBSITE).getMembershipId(), MEMBER_ACCOUNT.getId());
    }

    private CheckMemberOnPassengerListRequest getCheckMemberOnPassengerListRequest() {
        CheckMemberOnPassengerListRequest request = new CheckMemberOnPassengerListRequest();
        request.setSite(WEBSITE);
        request.setUserId(USER_ID);
        TravellerParametersContainer traveller = new TravellerParametersContainer();
        traveller.setLastNames(LAST_NAME);
        traveller.setName(NAME);
        List<TravellerParametersContainer> travellers = Collections.singletonList(traveller);
        request.setTravellerContainerList(travellers);
        return request;
    }

    private void setMemberAccountWithCustomMembership(final Membership membership) {
        final List<Membership> membershipList = new ArrayList<>();
        membership.setBookingLimitReached(Boolean.TRUE);
        membershipList.add(membership);
        MEMBER_ACCOUNT.setMemberships(membershipList);
    }

    @Test
    public void testMapFree() {
        when(freeTrialCandidateRequest.getEmail()).thenReturn(EMAIL);
        when(freeTrialCandidateRequest.getWebsite()).thenReturn(WEBSITE);
        when(freeTrialCandidateRequest.getName()).thenReturn(NAME);
        when(freeTrialCandidateRequest.getLastNames()).thenReturn(LAST_NAME);
        FreeTrialCandidate freeTrialCandidate = memberServiceMapping.mapFreeTrialCandidate(freeTrialCandidateRequest);
        assertEquals(freeTrialCandidate.getEmail(), EMAIL);
        assertEquals(freeTrialCandidate.getWebsite(), WEBSITE);
        assertEquals(freeTrialCandidate.getName(), NAME);
        assertEquals(freeTrialCandidate.getLastNames(), LAST_NAME);
    }
}
