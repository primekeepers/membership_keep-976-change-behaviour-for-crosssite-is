package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static com.odigeo.membership.MemberStatus.ACTIVATED;
import static com.odigeo.membership.MemberStatus.DEACTIVATED;
import static com.odigeo.membership.MemberStatus.PENDING_TO_ACTIVATE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class DeactivateMembershipOperationTest extends MembershipStatusChangeOperationTest {
    private DeactivateMembershipOperation deactivateMembershipOperation;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        deactivateMembershipOperation = new DeactivateMembershipOperation(dataSource, remindMeLaterOperation, memberService, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setRemindMeLater(FALSE)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED)))
                .thenReturn(TRUE);
        assertTrue(deactivateMembershipOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verify(membershipStore).updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED));
        verifySuccessfulDeactivation(membership);
        verifyZeroInteractions(remindMeLaterOperation);
    }

    @Test
    public void testUpdateWithRemindMeLater() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setRemindMeLater(TRUE)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED)))
                .thenReturn(TRUE);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.aDeactivateUpdateMembership();
        assertTrue(deactivateMembershipOperation.update(updateMembership));
        verify(membershipStore).updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED));
        verifySuccessfulDeactivation(membership);
        verify(remindMeLaterOperation).update(eq(UpdateMembership.builderCloneOf(updateMembership).remindMeLater(FALSE).build()));
    }

    @Test
    public void testUpdateStoreFails() throws ExistingRecurringException, DataAccessException, MissingElementException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setRemindMeLater(FALSE)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED)))
                .thenReturn(FALSE);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        assertFalse(deactivateMembershipOperation.update(updateMembership));
        verify(membershipStore).updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED));
        verifyZeroInteractions(membershipMessageSendingManager, remindMeLaterOperation, memberStatusActionStore);
    }

    @Test
    public void testUpdateNotActivatedMembership() throws ExistingRecurringException, DataAccessException, MissingElementException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setStatus(PENDING_TO_ACTIVATE)
                .setRemindMeLater(FALSE)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        assertFalse(deactivateMembershipOperation.update(updateMembership));
        verifyZeroInteractions(membershipStore, membershipMessageSendingManager, remindMeLaterOperation, memberStatusActionStore);
    }

    @Test(expectedExceptions = DataAccessRollbackException.class)
    public void testUpdateStatusThrowsException() throws DataAccessException, MissingElementException, ExistingRecurringException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setRemindMeLater(TRUE)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DEACTIVATED), eq(MembershipRenewal.DISABLED)))
                .thenReturn(TRUE);
        doThrow(new SQLException())
                .when(memberStatusActionStore)
                .createMemberStatusAction(eq(dataSource), eq(membership.getId()), eq(StatusAction.DEACTIVATION));
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        deactivateMembershipOperation.update(updateMembership);

    }

    private void verifySuccessfulDeactivation(Membership membership) throws SQLException {
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopicByRule(eq(ACTIVATED), eq(DEACTIVATED), eq(membership));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), eq(membership.getId()), eq(StatusAction.DEACTIVATION));
    }

}