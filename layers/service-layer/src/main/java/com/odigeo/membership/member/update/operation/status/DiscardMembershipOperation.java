package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;

public class DiscardMembershipOperation extends MembershipStatusChangeOperation {
    private final MemberService memberService;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        DiscardMembershipOperation createOperation(DataSource dataSource);
    }

    @Inject
    public DiscardMembershipOperation(@Assisted DataSource dataSource,
                                      MemberService memberService,
                                      MembershipMessageSendingManager membershipMessageSendingManager,
                                      MembershipStore membershipStore,
                                      MemberStatusActionStore memberStatusActionStore) {
        super(dataSource, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
        this.memberService = memberService;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        return MemberStatus.DISCARDED.name().equals(discardMembership(updateMembership));
    }

    public String discardMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        return changeStatusMembership(membership, MemberStatus.DISCARDED, StatusAction.DISCARD);
    }
}
