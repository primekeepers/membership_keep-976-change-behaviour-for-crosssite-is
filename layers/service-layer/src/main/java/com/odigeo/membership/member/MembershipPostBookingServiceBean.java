package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.PostBookingPageInfo;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.creation.MembershipCreationService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

@Stateless
@Local(MembershipPostBookingService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MembershipPostBookingServiceBean extends AbstractServiceBean implements MembershipPostBookingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipPostBookingServiceBean.class);
    private static final String BOOKING_ID_PATTERN = "bookingId=(.*)&";
    private static final String EMAIL_PATTERN = "email=(.*)";
    private static final int FIRST_PATTERN_MATCH = 1;
    private MembershipCreationService membershipCreationService;

    @Override
    public List<String> parseInfoAndCreateAccounts(String csv) {
        LOGGER.info("Internal employees creation with file");
        PostBookingParser parser = new PostBookingParser(csv);
        if (parser.getLines().isEmpty()) {
            return Collections.singletonList("No content in the file");
        }
        parser.getLines().forEach(line -> createEmployeeSubscription(parser, line));
        return parser.getErrors();
    }

    @Override
    public Long createPostBookingMembership(MembershipCreation membershipCreation, String email) throws DataAccessException, ActivatedMembershipException {
        try {
            String successfulLog = "Creating activated postBooking membership for userId {}";
            membershipCreation.setActivationDate(LocalDateTime.now());
            if (isNull(membershipCreation.getBalance())) {
                membershipCreation.setBalance(BigDecimal.ZERO);
            }
            if (MembershipType.BASIC_FREE.equals(membershipCreation.getMembershipType())) {
                membershipCreation.setAutoRenewal(MembershipRenewal.DISABLED);
            }
            Long memberId = createMembershipIfNotExistActive(membershipCreation, successfulLog, membershipCreation.getStatusAction());
            getMembershipMessageSendingManager().sendSubscriptionMessageToCRMTopic(email, membershipCreation, SubscriptionStatus.SUBSCRIBED);
            return memberId;
        } catch (DataAccessException | SQLException e) {
            throw new DataAccessRollbackException("Error creating membership for userId " + membershipCreation.getMemberAccountCreation().getUserId(), e);
        }
    }

    private Long createMembershipIfNotExistActive(MembershipCreation membershipCreation, String logMessage, StatusAction statusAction) throws ActivatedMembershipException, SQLException, DataAccessException {
        Long userId = membershipCreation.getMemberAccountCreation().getUserId();
        checkNoActivatedMemberForUser(userId, membershipCreation.getWebsite());
        LOGGER.info(logMessage, userId);
        Long memberId = getMemberManager().createMember(dataSource, membershipCreation);
        storeMemberStatusAction(memberId, statusAction);
        getMembershipMessageSendingManager().sendMembershipIdToMembershipReporter(memberId);
        return memberId;
    }

    @Override
    public Long createPostBookingMembershipPending(MembershipCreation membershipCreation) throws DataAccessException, ActivatedMembershipException {
        try {
            String successfulLog = "Creating postBooking membership pending to activate, userId: {}";
            StatusAction statusAction = StatusAction.PB_EMAIL_CREATION.equals(membershipCreation.getStatusAction())
                    ? membershipCreation.getStatusAction() : StatusAction.PB_PHONE_CREATION;
            return createMembershipIfNotExistActive(membershipCreation, successfulLog, statusAction);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error creating membership for userId " + membershipCreation.getMemberAccountCreation().getUserId(), e);
        }
    }

    @Override
    public PostBookingPageInfo getPostBookingPageInfo(String token) throws UnsupportedEncodingException {
        String postBookingPageInfoMsg = getCrmCipher().decryptToken(token);
        return parsePostPageInfoString(postBookingPageInfoMsg);
    }

    private PostBookingPageInfo parsePostPageInfoString(String postBookingPageInfoMsg) {
        PostBookingPageInfo postBookingPageInfo = new PostBookingPageInfo();
        Optional.ofNullable(getFirstMatchFromPattern(postBookingPageInfoMsg, BOOKING_ID_PATTERN)).map(Long::valueOf)
                .ifPresent(postBookingPageInfo::setBookingId);
        Optional.ofNullable(getFirstMatchFromPattern(postBookingPageInfoMsg, EMAIL_PATTERN))
                .ifPresent(postBookingPageInfo::setEmail);
        postBookingPageInfo.validatePageInfo();
        return postBookingPageInfo;
    }

    private String getFirstMatchFromPattern(String text, String patternStr) {
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(FIRST_PATTERN_MATCH);
        }
        return null;
    }

    private void createEmployeeSubscription(PostBookingParser parser, String line) {
        int row = parser.getLines().indexOf(line) + PostBookingParser.FILE_ROW_OFFSET;
        try {
            String[] userInfo = parser.parseLine(line);
            MembershipCreation membershipCreation = parser.getEmployeeMembershipCreation(userInfo);
            getMemberCreationService().create(membershipCreation);
        } catch (MissingElementException | DataAccessException e) {
            StringBuilder message = new StringBuilder("Row ").append(row).append(": ").append(e.getMessage());
            parser.addError(message.toString());
            LOGGER.error(message.toString());
        }
    }

    private void checkNoActivatedMemberForUser(Long userId, String website) throws ActivatedMembershipException, SQLException {
        List<MemberAccount> memberAccounts = getMemberAccountStore()
                .getMemberAccountAndMembershipsByStatus(dataSource, userId, MemberStatus.ACTIVATED).stream()
                .filter(MemberServiceUtils.websiteCheck(website)).collect(toList());
        if (!memberAccounts.isEmpty()) {
            throw new ActivatedMembershipException("An activated membership exists for userId " + userId + " and website " + website);
        }
    }

    private void storeMemberStatusAction(Long memberId, StatusAction action) throws DataAccessException {
        try {
            getMemberStatusActionStore().createMemberStatusAction(dataSource, memberId, action);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error creating memberStatusAction " + action.toString() + " for membershipId " + memberId, e);
        }
    }

    private MembershipCreationService getMemberCreationService() {
        return Optional.ofNullable(membershipCreationService).orElseGet(() -> {
            membershipCreationService = ConfigurationEngine.getInstance(MembershipCreationService.class);
            return membershipCreationService;
        });
    }


}
