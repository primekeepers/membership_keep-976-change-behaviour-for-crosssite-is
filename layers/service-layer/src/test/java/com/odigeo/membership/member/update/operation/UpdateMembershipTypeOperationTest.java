package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;

public class UpdateMembershipTypeOperationTest {

    private UpdateMembershipTypeOperation updateMembershipTypeOperation;
    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        updateMembershipTypeOperation = new UpdateMembershipTypeOperation(dataSource, membershipStore, membershipMessageSendingManager);
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateTypeUpdateMembership();
        long membershipId = Long.parseLong(updateMembership.getMembershipId());
        MembershipType membershipType = MembershipType.valueOf(updateMembership.getMembershipType());
        when(membershipStore.updateTypeAndRenewalPrice(eq(dataSource), eq(membershipId), eq(membershipType), eq(updateMembership.getRenewalPrice())))
                .thenReturn(Boolean.TRUE);
        boolean updated = updateMembershipTypeOperation.update(updateMembership);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membershipId));
        assertTrue(updated);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateWithExceptions() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateTypeUpdateMembership();
        long membershipId = Long.parseLong(updateMembership.getMembershipId());
        MembershipType membershipType = MembershipType.valueOf(updateMembership.getMembershipType());
        when(membershipStore.updateTypeAndRenewalPrice(eq(dataSource), eq(membershipId), eq(membershipType), eq(updateMembership.getRenewalPrice())))
                .thenThrow(new SQLException());
        updateMembershipTypeOperation.update(updateMembership);
    }


}