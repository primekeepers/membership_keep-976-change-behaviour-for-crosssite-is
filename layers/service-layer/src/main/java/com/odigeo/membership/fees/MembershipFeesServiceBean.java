package com.odigeo.membership.fees;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.fees.exception.FeesServiceException;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeContainer;
import com.odigeo.fees.model.FeeContainerType;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.fees.rest.FeesService;
import com.odigeo.fees.rest.requests.CreateContainerRequest;
import com.odigeo.membership.product.MembershipProductType;
import com.odigeo.util.FeeMapper;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class MembershipFeesServiceBean implements MembershipFeesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipFeesServiceBean.class);
    private FeesService feesService;
    private FeeMapper feeMapper;

    @Override
    public Optional<Long> requestFeeContainerCreation(BigDecimal subscriptionPrice, String currencyCode) {
        return Try.of(() -> getFeesService().createContainer(buildCreateContainerRequest(subscriptionPrice, currencyCode)))
                .map(FeeContainer::getId)
                .onFailure(FeesServiceException.class, e -> LOGGER.error("Error during Fee Container creation: {}", e.getMessage()))
                .toJavaOptional();
    }

    private CreateContainerRequest buildCreateContainerRequest(BigDecimal subscriptionPrice, String currencyCode) {
        CreateContainerRequest createContainerRequest = new CreateContainerRequest();
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setFeeMap(getFeeMapper().membershipFeeToFeeLabelMap(subscriptionPrice, currencyCode));
        feeContainer.setFeeContainerType(FeeContainerType.MEMBERSHIP_RENEWAL);
        createContainerRequest.setFeeContainer(feeContainer);
        createContainerRequest.setPartialSaveForFees(false);
        return createContainerRequest;
    }

    @Override
    public List<AbstractFee> retrieveFees(Long feeContainerId) {
        return Option.of(feeContainerId)
                .toTry()
                .mapTry(getFeesService()::findFeeContainer)
                .onFailure(FeesServiceException.class, e -> LOGGER.error("Error retrieving Fee Container: {}", e.getMessage()))
                .map(FeeContainer::getFeeMap)
                .map(Map::values)
                .recover(exception -> Collections.emptyList())
                .get()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public FixFee fillProductFee(BigDecimal price, String currencyCode) {
        FixFee fee = new FixFee(FeeLabel.MARKUP_TAX, price, Currency.getInstance(currencyCode), null);
        fee.setCreationDate(new Date());
        fee.setSubCode(MembershipProductType.MEMBERSHIP_RENEWAL.getFeeSubCode());
        return fee;
    }

    private FeesService getFeesService() {
        if (Objects.isNull(feesService)) {
            feesService = ConfigurationEngine.getInstance(FeesService.class);
        }
        return feesService;
    }

    private FeeMapper getFeeMapper() {
        if (Objects.isNull(feeMapper)) {
            feeMapper = ConfigurationEngine.getInstance(FeeMapper.class);
        }
        return feeMapper;
    }
}
