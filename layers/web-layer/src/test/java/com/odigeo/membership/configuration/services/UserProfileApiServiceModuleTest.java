package com.odigeo.membership.configuration.services;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v2.UserApiServiceInternal;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class UserProfileApiServiceModuleTest {

    private UserProfileApiSecurityConfiguration userProfileApiSecurityConfiguration;

    private UserProfileApiServiceModule userProfileApiServiceModule;

    @BeforeMethod
    public void beforeClass() {
        userProfileApiSecurityConfiguration = new UserProfileApiSecurityConfiguration();
        ConfigurationEngine.init(this::configure);
        userProfileApiServiceModule = new UserProfileApiServiceModule();
    }

    private void configure(Binder binder) {
        binder.bind(UserProfileApiSecurityConfiguration.class).toInstance(userProfileApiSecurityConfiguration);
    }

    @Test
    public void testGetServiceConfiguration() {
        Class<?> userApiService = UserApiServiceInternal.class;
        ServiceConfiguration<UserApiServiceInternal> userApiServiceInternalServiceConfiguration = userProfileApiServiceModule.getServiceConfiguration(userApiService);
        assertNotNull(userApiServiceInternalServiceConfiguration);
    }

}