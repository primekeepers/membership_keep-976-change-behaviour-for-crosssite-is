package com.odigeo.membership.tracking.autorenewal;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.annotations.VisibleForTesting;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.AutorenewalTracking;
import com.odigeo.membership.member.AbstractServiceBean;
import com.odigeo.membership.tracking.AutorenewalTrackingStore;
import com.odigeo.visitengineapi.v1.VisitEngine;
import com.odigeo.visitengineapi.v1.interfaces.Interface;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@Local(AutorenewalTrackingService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AutorenewalTrackingServiceBean extends AbstractServiceBean implements AutorenewalTrackingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutorenewalTrackingServiceBean.class);

    @Override
    @Asynchronous
    public void trackAutorenewalOperation(AutorenewalTracking autorenewalTracking) {
        if (!autorenewalTracking.getVisitInformation().isEmpty()) {
            Try.of(() -> getVisitEngine().buildVisit(autorenewalTracking.getVisitInformation()))
                    .onFailure(error -> LOGGER.error("Error getting visit data during auto-renewal tracking", error))
                    .toJavaOptional()
                    .map(VisitResponse::getClientInterface)
                    .map(Interface::getId)
                    .ifPresent(autorenewalTracking::setInterfaceId);
        }

        Try.run(() -> getAutoRenewalTrackingStore().insertAutorenewalTracking(dataSource, autorenewalTracking))
                .onFailure(error -> LOGGER.error("Error saving auto-renewal tracking information", error)).isSuccess();

        MetricsUtils.incrementCounter(MetricsBuilder.buildAutorenewalBySource(autorenewalTracking), MetricsNames.METRICS_REGISTRY_NAME);
    }

    @VisibleForTesting
    AutorenewalTrackingStore getAutoRenewalTrackingStore() {
        return ConfigurationEngine.getInstance(AutorenewalTrackingStore.class);
    }

    @VisibleForTesting
    VisitEngine getVisitEngine() {
        return ConfigurationEngine.getInstance(VisitEngine.class);
    }
}
