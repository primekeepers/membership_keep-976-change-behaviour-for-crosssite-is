package com.odigeo.membership.propertiesconfig;

import com.odigeo.membership.exception.DataNotFoundException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PropertiesConfigurationStoreTest {

    private static final String KEY = "TEST_KEY";

    @Mock
    private Connection connectionMock;
    @Mock
    private DataSource dataSourceMock;
    @Mock
    private PreparedStatement preparedStatementMock;
    @Mock
    private ResultSet resultSetMock;

    private PropertiesConfigurationStore propertiesConfigurationStore;

    @BeforeMethod
    public void setUp() throws SQLException {
        configMocks();
        propertiesConfigurationStore = new PropertiesConfigurationStore();
    }

    @Test
    public void testGetConfigValueByKey() throws SQLException, DataNotFoundException {
        when(resultSetMock.getInt("VALUE")).thenReturn(1);
        when(resultSetMock.next()).thenReturn(true);
        when(preparedStatementMock.executeQuery()).thenReturn(resultSetMock);
        propertiesConfigurationStore.getConfigValueByKey(dataSourceMock, KEY);
    }

    @Test (expectedExceptions = SQLException.class)
    public void testGetConfigValueByKeySQLException() throws SQLException, DataNotFoundException {
        when(preparedStatementMock.executeQuery()).thenThrow(new SQLException());
        propertiesConfigurationStore.getConfigValueByKey(dataSourceMock, KEY);
    }

    @Test (expectedExceptions = DataNotFoundException.class)
    public void testGetConfigValueByKeyDataNotFoundException() throws SQLException, DataNotFoundException {
        when(resultSetMock.next()).thenReturn(false);
        when(preparedStatementMock.executeQuery()).thenReturn(resultSetMock);
        propertiesConfigurationStore.getConfigValueByKey(dataSourceMock, KEY);
    }

    @Test
    public void testUpdateConfigValueByKeyToTrueUpdated() throws Exception {
        assertTrue(testUpdateConfigValueByKey(1,true, 1));
    }

    @Test
    public void testUpdateConfigValueByKeyToFalseUpdated() throws Exception {
        assertTrue(testUpdateConfigValueByKey(1,false, 0));
    }

    @Test
    public void testUpdateConfigValueByKeyToTrueNotUpdated() throws Exception {
        assertFalse(testUpdateConfigValueByKey(0,true, 1));
    }

    @Test
    public void testUpdateConfigValueByKeyToFalseNotUpdated() throws Exception {
        assertFalse(testUpdateConfigValueByKey(0,false, 0));
    }

    @Test(expectedExceptions = SQLException.class, expectedExceptionsMessageRegExp = "Expected SQLException")
    public void testUpdateConfigValueByKeyThrowsSqlException() throws Exception {
        //Given
        given(preparedStatementMock.executeUpdate()).willThrow(new SQLException("Expected SQLException"));
        //When
        propertiesConfigurationStore.updateConfigValueByKey(dataSourceMock, KEY, true);
    }

    public boolean testUpdateConfigValueByKey(int executionResult, boolean value, int intValue) throws Exception {
        //Given
        given(preparedStatementMock.executeUpdate()).willReturn(executionResult);
        //When
        boolean updated = propertiesConfigurationStore.updateConfigValueByKey(dataSourceMock, KEY, value);
        //Then
        verify(preparedStatementMock).setInt(1, intValue);
        verify(preparedStatementMock).setString(2, KEY);

        return updated;
    }

    private void configMocks() throws SQLException {
        openMocks(this);
        when(dataSourceMock.getConnection()).thenReturn(connectionMock);
        when(connectionMock.prepareStatement(anyString())).thenReturn(preparedStatementMock);
    }
}