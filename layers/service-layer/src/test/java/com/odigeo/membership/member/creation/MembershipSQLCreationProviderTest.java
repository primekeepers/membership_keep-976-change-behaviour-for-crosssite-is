package com.odigeo.membership.member.creation;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.member.creation.sql.BasicFreeMembershipService;
import com.odigeo.membership.member.creation.sql.NewMembershipSubscriptionService;
import com.odigeo.membership.member.creation.sql.PendingToCollectMembershipService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.search.SearchService;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import com.odigeo.userapi.UserApiManager;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertTrue;

public class MembershipSQLCreationProviderTest {

    @Mock
    private MembershipCreation membershipCreation;
    @Mock
    private UserApiManager userApiManager;
    @Mock
    private SearchService searchService;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;
    @Mock
    private MembershipKafkaSender membershipKafkaSender;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(UserApiManager.class).toInstance(userApiManager);
        binder.bind(SearchService.class).toInstance(searchService);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManager);
        binder.bind(MembershipKafkaSender.class).toInstance(membershipKafkaSender);
    }

    @Test
    public void getInstanceForNewSubscriptionCreation() {
        MembershipCreationServiceProvider membershipCreationFactory = getMembershipCreationFactoryProvider()
                .getInstance(membershipCreation);
        assertTrue(membershipCreationFactory instanceof NewMembershipSubscriptionService);
    }

    @Test
    public void getInstanceForPendingToCollectCreation() {
        when(membershipCreation.getMemberStatus()).thenReturn(MemberStatus.PENDING_TO_COLLECT);
        when(membershipCreation.getMembershipType()).thenReturn(MembershipType.BASIC);
        MembershipCreationServiceProvider membershipCreationFactory = getMembershipCreationFactoryProvider()
                .getInstance(membershipCreation);
        assertTrue(membershipCreationFactory instanceof PendingToCollectMembershipService);
    }

    @Test
    public void getInstanceForPendingToCollectBusinessCreation() {
        when(membershipCreation.getMemberStatus()).thenReturn(MemberStatus.PENDING_TO_COLLECT);
        when(membershipCreation.getMembershipType()).thenReturn(MembershipType.BUSINESS);
        MembershipCreationServiceProvider membershipCreationFactory = getMembershipCreationFactoryProvider()
                .getInstance(membershipCreation);
        assertTrue(membershipCreationFactory instanceof PendingToCollectMembershipService);
    }

    @Test
    public void getInstanceForBasicFreeCreation() {
        when(membershipCreation.getMemberStatus()).thenReturn(MemberStatus.ACTIVATED);
        when(membershipCreation.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        MembershipCreationServiceProvider membershipCreationFactory = getMembershipCreationFactoryProvider()
                .getInstance(membershipCreation);
        assertTrue(membershipCreationFactory instanceof BasicFreeMembershipService);
    }

    private MembershipCreationFactoryProvider getMembershipCreationFactoryProvider() {
        return ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class);
    }
}
