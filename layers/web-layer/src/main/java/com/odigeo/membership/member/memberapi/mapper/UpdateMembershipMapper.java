package com.odigeo.membership.member.memberapi.mapper;

import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import org.mapstruct.Mapper;

@Mapper
public interface UpdateMembershipMapper {

    UpdateMembership map(UpdateMembershipRequest updateMembershipRequest);
}
