package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.member.MemberAccountStore;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.creation.sql.NewMembershipSubscriptionService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class NewMembershipSubscriptionCreationTest {

    private static final long MEMBERSHIP_ID = 8L;
    private static final long MEMBER_ACCOUNT_ID = 91L;

    private final MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder();
    private final MembershipCreation membershipCreation = new MembershipCreation(membershipCreationBuilder);

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;

    private MembershipCreationServiceProvider membershipCreationFactory;

    @Captor
    ArgumentCaptor<MembershipCreation> membershipCreationArgumentCaptor;
    @Captor
    ArgumentCaptor<StatusAction> statusActionArgumentCaptor;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipCreationFactory = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class)
                .getInstance(membershipCreation);
    }

    private void configure(Binder binder) {
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
    }

    @Test
    public void checkRightFactoryReturnedTest() {
        assertTrue(membershipCreationFactory instanceof NewMembershipSubscriptionService);
    }

    @Test
    public void createMembershipTest() throws MissingElementException, DataAccessException, SQLException {
        when(memberAccountStore.createMemberAccount(any(DataSource.class), anyLong(), anyString(), anyString())).thenReturn(MEMBER_ACCOUNT_ID);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        long newMembershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        verify(memberManager).createMember(any(DataSource.class), membershipCreationArgumentCaptor.capture());
        verify(memberStatusActionStore).createMemberStatusAction(any(DataSource.class), anyLong(), statusActionArgumentCaptor.capture());
        assertEquals(MEMBERSHIP_ID, newMembershipId);
        assertEquals(SourceType.FUNNEL_BOOKING, membershipCreationArgumentCaptor.getValue().getSourceType());
        assertEquals(MemberStatus.PENDING_TO_ACTIVATE, membershipCreationArgumentCaptor.getValue().getMemberStatus());
        assertEquals(StatusAction.CREATION, statusActionArgumentCaptor.getValue());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void createMembershipSQLException() throws MissingElementException, DataAccessException, SQLException {
        doThrow(new SQLException()).when(memberStatusActionStore)
                .createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        membershipCreationFactory.createMembership(dataSource, membershipCreation);
    }
}
