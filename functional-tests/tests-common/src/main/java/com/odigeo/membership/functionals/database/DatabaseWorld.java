package com.odigeo.membership.functionals.database;

import com.google.inject.Inject;
import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.RandomBuilder;
import com.odigeo.membership.functionals.membership.AuditMemberAccountBuilder;
import com.odigeo.membership.functionals.membership.BlacklistPaymentMethodBuilder;
import com.odigeo.membership.functionals.membership.MemberAccountBuilder;
import com.odigeo.membership.functionals.membership.MemberStatusActionBuilder;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import com.odigeo.membership.functionals.membership.MembershipProductFeeBuilder;
import com.odigeo.membership.functionals.membership.booking.MembershipBookingBuilder;
import com.odigeo.membership.functionals.membership.booking.MembershipBookingHelper;
import com.odigeo.membership.mocks.database.stores.AuditMemberAccountStore;
import com.odigeo.membership.mocks.database.stores.AutorenewalTrackingStore;
import com.odigeo.membership.mocks.database.stores.MemberAccountStore;
import com.odigeo.membership.mocks.database.stores.MemberStatusActionStore;
import com.odigeo.membership.mocks.database.stores.MembershipBookingTrackingStore;
import com.odigeo.membership.mocks.database.stores.MembershipFeeStore;
import com.odigeo.membership.mocks.database.stores.MembershipProductFeeStore;
import com.odigeo.membership.mocks.database.stores.MembershipRulesStore;
import com.odigeo.membership.mocks.database.stores.MembershipStore;
import com.odigeo.membership.mocks.database.stores.PaymentMethodBlacklistStore;
import com.odigeo.membership.mocks.database.stores.PropertiesConfigurationStore;
import com.odigeo.membership.mocks.database.stores.RecurringMembershipStore;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DatabaseWorld {

    private static final long AUTORENEWAL_TRACKING_WAIT_SECONDS = 3;
    private final DatabaseInstallerWorld databaseInstallerWorld;
    private final Random random;
    private static final Logger LOGGER = Logger.getLogger(DatabaseWorld.class);
    private final PropertiesConfigurationStore propertiesConfigurationStore;

    @Inject
    private MembershipStore membershipStore;
    @Inject
    private MemberAccountStore memberAccountStore;
    @Inject
    private MembershipRulesStore membershipRulesStore;
    @Inject
    private AuditMemberAccountStore auditMemberAccountStore;
    @Inject
    private MembershipBookingTrackingStore membershipBookingTrackingStore;
    @Inject
    private MemberStatusActionStore memberStatusActionStore;
    @Inject
    private MembershipProductFeeStore membershipProductFeeStore;
    @Inject
    private RecurringMembershipStore recurringMembershipStore;
    @Inject
    private MembershipFeeStore membershipFeeStore;
    @Inject
    private PaymentMethodBlacklistStore paymentMethodBlacklistStore;
    @Inject
    private AutorenewalTrackingStore autorenewalTrackingStore;

    @Inject
    public DatabaseWorld(DatabaseInstallerWorld databaseInstallerWorld, RandomBuilder randomBuilder,
                         PropertiesConfigurationStore propertiesConfigurationStore) {
        this.databaseInstallerWorld = databaseInstallerWorld;
        this.random = randomBuilder.build();
        this.propertiesConfigurationStore = propertiesConfigurationStore;
    }

    public void addMemberships(List<MembershipBuilder> memberships) throws SQLException, InterruptedException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            membershipStore.saveMemberships(conn, memberships);
        }
    }

    public void addMembershipsRules(int numBookings, int numDays, int ruleActivated) throws SQLException, InterruptedException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            membershipRulesStore.saveMembershipRules(conn, numBookings, numDays, ruleActivated);
        }
    }

    public void addMemberBookings(long memberId, int numBookings, int numDays) throws SQLException, InterruptedException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            membershipBookingTrackingStore.
                    saveMembershipBooking(conn, MembershipBookingHelper.createMembershipBookingRandomList(memberId, numBookings, numDays, new Picker(random)));
        }
    }

    public void addTrackedBookings(final List<MembershipBookingBuilder> trackedBookings) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            membershipBookingTrackingStore.saveMembershipBooking(conn, trackedBookings);
        }
    }

    public void addMemberAccounts(List<MemberAccountBuilder> memberAccounts) throws SQLException, InterruptedException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            memberAccountStore.saveMemberAccounts(conn, memberAccounts);
        }
    }

    public MemberAccountBuilder getMemberAccountByMemberAccountId(Long memberAccountId) {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return memberAccountStore.getMemberAccountByMemberAccountId(conn, memberAccountId);
        } catch (SQLException | ClassNotFoundException | InterruptedException e) {
            LOGGER.error("Failed to getMemberAccount with ID: " + memberAccountId, e);
            return null;
        }
    }

    public List<AuditMemberAccountBuilder> getMemberAccountAudits(final Long memberAccountId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return auditMemberAccountStore.findMemberAccountAudits(conn, memberAccountId);
        }
    }

    public MembershipBuilder getMembershipById(final Long membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return membershipStore.getMembershipById(conn, membershipId);
        }
    }

    public Optional<String> getRecurringByMembershipId(final Long membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return recurringMembershipStore.getRecurringByMembershipId(conn, membershipId);
        }
    }

    public Optional<String> getRecurringCollectionIdByMembershipId(final Long membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return recurringMembershipStore.getRecurringCollectionIdByMembershipId(conn, membershipId);
        }
    }

    public Optional<MembershipFeeStore.MembershipFee> getMembershipFeesByMembershipId(final Long membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return membershipFeeStore.getMembershipFeesByMembershipId(conn, membershipId);
        }
    }

    public List<MembershipBuilder> getMembershipByMemberAccountId(final Long memberAccountId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return membershipStore.getMembershipByMemberAccountId(conn, memberAccountId);
        }
    }

    public List<MembershipBookingBuilder> findTrackedBookingsByBookingId(final Long bookingId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return membershipBookingTrackingStore.findByBookingId(conn, bookingId);
        }
    }

    public Optional<BigDecimal> findMembershipBalance(final Long membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return membershipStore.findMembershipBalance(conn, membershipId);
        }
    }

    public void addMemberStatusActions(final List<MemberStatusActionBuilder> statusActions) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            memberStatusActionStore.saveMemberStatusActions(conn, statusActions);
        }
    }

    public void addMembershipProductFees(List<MembershipProductFeeBuilder> membershipProductFees) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            membershipProductFeeStore.saveMembershipProductFees(conn, membershipProductFees);
        }
    }

    public void addPropertiesConfigurationValue(final String key, final boolean value) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            propertiesConfigurationStore.resetPropertyValue(conn, key, value);
        }
    }

    public boolean findPropertiesConfigurationValue(final String key) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            return propertiesConfigurationStore.findPropertyValue(conn, key);
        }
    }

    public void addBlacklistedPaymentMethods(List<BlacklistPaymentMethodBuilder> paymentMethods) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            paymentMethodBlacklistStore.saveBlacklistPaymentMethods(conn, paymentMethods);
        }
    }

    public boolean wasAutorenewalTrackingEventuallySaved(String membershipId, String autoRenewalOperation) throws InterruptedException, SQLException, ClassNotFoundException {
        try (Connection conn = databaseInstallerWorld.getConnection()) {
            TimeUnit.SECONDS.sleep(AUTORENEWAL_TRACKING_WAIT_SECONDS);
            return autorenewalTrackingStore.findAutorenewalTracking(conn, membershipId, autoRenewalOperation).size() > 0;
        }
    }
}
