package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static com.odigeo.membership.MemberStatus.ACTIVATED;
import static com.odigeo.membership.MemberStatus.DEACTIVATED;
import static com.odigeo.membership.MemberStatus.DISCARDED;
import static com.odigeo.membership.MemberStatus.PENDING_TO_COLLECT;
import static java.lang.Boolean.TRUE;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class DiscardMembershipOperationTest extends MembershipStatusChangeOperationTest{
    private DiscardMembershipOperation discardMembershipOperation;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        discardMembershipOperation = new DiscardMembershipOperation(dataSource, memberService, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
    }

    @Test
    public void testUpdate() throws MissingElementException, DataAccessException, ExistingRecurringException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setStatus(MemberStatus.PENDING_TO_COLLECT)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId()))
                .thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(DISCARDED), eq(membership.getAutoRenewal())))
                .thenReturn(TRUE);
        assertTrue(discardMembershipOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verifySuccessfulDiscard(membership);
    }

    @Test
    public void testUpdateOnlyPendingToCollect() throws MissingElementException, DataAccessException, ExistingRecurringException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId()))
                .thenReturn(membership);
        assertFalse(discardMembershipOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verifyZeroInteractions(membershipMessageSendingManager, memberStatusActionStore, membershipStore);
    }

    private void verifySuccessfulDiscard(Membership membership) throws SQLException {
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopicByRule(eq(PENDING_TO_COLLECT), eq(DISCARDED), eq(membership));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), eq(membership.getId()), eq(StatusAction.DISCARD));
    }
}