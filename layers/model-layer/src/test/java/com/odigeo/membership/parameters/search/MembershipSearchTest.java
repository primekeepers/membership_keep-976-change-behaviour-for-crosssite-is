package com.odigeo.membership.parameters.search;

import bean.test.BeanTest;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipSearchTest extends BeanTest<MembershipSearch> {

    private static final Long USER_ID = 123L;
    private static final String CURRENCY = "EUR";
    private static final String TEST_DATE = "2021-12-12";
    private static final String WEBSITE = "ES";
    private static final String LAST_NAME = "Sanchez";
    private static final String NAME = "Mario";
    private static final int DURATION = 12;

    private static final String SEARCH_MEMBERSHIP_BY_WEBSITE = " SELECT gmr.RECURRING_ID, gmrc.RECURRING_COLLECTION_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, " +
            "m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE, m.MEMBERSHIP_TYPE," +
            " m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.DURATION_TIME_UNIT, m.DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID, gmrml.REMIND_ME_LATER_FLAG, gmrml.REMIND_ME_LATER_LAST_UPDATE FROM  GE_MEMBERSHIP m " +
            " LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc ON gmrc.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml ON gmrml.MEMBERSHIP_ID = m.ID  WHERE m.WEBSITE = ? ";
    private static final String SEARCH_MEMBERSHIP_BY_WEBSITE_AND_USER_ID_WITH_ACCOUNT = " SELECT gmr.RECURRING_ID, gmrc.RECURRING_COLLECTION_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, " +
            "m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE, m.MEMBERSHIP_TYPE, m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.DURATION_TIME_UNIT, m.DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID, " +
            "gmrml.REMIND_ME_LATER_FLAG, gmrml.REMIND_ME_LATER_LAST_UPDATE, " +
            "a.ID as ACCOUNT_ID, a.USER_ID, a.FIRST_NAME, a.LAST_NAME, a.TIMESTAMP FROM  GE_MEMBERSHIP m  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc ON gmrc.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml ON gmrml.MEMBERSHIP_ID = m.ID  JOIN  GE_MEMBER_ACCOUNT a ON a.ID = m.MEMBER_ACCOUNT_ID  WHERE m.WEBSITE = ?  AND a.USER_ID = ? ";
    private static final String SEARCH_MEMBERSHIP_BY_WEBSITE_WITH_STATUS_ACTIONS = " SELECT gmr.RECURRING_ID, gmrc.RECURRING_COLLECTION_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, " +
            "m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE, m.MEMBERSHIP_TYPE," +
            " m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.DURATION_TIME_UNIT, m.DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID, gmrml.REMIND_ME_LATER_FLAG, gmrml.REMIND_ME_LATER_LAST_UPDATE, " +
            "gmsa.ID as MEMBER_STATUS_ACTION_ID , gmsa.MEMBER_ID, gmsa.ACTION_TYPE, gmsa.ACTION_DATE " +
            "FROM  GE_MEMBERSHIP m  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc ON gmrc.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml ON gmrml.MEMBERSHIP_ID = m.ID " +
            " LEFT OUTER JOIN  GE_MEMBER_STATUS_ACTION gmsa ON m.ID = gmsa.MEMBER_ID  WHERE m.WEBSITE = ? ";
    private static final String SEARCH_MEMBERSHIP_BY_WEBSITE_AND_USER_ID_WITH_ACCOUNT_AND_STATUS_ACTIONS = " SELECT gmr.RECURRING_ID, gmrc.RECURRING_COLLECTION_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, " +
            "m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE, m.MEMBERSHIP_TYPE, m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.DURATION_TIME_UNIT, m.DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID, " +
            "gmrml.REMIND_ME_LATER_FLAG, gmrml.REMIND_ME_LATER_LAST_UPDATE, " +
            "gmsa.ID as MEMBER_STATUS_ACTION_ID , gmsa.MEMBER_ID, gmsa.ACTION_TYPE, gmsa.ACTION_DATE, " +
            "a.ID as ACCOUNT_ID, a.USER_ID, a.FIRST_NAME, a.LAST_NAME, a.TIMESTAMP " +
            "FROM  GE_MEMBERSHIP m  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING_COLLECTION gmrc ON gmrc.MEMBERSHIP_ID = m.ID  LEFT OUTER JOIN GE_MEMBERSHIP_REMIND_ME_LATER gmrml ON gmrml.MEMBERSHIP_ID = m.ID " +
            " LEFT OUTER JOIN  GE_MEMBER_STATUS_ACTION gmsa ON m.ID = gmsa.MEMBER_ID " +
            " JOIN  GE_MEMBER_ACCOUNT a ON a.ID = m.MEMBER_ACCOUNT_ID  WHERE m.WEBSITE = ?  AND a.USER_ID = ? ";


    @Test
    public void testEmptiness() {
        MembershipSearch membershipSearch = new MembershipSearchBuilder().build();
        assertTrue(membershipSearch.getValues().isEmpty());
        assertTrue(membershipSearch.isEmpty());
        MembershipSearch membershipSearchOnlyAccount = new MembershipSearchBuilder().memberAccountSearch(MemberAccountSearch.builder().build()).build();
        assertTrue(membershipSearch.getValues().isEmpty());
        assertTrue(membershipSearch.isEmpty());
        MembershipSearch membershipWebsiteSearch = new MembershipSearchBuilder().website(WEBSITE).build();
        assertFalse(membershipWebsiteSearch.getValues().isEmpty());
        assertFalse(membershipWebsiteSearch.isEmpty());
    }

    @Test
    public void testGetQueryString() {
        MembershipSearchBuilder builder = new MembershipSearchBuilder();
        MembershipSearch membershipWebsiteSearch = builder.website(WEBSITE).build();
        assertEquals(membershipWebsiteSearch.getQueryString(), SEARCH_MEMBERSHIP_BY_WEBSITE);
        MembershipSearch membershipUserIdAndWebsiteSearch = builder.withMemberAccount(true)
                .memberAccountSearch(new MemberAccountSearch.Builder().userId(USER_ID).build())
                .build();
        assertEquals(membershipUserIdAndWebsiteSearch.getQueryString(), SEARCH_MEMBERSHIP_BY_WEBSITE_AND_USER_ID_WITH_ACCOUNT);
    }

    @Test
    public void testGetQueryStringWithStatusActions() {
        MembershipSearchBuilder builder = new MembershipSearchBuilder();
        MembershipSearch membershipWebsiteSearch = builder.website(WEBSITE).withStatusActions(true).build();
        assertEquals(membershipWebsiteSearch.getQueryString(), SEARCH_MEMBERSHIP_BY_WEBSITE_WITH_STATUS_ACTIONS);
    }

    @Test
    public void testGetQueryStringWithAccountAndStatusActions() {
        MembershipSearchBuilder builder = new MembershipSearchBuilder();
        MembershipSearch membershipWebsiteSearch = builder.website(WEBSITE)
                .withMemberAccount(true).memberAccountSearch(new MemberAccountSearch.Builder().userId(USER_ID).build())
                .withStatusActions(true).build();
        assertEquals(membershipWebsiteSearch.getQueryString(), SEARCH_MEMBERSHIP_BY_WEBSITE_AND_USER_ID_WITH_ACCOUNT_AND_STATUS_ACTIONS);
    }

    @Override
    protected MembershipSearch getBean() {
        return new MembershipSearchBuilder()
                .autoRenewal(MembershipRenewal.DISABLED.name())
                .currencyCode(CURRENCY)
                .fromActivationDate(TEST_DATE)
                .toActivationDate(TEST_DATE)
                .fromExpirationDate(TEST_DATE)
                .toExpirationDate(TEST_DATE)
                .maxBalance(BigDecimal.TEN)
                .membershipType(MembershipType.BASIC.name())
                .minBalance(BigDecimal.ONE)
                .monthsDuration(1)
                .productStatus(ProductStatus.INIT.name())
                .sourceType(SourceType.FUNNEL_BOOKING.name())
                .status(MemberStatus.ACTIVATED.name())
                .totalPrice(BigDecimal.TEN)
                .renewalPrice(BigDecimal.ONE)
                .renewalDuration(DURATION)
                .website(WEBSITE)
                .withMemberAccount(true)
                .withStatusActions(true)
                .sorting(SortingDTO.builder().build())
                .memberAccountSearch(new MemberAccountSearch.Builder()
                        .lastNames(LAST_NAME)
                        .userId(USER_ID)
                        .name(NAME).build())
                .build();
    }

    @Test
    public void testBuilderCopyOf() {
        MembershipSearch membershipSearch = getBean();
        assertEquals(MembershipSearch.builder(membershipSearch).build(), membershipSearch);
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(MembershipSearch.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
