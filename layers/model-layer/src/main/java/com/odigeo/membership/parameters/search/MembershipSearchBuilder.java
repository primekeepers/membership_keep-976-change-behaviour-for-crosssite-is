package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.Condition;
import com.odigeo.membership.enums.db.MembershipField;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.odigeo.membership.enums.db.Condition.GTE;
import static com.odigeo.membership.enums.db.Condition.LTE;

public class MembershipSearchBuilder {

    protected String email;
    protected String website;
    protected String status;
    protected String autoRenewal;
    protected String fromExpirationDate;
    protected String toExpirationDate;
    protected String fromActivationDate;
    protected String toActivationDate;
    protected String fromCreationDate;
    protected String toCreationDate;
    protected String membershipType;
    protected BigDecimal minBalance;
    protected BigDecimal maxBalance;
    protected Integer monthsDuration;
    protected String durationTimeUnit;
    protected Integer duration;
    protected String productStatus;
    protected BigDecimal totalPrice;
    protected BigDecimal renewalPrice;
    protected Integer renewalDuration;
    protected String currencyCode;
    protected String sourceType;
    protected Long memberAccountId;
    protected boolean withStatusActions;
    protected boolean withMemberAccount;
    protected MemberAccountSearch memberAccountSearch;
    protected SortingDTO sorting;
    protected PaginationDTO pagination;
    protected List<FilterCondition> filterConditions;

    public MembershipSearchBuilder website(String website) {
        this.website = website;
        return this;
    }

    public MembershipSearchBuilder duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public MembershipSearchBuilder timeUnit(String timeUnit) {
        this.durationTimeUnit = timeUnit;
        return this;
    }

    public MembershipSearchBuilder status(String status) {
        this.status = status;
        return this;
    }

    public MembershipSearchBuilder autoRenewal(String autoRenewal) {
        this.autoRenewal = autoRenewal;
        return this;
    }

    public MembershipSearchBuilder fromExpirationDate(String fromExpirationDate) {
        this.fromExpirationDate = fromExpirationDate;
        return this;
    }

    public MembershipSearchBuilder toExpirationDate(String toExpirationDate) {
        this.toExpirationDate = toExpirationDate;
        return this;
    }

    public MembershipSearchBuilder fromActivationDate(String fromActivationDate) {
        this.fromActivationDate = fromActivationDate;
        return this;
    }

    public MembershipSearchBuilder toActivationDate(String toActivationDate) {
        this.toActivationDate = toActivationDate;
        return this;
    }

    public MembershipSearchBuilder fromCreationDate(String fromCreationDate) {
        this.fromCreationDate = fromCreationDate;
        return this;
    }

    public MembershipSearchBuilder toCreationDate(String toCreationDate) {
        this.toCreationDate = toCreationDate;
        return this;
    }

    public MembershipSearchBuilder membershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public MembershipSearchBuilder monthsDuration(Integer monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public MembershipSearchBuilder productStatus(String productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public MembershipSearchBuilder totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public MembershipSearchBuilder renewalPrice(BigDecimal renewalPrice) {
        this.renewalPrice = renewalPrice;
        return this;
    }

    public MembershipSearchBuilder renewalDuration(Integer renewalDuration) {
        this.renewalDuration = renewalDuration;
        return this;
    }

    public MembershipSearchBuilder currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public MembershipSearchBuilder sourceType(String sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public MembershipSearchBuilder withStatusActions(boolean withStatusActions) {
        this.withStatusActions = withStatusActions;
        return this;
    }

    public MembershipSearchBuilder minBalance(BigDecimal minBalance) {
        this.minBalance = minBalance;
        return this;
    }

    public MembershipSearchBuilder maxBalance(BigDecimal maxBalance) {
        this.maxBalance = maxBalance;
        return this;
    }

    public MembershipSearchBuilder memberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MembershipSearchBuilder withMemberAccount(boolean withMemberAccount) {
        this.withMemberAccount = withMemberAccount;
        return this;
    }

    public MembershipSearchBuilder memberAccountSearch(MemberAccountSearch memberAccountSearch) {
        this.memberAccountSearch = memberAccountSearch;
        return this;
    }

    public MembershipSearchBuilder sorting(SortingDTO sorting) {
        this.sorting = sorting;
        return this;
    }

    public MembershipSearchBuilder email(String email) {
        this.email = email;
        return this;
    }

    public MembershipSearchBuilder durationTimeUnit(String durationTimeUnit) {
        this.durationTimeUnit = durationTimeUnit;
        return this;
    }

    public MembershipSearchBuilder pagination(PaginationDTO pagination) {
        this.pagination = pagination;
        return this;
    }

    private MembershipSearchBuilder filterConditions() {
        filterConditions = new ArrayList<>();
        fillEqualsToConditions();
        fillGreaterThanConditions();
        fillLowerThanConditions();
        return this;
    }

    private void fillEqualsToConditions() {
        List.of(
                Pair.of(MembershipField.WEBSITE, website),
                Pair.of(MembershipField.STATUS, status),
                Pair.of(MembershipField.AUTO_RENEWAL, autoRenewal),
                Pair.of(MembershipField.MEMBERSHIP_TYPE, membershipType),
                Pair.of(MembershipField.PRODUCT_STATUS, productStatus),
                Pair.of(MembershipField.TOTAL_PRICE, totalPrice),
                Pair.of(MembershipField.RENEWAL_PRICE, renewalPrice),
                Pair.of(MembershipField.MONTHS_RENEWAL_DURATION, renewalDuration),
                Pair.of(MembershipField.SOURCE_TYPE, sourceType),
                Pair.of(MembershipField.MONTHS_DURATION, monthsDuration),
                Pair.of(MembershipField.DURATION_TIME_UNIT, durationTimeUnit),
                Pair.of(MembershipField.DURATION, duration),
                Pair.of(MembershipField.CURRENCY_CODE, currencyCode),
                Pair.of(MembershipField.MEMBER_ACCOUNT_ID, memberAccountId)
        ).forEach(pair -> Optional.ofNullable(pair.getValue())
                .map(val -> new FilterCondition(pair.getKey(), val))
                .ifPresent(filterConditions::add));
    }

    private void fillGreaterThanConditions() {
        fillConditions(fromExpirationDate, fromActivationDate, fromCreationDate, minBalance, GTE, LocalDate::atStartOfDay);
    }

    private void fillLowerThanConditions() {
        fillConditions(toExpirationDate, toActivationDate, toCreationDate, maxBalance, LTE, LocalTime.MAX::atDate);
    }

    private void fillConditions(String expirationDate, String activationDate, String creationDate, BigDecimal balance, Condition condition, Function<LocalDate, LocalDateTime> toLocalDateTimeFun) {
        List.of(
                Pair.of(MembershipField.EXPIRATION_DATE, toTimestamp(expirationDate, toLocalDateTimeFun)),
                Pair.of(MembershipField.ACTIVATION_DATE, toTimestamp(activationDate, toLocalDateTimeFun)),
                Pair.of(MembershipField.TIMESTAMP, toTimestamp(creationDate, toLocalDateTimeFun)),
                Pair.of(MembershipField.BALANCE, balance)
        ).forEach(pair -> Optional.ofNullable(pair.getValue())
                .map(val -> new FilterCondition(pair.getKey(), condition, val))
                .ifPresent(filterConditions::add));
    }

    private static Timestamp toTimestamp(String date, Function<LocalDate, LocalDateTime> toTime) {
        return Optional.ofNullable(date)
                .map(LocalDate::parse)
                .map(toTime)
                .map(Timestamp::valueOf)
                .orElse(null);
    }

    public MembershipSearch build() {
        return new MembershipSearch(filterConditions());
    }
}


