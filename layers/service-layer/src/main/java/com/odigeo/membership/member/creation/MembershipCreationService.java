package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.MembershipCreation;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Optional;

public interface MembershipCreationService {
    Optional<Membership> activate(long membershipId, BigDecimal balance) throws DataAccessException, SQLException, MissingElementException;

    Long create(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException;
}
