package com.odigeo.membership.member.discount.rule;

import com.google.inject.Singleton;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;

import java.util.function.BiPredicate;

@Singleton
public class UserRules extends ApplyDiscountRules {

    private final BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> predicate;

    public UserRules() {
        this.predicate = isUserWithActiveMembership();
    }

    @Override
    protected BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> getPredicate() {
        return predicate;
    }

    @Override
    protected String getRuleId() {
        return RULES_USER;
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> isUserWithActiveMembership() {
        return (parameters, context) -> {
            boolean result = findMembershipsEligible(context).findAny().isPresent();
            if (!result) {
                addError(context, NotApplicationReason.MEMBERSHIP_UNKNOWN);
            }
            return result;
        };
    }

}
