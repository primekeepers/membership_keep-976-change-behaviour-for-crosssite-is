package com.odigeo.membership.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.testng.Assert.assertEquals;

public class JsonDateSerializerAndDeserializerTest {

    private static final String DATE_TIME_STRING = "1991-02-08 12:30";
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter
        .ofPattern(DATE_TIME_PATTERN);

    private ObjectMapper mapper;
    private JsonDateDeserializer deserializer;

    @BeforeClass
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("custom");
        module.addSerializer(LocalDateTime.class, new JsonDateSerializer());
        mapper.registerModule(module);
        deserializer = new JsonDateDeserializer();
    }

    @Test
    public void testSerializerAndDeserializerFlow() throws Exception {
        LocalDateTime dateTime = LocalDateTime.parse(DATE_TIME_STRING, FORMATTER);
        String dateString = mapper.writeValueAsString(dateTime);
        LocalDateTime deserializedDateTime = deserializeNumber(dateString);
        assertEquals(deserializedDateTime, dateTime);
    }

    private LocalDateTime deserializeNumber(String json) throws Exception {
        InputStream stream = new ByteArrayInputStream(json.getBytes());
        JsonParser parser = mapper.getJsonFactory().createJsonParser(stream);
        return deserializer.deserialize(parser, null);
    }
}