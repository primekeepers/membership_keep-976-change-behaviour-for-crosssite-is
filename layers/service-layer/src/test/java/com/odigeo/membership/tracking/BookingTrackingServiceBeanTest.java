package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.MembershipStore;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingTrackingServiceBeanTest {

    private static final String SITE_ES = "ES";
    private static final long BOOKING_ID = 123L;
    private static final long MEMBERSHIP_ID = 101L;
    private static final SQLException SQL_EXCEPTION = new SQLException();

    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private Membership membershipMock;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private BookingTrackingStore bookingTrackingStore;

    @InjectMocks
    private BookingTrackingServiceBean bookingTrackingServiceBean = new BookingTrackingServiceBean();

    @BeforeMethod
    public void setUp() throws Exception {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        setupMocks();
    }

    private void configure(Binder binder) {
        binder.bind(BookingTrackingStore.class).toInstance(bookingTrackingStore);
        binder.bind(MembershipStore.class).toInstance(membershipStore);
    }

    private void setupMocks() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        setUpMembershipMock();
    }

    private void setUpMembershipMock() {
        when(membershipMock.getId()).thenReturn(MEMBERSHIP_ID);
        when(membershipMock.getBalance()).thenReturn(BigDecimal.TEN);
        when(membershipMock.getStatus()).thenReturn(MemberStatus.ACTIVATED);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.ENABLED);
        when(membershipMock.getActivationDate()).thenReturn(LocalDateTime.now());
        when(membershipMock.getExpirationDate()).thenReturn(LocalDateTime.now().plusYears(1L));
        when(membershipMock.getWebsite()).thenReturn(SITE_ES);
    }

    private void mockFetchMembership(Membership expectedMembership) throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(expectedMembership);
    }

    private BookingTracking createBookingTracking(BigDecimal avoidFeeAmount, BigDecimal costFeeAmount) {
        BookingTracking bookingTracking = new BookingTracking();
        bookingTracking.setMembershipId(MEMBERSHIP_ID);
        bookingTracking.setBookingId(BOOKING_ID);
        bookingTracking.setCostFeeAmount(costFeeAmount);
        bookingTracking.setAvoidFeeAmount(avoidFeeAmount);
        bookingTracking.setPerksAmount(BigDecimal.ZERO);
        return bookingTracking;
    }

    @Test
    public void testIsBookingLimitReachedTrue() throws DataAccessException, SQLException {
        setIsBookingLimitReached(true);
        assertTrue(bookingTrackingServiceBean.isBookingLimitReached(membershipMock.getId()));
        verify(bookingTrackingStore).isBookingLimitReached(dataSource, membershipMock.getId());
    }

    @Test
    public void testIsBookingLimitReachedFalse() throws DataAccessException, SQLException {
        setIsBookingLimitReached(false);
        assertFalse(bookingTrackingServiceBean.isBookingLimitReached(membershipMock.getId()));
        verify(bookingTrackingStore).isBookingLimitReached(dataSource, membershipMock.getId());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testIsBookingLimitReachedException() throws Exception {
        setExceptionsTrackingStore();
        bookingTrackingServiceBean.isBookingLimitReached(membershipMock.getId());
    }

    private void setIsBookingLimitReached(final boolean limitReached) throws SQLException {
        when(bookingTrackingStore.isBookingLimitReached(any(), anyLong())).thenReturn(limitReached);
    }

    private void setExceptionsTrackingStore() throws SQLException {
        when(bookingTrackingStore.trackBooking(any(DataSource.class), any(BookingTracking.class))).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.removeTrackedBooking(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.getBookingTracked(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.isBookingTracked(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.isBookingLimitReached(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
    }

    @Test
    public void testGetMembershipBookingTracking() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        BookingTracking membershipBookingTracking = bookingTrackingServiceBean.getMembershipBookingTracking(BOOKING_ID);
        verify(bookingTrackingStore).getBookingTracked(eq(dataSource), eq(BOOKING_ID));
        assertEquals(membershipBookingTracking, bookingTracking);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipBookingTrackingException() throws SQLException, DataAccessException {
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenThrow(SQL_EXCEPTION);
        bookingTrackingServiceBean.getMembershipBookingTracking(BOOKING_ID);
    }

    @Test
    public void testInsertMembershipBookingTracking() throws SQLException, DataAccessException {
        mockFetchMembership(membershipMock);
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenReturn(Boolean.TRUE);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        BookingTracking bookingTrackingSaved = bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
        assertEquals(bookingTrackingSaved, bookingTracking);
        verify(bookingTrackingStore).trackBooking(eq(dataSource), eq(bookingTracking));
        verify(bookingTrackingStore).getBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(membershipStore).updateMembershipBalance(eq(dataSource), eq(bookingTracking.getMembershipId()), eq(BigDecimal.valueOf(11L)));
    }

    @Test
    public void testInsertMembershipBookingTrackingAvoidZero() throws SQLException, DataAccessException {
        mockFetchMembership(membershipMock);
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ZERO, BigDecimal.TEN);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenReturn(Boolean.TRUE);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        BookingTracking bookingTrackingSaved = bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
        assertEquals(bookingTrackingSaved, bookingTracking);
        verify(bookingTrackingStore).trackBooking(eq(dataSource), eq(bookingTracking));
        verify(bookingTrackingStore).getBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(membershipStore, never()).updateMembershipBalance(eq(dataSource), anyLong(), any(BigDecimal.class));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testInsertMembershipBookingTrackingMembershipNotFound() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenReturn(Boolean.TRUE);
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(bookingTracking.getMembershipId()))).thenThrow(new DataNotFoundException(StringUtils.EMPTY));
        bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testInsertMembershipBookingTrackingSQLException() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenThrow(SQL_EXCEPTION);
        bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
    }

    @Test
    public void testDeleteNotExistentMembershipBookingTracking() throws SQLException, DataAccessException {
        mockFetchMembership(membershipMock);
        bookingTrackingServiceBean.deleteMembershipBookingTracking(BOOKING_ID);
        verify(membershipStore, never()).updateMembershipBalance(eq(dataSource), anyLong(), any(BigDecimal.class));
        verify(bookingTrackingStore, never()).removeTrackedBooking(eq(dataSource), eq(BOOKING_ID));
    }

    @Test
    public void testDeleteMembershipBookingTracking() throws SQLException, DataAccessException {
        mockFetchMembership(membershipMock);
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(bookingTracking.getBookingId()))).thenReturn(Boolean.TRUE);
        bookingTrackingServiceBean.deleteMembershipBookingTracking(BOOKING_ID);
        BigDecimal expectedBalance = membershipMock.getBalance().subtract(bookingTracking.getAvoidFeeAmount());
        verify(membershipStore).updateMembershipBalance(eq(dataSource), eq(bookingTracking.getMembershipId()), eq(expectedBalance));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testDeleteMembershipBookingTrackingSQLException() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(BOOKING_ID))).thenThrow(SQL_EXCEPTION);
        bookingTrackingServiceBean.deleteMembershipBookingTracking(BOOKING_ID);
    }
}
