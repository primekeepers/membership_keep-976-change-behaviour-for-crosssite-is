package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;

@FunctionalInterface
public interface MembershipUpdateOperation {

    boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException;

}
