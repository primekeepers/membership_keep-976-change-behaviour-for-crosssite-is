package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.parameters.MembershipCreation;

import javax.sql.DataSource;

public interface MembershipCreationServiceProvider {

    Long createMembership(DataSource dataSource, MembershipCreation membershipCreation)
            throws MissingElementException, DataAccessException;
}
