package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.mocks.userapi.UserApiWorld;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.HashCode;
import com.odigeo.userprofiles.api.v2.model.HashType;
import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@ScenarioScoped
public class UserApiSteps {

    private static final String USER_ID = "userId";
    private static final String EMAIL = "email";
    private static final String STATUS = "status";
    private static final String BRAND = "brand";
    private static final String TOKEN = "token";
    private static final String TOKEN_TYPE = "tokenType";
    private static final String NULL_DATA = "null";

    private final UserApiWorld userApiWorld;

    @Inject
    public UserApiSteps(UserApiWorld userApiWorld) {
        this.userApiWorld = userApiWorld;
    }

    @Given("^the user-api has the following user info$")
    public void givenUserBasicInfo(DataTable dataTable) {
        List<Map<String, String>> inputValues = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> row : inputValues) {
            UserBasicInfo userBasicInfo = new UserBasicInfo();
            String id = row.get(USER_ID);
            if (!NULL_DATA.equals(id)) {
                userBasicInfo.setId(Long.valueOf(id));
            }
            userBasicInfo.setEmail(row.get(EMAIL));
            userBasicInfo.setStatus(Status.valueOf(row.get(STATUS).toUpperCase(Locale.getDefault())));
            userBasicInfo.setBrand(Brand.valueOf(row.get(BRAND).toUpperCase(Locale.getDefault())));
            userApiWorld.getUserApiInternalMock().addMockUserBasicInfo(userBasicInfo);

            String token = row.get(TOKEN);
            if (NULL_DATA.equalsIgnoreCase(token)) {
                userApiWorld.getUserApiInternalMock().addMockHashCodeToUser(null, userBasicInfo.getId());
            } else {
                HashCode hashCode = new HashCode();
                hashCode.setCode(token);
                hashCode.setHashType(HashType.valueOf(row.get(TOKEN_TYPE).toUpperCase(Locale.getDefault())));
                userApiWorld.getUserApiInternalMock().addMockHashCodeToUser(hashCode, userBasicInfo.getId());
            }
        }
    }
}
