package com.odigeo.membership.configuration;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.naming.NamingException;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertNull;


public class JeeServiceProviderTest extends AbstractContextFactory {


    protected ServiceNameResolver serviceNameResolver;

    @BeforeClass
    public void before() throws NamingException {
        super.before();
        openMocks(this);
        ConfigurationEngine.init();
        serviceNameResolver = ServiceNameResolver.getInstance();
        when(context.lookup(JeeServiceLocator.JNDI_APP_NAME)).thenReturn(JeeServiceLocatorTest.MY_APP_JNDI_NAME);
    }

    @Test(enabled = false)
    public void testRetrieveNonExistingService() throws UnavailableServiceException {
        assertNull(JeeServiceProvider.getInstance(String.class).get());
    }

}
