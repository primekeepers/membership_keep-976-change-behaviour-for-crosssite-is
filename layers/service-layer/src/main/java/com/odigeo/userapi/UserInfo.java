package com.odigeo.userapi;

import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;

import java.util.Optional;

public final class UserInfo {
    private final Long userId;
    private final String email;
    private final boolean shouldSetPassword;

    private UserInfo() {
        userId = null;
        email = null;
        shouldSetPassword = false;
    }

    private UserInfo(UserBasicInfo userBasicInfo) {
        shouldSetPassword = shouldSetPassword(userBasicInfo);
        email = userBasicInfo.getEmail();
        userId = userBasicInfo.getId();
    }

    public static UserInfo fromUserBasicInfo(UserBasicInfo userBasicInfo) {
        return new UserInfo(userBasicInfo);
    }

    public static UserInfo defaultUserInfo() {
        return new UserInfo();
    }

    public Optional<Long> getUserId() {
        return Optional.ofNullable(userId);
    }

    public String getEmail() {
        return email;
    }

    public boolean shouldSetPassword() {
        return shouldSetPassword;
    }

    private boolean shouldSetPassword(UserBasicInfo userInfo) {
        return Status.PENDING_LOGIN.equals(userInfo.getStatus());
    }
}
