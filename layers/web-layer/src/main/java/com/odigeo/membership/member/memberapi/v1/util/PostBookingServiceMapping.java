package com.odigeo.membership.member.memberapi.v1.util;

import com.google.common.annotations.VisibleForTesting;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.exception.MembershipForbiddenException;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static java.util.Objects.nonNull;

public final class PostBookingServiceMapping {
    @VisibleForTesting
    static final String EMAIL = "EMAIL";
    @VisibleForTesting
    static final String PHONE = "PHONE";
    @VisibleForTesting
    static final String ANDROID = "ANDROID";
    @VisibleForTesting
    static final String IOS = "IOS";
    private static final long DEFAULT_MONTHS_TO_RENEWAL = 6;

    private PostBookingServiceMapping() {
    }

    public static MembershipCreation getMembershipCreationFromPostBookingRequest(CreateMembershipSubscriptionRequest createMembershipRequest) {
        validateMembershipRequestParameters(createMembershipRequest);
        setMembershipTypeIfNull(createMembershipRequest);
        MembershipCreation membershipCreation = getMembershipCreationFromPostBookingRequest(createMembershipRequest, MemberStatus.ACTIVATED);
        setExpirationDate(membershipCreation, createMembershipRequest.getMonthsToRenewal());
        return membershipCreation;
    }

    public static MembershipCreation getMembershipPendingCreationFromPostBookingRequest(CreateMembershipSubscriptionRequest createMembershipRequest) throws InvalidParametersException {
        validateMonthsToRenewalIsInformed(createMembershipRequest.getMonthsToRenewal());
        validateMembershipRequestParameters(createMembershipRequest);
        setMembershipTypeIfNull(createMembershipRequest);
        return getMembershipCreationFromPostBookingRequest(createMembershipRequest, MemberStatus.PENDING_TO_ACTIVATE);
    }

    private static MembershipCreation getMembershipCreationFromPostBookingRequest(CreateMembershipSubscriptionRequest createMembershipRequest, MemberStatus memberStatus) {
        String monthsToRenewal = createMembershipRequest.getMonthsToRenewal();
        String userId = createMembershipRequest.getUserId();

        MembershipCreation membershipCreation = new MembershipCreationBuilder()
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                        .userId(Long.parseLong(userId))
                        .name(createMembershipRequest.getName())
                        .lastNames(createMembershipRequest.getLastNames()))
                .withWebsite(createMembershipRequest.getWebsite())
                .withBalance(createMembershipRequest.getBalance())
                .withMembershipType(MembershipType.valueOf(createMembershipRequest.getMembershipType()))
                .withSourceType(SourceType.POST_BOOKING)
                .withMonthsDuration(monthsToRenewal)
                .withMemberStatus(memberStatus)
                .withRenewalDuration(createMembershipRequest.getRenewalDuration())
                .withSubscriptionPrice(createMembershipRequest.getBalance())
                .withRenewalPrice(createMembershipRequest.getRenewalPrice())
                .withCurrencyCode(createMembershipRequest.getCurrencyCode())
                .build();
        setStatusAction(membershipCreation, createMembershipRequest.getChannel());
        return membershipCreation;
    }

    private static void validateMonthsToRenewalIsInformed(String monthsToRenewal) {
        if (StringUtils.isBlank(monthsToRenewal)) {
            throw new MembershipBadRequestException("Months to renewal must be informed");
        }
    }

    static void validateMembershipRequestParameters(CreateMembershipSubscriptionRequest createMembershipRequest) {
        String monthsToRenewal = createMembershipRequest.getMonthsToRenewal();
        if (StringUtils.isNotEmpty(monthsToRenewal)) {
            if (!StringUtils.isNumeric(monthsToRenewal)) {
                throw new MembershipBadRequestException("Number of months to renewal with non numerical format");
            }
            if (Integer.parseInt(monthsToRenewal) < 1) {
                throw new MembershipBadRequestException("Months to renewal cannot be 0 or negative");
            }
        }
        String userId = createMembershipRequest.getUserId();
        if (StringUtils.isBlank(userId) || !StringUtils.isNumeric(userId)) {
            throw new MembershipBadRequestException("User id missing or with non numerical format");
        }
        BigDecimal balance = createMembershipRequest.getBalance();
        if (nonNull(balance) && balance.signum() == -1) {
            throw new MembershipBadRequestException("Balance can't be negative");
        }
    }

    private static void setMembershipTypeIfNull(CreateMembershipSubscriptionRequest createMembershipRequest) {
        if (StringUtils.isEmpty(createMembershipRequest.getMembershipType())) {
            createMembershipRequest.setMembershipType(MembershipType.BASIC.toString());
        }
    }

    private static void setStatusAction(MembershipCreation membershipCreation, String channel) {
        if (channel == null) {
            return;
        }

        switch (channel) {
        case EMAIL:
            membershipCreation.setStatusAction(StatusAction.PB_EMAIL_CREATION);
            break;
        case PHONE:
            membershipCreation.setStatusAction(StatusAction.PB_PHONE_CREATION);
            break;
        case ANDROID:
            membershipCreation.setStatusAction(StatusAction.PB_ANDROID_CREATION);
            break;
        case IOS:
            membershipCreation.setStatusAction(StatusAction.PB_IOS_CREATION);
            break;
        default:
            break;
        }
    }

    private static void setExpirationDate(MembershipCreation membershipCreation, String monthsToRenewal) {
        if (StringUtils.isBlank(monthsToRenewal)) {
            membershipCreation.setExpirationDate(LocalDateTime.now().plusMonths(DEFAULT_MONTHS_TO_RENEWAL));
        } else if (StringUtils.isNumeric(monthsToRenewal)) {
            int numMonths = Integer.parseInt(monthsToRenewal);
            if (numMonths > DEFAULT_MONTHS_TO_RENEWAL) {
                throw new MembershipForbiddenException("Maximum number of months to renewal exceeded");
            } else if (numMonths <= 0) {
                throw new MembershipForbiddenException("Months to renewal cannot be 0 or negative");
            }
            membershipCreation.setExpirationDate(LocalDateTime.now().plusMonths(numMonths));
        }
    }
}
