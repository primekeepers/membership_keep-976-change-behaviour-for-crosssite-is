package com.odigeo.messaging.kafka;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.messaging.MembershipProductClosureService;
import com.odigeo.shoppingbasket.checkout.messages.ProductClosureRequested;
import com.odigeo.shoppingbasket.checkout.messages.ProductReference;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipProductClosureProcessorTest {

    private static final String MEMBERSHIP_PRODUCT = "MEMBERSHIP";
    private static final String NON_MEMBERSHIP_PRODUCT = "OTHER";

    @Mock
    private MembershipProductClosureService controller;
    @Mock
    private DomainEvent<ProductClosureRequested> domainEvent;
    @Mock
    private ProductClosureRequested productClosureRequested;
    @Mock
    private ProductReference productReference;

    private MembershipProductClosureProcessor membershipProductClosureProcessor;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(MembershipProductClosureService.class).toInstance(controller));
        membershipProductClosureProcessor = new MembershipProductClosureProcessor(controller);
    }

    @Test
    public void testOnMessageForMembershipProduct() throws DataAccessException, MissingElementException {
        when(domainEvent.getPayload()).thenReturn(productClosureRequested);
        when(productClosureRequested.getProductReference()).thenReturn(productReference);
        when(productReference.getType()).thenReturn(MEMBERSHIP_PRODUCT);

        membershipProductClosureProcessor.onMessage(domainEvent);

        verify(controller).closeMembership(eq(productClosureRequested));
    }

    @Test
    public void testOnMessageForNonMembershipProduct() throws DataAccessException, MissingElementException {
        when(domainEvent.getPayload()).thenReturn(productClosureRequested);
        when(productClosureRequested.getProductReference()).thenReturn(productReference);
        when(productReference.getType()).thenReturn(NON_MEMBERSHIP_PRODUCT);

        membershipProductClosureProcessor.onMessage(domainEvent);

        verify(controller, never()).closeMembership(any());
    }
}