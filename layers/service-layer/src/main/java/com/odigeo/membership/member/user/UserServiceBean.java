package com.odigeo.membership.member.user;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.BrandMapper;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserDTO;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v2.UserApiService;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.requests.ThirdAppRequest;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.AppCredentials;
import com.odigeo.userprofiles.api.v2.model.responses.UserRegisteredResponse;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidFindException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.StatusUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;

@Singleton
public class UserServiceBean implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceBean.class);
    private static final String PROBLEM_CHECKING_IS_USER_REGISTERED = "Problem checking isUserRegistered {}";
    private UserApiService userApiService;
    private UserProfileApiSecurityConfiguration userApiSecurityConfig;
    private AppCredentials appCredentials;
    private UserApiManager userApiManager;

    @Override
    public Long saveUser(UserCreation userCreation) throws DataAccessException {
        LOGGER.info("saveUser {}", userCreation);
        UserRegisteredResponse registeredUserInfo = getRegisteredUser(userCreation);
        if (registeredUserInfo.isRegistered()) {
            LOGGER.info("User with id {} and status {} already exists", registeredUserInfo.getId(), registeredUserInfo.getUserStatus());
            return registeredUserInfo.getId();
        }
        User user = UserCreationMapper.mapToUser(userCreation);
        return createUserInUserApiService(user);
    }

    @Override
    public Long saveUser(UserDTO userDTO) throws DataAccessException {
        User newUser = UserCloningMapper.mapToUser(userDTO);
        return createUserInUserApiService(newUser);
    }

    @Override
    public UserDTO getUser(long userId) {
        return getUserApiManager().getUser(userId).orElse(null);
    }

    @Override
    public Optional<Long> fixPendingMailConfirmationUserAccount(long userId) throws UserApiException, DataAccessException {
        UserDTO userDTO = getUser(userId);
        if (!Status.PENDING_MAIL_CONFIRMATION.equals(userDTO.getStatus())) {
            return Optional.empty();
        }
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.FIX_USER_ACCOUNT_STATUS_ONE_ATTEMPT), MetricsNames.METRICS_REGISTRY_NAME);
        if (!deletePendingMailConfirmationUserAccount(userDTO.getEmail(), userDTO.getBrand())) {
            throw new UserApiException("Could not delete user with user Id: " + userDTO.getId());
        }
        userDTO.setStatus(Status.PENDING_LOGIN);
        userDTO.setId(null);
        @SuppressWarnings("PMD.PrematureDeclaration")
        Long newUserId = saveUser(userDTO);
        return Optional.of(newUserId);
    }

    private long createUserInUserApiService(User user) throws DataAccessException {
        try {
            User createdUser = getUserApiService().saveUser(user);
            LOGGER.info("createdUser with id {}", createdUser.getId());
            return createdUser.getId();
        } catch (InvalidCredentialsException | RemoteException | StatusUserException | InvalidValidationException e) {
            LOGGER.error("Problem occurred saving user {}", user, e);
            throw new DataAccessException("Problem occurred saving user", e);
        }
    }

    private boolean deletePendingMailConfirmationUserAccount(String userEmail, Brand userBrand) {
        return getUserApiManager().deletePendingMailConfirmationUserAccount(userEmail, userBrand);
    }

    @Override
    public Long getUserIdBy(String email, String website) throws UserAccountNotFoundException, DataAccessException {
        UserRegisteredResponse userRegisteredResponse;
        try {
            userRegisteredResponse = getRegisteredUser(new UserCreation.Builder().withEmail(email).withWebsite(website).build());
        } catch (DataAccessException e) {
            LOGGER.error(PROBLEM_CHECKING_IS_USER_REGISTERED, email, e);
            throw new DataAccessException("Could not get UserRegisteredResponse", e);
        }

        if (!userRegisteredResponse.isRegistered() || userRegisteredResponse.getUserStatus().equals(Status.DELETED)) {
            throw new UserAccountNotFoundException("User account was not found with email: " + email + " and website: " + website);
        }

        return userRegisteredResponse.getId();
    }

    private UserRegisteredResponse getRegisteredUser(UserCreation userCreation) throws DataAccessException {
        ThirdAppRequest thirdAppRequest = new ThirdAppRequest();
        thirdAppRequest.setAppCredentials(getAppCredentials());
        thirdAppRequest.setBrand(BrandMapper.map(userCreation.getWebsite()).orElse(null));
        thirdAppRequest.setEmail(userCreation.getEmail());
        try {
            return getUserApiService().isUserRegistered(thirdAppRequest);
        } catch (InvalidValidationException | RemoteException | InvalidFindException | InvalidCredentialsException e) {
            LOGGER.error(PROBLEM_CHECKING_IS_USER_REGISTERED, userCreation, e);
            throw new DataAccessException("Problem checking isUserRegistered", e);
        }
    }

    private AppCredentials getAppCredentials() {
        if (Objects.isNull(appCredentials)) {
            appCredentials = new AppCredentials();
            appCredentials.setApplicationName(getUserApiSecurityConfig().getUser());
            appCredentials.setPassword(getUserApiSecurityConfig().getPassword());
        }
        return appCredentials;
    }

    private UserProfileApiSecurityConfiguration getUserApiSecurityConfig() {
        if (Objects.isNull(userApiSecurityConfig)) {
            userApiSecurityConfig = ConfigurationEngine.getInstance(UserProfileApiSecurityConfiguration.class);
        }
        return userApiSecurityConfig;
    }

    private UserApiManager getUserApiManager() {
        return Optional.ofNullable(userApiManager).orElseGet(() -> {
            userApiManager = ConfigurationEngine.getInstance(UserApiManager.class);
            return userApiManager;
        });
    }

    private UserApiService getUserApiService() {
        if (Objects.isNull(userApiService)) {
            userApiService = ConfigurationEngine.getInstance(UserApiService.class);
        }
        return userApiService;
    }
}
