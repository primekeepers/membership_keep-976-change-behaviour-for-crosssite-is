package com.odigeo.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.v4.messages.MembershipRenewal;
import com.odigeo.membership.v4.messages.MembershipType;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Singleton
class MembershipSubscriptionMessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipSubscriptionMessageService.class);

    private final UserApiManager userApiManager;
    private final MembershipKafkaSender membershipKafkaSender;
    private final MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;

    @Inject
    MembershipSubscriptionMessageService(UserApiManager userApiManager, MembershipKafkaSender membershipKafkaSender,
                                         MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper) {
        this.userApiManager = userApiManager;
        this.membershipKafkaSender = membershipKafkaSender;
        this.memberStatusToCrmStatusMapper = memberStatusToCrmStatusMapper;
    }

    void sendSubscriptionMessageToCRMTopicByRule(MemberStatus previousStatus, MemberStatus newStatus, Membership membership) {
        if (previousStatus.isNot(MemberStatus.PENDING_TO_ACTIVATE) && nonNull(membership.getExpirationDate())) {
            SubscriptionStatus subscriptionStatus = memberStatusToCrmStatusMapper.mapForUpdatedMembership(membership, newStatus);
            sendSubscriptionMessageToCRMTopic(membership, subscriptionStatus);
        }
    }

    boolean sendSubscriptionMessageToCRMTopic(Membership membership, SubscriptionStatus subscriptionStatus) {
        try {
            final UserInfo userInfo = getUserInfo(membership);
            final MembershipSubscriptionMessage membershipSubscriptionMessage = buildMembershipSubscriptionMessageFromMembership(membership, userInfo, subscriptionStatus);
            membershipKafkaSender.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
            return true;
        } catch (DataNotFoundException | PublishMessageException e) {
            LOGGER.warn("Impossible to send subscription message with membershipId {}: {}", membership.getId(), e.getMessage());
            return false;
        }
    }

    void sendSubscriptionMessageToCRMTopic(UserInfo userInfo, Membership membership, SubscriptionStatus subscriptionStatus) {
        try {
            final MembershipSubscriptionMessage membershipSubscriptionMessage = buildMembershipSubscriptionMessageFromMembership(membership, userInfo, subscriptionStatus);
            membershipKafkaSender.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
        } catch (DataNotFoundException | PublishMessageException e) {
            LOGGER.warn("Impossible to send subscription message with membershipId {}: {}", membership.getId(), e.getMessage());
        }
    }

    void sendSubscriptionMessageToCRMTopic(String email, MembershipCreation membershipCreation, SubscriptionStatus subscriptionStatus) {
        final UserInfo userInfo = userApiManager.getUserInfo(email, membershipCreation.getWebsite());
        final MembershipSubscriptionMessage membershipSubscriptionMessage = buildMembershipSubscriptionMessageFromMembershipCreation(membershipCreation, email, userInfo, subscriptionStatus);

        try {
            membershipKafkaSender.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
        } catch (PublishMessageException e) {
            LOGGER.error("Error publishing MembershipSubscriptionMessage");
        }
    }

    private MembershipSubscriptionMessage buildMembershipSubscriptionMessageFromMembership(Membership membership, UserInfo userInfo,
                                                                                           SubscriptionStatus subscriptionStatus) throws DataNotFoundException {
        String email = Optional.ofNullable(userInfo.getEmail())
                .orElseThrow(() -> new DataNotFoundException("User api returned email null for membershipId " + membership.getId()));
        LocalDate activationDate = Optional.ofNullable(membership.getActivationDate()).map(LocalDateTime::toLocalDate)
                .orElse(membership.getExpirationDate().toLocalDate().minusMonths(membership.getMonthsDuration()));
        MembershipSubscriptionMessage.Builder subscriptionMessageBuilder = new MembershipSubscriptionMessage.Builder(email, membership.getWebsite())
                .withDates(activationDate, membership.getExpirationDate().toLocalDate())
                .withSubscriptionStatus(subscriptionStatus)
                .withShouldSetPassword(userInfo.shouldSetPassword())
                .withRemindMeLater(membership.getRemindMeLater())
                .withMembershipId(membership.getId())
                .withMemberAccountId(membership.getMemberAccountId())
                .withMembershipType(MembershipType.valueOf(membership.getMembershipType().toString()))
                .withMembershipDuration(membership.getDuration())
                .withMembershipPrice(membership.getTotalPrice())
                .withMembershipCurrency(membership.getCurrencyCode())
                .withAutoRenewalStatus(membership.getAutoRenewal() != null ? MembershipRenewal.valueOf(membership.getAutoRenewal().toString()) : null);

        addForgetPasswordTokenToSubscriptionMessage(userInfo, subscriptionMessageBuilder);
        return subscriptionMessageBuilder.build();
    }

    private MembershipSubscriptionMessage buildMembershipSubscriptionMessageFromMembershipCreation(MembershipCreation membershipCreation,
                                                                                                   String email, UserInfo userInfo,
                                                                                                   SubscriptionStatus subscriptionStatus) {
        MembershipSubscriptionMessage.Builder subscriptionMessageBuilder = new MembershipSubscriptionMessage.Builder(email, membershipCreation.getWebsite())
                .withDates(membershipCreation.getActivationDate().toLocalDate(), membershipCreation.getExpirationDate().toLocalDate())
                .withSubscriptionStatus(subscriptionStatus)
                .withShouldSetPassword(userInfo.shouldSetPassword());

        addForgetPasswordTokenToSubscriptionMessage(userInfo, subscriptionMessageBuilder);
        return subscriptionMessageBuilder.build();
    }

    private UserInfo getUserInfo(Membership membership) throws DataNotFoundException {
        long userId = Optional.ofNullable(membership.getMemberAccount()).map(MemberAccount::getUserId)
                .orElseThrow(() -> new DataNotFoundException("Can't generate subscription message for membershipId " + membership.getId() + ": MemberAccount is null"));
        return userApiManager.getUserInfo(userId);
    }

    private void addForgetPasswordTokenToSubscriptionMessage(UserInfo userInfo, MembershipSubscriptionMessage.Builder messageBuilder) {
        if (userInfo.getUserId().isPresent() && userInfo.shouldSetPassword()) {
            Optional<String> forgetPwdToken = userInfo.getUserId().flatMap(userApiManager::getForgetPasswordToken);
            if (forgetPwdToken.isPresent()) {
                messageBuilder.withForgetPasswordToken(forgetPwdToken.get());
            } else {
                messageBuilder.withShouldSetPassword(false);
                LOGGER.error("Error: expected ForgetPasswordToken but retrieved null for userId: {}", userInfo.getUserId());
            }
        }
    }
}
