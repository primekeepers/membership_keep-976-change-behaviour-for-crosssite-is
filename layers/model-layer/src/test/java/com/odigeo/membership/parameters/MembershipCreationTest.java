package com.odigeo.membership.parameters;

import bean.test.BeanTest;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.AbstractConverter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class MembershipCreationTest extends BeanTest<MembershipCreation> {

    private static final Long USER_ID = 1L;
    private static final String NAME = "Yukihiro";
    private static final String LAST_NAME = "Matsumoto";
    private static final String WEBSITE = "IT";
    private static final MemberStatus MEMBER_STATUS = MemberStatus.ACTIVATED;
    private static final MembershipRenewal AUTO_RENEWAL = MembershipRenewal.ENABLED;
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now();
    private static final StatusAction STATUS_ACTION = StatusAction.CREATION;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final String MONTHS_DURATION = "6";
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final BigDecimal RENEWAL_PRICE = new BigDecimal("7.99");
    private static final String CURRENCY_CODE = "EUR";
    private static final Long MEMBER_ACCOUNT_ID = 123L;
    private static final String LOCALE = "es_ES";
    private static final String EMAIL = "prime@mail.com";
    private static final int RENEWAL_DURATION = 12;
    private static final Long FEE_CONTAINER_ID = 22L;

    @Override
    protected MembershipCreation getBean() {
        ConvertUtils.register(new UUIDConverter(null), UUID.class);

        return new MembershipCreationBuilder()
                .withActivationDate(ACTIVATION_DATE)
                .withExpirationDate(EXPIRATION_DATE)
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                        .lastNames(LAST_NAME)
                        .name(NAME)
                        .userId(USER_ID))
                .withMembershipType(MEMBERSHIP_TYPE)
                .withWebsite(WEBSITE)
                .withMemberStatus(MEMBER_STATUS)
                .withAutoRenewal(AUTO_RENEWAL)
                .withBalance(BALANCE)
                .withStatusAction(STATUS_ACTION)
                .withMonthsDuration(MONTHS_DURATION)
                .withSourceType(SOURCE_TYPE)
                .withSubscriptionPrice(SUBSCRIPTION_PRICE)
                .withRenewalPrice(RENEWAL_PRICE)
                .withRenewalDuration(RENEWAL_DURATION)
                .withCurrencyCode(CURRENCY_CODE)
                .withMemberAccountId(MEMBER_ACCOUNT_ID)
                .withFeeContainerId(FEE_CONTAINER_ID)
                .withUserCreation(new UserCreation.Builder()
                        .withEmail(EMAIL)
                        .withLocale(LOCALE)
                        .withTrafficInterfaceId(1)
                        .build())
                .build();
    }

    static class UUIDConverter extends AbstractConverter {

        protected UUIDConverter(Object defaultValue) {
            super(defaultValue);
        }

        @Override
        public Object convert(Class type, Object value) {
            if (UUID.class.equals(type)) {
                return value == null ? null : type.cast(UUID.fromString(String.valueOf(value)));
            }

            throw new ConversionException("Invalid type: " + type + " or value: " + value);
        }

        @Override
        protected Object convertToType(Class type, Object value) throws Throwable {
            return convert(type, value);
        }

        @Override
        protected Class getDefaultType() {
            return UUID.class;
        }
    }
}
