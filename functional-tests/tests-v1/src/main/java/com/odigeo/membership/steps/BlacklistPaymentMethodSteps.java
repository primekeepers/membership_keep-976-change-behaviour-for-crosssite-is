package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.functionals.membership.BlacklistPaymentMethodBuilder;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.util.DateConverter;
import com.odigeo.membership.world.BlacklistedPaymentMethodsWorld;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static  org.testng.Assert.assertEquals;
import static  org.testng.Assert.assertFalse;
import static  org.testng.Assert.assertTrue;

@ScenarioScoped
public class BlacklistPaymentMethodSteps extends CommonSteps {

    private final BlacklistedPaymentMethodsWorld world;

    @Inject
    public BlacklistPaymentMethodSteps(ServerConfiguration serverConfiguration, BlacklistedPaymentMethodsWorld world) {
        super(serverConfiguration);
        this.world = world;
    }

    @When("^requested getBlacklistedPaymentMethods with membershipId (\\d+)$")
    public void requestGetBlacklistedPaymentMethods(long membershipId) throws InvalidParametersException {
        world.setBlackListedPaymentMethods(membershipService.getBlacklistedPaymentMethods(membershipId));
    }

    @When("^called addToBlacklist payment methods for membershipId (\\d+)$")
    public void callAddToBlacklist(long membershipId, List<BlacklistPaymentMethodBuilder> paymentMethods) {
        assertTrue(membershipId > 0);
        assertFalse(paymentMethods.isEmpty());
        AddToBlackListRequest request = fillAddToBlackListRequest(paymentMethods);
        membershipService.addToBlackList(membershipId, request);
    }

    @Then("^the resulting blacklisted payment methods list contains (\\d+) items$")
    public void blacklistedPaymentMethodListHasSize(long listSize) throws InvalidParametersException {
        assertEquals(world.getBlackListedPaymentMethods().size(), listSize);
    }

    private AddToBlackListRequest fillAddToBlackListRequest(List<BlacklistPaymentMethodBuilder> paymentMethods) {
        List<BlackListedPaymentMethod> blacklistedPaymentMethods = new ArrayList<>();
        for (BlacklistPaymentMethodBuilder paymentMethod : paymentMethods) {
            BlackListedPaymentMethod item = new BlackListedPaymentMethod();
            item.setId(Long.valueOf(paymentMethod.getId()));
            item.setErrorMessage(paymentMethod.getErrorMessage());
            item.setErrorType(paymentMethod.getErrorType());
            Date date = DateConverter.toSqlDate(paymentMethod.getTimestamp());
            if (date != null) {
                item.setTimestamp(date);
            }
            blacklistedPaymentMethods.add(item);
        }
        AddToBlackListRequest request = new AddToBlackListRequest();
        request.setBlackListedPaymentMethods(blacklistedPaymentMethods);
        return request;
    }
}
