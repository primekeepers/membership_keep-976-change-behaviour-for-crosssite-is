package com.odigeo.messaging;

import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertTrue;

public class MembershipMessageSendingManagerTest {

    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long USER_ID = 343L;
    private static final String EMAIL = "user@odigeo.com";
    private static final String WEBSITE = "website";
    private static final Long BOOKING_ID = 200L;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = ACTIVATION_DATE.plusMonths(6);


    @Mock
    private Appender mockAppender;
    @Mock
    private MembershipSubscriptionMessageService membershipSubscriptionMessageServiceMock;
    @Mock
    private MembershipUpdateMessageService membershipUpdateMessageServiceMock;
    @Mock
    private MembershipMailerMessageService membershipMailerMessageServiceMock;

    private MembershipMessageSendingManager membershipMessageSendingManager;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        membershipMessageSendingManager = new MembershipMessageSendingManager(membershipSubscriptionMessageServiceMock,
                membershipUpdateMessageServiceMock, membershipMailerMessageServiceMock);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicByRule() {
        final MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        final Membership expiredMembership = new MembershipBuilder()
                .setMemberAccount(memberAccount)
                .build();

        membershipMessageSendingManager.sendSubscriptionMessageToCRMTopicByRule(MemberStatus.ACTIVATED, MemberStatus.EXPIRED, expiredMembership);
        verify(membershipSubscriptionMessageServiceMock).sendSubscriptionMessageToCRMTopicByRule(eq(MemberStatus.ACTIVATED), eq(MemberStatus.EXPIRED), eq(expiredMembership));
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithMembership() {
        final Membership membership = getMembership();
        when(membershipSubscriptionMessageServiceMock.sendSubscriptionMessageToCRMTopic(eq(membership), any(SubscriptionStatus.class))).thenReturn(true);

        boolean result = membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(membership, SubscriptionStatus.SUBSCRIBED);

        assertTrue(result);
        verify(membershipSubscriptionMessageServiceMock).sendSubscriptionMessageToCRMTopic(eq(membership), eq(SubscriptionStatus.SUBSCRIBED));
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfoAndMembership() {
        final UserInfo userInfo = getUserInfo();
        final Membership membership = getMembership();

        membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(userInfo, membership, SubscriptionStatus.SUBSCRIBED);

        verify(membershipSubscriptionMessageServiceMock).sendSubscriptionMessageToCRMTopic(eq(userInfo), eq(membership), eq(SubscriptionStatus.SUBSCRIBED));
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithEmailAndMembershipCreation() {
        final MembershipCreation membershipCreation = getMembershipCreation();

        membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(EMAIL, membershipCreation, SubscriptionStatus.SUBSCRIBED);

        verify(membershipSubscriptionMessageServiceMock).sendSubscriptionMessageToCRMTopic(eq(EMAIL), eq(membershipCreation), eq(SubscriptionStatus.SUBSCRIBED));
    }

    @Test
    public void testSendMembershipIdToMembershipReporter() {
        membershipMessageSendingManager.sendMembershipIdToMembershipReporter(MEMBERSHIP_ID);
        verify(membershipUpdateMessageServiceMock).sendMembershipIdToMembershipReporter(MEMBERSHIP_ID);
    }

    @Test
    public void testSendWelcomeToPrimeMessageToMembershipTransactionalTopic() {
        final Membership membership = getMembership();

        membershipMessageSendingManager.sendWelcomeToPrimeMessageToMembershipTransactionalTopic(membership, BOOKING_ID, false);

        verify(membershipMailerMessageServiceMock).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(eq(membership), eq(BOOKING_ID), eq(false));
    }

    private Membership getMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        return new MembershipBuilder().setWebsite(WEBSITE)
                .setActivationDate(ACTIVATION_DATE).setExpirationDate(EXPIRATION_DATE)
                .setMemberAccount(memberAccount).build();
    }

    private UserInfo getUserInfo() {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setId(USER_ID);
        return UserInfo.fromUserBasicInfo(userBasicInfo);
    }

    private MembershipCreation getMembershipCreation() {
        return new MembershipCreationBuilder()
                .withWebsite(WEBSITE)
                .withActivationDate(ACTIVATION_DATE)
                .withExpirationDate(EXPIRATION_DATE)
                .build();
    }
}
