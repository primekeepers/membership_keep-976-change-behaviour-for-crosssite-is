package com.odigeo.membership.exception;

public class AlreadyHasPrimeMembershipException extends Exception {

    public AlreadyHasPrimeMembershipException(String message) {
        this(message, null);
    }

    public AlreadyHasPrimeMembershipException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
