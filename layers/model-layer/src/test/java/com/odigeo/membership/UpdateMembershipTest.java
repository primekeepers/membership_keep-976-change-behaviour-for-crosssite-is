package com.odigeo.membership;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class UpdateMembershipTest extends BeanTest<UpdateMembership> {

    @Override
    protected UpdateMembership getBean() {
        return UpdateMembership.builder()
                .build();
    }
    @Test
    public void testBuilderCopyOf(){
        assertEquals(getBean(), UpdateMembership.builderCloneOf(getBean()).build());
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(UpdateMembership.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}