package com.odigeo.membership.member.discount.rule;

import com.google.inject.Singleton;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.member.MemberServiceUtils;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.membership.parameters.TravellerParameter;

import java.util.Collection;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Singleton
public class TravellersRules extends ApplyDiscountRules {

    private final BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> predicate;

    public TravellersRules() {
        this.predicate = isUserPrimeATraveller();
    }

    @Override
    protected BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> getPredicate() {
        return predicate;
    }

    @Override
    protected String getRuleId() {
        return RULES_TRAVELLERS;
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> isUserPrimeATraveller() {
        return (parameters, context) -> {
            Collection<TravellerParameter> travellers = parameters.getTravellers();
            List<Membership> memberships = findMembershipsEligible(context).filter(travellersRule(travellers)).collect(Collectors.toList());

            if (memberships.isEmpty()) {
                addError(context, NotApplicationReason.TRAVELLERS_INVALID);
                return false;
            }

            updateMembershipsEligible(context, memberships);

            return true;
        };
    }

    private Predicate<Membership> travellersRule(Collection<TravellerParameter> travellers) {
        return membership -> MemberServiceUtils.isMemberOnList(membership.getMemberAccount(), travellers);
    }

}
