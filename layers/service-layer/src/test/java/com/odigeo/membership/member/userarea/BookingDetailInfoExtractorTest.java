package com.odigeo.membership.member.userarea;

import com.odigeo.bookingapi.v14.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.CollectionAttempt;
import com.odigeo.bookingapi.v14.responses.CollectionSummary;
import com.odigeo.bookingapi.v14.responses.CreditCard;
import com.odigeo.bookingapi.v14.responses.FeeContainer;
import com.odigeo.bookingapi.v14.responses.MembershipPerks;
import com.odigeo.bookingapi.v14.responses.Money;
import com.odigeo.bookingapi.v14.responses.Movement;
import com.odigeo.membership.PrimeBookingInformation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.odigeo.membership.member.userarea.MoneyOperation.buildMoney;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class BookingDetailInfoExtractorTest {

    private static final String PAID = "PAID";

    private BookingDetailInfoExtractor bookingDetailInfoExtractor;

    @BeforeMethod
    public void setUp() {
        bookingDetailInfoExtractor = new BookingDetailInfoExtractor();
    }

    @Test
    public void testSubscriptionPayMethodIsEmptyWhenBookingDetailHasNoCollectionSummary() {
        //Given
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setCollectionSummary(null);
        //When
        final Optional<CreditCard> subscriptionPaymentMethod = bookingDetailInfoExtractor.extractSubscriptionPaymentMethodFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethod, Optional.empty());
    }

    @Test
    public void testSubscriptionPayMethodIsEmptyWhenBookingDetailHasNoLastCreditCard() {
        //Given
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        collectionSummary.setLastCreditCard(null);
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final Optional<CreditCard> subscriptionPaymentMethod = bookingDetailInfoExtractor.extractSubscriptionPaymentMethodFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethod, Optional.empty());
    }

    @Test
    public void testSubscriptionPayMethodReturnsMaskedCardWhenBookingDetailHasLastCreditCard() {
        //Given
        String creditCardNumber = "12345678";
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        CreditCard lastCreditCard = new CreditCard();
        lastCreditCard.setCreditCardNumber(creditCardNumber);
        collectionSummary.setLastCreditCard(lastCreditCard);
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final Optional<CreditCard> subscriptionPaymentMethod = bookingDetailInfoExtractor.extractSubscriptionPaymentMethodFromBookingDetail().apply(bookingDetail);
        //Then
        assertTrue(subscriptionPaymentMethod.isPresent());
        assertEquals(subscriptionPaymentMethod.get().getCreditCardNumber(), "****5678");
    }

    @Test
    public void testPaymentMethodIsBlankWhenBookingDetailHasNoCollectionSummary() {
        //Given
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setCollectionSummary(null);
        //When
        final String subscriptionPaymentMethodType = bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethodType, EMPTY);
    }

    @Test
    public void testPaymentMethodIsBlankWhenBookingDetailHasEmptyAttempts() {
        //Given
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        collectionSummary.setAttempts(emptyList());
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final String subscriptionPaymentMethodType = bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethodType, EMPTY);
    }

    @Test
    public void testPaymentMethodIsBlankWhenBookingDetailHasNoValidMovements() {
        //Given
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        collectionAttempt.setValidMovements(emptyList());
        collectionSummary.setAttempts(singletonList(collectionAttempt));
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final String subscriptionPaymentMethodType = bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethodType, EMPTY);
    }

    @Test
    public void testPaymentMethodIsBlankWhenBookingDetailHasNoPaidValidMovements() {
        //Given
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        Movement movement = new Movement();
        movement.setStatus("NO PAID");
        collectionAttempt.setValidMovements(singletonList(movement));
        collectionSummary.setAttempts(singletonList(collectionAttempt));
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final String subscriptionPaymentMethodType = bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethodType, EMPTY);
    }

    @Test
    public void testPaymentMethodIsAttemptTypeWhenBookingDetailHasPaidValidMovement() {
        //Given
        final String expectedPaymentMethodType = "pay type";
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        collectionAttempt.setType(expectedPaymentMethodType);
        Movement movement = new Movement();
        movement.setStatus(PAID);
        collectionAttempt.setValidMovements(singletonList(movement));
        collectionSummary.setAttempts(singletonList(collectionAttempt));
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final String subscriptionPaymentMethodType = bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethodType, expectedPaymentMethodType);
    }

    @Test
    public void testPaymentMethodIsAttemptGatewayTypeWhenBookingDetailHasPaidValidMovementAndNoAttemptType() {
        //Given
        final String expectedPaymentMethodType = "gateway pay type";
        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        collectionAttempt.setType(null);
        collectionAttempt.setGatewayCollectionMethodType(expectedPaymentMethodType);
        Movement movement = new Movement();
        movement.setStatus(PAID);
        collectionAttempt.setValidMovements(singletonList(movement));
        collectionSummary.setAttempts(singletonList(collectionAttempt));
        bookingDetail.setCollectionSummary(collectionSummary);
        //When
        final String subscriptionPaymentMethodType = bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail().apply(bookingDetail);
        //Then
        assertEquals(subscriptionPaymentMethodType, expectedPaymentMethodType);
    }

    @Test
    public void testBookingsTotalSavingsIsDefaultZeroWhenNoBookings() {
        //Given
        List<BookingDetail> emptyBookingDetails = emptyList();
        //When
        final Money money = bookingDetailInfoExtractor.extractTotalSavingsFromBookingDetails(emptyBookingDetails);
        //Then
        assertEqualsMoney(money, ZERO, EMPTY);
    }

    @Test
    public void testBookingsTotalSavingsIsDefaultZeroWhenBookingHasNoMembershipPerks() {
        //Given
        final BookingDetail bookingDetail = new BookingDetail();
        //When
        final Money money = bookingDetailInfoExtractor.extractTotalSavingsFromBookingDetails(singletonList(bookingDetail));
        //Then
        assertEqualsMoney(money, ZERO, null);
    }

    @Test
    public void testBookingsTotalSavingsIsZeroWhenBookingHasEmptyFeeContainers() {
        //Given
        final String currencyCode = "GBP";
        BookingDetail bookingDetail = bookingDetailWithPerkFee(currencyCode, 123L);
        //When
        final Money money = bookingDetailInfoExtractor.extractTotalSavingsFromBookingDetails(singletonList(bookingDetail));
        //Then
        assertEqualsMoney(money, ZERO, currencyCode);
    }

    @Test
    public void testBookingsTotalSavingsIsZeroWhenBookingHasNoPerksFeeContainers() {
        //Given
        final String currencyCode = "GBP";
        final long perksFeeContainerId = 1933L;
        final long noPerksFeeContainerId = 678L;
        FeeContainer noPerksFeeContainer = feeContainerWithId(noPerksFeeContainerId);
        BookingDetail bookingDetail = bookingDetailWithPerkFee(currencyCode, perksFeeContainerId, noPerksFeeContainer);
        //When
        final Money money = bookingDetailInfoExtractor.extractTotalSavingsFromBookingDetails(singletonList(bookingDetail));
        //Then
        assertEqualsMoney(money, ZERO, currencyCode);
    }

    @Test
    public void testBookingsTotalSavingsIsTotalSumWhenBookingHasNoPerksFees() {
        //Given
        final String currencyCode = "EUR";
        final long perksFeeContainerId = 553L;
        FeeContainer firstPerksFeeContainer = feeContainerWithIdAndAmount(perksFeeContainerId, BigDecimal.valueOf(12.3));
        FeeContainer secondPerksFeeContainer = feeContainerWithIdAndAmount(perksFeeContainerId, BigDecimal.valueOf(11.5));
        BookingDetail firstBookingDetail = bookingDetailWithPerkFee(currencyCode, perksFeeContainerId, firstPerksFeeContainer);
        BookingDetail secondBookingDetail = bookingDetailWithPerkFee(currencyCode, perksFeeContainerId, secondPerksFeeContainer);
        //When
        final Money money = bookingDetailInfoExtractor.extractTotalSavingsFromBookingDetails(asList(firstBookingDetail, secondBookingDetail));
        //Then
        assertEqualsMoney(money, BigDecimal.valueOf(23.8), currencyCode);
    }

    @Test
    public void testPrimeBookingInfoIsEmptyWhenEmptyDetails() {
        //Given
        List<BookingDetail> emptyBookingDetails = emptyList();
        //When
        final List<PrimeBookingInformation> primeBookingInfo = bookingDetailInfoExtractor.extractMemberSubscriptionDetailsFromBookingDetails(emptyBookingDetails);
        //Then
        assertEquals(primeBookingInfo.size(), 0);
    }

    @Test
    public void testPrimeBookingInfoPriceIsBookingDetailPrice() {
        //Given
        final Money bookingPrice = buildMoney(TEN, "EUR");
        final BookingDetail bookingDetail = bookingDetailWithMoneyAndBookingId(732L, bookingPrice);
        //When
        final List<PrimeBookingInformation> primeBookingInfo = bookingDetailInfoExtractor.extractMemberSubscriptionDetailsFromBookingDetails(singletonList(bookingDetail));
        //Then
        assertEquals(primeBookingInfo.iterator().next().getPrice(), bookingPrice);
    }

    @Test
    public void testPrimeBookingInfoIdIsBookingDetailId() {
        //Given
        final long bookingId = 829L;
        final BookingDetail bookingDetail = bookingDetailWithMoneyAndBookingId(bookingId, buildMoney(TEN, "EUR"));
        //When
        final List<PrimeBookingInformation> primeBookingInfo = bookingDetailInfoExtractor.extractMemberSubscriptionDetailsFromBookingDetails(singletonList(bookingDetail));
        //Then
        assertEquals(primeBookingInfo.iterator().next().getBookingId(), bookingId);
    }

    @Test
    public void testPrimeBookingDiscountIsBookingDetailPerksAmount() {
        //Given
        final long bookingId = 829L;
        final long perksFeeContainerId = 998877L;
        final String currencyCode = "EUR";
        BigDecimal discount = BigDecimal.valueOf(11.5);
        FeeContainer perksFeeContainer = feeContainerWithIdAndAmount(perksFeeContainerId, discount);
        BookingDetail bookingDetail = bookingDetailWithPerkFee(currencyCode, perksFeeContainerId, perksFeeContainer);
        bookingDetail.setPrice(buildMoney(TEN, currencyCode));
        BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
        bookingBasicInfo.setId(bookingId);
        bookingDetail.setBookingBasicInfo(bookingBasicInfo);
        //When
        final List<PrimeBookingInformation> primeBookingInfo = bookingDetailInfoExtractor.extractMemberSubscriptionDetailsFromBookingDetails(singletonList(bookingDetail));
        //Then
        assertEquals(primeBookingInfo.iterator().next().getDiscount().getAmount(), discount);
        assertEquals(primeBookingInfo.iterator().next().getDiscount().getCurrency(), currencyCode);
    }

    private BookingDetail bookingDetailWithMoneyAndBookingId(final long bookingId, final Money bookingPrice) {
        final BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setPrice(bookingPrice);
        BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
        bookingBasicInfo.setId(bookingId);
        bookingDetail.setBookingBasicInfo(bookingBasicInfo);
        return bookingDetail;
    }

    private BookingDetail bookingDetailWithPerkFee(final String currencyCode, final long perksFeeContainerId, final FeeContainer... perksFeeContainer) {
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setCurrencyCode(currencyCode);
        MembershipPerks membershipPerks = new MembershipPerks();
        membershipPerks.setFeeContainerId(perksFeeContainerId);
        bookingDetail.setMembershipPerks(membershipPerks);
        bookingDetail.setAllFeeContainers(perksFeeContainer == null ? emptyList() : asList(perksFeeContainer));
        return bookingDetail;
    }

    private FeeContainer feeContainerWithId(final long perksFeeContainerId) {
        FeeContainer perksFeeContainer = new FeeContainer();
        perksFeeContainer.setId(perksFeeContainerId);
        return perksFeeContainer;
    }

    private FeeContainer feeContainerWithIdAndAmount(final long perksFeeContainerId, final BigDecimal amount) {
        FeeContainer perksFeeContainer = new FeeContainer();
        perksFeeContainer.setId(perksFeeContainerId);
        perksFeeContainer.setTotalAmount(amount);
        return perksFeeContainer;
    }

    private void assertEqualsMoney(final Money money, final BigDecimal expectedAmount, final String expectedCurrency) {
        assertNotNull(money);
        assertEquals(money.getAmount(), expectedAmount);
        assertEquals(money.getCurrency(), expectedCurrency);
    }
}
