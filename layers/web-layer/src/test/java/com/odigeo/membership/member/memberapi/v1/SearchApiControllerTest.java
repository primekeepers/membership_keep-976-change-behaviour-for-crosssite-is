package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.search.SearchService;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class SearchApiControllerTest {
    private static final Long MEMBER_ACCOUNT_ID = 123L;
    private static final long USER_ID = 321L;
    private static final String NAME = "name";
    private static final String LAST_NAME = "lastname";
    private static final String TIME_UNIT = "DAYS";
    private static final Long MEMBERSHIP_ID = 123321L;
    private static final String WEBSITE = "IT";
    private static final Integer RENEWAL_DURATION = 6;
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME);
    private static final Membership MEMBERSHIP = new Membership(new MembershipBuilder().setId(MEMBERSHIP_ID).setRenewalDuration(RENEWAL_DURATION));
    private static final Membership MEMBERSHIP_WITH_ACCOUNT = new Membership(new MembershipBuilder().setId(MEMBERSHIP_ID).setMemberAccount(MEMBER_ACCOUNT));
    private static final MemberAccount MEMBER_ACCOUNT_WITH_MEMBERSHIP = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME)
            .setMemberships(Collections.singletonList(MEMBERSHIP));
    private static final Long STATUS_ACTION_ID = 999L;
    private static final MemberStatusAction MEMBER_STATUS_ACTION = new MemberStatusAction(STATUS_ACTION_ID, MEMBERSHIP_ID, StatusAction.ACTIVATION, new Date());
    private static final Membership MEMBERSHIP_WITH_STATUS_ACTIONS = new Membership(new MembershipBuilder().setId(MEMBERSHIP_ID).setRenewalDuration(12).setMemberStatusActions(Collections.singletonList(MEMBER_STATUS_ACTION)));

    private MockedStatic<ConfigurationEngine> configurationEngine;
    @Mock
    private MemberService memberService;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private SearchService searchService;

    private SearchApiController searchApiController;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        configurationEngineMock();
        searchApiController = new SearchApiController();

    }

    private void configurationEngineMock() {
        configurationEngine.when(() -> ConfigurationEngine.getInstance(MemberService.class)).thenReturn(memberService);
        configurationEngine.when(() -> ConfigurationEngine.getInstance(MemberAccountService.class)).thenReturn(memberAccountService);
        configurationEngine.when(() -> ConfigurationEngine.getInstance(SearchService.class)).thenReturn(searchService);
    }

    @BeforeClass
    public void beforeClass() {
        configurationEngine = Mockito.mockStatic(ConfigurationEngine.class);
    }

    @AfterClass
    public void afterClass() {
        configurationEngine.close();
    }

    @Test
    public void testGetMembership() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(MEMBERSHIP_ID)).thenReturn(MEMBERSHIP);
        MembershipResponse membershipResponse = searchApiController.getMembership(MEMBERSHIP_ID, false);
        assertEquals(membershipResponse.getId(), MEMBERSHIP_ID.longValue());
    }

    @Test
    public void testGetMembershipWithMemberAccount() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(MEMBERSHIP_WITH_ACCOUNT);
        MembershipResponse membershipResponse = searchApiController.getMembership(MEMBERSHIP_ID, true);
        assertEquals(membershipResponse.getId(), MEMBERSHIP_ID.longValue());
        assertNotNull(membershipResponse.getMemberAccount());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testGetMembershipThrowDataAccess() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(MEMBERSHIP_ID)).thenThrow(new DataAccessException(StringUtils.EMPTY));
        MembershipResponse membershipResponse = searchApiController.getMembership(MEMBERSHIP_ID, false);
        assertEquals(membershipResponse.getId(), MEMBERSHIP_ID.longValue());
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testGetMembershipThrowMissingElement() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(MEMBERSHIP_ID)).thenThrow(new MissingElementException(StringUtils.EMPTY));
        MembershipResponse membershipResponse = searchApiController.getMembership(MEMBERSHIP_ID, false);
        assertEquals(membershipResponse.getId(), MEMBERSHIP_ID.longValue());
    }

    @Test
    public void testGetMemberAccount() throws MissingElementException, DataAccessException {
        when(memberAccountService.getMemberAccountById(eq(MEMBER_ACCOUNT_ID), eq(FALSE))).thenReturn(MEMBER_ACCOUNT);
        MemberAccountResponse memberAccountResponse = searchApiController.getMemberAccount(MEMBER_ACCOUNT_ID, FALSE);
        verify(memberAccountService).getMemberAccountById(eq(MEMBER_ACCOUNT_ID), eq(FALSE));
        checkMemberAccountResponse(memberAccountResponse, FALSE);
    }

    @Test
    public void testGetMemberAccountWithMembership() throws MissingElementException, DataAccessException {
        when(memberAccountService.getMemberAccountById(eq(MEMBER_ACCOUNT_ID), eq(TRUE))).thenReturn(MEMBER_ACCOUNT_WITH_MEMBERSHIP);
        MemberAccountResponse memberAccountResponse = searchApiController.getMemberAccount(MEMBER_ACCOUNT_ID, TRUE);
        verify(memberAccountService).getMemberAccountById(eq(MEMBER_ACCOUNT_ID), eq(TRUE));
        checkMemberAccountResponse(memberAccountResponse, TRUE);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testGetMemberAccountMissing() throws MissingElementException, DataAccessException {
        when(memberAccountService.getMemberAccountById(eq(MEMBER_ACCOUNT_ID), anyBoolean())).thenThrow(new MissingElementException(StringUtils.EMPTY));
        searchApiController.getMemberAccount(MEMBER_ACCOUNT_ID, FALSE);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testGetMemberAccountException() throws MissingElementException, DataAccessException {
        when(memberAccountService.getMemberAccountById(eq(MEMBER_ACCOUNT_ID), anyBoolean())).thenThrow(new DataAccessException(StringUtils.EMPTY));
        searchApiController.getMemberAccount(MEMBER_ACCOUNT_ID, FALSE);
    }

    @Test
    public void testSearchAccounts() throws DataAccessException {
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.singletonList(MEMBER_ACCOUNT));
        MemberAccountSearchRequest memberAccountSearchRequest = new MemberAccountSearchRequest.Builder().userId(USER_ID).build();
        List<MemberAccountResponse> memberAccountResponses = searchApiController.searchAccounts(memberAccountSearchRequest);
        assertEquals(memberAccountResponses.get(0).getUserId(), USER_ID);
        assertTrue(memberAccountResponses.get(0).getMemberships().isEmpty());
    }

    @Test
    public void testSearchAccountsWithMemberships() throws DataAccessException {
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.singletonList(MEMBER_ACCOUNT_WITH_MEMBERSHIP));
        MemberAccountSearchRequest memberAccountSearchRequest = new MemberAccountSearchRequest.Builder().userId(USER_ID).build();
        List<MemberAccountResponse> memberAccountResponses = searchApiController.searchAccounts(memberAccountSearchRequest);
        assertEquals(memberAccountResponses.get(0).getUserId(), USER_ID);
        assertFalse(memberAccountResponses.get(0).getMemberships().isEmpty());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testSearchAccountsWithException() throws DataAccessException {
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenThrow(new DataAccessException(StringUtils.EMPTY));
        MemberAccountSearchRequest memberAccountSearchRequest = new MemberAccountSearchRequest.Builder().userId(USER_ID).build();
        searchApiController.searchAccounts(memberAccountSearchRequest);
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testSearchAccountsInvalidSearch() {
        MemberAccountSearchRequest memberAccountSearchRequest = new MemberAccountSearchRequest.Builder().build();
        searchApiController.searchAccounts(memberAccountSearchRequest);
    }

    @Test
    public void testSearchMemberships() throws DataAccessException {
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenReturn(Collections.singletonList(MEMBERSHIP));
        MembershipSearchRequest searchRequest = new MembershipSearchRequest.Builder().website(WEBSITE).durationTimeUnit(TIME_UNIT).build();
        List<MembershipResponse> membershipResponses = searchApiController.searchMemberships(searchRequest);
        assertEquals(membershipResponses.get(0).getId(), MEMBERSHIP_ID.longValue());
        assertTrue(membershipResponses.get(0).getMemberStatusActions().isEmpty());
        assertNull(membershipResponses.get(0).getMemberAccount());
    }

    @Test
    public void testSearchMembershipsWithStatusActions() throws DataAccessException {
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenReturn(Collections.singletonList(MEMBERSHIP_WITH_STATUS_ACTIONS));
        MembershipSearchRequest searchRequest = new MembershipSearchRequest.Builder().withStatusActions(true).website(WEBSITE).durationTimeUnit(TIME_UNIT).build();
        List<MembershipResponse> membershipResponses = searchApiController.searchMemberships(searchRequest);
        assertEquals(membershipResponses.get(0).getId(), MEMBERSHIP_ID.longValue());
        assertFalse(membershipResponses.get(0).getMemberStatusActions().isEmpty());
        assertNull(membershipResponses.get(0).getMemberAccount());
    }

    @Test
    public void testSearchMembershipsWithAccount() throws DataAccessException {
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenReturn(Collections.singletonList(MEMBERSHIP_WITH_ACCOUNT));
        MembershipSearchRequest searchRequest = new MembershipSearchRequest.Builder().website(WEBSITE).durationTimeUnit(TIME_UNIT).withMemberAccount(TRUE)
                .memberAccountSearchRequest(new MemberAccountSearchRequest.Builder().userId(USER_ID).build())
                .build();
        List<MembershipResponse> membershipResponses = searchApiController.searchMemberships(searchRequest);
        assertEquals(membershipResponses.get(0).getId(), MEMBERSHIP_ID.longValue());
        assertTrue(membershipResponses.get(0).getMemberStatusActions().isEmpty());
        assertNotNull(membershipResponses.get(0).getMemberAccount());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testSearchMembershipsWithException() throws DataAccessException {
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenThrow(new DataAccessException(StringUtils.EMPTY));
        MembershipSearchRequest searchRequest = new MembershipSearchRequest.Builder().website(WEBSITE).durationTimeUnit(TIME_UNIT).build();
        searchApiController.searchMemberships(searchRequest);
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testSearchMembershipsInvalidSearch() {
        MembershipSearchRequest searchRequest = new MembershipSearchRequest.Builder().durationTimeUnit(TIME_UNIT).build();
        searchApiController.searchMemberships(searchRequest);
    }

    @Test
    public void checkLazyConfigurationEngineCalls() throws DataAccessException, MissingElementException {
        ConfigurationEngine.init(this::configure);
        SearchApiController searchApiController = new SearchApiController();
        when(memberAccountService.getMemberAccountById(eq(MEMBER_ACCOUNT_ID), eq(FALSE))).thenReturn(MEMBER_ACCOUNT);
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.singletonList(MEMBER_ACCOUNT));
        when(memberService.getMembershipById(eq(MEMBERSHIP_ID))).thenReturn(MEMBERSHIP);
        assertFalse(searchApiController.searchAccounts(new MemberAccountSearchRequest.Builder().userId(USER_ID).build()).isEmpty());
        assertNotNull(searchApiController.getMemberAccount(MEMBER_ACCOUNT_ID, FALSE));
        assertNotNull(searchApiController.getMembership(MEMBERSHIP_ID, false));
    }

    private void configure(Binder binder) {
        binder.bind(MemberAccountService.class).toInstance(memberAccountService);
        binder.bind(SearchService.class).toInstance(searchService);
        binder.bind(MemberService.class).toInstance(memberService);
    }

    private void checkMemberAccountResponse(MemberAccountResponse memberAccountResponse, boolean withMemberships) {
        assertEquals(memberAccountResponse.getId(), MEMBER_ACCOUNT.getId());
        assertEquals(memberAccountResponse.getName(), MEMBER_ACCOUNT.getName());
        assertEquals(memberAccountResponse.getLastNames(), MEMBER_ACCOUNT.getLastNames());
        assertEquals(memberAccountResponse.getUserId(), MEMBER_ACCOUNT.getUserId());
        if (withMemberships) {
            assertFalse(memberAccountResponse.getMemberships().isEmpty());
            assertEquals(memberAccountResponse.getMemberships().get(0).getId(), MEMBERSHIP_ID.longValue());
        } else {
            assertTrue(memberAccountResponse.getMemberships().isEmpty());
        }
    }
}
