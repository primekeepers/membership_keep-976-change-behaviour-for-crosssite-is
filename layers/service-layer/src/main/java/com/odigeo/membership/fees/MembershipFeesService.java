package com.odigeo.membership.fees;

import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FixFee;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface MembershipFeesService {

    List<AbstractFee> retrieveFees(Long feeContainerId);

    FixFee fillProductFee(BigDecimal amount, String currency);

    Optional<Long> requestFeeContainerCreation(BigDecimal subscriptionPrice, String currencyCode);

}
