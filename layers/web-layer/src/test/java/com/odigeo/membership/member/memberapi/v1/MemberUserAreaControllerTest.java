package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.Flight;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberAccountBooking;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.memberapi.v1.util.MemberServiceMapping;
import com.odigeo.membership.member.statusaction.MemberStatusActionService;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.response.FutureFlight;
import com.odigeo.membership.response.MembershipAccountInfo;
import com.odigeo.membership.response.MembershipInfo;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MemberUserAreaControllerTest {

    private static final long MEMBER_ACCOUNT_ID = new Random().nextLong();
    private static final String LAST_STATUS_CHANGE_DATE_STR = "2007-12-03";
    private static final Date LAST_STATUS_CHANGE_DATE = Date.from(Instant.parse(LAST_STATUS_CHANGE_DATE_STR.concat("T00:00:00.00Z")));
    private static final String NAME = "Squall";
    private static final String LAST_NAME = "Leonhart";
    private static final long USER_ID = 8L;
    private static final long MEMBERSHIP_ID_ONE = 1L;
    private static final long MEMBERSHIP_ID_TWO = 2L;
    private static final Integer RENEWAL_DURATION = 12;
    private static final String WEBSITE = "GB";
    private static final MemberStatus MEMBER_STATUS = MemberStatus.ACTIVATED;
    private static final MembershipRenewal MEMBERSHIP_RENEWAL = MembershipRenewal.ENABLED;
    private static final long BOOKING_ID = 91L;
    private static final MemberSubscriptionDetails MEMBER_SUBSCRIPTION_DETAILS_1 = new MemberSubscriptionDetails().setMembershipId(MEMBERSHIP_ID_ONE);
    private static final MemberSubscriptionDetails MEMBER_SUBSCRIPTION_DETAILS_2 = new MemberSubscriptionDetails().setMembershipId(MEMBERSHIP_ID_TWO);
    private static final List<MemberSubscriptionDetails> MEMBER_SUBSCRIPTION_DETAILS = Arrays.asList(MEMBER_SUBSCRIPTION_DETAILS_1, MEMBER_SUBSCRIPTION_DETAILS_2);
    private static final String TEST = "TEST";
    private static final String TIMESTAMP_STR = "2019-12-11T10:15:30";
    private static final String EXPIRATION_DATE_STR = "2020-01-01";
    private static final String ACTIVATION_DATE_STR = "2019-06-01";
    private static final String CURRENCY_CODE = "TEST CURRENCY";
    private static final BigDecimal PRICE = BigDecimal.valueOf(12.34);
    private MemberUserAreaController memberUserAreaController;

    @Mock
    private MemberUserAreaService memberUserAreaService;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private MemberStatusActionService memberStatusActionService;
    @Mock
    private MemberService memberService;
    @Spy
    private MapperFacade mapper = new GeneralMapperCreator().getMapper();

    @BeforeMethod
    public void setUp() throws Exception {
        openMocks(this);
        configureModules();
        configureMemberUserAreaService();
        memberUserAreaController = new MemberUserAreaController(mapper);
    }

    @Test
    public void testGetAllMemberSubscriptionsDetails() {
        List<com.odigeo.membership.response.MemberSubscriptionDetails> allMemberSubscriptionsDetailsResponse = memberUserAreaController.getAllMemberSubscriptionsDetails(MEMBER_ACCOUNT_ID);
        assertNotNull(allMemberSubscriptionsDetailsResponse);
        assertEquals(allMemberSubscriptionsDetailsResponse.size(), MEMBER_SUBSCRIPTION_DETAILS.size());
        assertEquals(allMemberSubscriptionsDetailsResponse.get(0).getMembershipId().longValue(), MEMBERSHIP_ID_ONE);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetAllMemberSubscriptionsDetailsMissingElementException() throws Exception {
        when(memberUserAreaService.getAllMemberSubscriptionDetails(anyLong())).thenThrow(new MissingElementException(StringUtils.EMPTY));
        memberUserAreaController.getAllMemberSubscriptionsDetails(MEMBER_ACCOUNT_ID);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetAllMemberSubscriptionsDetailsDataAccessException() throws Exception {
        when(memberUserAreaService.getAllMemberSubscriptionDetails(anyLong())).thenThrow(new DataAccessException(StringUtils.EMPTY));
        memberUserAreaController.getAllMemberSubscriptionsDetails(MEMBER_ACCOUNT_ID);
    }

    @Test
    public void testGetMembershipInfo() throws MissingElementException, DataAccessException, BookingApiException, InvalidParametersException {
        final MemberAccountBooking membershipAccountBooking = new MemberAccountBooking(getMemberAccountWithMembership(), BOOKING_ID, LAST_STATUS_CHANGE_DATE);
        when(memberUserAreaService.getMembershipInfo(MEMBERSHIP_ID_ONE, false)).thenReturn(membershipAccountBooking);
        validateTestGetMembershipInfo(memberUserAreaController.getMembershipInfo(MEMBERSHIP_ID_ONE, false));
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testGetMembershipInfoMissingElementException() throws MissingElementException, DataAccessException, BookingApiException, InvalidParametersException {
        when(memberUserAreaService.getMembershipInfo(MEMBERSHIP_ID_ONE, false)).thenThrow(new MissingElementException(TEST));
        memberUserAreaController.getMembershipInfo(MEMBERSHIP_ID_ONE, false);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testGetMembershipInfoBookingApiException() throws MissingElementException, DataAccessException, BookingApiException, InvalidParametersException {
        when(memberUserAreaService.getMembershipInfo(MEMBERSHIP_ID_ONE, false))
                .thenThrow(new BookingApiException(TEST, new Exception(TEST)));
        memberUserAreaController.getMembershipInfo(MEMBERSHIP_ID_ONE, false);
    }

    @Test
    public void testGetUserId() throws MissingElementException, DataAccessException {
        setMemberServiceResponse();
        assertEquals(memberUserAreaController.getUserId(MEMBERSHIP_ID_ONE), USER_ID);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testGetUserIdMissingElementException() throws MissingElementException, DataAccessException {
        setMemberServiceResponse();
        when(memberAccountService.getMemberAccountByMembershipId(anyLong())).thenThrow(new MissingElementException(TEST));
        memberUserAreaController.getUserId(MEMBERSHIP_ID_ONE);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetUserIdDataAccessException() throws MissingElementException, DataAccessException {
        setMemberServiceResponse();
        when(memberAccountService.getMemberAccountByMembershipId(anyLong())).thenThrow(new DataAccessException(TEST));
        memberUserAreaController.getUserId(MEMBERSHIP_ID_ONE);
    }

    @Test
    public void testUserHasAnyMembershipForBrandTrue() throws Exception {
        //Given
        final long userId = 538L;
        final String brand = "OP";
        when(memberUserAreaService.userHasAnyMembershipForBrand(userId, brand)).thenReturn(TRUE);
        //When
        boolean userHasAnyMembership = memberUserAreaController.userHasMembershipForBrand(userId, brand);
        //Then
        assertTrue(userHasAnyMembership);
    }

    @Test
    public void testUserHasAnyMembershipForBrandFalse() throws Exception {
        //Given
        final long userId = 6838L;
        final String brand = "ED";
        when(memberUserAreaService.userHasAnyMembershipForBrand(userId, brand)).thenReturn(FALSE);
        //When
        boolean userHasAnyMembership = memberUserAreaController.userHasMembershipForBrand(userId, brand);
        //Then
        assertFalse(userHasAnyMembership);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testUserHasAnyMembershipThrowsExceptionWhenThereIsAnError() throws Exception {
        //Given
        final long userId = 938L;
        final String brand = "GO";
        when(memberUserAreaService.userHasAnyMembershipForBrand(userId, brand)).thenThrow(new DataAccessException("expected exception"));
        //When
        memberUserAreaController.userHasMembershipForBrand(userId, brand);
    }

    @Test
    public void testGetMembershipAccountId() throws MissingElementException, DataAccessException {
        setMemberServiceResponse();
        assertEquals(memberUserAreaController.getMembershipAccountId(MEMBERSHIP_ID_ONE), MEMBER_ACCOUNT_ID);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testGetMembershipAccountIdMissingElementException() throws MissingElementException, DataAccessException {
        setMemberServiceResponse();
        when(memberService.getMembershipById(anyLong())).thenThrow(new MissingElementException(TEST));
        memberUserAreaController.getMembershipAccountId(MEMBERSHIP_ID_ONE);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testGetMembershipAccountIdDataAccessException() throws MissingElementException, DataAccessException {
        setMemberServiceResponse();
        when(memberService.getMembershipById(anyLong())).thenThrow(new DataAccessException(TEST));
        memberUserAreaController.getMembershipAccountId(MEMBERSHIP_ID_ONE);
    }

    @Test
    public void testGetMembershipAccountReturnsAccount() throws MissingElementException, DataAccessException {
        //Given
        final long accountId = 7383L;
        String timestamp = "2007-12-03T10:15:30";
        final MemberAccount foundMembershipAccount = new MemberAccount(accountId, 23L, "John", "Doe").setTimestamp(LocalDateTime.parse(timestamp));
        final MembershipAccountInfo expectedMembershipAccount = membershipAccount(accountId, 23L, "John", "Doe", timestamp);
        when(memberAccountService.getMemberAccountById(accountId, false)).thenReturn(foundMembershipAccount);
        //When
        MembershipAccountInfo membershipAccount = memberUserAreaController.getMembershipAccount(accountId);
        //Then
        assertEqualsMembershipAccount(membershipAccount, expectedMembershipAccount);
    }

    @Test(expectedExceptions = MembershipServiceException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipAccountInfoThrowsDataAccessException() throws MissingElementException, DataAccessException {
        long accountId = 123L;
        when(memberAccountService.getMemberAccountById(accountId, false)).thenThrow(new DataAccessException("Expected exception"));
        memberUserAreaController.getMembershipAccount(accountId);
    }

    @Test(expectedExceptions = MembershipServiceException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipAccountInfoThrowsMissingElementException() throws MissingElementException, DataAccessException {
        long accountId = 4523L;
        when(memberAccountService.getMemberAccountById(accountId, false)).thenThrow(new MissingElementException("Expected exception"));
        memberUserAreaController.getMembershipAccount(accountId);
    }

    private void assertEqualsMembershipAccount(final MembershipAccountInfo membershipAccount, final MembershipAccountInfo expectedMembershipAccount) {
        assertEquals(membershipAccount.getId(), expectedMembershipAccount.getId());
        assertEquals(membershipAccount.getUserId(), expectedMembershipAccount.getUserId());
        assertEquals(membershipAccount.getFirstName(), expectedMembershipAccount.getFirstName());
        assertEquals(membershipAccount.getLastName(), expectedMembershipAccount.getLastName());
        assertEquals(membershipAccount.getTimestamp(), expectedMembershipAccount.getTimestamp());
    }

    private MembershipAccountInfo membershipAccount(final Long accountId, final long userId, final String firstName, final String lastName, final String timestamp) {
        MembershipAccountInfo membershipAccountInfo = new MembershipAccountInfo();
        membershipAccountInfo.setId(accountId);
        membershipAccountInfo.setUserId(userId);
        membershipAccountInfo.setFirstName(firstName);
        membershipAccountInfo.setLastName(lastName);
        membershipAccountInfo.setTimestamp(timestamp);
        return membershipAccountInfo;
    }

    private void validateTestGetMembershipInfo(MembershipInfo membershipInfo) {
        assertEquals(membershipInfo.getMembershipId(), MEMBERSHIP_ID_ONE);
        assertEquals(membershipInfo.getName(), NAME);
        assertEquals(membershipInfo.getLastNames(), LAST_NAME);
        assertEquals(membershipInfo.getWebsite(), WEBSITE);
        assertEquals(membershipInfo.getMembershipStatus(), MEMBER_STATUS.toString());
        assertEquals(membershipInfo.getAutoRenewalStatus(), MEMBERSHIP_RENEWAL.toString());
        assertEquals(membershipInfo.getBookingIdSubscription(), BOOKING_ID);
        assertEquals(membershipInfo.getLastStatusModificationDate(), LAST_STATUS_CHANGE_DATE_STR);
        assertEquals(membershipInfo.getMemberAccountId(), MEMBER_ACCOUNT_ID);
        assertEquals(membershipInfo.getExpirationDate(), EXPIRATION_DATE_STR);
        assertEquals(membershipInfo.getActivationDate(), ACTIVATION_DATE_STR);
        assertEquals(membershipInfo.getSourceType(), SourceType.POST_BOOKING.toString());
        assertEquals(membershipInfo.getTotalPrice(), PRICE);
        assertEquals(membershipInfo.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(membershipInfo.getTimestamp(), TIMESTAMP_STR);
        assertEquals(membershipInfo.getMembershipType(), MembershipType.BUSINESS.toString());
    }

    private void setMemberServiceResponse() throws MissingElementException, DataAccessException {
        MemberAccount memberAccountWithMembership = getMemberAccountWithMembership();
        when(memberAccountService.getMemberAccountByMembershipId(anyLong())).thenReturn(memberAccountWithMembership);
        when(memberService.getMembershipById(MEMBERSHIP_ID_ONE)).thenReturn(memberAccountWithMembership.getMemberships().iterator().next());
    }

    private MemberAccount getMemberAccountWithMembership() {
        LocalDate activationDate = LocalDate.parse(ACTIVATION_DATE_STR);
        LocalDate expirationDate = LocalDate.parse(EXPIRATION_DATE_STR);
        MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME).setTimestamp(LocalDateTime.now());
        List<com.odigeo.membership.Membership> membershipList = new ArrayList<>();
        com.odigeo.membership.Membership membership = new MembershipBuilder().setId(MEMBERSHIP_ID_ONE).setWebsite(WEBSITE)
                .setStatus(MEMBER_STATUS).setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID)
                .setProductStatus(ProductStatus.CONTRACT).setExpirationDate(expirationDate.atStartOfDay())
                .setActivationDate(activationDate.atStartOfDay()).setSourceType(SourceType.POST_BOOKING)
                .setMembershipPricesBuilder(MembershipPrices.builder()
                        .totalPrice(PRICE)
                        .currencyCode(CURRENCY_CODE))
                .setTimestamp(LocalDateTime.parse(TIMESTAMP_STR))
                .setRenewalDuration(RENEWAL_DURATION)
                .setMembershipType(MembershipType.BUSINESS).build();
        membershipList.add(membership);
        memberAccount.setMemberships(membershipList);
        return memberAccount;
    }

    private void configureMemberUserAreaService() throws MissingElementException, DataAccessException {
        when(memberUserAreaService.getMemberSubscriptionDetails(eq(MEMBERSHIP_ID_ONE))).thenReturn(MEMBER_SUBSCRIPTION_DETAILS_1);
        when(memberUserAreaService.getMemberSubscriptionDetails(eq(MEMBERSHIP_ID_TWO))).thenReturn(MEMBER_SUBSCRIPTION_DETAILS_2);
        when(memberUserAreaService.getAllMemberSubscriptionDetails(anyLong())).thenReturn(MEMBER_SUBSCRIPTION_DETAILS);
    }

    private void configureModules() {
        ConfigurationEngine.init(binder -> {
            binder.bind(MemberUserAreaService.class).toProvider(() -> memberUserAreaService);
            binder.bind(MemberAccountService.class).toInstance(memberAccountService);
            binder.bind(MemberServiceMapping.class).toInstance(new MemberServiceMapping());
            binder.bind(MemberStatusActionService.class).toInstance(memberStatusActionService);
            binder.bind(MemberService.class).toInstance(memberService);
        });
    }

    @Test
    public void testGetFutureFlights() throws DataAccessException {

        //Given
        Set<Flight> flights = Collections.singleton(new Flight(1L, ZonedDateTime.parse("2020-12-31T00:00Z"), "iMADestination"));
        given(memberUserAreaService.getFlights(MEMBERSHIP_ID_ONE)).willReturn(flights);
        FutureFlight futureFlight = new FutureFlight();
        futureFlight.setDepartureDate("2020-12-31T12:00");
        futureFlight.setDestination("iMADestination");
        doReturn(Collections.singletonList(futureFlight)).when(mapper).mapAsList(flights, FutureFlight.class);
        //When
        List<FutureFlight> res = memberUserAreaController.getFutureFlights(MEMBERSHIP_ID_ONE);
        //Then
        assertNotNull(res);
    }

}
