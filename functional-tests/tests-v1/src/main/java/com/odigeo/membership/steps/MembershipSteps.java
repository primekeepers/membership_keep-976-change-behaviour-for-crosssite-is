package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.converter.BooleanConverter;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import com.odigeo.membership.functionals.membership.MembershipVerifier;
import com.odigeo.membership.functionals.membership.booking.FutureFlightBuilder;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.response.CreditCard;
import com.odigeo.membership.response.FutureFlight;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.testng.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@SuppressWarnings("PMD.GodClass")
@ScenarioScoped
public class MembershipSteps extends CommonSteps {

    private final MembershipManagementWorld world;
    private final DatabaseWorld databaseWorld;
    private final Set<Object> verified;
    private static final String UNAUTHORIZED_MESSAGE = "type=MembershipServiceException and message = Authentication header client not found";

    @Inject
    public MembershipSteps(MembershipManagementWorld world, DatabaseWorld databaseWorld, ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
        this.world = world;
        this.databaseWorld = databaseWorld;
        verified = new HashSet<>();
    }

    @When("^requested the member information with userId (\\d+) and website (\\w+)$")
    public void requestedTheMemberInformationWithMemberId(long memberId, String website) {
        world.setMembership(membershipService.getMembership(memberId, website));
    }

    @When("^requested the current membership with userId (\\d+) and website (\\w+)$")
    public void requestedTheCurrentMembershipWithMemberIdAndWebsite(long userId, String website) {
        world.setMembershipResponse(membershipService.getCurrentMembership(userId, website));
    }

    @Then("^the response to the member request is:$")
    public void theResponseToTheMemberRequestIs(List<MembershipVerifier> verifiers) {
        for (MembershipVerifier membershipVerifier : verifiers) {
            membershipVerifier.verifyMembership(world.getMembership());
        }
    }

    @Then("^the retrieved membership has status (\\w+)$")
    public void theRetrievedMembershipHasStatus(String expectedStatus) {
        assertEquals(world.getMembershipResponse().getStatus(), expectedStatus);
    }

    @Then("the retrieved membership has type (\\w+) and renewalPrice (\\d+.\\d+)$")
    public void theRetrievedMembershipHasType(String expectedType, BigDecimal expectedRenewalPrice) {
        assertEquals(world.getMembershipResponse().getMembershipType(), expectedType);
        assertEquals(world.getMembershipResponse().getRenewalPrice(), expectedRenewalPrice);
    }

    @Then("^there is not current membership$")
    public void thereIsNoCurrentMembership() {
        Assert.assertNull(world.getMembershipResponse());
    }

    @Then("^the response attributes include values:$")
    public void theResponseAttributesIncludeValues(List<Membership> expectedMemberships) {
        Membership responseMembership = world.getMembership();
        for (Membership membership : expectedMemberships) {
            assertEquals(responseMembership.getMemberId(), membership.getMemberId());
            assertEquals(responseMembership.getSourceType(), membership.getSourceType());
            assertEquals(responseMembership.getMonthsDuration(), membership.getMonthsDuration());
        }
    }

    @Then("^the warningsList size is (\\d+)$")
    public void theWarningsListSizeIs(int warningListSize) {
        assertEquals(world.getMembership().getWarnings().size(), warningListSize);
    }

    @Then("^the response to the member request is void$")
    public void theResponseToTheMemberRequestIsVoid() {
        Assert.assertNull(world.getMembership());
    }

    @Then("^the member is created correctly with this information$")
    public void theMemberIsCreatedCorrectlyWithThisInformation() {
        assertNotNull(world.getCreatedMemberId());
        MembershipInfo membershipInfo = userAreaService.getMembershipInfo(world.getCreatedMemberId(), false);
        assertNotNull(membershipInfo);
        assertEquals(membershipInfo.getSourceType(), "FUNNEL_BOOKING");
    }

    @Then("^new membership has the same source type as the expired membership with id (\\d+)$")
    public void newMembershipHasSameSourceTypeAsExpiredOne(Long membershipId) {
        Long createdMembershipId = world.getCreatedMemberId();
        MembershipInfo oldMembershipInfo = userAreaService.getMembershipInfo(membershipId, false);
        MembershipInfo newMembershipInfo = userAreaService.getMembershipInfo(createdMembershipId, false);
        assertEquals(newMembershipInfo.getSourceType(), oldMembershipInfo.getSourceType());
    }

    @When("^requested to disable auto renewal for member with memberId (\\d+)$")
    public void requestedToDisableAutoRenewalForMemberWithMemberId(Long memberId) {
        world.setMembershipAutoRenewalStatus(membershipService.disableAutoRenewal(memberId));
    }

    @And("^requested to enable auto renewal for member with memberId (\\d+)$")
    public void requestedToEnableAutoRenewalForMemberWithMemberId(Long memberId) {
        world.setMembershipAutoRenewalStatus(membershipService.enableAutoRenewal(memberId));
        assertEquals(membershipService.getMembership(4321L, "ES").getAutoRenewalStatus(), "ENABLED");
    }

    @Then("^member has the auto renewal with status (\\w+)$")
    public void memberHasTheAutoRenewalWithStatus(String status) {
        assertEquals(world.getMembershipAutoRenewalStatus().getRenewalStatus(), status);
    }

    @And("^member with memberId (\\d+) has the auto renewal with status (\\w+)$")
    public void memberHasTheAutoRenewalWithStatus(Long membershipId, String autoRenewalExpected) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membership = databaseWorld.getMembershipById(membershipId);
        assertEquals(membership.getAutoRenewal(), autoRenewalExpected);
    }

    @Then("^the response membership List size is (\\d+)$")
    public void theResponseMembershipListSizeIsListSize(int listSize) {
        assertEquals(world.getMembershipList().size(), listSize);
    }

    @Then("^the response has expected attributes for membership with id (\\w+)$")
    public void theResponseMembershipListSizeIsListSize(Long membershipId, List<Membership> expectedMemberships) {
        assertFalse(world.getMembershipList().isEmpty());
        Membership membershipToCompare = world.getMembershipList()
                .stream()
                .filter(membership -> membership.getMemberId() == membershipId)
                .findAny()
                .orElseThrow(() -> new AssertionError("Membership to compare with not found in response"));
        for (Membership expected : expectedMemberships) {
            assertEquals(membershipToCompare.getSourceType(), expected.getSourceType());
        }
    }

    @Then("^the subscription details has (\\d+) prime bookings info and the total amount saved is (\\d+)$")
    public void theSubscriptionDetailsHasPrimeBookingsInfoAndTheTotalAmountSavedIsEUR(int numberBookings, long totalSavings) {
        int totalBookings = world.getSubscriptionsDetails().stream().mapToInt(subDetails -> subDetails.getPrimeBookingsInfo().size()).sum();
        assertEquals(totalBookings, numberBookings);
        BigDecimal total = BigDecimal.ZERO;
        for (MemberSubscriptionDetails memberSubscriptionDetails : world.getSubscriptionsDetails()) {
            total = memberSubscriptionDetails.getTotalSavings().getAmount().add(total);
        }
        assertEquals(total, new BigDecimal(totalSavings));
    }

    @When("^the client calls getMembershipInfo to membership Api with membershipId (\\d+) and skip booking is (true|false)$")
    public void callGetMembershipInfo(long membershipId, @Transform(BooleanConverter.class) Boolean skipBooking) {
        world.setMembershipInfo(userAreaService.getMembershipInfo(membershipId, skipBooking));
    }

    @When("^the client calls getFutureFlights to membership Api with membershipId (\\d+)$")
    public void callGetFutureFlights(long membershipId) {
        world.setFutureFlights(userAreaService.getFutureFlights(membershipId));
    }

    @When("^the userId for member with membershipId (\\d+) is requested$")
    public void userIdForMemberWithMembershipIdIsRequested(long membershipId) {
        world.setUserId(userAreaService.getUserId(membershipId));
    }

    @When("^the userId for member with invalid membershipId (\\d+) is requested$")
    public void userIdForMemberWithInvalidMembershipIdIsRequested(long membershipId) {
        try {
            world.setUserId(userAreaService.getUserId(membershipId));
        } catch (UnsupportedOperationException e) {
            world.addException(e);
        }
    }

    @When("^the userId (\\d+) is returned$")
    public void userIdIsReturned(long userId) {
        assertEquals(world.getUserId().longValue(), userId);
    }

    @Then("^the membershipInfo response is:$")
    public void theMembershipInfoResponseIs(DataTable dataTable) {
        List<Map<String, String>> dataTableMapList = dataTable.asMaps(String.class, String.class);
        dataTableMapList.forEach(this::verifyMembershipInfo);
    }

    @Then("^the futureFlights contains only:$")
    public void theFutureFlightsResponseIs(List<FutureFlightBuilder> dataTable) {
        assertEquals(world.getFutureFlights().size(), dataTable.size(), "World only contains " + world.getFutureFlights().size() + " futureFlights");
        dataTable.forEach(this::verifyFutureFlight);
    }



    @And("^for each subscription the subscription payment method is the following$")
    public void theSubscriptionPaymentMethodIsTheFollowingOne(DataTable dataTable) {
        List<Map<String, String>> dataTableMap = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> paymentMethods : dataTableMap) {
            verifySubscriptionPaymentMethod(paymentMethods);
        }
    }

    @Then("^a membership is created with the correct number of months duration:$")
    public void theMembershipIsCreatedWithTheCorrectNumberOfMonthsDuration(List<MembershipBuilder> membershipBuilderList) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilderDB = databaseWorld.getMembershipById(world.getCreatedMemberId());
        assertEquals(membershipBuilderList.iterator().next().getMonthsDuration().longValue(), membershipBuilderDB.getMonthsDuration().longValue());
    }

    @When("^requested getAllMembership with userId (\\d+)$")
    public void requestGetAllMembership(long userId) {
        world.setWebsiteMembership(membershipService.getAllMembership(userId));
    }

    @Then("^the resulting WebsiteMembership contains (\\d+) memberships$")
    public void websiteMembershipContains(long membershipCount) {
        assertNotNull(world.getWebsiteMembership());
        assertEquals(world.getWebsiteMembership().getWebsiteMembershipMap().size(), membershipCount);
    }

    @When("^requested to activate membership (\\d+) with bookingId (\\d+) and set balance equals to (\\d+) with authentication$")
    public void activateMembership(long membershipId, long bookingId, long balance) {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBalance(BigDecimal.valueOf(balance));
        activationRequest.setBookingId(bookingId);
        world.setMembershipResponse(membershipAuthenticatedService.activateMembership(membershipId, activationRequest));
    }

    @When("^activateMembership endpoint of MembershipService is called with null (bookingId|balance) in ActivationRequest an exception is thrown$")
    public void activateMembershipWithInvalidActivationRequestString(String nullProperty) {
        ActivationRequest activationRequest = new ActivationRequest();
        if ("bookingId".equals(nullProperty)) {
            activationRequest.setBalance(BigDecimal.ZERO);
        } else if ("balance".equals(nullProperty)) {
            activationRequest.setBookingId(500L);
        }

        try {
            membershipAuthenticatedService.activateMembership(1L, activationRequest);
        } catch (MembershipInternalServerErrorException e) {
            assertNotNull(e);
        }
    }


    @When("^requested to activate membership without authentication unauthorized exception is thrown$")
    public void activateMembershipWithoutAuthentication() {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBalance(BigDecimal.ZERO);
        activationRequest.setBookingId(500L);
        try {
            membershipService.activateMembership(1L, activationRequest);
        } catch (UnsupportedOperationException e) {
            assertNotNull(e);
            checkMembershipUnauthorizedExceptionExpected(e, UNAUTHORIZED_MESSAGE);
        }
    }

    @Then("^membership response has status (\\w+) and balance (\\d+)$")
    public void theRetrievedMembershipHasStatus(String expectedStatus, long expectedBalance) {
        assertEquals(world.getMembershipResponse().getStatus(), expectedStatus);
        assertEquals(world.getMembershipResponse().getBalance(), BigDecimal.valueOf(expectedBalance));
    }

    @When("a request to remind me later is received for membershipId (\\d+) set to (true|false)")
    public void setRemindMeLater(Long membershipId, Boolean value) {
        UpdateMembershipRequest updateRequest = new UpdateMembershipRequest();
        updateRequest.setOperation("SET_REMIND_ME_LATER");
        updateRequest.setMembershipId(String.valueOf(membershipId));
        updateRequest.setRemindMeLater(value);
        membershipService.updateMembership(updateRequest);
    }

    @Then("the membershipId (\\d+) has the remind me later flag set to (true|false)")
    public void checkMembershipRML(Long membershipId, Boolean expectedValue) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilder = databaseWorld.getMembershipById(membershipId);
        assertEquals(membershipBuilder.getRemindMeLater(), expectedValue);
    }

    @Then("the membershipId (\\d+) has the remind me later flag not set")
    public void checkMembershipRMLSetNull(Long membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilder = databaseWorld.getMembershipById(membershipId);
        // A null value from the db with come up as false all the way from the store-layer.
        assertFalse(membershipBuilder.getRemindMeLater());
    }

    private void verifySubscriptionPaymentMethod(Map<String, String> stringMap) {
        for (int i = 0; i < stringMap.size(); i++) {
            String membershipId = stringMap.get("membershipId");
            MemberSubscriptionDetails subscriptionDetails = world.getSubscriptionDetailsByMembershipId(Long.parseLong(membershipId));
            assertEquals(subscriptionDetails.getSubscriptionPaymentMethod().getCreditCardNumber(), stringMap.get("ccNumber"));
            assertTrue(sameDate(subscriptionDetails.getSubscriptionPaymentMethod(), stringMap.get("ccExpirationDate")));
        }
    }

    private boolean sameDate(CreditCard creditCard, String ccExpirationDate) {
        String creditCardDate = String.format("%s/%s", creditCard.getExpirationDateMonth(), creditCard.getExpirationDateYear());
        return creditCardDate.equalsIgnoreCase(ccExpirationDate);
    }

    private void verifyMembershipInfo(Map<String, String> stringMap) {
        assertEquals(world.getMembershipInfo().getName(), stringMap.get("name"));
        assertEquals(world.getMembershipInfo().getLastNames(), stringMap.get("lastNames"));
        assertEquals(world.getMembershipInfo().getMembershipId(), Long.parseLong(stringMap.get("membershipId")));
        assertEquals(world.getMembershipInfo().getMemberAccountId(), Long.parseLong(stringMap.get("membershipAccountId")));
        assertEquals(world.getMembershipInfo().getMembershipType(), stringMap.get("membershipType"));
        assertEquals(world.getMembershipInfo().getExpirationDate(), stringMap.get("expirationDate"));
        assertEquals(world.getMembershipInfo().getActivationDate(), stringMap.get("activationDate"));
        assertEquals(world.getMembershipInfo().getSourceType(), stringMap.get("sourceType"));
        assertEquals(world.getMembershipInfo().getCurrencyCode(), stringMap.get("currencyCode"));
        assertEquals(world.getMembershipInfo().getTotalPrice(), new BigDecimal(stringMap.get("totalPrice")));
        assertEquals(world.getMembershipInfo().getWebsite(), stringMap.get("website"));
        assertEquals(world.getMembershipInfo().getMembershipStatus(), stringMap.get("status"));
        assertEquals(world.getMembershipInfo().getAutoRenewalStatus(), stringMap.get("autoRenewal"));
        assertEquals(world.getMembershipInfo().getTimestamp(), stringMap.get("timestamp"));
        assertEquals(world.getMembershipInfo().getBookingIdSubscription(), Long.parseLong(stringMap.get("bookingIdSubscription")));
        assertEquals(world.getMembershipInfo().getLastStatusModificationDate(), stringMap.get("lastStatusModificationDate"));
    }

    private void verifyFutureFlight(FutureFlightBuilder stringMap) {
        assertTrue(world.getFutureFlights().stream()
                        .filter(f -> equalsDate(stringMap.getDepartureDate(), f.getDepartureDate()))
                        .filter(f -> stringMap.getDestination().equals(f.getDestination()))
                        .anyMatch(verified::add),
                String.format("%s %s not found in%n%s%n%s",
                        stringMap.getDepartureDate(),
                        stringMap.getDestination(),
                        world.getFutureFlights().stream().map(FutureFlight::getDepartureDate).collect(Collectors.joining(",")),
                        world.getFutureFlights().stream().map(FutureFlight::getDestination).collect(Collectors.joining(","))));

    }

    private static boolean equalsDate(String a, String b) {
        OffsetDateTime aDate = OffsetDateTime.parse(a, DateTimeFormatter.ISO_DATE_TIME);
        OffsetDateTime bDate = OffsetDateTime.parse(b, DateTimeFormatter.ISO_DATE_TIME);
        return OffsetDateTime.timeLineOrder().compare(aDate, bDate) == 0;
    }

    private void checkMembershipUnauthorizedExceptionExpected(Throwable wrapped, String exceptionMessage) {
        Throwable unwrapped = wrapped;
        boolean canBeUnwrapped = true;
        while (canBeUnwrapped) {
            if (unwrapped instanceof InvocationTargetException) {
                unwrapped = ((InvocationTargetException) unwrapped).getTargetException();
            } else if (unwrapped instanceof UndeclaredThrowableException) {
                unwrapped = ((UndeclaredThrowableException) unwrapped).getUndeclaredThrowable();
            } else {
                canBeUnwrapped = false;
            }
        }
        assertTrue(unwrapped.getMessage().contains(exceptionMessage));
    }
}
