package com.odigeo.membership;

import com.odigeo.commons.test.docker.Artifact;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.SocketPingChecker;
import com.odigeo.technology.docker.UrlPingChecker;

import java.util.Objects;

public class CustomContainerInfoBuilderFactory {

    private static final String REGISTRY_BASE_NAME = "eu.gcr.io/edo-test-resources";

    private static final String LOCALHOST = "localhost";

    private static final String ZOOKEEPER_NAME = "zookeeper";
    private static final int ZOOKEEPER_PORT = 2181;
    private static final String KAFKA_NAME = "broker.kafka";
    private static final int KAFKA_PORT = 9092;
    private static final int MAPPED_KAFKA_PORT = 29092;
    private static final String KAFKA_INITIALIZER_NAME = "kafka-initializer";
    private static final String KAFKA_INITIALIZER_VERSION = "1.1.4";
    private static final int KAFKA_INITIALIZER_PORT = 5555;
    private static final String SCHEMA_REGISTRY_NAME = "schema-registry";
    private static final int SCHEMA_REGISTRY_PORT = 8081;


    public static ContainerInfoBuilder newZookeeperKafka() throws ContainerException {
        return newStandaloneEdoKafka().addDependency(newZookeeper());
    }

    public static ContainerInfoBuilder newKafkaInitializerSchemaRegistry(Artifact artifact, String topic, String... additionalTopics) throws ContainerException {
        Objects.requireNonNull(topic);
        ContainerInfo kafkaInitializer = newKafkaInitializer(artifact, topic, additionalTopics).build();
        return newSchemaRegistry()
                .addDependency(kafkaInitializer);
    }

    private static ContainerInfo newZookeeper() throws ContainerException {
        return new ContainerInfoBuilder("confluentinc/cp-zookeeper:3.3.1")
                .setName(ZOOKEEPER_NAME)
                .addPortMapping(ZOOKEEPER_PORT, ZOOKEEPER_PORT)
                .addEvironmentVariable("ZOOKEEPER_CLIENT_PORT", String.valueOf(ZOOKEEPER_PORT))
                .setPingChecker(new SocketPingChecker(LOCALHOST, ZOOKEEPER_PORT))
                .build();
    }

    private static ContainerInfoBuilder newStandaloneEdoKafka() {
        return new ContainerInfoBuilder("confluentinc/cp-kafka:5.5.1")
                .setName(KAFKA_NAME)
                .addPortMapping(MAPPED_KAFKA_PORT, MAPPED_KAFKA_PORT)
                .addEvironmentVariable("KAFKA_ZOOKEEPER_CONNECT", "zookeeper:" + ZOOKEEPER_PORT)
                .addEvironmentVariable("KAFKA_BROKER_ID", "1")
                .addEvironmentVariable("KAFKA_ADVERTISED_LISTENERS", "DOCKER://broker.kafka:" + KAFKA_PORT + ",LOCALHOST://" + LOCALHOST + ":" + MAPPED_KAFKA_PORT)
                .addEvironmentVariable("KAFKA_LISTENER_SECURITY_PROTOCOL_MAP", "DOCKER:PLAINTEXT,LOCALHOST:PLAINTEXT")
                .addEvironmentVariable("KAFKA_INTER_BROKER_LISTENER_NAME", "DOCKER")
                .addEvironmentVariable("KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR", "1")
                .addEvironmentVariable("KAFKA_AUTO_CREATE_TOPICS_ENABLE", "false")
                .setPingChecker(new SocketPingChecker(LOCALHOST, MAPPED_KAFKA_PORT));
    }

    private static ContainerInfoBuilder newSchemaRegistry() {
        return new ContainerInfoBuilder("confluentinc/cp-schema-registry:5.5.1")
                .setName(SCHEMA_REGISTRY_NAME)
                .addPortMapping(SCHEMA_REGISTRY_PORT, SCHEMA_REGISTRY_PORT)
                .addEvironmentVariable("SCHEMA_REGISTRY_HOST_NAME", SCHEMA_REGISTRY_NAME)
                .addEvironmentVariable("SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS", "PLAINTEXT://broker.kafka:" + KAFKA_PORT)
                .addEvironmentVariable("SCHEMA_REGISTRY_SCHEMA_COMPATIBILITY_LEVEL", "forward_transitive")
                .setPingChecker(new UrlPingChecker(LOCALHOST, 8081));
    }

    private static ContainerInfoBuilder newKafkaInitializer(Artifact artifact, String topic, String... additionalTopics) throws ContainerException {
        Folder folder = new Folder();
        return new ContainerInfoBuilder(getRegistryImageNameWithVersion(KAFKA_INITIALIZER_NAME, KAFKA_INITIALIZER_VERSION))
                .setName(KAFKA_INITIALIZER_NAME)
                .addPortMapping(KAFKA_INITIALIZER_PORT, 80)
                .addEvironmentVariable("KAFKA_INITIALIZER_CREATE_TOPICS", getKafkaInitializerCreateTopicsEnvVariable(topic, additionalTopics))
                .addEvironmentVariable("KAFKA_INITIALIZER_KAFKA_URL", "broker.kafka:" + KAFKA_PORT)
                .addEvironmentVariable("KAFKA_INITIALIZER_ZOOKEEPER_URL", ZOOKEEPER_NAME + ":" + ZOOKEEPER_PORT)
                .addVolumeMapping(folder.createAndgetHostLogsFolderInDockerFormat(KAFKA_INITIALIZER_NAME, artifact.getUUID()), folder.getContainerLogsFolder())
                .setPingChecker(new UrlPingChecker(LOCALHOST, KAFKA_INITIALIZER_PORT));
    }


    private static String getKafkaInitializerCreateTopicsEnvVariable(String topic, String... additionalTopics) {
        String kafkaInitializerCreateTopics = topic;

        if (additionalTopics != null && additionalTopics.length > 0) {
            kafkaInitializerCreateTopics = String.join(",", kafkaInitializerCreateTopics, String.join(",", additionalTopics));
        }

        return kafkaInitializerCreateTopics;
    }

    private static String getRegistryImageNameWithVersion(String module, String version) {
        return String.format("%s/%s:%s", REGISTRY_BASE_NAME, module, version);
    }
}
