package com.odigeo.membership.mocks.recurringcollectionresource;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class RecurringCollectionResourceHttpServer extends JaxRsServiceHttpServer {
    private static final String PATH = "/recurring-collection";
    private static final int PORT = 58021;

    @Inject
    public RecurringCollectionResourceHttpServer() {
        super(PATH, PORT);
    }
}
