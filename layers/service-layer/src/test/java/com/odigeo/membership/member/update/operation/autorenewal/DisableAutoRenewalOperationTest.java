package com.odigeo.membership.member.update.operation.autorenewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertTrue;

public class DisableAutoRenewalOperationTest extends AutoRenewalChangeOperationTest {

    private DisableAutoRenewalOperation disableAutoRenewalOperation;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        disableAutoRenewalOperation = new DisableAutoRenewalOperation(dataSource, remindMelaterOperation, memberService, membershipStore, membershipMessageSendingManager);
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setRemindMeLater(Boolean.FALSE)
                .build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.updateAutoRenewal(eq(dataSource), eq(membership.getId()), eq(AutoRenewalOperation.DISABLE_AUTO_RENEW)))
                .thenReturn(Boolean.TRUE);
        assertTrue(disableAutoRenewalOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
        verifyNoInteractions(remindMelaterOperation);
    }

    @Test
    public void testUpdateWithRemindMeLater() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setRemindMeLater(Boolean.TRUE)
                .build();
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.updateAutoRenewal(eq(dataSource), eq(membership.getId()), eq(AutoRenewalOperation.DISABLE_AUTO_RENEW)))
                .thenReturn(Boolean.TRUE);
        assertTrue(disableAutoRenewalOperation.update(updateMembership));
        verify(membershipStore).updateAutoRenewal(eq(dataSource), eq(membership.getId()), eq(AutoRenewalOperation.DISABLE_AUTO_RENEW));
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
        UpdateMembership updateMembershipWithRML = UpdateMembership.builderCloneOf(updateMembership)
                .remindMeLater(Boolean.FALSE).build();
        verify(remindMelaterOperation).setRemindMeLater(eq(updateMembershipWithRML));
    }

    @Test
    public void testUpdateAlreadyDisabled() throws ExistingRecurringException, DataAccessException, MissingElementException {
        configureOperation(MembershipRenewal.DISABLED);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        assertTrue(disableAutoRenewalOperation.update(updateMembership));
        verifyNoInteractions(membershipMessageSendingManager, remindMelaterOperation, membershipStore);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateWithException() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        configureOperationException(MembershipRenewal.ENABLED, AutoRenewalOperation.DISABLE_AUTO_RENEW);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        disableAutoRenewalOperation.update(updateMembership);
    }
}