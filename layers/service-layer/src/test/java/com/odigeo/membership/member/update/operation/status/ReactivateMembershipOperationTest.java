package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static com.odigeo.membership.MemberStatus.ACTIVATED;
import static com.odigeo.membership.MemberStatus.DEACTIVATED;
import static com.odigeo.membership.MemberStatus.PENDING_TO_ACTIVATE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ReactivateMembershipOperationTest extends MembershipStatusChangeOperationTest {
    private ReactivateMembershipOperation reactivateMembershipOperation;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        reactivateMembershipOperation = new ReactivateMembershipOperation(dataSource, memberService, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, DataAccessException, MissingElementException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setStatus(DEACTIVATED)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        when(membershipStore.updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(ACTIVATED), eq(MembershipRenewal.ENABLED)))
                .thenReturn(TRUE);
        assertTrue(reactivateMembershipOperation.update(UpdateMembershipObjectMother.anUpdateMembership()));
        verify(membershipStore).updateStatusAndAutorenewal(eq(dataSource), eq(membership.getId()), eq(ACTIVATED), eq(MembershipRenewal.ENABLED));
        verifySuccessfulReactivation(membership);
        verifyZeroInteractions(remindMeLaterOperation);
    }

    @Test
    public void testUpdateNotActivatedMembership() throws ExistingRecurringException, DataAccessException, MissingElementException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setStatus(PENDING_TO_ACTIVATE)
                .setRemindMeLater(FALSE)
                .build();
        when(memberService.getMembershipByIdWithMemberAccount(membership.getId())).thenReturn(membership);
        UpdateMembership updateMembership = UpdateMembershipObjectMother.anUpdateMembership();
        assertFalse(reactivateMembershipOperation.update(updateMembership));
        verifyZeroInteractions(membershipStore, membershipMessageSendingManager, remindMeLaterOperation, memberStatusActionStore);
    }


    private void verifySuccessfulReactivation(Membership membership) throws SQLException {
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(membership.getId()));
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopicByRule(eq(DEACTIVATED), eq(ACTIVATED), eq(membership));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), eq(membership.getId()), eq(StatusAction.ACTIVATION));
    }

}