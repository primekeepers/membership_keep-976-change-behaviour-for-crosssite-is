package com.odigeo.membership.member.discount;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.member.discount.rule.ApplyDiscountRules;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiPredicate;

import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_PRIME_SHARE;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_TRAVELLERS;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_USER;
import static com.odigeo.membership.member.discount.rule.ApplyDiscountRules.RULES_WEBSITE;

public class ApplyDiscountChecker {

    @Inject
    static Map<String, Provider<ApplyDiscountRules>> rulesProvider;

    private final Provider<ApplyDiscountRules> isMember;
    private final Provider<ApplyDiscountRules> isWebsite;
    private final Provider<ApplyDiscountRules> isPrimeShare;
    private final Provider<ApplyDiscountRules> isTraveller;

    private final ApplyDiscountParameters applyDiscountParameters;
    private final ApplyDiscountContext applyDiscountContext;

    private ApplyDiscountChecker(ApplyDiscountParameters applyDiscountParameters, ApplyDiscountContext applyDiscountContext) {
        this.applyDiscountParameters = applyDiscountParameters;
        this.applyDiscountContext = applyDiscountContext;
        this.isMember = rulesProvider.get(RULES_USER);
        this.isWebsite = rulesProvider.get(RULES_WEBSITE);
        this.isPrimeShare = rulesProvider.get(RULES_PRIME_SHARE);
        this.isTraveller = rulesProvider.get(RULES_TRAVELLERS);
    }

    public static Builder of(ApplyDiscountParameters applyDiscountParameters) {
        return new Builder(applyDiscountParameters);
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> buildPredicate() {
        return isMember.get().and(isWebsite.get()).and(isPrimeShare.get().or(isTraveller.get()));
    }

    public boolean test() {
        return buildPredicate().test(applyDiscountParameters, applyDiscountContext);
    }

    public ApplyDiscountResult testAndGetResult() {
        return test() ? ApplyDiscountResult.applicable(getMembershipApplicable()) : ApplyDiscountResult.notApplicable(getReason());
    }

    private NotApplicationReason getReason() {
        return applyDiscountContext.getNotApplicationReason().stream().filter(Objects::nonNull).reduce((first, second) -> second).orElse(NotApplicationReason.UNKNOWN);
    }

    private Membership getMembershipApplicable() {
        return applyDiscountContext.getMembershipsEligible().stream().max(Comparator.comparing(Membership::getExpirationDate)).orElseThrow();
    }

    public static class Builder {
        private final ApplyDiscountParameters applyDiscountParameters;
        private final TestTokenUtils testTokenMatcher;

        public Builder(ApplyDiscountParameters applyDiscountParameters) {
            this.applyDiscountParameters = applyDiscountParameters;
            this.testTokenMatcher = TestTokenUtils.forTestDimensions(applyDiscountParameters.getTestDimensions());
        }

        public ApplyDiscountChecker withMemberships(Collection<Membership> memberships) {
            ApplyDiscountContext applyDiscountContext = new ApplyDiscountContext(testTokenMatcher, memberships);
            return new ApplyDiscountChecker(applyDiscountParameters, applyDiscountContext);
        }
    }

}
