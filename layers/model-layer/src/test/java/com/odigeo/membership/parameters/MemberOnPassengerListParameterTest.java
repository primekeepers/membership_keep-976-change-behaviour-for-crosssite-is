package com.odigeo.membership.parameters;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class MemberOnPassengerListParameterTest extends BeanTest<MemberOnPassengerListParameter> {

    @Override
    protected MemberOnPassengerListParameter getBean() {
        return assembleMemberOnPassengerListParameter();
    }

    @Test
    public MemberOnPassengerListParameter assembleMemberOnPassengerListParameter() {
        final MemberOnPassengerListParameter memberOnPassengerListParameter = new MemberOnPassengerListParameter();
        Long userId = 1234L;
        final List<TravellerParameter> travellerList = new ArrayList<>();
        TravellerParameter traveller1 = new TravellerParameter();
        traveller1.setName("NAME1");
        traveller1.setLastNames("LASTNAMES1");
        travellerList.add(traveller1);
        TravellerParameter traveller2 = new TravellerParameter();
        traveller2.setName("NAME2");
        traveller2.setLastNames("LASTNAMES2");
        travellerList.add(traveller2);
        memberOnPassengerListParameter.setUserId(userId);
        memberOnPassengerListParameter.setTravellerList(travellerList);
        memberOnPassengerListParameter.setSite("ES");
        return memberOnPassengerListParameter;
    }
}
