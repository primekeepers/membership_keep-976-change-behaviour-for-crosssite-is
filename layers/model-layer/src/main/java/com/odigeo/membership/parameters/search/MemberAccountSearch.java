package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.MemberAccountField;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.List.copyOf;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;

public class MemberAccountSearch extends SearchQueries {

    private final String name;
    private final String lastNames;
    private final Long userId;
    private final boolean withMemberships;
    private final String query;
    private final List<FilterCondition> filterConditions;

    public MemberAccountSearch(Builder builder) {
        name = builder.name;
        lastNames = builder.lastNames;
        userId = builder.userId;
        withMemberships = builder.withMembership;
        filterConditions = builder.filterConditions;
        query = queryCompiler(filterConditions);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(MemberAccountSearch memberAccountSearch) {
        Builder builder = new Builder();
        if (nonNull(memberAccountSearch)) {
            builder.userId(memberAccountSearch.userId)
                    .lastNames(memberAccountSearch.lastNames)
                    .name(memberAccountSearch.name)
                    .withMembership(memberAccountSearch.withMemberships);
        }
        return builder;
    }

    public List<Object> getValues() {
        return filterConditions.stream()
                .map(FilterCondition::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isEmpty() {
        return Arrays.stream(new Object[]{name, lastNames, userId}).noneMatch(Objects::nonNull);
    }

    public String getQueryString() {
        return query;
    }

    public boolean isWithMemberships() {
        return withMemberships;
    }

    protected List<FilterCondition> getFilterConditions() {
        return copyOf(filterConditions);
    }

    public static class Builder {
        private String name;
        private String lastNames;
        private Long userId;
        private List<FilterCondition> filterConditions;
        private boolean withMembership;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder withMembership(boolean withMembership) {
            this.withMembership = withMembership;
            return this;
        }

        private Builder filterConditions() {
            this.filterConditions = new ArrayList<>();
            List.of(
                    Pair.of(MemberAccountField.USER_ID, userId),
                    Pair.of(MemberAccountField.FIRST_NAME, name),
                    Pair.of(MemberAccountField.LAST_NAME, lastNames)
            ).forEach(pair -> Optional.ofNullable(pair.getValue())
                    .map(val -> new FilterCondition(pair.getKey(), val))
                    .ifPresent(filterConditions::add));
            return this;
        }

        public MemberAccountSearch build() {
            return new MemberAccountSearch(filterConditions());
        }
    }

    private String queryCompiler(List<FilterCondition> filterConditions) {
        StringBuilder queryBuilder = new StringBuilder(withMemberships ? SEARCH_MEMBERSHIPS_WITH_ACCOUNT : SEARCH_ACCOUNTS)
                .append(SqlKeywords.WHERE)
                .append(filterConditions.stream()
                        .map(FilterCondition::toString)
                        .collect(joining(SqlKeywords.AND.toString())));
        return withMemberships ? queryBuilder.append(SORT_BY_EXPDATE).toString()
                : queryBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberAccountSearch that = (MemberAccountSearch) o;
        return withMemberships == that.withMemberships && Objects.equals(name, that.name) && Objects.equals(lastNames, that.lastNames) && Objects.equals(userId, that.userId) && Objects.equals(query, that.query) && Objects.equals(filterConditions, that.filterConditions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastNames, userId, withMemberships, query, filterConditions);
    }
}
