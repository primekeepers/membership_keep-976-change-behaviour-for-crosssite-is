package com.odigeo.product.membership.service;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.common.base.Strings;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.membership.Membership;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.product.membership.service.util.ProductServiceMappingV2;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.response.exception.MembershipProductServiceException;
import com.odigeo.product.response.exception.MembershipProductServiceExceptionType;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.CloseProductProviderRequest;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.enums.CloseActionResult;
import com.odigeo.product.v2.model.enums.ProductType;
import com.odigeo.product.v2.model.enums.ProviderProductStatus;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class ProductServiceV2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceV2.class);
    private static final String MESSAGE_LOG_DEFAULT_CALL_FUNCTION = "Call function: {} with ProductId: {} and TransactionProductId {}";
    private static final String MESSAGE_LOG_RETRIEVE_FEES_LOCALLY = "Retrieving fees from local database for the product ID: {}";
    private static final String MESSAGE_LOG_RETRIEVE_FEES_FROM_SERVICE = "Retrieving fees from fees-service for the fee container ID: {}";
    private static final String MESSAGE_LOG_PRODUCT_IDENTIFIER_NOT_FOUND = "The product identifier must not be empty or null";
    static final String MESSAGE_LOG_DEFAULT_EXCEPTION = "Caught exception: {}";
    static final String ROLLBACK_TRANSACTION_PRODUCT = "rollbackTransactionProduct";
    static final String CANCEL_TRANSACTION_PRODUCT = "cancelTransactionProduct";
    static final String SUSPEND_TRANSACTION_PRODUCT = "suspendTransactionProduct";
    static final String RESUME_TRANSACTION_PRODUCT = "resumeTransactionProduct";

    private final MemberService memberService;
    private final MembershipProductService membershipProductService;
    private final MembershipFeesService membershipFeesService;

    public ProductServiceV2(MemberService memberService,
                            MembershipProductService membershipProductService, MembershipFeesService membershipFeesService) {
        this.memberService = memberService;
        this.membershipProductService = membershipProductService;
        this.membershipFeesService = membershipFeesService;
    }

    public abstract Product getProduct(String productId) throws ProductException;

    public abstract void saveProduct(String productId, String productProviderId, boolean isPartial) throws ProductException;

    void logAndValidateProductId(String function, String productId, String transactionProductId) throws ProductException {
        defaultLoggerMessage(function, productId, transactionProductId);
        validateProductId(productId);
    }

    protected void defaultLoggerMessage(String function, String productId, String transactionProductId) {
        LOGGER.info(MESSAGE_LOG_DEFAULT_CALL_FUNCTION,
                function,
                productId,
                transactionProductId);
    }

    private void validateProductId(String productId) throws ProductException {
        if (Strings.isNullOrEmpty(productId)) {
            LOGGER.error(MESSAGE_LOG_PRODUCT_IDENTIFIER_NOT_FOUND);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.BAD_REQUEST, productId, ProductType.MEMBERSHIP_RENEWAL, MESSAGE_LOG_PRODUCT_IDENTIFIER_NOT_FOUND);
        }
    }

    Product getProductFromDatabase(String productId) throws MissingElementException, DataAccessException {
        Membership membership = memberService.getMembershipById(Long.parseLong(productId));
        return ProductServiceMappingV2.asProduct(membership, getFees(membership.getFeeContainerId(), productId));
    }

    private List<AbstractFee> getFees(Long feeContainerId, String productId) throws DataAccessException {
        if (Objects.isNull(feeContainerId)) {
            LOGGER.info(MESSAGE_LOG_RETRIEVE_FEES_LOCALLY, productId);
            return membershipProductService.getMembershipFees(Long.parseLong(productId))
                    .stream()
                    .map(membershipFee -> membershipFeesService.fillProductFee(membershipFee.getAmount(), membershipFee.getCurrency()))
                    .collect(Collectors.toList());
        } else {
            LOGGER.info(MESSAGE_LOG_RETRIEVE_FEES_FROM_SERVICE, feeContainerId);
            return membershipFeesService.retrieveFees(feeContainerId);
        }
    }

    protected void validateProductInContract(Membership membership) throws ProductException {
        ProductStatus productStatus = membership.getProductStatus();
        if (ProductStatus.CONTRACT != productStatus) {
            String messageError = "Can't activate a membership that is not in CONTRACT status. Id: " + membership.getId() + ", status: " + productStatus;
            LOGGER.error(messageError);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.FORBIDDEN, String.valueOf(membership.getId()), ProductType.MEMBERSHIP_RENEWAL, messageError);
        }
    }

    public void commitTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId("commitTransactionProduct", productId, transactionProductId);

        try {
            Membership membershipToCommit = getMemberService().getMembershipById(Long.parseLong(productId));
            ProductStatus productStatus = membershipToCommit.getProductStatus();
            if (!ProductStatus.INIT.equals(productStatus)) {
                String messageError = "Can't commit an order that is not in INIT status. Id: " + productId + ", status: " + productStatus;
                LOGGER.error(messageError);
                throw new MembershipProductServiceException(MembershipProductServiceExceptionType.FORBIDDEN, productId, ProductType.MEMBERSHIP_RENEWAL, messageError);
            }
            if (commitMembership(membershipToCommit)) {
                MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.RENEWAL_CONTRACT_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
            }
        } catch (MissingElementException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        } catch (DataAccessException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        }
    }

    protected abstract boolean commitMembership(Membership membership) throws MissingElementException, DataAccessException;

    public void rollbackTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(ROLLBACK_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public void cancelTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(CANCEL_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public void suspendTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(SUSPEND_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public void resumeTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(RESUME_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public BookingApiMembershipInfo getBookingApiProductInfo(String productId) throws ProductException {
        LOGGER.info("Call function: getBookingApiProductInfo with ProductId: {}", productId);
        validateProductId(productId);
        try {
            Membership membership = memberService.getMembershipById(Long.parseLong(productId));
            return ProductServiceMappingV2.asBookingApiMembershipInfo(membership, getFees(membership.getFeeContainerId(), productId));
        } catch (DataNotFoundException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        } catch (MissingElementException | DataAccessException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        }
    }

    public CloseProviderProductResponse closeProviderProduct(String productId, String transactionProductId, CloseProductProviderRequest request) {
        LOGGER.info("Call function: closeProviderProduct with ProductId: {}, transactionProductId: {}, request: {}", productId, transactionProductId, request);
        CloseProviderProductResponse closeProviderProductResponse = new CloseProviderProductResponse();
        closeProviderProductResponse.setCloseActionResult(CloseActionResult.OK);
        closeProviderProductResponse.setProviderProductStatus(ProviderProductStatus.CONTRACT);
        closeProviderProductResponse.setRollbackable(false);
        closeProviderProductResponse.setUserInteractionNeeded(null);
        return closeProviderProductResponse;
    }

    MemberService getMemberService() {
        return memberService;
    }
}
