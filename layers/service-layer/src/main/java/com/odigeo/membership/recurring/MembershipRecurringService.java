package com.odigeo.membership.recurring;

import java.util.UUID;

public interface MembershipRecurringService {

    UUID createMembershipRecurringCollection(MembershipRecurringCreation membershipRecurringCreation);
}
