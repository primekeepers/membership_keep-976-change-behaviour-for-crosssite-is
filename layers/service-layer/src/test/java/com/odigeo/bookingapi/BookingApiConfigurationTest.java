package com.odigeo.bookingapi;

import bean.test.BeanTest;

public class BookingApiConfigurationTest extends BeanTest<BookingApiConfiguration> {

    @Override
    protected BookingApiConfiguration getBean() {
        return new BookingApiConfiguration();
    }
}