package com.odigeo.userapi;

import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.HashCode;
import com.odigeo.userprofiles.api.v2.model.HashType;
import com.odigeo.userprofiles.api.v2.model.UserBasicInfo;
import com.odigeo.userprofiles.api.v2.UserApiServiceInternal;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InternalServerException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidFindException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import com.odigeo.userprofiles.api.v2.utils.BasicAuth;
import org.jboss.resteasy.client.core.BaseClientResponse;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

import static com.odigeo.userprofiles.api.v2.model.Status.ACTIVE;
import static com.odigeo.userprofiles.api.v2.model.Status.PENDING_LOGIN;
import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class UserApiManagerTest {

    private static final List<String> TOKENS = asList("firstToken", "secondToken");
    private static final Long USER_ID = 8373L;
    private static final String EMAIL = "test@test.com";
    private static final String INVALID_WEBSITE = "ed.es";
    private static final String WEBSITE_ES = "ES";
    private static final Brand BRAND = Brand.ED;
    private static final String USER = "user";
    private static final String PASSWORD = "password";

    @Mock
    private UserApiServiceInternal userApiServiceInternal;
    @Mock
    private UserProfileApiSecurityConfiguration configuration;

    private final String basicAuth = new BasicAuth(USER, PASSWORD).getAuthorizationHeader();
    private UserApiManager userApiManager;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(configuration.getUser()).thenReturn(USER);
        when(configuration.getPassword()).thenReturn(PASSWORD);
        userApiManager = new UserApiManager(userApiServiceInternal, configuration);
    }

    @Test
    public void testForgetPasswordTokenReturnsValidToken() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException {
        //Given
        List<HashCode> hashCodes = sampleHashCodes();
        BaseClientResponse response = mock(BaseClientResponse.class);
        when(response.getEntity(any(UserApiManager.HashCodeListType.class))).thenReturn(hashCodes);
        given(userApiServiceInternal.getHashCodes(basicAuth, USER_ID, HashType.REQUEST_PASSWORD))
            .willReturn(response);
        //When
        Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(USER_ID);
        //Then
        assertTrue(forgetPasswordToken.isPresent());
        assertTrue(TOKENS.contains(forgetPasswordToken.get()));
    }

    @Test
    public void testForgetPasswordTokenReturnsNoToken() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userApiServiceInternal.getHashCodes(basicAuth, USER_ID, HashType.REQUEST_PASSWORD))
            .willReturn(BaseClientResponse.noContent().build());
        //When
        Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(USER_ID);
        //Then
        assertFalse(forgetPasswordToken.isPresent());
    }

    @Test
    public void testForgetPasswordTokenReturnsNoTokenWhenException() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userApiServiceInternal.getHashCodes(basicAuth, USER_ID, HashType.REQUEST_PASSWORD)).willThrow(new InternalServerException("expected exception"));
        //When
        Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(USER_ID);
        //Then
        assertFalse(forgetPasswordToken.isPresent());
    }

    @Test
    public void testGetUserInfoHasDefaultValuesWhenIncorrectWebsite() {
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, INVALID_WEBSITE);
        //Then
        assertEquals(userInfo.getUserId(), Optional.empty());
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasDefaultValuesWhenInternalServerException() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, EMAIL, BRAND)).willThrow(new InternalServerException("expected exception"));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertEquals(userInfo.getUserId(), Optional.empty());
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasDefaultValuesWhenNullUserBasicInfo() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, EMAIL, BRAND)).willReturn(Response.noContent().build());
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertEquals(userInfo.getUserId(), Optional.empty());
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasUserBasicInfoValuesWithPendingLogin() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        UserBasicInfo userBasicInfo = sampleUserBasicInfo(true);
        BaseClientResponse response = mock(BaseClientResponse.class);
        when(response.getEntity(UserBasicInfo.class)).thenReturn(userBasicInfo);
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, EMAIL, BRAND))
            .willReturn(response);
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertTrue(userInfo.getUserId().isPresent());
        assertEquals(userInfo.getUserId().get(), USER_ID);
        assertTrue(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasUserBasicInfoValuesWithNoPendingLogin() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        UserBasicInfo userBasicInfo = sampleUserBasicInfo(false);
        BaseClientResponse response = mock(BaseClientResponse.class);
        when(response.getEntity(UserBasicInfo.class)).thenReturn(userBasicInfo);
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, EMAIL, BRAND))
            .willReturn(response);
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertTrue(userInfo.getUserId().isPresent());
        assertEquals(userInfo.getUserId().get(), USER_ID);
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoByUserId() throws InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        UserBasicInfo userBasicInfo = sampleUserBasicInfo(false);
        BaseClientResponse response = mock(BaseClientResponse.class);
        when(response.getEntity(UserBasicInfo.class)).thenReturn(userBasicInfo);
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, USER_ID))
            .willReturn(response);
        //When
        UserInfo userInfo = userApiManager.getUserInfo(USER_ID);
        //Then
        assertEquals(userInfo.getEmail(), EMAIL);
    }

    @Test
    public void testGetUserInfoByUserIdThrowsInternalServerException() throws InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, USER_ID))
            .willThrow(new InternalServerException("expected exception"));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(USER_ID);
        //Then
        assertNull(userInfo.getEmail());
    }

    @Test
    public void testGetUserInfoByUserIdThrowsGenericException() throws InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, USER_ID)).willThrow(new RuntimeException("expected exception"));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(USER_ID);
        //Then
        assertNull(userInfo.getEmail());
    }

    @Test
    public void testGetEmail() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException, DataNotFoundException {
        UserBasicInfo userBasicInfo = sampleUserBasicInfo(false);
        BaseClientResponse response = mock(BaseClientResponse.class);
        when(response.getEntity(UserBasicInfo.class)).thenReturn(userBasicInfo);
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, USER_ID))
            .willReturn(response);
        //When
        String email = userApiManager.getEmail(USER_ID);
        //Then
        assertEquals(email, EMAIL);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetEmail_nullEmail() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException, DataNotFoundException {
        given(userApiServiceInternal.getUserBasicInfo(basicAuth, USER_ID)).willReturn(Response.ok(new UserBasicInfo()).build());

        //When
        userApiManager.getEmail(USER_ID);
    }

    private UserBasicInfo sampleUserBasicInfo(boolean isPendingLogin) {
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setId(USER_ID);
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setStatus(isPendingLogin ? PENDING_LOGIN : ACTIVE);
        return userBasicInfo;
    }

    private List<HashCode> sampleHashCodes() {
        HashCode firstHashCode = new HashCode();
        firstHashCode.setCode(TOKENS.get(0));
        HashCode secondHashCode = new HashCode();
        secondHashCode.setCode(TOKENS.get(1));
        return asList(firstHashCode, secondHashCode);
    }
}