package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.exception.AlreadyHasPrimeMembershipException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.member.memberapi.converter.CreateMembershipConverter;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MembershipCreationRequestMapper;
import com.odigeo.membership.member.memberapi.v1.util.MembershipCreationRequestValidator;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.member.rest.utils.ValidatorRequestAdapter;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.crm.CrmUserInfo;
import com.odigeo.membership.request.membership.ApplyItineraryDiscountRequest;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.WebsiteMembership;
import com.odigeo.membership.response.membership.ApplyItineraryDiscountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.v1.MembershipService;
import com.odigeo.tracking.client.track.method.Track;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MembershipServiceController extends AbstractController implements MembershipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipServiceController.class);
    private static final String WITH_MEMBERSHIP_ID = "- membershipId: ";
    private static final String WITH_USER_ID = "- userId: ";
    private static final String WITH_USER = "- user: ";
    private static final String WITH_BOOKING_ID = "- bookingId: ";
    private final MapperFacade mapperFacade;

    @SuppressWarnings("unused")
    public MembershipServiceController() {
        this.mapperFacade = new GeneralMapperCreator().getMapper();
    }

    MembershipServiceController(MapperFacade mapperFacade) {
        this.mapperFacade = mapperFacade;
    }

    @Override
    @Track(name = "getMembership")
    public Membership getMembership(Long userId, String website) {
        try {
            return Optional.ofNullable(getMemberAccountService().getMembershipAccount(userId, website))
                    .map(getMembershipServiceMapper()::asMembershipResponse)
                    .orElse(null);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER_ID + userId + " website: " + website);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipResponse getCurrentMembership(Long userId, String website) {
        return Optional.ofNullable(ConfigurationEngine.getInstance(SearchService.class))
                .flatMap(searchService -> {
                    try {
                        return searchService.getCurrentMembership(userId, website);
                    } catch (DataAccessException e) {
                        LogUtils.defaultLoggerError(LOGGER, e);
                        throw MembershipExceptionMapper.map(e);
                    }
                })
                .map(currentMembership -> mapperFacade.map(currentMembership, MembershipResponse.class))
                .orElse(null);
    }

    @Override
    public MembershipResponse activateMembership(long membershipId, ActivationRequest activateRequest) {
        ValidatorRequestAdapter.applyValidation(activateRequest);
        try {
            getMemberService().activateMembership(membershipId, activateRequest.getBookingId(), activateRequest.getBalance());
            return Optional.of(getMemberService().getMembershipById(membershipId))
                    .map(membership -> mapperFacade.map(membership, MembershipResponse.class))
                    .orElse(null);
        } catch (DataAccessException | MissingElementException | UserApiException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean isEligibleForFreeTrial(FreeTrialCandidateRequest freeTrialCandidateRequest) throws MembershipServiceException {
        ValidatorRequestAdapter.applyValidation(freeTrialCandidateRequest);
        try {
            return getMembershipValidationService().isEligibleForFreeTrial(getMembershipServiceMapper()
                    .mapFreeTrialCandidate(freeTrialCandidateRequest));
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    @Track(name = "getAllMembership")
    public WebsiteMembership getAllMembership(Long userId) {
        try {
            WebsiteMembership websiteMembershipMap = new WebsiteMembership();
            websiteMembershipMap.setWebsiteMembershipMap(
                    Optional.of(getMemberAccountService().getActiveMembersByUserId(userId))
                            .map(getMembershipServiceMapper()::asWebsiteMemberMap)
                            .orElse(Collections.emptyMap()));
            return websiteMembershipMap;
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER_ID + userId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    @Track(name = "applyMembership")
    public Boolean applyMembership(CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) {
        ValidatorRequestAdapter.applyValidation(checkMemberOnPassengerListRequest);
        Boolean result = Boolean.FALSE;
        MemberOnPassengerListParameter memberOnPassengerListParameter = getMembershipServiceMapper()
                .asPassengerList(checkMemberOnPassengerListRequest);
        try {
            result = getMembershipValidationService().applyMembership(memberOnPassengerListParameter);
        } catch (MissingElementException e) {
            LogUtils.smallLog(LOGGER, "MissingElementException checking member on PassengerList:", e);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER_ID + checkMemberOnPassengerListRequest.getUserId());
            throw MembershipExceptionMapper.map(e);
        }
        return result;
    }

    @Override
    @Track(name = "applyItineraryDiscount")
    public ApplyItineraryDiscountResponse applyItineraryDiscount(ApplyItineraryDiscountRequest applyItineraryDiscountRequest) {

        ApplyDiscountParameters applyDiscountParameters = getMembershipMapper().toApplyDiscountParameters(applyItineraryDiscountRequest);

        ApplyDiscountResult applyDiscountResult;
        try {
            applyDiscountResult = getMembershipValidationService().applyMembership(applyDiscountParameters);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER + applyItineraryDiscountRequest.getUser());
            throw MembershipExceptionMapper.map(e);
        }

        return getMembershipMapper().toApplyItineraryDiscountResponse(applyDiscountResult);

    }

    @Override
    public MembershipStatus disableMembership(Long membershipId) {
        try {
            UpdateMembership updateMembership = UpdateMembership.builder()
                    .membershipId(String.valueOf(membershipId))
                    .operation(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP.name())
                    .build();
            return new MembershipStatus(getUpdateMembershipService().deactivateMembership(updateMembership));
        } catch (MissingElementException | DataAccessException | ExistingRecurringException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipRenewal disableAutoRenewal(Long membershipId) {
        try {
            UpdateMembership updateMembership = UpdateMembership.builder()
                    .membershipId(String.valueOf(membershipId))
                    .operation(UpdateMembershipAction.DISABLE_AUTO_RENEWAL.name())
                    .build();
            return new MembershipRenewal(getUpdateMembershipService().disableAutoRenewal(updateMembership));
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipRenewal enableAutoRenewal(Long membershipId) {
        try {
            return new MembershipRenewal(getUpdateMembershipService().enableAutoRenewal(UpdateMembership.builder()
                    .membershipId(String.valueOf(membershipId))
                    .operation(UpdateMembershipAction.ENABLE_AUTO_RENEWAL.name())
                    .build()));
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipStatus reactivateMembership(Long membershipId) {
        try {
            UpdateMembership updateMembership = UpdateMembership.builder()
                    .membershipId(String.valueOf(membershipId))
                    .operation(UpdateMembershipAction.REACTIVATE_MEMBERSHIP.name())
                    .build();
            String result = getUpdateMembershipService().reactivateMembership(updateMembership);
            return new MembershipStatus(result);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean isMembershipPerksActiveOn(String siteId) {
        return getMembershipValidationService().isMembershipActiveOnWebsite(siteId);
    }

    @Override
    @Track(name = "createMembership", converter = CreateMembershipConverter.class)
    public Long createMembership(CreateMembershipRequest createMembershipRequest) {
        try {
            ConfigurationEngine.getInstance(MembershipCreationRequestValidator.class)
                    .validateMembershipRequestParameters(createMembershipRequest);
            MembershipCreation membershipCreation = MembershipCreationRequestMapper
                    .getMembershipCreationFromRequestFields(createMembershipRequest.getAsMap());
            return getMemberService().createMembership(membershipCreation);
        } catch (DataAccessException | MissingElementException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean updateMemberAccount(UpdateMemberAccountRequest updateMemberAccountRequest) {
        try {
            return getMemberAccountService().updateMemberAccount(mapperFacade.map(updateMemberAccountRequest, UpdateMemberAccount.class));
        } catch (DataAccessException | MissingElementException | UserAccountNotFoundException | AlreadyHasPrimeMembershipException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean updateMembership(UpdateMembershipRequest updateMembershipRequest) {
        try {
            return getUpdateMembershipService().updateMembership(getUpdateMembershipMapper().map(updateMembershipRequest));
        } catch (DataAccessException | MissingElementException | ExistingRecurringException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public List<BlackListedPaymentMethod> getBlacklistedPaymentMethods(Long membershipId) {
        try {
            return mapperFacade.mapAsList(getBlacklistPaymentService().getBlacklistedPaymentMethods(membershipId).toArray(), BlackListedPaymentMethod.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean addToBlackList(Long membershipId, AddToBlackListRequest addToBlackListRequest) {
        try {
            return getBlacklistPaymentService().addToBlackList(getMembershipServiceMapper()
                    .mapToBlackListedPaymentMethods(membershipId, addToBlackListRequest.getBlackListedPaymentMethods()));
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean isMembershipToBeRenewed(Long membershipId) {
        try {
            return getMembershipValidationService().isMembershipToBeRenewed(membershipId);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipBookingTracking getBookingTracking(long bookingId) {
        try {
            return mapperFacade.map(getBookingTrackingService()
                    .getMembershipBookingTracking(bookingId), MembershipBookingTracking.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_BOOKING_ID + bookingId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipBookingTracking addBookingTrackingUpdateBalance(MembershipBookingTrackingRequest membershipBookingTrackingRequest) {
        try {
            return mapperFacade.map(getBookingTrackingService()
                    .insertMembershipBookingTracking(mapperFacade.map(membershipBookingTrackingRequest, BookingTracking.class)), MembershipBookingTracking.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_BOOKING_ID + membershipBookingTrackingRequest.getBookingId()
                    + " membershipId " + membershipBookingTrackingRequest.getMembershipId());
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public void deleteBookingTrackingUpdateBalance(long bookingId) {
        try {
            getBookingTrackingService().deleteMembershipBookingTracking(bookingId);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_BOOKING_ID + bookingId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public String encrypt(CrmUserInfo userInfo) throws MembershipServiceException {
        try {
            return getCipher().encryptMessage(userInfo.asUrlParams());
        } catch (UnsupportedEncodingException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public String decrypt(String token) throws MembershipServiceException {
        try {
            return getCipher().decryptToken(token);
        } catch (UnsupportedEncodingException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }


}
