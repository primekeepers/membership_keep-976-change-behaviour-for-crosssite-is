
package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.Optional;

public class UpdateMembershipTypeOperation implements MembershipUpdateOperation {
    private final DataSource dataSource;
    private final MembershipStore membershipStore;
    private final MembershipMessageSendingManager membershipMessageSendingManager;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        UpdateMembershipTypeOperation createOperation(DataSource dataSource);
    }

    @Inject
    public UpdateMembershipTypeOperation(@Assisted DataSource dataSource,
                                         MembershipStore membershipStore,
                                         MembershipMessageSendingManager membershipMessageSendingManager) {
        this.membershipStore = membershipStore;
        this.dataSource = dataSource;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        final long membershipId = Long.parseLong(updateMembership.getMembershipId());
        final MembershipType membershipType = MembershipType.valueOf(updateMembership.getMembershipType());
        final BigDecimal renewalPrice = Optional.ofNullable(updateMembership.getRenewalPrice()).orElseThrow(InvalidParameterException::new);
        boolean isUpdateSuccessful;
        try {
            isUpdateSuccessful = membershipStore.updateTypeAndRenewalPrice(dataSource, membershipId, membershipType, renewalPrice);
        } catch (SQLException e) {
            throw new DataAccessException("Update membership type for membership id " + membershipId + "failed : ", e);
        }
        if (isUpdateSuccessful) {
            membershipMessageSendingManager.sendMembershipIdToMembershipReporter(membershipId);
        }
        return isUpdateSuccessful;
    }
}
