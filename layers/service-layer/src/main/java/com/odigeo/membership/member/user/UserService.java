package com.odigeo.membership.member.user;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.UserDTO;

import java.util.Optional;

public interface UserService {

    Long saveUser(UserCreation userCreation) throws DataAccessException;

    Long getUserIdBy(String email, String website) throws UserAccountNotFoundException, DataAccessException;

    Long saveUser(UserDTO userDTO) throws DataAccessException;

    UserDTO getUser(long userId);

    Optional<Long> fixPendingMailConfirmationUserAccount(long userId) throws DataAccessException, MissingElementException, UserApiException;
}
