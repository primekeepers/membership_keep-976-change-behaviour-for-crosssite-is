<%@ page import="com.edreams.persistance.cache.Cache" %>
<%@ page import="com.edreams.persistance.cache.impl.CacheRegistry" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<HTML>
<head>
    <title>eDreams JBoss Caches Status</title>
    <style type="text/css">

        .success {
            background-color: lawngreen;
        }

        .failure {
            background-color: tomato;
        }

        .attempts {
            background-color: gainsboro;
        }

        .adds {
            background-color: gainsboro;
        }
    </style>
    <script language="Javascript">
        function cacheAction(strURL) {
            var xmlHttpReq = false;
            var self = this;
            // Mozilla/Safari
            if (window.XMLHttpRequest) {
                self.xmlHttpReq = new XMLHttpRequest();
            }
            // IE
            else if (window.ActiveXObject) {
                self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            self.xmlHttpReq.open('GET', strURL, true);
            self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            self.xmlHttpReq.onreadystatechange = function () {
                if (self.xmlHttpReq.readyState == 4) {
                    location.reload(true);
                }
            };

            self.xmlHttpReq.send(null);
        }
    </script>
</head>
<BODY>


<%
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    numberFormat.setMinimumFractionDigits(2);
    numberFormat.setMaximumFractionDigits(2);

    Iterator<Cache> itCaches = CacheRegistry.getInstance().iterator();
%>

<br>
<TABLE BORDER="1">
    <thead>
    <TR align="">
        <TH align="left" colspan=17>CACHES STATUS</TH>
    </TR>
    </thead>
    <TR>
        <TH colspan="4"></TH>
        <TH colspan="5">Cache</TH>
        <TH colspan="5">Miss Cache</TH>
        <TH colspan="1"></TH>
    </TR>
    <TR>
        <TH>ID</TH>
        <TH>Name</TH>
        <TH>StartDate</TH>
        <TH>ClassName</TH>
        <TH class="success">Success</TH>
        <TH class="failure">Failure</TH>
        <TH class="attempts">Adds</TH>
        <TH class="attempts">Attempts</TH>
        <TH>CacheSize</TH>
        <TH class="success">Success</TH>
        <TH class="failure">Failure</TH>
        <TH class="attempts">Adds</TH>
        <TH class="attempts">Attempts</TH>
        <TH>CacheSize</TH>
        <TH>Actions</TH>
    </TR>
    <%
        while (itCaches.hasNext()) {
            Cache cache = itCaches.next();
    %>

    <TR>
        <TD>
            <%=cache.getId()%>
        </TD>
        <TD>
            <%=cache.getStorageSpaceName()%>
        </TD>
        <TD>
                <%=dateFormat.format(cache.getStartDate())%>
        <TD>
            <%=cache.getClassName()%>
        </TD>
        <TD class="success" align="right">
            <%=numberFormat.format(cache.getSuccessRatio())%>
        </TD>
        <TD class="failure" align="right">
            <%=numberFormat.format(cache.getFailureRatio())%>
        </TD>
        <TD class="adds" align="right">
            <%=cache.getAdds()%>
        </TD>
        <TD class="attempts" align="right">
            <%=cache.getAttempts()%>
        </TD>
        <TD align="right">
            <%=cache.getCacheSize()%>
        </TD>
        <TD class="success" align="right">
            <%=numberFormat.format(cache.getMissedSuccessRatio())%>
        </TD>
        <TD class="failure" align="right">
            <%=numberFormat.format(cache.getMissedFailureRatio())%>
        </TD>
        <TD class="adds" align="right">
            <%=cache.getMissedAdds()%>
        </TD>
        <TD class="attempts" align="right">
            <%=cache.getMissedAttempts()%>
        </TD>
        <TD>
            <%=cache.getMissedCacheSize()%>
        </TD>
        <TD>
            <% if (cache.isClearable()) { %>
            <input id="start<%=cache.getId()%>" value="Clear Cache" type="button" onclick="cacheAction('<%=request.getContextPath()%>/engineering/cache/CacheAdmin/clearCache?cacheName=<%=cache.getStorageSpaceName()%>')">
            <% } %>
        </TD>
    </TR>

    <%
        }
    %>

</TABLE>
<br/><br/>

<TABLE BORDER="1">
    <thead>
    <TR>
        <TH colspan=2>Caches Borradas (Garbage)</TH>
    </TR>
    </thead>
    <TR>
        <TH>ClassName</TH>
        <TH>Count</TH>
    </TR>

    <%
        //CHECK GARBAGE CACHES
        Map<String, Integer> garbageCaches = CacheRegistry.getInstance().getGarbagedLog();

        if (garbageCaches != null && !garbageCaches.isEmpty()) {
            for (Map.Entry<String, Integer> cacheEntry : garbageCaches.entrySet()) {
    %>
    <TR>
        <TD>
            <%=cacheEntry.getKey()%>
        </TD>
        <TD>
            <%=cacheEntry.getValue()%>
        </TD>
    </TR>
    <%
        }
    } else {

    %>

    <TR>
        <TD colspan="2" align="center">
            NINGUNA
        </TD>
    </TR>

</TABLE>

<%
    }
%>
</BODY>
</HTML>
