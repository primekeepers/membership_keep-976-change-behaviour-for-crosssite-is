package com.odigeo.membership.recurring;

import bean.test.BeanTest;

public class MembershipRecurringCreationTest extends BeanTest<MembershipRecurringCreation> {

    private static final String MEMBERSHIP_ID = "8";
    private static final String TYPE = "MEMBERSHIP_SUBSCRIPTION";
    private static final String WEBSITE = "IT";
    private static final String USER_ID = "1";

    @Override
    protected MembershipRecurringCreation getBean() {
        return MembershipRecurringCreation.builder()
                .membershipId(MEMBERSHIP_ID).type(TYPE)
                .website(WEBSITE).userId(USER_ID).build();
    }
}