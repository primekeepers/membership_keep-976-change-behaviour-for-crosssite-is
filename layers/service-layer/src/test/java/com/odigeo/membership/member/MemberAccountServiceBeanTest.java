package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.v14.InvalidParametersException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.exception.AlreadyHasPrimeMembershipException;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.member.update.MemberAccountUserIdUpdater;
import com.odigeo.membership.member.update.MembershipTransferMarketingHandler;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.tracking.BookingTrackingService;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.odigeo.membership.UpdateMemberAccountOperation.UPDATE_NAMES;
import static com.odigeo.membership.UpdateMemberAccountOperation.UPDATE_USER_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MemberAccountServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private BookingTrackingService bookingTrackingService;
    @Mock
    private MemberAccountUserIdUpdater memberAccountUserIdUpdater;
    @Mock
    private UserService userService;
    @Mock
    private MembershipTransferMarketingHandler membershipTransferMarketingHandler;

    @InjectMocks
    private MemberAccountServiceBean memberAccountServiceBean = new MemberAccountServiceBean();

    private static final long USER_ID = 123L;
    private static final long NEW_USER_ID = 678L;
    private static final long MEMBERSHIP_ID1 = 10L, MEMBERSHIP_ID2 = 11L, MEMBERSHIP_ID3 = 12L, MEMBERSHIP_ID4 = 13L, MEMBERSHIP_ID5 = 5L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L, MEMBER_ACCOUNT_ID2 = 2L, MEMBER_ACCOUNT_ID3 = 3L;
    private static final String WEBSITE_ES = "ES";
    private static final String WEBSITE_FR = "FR";
    private static final String WEBSITE_DE = "DE";
    private static final String MEMBER_EMAIL1 = "jose.luis69@gmail.com";
    private static final String MEMBER_BAD_EMAIL = "jose.ferreira222@gmail.com";
    private static final String MEMBER_NAME1 = "JOSE";
    private static final String MEMBER_SURNAME1 = "LUIS";
    private static final String MEMBER_NAME2 = "Member2";
    private static final String MEMBER_SURNAME2 = "Inactive";
    private static final String MEMBER_NAME3_SPACE = "  Member3 Compose ";
    private static final String MEMBER_BAD_NAME = "JOSE */()";
    private static final String MEMBER_BAD_SURNAME = "LUIS 1231";
    private static final String TEST_EXCEPTION = "TestException";
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final int DEFAULT_MONTHS_DURATION = 12;
    private Membership activeMembership1 = new MembershipBuilder().setId(MEMBERSHIP_ID1).setWebsite(WEBSITE_ES).setStatus(MemberStatus.ACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(NOW).setExpirationDate(NOW.plusMonths(DEFAULT_MONTHS_DURATION))
            .setMemberAccountId(MEMBER_ACCOUNT_ID1).build();
    private Membership inactiveMembership2 = new MembershipBuilder().setId(MEMBERSHIP_ID2).setWebsite(WEBSITE_FR).setStatus(MemberStatus.DEACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID2).build();
    private Membership pendingMembership3 = new MembershipBuilder().setId(MEMBERSHIP_ID3).setWebsite(WEBSITE_DE).setStatus(MemberStatus.PENDING_TO_ACTIVATE)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID3).build();
    private Membership activeMembership4 = new MembershipBuilder().setId(MEMBERSHIP_ID4).setWebsite(WEBSITE_DE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID3).build();
    private Membership activeMembershipExpirationDateInThePast = new MembershipBuilder().setId(MEMBERSHIP_ID5).setMemberAccountId(MEMBER_ACCOUNT_ID3)
            .setStatus(MemberStatus.ACTIVATED).setExpirationDate(LocalDateTime.now().minusDays(1L)).setWebsite(WEBSITE_FR).build();
    private MemberAccount memberAccount1 = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1)
            .setTimestamp(NOW).setMemberships(Collections.singletonList(activeMembership1));
    private MemberAccount memberAccount2 = new MemberAccount(MEMBER_ACCOUNT_ID2, USER_ID, MEMBER_NAME2, MEMBER_SURNAME2)
            .setTimestamp(NOW).setMemberships(Collections.singletonList(inactiveMembership2));
    private MemberAccount memberAccount3 = new MemberAccount(MEMBER_ACCOUNT_ID3, USER_ID, MEMBER_NAME3_SPACE, MEMBER_SURNAME1).setTimestamp(NOW);

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(BookingTrackingService.class).toInstance(bookingTrackingService);
        binder.bind(MemberAccountUserIdUpdater.class).toInstance(memberAccountUserIdUpdater);
        binder.bind(MembershipTransferMarketingHandler.class).toInstance(membershipTransferMarketingHandler);
        binder.bind(UserService.class).toInstance(userService);
    }

    @Test
    public void testGetActiveMemberAccountByUserId() throws DataAccessException {
        setUpMembersByUserIdResponse();
        List<MemberAccount> members = memberAccountServiceBean.getActiveMembersByUserId(USER_ID);
        assertTrue(members.contains(memberAccount1));
        assertTrue(members.contains(memberAccount3));
        assertFalse(members.contains(memberAccount2));
    }

    private void setUpMembersByUserIdResponse() throws DataAccessException {
        memberAccount1.setMemberships(Collections.singletonList(activeMembership1));
        memberAccount2.setMemberships(Collections.singletonList(inactiveMembership2));
        memberAccount3.setMemberships(Arrays.asList(pendingMembership3, activeMembership4));
        when(memberManager.getMembersWithActivatedMembershipsByUserId(USER_ID, dataSource)).thenReturn(Arrays.asList(memberAccount1, memberAccount3));
    }

    @Test
    public void testGetMemberAccountByMembershipId() throws MissingElementException, DataAccessException {
        setUpGetMemberAccountByMembershipId();
        assertEquals(memberAccountServiceBean.getMemberAccountByMembershipId(MEMBERSHIP_ID1).getId(), memberAccount1.getId());
    }

    private void setUpGetMemberAccountByMembershipId() throws MissingElementException, DataAccessException {
        when(memberManager.getMemberAccountsByMemberAccountId(anyLong(), eq(false), any(DataSource.class))).thenReturn(memberAccount1);
        when(memberManager.getMembershipById(any(DataSource.class), anyLong())).thenReturn(activeMembership1);
    }

    @Test
    public void testGetMembershipAccount() throws DataAccessException {
        setUpMembersByUserIdResponse();
        MemberAccount memberAccount = memberAccountServiceBean.getMembershipAccount(USER_ID, WEBSITE_ES);
        assertNotNull(memberAccount);
        assertFalse(memberAccount.getMemberships().get(0).getBookingLimitReached());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMemberByMemberAccountIdDataAccessException() throws MissingElementException, DataAccessException {
        when(memberManager.getMembershipById(any(DataSource.class), anyLong()))
                .thenThrow(new DataAccessException(TEST_EXCEPTION));
        memberAccountServiceBean.getMemberAccountByMembershipId(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMemberByMemberAccountIdMissingElementException() throws MissingElementException, DataAccessException {
        when(memberManager.getMembershipById(any(DataSource.class), anyLong()))
                .thenThrow(new MissingElementException(TEST_EXCEPTION));
        memberAccountServiceBean.getMemberAccountByMembershipId(MEMBERSHIP_ID1);
    }

    @Test
    public void testUpdateMemberAccountDefaultShouldUpdateNames() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = new UpdateMemberAccount(String.valueOf(USER_ID), MEMBER_NAME1, MEMBER_SURNAME1, MEMBER_EMAIL1, WEBSITE_DE);
        setUpUpdateMemberNames();
        memberAccountServiceBean.updateMemberAccount(updateMemberAccount);
        verify(memberManager).updateMemberAccountNames(dataSource, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
    }

    @Test
    public void testUpdateMemberAccountNames() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_NAMES.toString(), MEMBER_ACCOUNT_ID1, MEMBER_EMAIL1);
        setUpUpdateMemberNames();
        assertTrue(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
    }

    @Test(dataProvider = "requestsWithInvalidNames", expectedExceptions = InvalidParametersException.class)
    public void testUpdateMemberAccountNotValidLastNames(UpdateMemberAccount updateMemberAccount) throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        setUpUpdateMemberNames();
        memberAccountServiceBean.updateMemberAccount(updateMemberAccount);
    }

    @DataProvider(name = "requestsWithInvalidNames")
    private static Object[][] requestsWithInvalidNames() {
        return new Object[][]{
                new Object[]{new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID1), MEMBER_BAD_NAME, MEMBER_BAD_SURNAME, MEMBER_EMAIL1, WEBSITE_ES)},
                new Object[]{new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID1), MEMBER_BAD_NAME, MEMBER_SURNAME1, MEMBER_EMAIL1, WEBSITE_ES)},
                new Object[]{new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID1), MEMBER_NAME1, MEMBER_BAD_SURNAME, MEMBER_EMAIL1, WEBSITE_DE)}
        };
    }

    @Test
    public void testUpdateMemberAccountUserId_HappyPath() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID1, MEMBER_EMAIL1);
        mockUpdateMemberAccountUserIdResponse();
        mockNewUser();
        when(memberManager.getMemberAccountsByMemberAccountId(MEMBER_ACCOUNT_ID1, true, dataSource)).thenReturn(memberAccount1);
        assertTrue(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
        verify(memberManager, times(2)).getMembersWithActivatedMembershipsByUserId(anyLong(), any(DataSource.class));
        verify(membershipTransferMarketingHandler).subscribeNewUserAndUnsubscribeOldUser(USER_ID, NEW_USER_ID,
                memberAccount1.getMemberships(), Collections.emptySet());
    }

    @Test
    public void testUpdateMemberAccountUserId_UserHasNoActiveMembers() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID2, MEMBER_EMAIL1);
        mockUpdateMemberAccountUserIdResponse();
        mockNewUser();
        when(memberManager.getMemberAccountsByMemberAccountId(MEMBER_ACCOUNT_ID2, true, dataSource)).thenReturn(memberAccount2);
        assertFalse(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
        verify(membershipTransferMarketingHandler, never()).subscribeNewUserAndUnsubscribeOldUser(USER_ID, NEW_USER_ID,
                memberAccount2.getMemberships(), Collections.emptySet());
    }

    @Test(expectedExceptions = AlreadyHasPrimeMembershipException.class)
    public void testUpdateMemberAccountUserId_NewUserCannotHaveActiveMembershipForTheSameWebsite() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID1, MEMBER_EMAIL1);

        when(memberManager.getMemberAccountsByMemberAccountId(anyLong(), eq(true), any(DataSource.class))).thenReturn(memberAccount1);
        when(memberManager.getMembershipById(any(DataSource.class), anyLong())).thenReturn(activeMembership1);
        mockNewUserWithMembershipForWebsite(activeMembership1.getWebsite(), MemberStatus.ACTIVATED);

        assertFalse(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
        verify(memberManager).getMembersWithActivatedMembershipsByUserId(anyLong(), any(DataSource.class));
    }

    @Test(expectedExceptions = UserAccountNotFoundException.class)
    public void testUpdateMemberAccountUserId_theEmailShouldBeAssociatedWithANewUser() throws AlreadyHasPrimeMembershipException, DataAccessException, MissingElementException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID1, MEMBER_BAD_EMAIL);

        when(userService.getUserIdBy(MEMBER_BAD_EMAIL, WEBSITE_ES)).thenThrow(new UserAccountNotFoundException(""));

        assertFalse(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
        verifyZeroInteractions(memberManager);
    }

    @Test
    public void testUpdateMemberAccountUserId_NewUserCanHaveActiveMembershipForDifferentWebsite() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID1, MEMBER_EMAIL1);

        when(memberManager.getMemberAccountsByMemberAccountId(anyLong(), eq(true), any(DataSource.class))).thenReturn(memberAccount1);
        when(memberManager.getMembershipById(any(DataSource.class), anyLong())).thenReturn(activeMembership1);
        mockNewUserWithMembershipForWebsite(WEBSITE_DE, MemberStatus.ACTIVATED);
        mockUpdateMemberAccountUserIdResponse();

        assertTrue(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
        verify(memberManager, times(2)).getMembersWithActivatedMembershipsByUserId(anyLong(), any(DataSource.class));
    }

    @Test
    public void testUpdateMemberAccountUserId_NewUserCanHavePendingToActivateMembershipForTheSameWebsite() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID1, MEMBER_EMAIL1);

        when(memberManager.getMemberAccountsByMemberAccountId(anyLong(), eq(true), any(DataSource.class))).thenReturn(memberAccount1);
        when(memberManager.getMembershipById(any(DataSource.class), anyLong())).thenReturn(activeMembership1);
        mockNewUserWithMembershipForWebsite(WEBSITE_DE, MemberStatus.PENDING_TO_ACTIVATE);
        mockUpdateMemberAccountUserIdResponse();

        assertTrue(memberAccountServiceBean.updateMemberAccount(updateMemberAccount));
        verify(memberManager, times(2)).getMembersWithActivatedMembershipsByUserId(anyLong(), any(DataSource.class));
    }

    @Test(expectedExceptions = InvalidParameterException.class)
    public void testUpdateMemberAccountUserIdNull() throws DataAccessException, MissingElementException, AlreadyHasPrimeMembershipException, UserAccountNotFoundException {
        UpdateMemberAccount updateMemberAccount = getUpdateMemberAccount(UPDATE_USER_ID.toString(), MEMBER_ACCOUNT_ID1, null);
        doCallRealMethod().when(memberAccountUserIdUpdater).validateParametersForUpdateUserIdOperation(updateMemberAccount);
        memberAccountServiceBean.updateMemberAccount(updateMemberAccount);
    }

    private UpdateMemberAccount getUpdateMemberAccount(String operation, Long memberAccountId, String email) {
        return new UpdateMemberAccount(String.valueOf(memberAccountId), MEMBER_NAME1, MEMBER_SURNAME1, email, WEBSITE_ES).setOperation(operation);
    }

    @Test
    public void testActiveMembershipSitesByUserIsEmptyWhenNoMemberships() throws Exception {
        //Given
        final long userId = 324L;
        when(memberManager.getMembersWithActivatedMembershipsByUserId(userId, dataSource)).thenReturn(Collections.emptyList());
        //When
        Set<String> userActiveMembershipSites = memberAccountServiceBean.getActiveMembershipSitesByUserId(userId);
        //Then
        assertEquals(userActiveMembershipSites.size(), 0);
    }

    @Test
    public void testActiveMembershipSitesByUserDoesNotReturnDuplicatedValues() throws Exception {
        //Given
        final long userId = 6434L;
        when(memberManager.getMembersWithActivatedMembershipsByUserId(userId, dataSource)).thenReturn(Arrays.asList(memberAccountWithSite("GOFR"), memberAccountWithSite("GOFR")));
        //When
        Set<String> userActiveMembershipSites = memberAccountServiceBean.getActiveMembershipSitesByUserId(userId);
        //Then
        assertEquals(userActiveMembershipSites.size(), 1);
        assertTrue(userActiveMembershipSites.contains("GOFR"));
    }

    @Test
    public void testActiveMembershipSitesByUserDoesNotReturnMultipleBrandSites() throws Exception {
        //Given
        final long userId = 914L;
        when(memberManager.getMembersWithActivatedMembershipsByUserId(userId, dataSource))
                .thenReturn(Arrays.asList(memberAccountWithSite("GOFR"), memberAccountWithSite("ES"), memberAccountWithSite("whatever")));
        //When
        Set<String> userActiveMembershipSites = memberAccountServiceBean.getActiveMembershipSitesByUserId(userId);
        //Then
        assertEquals(userActiveMembershipSites.size(), 3);
        assertTrue(userActiveMembershipSites.containsAll(Arrays.asList("GOFR", "ES", "WHATEVER")));
    }

    @Test
    public void testGetMembershipAccountById() throws MissingElementException, DataAccessException {
        //Given
        final long accountId = 234L;
        final String timestamp = "2019-11-03T10:15:30";
        MemberAccount expectedAccount = new MemberAccount(accountId, 123L, "John", "Doe").setTimestamp(LocalDateTime.parse(timestamp));
        when(memberManager.getMemberAccountsByMemberAccountId(accountId, false, dataSource)).thenReturn(expectedAccount);
        //When
        MemberAccount account = memberAccountServiceBean.getMemberAccountById(accountId, false);
        //Then
        assertMemberAccount(expectedAccount, account);
    }

    @Test(expectedExceptions = MissingElementException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipAccountByIdThrowsMissingElementException() throws MissingElementException, DataAccessException {
        //Given
        final long accountId = 324L;
        when(memberAccountServiceBean.getMemberAccountById(accountId, false)).thenThrow(new MissingElementException("Expected exception"));
        //When
        memberAccountServiceBean.getMemberAccountById(accountId, false);
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Expected exception")
    public void testGetMembershipAccountByIdThrowsDataAccessException() throws MissingElementException, DataAccessException {
        //Given
        final long accountId = 1234L;
        when(memberAccountServiceBean.getMemberAccountById(accountId, false)).thenThrow(new DataAccessException("Expected exception"));
        //When
        memberAccountServiceBean.getMemberAccountById(accountId, false);
    }

    private void assertMemberAccount(final MemberAccount expectedAccount, final MemberAccount account) {
        assertEquals(account, expectedAccount);
        assertEquals(account.getId(), expectedAccount.getId());
        assertEquals(account.getUserId(), expectedAccount.getUserId());
        assertEquals(account.getName(), expectedAccount.getName());
        assertEquals(account.getLastNames(), expectedAccount.getLastNames());
        assertEquals(account.getTimestamp(), expectedAccount.getTimestamp());
    }

    private MemberAccount memberAccountWithSite(final String site) {
        MemberAccount memberAccount = buildMemberAccount();
        memberAccount.setMemberships(Collections.singletonList(new Membership(new MembershipBuilder().setWebsite(site))));
        return memberAccount;
    }

    private MemberAccount buildMemberAccount() {
        return new MemberAccount(RandomUtils.nextLong(), RandomUtils.nextLong(), RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphabetic(10)).setTimestamp(NOW);
    }

    private void setUpUpdateMemberNames() throws DataAccessException {
        when(memberManager.updateMemberAccountNames(any(DataSource.class), anyLong(), anyString(), anyString())).thenReturn(Boolean.TRUE);
    }

    private void mockUpdateMemberAccountUserIdResponse() throws DataAccessException {
        when(memberAccountUserIdUpdater.updateMemberAccountUserId(any(DataSource.class), anyLong(), anyLong())).thenReturn(Boolean.TRUE);
    }

    private void mockNewUser() throws UserAccountNotFoundException, DataAccessException {
        when(userService.getUserIdBy(anyString(), anyString())).thenReturn(NEW_USER_ID);
    }

    private void mockNewUserWithMembershipForWebsite(String website, MemberStatus status) throws DataAccessException, UserAccountNotFoundException {
        mockNewUser();
        MemberAccount memberAccount = buildMemberAccount()
                .setMemberships(Collections.singletonList(new MembershipBuilder().setWebsite(website).setStatus(status).build()));
        when(memberManager.getMembersWithActivatedMembershipsByUserId(anyLong(), any(DataSource.class))).thenReturn(Collections.singletonList(memberAccount));
    }
}
