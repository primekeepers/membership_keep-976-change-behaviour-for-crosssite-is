package com.odigeo.membership;

import bean.test.BeanTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class MemberAccountTest extends BeanTest<MemberAccount> {

    private static final Long USER_ID = 1234L;
    private static final Long USER_ID2 = 4321L;
    private static final Long MEMBER_ACCOUNT_ID = 1234L;
    private static final Long MEMBER_ACCOUNT_ID2 = 4321L;
    private static final String NAME = "John";
    private static final String NAME2 = "Joe";
    private static final String LAST_NAMES = "Smith";
    private static final String LAST_NAMES2 = "Cobra";
    private static final LocalDateTime TIMESTAMP = LocalDateTime.now();

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    protected MemberAccount getBean() {
        return getMemberAccount();
    }

    private MemberAccount getMemberAccount() {
        return new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAMES).setTimestamp(TIMESTAMP);
    }

    @Test
    protected void checkEqualMembers() {
        MemberAccount mb1 = getMemberAccount();
        MemberAccount mb2 = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID2, NAME2, LAST_NAMES2).setTimestamp(TIMESTAMP);
        MemberAccount mb3 = new MemberAccount(MEMBER_ACCOUNT_ID2, USER_ID2, NAME, LAST_NAMES).setTimestamp(TIMESTAMP);

        assertEquals(mb2, mb1);
        assertEquals(mb1, mb1);
        assertNotEquals(mb3, mb1);
        assertNotEquals(mb1, null);
    }

    @Test
    protected void testHashCode() {
        MemberAccount mb1 = getMemberAccount();
        mb1.setTimestamp(LocalDateTime.now());
        Assert.assertNotNull(mb1.hashCode());
    }

    @Test
    protected void testJsonConstructor() throws Exception {
        String memberAccountJson = mapper.writeValueAsString(getMemberAccount());
        MemberAccount deserializedMemberAccount = mapper.readValue(memberAccountJson, MemberAccount.class);
        assertEquals(deserializedMemberAccount, getMemberAccount());
    }
}
