package com.odigeo.membership.propertiesconfig;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.configuration.AbstractContextFactory;
import com.odigeo.membership.configuration.JeeServiceLocator;
import com.odigeo.membership.configuration.JeeServiceLocatorTest;
import com.odigeo.membership.exception.DataNotFoundException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static com.odigeo.membership.propertiesconfig.PropertiesConfigurationStoreKeys.SEND_IDS_TO_KAFKA;
import static com.odigeo.membership.propertiesconfig.PropertiesConfigurationStoreKeys.TRANSACTIONAL_EMAILS;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PropertiesConfigurationServiceBeanTest extends AbstractContextFactory {

    private static final String TEST_KEY = "test key";
    public static final String MY_APP_JNDI_NAME = "myApp";

    private DataSource dataSource;
    private PropertiesConfigurationManager propertiesConfigManagerMock;
    private PropertiesCacheService propertiesCacheServiceMock;
    private PropertiesConfigurationService propertiesConfigService;

    @BeforeMethod
    public void setUp() {
        propertiesConfigService = new PropertiesConfigurationServiceBean();
        ConfigurationEngine.init(this::configureMocks);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsTrue_WhenRetrievedValueIsTrue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(SEND_IDS_TO_KAFKA))).willReturn(true);
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertTrue(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsFalse_WhenRetrievedValueIsFalse() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(SEND_IDS_TO_KAFKA))).willReturn(false);
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsFalse_WhenSqlExceptionRetrievingValue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(SEND_IDS_TO_KAFKA))).willThrow(new SQLException("expected SQL exception"));
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsFalse_WhenDataNotFoundException() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(SEND_IDS_TO_KAFKA))).willThrow(new SQLException("expected DataNotFoundException exception"));
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsTransactionalWelcomeEmailActiveIsTrue_WhenRetrievedValueIsTrue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(TRANSACTIONAL_EMAILS))).willReturn(true);
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isTransactionalWelcomeEmailActive();
        //Then
        assertTrue(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsTransactionalWelcomeEmailActiveIsFalse_WhenRetrievedValueIsFalse() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(TRANSACTIONAL_EMAILS))).willReturn(false);
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isTransactionalWelcomeEmailActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsTransactionalWelcomeEmailActiveIsFalse_WhenSqlExceptionRetrievingValue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(TRANSACTIONAL_EMAILS))).willThrow(new SQLException("expected SQL exception"));
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isTransactionalWelcomeEmailActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsTransactionalWelcomeEmailActiveIsFalse_WhenDataNotFoundException() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isConfigurableFeatureActive(eq(dataSource), eq(TRANSACTIONAL_EMAILS))).willThrow(new SQLException("expected DataNotFoundException exception"));
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isTransactionalWelcomeEmailActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testUpdatePropertyValueIsTrue_WhenUpdated() throws Exception {
        //Given
        given(propertiesConfigManagerMock.updatePropertyValue(eq(dataSource), eq(TEST_KEY), eq(FALSE))).willReturn(TRUE);
        //When
        boolean updatePropertyValue = propertiesConfigService.updatePropertyValue(TEST_KEY, FALSE);
        //Then
        assertTrue(updatePropertyValue);
    }

    @Test
    public void testUpdatePropertyValueIsFalse_WhenNotUpdated() throws Exception {
        //Given
        given(propertiesConfigManagerMock.updatePropertyValue(eq(dataSource), eq(TEST_KEY), eq(TRUE))).willReturn(FALSE);
        //When
        boolean updatePropertyValue = propertiesConfigService.updatePropertyValue(TEST_KEY, TRUE);
        //Then
        assertFalse(updatePropertyValue);
    }

    @Test
    public void testUpdatePropertyValueIsFalse_WhenErrorUpdating() throws Exception {
        //Given
        given(propertiesConfigManagerMock.updatePropertyValue(eq(dataSource), eq(TEST_KEY), eq(TRUE))).willThrow(new DataAccessException("expected exception"));
        //When
        boolean updatePropertyValue = propertiesConfigService.updatePropertyValue(TEST_KEY, TRUE);
        //Then
        assertFalse(updatePropertyValue);
    }

    private void configureMocks(final Binder binder) {
        dataSource = null;
        propertiesConfigManagerMock = mock(PropertiesConfigurationManager.class);
        propertiesCacheServiceMock = mock(PropertiesCacheService.class);

        binder.bind(PropertiesConfigurationManager.class).toInstance(propertiesConfigManagerMock);
        binder.bind(PropertiesCacheService.class).toInstance(propertiesCacheServiceMock);
    }
}