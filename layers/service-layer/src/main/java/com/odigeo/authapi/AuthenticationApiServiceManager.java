package com.odigeo.authapi;

import com.google.inject.Inject;
import com.odigeo.authservices.authentication.api.v1.AuthServicesAuthenticationApiService;
import com.odigeo.authservices.authentication.api.v1.exceptions.AuthServicesAuthenticationApiException;
import com.odigeo.authservices.authorization.api.v1.AuthServicesAuthorizationApiService;
import com.odigeo.authservices.authorization.api.v1.exceptions.AuthServicesAuthorizationApiException;
import com.odigeo.authservices.authorization.api.v1.exceptions.UnauthorizedException;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.membership.auth.AuthenticationResponse;
import com.odigeo.membership.auth.PermissionResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import ma.glasnost.orika.MapperFacade;
import org.apache.log4j.Logger;

public class AuthenticationApiServiceManager implements AuthContract {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationApiServiceManager.class);

    private static final String APP_ID = "18";

    private static final String FAILED_LOGIN = "Error during login for user %s";
    private static final String FAILED_LOGOUT = "Error during the logout";
    private static final String FAILED_PERMISSIONS = "Error during the permissions";
    private static final String INVALID_USER = "Unauthorized user for appId %s";
    private static final String FAILED_AUTHORIZATION = "Error getting token permissions";

    private final AuthServicesAuthenticationApiService authServicesAuthenticationApiService;
    private final AuthServicesAuthorizationApiService authServicesAuthorizationApiService;
    private final MapperFacade mapperFacade;

    @Inject
    public AuthenticationApiServiceManager(AuthenticationApiServiceConfiguration authenticationApiServiceConfiguration) {
        this.authServicesAuthenticationApiService = new ServiceBuilder<>(AuthServicesAuthenticationApiService.class,
                authenticationApiServiceConfiguration.getAuthApiHost(),
                new SimpleRestErrorsHandler(AuthServicesAuthenticationApiService.class))
                .build();
        this.authServicesAuthorizationApiService = new ServiceBuilder<>(AuthServicesAuthorizationApiService.class,
                authenticationApiServiceConfiguration.getAuthApiHost(),
                new SimpleRestErrorsHandler(AuthServicesAuthorizationApiService.class))
                .build();
        this.mapperFacade = new GeneralMapperCreator().getMapper();
    }

    @Override
    public AuthenticationResponse login(final String userName, final String password) throws MembershipAuthServiceException {
        try {
            return mapperFacade.map(authServicesAuthenticationApiService.login(userName, password), AuthenticationResponse.class);
        } catch (AuthServicesAuthenticationApiException e) {
            LOGGER.error(String.format(FAILED_LOGIN, userName), e);
            throw new MembershipAuthServiceException(FAILED_LOGOUT, e);
        }
    }

    @Override
    public AuthenticationResponse logout(final String token) throws MembershipAuthServiceException {
        try {
            return mapperFacade.map(authServicesAuthenticationApiService.logout(token), AuthenticationResponse.class);
        } catch (AuthServicesAuthenticationApiException e) {
            LOGGER.error(FAILED_LOGOUT, e);
            throw new MembershipAuthServiceException(FAILED_LOGOUT, e);
        }
    }

    @Override
    public PermissionResponse getTokenPermissions(final String token) throws MembershipAuthServiceException {
        try {
            return mapperFacade.map(authServicesAuthorizationApiService.getTokenPermissions(APP_ID, token), PermissionResponse.class);
        } catch (UnauthorizedException e) {
            LOGGER.error(String.format(INVALID_USER, APP_ID), e);
            throw new MembershipAuthServiceException(FAILED_LOGOUT, e);
        } catch (AuthServicesAuthorizationApiException e) {
            LOGGER.error(FAILED_AUTHORIZATION, e);
            throw new MembershipAuthServiceException(FAILED_PERMISSIONS, e);
        }
    }
}
