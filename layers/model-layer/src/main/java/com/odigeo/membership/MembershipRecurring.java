package com.odigeo.membership;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

public class MembershipRecurring implements Serializable {

    private final String recurringId;
    private final String status;

    private MembershipRecurring(Builder builder) {
        this.recurringId = builder.recurringId;
        this.status = builder.status;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public String getStatus() {
        return status;
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {

        private String recurringId;
        private String status;

        public Builder recurringId(String recurringId) {
            this.recurringId = recurringId;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public MembershipRecurring build() {
            return new MembershipRecurring(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipRecurring that = (MembershipRecurring) o;
        return Objects.equals(recurringId, that.recurringId)
                && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recurringId, status);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MembershipRecurring.class.getSimpleName() + "[", "]")
                .add("recurringId='" + recurringId + '\'')
                .add("status='" + status + '\'')
                .toString();
    }
}
