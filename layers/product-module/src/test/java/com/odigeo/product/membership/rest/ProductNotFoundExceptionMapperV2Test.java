package com.odigeo.product.membership.rest;

import com.odigeo.product.response.exception.MembershipProductServiceException;
import com.odigeo.product.response.exception.MembershipProductServiceExceptionType;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.enums.ProductType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ProductNotFoundExceptionMapperV2Test {

    private static final String PRODUCT_ID = "1";
    private static final String MESSAGE = "Message";

    private final ProductNotFoundExceptionMapperV2 productNotFoundExceptionMapperV2 = new ProductNotFoundExceptionMapperV2();

    @DataProvider(name = "dataExceptionResponse")
    public Object[][] getData() {
        return new Object[][] {
            {new ProductException(), 400},
            {new MembershipProductServiceException(MembershipProductServiceExceptionType.BAD_REQUEST, PRODUCT_ID, ProductType.MEMBERSHIP_RENEWAL, MESSAGE),
                MembershipProductServiceExceptionType.BAD_REQUEST.getHttpCode()},
            {new MembershipProductServiceException(MembershipProductServiceExceptionType.FORBIDDEN, PRODUCT_ID, ProductType.MEMBERSHIP_RENEWAL, MESSAGE),
                MembershipProductServiceExceptionType.FORBIDDEN.getHttpCode()},
            {new MembershipProductServiceException(MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR, PRODUCT_ID, ProductType.MEMBERSHIP_RENEWAL, MESSAGE),
                MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR.getHttpCode()},
            {new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, PRODUCT_ID, ProductType.MEMBERSHIP_RENEWAL, MESSAGE),
                MembershipProductServiceExceptionType.NOT_FOUND.getHttpCode()},
            {new MembershipProductServiceException(MembershipProductServiceExceptionType.UNKNOWN, PRODUCT_ID, ProductType.MEMBERSHIP_RENEWAL, MESSAGE),
                MembershipProductServiceExceptionType.UNKNOWN.getHttpCode()},
        };
    }

    @Test (dataProvider = "dataExceptionResponse")
    public void testResponseProductNotFound(ProductException productException, int responseCode) {
        assertEquals(productNotFoundExceptionMapperV2.toResponse(productException).getStatus(), responseCode);
    }
}