package com.odigeo.membership.member.discount.rule;

import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.enums.Interface;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;
import com.odigeo.visitengineapi.v1.multitest.TestDimension;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.odigeo.membership.parameters.NotApplicationReason.WEBSITE_INVALID;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class WebsiteRulesTest extends ApplyDiscountRulesTest {

    private static final String WEBSITE_USER = "ES";
    private static final String WEBSITE_OTHER = "COM";
    private static final String PRIME_CROSS_SITE_AB = "X16-4";

    private WebsiteRules websiteRules;

    @Mock
    private TestTokenUtils testTokenUtils;

    @Mock
    private Membership membership1;
    @Mock
    private Membership membership2;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        websiteRules = new WebsiteRules();
    }

    @DataProvider
    public static Object[][] websites() {
        return new Object[][]{
                {WEBSITE_USER, Interface.ONE_FRONT_DESKTOP, FALSE, TRUE, null },
                {WEBSITE_OTHER, Interface.ONE_FRONT_DESKTOP, FALSE, FALSE, WEBSITE_INVALID},
                {WEBSITE_USER, Interface.ONE_FRONT_DESKTOP, TRUE, TRUE, null, },
                {WEBSITE_OTHER, Interface.ONE_FRONT_DESKTOP, TRUE, TRUE, null},
                {WEBSITE_USER, Interface.ONE_FRONT_SMARTPHONE, TRUE, TRUE, null},
                {WEBSITE_OTHER, Interface.ONE_FRONT_SMARTPHONE, TRUE, FALSE, WEBSITE_INVALID}
        };
    }

    @Test(dataProvider = "websites")
    public void testTest1(String website, Interface clientInterface, boolean isCrossSite, boolean expected, NotApplicationReason reason) {

        when(membership1.getWebsite()).thenReturn(WEBSITE_USER);


        ApplyDiscountParameters parameters = buildApplyDiscountParameters(website, clientInterface, isCrossSite);
        ApplyDiscountContext context = buildContext();
        when(ApplyDiscountRules.isABActive(context, TestTokenUtils.PRIME_CROSS_SITE_DESKTOP)).thenReturn(isCrossSite);

        //WHEN
        boolean result = websiteRules.test(parameters, context);

        //THEN
        assertEquals(result, expected);
        checkContext(context, result, reason);

    }

    private ApplyDiscountParameters buildApplyDiscountParameters(String website, Interface clientInterface, boolean isCrossSite) {
        ApplyDiscountParameters parameters = new ApplyDiscountParameters();
        parameters.setWebsite(website);
        if(isCrossSite) {
            parameters.setClientInterface(clientInterface);
        }
        return parameters;
    }

    private ApplyDiscountContext buildContext() {
        return new ApplyDiscountContext(testTokenUtils, List.of(membership1, membership2));
    }
}
