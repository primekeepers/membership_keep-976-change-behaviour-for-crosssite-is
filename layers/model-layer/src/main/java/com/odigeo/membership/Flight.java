package com.odigeo.membership;

import java.time.ZonedDateTime;
import java.util.Objects;

public class Flight {

    private final Long bookingId;
    private final ZonedDateTime departure;
    private final String destination;

    public Flight(Long bookingId, ZonedDateTime departure, String destination) {
        this.bookingId = bookingId;
        this.departure = departure;
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Flight flight = (Flight) o;
        return Objects.equals(bookingId, flight.bookingId) && Objects.equals(departure, flight.departure) && Objects.equals(destination, flight.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, departure, destination);
    }

    public Long getBookingId() {
        return bookingId;
    }

    public String getDestination() {
        return destination;
    }

    public ZonedDateTime getDeparture() {
        return departure;
    }

}
