package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.booking.MembershipBookingBuilder;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MembershipBookingTrackingStore {

    private static final String SAVE_BOOKING = "INSERT INTO MEMBERSHIP_OWN.GE_MEMBERSHIP_BOOKING_TRACKING (BOOKING_ID, MEMBERSHIP_ID, BOOKING_DATE, AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_FEE_AMOUNT) VALUES (?, ?, ?, ?, ? ,?)";
    private static final String FIND_BOOKING_TRACKING = "SELECT bt.BOOKING_ID, bt.MEMBERSHIP_ID, bt.BOOKING_DATE, bt.AVOID_FEE_AMOUNT, bt.COST_FEE_AMOUNT, bt.PERKS_FEE_AMOUNT FROM GE_MEMBERSHIP_BOOKING_TRACKING bt WHERE bt.BOOKING_ID = ?";
    private static final String BOOKING_ID = "BOOKING_ID";
    private static final String MEMBERSHIP_ID = "MEMBERSHIP_ID";
    private static final String BOOKING_DATE = "BOOKING_DATE";
    private static final String AVOID_FEE_AMOUNT = "AVOID_FEE_AMOUNT";
    private static final String COST_FEE_AMOUNT = "COST_FEE_AMOUNT";
    private static final String PERKS_FEE_AMOUNT = "PERKS_FEE_AMOUNT";


    public void saveMembershipBooking(Connection conn, List<MembershipBookingBuilder> membershipBookingBuilderList) throws SQLException {

        try (PreparedStatement pstmt = conn.prepareStatement(SAVE_BOOKING)) {
            for (MembershipBookingBuilder membershipBooking : membershipBookingBuilderList) {
                pstmt.setLong(1, membershipBooking.getBookingId());
                pstmt.setLong(2, membershipBooking.getMemberId());
                pstmt.setDate(3, new Date(membershipBooking.getBookingDate().getTime()));
                pstmt.setBigDecimal(4, membershipBooking.getAvoidFeeAmount());
                pstmt.setBigDecimal(5, membershipBooking.getCostFeeAmount());
                pstmt.setBigDecimal(6, membershipBooking.getPerksFeeAmount());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }

    public List<MembershipBookingBuilder> findByBookingId(final Connection conn, final Long bookingId) throws SQLException {
        List<MembershipBookingBuilder> audits = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(FIND_BOOKING_TRACKING)) {
            preparedStatement.setLong(1, bookingId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    audits.add(buildBookingTracking(rs));
                }
            }
        }
        return audits;
    }

    private MembershipBookingBuilder buildBookingTracking(final ResultSet rs) throws SQLException {
        return new MembershipBookingBuilder()
                .setBookingId(rs.getLong(BOOKING_ID))
                .setMemberId(rs.getLong(MEMBERSHIP_ID))
                .setBookingDate(rs.getDate(BOOKING_DATE))
                .setAvoidFeeAmount(rs.getBigDecimal(AVOID_FEE_AMOUNT))
                .setCostFeeAmount(rs.getBigDecimal(COST_FEE_AMOUNT))
                .setPerksFeeAmount(rs.getBigDecimal(PERKS_FEE_AMOUNT));
    }
}
