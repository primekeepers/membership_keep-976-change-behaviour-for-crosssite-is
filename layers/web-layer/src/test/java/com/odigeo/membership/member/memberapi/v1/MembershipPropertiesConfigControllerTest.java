package com.odigeo.membership.member.memberapi.v1;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.propertiesconfig.PropertiesConfigurationService;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.BDDMockito.given;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipPropertiesConfigControllerTest {

    private static final String TEST_PROPERTY = "test-prop";

    private PropertiesConfigurationService propertiesConfigurationServiceMock;
    private MembershipPropertiesConfigController propertiesConfigController;

    @BeforeMethod
    public void setUp() {
        propertiesConfigController = new MembershipPropertiesConfigController();
        ConfigurationEngine.init(this::openMocks);
    }

    @Test
    public void testEnableConfigurationPropertyIsTrueWhenEnabled() {
        //Given
        given(propertiesConfigurationServiceMock.updatePropertyValue(TEST_PROPERTY, TRUE)).willReturn(TRUE);
        //When
        Boolean enabledProperty = propertiesConfigController.enableConfigurationProperty(TEST_PROPERTY);
        //Then
        assertTrue(enabledProperty);
    }

    @Test
    public void testEnableConfigurationPropertyIsFalseWhenNotEnabled() {
        //Given
        given(propertiesConfigurationServiceMock.updatePropertyValue(TEST_PROPERTY, TRUE)).willReturn(FALSE);
        //When
        Boolean enabledProperty = propertiesConfigController.enableConfigurationProperty(TEST_PROPERTY);
        //Then
        assertFalse(enabledProperty);
    }

    @Test
    public void testDisableConfigurationPropertyIsTrueWhenDisabled() {
        //Given
        given(propertiesConfigurationServiceMock.updatePropertyValue(TEST_PROPERTY, FALSE)).willReturn(TRUE);
        //When
        Boolean disabledProperty = propertiesConfigController.disableConfigurationProperty(TEST_PROPERTY);
        //Then
        assertTrue(disabledProperty);
    }

    @Test
    public void testDisableConfigurationPropertyIsFalseWhenNotDisabled() {
        //Given
        given(propertiesConfigurationServiceMock.updatePropertyValue(TEST_PROPERTY, FALSE)).willReturn(FALSE);
        //When
        Boolean disabledProperty = propertiesConfigController.disableConfigurationProperty(TEST_PROPERTY);
        //Then
        assertFalse(disabledProperty);
    }

    private void openMocks(final Binder binder) {
        propertiesConfigurationServiceMock = Mockito.mock(PropertiesConfigurationService.class);

        binder.bind(PropertiesConfigurationService.class).toInstance(propertiesConfigurationServiceMock);
    }
}