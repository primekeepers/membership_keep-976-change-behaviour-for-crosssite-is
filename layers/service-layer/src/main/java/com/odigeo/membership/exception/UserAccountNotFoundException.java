package com.odigeo.membership.exception;

public class UserAccountNotFoundException extends Exception {

    public UserAccountNotFoundException(String message) {
        super(message);
    }
}
