package com.odigeo.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.messaging.kafka.MembershipKafkaSender;


@Singleton
class MembershipUpdateMessageService {

    private final MembershipKafkaSender membershipKafkaSender;

    @Inject
    MembershipUpdateMessageService(MembershipKafkaSender membershipKafkaSender) {
        this.membershipKafkaSender = membershipKafkaSender;
    }

    void sendMembershipIdToMembershipReporter(long membershipId) {
        final MembershipUpdateMessage membershipUpdateMessage = getMembershipUpdateMessage(membershipId);
        membershipKafkaSender.sendMembershipUpdateMessageToKafka(membershipUpdateMessage);
    }

    private MembershipUpdateMessage getMembershipUpdateMessage(long membershipId) {
        return MembershipUpdateMessage.Builder.create()
                .membershipId(String.valueOf(membershipId))
                .build();
    }
}
