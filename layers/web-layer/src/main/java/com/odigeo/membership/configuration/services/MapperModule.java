package com.odigeo.membership.configuration.services;

import com.google.inject.AbstractModule;
import com.odigeo.membership.member.memberapi.mapper.MembershipMapper;
import com.odigeo.membership.member.memberapi.mapper.MembershipMapperImpl;
import com.odigeo.membership.member.memberapi.mapper.UpdateMembershipMapper;
import com.odigeo.membership.member.memberapi.mapper.UpdateMembershipMapperImpl;

public class MapperModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MembershipMapper.class).to(MembershipMapperImpl.class);
        bind(UpdateMembershipMapper.class).to(UpdateMembershipMapperImpl.class);
    }
}
