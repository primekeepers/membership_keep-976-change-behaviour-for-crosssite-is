package com.odigeo.membership.member.discount.rule;

import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.enums.Interface;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static com.odigeo.membership.enums.Interface.NATIVE_IOS_EDREAMS;
import static com.odigeo.membership.enums.Interface.ONE_FRONT_SMARTPHONE;
import static com.odigeo.membership.enums.MembershipType.BASIC;
import static com.odigeo.membership.enums.MembershipType.PLUS;
import static com.odigeo.membership.parameters.NotApplicationReason.INTERFACE_INVALID;
import static com.odigeo.membership.parameters.NotApplicationReason.MEMBERSHIP_INVALID;
import static com.odigeo.membership.parameters.NotApplicationReason.ORIGIN_INVALID;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class PrimeShareRulesTest extends ApplyDiscountRulesTest {

    private PrimeShareRules primeShareRules;

    @Mock
    private TestTokenUtils testTokenUtils;
    @Mock
    private Membership membership1;
    @Mock
    private Membership membership2;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        primeShareRules = new PrimeShareRules();
    }

    @DataProvider
    public static Object[][] primeSharing() {
        return new Object[][]{
                {ONE_FRONT_SMARTPHONE, TRUE, TRUE, BASIC, TRUE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, FALSE, BASIC, TRUE, null},
                {ONE_FRONT_SMARTPHONE, FALSE, TRUE, BASIC, FALSE, MEMBERSHIP_INVALID},
                {ONE_FRONT_SMARTPHONE, FALSE, FALSE, BASIC, FALSE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, TRUE, PLUS, TRUE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, FALSE, PLUS, TRUE, null},
                {ONE_FRONT_SMARTPHONE, FALSE, TRUE, PLUS, TRUE, null},
                {ONE_FRONT_SMARTPHONE, FALSE, FALSE, PLUS, FALSE, null}
        };
    }

    @Test(dataProvider = "primeSharing")
    public void testPrimeSharing(Interface anInterface, boolean primeSharingABActive, boolean primeSharingPlusActive, MembershipType membershipType, boolean expected, NotApplicationReason reason) {
        //GIVEN
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_META_BLOCKED)).thenReturn(false);

        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_FOR_ALL)).thenReturn(primeSharingABActive);
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_ONLY_PLUS)).thenReturn(primeSharingPlusActive);

        ApplyDiscountParameters parameters = buildParameters(anInterface, false);
        ApplyDiscountContext context = buildContext(membershipType);

        //WHEN
        boolean result = primeShareRules.test(parameters, context);

        //THEN
        assertEquals(result, expected);

        checkContext(context, result, reason);

        if (!primeSharingABActive && primeSharingPlusActive && PLUS.equals(membershipType)) {
            assertEquals(context.getMembershipsEligible().size(), 1);
        }

    }

    @DataProvider
    public static Object[][] primeSharingInterface() {
        return new Object[][]{
                {NATIVE_IOS_EDREAMS, TRUE, TRUE, BASIC, TRUE, null},
                {NATIVE_IOS_EDREAMS, TRUE, FALSE, BASIC, TRUE, null},
                {NATIVE_IOS_EDREAMS, FALSE, TRUE, BASIC, FALSE, INTERFACE_INVALID},
                {NATIVE_IOS_EDREAMS, FALSE, FALSE, BASIC, FALSE, INTERFACE_INVALID},
                {NATIVE_IOS_EDREAMS, TRUE, TRUE, PLUS, TRUE, null},
                {NATIVE_IOS_EDREAMS, TRUE, FALSE, PLUS, TRUE, null},
                {NATIVE_IOS_EDREAMS, FALSE, TRUE, PLUS, TRUE, null},
                {NATIVE_IOS_EDREAMS, FALSE, FALSE, PLUS, FALSE, INTERFACE_INVALID},
                {ONE_FRONT_SMARTPHONE, TRUE, TRUE, BASIC, TRUE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, FALSE, BASIC, TRUE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, TRUE, PLUS, TRUE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, FALSE, PLUS, TRUE, null},
                {ONE_FRONT_SMARTPHONE, FALSE, TRUE, PLUS, TRUE, null}
        };
    }

    @Test(dataProvider = "primeSharingInterface")
    public void testPrimeSharingInterface(Interface anInterface, boolean primeSharingABActive, boolean primeSharingPlusActive, MembershipType membershipType, boolean expected, NotApplicationReason reason) {
        //GIVEN
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_META_BLOCKED)).thenReturn(false);
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_FOR_ALL)).thenReturn(true);

        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_NATIVE_FOR_ALL)).thenReturn(primeSharingABActive);
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_NATIVE_ONLY_PLUS)).thenReturn(primeSharingPlusActive);

        ApplyDiscountParameters parameters = buildParameters(anInterface, false);
        ApplyDiscountContext context = buildContext(membershipType);

        //WHEN
        boolean result = primeShareRules.test(parameters, context);

        //THEN
        assertEquals(result, expected);

        checkContext(context, result, reason);

        if (!primeSharingABActive && primeSharingPlusActive && PLUS.equals(membershipType) && NATIVE_IOS_EDREAMS.equals(anInterface)) {
            assertEquals(context.getMembershipsEligible().size(), 1);
        }

    }

    @DataProvider
    public static Object[][] primeSharingOrigin() {
        return new Object[][]{
                {NATIVE_IOS_EDREAMS, TRUE, TRUE, BASIC, FALSE, ORIGIN_INVALID},
                {NATIVE_IOS_EDREAMS, TRUE, FALSE, BASIC, FALSE, ORIGIN_INVALID},
                {NATIVE_IOS_EDREAMS, FALSE, TRUE, BASIC, FALSE, MEMBERSHIP_INVALID},
                {NATIVE_IOS_EDREAMS, FALSE, FALSE, BASIC, FALSE, null},
                {NATIVE_IOS_EDREAMS, TRUE, TRUE, PLUS, FALSE, ORIGIN_INVALID},
                {NATIVE_IOS_EDREAMS, TRUE, FALSE, PLUS, FALSE, ORIGIN_INVALID},
                {NATIVE_IOS_EDREAMS, FALSE, TRUE, PLUS, FALSE, ORIGIN_INVALID},
                {NATIVE_IOS_EDREAMS, FALSE, FALSE, PLUS, FALSE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, TRUE, BASIC, FALSE, ORIGIN_INVALID},
                {ONE_FRONT_SMARTPHONE, TRUE, FALSE, BASIC, FALSE, ORIGIN_INVALID},
                {ONE_FRONT_SMARTPHONE, FALSE, TRUE, BASIC, FALSE, MEMBERSHIP_INVALID},
                {ONE_FRONT_SMARTPHONE, FALSE, FALSE, BASIC, FALSE, null},
                {ONE_FRONT_SMARTPHONE, TRUE, TRUE, PLUS, FALSE, ORIGIN_INVALID},
                {ONE_FRONT_SMARTPHONE, TRUE, FALSE, PLUS, FALSE, ORIGIN_INVALID},
                {ONE_FRONT_SMARTPHONE, FALSE, TRUE, PLUS, FALSE, ORIGIN_INVALID},
                {ONE_FRONT_SMARTPHONE, FALSE, FALSE, PLUS, FALSE, null}
        };
    }

    @Test(dataProvider = "primeSharingOrigin")
    public void testPrimeSharingOrigin(Interface anInterface, boolean primeSharingABActive, boolean primeSharingPlusActive, MembershipType membershipType, boolean expected, NotApplicationReason reason) {
        //GIVEN
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_META_BLOCKED)).thenReturn(true);

        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_FOR_ALL)).thenReturn(primeSharingABActive);
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_ONLY_PLUS)).thenReturn(primeSharingPlusActive);
        when(testTokenUtils.matches(TestTokenUtils.PRIME_SHARE_NATIVE_FOR_ALL)).thenReturn(true);

        ApplyDiscountParameters parameters = buildParameters(anInterface, true);
        ApplyDiscountContext context = buildContext(membershipType);

        //WHEN
        boolean result = primeShareRules.test(parameters, context);

        //THEN
        assertEquals(result, expected);

        checkContext(context, result, reason);
    }

    private ApplyDiscountContext buildContext(MembershipType membershipType) {
        Membership membership = Membership.builder().setMembershipType(membershipType).build();
        return new ApplyDiscountContext(testTokenUtils, List.of(membership, membership1, membership2));
    }

    private ApplyDiscountParameters buildParameters(Interface anInterface, boolean meta) {
        ApplyDiscountParameters parameters = new ApplyDiscountParameters();
        parameters.setClientInterface(anInterface);
        parameters.setMetasearch(meta);
        return parameters;
    }


}