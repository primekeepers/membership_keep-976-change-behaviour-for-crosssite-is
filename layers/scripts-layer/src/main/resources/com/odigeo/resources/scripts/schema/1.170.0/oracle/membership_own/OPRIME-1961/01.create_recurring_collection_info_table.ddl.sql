CREATE TABLE MEMBERSHIP_OWN.GE_MEMBERSHIP_RECURRING_COLLECTION (
MEMBERSHIP_ID           VARCHAR2(25 CHAR) NOT NULL ,
RECURRING_COLLECTION_ID VARCHAR2(36 CHAR),
TIMESTAMP               DATE,
CONSTRAINT PK_MEM_REC_COLL PRIMARY KEY(MEMBERSHIP_ID)
);
