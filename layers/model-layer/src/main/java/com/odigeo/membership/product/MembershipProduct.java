package com.odigeo.membership.product;

import com.odigeo.fees.model.AbstractFee;
import com.odigeo.membership.util.Money;

import java.util.List;

public class MembershipProduct {

    private final String membershipId;
    private final MembershipProductType type;
    private final Money money;
    private final List<AbstractFee> fees;
    private final boolean rolledBackAllowed;
    private final boolean recurringCollection;

    MembershipProduct(Builder builder) {
        this.membershipId = builder.membershipId;
        this.type = builder.type;
        this.money = builder.money;
        this.fees = builder.fees;
        this.rolledBackAllowed = Builder.ROLLED_BACK_ALLOWED;
        this.recurringCollection = Builder.RECURRING_COLLECTION;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public MembershipProductType getType() {
        return type;
    }

    public Money getMoney() {
        return money;
    }

    public List<AbstractFee> getFees() {
        return fees;
    }

    public boolean isRolledBackAllowed() {
        return rolledBackAllowed;
    }

    public boolean isRecurringCollection() {
        return recurringCollection;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private static final boolean ROLLED_BACK_ALLOWED = true;
        private static final boolean RECURRING_COLLECTION = true;

        private String membershipId;
        private MembershipProductType type;
        private Money money;
        private List<AbstractFee> fees;

        public Builder membershipId(String membershipId) {
            this.membershipId = membershipId;
            return this;
        }

        public Builder type(MembershipProductType type) {
            this.type = type;
            return this;
        }

        public Builder money(Money money) {
            this.money = money;
            return this;
        }

        public Builder fees(List<AbstractFee> fees) {
            this.fees = fees;
            return this;
        }
        public MembershipProduct build() {
            return new MembershipProduct(this);
        }

    }
}
