package com.odigeo.membership.tracking;

import com.google.inject.Singleton;
import com.odigeo.commons.uuid.UUIDSerializer;
import com.odigeo.membership.AutorenewalTracking;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Objects;

@Singleton
public class AutorenewalTrackingStore {

    private static final String INSERT_AUTORENEWAL_TRACKING = "INSERT INTO GE_AUTORENEWAL_TRACKING (ID, MEMBERSHIP_ID, TIMESTAMP, OPERATION, REQUESTED_METHOD, REQUESTER, INTERFACE_ID) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?) ";

    public void insertAutorenewalTracking(DataSource dataSource, AutorenewalTracking autorenewalTracking) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT_AUTORENEWAL_TRACKING)) {
            ps.setBytes(1, UUIDSerializer.toBytes(autorenewalTracking.getId()));
            ps.setString(2, autorenewalTracking.getMembershipId());
            ps.setTimestamp(3, Timestamp.valueOf(autorenewalTracking.getTimestamp()));
            ps.setString(4, autorenewalTracking.getAutoRenewalOperation().toString());
            ps.setString(5, autorenewalTracking.getRequestedMethod());
            ps.setString(6, autorenewalTracking.getRequester());
            if (Objects.isNull(autorenewalTracking.getInterfaceId())) {
                ps.setNull(7, Types.INTEGER);
            } else {
                ps.setInt(7, autorenewalTracking.getInterfaceId());
            }
            ps.executeUpdate();
        }
    }
}
