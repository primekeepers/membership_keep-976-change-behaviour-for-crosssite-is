package com.odigeo.membership.parameters.request;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;

public class MembershipCreationBuilderTest {

    private static final int MONTHS_DURATION_DEFAULT = 12;

    private static final Long USER_ID = 1L;
    private static final String NAME = "Yukihiro";
    private static final String LAST_NAME = "Matsumoto";
    private static final String WEBSITE = "IT";
    private static final MemberStatus MEMBER_STATUS = MemberStatus.ACTIVATED;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now();
    private static final StatusAction STATUS_ACTION = StatusAction.CREATION;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final String MONTHS_DURATION = "6";
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final Long FEE_CONTAINER_ID = 222L;

    @Test
    public void testBuilderWithCorrectMonthsDuration() {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        validateCommonFields(membershipCreationBuilder);
        assertEquals(membershipCreationBuilder.getMonthsDuration(), Integer.parseInt(MONTHS_DURATION));
    }

    @Test
    public void testBuilderWithoutMonthsDuration() {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        membershipCreationBuilder.withMonthsDuration(null);
        assertEquals(membershipCreationBuilder.getMonthsDuration(), MONTHS_DURATION_DEFAULT);
    }

    @Test
    public void testBuilderWithEmptyDuration() {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        membershipCreationBuilder.withMonthsDuration(StringUtils.EMPTY);
        assertEquals(membershipCreationBuilder.getMonthsDuration(), MONTHS_DURATION_DEFAULT);
    }

    @Test
    public void testBuilderWithMonthsDuration() {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        membershipCreationBuilder.withMonthsDuration(String.valueOf(MONTHS_DURATION_DEFAULT));
        assertEquals(membershipCreationBuilder.getMonthsDuration(), MONTHS_DURATION_DEFAULT);
    }

    private void validateCommonFields(MembershipCreationBuilder membershipCreationBuilder) {
        assertEquals(membershipCreationBuilder.getActivationDate(), ACTIVATION_DATE);
        assertEquals(membershipCreationBuilder.getExpirationDate(), EXPIRATION_DATE);
        assertEquals(membershipCreationBuilder.getMembershipType(), MEMBERSHIP_TYPE);
        assertEquals(membershipCreationBuilder.getMemberStatus(), MEMBER_STATUS);
        assertEquals(membershipCreationBuilder.getSourceType(), SOURCE_TYPE);
        assertEquals(membershipCreationBuilder.getStatusAction(), STATUS_ACTION);
        assertEquals(membershipCreationBuilder.getSubscriptionPrice(), SUBSCRIPTION_PRICE);
        assertEquals(membershipCreationBuilder.getFeeContainerId(), FEE_CONTAINER_ID);
        MemberAccountCreation memberAccountCreation = membershipCreationBuilder.getMemberAccountCreationBuilder().build();
        assertEquals(memberAccountCreation.getLastNames(), LAST_NAME);
        assertEquals(memberAccountCreation.getName(), NAME);
        assertEquals(memberAccountCreation.getUserId(), USER_ID);
    }

    private MembershipCreationBuilder buildMembershipCreationBuilderByDefault() {
        MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder();
        membershipCreationBuilder.withActivationDate(ACTIVATION_DATE);
        membershipCreationBuilder.withExpirationDate(EXPIRATION_DATE);
        membershipCreationBuilder.withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                .lastNames(LAST_NAME)
                .name(NAME)
                .userId(USER_ID));
        membershipCreationBuilder.withMembershipType(MEMBERSHIP_TYPE);
        membershipCreationBuilder.withWebsite(WEBSITE);
        membershipCreationBuilder.withMemberStatus(MEMBER_STATUS);
        membershipCreationBuilder.withStatusAction(STATUS_ACTION);
        membershipCreationBuilder.withMonthsDuration(MONTHS_DURATION);
        membershipCreationBuilder.withDuration(Integer.getInteger(MONTHS_DURATION));
        membershipCreationBuilder.withDurationTimeUnit(TimeUnit.MONTHS);
        membershipCreationBuilder.withSourceType(SOURCE_TYPE);
        membershipCreationBuilder.withSubscriptionPrice(SUBSCRIPTION_PRICE);
        membershipCreationBuilder.withFeeContainerId(FEE_CONTAINER_ID);
        return membershipCreationBuilder;
    }

}
