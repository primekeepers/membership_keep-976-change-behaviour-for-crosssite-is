package com.odigeo.membership.member.update;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.member.AuditMemberAccountManager;
import com.odigeo.membership.member.MemberAccountStore;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertTrue;

public class MemberAccountUserIdUpdaterTest {

    private static final Long USER_ID = 123L;
    private static final Long MEMBER_ACCOUNT_ID = 1L;
    private static final String MEMBER_ACCOUNT_EMAIL = "joao.filho@hotmail.com";
    private static final String WEBSITE_ES = "ES";
    private static final UpdateMemberAccount UPDATE_MEMBER_ACCOUNT =
            new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID), "test", "test", MEMBER_ACCOUNT_EMAIL, WEBSITE_ES);

    private MemberAccountUserIdUpdater memberAccountUserIdUpdater;

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private AuditMemberAccountManager auditMemberAccountManager;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        memberAccountUserIdUpdater = new MemberAccountUserIdUpdater(memberAccountStore, auditMemberAccountManager);
    }

    @Test
    public void testUpdateMemberUserIdAndAudit() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong())).thenReturn(true);
        assertTrue(memberAccountUserIdUpdater.updateMemberAccountUserId(dataSource, MEMBER_ACCOUNT_ID, USER_ID));
        InOrder verifyOrder = inOrder(memberAccountStore, auditMemberAccountManager);
        verifyOrder.verify(memberAccountStore, times(1)).updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong());
        verifyOrder.verify(auditMemberAccountManager).auditUpdatedMemberAccount(dataSource, MEMBER_ACCOUNT_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateMemberUserIdThrowsException() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong())).thenThrow(new SQLException());
        memberAccountUserIdUpdater.updateMemberAccountUserId(dataSource, MEMBER_ACCOUNT_ID, USER_ID);
    }

    @Test(dataProvider = "notValidUpdateMemberAccountObjects", expectedExceptions = InvalidParameterException.class)
    public void testValidateParametersForUpdateUserIdOperation(UpdateMemberAccount updateMemberAccount) {
        memberAccountUserIdUpdater.validateParametersForUpdateUserIdOperation(updateMemberAccount);
    }

    @DataProvider(name = "notValidUpdateMemberAccountObjects")
    private static Object[][] notValidUpdateMemberAccountObjects() {
        return new Object[][]{
                new Object[]{new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID), "test", "test", null, null)},
                new Object[]{new UpdateMemberAccount(null, "test", "test", MEMBER_ACCOUNT_EMAIL, WEBSITE_ES)},
                new Object[]{new UpdateMemberAccount(null, "test", "test", null, WEBSITE_ES)}
        };
    }
}
