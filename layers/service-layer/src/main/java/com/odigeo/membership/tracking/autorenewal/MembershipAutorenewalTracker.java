package com.odigeo.membership.tracking.autorenewal;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.CaseFormat;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.AutorenewalTracking;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.tracking.RequestContext;
import io.vavr.control.Try;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Singleton;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class MembershipAutorenewalTracker {
    private static final Logger LOGGER = Logger.getLogger(MembershipAutorenewalTracker.class);
    private static final String COOKIE_HEADER_DIVIDER = ":";
    private static final String UNKNOWN_VISIT_INFORMATION = "";
    private static final String UNKNOWN_MODULE_NAME = "unknown module";
    private static final String VISIT_INFORMATION_HEADER = "visitInformation";
    private static final String MODULE_INFO_HEADER = "odigeo-module-info";
    private static final List<String> ENABLE_AUTO_RENEWAL_METHODS_NAME = Arrays.asList("enableAutoRenewal", "reactivateMembership");
    private static final List<String> DISABLE_AUTO_RENEWAL_METHODS_NAME = Arrays.asList("disableAutoRenewal", "deactivateMembership");
    private static final List<UpdateMembershipAction> ENABLE_AUTO_RENEWAL_OPERATIONS = Arrays.asList(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, UpdateMembershipAction.REACTIVATE_MEMBERSHIP);
    private static final List<UpdateMembershipAction> DISABLE_AUTO_RENEWAL_OPERATIONS = Arrays.asList(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, UpdateMembershipAction.DEACTIVATE_MEMBERSHIP);

    @AroundInvoke
    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    public Object aroundInvoke(InvocationContext context) throws Exception {
        Object proceed = context.proceed();
        long startTime = System.currentTimeMillis();
        extractFields(context).ifPresent(getAutorenewalTrackingService()::trackAutorenewalOperation);
        LOGGER.debug("Membership Auto-renewal tracking was started asynchronously. Returning now (" + (System.currentTimeMillis() - startTime) + " ms)");
        return proceed;
    }

    @VisibleForTesting
    Optional<AutorenewalTracking> extractFields(InvocationContext context) {
        final AutorenewalTracking fields = new AutorenewalTracking();
        return extractMembershipId(context).flatMap(membershipId -> {
            fields.setMembershipId(membershipId);
            return extractOperation(context);
        }).map(operation -> {
            fields.setAutoRenewalOperation(operation);
            fields.setRequestedMethod(CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, context.getMethod().getName()));
            fields.setRequester(extractClientModuleFromHeader());
            fields.setVisitInformation(getRequestContext().getHeader(VISIT_INFORMATION_HEADER).orElse(UNKNOWN_VISIT_INFORMATION));
            return fields;
        });
    }

    private Optional<AutoRenewalOperation> extractOperation(InvocationContext context) {
        Optional<AutoRenewalOperation> operation = Try.of(() -> (UpdateMembership) context.getParameters()[0])
                .onFailure(failure -> LOGGER.error("Error getting UpdateMembershipRequest from InvocationContext", failure))
                .toJavaOptional()
                .map(UpdateMembership::getOperation)
                .flatMap(this::toValidOperation);
        if (operation.isEmpty()) {
            operation = extractOperationFromMethod(context);
        }
        return operation;
    }

    private Optional<AutoRenewalOperation> extractOperationFromMethod(InvocationContext context) {
        String methodName = context.getMethod().getName();
        return lookForAutoRenewalMethod(ENABLE_AUTO_RENEWAL_METHODS_NAME, methodName, AutoRenewalOperation.ENABLE_AUTO_RENEW).map(Optional::of)
                .orElseGet(() -> lookForAutoRenewalMethod(DISABLE_AUTO_RENEWAL_METHODS_NAME, methodName, AutoRenewalOperation.DISABLE_AUTO_RENEW));
    }

    private Optional<AutoRenewalOperation> toValidOperation(String value) {
        return lookForAutoRenewalOperation(ENABLE_AUTO_RENEWAL_OPERATIONS, value, AutoRenewalOperation.ENABLE_AUTO_RENEW).map(Optional::of)
                .orElseGet(() -> lookForAutoRenewalOperation(DISABLE_AUTO_RENEWAL_OPERATIONS, value, AutoRenewalOperation.DISABLE_AUTO_RENEW));
    }

    private Optional<AutoRenewalOperation> lookForAutoRenewalOperation(List<UpdateMembershipAction> autoRenewalOperations, String value, AutoRenewalOperation responseExpected) {
        return autoRenewalOperations
                .stream()
                .filter(autoRenewalOperation -> StringUtils.isNotBlank(value) && autoRenewalOperation.name().contains(value))
                .map(enableAutoRenewalOperation -> responseExpected)
                .findFirst();
    }

    private Optional<AutoRenewalOperation> lookForAutoRenewalMethod(List<String> autoRenewalMethods, String methodName, AutoRenewalOperation responseExpected) {
        return autoRenewalMethods
                .stream()
                .filter(autoRenewalMethod -> autoRenewalMethod.equals(methodName))
                .map(enableAutoRenewalMethod -> responseExpected)
                .findFirst();
    }

    private Optional<String> extractMembershipId(InvocationContext context) {
        return Try.of(() -> (UpdateMembership) context.getParameters()[0])
                .onFailure(failure -> LOGGER.error("Error getting UpdateMembershipRequest from InvocationContext", failure))
                .map(UpdateMembership::getMembershipId)
                .toJavaOptional();
    }

    private String extractClientModuleFromHeader() {
        return getRequestContext().getHeader(MODULE_INFO_HEADER)
                .map(requestHeader -> requestHeader.split(COOKIE_HEADER_DIVIDER)[0])
                .filter(headerValue -> !headerValue.isEmpty())
                .orElse(UNKNOWN_MODULE_NAME);
    }

    @VisibleForTesting
    RequestContext getRequestContext() {
        return ConfigurationEngine.getInstance(RequestContext.class);
    }

    @VisibleForTesting
    AutorenewalTrackingService getAutorenewalTrackingService() {
        return ConfigurationEngine.getInstance(AutorenewalTrackingService.class);
    }

}
