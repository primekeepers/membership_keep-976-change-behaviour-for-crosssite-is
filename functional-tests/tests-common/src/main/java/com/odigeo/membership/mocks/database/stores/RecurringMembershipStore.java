package com.odigeo.membership.mocks.database.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class RecurringMembershipStore {

    private static final String GET_RECURRING_BY_MEMBERSHIP_ID_SQL = "SELECT RECURRING_ID FROM MEMBERSHIP_OWN.GE_MEMBERSHIP_RECURRING WHERE MEMBERSHIP_ID = ?";
    private static final String GET_RECURRING_COLLECTION_ID_BY_MEMBERSHIP_ID_SQL = "SELECT RECURRING_COLLECTION_ID FROM MEMBERSHIP_OWN.GE_MEMBERSHIP_RECURRING_COLLECTION WHERE MEMBERSHIP_ID = ?";

    public Optional<String> getRecurringByMembershipId(Connection conn, final long membershipId) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_RECURRING_BY_MEMBERSHIP_ID_SQL)) {
            preparedStatement.setLong(1, membershipId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(rs.getString("RECURRING_ID"));
                }
            }
        }
        return Optional.empty();
    }

    public Optional<String> getRecurringCollectionIdByMembershipId(Connection conn, final long membershipId) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_RECURRING_COLLECTION_ID_BY_MEMBERSHIP_ID_SQL)) {
            preparedStatement.setLong(1, membershipId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(rs.getString("RECURRING_COLLECTION_ID"));
                }
            }
        }
        return Optional.empty();
    }
}
