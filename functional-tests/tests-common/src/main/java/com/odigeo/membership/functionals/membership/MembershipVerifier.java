package com.odigeo.membership.functionals.membership;

import com.odigeo.membership.response.Membership;

import static org.testng.Assert.assertEquals;

public class MembershipVerifier {
    private final long memberId;
    private final String website;

    public MembershipVerifier(Long memberId, String website) {
        this.memberId = memberId;
        this.website = website;
    }

    public void verifyMembership(Membership membership) {
        assertEquals(this.memberId, membership.getMemberId());
        assertEquals(this.website, membership.getWebsite());
    }

}
