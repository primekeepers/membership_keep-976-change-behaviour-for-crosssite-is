package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRecurring;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

public class MemberAccountEntityBuilderTest {

    private static final long ID_1 = 111L;
    private static final long ID_2 = 222L;
    private static final long USER_ID_1 = 123L;
    private static final long USER_ID_2 = 456L;
    private static final String NAME = "name_test";
    private static final String LAST_NAMES = "lastName_test";
    private static final Date UTIL_DATE = new Date();
    private static final Timestamp TIMESTAMP = new Timestamp(UTIL_DATE.getTime());
    private static final long TEST_MEMBERSHIP_ID_ONE = 123L;
    private static final long TEST_MEMBERSHIP_ID_TWO = 456L;
    private static final String ID_MEMBERSHIP_COLUMN_LABEL = "ID";
    private static final String ID_COLUMN_LABEL = "ACCOUNT_ID";
    private static final String WEBSITE = "ES";
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final BigDecimal TOTAL_PRICE = BigDecimal.valueOf(34.02);
    private static final String CURRENCY_CODE = "EUR";
    private static final LocalDateTime NOW_DATETIME = LocalDateTime.now();
    private static final MembershipType MEMBERSHIP_TYPE_BASIC = MembershipType.BASIC;
    private static final String RECURRING_ID_01 = "A1234556";
    private static final String RECURRING_ID_02 = "B1234556";
    private Membership MEMBERSHIP_01 = new MembershipBuilder().setId(TEST_MEMBERSHIP_ID_ONE).setWebsite(WEBSITE)
            .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED)
            .setActivationDate(NOW_DATETIME).setExpirationDate(NOW_DATETIME.plusYears(1L))
            .setMemberAccountId(ID_1).setMembershipType(MEMBERSHIP_TYPE_BASIC)
            .setMembershipPricesBuilder(MembershipPrices.builder()
                    .totalPrice(TOTAL_PRICE)
                    .currencyCode(CURRENCY_CODE))
            .setProductStatus(ProductStatus.CONTRACT)
            .setTimestamp(NOW_DATETIME).setSourceType(SOURCE_TYPE).setRecurringId(RECURRING_ID_01).build();
    private Membership MEMBERSHIP_02 = new MembershipBuilder().setId(TEST_MEMBERSHIP_ID_TWO).setWebsite(WEBSITE)
            .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED)
            .setActivationDate(NOW_DATETIME).setExpirationDate(NOW_DATETIME.plusYears(1L))
            .setMemberAccountId(ID_2).setMembershipType(MEMBERSHIP_TYPE_BASIC)
            .setMembershipPricesBuilder(MembershipPrices.builder()
                    .totalPrice(TOTAL_PRICE)
                    .currencyCode(CURRENCY_CODE))
            .setProductStatus(ProductStatus.CONTRACT)
            .setTimestamp(NOW_DATETIME).setSourceType(SOURCE_TYPE).setRecurringId(RECURRING_ID_02).build();
    private static final MembershipRecurring MEMBERSHIP_RECURRING_01 = MembershipRecurring.builder()
            .recurringId(RECURRING_ID_01).build();
    private static final MembershipRecurring MEMBERSHIP_RECURRING_02 = MembershipRecurring.builder()
            .recurringId(RECURRING_ID_02).build();


    @Mock
    private ResultSet resultSet;
    @Mock
    private MembershipEntityBuilder membershipEntityBuilder;
    @Mock
    private MembershipRecurringEntityBuilder membershipRecurringEntityBuilder;

    private MemberAccountEntityBuilder memberAccountEntityBuilder;

    @BeforeMethod
    public void before() {
        openMocks(this);
        ConfigurationEngine.init(this::init);
        memberAccountEntityBuilder = new MemberAccountEntityBuilder();
    }

    @Test
    public void testBuild() throws SQLException {
        // Given
        mockResultSet();
        // When
        MemberAccount memberAccount = memberAccountEntityBuilder.build(resultSet, ID_COLUMN_LABEL);
        // Then
        assertEquals(memberAccount.getId(), ID_1);
        assertEquals(memberAccount.getUserId(), USER_ID_1);
        assertEquals(memberAccount.getName(), NAME);
        assertEquals(memberAccount.getLastNames(), LAST_NAMES);
        assertEquals(memberAccount.getTimestamp(), TIMESTAMP.toLocalDateTime());
        assertTrue(memberAccount.getMemberships().isEmpty());
    }

    @Test
    public void testBuildWithMembership() throws SQLException {
        // Given
        mockResultSet();
        mockMembershipRecurringEntityBuilder();
        when(membershipEntityBuilder.build(resultSet, false)).thenReturn(MEMBERSHIP_01).thenReturn(MEMBERSHIP_02);
        // When
        List<MemberAccount> memberAccountList = memberAccountEntityBuilder.buildWithMembership(resultSet);
        // Then
        assertEquals(memberAccountList.size(), 2);
        assertNotEquals(memberAccountList.get(0).getUserId(), memberAccountList.get(1).getUserId());
        assertNotEquals(memberAccountList.get(0).getId(), memberAccountList.get(1).getId());
        assertEquals(memberAccountList.get(0).getName(), memberAccountList.get(1).getName());
        assertEquals(memberAccountList.get(0).getLastNames(), memberAccountList.get(1).getLastNames());
        assertEquals(memberAccountList.get(0).getTimestamp(), memberAccountList.get(1).getTimestamp());
        memberAccountList.forEach(memberAccount -> assertEquals(memberAccount.getMemberships().size(), 1));
        List<Membership> memberships = memberAccountList.stream()
                                        .flatMap(memberAccount -> memberAccount.getMemberships().stream())
                                        .sorted(Comparator.comparingLong(Membership::getId))
                                        .collect(Collectors.toList());
        assertEquals(memberships, Arrays.asList(MEMBERSHIP_01, MEMBERSHIP_02));
        List<MembershipRecurring> membershipRecurring = memberships.stream()
                                        .flatMap(membershipWithRecurring -> membershipWithRecurring.getMembershipRecurring().stream())
                                        .sorted(Comparator.comparing(MembershipRecurring::getRecurringId))
                                        .collect(Collectors.toList());
        assertEquals(membershipRecurring, Arrays.asList(MEMBERSHIP_RECURRING_01, MEMBERSHIP_RECURRING_02));

    }

    private void mockResultSet() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getLong(ID_COLUMN_LABEL)).thenReturn(ID_1).thenReturn(ID_2);
        when(resultSet.getLong("USER_ID")).thenReturn(USER_ID_1).thenReturn(USER_ID_2);
        when(resultSet.getString("FIRST_NAME")).thenReturn(NAME);
        when(resultSet.getString("LAST_NAME")).thenReturn(LAST_NAMES);
        when(resultSet.getTimestamp("TIMESTAMP")).thenReturn(TIMESTAMP);
        when(resultSet.getLong(ID_MEMBERSHIP_COLUMN_LABEL)).thenReturn(TEST_MEMBERSHIP_ID_ONE).thenReturn(TEST_MEMBERSHIP_ID_TWO);
    }

    private void init(Binder binder) {
        binder.bind(MembershipEntityBuilder.class).toInstance(membershipEntityBuilder);
        binder.bind(MembershipRecurringEntityBuilder.class).toInstance(membershipRecurringEntityBuilder);
    }

    private void mockMembershipRecurringEntityBuilder() throws SQLException {
        when(membershipRecurringEntityBuilder.build(resultSet)).thenReturn(Optional.of(MEMBERSHIP_RECURRING_01)).thenReturn(Optional.of(MEMBERSHIP_RECURRING_02));
    }
}
