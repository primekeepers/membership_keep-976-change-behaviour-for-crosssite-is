package com.odigeo.membership.member.userarea;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingapi.BookingApiDAO;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.mock.v14.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v14.response.BuilderException;
import com.odigeo.bookingapi.mock.v14.response.CollectionAttemptBuilder;
import com.odigeo.bookingapi.mock.v14.response.CollectionSummaryBuilder;
import com.odigeo.bookingapi.mock.v14.response.CreditCardBuilder;
import com.odigeo.bookingapi.mock.v14.response.FeeContainerBuilder;
import com.odigeo.bookingapi.mock.v14.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v14.response.MovementBuilder;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.CollectionAttempt;
import com.odigeo.bookingapi.v14.responses.CollectionSummary;
import com.odigeo.bookingapi.v14.responses.FeeContainer;
import com.odigeo.bookingapi.v14.responses.ItineraryBooking;
import com.odigeo.bookingapi.v14.responses.MembershipPerks;
import com.odigeo.bookingapi.v14.responses.MembershipSubscriptionBooking;
import com.odigeo.bookingapi.v14.responses.ProductCategoryBooking;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MemberUserAreaServiceBeanAcceptanceTest {

    private static final long MEMBERSHIP_ID = 9999L;
    private static final long BOOKING_ID = 1234L;
    private static final long BOOKING_ID_TWO = 2345L;
    private static final long BOOKING_ID_THREE = 3456L;
    private static final long UNKNOWN_BOOKING_ID = 7777L;
    private static final long NULL_BOOKING_ID = 8888L;
    private static final Long FEE_ID = 11L;
    private static final BigDecimal THIRTY = new BigDecimal("30");
    private static final String WEBSITE_IT = Locale.ITALIAN.getCountry();
    private static final long MEMBER_ACCOUNT_ID = 666L;
    private static final String MEMBERSHIP_SUBSCRIPTION = "MEMBERSHIP_SUBSCRIPTION";
    private static final String ITINERARY = "Itinerary";
    private static final String PAID_STATUS = "PAID";
    private static final String CREDIT_CARD = "CREDITCARD";
    private static final String DECLINED_STATUS = "DECLINED";
    private final ProductCategoryBooking membershipSubscriptionBooking = new MembershipSubscriptionBooking();
    private final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private MemberService memberService;
    @Mock
    private UserHasAnyMembershipForBrandPredicate userHasAnyMembershipForBrandPredicateMock;
    @Mock
    private MembershipNoSQLManager membershipNoSQLManager;
    private MemberUserAreaServiceBean memberUserAreaServiceBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(getAbstractModule());
        memberUserAreaServiceBean = new MemberUserAreaServiceBean(
                new MemberSubscriptionDetailsBean(new BookingDetailInfoExtractor(),
                        new MembershipBookingDetailsBean(new BookingApiDAO(), membershipNoSQLManager)),
                memberService, userHasAnyMembershipForBrandPredicateMock, null,
                null, membershipNoSQLManager,null, null, null);
        when(membershipNoSQLManager.getBookingIdFromCache(anyLong())).thenReturn(Optional.empty());
    }

    @Test
    public void testGetAllMemberSubscriptionDetailsUnique() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, PAID_STATUS);
        configureMemberService(false);
        List<MemberSubscriptionDetails> allMemberSubscriptionDetails = memberUserAreaServiceBean.getAllMemberSubscriptionDetails(MEMBER_ACCOUNT_ID);
        assertTrue(amountEqualsTo(allMemberSubscriptionDetails.get(0), THIRTY));
    }

    @Test
    public void testGetAllMemberSubscriptionDetailsTwoSubs() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, PAID_STATUS);
        configureMemberService(true);
        List<MemberSubscriptionDetails> allMemberSubscriptionDetails = memberUserAreaServiceBean.getAllMemberSubscriptionDetails(MEMBER_ACCOUNT_ID);
        assertTrue(amountEqualsTo(allMemberSubscriptionDetails.get(1), THIRTY));
    }

    @Test
    public void testGetMemberSavedMoneySingleBooking() throws Exception {
        configureBookingApiManager(true, false, CREDIT_CARD, PAID_STATUS);
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertTrue(amountEqualsTo(memberSubscriptionDetails, BigDecimal.TEN));
    }

    @Test
    public void testGetMemberSavedMoneyMultipleBookings() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, PAID_STATUS);
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertTrue(amountEqualsTo(memberSubscriptionDetails, THIRTY));
    }

    @Test
    public void testGetMemberSavedMoneyNotExistentsBookings() throws Exception {
        configureBookingApiManager(false, true, CREDIT_CARD, PAID_STATUS);
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertEquals(THIRTY, memberSubscriptionDetails.getTotalSavings().getAmount());
    }

    @Test
    public void testGetMemberSavedMoneyNullBooking() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, PAID_STATUS);
        when(bookingApiManager.getBooking(NULL_BOOKING_ID)).thenReturn(null);
        when(bookingApiManager.searchBookings(any(SearchBookingsRequest.class))).thenReturn(buildSearchBookingResponse(Collections.singletonList(buildBookingSummary(NULL_BOOKING_ID))));
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertTrue(amountEqualsTo(memberSubscriptionDetails, BigDecimal.ZERO));
    }

    @Test
    public void testGetMemberBookingsException() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, PAID_STATUS);
        when(bookingApiManager.searchBookings(any(SearchBookingsRequest.class))).thenThrow(new DataAccessException("Bookings not found"));
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertTrue(amountEqualsTo(memberSubscriptionDetails, BigDecimal.ZERO));
    }

    @Test
    public void testGetBookingDetailWithSubscription() throws Exception {
        configureBookingSummaryList(false, false);
        setGetBookingsBySearchBookingPageResponseWithSubscription(Boolean.TRUE);
        validateBookingDetailWithSubscription(memberUserAreaServiceBean.getBookingDetailSubscription(BOOKING_ID));
    }

    @Test
    public void testGetBookingDetailWithoutSubscription() throws Exception {
        configureBookingSummaryList(false, false);
        setGetBookingsBySearchBookingPageResponseWithSubscription(Boolean.FALSE);
        validateBookingDetailWithoutSubscription(memberUserAreaServiceBean.getBookingDetailSubscription(BOOKING_ID));
    }

    @Test
    public void testGetBookingDetailWithSubscriptionPaymentMethod() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, PAID_STATUS);
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertEquals(memberSubscriptionDetails.getSubscriptionPaymentMethodType(), CREDIT_CARD);
    }

    @Test
    public void testGetBookingDetailWithSubscriptionPaymentMethodNotPaid() throws Exception {
        configureBookingApiManager(false, false, CREDIT_CARD, DECLINED_STATUS);
        MemberSubscriptionDetails memberSubscriptionDetails = memberUserAreaServiceBean.getMemberSubscriptionDetails(MEMBERSHIP_ID);
        assertEquals(memberSubscriptionDetails.getSubscriptionPaymentMethodType(), StringUtils.EMPTY);
    }

    private void validateBookingDetailWithoutSubscription(BookingDetail bookingDetailSubscription) {
        assertNull(bookingDetailSubscription);
    }

    private void validateBookingDetailWithSubscription(BookingDetail bookingDetailSubscription) {
        assertFalse(bookingDetailSubscription.getBookingProducts().isEmpty());
        assertTrue(bookingDetailSubscription.getBookingProducts().contains(membershipSubscriptionBooking));
        assertNotNull(bookingDetailSubscription.getCollectionSummary().getLastCreditCard());
    }

    private void setGetBookingsBySearchBookingPageResponseWithSubscription(final boolean hasSubscription)
            throws DataAccessException, BuilderException,
            com.odigeo.bookingsearchapi.mock.v1.response.BuilderException, BookingApiException {
        List<ProductCategoryBooking> productCategoryBookingList = getProductCategoryBooking(hasSubscription);
        CollectionSummary collectionSummary = new CollectionSummary();
        SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        if (hasSubscription) {
            CollectionAttempt collectionAttempt = getCollectionAttempt(CREDIT_CARD, PAID_STATUS);
            collectionSummary.setLastCreditCard(new CreditCardBuilder().build(new Random()));
            collectionSummary.setAttempts(Collections.singletonList(collectionAttempt));
            searchBookingsPageResponse.setBookings(Collections.singletonList(buildBookingSummary(BOOKING_ID)));
        }
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setBookingProducts(productCategoryBookingList);
        bookingDetail.setCollectionSummary(collectionSummary);
        when(bookingApiManager.searchBookings(any(SearchBookingsRequest.class))).thenReturn(searchBookingsPageResponse);
        when(bookingApiManager.getBooking(anyLong())).thenReturn(bookingDetail);
    }

    private List<ProductCategoryBooking> getProductCategoryBooking(boolean hasSubscription) {

        List<ProductCategoryBooking> productCategoryBookingList = new ArrayList<>();
        ProductCategoryBooking itineraryBooking = new ItineraryBooking();
        itineraryBooking.setProductType(ITINERARY);
        productCategoryBookingList.add(itineraryBooking);
        if (hasSubscription) {
            membershipSubscriptionBooking.setProductType(MEMBERSHIP_SUBSCRIPTION);
            productCategoryBookingList.add(membershipSubscriptionBooking);
        }
        return productCategoryBookingList;
    }

    private CollectionAttempt getCollectionAttempt(String type, String status) throws BuilderException {
        return new CollectionAttemptBuilder()
                .validMovements(Collections.singletonList(new MovementBuilder().status(status)))
                .gatewayCollectionMethodType(type).build(new Random());
    }

    private void configureBookingApiManager(boolean singletonList, boolean nonexistentBooking,
                                            String paymentMethod, String status)
            throws DataAccessException, BuilderException,
            com.odigeo.bookingsearchapi.mock.v1.response.BuilderException, BookingApiException {
        BookingDetail bookingDetail = configureBookingDetail(paymentMethod, status);
        when(bookingApiManager.getBooking(eq(BOOKING_ID))).thenReturn(bookingDetail);
        when(bookingApiManager.getBooking(eq(BOOKING_ID_TWO))).thenReturn(bookingDetail);
        when(bookingApiManager.getBooking(eq(BOOKING_ID_THREE))).thenReturn(bookingDetail);
        when(bookingApiManager.getBooking(UNKNOWN_BOOKING_ID)).thenThrow(new BookingApiException("Booking not found"));
        configureBookingSummaryList(singletonList, nonexistentBooking);
    }

    private void configureBookingSummaryList(boolean singletonList, boolean nonexistentBooking) throws DataAccessException, com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        List<BookingSummary> bookingSummaries = Arrays.asList(buildBookingSummary(BOOKING_ID), buildBookingSummary(BOOKING_ID_TWO), buildBookingSummary(BOOKING_ID_THREE));
        if (nonexistentBooking) {
            bookingSummaries = Arrays.asList(buildBookingSummary(BOOKING_ID), buildBookingSummary(BOOKING_ID_TWO), buildBookingSummary(BOOKING_ID_THREE), buildBookingSummary(UNKNOWN_BOOKING_ID));
        }
        if (singletonList) {
            when(bookingApiManager.searchBookings(any(SearchBookingsRequest.class))).thenReturn(buildSearchBookingResponse(Collections.singletonList(buildBookingSummary(BOOKING_ID))));
        } else {
            when(bookingApiManager.searchBookings(any(SearchBookingsRequest.class))).thenReturn(buildSearchBookingResponse(bookingSummaries));
        }
    }

    private SearchBookingsPageResponse<List<BookingSummary>> buildSearchBookingResponse(List<BookingSummary> bookingSummaries) {
        SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        searchBookingsPageResponse.setBookings(bookingSummaries);
        return searchBookingsPageResponse;
    }

    private BookingSummary buildBookingSummary(long bookingId) throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        BookingSummaryBuilder bookingSummaryBuilder = new BookingSummaryBuilder();
        BookingSummary bookingSummaryOne = bookingSummaryBuilder.build(new Random());
        bookingSummaryOne.getBookingBasicInfo().setId(bookingId);
        return bookingSummaryOne;
    }

    private void configureMemberService(boolean multiSubscription) throws MissingElementException, DataAccessException {
        when(memberService.getMembershipsByAccountId(anyLong())).thenReturn(buildMembershipList(multiSubscription));
    }


    private List<Membership> buildMembershipList(boolean multipleSubscriptions) {
        Membership membership = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE_IT).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(LocalDateTime.now()).setExpirationDate(LocalDateTime.MAX).setMemberAccountId(MEMBER_ACCOUNT_ID).setMembershipType(MEMBERSHIP_TYPE).build();
        if (multipleSubscriptions) {
            return Arrays.asList(membership, membership);
        } else {
            return Collections.singletonList(membership);
        }
    }

    private AbstractModule getAbstractModule() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(BookingApiManager.class).toProvider(() -> bookingApiManager);
                bind(MemberService.class).toProvider(() -> memberService);
            }
        };
    }

    private boolean amountEqualsTo(MemberSubscriptionDetails memberSubscriptionDetails, BigDecimal amount) {
        return memberSubscriptionDetails
                .getTotalSavings()
                .getAmount()
                .equals(amount);
    }

    private BookingDetail configureBookingDetail(String paymentMethod, String status) throws BuilderException {
        BookingDetail bookingDetail = new BookingDetailBuilder().build(new Random());
        bookingDetail.setAllFeeContainers(Collections.singletonList(buildFeeContainer()));
        bookingDetail.setMembershipPerks(buildMembershipPerks());
        bookingDetail.setCollectionSummary(buildCollectionSummary(paymentMethod, status));
        bookingDetail.setBookingProducts(getProductCategoryBooking(true));
        return bookingDetail;
    }

    private CollectionSummary buildCollectionSummary(String paymentMethod, String status) throws BuilderException {
        CollectionSummaryBuilder collectionSummaryBuilder = new CollectionSummaryBuilder();
        CollectionSummary collectionSummary = collectionSummaryBuilder.build(new Random());
        collectionSummary.setLastCreditCard(new CreditCardBuilder().build(new Random()));
        CollectionAttempt collectionAttempt = getCollectionAttempt(paymentMethod, status);
        collectionSummary.setAttempts(Collections.singletonList(collectionAttempt));
        membershipSubscriptionBooking.setProductType(MEMBERSHIP_SUBSCRIPTION);
        List<ProductCategoryBooking> productCategoryBookingList = getProductCategoryBooking(true);
        productCategoryBookingList.add(membershipSubscriptionBooking);
        return collectionSummary;
    }

    private MembershipPerks buildMembershipPerks() throws BuilderException {
        MembershipPerks membershipPerks = new MembershipPerksBuilder().build(new Random());
        membershipPerks.setFeeContainerId(FEE_ID);
        return membershipPerks;
    }

    private FeeContainer buildFeeContainer() throws BuilderException {
        FeeContainer feeContainer = new FeeContainerBuilder().build(new Random());
        feeContainer.setTotalAmount(BigDecimal.TEN);
        feeContainer.setId(FEE_ID);
        return feeContainer;
    }
}
