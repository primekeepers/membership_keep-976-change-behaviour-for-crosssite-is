package com.odigeo.membership.member.statusaction;

import com.odigeo.membership.MemberStatusAction;

import java.util.Optional;

public interface MemberStatusActionService {

    Optional<MemberStatusAction> lastStatusActionByMembershipId(Long memberId);
}
