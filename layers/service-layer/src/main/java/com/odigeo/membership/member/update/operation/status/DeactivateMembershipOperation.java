package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.membership.member.update.operation.RemindMeLaterOperation;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;

public class DeactivateMembershipOperation extends MembershipStatusChangeOperation {
    private final RemindMeLaterOperation remindMeLaterOperation;
    private final MemberService memberService;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        DeactivateMembershipOperation createOperation(DataSource dataSource);
    }

    @Inject
    public DeactivateMembershipOperation(@Assisted DataSource dataSource,
                                         RemindMeLaterOperation remindMeLaterOperation,
                                         MemberService memberService,
                                         MembershipMessageSendingManager membershipMessageSendingManager,
                                         MembershipStore membershipStore,
                                         MemberStatusActionStore memberStatusActionStore) {
        super(dataSource, membershipMessageSendingManager, membershipStore, memberStatusActionStore);
        this.memberService = memberService;
        this.remindMeLaterOperation = remindMeLaterOperation;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        return MemberStatus.DEACTIVATED.name().equals(deactivateMembership(updateMembership));
    }

    public String deactivateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, ExistingRecurringException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        if (membership.getStatus() == MemberStatus.ACTIVATED) {
            String newStatus = changeStatusMembership(membership, MemberStatus.DEACTIVATED, StatusAction.DEACTIVATION);
            if (membership.getRemindMeLater()) {
                remindMeLaterOperation.update(UpdateMembership.builderCloneOf(updateMembership)
                        .remindMeLater(Boolean.FALSE)
                        .build());
            }
            return newStatus;
        }
        return membership.getStatus().toString();
    }
}
