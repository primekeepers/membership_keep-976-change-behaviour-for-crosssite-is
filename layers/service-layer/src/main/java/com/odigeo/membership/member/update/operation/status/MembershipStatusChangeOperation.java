package com.odigeo.membership.member.update.operation.status;

import com.edreams.base.DataAccessException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperation;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;

public abstract class MembershipStatusChangeOperation implements MembershipUpdateOperation {

    private static final ImmutableMap<MemberStatus, MembershipRenewal> TRANSITION_CHANGE_STATUS_TO_AUTORENEWAL = Maps.immutableEnumMap(ImmutableMap.of(
            MemberStatus.ACTIVATED, MembershipRenewal.ENABLED,
            MemberStatus.DEACTIVATED, MembershipRenewal.DISABLED
    ));
    private final DataSource dataSource;
    private final MembershipMessageSendingManager membershipMessageSendingManager;
    private final MembershipStore membershipStore;
    private final MemberStatusActionStore memberStatusActionStore;


    protected MembershipStatusChangeOperation(DataSource dataSource,
                                              MembershipMessageSendingManager membershipMessageSendingManager,
                                              MembershipStore membershipStore,
                                              MemberStatusActionStore memberStatusActionStore) {
        this.dataSource = dataSource;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
        this.membershipStore = membershipStore;
        this.memberStatusActionStore = memberStatusActionStore;
    }

    protected String changeStatusMembership(Membership membership, MemberStatus desiredNewStatus, StatusAction statusAction) throws DataAccessException {
        MemberStatus previousStatus = membership.getStatus();
        MemberStatus newStatus = performStatusChange(membership, desiredNewStatus);
        if (newStatus.equals(desiredNewStatus)) {
            storeStatusAction(membership.getId(), statusAction);
            membershipMessageSendingManager.sendSubscriptionMessageToCRMTopicByRule(previousStatus, newStatus, membership);
            membershipMessageSendingManager.sendMembershipIdToMembershipReporter(membership.getId());
        }
        return newStatus.toString();
    }

    private MemberStatus performStatusChange(Membership membership, MemberStatus desiredStatus) throws DataAccessException {
        MemberStatus status = membership.getStatus();
        boolean isTransitionAllowed = status.isStatusTransitionAllowed(desiredStatus);
        if (isTransitionAllowed && membershipStore.updateStatusAndAutorenewal(dataSource, membership.getId(), desiredStatus, getMembershipRenewalByStatusChange(membership, desiredStatus))) {
            return desiredStatus;
        }
        return status;
    }

    private MembershipRenewal getMembershipRenewalByStatusChange(Membership membership, MemberStatus status) {
        return Optional.ofNullable(TRANSITION_CHANGE_STATUS_TO_AUTORENEWAL.get(status))
                .orElse(membership.getAutoRenewal());
    }

    private void storeStatusAction(Long membershipId, StatusAction statusAction) throws DataAccessRollbackException {
        try {
            memberStatusActionStore.createMemberStatusAction(dataSource, membershipId, statusAction);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to create memberStatusAction " + statusAction.toString() + " for membershipId " + membershipId, e);
        }
    }
}
