package com.odigeo.membership.functionals.membership;

public class MembershipAccountUpdateRequestBuilder {

    private String memberAccountId;
    private String name;
    private String lastNames;
    private String userId;
    private String operation;
    private String email;
    private String website;

    public String getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipAccountUpdateRequestBuilder setMemberAccountId(String memberId) {
        this.memberAccountId = memberId;
        return this;
    }

    public String getName() {
        return name;
    }

    public MembershipAccountUpdateRequestBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastNames() {
        return lastNames;
    }

    public MembershipAccountUpdateRequestBuilder setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public MembershipAccountUpdateRequestBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getOperation() {
        return operation;
    }

    public MembershipAccountUpdateRequestBuilder setOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public MembershipAccountUpdateRequestBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public MembershipAccountUpdateRequestBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }
}
