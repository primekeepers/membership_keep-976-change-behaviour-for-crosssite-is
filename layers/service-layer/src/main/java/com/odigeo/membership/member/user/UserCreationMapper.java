package com.odigeo.membership.member.user;

import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.BrandMapper;
import com.odigeo.userprofiles.api.v2.model.TrafficInterface;
import com.odigeo.userprofiles.api.v2.model.User;

import java.security.InvalidParameterException;
import java.util.Optional;

final class UserCreationMapper {

    private UserCreationMapper() {
    }

    static User mapToUser(UserCreation userCreation) {
        User user = new User();
        user.setEmail(userCreation.getEmail());
        user.setLocale(userCreation.getLocale());
        user.setWebsite(userCreation.getWebsite());
        user.setBrand(BrandMapper.map(userCreation.getWebsite()).orElse(null));
        user.setTrafficInterface(mapToTrafficInterface(userCreation.getTrafficInterfaceId()));
        return user;
    }

    private static TrafficInterface mapToTrafficInterface(Integer trafficInterfaceId) {
        TrafficInterface trafficInterface = TrafficInterface.fromId(trafficInterfaceId);
        return Optional.ofNullable(trafficInterface).orElseThrow(() -> new InvalidParameterException("Invalid traffic interface id " + trafficInterfaceId));
    }
}
