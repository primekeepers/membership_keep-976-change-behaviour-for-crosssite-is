package com.odigeo.membership.parameters;

import com.odigeo.membership.Membership;

public final class MembershipCreationMapper {

    public static MembershipCreation from(Membership membership) {
        return new MembershipCreation(new MembershipCreationBuilder()
                .withWebsite(membership.getWebsite())
                .withMemberAccountId(membership.getMemberAccountId())
                .withMemberAccountCreationBuilder(new MemberAccountCreation.
                        MemberAccountCreationBuilder().userId(membership.getMemberAccount().getUserId())
                        .name(membership.getMemberAccount().getName())
                        .lastNames(membership.getMemberAccount().getLastNames()))
                .withMemberStatus(membership.getStatus())
                .withAutoRenewal(membership.getAutoRenewal())
                .withExpirationDate(membership.getExpirationDate())
                .withBalance(membership.getBalance())
                .withProductStatus(membership.getProductStatus().name())
                .withSourceType(membership.getSourceType())
                .withMembershipType(membership.getMembershipType())
                .withMonthsDuration(String.valueOf(membership.getMonthsDuration()))
                .withDuration(membership.getDuration())
                .withDurationTimeUnit(membership.getDurationTimeUnit())
                .withCurrencyCode(membership.getCurrencyCode())
                .withRenewalPrice(membership.getRenewalPrice())
                .withSubscriptionPrice(membership.getTotalPrice())
                .withRecurringId(membership.getRecurringId())
                .withRecurringCollectionId(membership.getRecurringCollectionId())
                .withFeeContainerId(membership.getFeeContainerId()));
    }
}
