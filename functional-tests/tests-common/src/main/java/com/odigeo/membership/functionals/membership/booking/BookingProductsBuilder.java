package com.odigeo.membership.functionals.membership.booking;

import com.odigeo.bookingapi.mock.v14.response.AdditionalProductBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.InsuranceBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.MembershipSubscriptionBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.MonitoringAlertBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.PostsellServiceOptionBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.ProductCategoryBookingBuilder;
import com.odigeo.bookingapi.mock.v14.response.SmsNotificationBookingBuilder;
import com.odigeo.membership.util.ZonedDateTimeISOConverter;
import cucumber.deps.com.thoughtworks.xstream.annotations.XStreamConverter;

import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class BookingProductsBuilder {

    private Long bookingId;
    private ProductCategoryBookingType productCategoryBooking;
    @XStreamConverter(ZonedDateTimeISOConverter.class)
    private OffsetDateTime departureDate;
    private String arrivalName;

    public ProductCategoryBookingType getProductCategoryBooking() {
        return productCategoryBooking;
    }

    public void setProductCategoryBooking(ProductCategoryBookingType productCategoryBooking) {
        this.productCategoryBooking = productCategoryBooking;
    }

    public String getArrivalName() {
        return arrivalName;
    }

    public void setArrivalName(String arrivalName) {
        this.arrivalName = arrivalName;
    }

    public Calendar getDepartureDate() {
        return departureDate != null ? GregorianCalendar.from(departureDate.toZonedDateTime()) : null;
    }

    public void setDepartureDate(OffsetDateTime departureDate) {
        this.departureDate = departureDate;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public enum ProductCategoryBookingType {
        ITINERARY_BOOKING(ItineraryBookingBuilder.class),
        INSURANCE_BOOKING(InsuranceBookingBuilder.class),
        ADDITIONAL_PRODUCT_BOOKING(AdditionalProductBookingBuilder.class),
        MONITORING_ALERT_BOOKING(MonitoringAlertBookingBuilder.class),
        POSTSELL_SERVICE_OPTION_BOOKING(PostsellServiceOptionBookingBuilder.class),
        SMS_NOTIFICATION_BOOKING(SmsNotificationBookingBuilder.class),
        MEMBERSHIP_SUBSCRIPTION_BOOKING(MembershipSubscriptionBookingBuilder.class);

        private final Class<? extends ProductCategoryBookingBuilder<?>> builderClass;

        <T extends ProductCategoryBookingBuilder<?>> ProductCategoryBookingType(Class<T> builderClass) {
            this.builderClass = builderClass;
        }

        public ProductCategoryBookingBuilder<?> getBuilder() throws InstantiationException, IllegalAccessException {
            return builderClass.newInstance();
        }
    }
}
