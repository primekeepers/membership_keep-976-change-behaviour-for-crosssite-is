package com.odigeo.membership.util;

import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class EncryptUtilsTest {
    private EncryptUtils encryptUtils = new EncryptUtils();
    private static final String TEST_VALUE = "Aupa Atleti";

    @Test
    public void testSymmetricEncryptionAndDecryption() {
        final String encryptedValue = encryptUtils.encode64(TEST_VALUE);
        final String decryptedValue = encryptUtils.decode64(encryptedValue);
        assertEquals(decryptedValue, TEST_VALUE);
    }

    @Test
    public void testConsistentEncryptionAndDecryption() {
        assertEquals(encryptUtils.encode64(TEST_VALUE), encryptUtils.encode64(TEST_VALUE));
    }

    @Test
    public void testNotIdempotentEncryptionAndDecryption() {
        assertNotEquals(encryptUtils.encode64(encryptUtils.encode64(TEST_VALUE)), encryptUtils.encode64(TEST_VALUE));
    }

    @Test
    public void testUrlEncoding() throws UnsupportedEncodingException {
        String encodeDecodeText = encryptUtils.decodeUrlToken(encryptUtils.encodeUrlToken(TEST_VALUE));
        assertEquals(encodeDecodeText, TEST_VALUE);
    }
}
