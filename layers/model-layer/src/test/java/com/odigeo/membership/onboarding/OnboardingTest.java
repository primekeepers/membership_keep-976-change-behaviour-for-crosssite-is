package com.odigeo.membership.onboarding;


import bean.test.BeanTest;

public class OnboardingTest extends BeanTest<Onboarding> {

    @Override
    protected Onboarding getBean() {
        return new Onboarding.Builder()
                .withDevice(OnboardingDevice.APP)
                .withEvent(OnboardingEvent.COMPLETED)
                .withMemberAccountId(83L)
                .build();
    }
}