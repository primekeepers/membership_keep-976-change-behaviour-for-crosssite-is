package com.odigeo.membership.auth.usecases;

import com.google.inject.Inject;
import com.odigeo.authapi.AuthContract;
import com.odigeo.membership.auth.PermissionResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public class AuthorizationPermissionsUseCaseImpl implements AuthorizationPermissionsUseCase {
    @Inject
    private AuthContract authContract;

    @Override
    public PermissionResponse getTokenPermissions(String token) throws MembershipAuthServiceException {
        return authContract.getTokenPermissions(token);
    }
}
