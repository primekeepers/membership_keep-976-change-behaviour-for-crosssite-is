package com.odigeo.membership.member.memberapi.v1.util;

import com.google.inject.Singleton;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;

import java.util.Optional;

@Singleton
public class MembershipCreationRequestValidator {

    public void validateMembershipRequestParameters(CreateMembershipRequest createMembershipRequest) {
        int monthsDurationValue = createMembershipRequest.getMonthsToRenewal();
        TimeUnit durationTimeUnit = TimeUnit.getFromNullableValue(createMembershipRequest.getDurationTimeUnit());
        Optional<Integer> duration = Optional.ofNullable(createMembershipRequest.getDuration());
        if (monthsDurationValue > 0) {
            checkMonthDurationValues(monthsDurationValue, durationTimeUnit, duration);
        }
        if (duration.isEmpty() && monthsDurationValue == 0) {
            throw new MembershipBadRequestException("At least any of those fields must be informed: 'duration' or 'monthsToRenewal'");
        }
    }

    private void checkMonthDurationValues(Integer monthsDurationValue, TimeUnit durationTimeUnit, Optional<Integer> duration) {
        if (durationTimeUnit.equals(TimeUnit.DAYS)) {
            throw new MembershipBadRequestException("'monthsToRenewal' cannot be positive for 'durationTimeUnit' as DAYS, you must use duration field instead");
        }
        if (duration.isPresent() && durationTimeUnit.equals(TimeUnit.MONTHS)
                && !monthsDurationValue.equals(duration.get())) {
            throw new MembershipBadRequestException("'monthsToRenewal' and 'duration' cannot be different for durationTimeUnit as MONTHS");
        }
    }

}
