package com.odigeo.membership.member.userarea;

import com.odigeo.bookingapi.BookingApiDAO;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.responses.BookingBasicInfo;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipBookingDetailsBeanTest {

    @Mock
    private BookingApiDAO bookingApiDaoMock;
    @Mock
    private MembershipNoSQLManager membershipNoSQLManager;

    private MembershipBookingDetailsBean membershipBookingDetailsBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipBookingDetailsBean = new MembershipBookingDetailsBean(bookingApiDaoMock, membershipNoSQLManager);
    }

    @Test
    public void testBookingDetailSubscriptionReturnsEmptyWhenNoPrimeBookingFoundForGivenMembershipId() throws Exception {
        //Given
        final long membershipId = 244L;
        given(bookingApiDaoMock.searchPrimeBookingsByMembershipId(membershipId)).willReturn(emptyList());
        given(membershipNoSQLManager.getBookingIdFromCache(membershipId)).willReturn(Optional.empty());
        //When
        Optional<BookingDetail> foundBookingDetail = membershipBookingDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
        //Then
        assertFalse(foundBookingDetail.isPresent());
    }

    @Test
    public void testBookingDetailSubscriptionReturnsDetailWhenPrimeBookingFoundForGivenMembershipId() throws Exception {
        //Given
        final long membershipId = 244L;
        final long primeBookingId = 8883L;
        final BookingSummary primeBookingSummary = new BookingSummary();
        primeBookingSummary.setBookingBasicInfo(new BookingBasicInfo());
        primeBookingSummary.getBookingBasicInfo().setId(primeBookingId);
        final BookingDetail expectedPrimeBookingDetail = new BookingDetail();
        given(bookingApiDaoMock.searchPrimeBookingsByMembershipId(membershipId)).willReturn(singletonList(primeBookingSummary));
        given(bookingApiDaoMock.getBooking(primeBookingId)).willReturn(expectedPrimeBookingDetail);
        given(membershipNoSQLManager.getBookingIdFromCache(membershipId)).willReturn(Optional.empty());
        //When
        Optional<BookingDetail> foundBookingDetail = membershipBookingDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
        //Then
        assertEquals(foundBookingDetail, Optional.of(expectedPrimeBookingDetail));
    }

    @Test
    public void testBookingDetailsByMembershipIsEmptyWhenNoBookingsFoundByGivenMembershipId() {
        //Given
        final long membershipId = 324L;
        given(bookingApiDaoMock.searchBookingsByMembershipId(membershipId)).willReturn(emptyList());
        //When
        List<BookingDetail> bookingDetails = membershipBookingDetailsBean.getBookingDetailsByMembershipId(membershipId);
        //Then
        assertTrue(bookingDetails.isEmpty());
    }

    @Test
    public void testBookingDetailsByMembershipIsEmptyWhenExceptionRetrievingSingleBooking() throws Exception {
        //Given
        final long membershipId = 324L;
        final long bookingId = 4346L;
        BookingSummary bookingSummary = new BookingSummary();
        bookingSummary.setBookingBasicInfo(new BookingBasicInfo());
        bookingSummary.getBookingBasicInfo().setId(bookingId);
        given(bookingApiDaoMock.searchBookingsByMembershipId(membershipId)).willReturn(singletonList(bookingSummary));
        given(bookingApiDaoMock.getBooking(bookingId)).willThrow(new BookingApiException("expected exception retrieving booking"));
        //When
        List<BookingDetail> bookingDetails = membershipBookingDetailsBean.getBookingDetailsByMembershipId(membershipId);
        //Then
        assertTrue(bookingDetails.isEmpty());
    }

    @Test
    public void testBookingDetailsByMembershipReturnsBooking() throws Exception {
        //Given
        final long membershipId = 324L;
        final long bookingId = 4346L;
        BookingSummary bookingSummary = new BookingSummary();
        bookingSummary.setBookingBasicInfo(new BookingBasicInfo());
        bookingSummary.getBookingBasicInfo().setId(bookingId);
        BookingDetail expectedBookingDetail = new BookingDetail();
        given(bookingApiDaoMock.searchBookingsByMembershipId(membershipId)).willReturn(singletonList(bookingSummary));
        given(bookingApiDaoMock.getBooking(bookingId)).willReturn(expectedBookingDetail);
        //When
        List<BookingDetail> bookingDetails = membershipBookingDetailsBean.getBookingDetailsByMembershipId(membershipId);
        //Then
        assertEquals(bookingDetails.size(), 1);
        assertTrue(bookingDetails.contains(expectedBookingDetail));
    }

    @Test
    public void testBookingDetailSubscriptionReturnsDetailWhenPrimeBookingFoundForGivenMembershipIdfromCache() throws Exception {
        //Given
        final long membershipId = 8L;
        final long bookingId = 91L;

        BookingDetail expectedBookingDetail = new BookingDetail();
        given(membershipNoSQLManager.getBookingIdFromCache(membershipId)).willReturn(Optional.of(bookingId));
        given(bookingApiDaoMock.getBooking(bookingId)).willReturn(expectedBookingDetail);
        //When
        Optional<BookingDetail> foundBookingDetail = membershipBookingDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
        //Then
        assertEquals(foundBookingDetail, Optional.of(expectedBookingDetail));
    }
}
