package com.odigeo.membership.product;

import com.google.inject.Singleton;
import com.odigeo.db.DbUtils;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.exception.DataNotFoundException;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class MembershipSubscriptionFeeStore {

    private static final Logger logger = Logger.getLogger(MembershipSubscriptionFeeStore.class);

    private static final String ADD_FEE_SQL = "INSERT INTO GE_MEMBERSHIP_FEES(ID, MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE) VALUES (?, ?, ?, ?, ?) ";
    private static final String GET_FEE_SQL = "SELECT f.MEMBERSHIP_ID, f.FEE_TYPE, f.AMOUNT, f.CURRENCY FROM GE_MEMBERSHIP_FEES f WHERE f.MEMBERSHIP_ID = ? ";

    private static final String SELECT_NEXT_FEE_ID = "SELECT SEQ_GE_MEMBERSHIP_FEES_ID.nextval FROM dual ";

    public boolean createMembershipFee(DataSource dataSource, String membershipId, BigDecimal amount, String currency, String feeType) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_FEE_SQL)) {

            long feeId = DbUtils.nextSequence(connection.prepareStatement(SELECT_NEXT_FEE_ID));
            preparedStatement.setString(1, String.valueOf(feeId));
            preparedStatement.setString(2, membershipId);
            preparedStatement.setBigDecimal(3, amount);
            preparedStatement.setString(4, currency);
            preparedStatement.setString(5, feeType);

            preparedStatement.execute();
        }
        return true;
    }

    public List<MembershipFee> getMembershipFee(DataSource datasource, String membershipId) throws SQLException, DataNotFoundException {
        List<MembershipFee> membershipFeeList = new ArrayList<>();

        try (Connection connection = datasource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_FEE_SQL)) {

            preparedStatement.setString(1, String.valueOf(membershipId));
            try (ResultSet rs = preparedStatement.executeQuery()) {
                createFeeList(membershipFeeList, rs);
                if (membershipFeeList.isEmpty()) {
                    logger.warn("Missing fee for membershipId: " + membershipId);
                    throw new DataNotFoundException("Missing fee for membershipId: " + membershipId);
                }
            }
        }
        return membershipFeeList;
    }

    private void createFeeList(List<MembershipFee> membershipFeeList, ResultSet rs) throws SQLException {
        while (rs.next()) {
            membershipFeeList.add(buildFee(rs));
        }
    }

    private MembershipFee buildFee(ResultSet rs) throws SQLException {
        BigDecimal amount = rs.getBigDecimal("AMOUNT");
        String currency = rs.getString("CURRENCY");
        String membershipId = rs.getString("MEMBERSHIP_ID");
        String feeType = rs.getString("FEE_TYPE");
        return new MembershipFee(membershipId, amount, currency, feeType);
    }

}
