package com.odigeo.membership.member.rest;

import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.error.JsonIgnorePropertiesContextResolver;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import org.apache.http.auth.Credentials;

public final class ServiceConfigurationBuilder {

    private static final Integer MAX_CONCURRENT_CONNECTIONS = 20;
    private static final Integer DEFAULT_SOCKET_TIMEOUT_MS = 5000;
    private static final Integer DEFAULT_CONNECTION_TIMEOUT_MS = 5000;

    private ServiceConfigurationBuilder() {
    }

    public static <T> ServiceConfiguration<T> setUpDefault(Class<T> serviceClass) {
        ConnectionConfiguration connectionConfiguration = getConnectionConfiguration(DEFAULT_SOCKET_TIMEOUT_MS, DEFAULT_CONNECTION_TIMEOUT_MS);
        ServiceConfiguration<T> serviceConfiguration = new ServiceConfiguration.Builder<>(serviceClass)
                .withInterceptorConfiguration(new InterceptorConfiguration<>())
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(new SimpleRestErrorsHandler(serviceClass))
                .build();
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());
        return serviceConfiguration;
    }

    public static <T> ServiceConfiguration<T> setUpCustomWithCredentials(Class<T> serviceClass,
                                                                         Credentials credentials) {
        ConnectionConfiguration connectionConfiguration = getConnectionConfigurationWithCredentials(DEFAULT_SOCKET_TIMEOUT_MS,
            DEFAULT_CONNECTION_TIMEOUT_MS, credentials);
        ServiceConfiguration<T> serviceConfiguration = new ServiceConfiguration.Builder<>(serviceClass)
            .withInterceptorConfiguration(new InterceptorConfiguration<>())
            .withConnectionConfiguration(connectionConfiguration)
            .withRestErrorsHandler(new SimpleRestErrorsHandler(serviceClass))
            .build();
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());
        return serviceConfiguration;
    }

    private static ConnectionConfiguration getConnectionConfiguration(Integer socketTimeout, Integer connectionTimeout) {
        return new ConnectionConfiguration.Builder()
                .socketTimeoutInMillis(socketTimeout)
                .connectionTimeoutInMillis(connectionTimeout)
                .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .build();
    }

    private static ConnectionConfiguration getConnectionConfigurationWithCredentials(Integer socketTimeout, Integer connectionTimeout, Credentials credentials) {
        return new ConnectionConfiguration.Builder()
            .socketTimeoutInMillis(socketTimeout)
            .connectionTimeoutInMillis(connectionTimeout)
            .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
            .credentials(credentials)
            .build();
    }

}
