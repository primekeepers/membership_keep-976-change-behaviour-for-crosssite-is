package com.odigeo.membership.member.checkoutapi;

import com.edreams.base.BaseException;
import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.checkout.checkoutapi.v1.exception.ProductException;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.Fee;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.Price;
import com.edreamsodigeo.checkout.checkoutapi.v1.model.Product;
import com.edreamsodigeo.checkout.checkoutapi.v1.request.ProductPaymentSpecIdRequest;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.product.MembershipProduct;
import com.odigeo.membership.product.MembershipProductType;
import com.odigeo.membership.util.Money;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipCheckoutApiControllerTest {

    private static final String MEMBERSHIP_ID = "8";
    private static final Money MONEY = new Money(BigDecimal.TEN, Currency.getInstance("USD"));
    private static final String SUB_CODE = "AE10";
    private static final Date CURRENT_DATE = new Date();
    private static final AbstractFee FEE = new FixFee(Long.valueOf(MEMBERSHIP_ID), FeeLabel.MARKUP_TAX, SUB_CODE,
            Currency.getInstance("USD"), null, BigDecimal.TEN, CURRENT_DATE);
    private static final String ERROR = "error";
    private static final String TRANSACTION_ID = "91";
    private static final String TRANSACTION_TYPE = "type";

    @Mock
    private MemberService memberService;
    @Mock
    private MembershipProduct membershipProduct;
    @Mock
    private ProductPaymentSpecIdRequest productPaymentSpecIdRequest;

    private MembershipCheckoutApiController membershipCheckoutApiController;
    private final Fee product_fee = new Fee();

    @BeforeMethod
    public void init() {
        openMocks(this);
        ConfigurationEngine.init(
                binder -> binder.bind(MemberService.class).toInstance(memberService)
        );
        membershipCheckoutApiController = new MembershipCheckoutApiController();
        when(membershipProduct.getMembershipId()).thenReturn(MEMBERSHIP_ID);
        when(membershipProduct.getType()).thenReturn(MembershipProductType.MEMBERSHIP);
        when(membershipProduct.getMoney()).thenReturn(MONEY);
        when(membershipProduct.getFees()).thenReturn(Collections.singletonList(FEE));
        product_fee.setId(Long.parseLong(MEMBERSHIP_ID));
        product_fee.setLabel(FeeLabel.MARKUP_TAX.name());
        product_fee.setSubCode(SUB_CODE);
        product_fee.setPrice(new Price(BigDecimal.TEN, Currency.getInstance("USD")));
        product_fee.setCreationDate(CURRENT_DATE);
    }

    @Test
    public void testGetProduct() throws Exception {
        when(memberService.getProductById(MEMBERSHIP_ID)).thenReturn(membershipProduct);
        Product product = membershipCheckoutApiController.getProduct(MEMBERSHIP_ID);
        validateProduct(product);
    }

    @Test
    public void testGetProductShouldReturnNull() throws Exception {
        when(memberService.getProductById(MEMBERSHIP_ID)).thenReturn(null);
        Product product = membershipCheckoutApiController.getProduct(MEMBERSHIP_ID);
        assertNull(product);
    }

    private void validateProduct(Product product) {
        assertEquals(product.getId(), MEMBERSHIP_ID);
        assertEquals(product.getType(), MembershipProductType.MEMBERSHIP.name());
        assertEquals(product.getSellingPrice().getAmount(), MONEY.getAmount());
        assertEquals(product.getSellingPrice().getCurrency(), MONEY.getCurrency());
        assertEquals(product.getFees(), Collections.singletonList(product_fee));
    }

    @DataProvider(name = "dataExceptions")
    public Object[][] getData() {
        return new Object[][]{
                {new MissingElementException(ERROR)},
                {new DataAccessException(ERROR)}
        };
    }

    @Test(dataProvider = "dataExceptions", expectedExceptions = ProductException.class)
    public void testGetProduct(BaseException exception) throws Exception {
        when(memberService.getProductById(MEMBERSHIP_ID)).thenThrow(exception);
        membershipCheckoutApiController.getProduct(MEMBERSHIP_ID);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSetProductPaymentSpecId() throws Exception {
        membershipCheckoutApiController.setProductPaymentSpecId(TRANSACTION_ID, productPaymentSpecIdRequest);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testGetPaymentOptions() {
        membershipCheckoutApiController.getPaymentOptions(TRANSACTION_ID, TRANSACTION_TYPE);
    }
}
