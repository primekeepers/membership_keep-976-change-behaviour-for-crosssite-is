package com.odigeo.membership.mocks.feesapi;

import com.google.inject.Inject;
import com.odigeo.membership.server.ServerStopException;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;

@ScenarioScoped
public class FeesApiWorld {
    private final FeesServiceHttpServer feesServiceHttpServer;
    private final FeesServiceMock feesServiceMock;

    @Inject
    public FeesApiWorld(FeesServiceHttpServer feesServiceHttpServer, FeesServiceMock feesServiceMock) {
        this.feesServiceHttpServer = feesServiceHttpServer;
        this.feesServiceMock = feesServiceMock;
    }

    public void install() throws ServerStopException {
        if (feesServiceHttpServer.serverNotCreated()) {
            feesServiceHttpServer.startServer();
        }
        feesServiceHttpServer.addService(feesServiceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(feesServiceHttpServer)) {
            feesServiceHttpServer.clearServices();
        }
    }

    public FeesServiceMock getFeesServiceMock() {
        return feesServiceMock;
    }
}
