package com.odigeo.membership.member.monitoring.redis;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.odigeo.membership.redis.RedisClient;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class RedisHealthCheck extends HealthCheck {

    private static final String UP_MESSAGE = "Redis is up. URL: ";
    private static final String DOWN_MESSAGE = "Redis is down. URL: ";
    private static final String PONG = "PONG";

    private final RedisClient redisClient;

    @Inject
    public RedisHealthCheck(RedisClient redisClient) {
        super();
        this.redisClient = redisClient;
    }

    @Override
    protected Result check() {
        try (Jedis jedis = redisClient.getJedisConnection()) {
            return (jedis.ping().equalsIgnoreCase(PONG)) ? Result
                    .healthy(UP_MESSAGE + redisClient.getRedisClientConfiguration())
                    : Result.unhealthy(DOWN_MESSAGE + redisClient.getRedisClientConfiguration());
        } catch (JedisConnectionException e) {
            return Result.unhealthy(DOWN_MESSAGE + redisClient.getRedisClientConfiguration());
        }
    }
}
