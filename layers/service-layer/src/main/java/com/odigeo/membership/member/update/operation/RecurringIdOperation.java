package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import org.apache.commons.lang.StringUtils;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.Optional;

public class RecurringIdOperation implements MembershipUpdateOperation {
    private final DataSource dataSource;
    private final MembershipRecurringStore membershipRecurringStore;
    private final MembershipStore membershipStore;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        RecurringIdOperation createOperation(DataSource dataSource);
    }

    @Inject
    public RecurringIdOperation(@Assisted DataSource dataSource,
                                MembershipRecurringStore membershipRecurringStore,
                                MembershipStore membershipStore) {
        this.dataSource = dataSource;
        this.membershipRecurringStore = membershipRecurringStore;
        this.membershipStore = membershipStore;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        try {
            final long membershipId = Long.parseLong(updateMembership.getMembershipId());
            validateMembershipExistsAndDoesNotHaveRecurring(membershipId);
            String recurringId = Optional.ofNullable(updateMembership.getRecurringId())
                    .filter(StringUtils::isNotEmpty)
                    .orElseThrow(() -> new InvalidParameterException("Recurring id can't be null or empty string"));
            membershipRecurringStore.insertMembershipRecurring(dataSource, membershipId, recurringId);
        } catch (SQLException e) {
            throw new DataAccessException("Error inserting the recurring ID for membership ID : " + updateMembership.getMembershipId(), e);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Membership ID : " + updateMembership.getMembershipId() + " not available.", e);
        }
        return Boolean.TRUE;
    }

    private void validateMembershipExistsAndDoesNotHaveRecurring(long membershipId) throws SQLException, DataNotFoundException, ExistingRecurringException {
        final Membership membership = Optional.ofNullable(membershipStore.fetchMembershipById(dataSource, membershipId))
                .orElseThrow(() -> new DataNotFoundException("Membership not found"));
        if (StringUtils.isNotEmpty(membership.getRecurringId())) {
            throw new ExistingRecurringException("Recurring id already exists for membership " + membership.getId());
        }
    }
}
