package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Provider
@ServerInterceptor
public class LoggingInterceptor implements ContainerRequestFilter, WriterInterceptor {

    protected static final Logger LOGGER = Logger.getLogger(LoggingInterceptor.class);

    private final InterceptorHelper interceptorHelper = ConfigurationEngine.getInstance(InterceptorHelper.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            byte[] content = IOUtils.toByteArray(requestContext.getEntityStream());
            String preprocessedPath = requestContext.getUriInfo().getPath();
            if (!preprocessedPath.contains("auth")) {
                LOGGER.info("Received request (" + preprocessedPath + "): " + interceptorHelper.buildOneLineString(content));
                MultivaluedMap<String, String> queryParameters = requestContext.getUriInfo().getQueryParameters();
                if (!queryParameters.isEmpty()) {
                    LOGGER.info("queryParameters: " + ArrayUtils.toString(queryParameters.values()));
                }
            }
            requestContext.setEntityStream(new ByteArrayInputStream(content));
        } catch (IOException e) {
            throw new WebApplicationException(e);
        }
    }

    private void writeStream(WriterInterceptorContext context, OutputStream originalStream, ByteArrayOutputStream temporaryStream) throws IOException {
        context.setOutputStream(temporaryStream);
        context.proceed();
        LOGGER.info("Sending Response: " + interceptorHelper.buildOneLineString(temporaryStream.toByteArray()));
        temporaryStream.writeTo(originalStream);
        temporaryStream.close();
        context.setOutputStream(originalStream);
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext) throws IOException, WebApplicationException {
        try (OutputStream originalStream = writerInterceptorContext.getOutputStream()) {
            ByteArrayOutputStream temporaryStream = new ByteArrayOutputStream();
            writeStream(writerInterceptorContext, originalStream, temporaryStream);
        }
    }
}
