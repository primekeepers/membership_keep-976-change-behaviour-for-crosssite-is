package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;

public interface UpdateMembershipService {

    Boolean updateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, ExistingRecurringException;

    String deactivateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, ExistingRecurringException;

    String reactivateMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    String expireMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    String discardMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    boolean activateRenewalPendingToCollect(Membership membership) throws DataAccessException;

    String disableAutoRenewal(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;

    String enableAutoRenewal(UpdateMembership updateMembership) throws DataAccessException, MissingElementException;
}
