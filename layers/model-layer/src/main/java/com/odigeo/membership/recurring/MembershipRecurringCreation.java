package com.odigeo.membership.recurring;

public class MembershipRecurringCreation {

    private final String membershipId;
    private final String type;
    private final String website;
    private final String userId;

    public MembershipRecurringCreation(Builder builder) {
        this.membershipId = builder.membershipId;
        this.type = builder.type;
        this.website = builder.website;
        this.userId = builder.userId;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public String getType() {
        return type;
    }

    public String getWebsite() {
        return website;
    }

    public String getUserId() {
        return userId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String membershipId;
        private String type;
        private String website;
        private String userId;

        public Builder membershipId(String membershipId) {
            this.membershipId = membershipId;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public MembershipRecurringCreation build() {
            return new MembershipRecurringCreation(this);
        }
    }

}
