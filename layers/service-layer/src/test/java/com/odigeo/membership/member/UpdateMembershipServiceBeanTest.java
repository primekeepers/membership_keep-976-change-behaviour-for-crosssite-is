package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.renewal.MembershipRenewalService;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperation;
import com.odigeo.membership.member.update.operation.MembershipUpdateOperationFactory;
import com.odigeo.membership.member.update.operation.autorenewal.DisableAutoRenewalOperation;
import com.odigeo.membership.member.update.operation.autorenewal.EnableAutoRenewalOperation;
import com.odigeo.membership.member.update.operation.status.DeactivateMembershipOperation;
import com.odigeo.membership.member.update.operation.status.DiscardMembershipOperation;
import com.odigeo.membership.member.update.operation.status.ExpireMembershipOperation;
import com.odigeo.membership.member.update.operation.status.ReactivateMembershipOperation;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import static java.lang.Boolean.TRUE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UpdateMembershipServiceBeanTest {

    private static final String EXAMPLE_RETURN = "ABC";
    private static final UpdateMembership UPDATE_MEMBERSHIP = UpdateMembership.builder().build();
    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipRenewalService membershipRenewalService;
    @Mock
    private DeactivateMembershipOperation.Factory deactivateOperationFactory;
    @Mock
    private ReactivateMembershipOperation.Factory reactivateOperationFactory;
    @Mock
    private ExpireMembershipOperation.Factory expireMembershipOperationFactory;
    @Mock
    private EnableAutoRenewalOperation.Factory enableAutoRenewalFactory;
    @Mock
    private DisableAutoRenewalOperation.Factory disableAutoRenewalFactory;
    @Mock
    private DiscardMembershipOperation.Factory discardMembershipOperationFactory;

    @Mock
    private DeactivateMembershipOperation deactivateMembershipOperation;
    @Mock
    private ReactivateMembershipOperation reactivateOperation;
    @Mock
    private ExpireMembershipOperation expireMembershipOperation;
    @Mock
    private EnableAutoRenewalOperation enableAutoRenewalOperation;
    @Mock
    private DisableAutoRenewalOperation disableAutoRenewalOperation;
    @Mock
    private DiscardMembershipOperation discardMembershipOperation;

    @InjectMocks
    private final UpdateMembershipServiceBean updateMembershipServiceBean = new UpdateMembershipServiceBean();
    private ImmutableMap<UpdateMembershipAction, MembershipUpdateOperation> operations;

    @BeforeMethod
    public void setUp() throws ExistingRecurringException, DataAccessException, MissingElementException {
        openMocks(this);
        ImmutableMap<UpdateMembershipAction, MembershipUpdateOperationFactory> operationFactories = ImmutableMap.<UpdateMembershipAction, MembershipUpdateOperationFactory>builder()
                .put(UpdateMembershipAction.EXPIRE_MEMBERSHIP, expireMembershipOperationFactory)
                .put(UpdateMembershipAction.REACTIVATE_MEMBERSHIP, reactivateOperationFactory)
                .put(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP, deactivateOperationFactory)
                .put(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, enableAutoRenewalFactory)
                .put(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, disableAutoRenewalFactory)
                .put(UpdateMembershipAction.DISCARD_MEMBERSHIP, discardMembershipOperationFactory)
                .build();
        operations = ImmutableMap.<UpdateMembershipAction, MembershipUpdateOperation>builder()
                .put(UpdateMembershipAction.EXPIRE_MEMBERSHIP, expireMembershipOperation)
                .put(UpdateMembershipAction.REACTIVATE_MEMBERSHIP, reactivateOperation)
                .put(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP, deactivateMembershipOperation)
                .put(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, enableAutoRenewalOperation)
                .put(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, disableAutoRenewalOperation)
                .put(UpdateMembershipAction.DISCARD_MEMBERSHIP, discardMembershipOperation)
                .build();
        updateMembershipServiceBean.setOperations(operationFactories);
        updateMembershipServiceBean.setMembershipRenewalService(membershipRenewalService);
        operationFactories.forEach((action, factory) -> when(factory.createOperation(eq(dataSource))).thenReturn(operations.get(action)));
    }

    @Test
    public void testUpdate() throws ExistingRecurringException, MissingElementException, DataAccessException {
        when(deactivateMembershipOperation.update(any())).thenReturn(TRUE);
        assertTrue(updateMembershipServiceBean.updateMembership(UpdateMembershipObjectMother.aDeactivateUpdateMembership()));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testUpdateWrongOperation() throws ExistingRecurringException, MissingElementException, DataAccessException {
        assertTrue(updateMembershipServiceBean.updateMembership(UpdateMembershipObjectMother.anUpdateMembership()));
    }

    @Test
    public void deactivateMembership() throws MissingElementException, DataAccessException, ExistingRecurringException {
        when(deactivateMembershipOperation.deactivateMembership(any(UpdateMembership.class))).thenReturn(EXAMPLE_RETURN);
        assertEquals(updateMembershipServiceBean.deactivateMembership(UPDATE_MEMBERSHIP), EXAMPLE_RETURN);
    }

    @Test
    public void reactivateMembership() throws DataAccessException, MissingElementException {
        when(reactivateOperation.reactivateMembership(any(UpdateMembership.class))).thenReturn(EXAMPLE_RETURN);
        assertEquals(updateMembershipServiceBean.reactivateMembership(UPDATE_MEMBERSHIP), EXAMPLE_RETURN);
    }

    @Test
    public void expireMembership() throws DataAccessException, MissingElementException {
        when(expireMembershipOperation.expireMembership(any(UpdateMembership.class))).thenReturn(EXAMPLE_RETURN);
        assertEquals(updateMembershipServiceBean.expireMembership(UPDATE_MEMBERSHIP), EXAMPLE_RETURN);
    }

    @Test
    public void discardMembership() throws MissingElementException, DataAccessException {
        when(discardMembershipOperation.discardMembership(any(UpdateMembership.class))).thenReturn(EXAMPLE_RETURN);
        assertEquals(updateMembershipServiceBean.discardMembership(UPDATE_MEMBERSHIP), EXAMPLE_RETURN);
    }

    @Test
    public void enableAutoRenewal() throws MissingElementException, DataAccessException {
        when(enableAutoRenewalOperation.enableAutoRenewal(any(UpdateMembership.class))).thenReturn(EXAMPLE_RETURN);
        assertEquals(updateMembershipServiceBean.enableAutoRenewal(UPDATE_MEMBERSHIP), EXAMPLE_RETURN);
    }

    @Test
    public void disableAutoRenewal() throws MissingElementException, DataAccessException {
        when(disableAutoRenewalOperation.disableAutoRenewal(any(UpdateMembership.class))).thenReturn(EXAMPLE_RETURN);
        assertEquals(updateMembershipServiceBean.disableAutoRenewal(UPDATE_MEMBERSHIP), EXAMPLE_RETURN);
    }

    @Test
    public void testActivateRenewalPendingToCollect() throws Exception {
        Membership pendingToCollectMembership = UpdateMembershipObjectMother.anActivatedMembershipBuilder().setStatus(MemberStatus.PENDING_TO_COLLECT).build();
        when(membershipRenewalService.activatePendingToCollect(eq(dataSource), eq(pendingToCollectMembership)))
                .thenReturn(true);
        assertTrue(updateMembershipServiceBean.activateRenewalPendingToCollect(pendingToCollectMembership));
    }
}