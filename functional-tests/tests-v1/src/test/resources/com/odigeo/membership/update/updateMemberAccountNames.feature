Feature: Update names with updateMemberAccount service
  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
    And property to send IDs to Kafka has value false in db:

  Scenario Outline: The user wants to update the name and last name of the account
    When the user wants to update the MemberAccount:
      | memberAccountId | name   | lastNames   | operation   |
      | 123             | <name> | <lastNames> | <operation> |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId | firstName | lastNames   |
      | 123             | 4321   | <name>    | <lastNames> |
    Examples:
      | name   | lastNames | operation      |
      | GARCIA | JOSE      | UPDATE_NAMES   |