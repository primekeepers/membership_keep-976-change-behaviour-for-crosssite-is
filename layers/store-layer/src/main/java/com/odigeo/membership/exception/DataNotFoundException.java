package com.odigeo.membership.exception;

import com.edreams.base.DataAccessException;

public class DataNotFoundException extends DataAccessException {

    public DataNotFoundException(String message) {
        super(message);
    }
}
