package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.product.MembershipProduct;

import java.math.BigDecimal;
import java.util.List;

public interface MemberService {

    Membership getMembershipById(long membershipId) throws MissingElementException, DataAccessException;

    Membership getMembershipByIdWithMemberAccount(long membershipId) throws MissingElementException, DataAccessException;

    List<Membership> getMembershipsByAccountId(long memberAccountId) throws MissingElementException, DataAccessException;

    Boolean activateMembership(long membershipId, long bookingId, BigDecimal balance) throws DataAccessException, MissingElementException, UserApiException;

    Long createMembership(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException;

    MembershipProduct getProductById(String productId) throws MissingElementException, DataAccessException;
}
