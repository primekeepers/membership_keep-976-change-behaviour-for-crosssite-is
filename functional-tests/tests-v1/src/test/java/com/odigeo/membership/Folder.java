package com.odigeo.membership;

import com.odigeo.technology.docker.ContainerException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Locale;

class Folder {

    String getUserHomeFolder() {
        return System.getProperty("user.home");
    }

    String createAndgetHostLogsFolderInDockerFormat(String containerName, String uuid) throws ContainerException {
        try {
            String folderPath = String.format("%s/%s", getExecutionHostLogsFolder(uuid), containerName);
            Files.createDirectories(Paths.get(folderPath));
            if (isNotWindows()) {
                Files.setPosixFilePermissions(Paths.get(folderPath), PosixFilePermissions.fromString("rwxrwxrwx"));
            }
            return dockerFormat(folderPath);
        } catch (IOException e) {
            throw new ContainerException("cannot create host logs folder", e);
        }
    }

    String getExecutionHostLogsFolder(String uuid) {
        return String.format("%s/%s/%s", getUserHomeFolder(), "containersLogs", uuid);
    }

    String getContainerLogsFolder() {
        return "/opt/logs";
    }

    String dockerFormat(String folder) {
        return Paths.get(folder).toUri().getPath().replace(":", "").replace("/C/", "/c/");
    }

    private boolean isNotWindows() {
        String os = System.getProperty("os.name").toLowerCase(Locale.ENGLISH);
        return !os.contains("win");
    }
}
