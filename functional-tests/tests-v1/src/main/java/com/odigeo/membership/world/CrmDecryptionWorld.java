package com.odigeo.membership.world;

import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class CrmDecryptionWorld {
    private String decryptedToken;
    private MembershipInternalServerErrorException crmException;

    public String getDecryptedToken() {
        return decryptedToken;
    }

    public void setDecryptedToken(String decryptedToken) {
        this.decryptedToken = decryptedToken;
    }

    public MembershipInternalServerErrorException getCrmException() {
        return crmException;
    }

    public void setCrmException(MembershipInternalServerErrorException crmException) {
        this.crmException = crmException;
    }
}
