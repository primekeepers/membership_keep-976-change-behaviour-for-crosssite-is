package com.odigeo.membership.member.update.operation.autorenewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import com.odigeo.membership.member.update.operation.RemindMeLaterOperation;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.Mock;

import javax.sql.DataSource;

import java.sql.SQLException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public abstract class AutoRenewalChangeOperationTest {
    @Mock
    protected DataSource dataSource;
    @Mock
    protected RemindMeLaterOperation remindMelaterOperation;
    @Mock
    protected MemberService memberService;
    @Mock
    protected MembershipStore membershipStore;
    @Mock
    protected MembershipMessageSendingManager membershipMessageSendingManager;

    protected void configureOperation(MembershipRenewal membershipRenewal) throws MissingElementException, DataAccessException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setMembershipRenewal(membershipRenewal)
                .build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
    }

    protected void configureOperationException(MembershipRenewal membershipRenewal, AutoRenewalOperation autoRenewalOperation) throws MissingElementException, DataAccessException, SQLException {
        Membership membership = UpdateMembershipObjectMother.anActivatedMembershipBuilder()
                .setMembershipRenewal(membershipRenewal)
                .build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.updateAutoRenewal(eq(dataSource), eq(membership.getId()), eq(autoRenewalOperation)))
                .thenThrow(new SQLException());
    }
}
