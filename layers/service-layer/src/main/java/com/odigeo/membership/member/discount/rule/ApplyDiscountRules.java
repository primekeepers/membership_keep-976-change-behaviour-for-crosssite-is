package com.odigeo.membership.member.discount.rule;

import com.google.common.collect.Iterables;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.visitengineapi.v1.client.multitest.TestTokenMatcher;
import com.odigeo.visitengineapi.v1.client.multitest.TestTokenMatcherFactory;

import java.util.Collection;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Stream;

public abstract class ApplyDiscountRules implements BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> {

    public static final String RULES_USER = "User";
    public static final String RULES_WEBSITE = "Website";
    public static final String RULES_PRIME_SHARE = "PrimeShare";
    public static final String RULES_TRAVELLERS = "Travellers";

    @Override
    public boolean test(ApplyDiscountParameters parameters, ApplyDiscountContext context) {

        boolean result = getPredicate().test(parameters, context);

        updateGlobalContext(context, result);

        return result;

    }

    protected abstract BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> getPredicate();

    protected static boolean isABActive(ApplyDiscountContext context, Function<TestTokenMatcherFactory, TestTokenMatcher> ab) {
        return context.getTestTokenMatcher().matches(ab);
    }

    protected Stream<Membership> findMembershipsEligible(ApplyDiscountContext context) {
        return context.getRuleMembershipsEligible(getRuleId()).stream();
    }

    protected Stream<Membership> findMembershipsEligible(ApplyDiscountContext context, MembershipType membershipType) {
        return context.getRuleMembershipsEligible(getRuleId()).stream().filter(membership -> membershipType.equals(membership.getMembershipType()));
    }

    protected boolean updateMembershipsEligible(ApplyDiscountContext context, Collection<Membership> memberships) {
        return context.getRuleMembershipsEligible(getRuleId()).retainAll(memberships);
    }

    protected void updateGlobalContext(ApplyDiscountContext context, boolean result) {
        if (result) {
            Collection<Membership> memberships = context.getRuleMembershipsEligible(getRuleId());
            context.updateMembershipsEligible(memberships);
        } else {
            NotApplicationReason reason = Iterables.getLast(context.getRuleErrors(getRuleId()), null);
            context.addNotApplicationReason(reason);
        }
        context.addRuleResult(getRuleId(), result);
    }

    public void addError(ApplyDiscountContext context, NotApplicationReason error) {
        context.addRuleError(getRuleId(), error);
    }

    protected abstract String getRuleId();
}
