package com.odigeo.membership.member.util;

import com.odigeo.membership.MembershipRecurring;
import com.odigeo.membership.enums.db.MembershipRecurringField;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.MockitoAnnotations.openMocks;

import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class MembershipRecurringEntityBuilderTest {

    private static final String RECURRING_ID_COLUMN_NAME = MembershipRecurringField.RECURRING_ID.name();
    private static final String RECURRING_ID = "A123456";
    private static final MembershipRecurring MEMBERSHIP_RECURRING = MembershipRecurring.builder()
            .recurringId(RECURRING_ID)
            .build();

    @Mock
    ResultSet resultSet;

    MembershipRecurringEntityBuilder membershipRecurringEntityBuilder;

    @BeforeMethod
    public void init() {
        openMocks(this);
        membershipRecurringEntityBuilder = new MembershipRecurringEntityBuilder();
    }

    @Test
    public void testRecurringIsNull() throws SQLException {
        mockResultSet(null);
        assertFalse(membershipRecurringEntityBuilder.build(resultSet).isPresent());
    }

    @Test
    public void testRecurringIsEmpty() throws SQLException {
        mockResultSet(StringUtils.EMPTY);
        assertFalse(membershipRecurringEntityBuilder.build(resultSet).isPresent());
    }

    @Test
    public void testRecurringHasValue() throws SQLException {
        mockResultSet(RECURRING_ID);
        assertEquals(membershipRecurringEntityBuilder.build(resultSet).get(), MEMBERSHIP_RECURRING);
    }

    private void mockResultSet(String recurringId) throws SQLException {
        when(resultSet.getString(eq(RECURRING_ID_COLUMN_NAME))).thenReturn(recurringId);
    }
}
