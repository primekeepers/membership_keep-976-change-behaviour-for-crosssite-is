package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.rest.error.UnhandledExceptionMapper;
import com.odigeo.membership.SwaggerController;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.auth.AuthResourceImpl;
import com.odigeo.membership.member.checkoutapi.MembershipCheckoutApiController;
import com.odigeo.membership.member.memberapi.v1.CrmDecryptionController;
import com.odigeo.membership.member.memberapi.v1.MemberUserAreaController;
import com.odigeo.membership.member.memberapi.v1.MembershipBackOfficeServiceController;
import com.odigeo.membership.member.memberapi.v1.MembershipPropertiesConfigController;
import com.odigeo.membership.member.memberapi.v1.MembershipServiceController;
import com.odigeo.membership.member.memberapi.v1.OnboardingController;
import com.odigeo.membership.member.memberapi.v1.PostBookingMemberServiceController;
import com.odigeo.membership.member.memberapi.v1.SearchApiController;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.SwaggerContextService;
import ma.glasnost.orika.MapperFactory;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import java.util.HashSet;
import java.util.Set;

import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerCreditCardMapper;
import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerCreditCardTypeMapper;
import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerFlightMapper;
import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerMemberSubscriptionDetailsMapper;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication(@Context ServletConfig sc) {
        initSwagger(sc);
        singletons.add(ConfigurationEngine.getInstance(MembershipServiceController.class));
        singletons.add(new CrmDecryptionController());
        singletons.add(new MemberUserAreaController(getMemberUserAreaMapperFactory().getMapperFacade()));
        singletons.add(new PostBookingMemberServiceController(new GeneralMapperCreator().getMapper()));
        singletons.add(new MembershipBackOfficeServiceController());
        singletons.add(new SearchApiController());
        singletons.add(new MembershipPropertiesConfigController());
        singletons.add(new OnboardingController());
        singletons.add(new MembershipAuthenticationInterceptor());
        singletons.add(new LoggingInterceptor());
        singletons.add(new SwaggerController());
        singletons.add(new AuthResourceImpl());
        singletons.add(new MembershipCheckoutApiController());
        singletons.add(new UnhandledExceptionMapper());
        singletons.addAll(MembershipServiceConfiguration.getMembershipApiExceptionMappers());
    }

    private MapperFactory getMemberUserAreaMapperFactory() {
        MapperFactory mapperFactory = new GeneralMapperCreator().getMapperFactory();
        registerCreditCardMapper(mapperFactory);
        registerMemberSubscriptionDetailsMapper(mapperFactory);
        registerCreditCardTypeMapper(mapperFactory);
        registerFlightMapper(mapperFactory);
        return mapperFactory;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    private void initSwagger(ServletConfig sc) {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setBasePath("membership");
        beanConfig.setResourcePackage("com.odigeo.membership");
        new SwaggerContextService()
                .withServletConfig(sc)
                .withScanner(beanConfig)
                .initScanner();
    }
}
