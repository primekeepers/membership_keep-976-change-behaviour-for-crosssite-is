package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.userapi.UserApiException;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.creation.MembershipCreationFactoryProvider;
import com.odigeo.membership.member.creation.MembershipCreationService;
import com.odigeo.membership.member.creation.sql.BasicFreeMembershipService;
import com.odigeo.membership.member.creation.sql.NewMembershipSubscriptionService;
import com.odigeo.membership.member.creation.sql.PendingToCollectMembershipService;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.member.update.MemberAccountUserIdUpdater;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.membership.product.MembershipProduct;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.userapi.UserDTO;
import com.odigeo.userprofiles.api.v1.UserServiceInternalManager;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MemberServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private MembershipCreationFactoryProvider membershipCreationFactoryProviderMock;
    @Mock
    private NewMembershipSubscriptionService newMembershipSubscriptionCreationMock;
    @Mock
    private PendingToCollectMembershipService pendingToCollectMembershipCreationMock;
    @Mock
    private BasicFreeMembershipService basicFreeMembershipServiceCreationMock;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManagerMock;
    @Mock
    private UserService userService;
    @Mock
    private MembershipCreationService membershipCreationService;
    @Mock
    private UserServiceInternalManager userServiceInternalManagerMock;
    @Mock
    private MemberAccountUserIdUpdater memberAccountUserIdUpdater;
    @Mock
    private MembershipNoSQLManager membershipNoSQLManager;
    @Mock
    private MembershipFeesService membershipFeesService;

    @InjectMocks
    private final MemberServiceBean memberServiceBean = new MemberServiceBean();

    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final int DEFAULT_DURATION = 12;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final long USER_ID = 123L;
    private static final long NEW_USER_ID = 321L;
    private static final long MEMBERSHIP_ID1 = 10L;
    private static final long MEMBERSHIP_ID2 = 11L;
    private static final long MEMBERSHIP_ID4 = 13L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long MEMBER_ACCOUNT_ID2 = 2L;
    private static final long BOOKING_ID = 800L;
    private static final String WEBSITE1 = "ES";
    private static final String WEBSITE2 = "FR";
    private static final String MEMBER_NAME1 = "JOSE";
    private static final String MEMBER_SURNAME1 = "LUIS";
    private static final String TEST_EXCEPTION = "TestException";
    private static final String TEST_EMAIL = "test@edreams.com";
    private static final long FEE_CONTAINER_ID = 91L;
    private static final long FEE_ID = 2L;
    private static final String SUB_CODE = "AE10";
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final String CURRENCY_CODE = "USD";
    private static final AbstractFee FEE = new FixFee(FEE_ID, FeeLabel.MARKUP_TAX, SUB_CODE,
        Currency.getInstance(CURRENCY_CODE), null, SUBSCRIPTION_PRICE, new Date());

    private final MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
    private final UserDTO userDTO = new UserDTO();
    private final Membership activeMembership1 = getMembership(MEMBERSHIP_ID1, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, null);
    private final Membership inactiveMembership2 = getMembership(MEMBERSHIP_ID2, WEBSITE2, MemberStatus.DEACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID2, null);
    private final Membership activeMembership4 = getMembership(MEMBERSHIP_ID4, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, null);
    private final Membership pendingMembershipWithMemberAccount = getMembership(MEMBERSHIP_ID4, WEBSITE1, MemberStatus.PENDING_TO_ACTIVATE, MembershipRenewal.DISABLED, MEMBER_ACCOUNT_ID1, memberAccount);
    private final Membership activeMembership1WithMemberAccount = getMembership(MEMBERSHIP_ID1, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, memberAccount);
    private final MembershipCreation membershipCreation = new MembershipCreationBuilder()
            .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                    .userId(USER_ID)
                    .lastNames(MEMBER_SURNAME1)
                    .name(MEMBER_NAME1))
            .withWebsite(WEBSITE1).withMembershipType(MEMBERSHIP_TYPE)
        .withSourceType(SourceType.POST_BOOKING).withSubscriptionPrice(BigDecimal.TEN)
            .withMemberAccountId(MEMBER_ACCOUNT_ID1).build();

    @BeforeMethod
    public void setUp() throws DataAccessException, SQLException, MissingElementException, UserApiException {
            openMocks(this);
        ConfigurationEngine.init(this::configure);
        when(membershipCreationFactoryProviderMock.getInstance(any(MembershipCreation.class))).thenCallRealMethod();
        when(membershipStore.fetchMembershipByIdWithMemberAccount(dataSource, MEMBERSHIP_ID1)).thenReturn(pendingMembershipWithMemberAccount);
        when(userService.getUser(USER_ID)).thenReturn(userDTO);
        when(userService.fixPendingMailConfirmationUserAccount(anyLong())).thenReturn(Optional.empty());
    }

    private void configure(Binder binder) {
        binder.bind(MemberAccountService.class).toInstance(memberAccountService);
        binder.bind(NewMembershipSubscriptionService.class).toInstance(newMembershipSubscriptionCreationMock);
        binder.bind(PendingToCollectMembershipService.class).toInstance(pendingToCollectMembershipCreationMock);
        binder.bind(BasicFreeMembershipService.class).toInstance(basicFreeMembershipServiceCreationMock);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManagerMock);
        binder.bind(MembershipCreationService.class).toInstance(membershipCreationService);
        binder.bind(UserServiceInternalManager.class).toInstance(userServiceInternalManagerMock);
        binder.bind(MembershipNoSQLManager.class).toInstance(membershipNoSQLManager);
    }

    @Test
    public void testGetMembershipsById() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipById(dataSource, MEMBERSHIP_ID1)).thenReturn(activeMembership1);
        when(membershipStore.fetchMembershipById(dataSource, MEMBERSHIP_ID2)).thenReturn(inactiveMembership2);
        assertEquals(memberServiceBean.getMembershipById(MEMBERSHIP_ID1), activeMembership1);
        assertEquals(memberServiceBean.getMembershipById(MEMBERSHIP_ID2), inactiveMembership2);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipsByIdException() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipById(dataSource, MEMBERSHIP_ID1)).thenThrow(new SQLException());
        memberServiceBean.getMembershipById(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMembershipsByIdNotFound() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipById(dataSource, MEMBERSHIP_ID1)).thenThrow(new DataNotFoundException(StringUtils.EMPTY));
        memberServiceBean.getMembershipById(MEMBERSHIP_ID1);
    }

    @Test
    public void testCreateMembershipNewSubscription() throws DataAccessException, MissingElementException {
        when(membershipCreationService.create(membershipCreation)).thenReturn(MEMBERSHIP_ID1);
        long newMembershipId = memberServiceBean.createMembership(membershipCreation);
        assertEquals(newMembershipId, MEMBERSHIP_ID1);
        verify(membershipCreationService).create(membershipCreation);
    }

    @Test
    public void testCreateMembershipPendingToCollect() throws DataAccessException, MissingElementException {
        MembershipCreation createMembershipPendingToCollect = new MembershipCreationBuilder()
                .withMemberStatus(MemberStatus.PENDING_TO_COLLECT)
                .withMemberAccountId(MEMBER_ACCOUNT_ID1)
                .build();
        when(membershipCreationService.create(createMembershipPendingToCollect)).thenReturn(MEMBERSHIP_ID2);
        long newMembershipId = memberServiceBean.createMembership(createMembershipPendingToCollect);
        assertEquals(newMembershipId, MEMBERSHIP_ID2);
    }

    @Test
    public void testCreateMembershipBasicFree() throws DataAccessException, MissingElementException {
        MembershipCreation createMembershipBasicFree = new MembershipCreationBuilder()
                .withMembershipType(MembershipType.BASIC_FREE)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID))
                .build();
        when(membershipCreationService.create(eq(createMembershipBasicFree))).thenReturn(MEMBERSHIP_ID2);
        long newMembershipId = memberServiceBean.createMembership(createMembershipBasicFree);
        assertEquals(newMembershipId, MEMBERSHIP_ID2);
        verifyNoInteractions(userService);
    }

    @Test
    public void testCreateMembershipAndUserBasicFree() throws DataAccessException, MissingElementException {
        UserCreation userCreation = new UserCreation.Builder()
                .withEmail(TEST_EMAIL)
                .withLocale(Locale.ENGLISH.toString())
                .withTrafficInterfaceId(1)
                .build();
        MembershipCreation createMembershipBasicFree = new MembershipCreationBuilder()
                .withMembershipType(MembershipType.BASIC_FREE)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withUserCreation(userCreation)
                .build();
        when(membershipCreationService.create(eq(createMembershipBasicFree))).thenReturn(MEMBERSHIP_ID2);
        long newMembershipId = memberServiceBean.createMembership(createMembershipBasicFree);
        assertEquals(newMembershipId, MEMBERSHIP_ID2);
    }

    @Test

    public void testActivateMembership() throws DataAccessException, SQLException, MissingElementException, UserApiException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBERSHIP_ID1, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership1));
        assertTrue(memberServiceBean.activateMembership(MEMBERSHIP_ID1, BOOKING_ID, BigDecimal.TEN));
        verify(membershipCreationService).activate(MEMBERSHIP_ID1, BigDecimal.TEN);
    }

    @Test
    public void testActivateMembershipForPendingConfirmationEmailStatus() throws DataAccessException, SQLException, MissingElementException, UserApiException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(userService.fixPendingMailConfirmationUserAccount(USER_ID)).thenReturn(Optional.of(NEW_USER_ID));
        when(memberAccountUserIdUpdater.updateMemberAccountUserId(any(DataSource.class), eq(MEMBER_ACCOUNT_ID1), eq(NEW_USER_ID))).thenReturn(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBERSHIP_ID1, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership1));
        assertTrue(memberServiceBean.activateMembership(MEMBERSHIP_ID1, BOOKING_ID, BigDecimal.TEN));
        verify(membershipCreationService).activate(MEMBERSHIP_ID1, BigDecimal.TEN);
    }

    @Test
    public void testNotActivateMembership() throws DataAccessException, SQLException, MissingElementException, UserApiException {
        when(membershipCreationService.activate(MEMBERSHIP_ID1, BigDecimal.TEN)).thenReturn(Optional.empty());
        assertFalse(memberServiceBean.activateMembership(MEMBERSHIP_ID1, BOOKING_ID, BigDecimal.TEN));
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testActivateMemberSQLException() throws DataAccessException, SQLException, MissingElementException, UserApiException {
        setUpActivateMembershipStoreResponseException();
        when(membershipCreationService.activate(MEMBERSHIP_ID1, BigDecimal.TEN)).thenThrow(new DataAccessException(""));
        memberServiceBean.activateMembership(MEMBERSHIP_ID1, BOOKING_ID, BigDecimal.TEN);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipsByAccountIdSQLException()
            throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.fetchMembershipByMemberAccountId(dataSource, MEMBER_ACCOUNT_ID1)).thenThrow(new SQLException());
        memberServiceBean.getMembershipsByAccountId(MEMBER_ACCOUNT_ID1);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMembershipsByAccountIdMissingElementException()
            throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.fetchMembershipByMemberAccountId(dataSource, MEMBER_ACCOUNT_ID1)).thenThrow(new DataNotFoundException(StringUtils.EMPTY));
        memberServiceBean.getMembershipsByAccountId(MEMBER_ACCOUNT_ID1);
    }

    @Test
    public void testGetMembershipsByAccountId() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipByMemberAccountId(dataSource, MEMBER_ACCOUNT_ID1)).thenReturn(Collections.singletonList(activeMembership1));
        assertNotNull(memberServiceBean.getMembershipsByAccountId(MEMBER_ACCOUNT_ID1));
    }

    @Test
    public void testActivateMembership_SendCorrectIdToReporter() throws DataAccessException, SQLException, MissingElementException, UserApiException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBERSHIP_ID4, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership4));
        assertTrue(memberServiceBean.activateMembership(MEMBERSHIP_ID4, BOOKING_ID, BigDecimal.TEN));
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(MEMBERSHIP_ID4);
    }

    @Test
    public void testCreateMembership_SendCorrectIdToReporter() throws DataAccessException, MissingElementException {
        when(newMembershipSubscriptionCreationMock.createMembership(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID1);
        long newMembershipId = memberServiceBean.createMembership(membershipCreation);
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(newMembershipId);
    }

    @Test
    public void testActivateMembershipSubscription() throws SQLException, DataAccessException, MissingElementException, UserApiException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBERSHIP_ID4, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership4));
        assertTrue(memberServiceBean.activateMembership(MEMBERSHIP_ID4, BOOKING_ID, BigDecimal.TEN));
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(MEMBERSHIP_ID4);
        verify(membershipMessageSendingManagerMock).sendSubscriptionMessageToCRMTopic(activeMembership4, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManagerMock).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(any(Membership.class), eq(BOOKING_ID), eq(false));
    }

    @Test
    public void testActivateMembership_AlreadyHasAnActiveMembership() throws DataAccessException, SQLException, MissingElementException, UserApiException {
        // GIVEN
        when(membershipCreationService.activate(MEMBERSHIP_ID1, BigDecimal.TEN)).thenReturn(Optional.empty());

        // WHEN
        final Boolean isMembershipActivated = memberServiceBean.activateMembership(MEMBERSHIP_ID1, BOOKING_ID, BigDecimal.TEN);
        // THEN
        assertFalse(isMembershipActivated);
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingManagerMock, never()).sendMembershipIdToMembershipReporter(anyLong());
        verify(membershipMessageSendingManagerMock, never()).sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class));
        verify(membershipMessageSendingManagerMock, never()).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(any(Membership.class), anyLong(), eq(false));
    }

    private void setUpActivateMembershipStoreResponse(final boolean isUpdated) throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipByIdWithMemberAccount(eq(dataSource), anyLong())).thenReturn(pendingMembershipWithMemberAccount).thenReturn(activeMembership1WithMemberAccount);
        when(membershipStore.activateMember(any(DataSource.class), anyLong(), any(LocalDateTime.class), any(LocalDateTime.class), any(BigDecimal.class))).thenReturn(isUpdated);
    }

    private void setUpActivateMembershipStoreResponseException() throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipById(any(DataSource.class), anyLong())).thenThrow(new SQLException(TEST_EXCEPTION));
        when(membershipStore.activateMember(any(DataSource.class), anyLong(), any(LocalDateTime.class), any(LocalDateTime.class), any(BigDecimal.class))).thenThrow(new SQLException(TEST_EXCEPTION));
    }

    private static Membership getMembership(long memberId, String website, MemberStatus status, MembershipRenewal renewal, long memberAccountId, MemberAccount memberAccount) {
        MembershipBuilder builder = new MembershipBuilder().setId(memberId).setWebsite(website)
            .setStatus(status).setMembershipRenewal(renewal).setActivationDate(NOW)
            .setMemberAccountId(memberAccountId).setSourceType(SourceType.FUNNEL_BOOKING)
            .setMemberAccount(memberAccount).setMonthsDuration(DEFAULT_DURATION)
            .setFeeContainerId(FEE_CONTAINER_ID)
            .setMembershipPricesBuilder(MembershipPrices.builder().totalPrice(SUBSCRIPTION_PRICE)
                .currencyCode(CURRENCY_CODE));
        if (MemberStatus.ACTIVATED == status) {
            builder.setExpirationDate(NOW.plusMonths(DEFAULT_DURATION));
        }
        return builder.build();
    }

    @Test
    public void testGetProductByIdRedis() throws Exception {
        when(membershipNoSQLManager.get(String.valueOf(MEMBERSHIP_ID4)))
            .thenReturn(Optional.of(pendingMembershipWithMemberAccount));
        when(membershipFeesService.retrieveFees(any())).thenReturn(Collections.singletonList(FEE));
        MembershipProduct product = memberServiceBean.getProductById(String.valueOf(MEMBERSHIP_ID4));
        validateMembershipProduct(product);
    }

    private void validateMembershipProduct(MembershipProduct product) {
        assertNotNull(product);
        assertEquals(product.getMembershipId(), String.valueOf(MEMBERSHIP_ID4));
        assertEquals(product.getFees(), Collections.singletonList(FEE));
        assertEquals(product.getMoney().getAmount(), SUBSCRIPTION_PRICE);
        assertEquals(product.getMoney().getCurrency(), Currency.getInstance(CURRENCY_CODE));
    }

    @Test
    public void testGetProductByIdOracle() throws Exception {
        when(membershipNoSQLManager.get(String.valueOf(MEMBERSHIP_ID4)))
            .thenReturn(Optional.empty());
        when(membershipStore.fetchMembershipById(dataSource, MEMBERSHIP_ID4))
            .thenReturn(pendingMembershipWithMemberAccount);
        when(membershipFeesService.retrieveFees(any())).thenReturn(Collections.singletonList(FEE));
        MembershipProduct product = memberServiceBean.getProductById(String.valueOf(MEMBERSHIP_ID4));
        validateMembershipProduct(product);
    }
}
