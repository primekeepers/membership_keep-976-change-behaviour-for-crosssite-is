package com.odigeo.membership.tracking.autorenewal;

import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.AutorenewalTracking;
import com.odigeo.membership.tracking.AutorenewalTrackingStore;
import com.odigeo.visitengineapi.v1.VisitEngine;
import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.interfaces.Interface;
import com.odigeo.visitengineapi.v1.request.InvalidParametersException;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class AutorenewalTrackingServiceBeanTest {
    private static final String VISIT_INFORMATION = "123";
    private static final Integer VISIT_ID = 321;
    private AutorenewalTrackingServiceBean autorenewalTrackingServiceBean;

    @Mock
    AutorenewalTrackingStore autorenewalTrackingStore;
    @Mock
    VisitEngine visitEngine;
    @Mock
    VisitResponse visitResponse;
    @Mock
    Interface clientInterface;

    @Captor
    ArgumentCaptor<AutorenewalTracking> autorenewalTrackingBuilderCaptor;
    @Captor
    ArgumentCaptor<String> visitInformationCaptor;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        autorenewalTrackingServiceBean = spy(new AutorenewalTrackingServiceBean());
        doReturn(autorenewalTrackingStore).when(autorenewalTrackingServiceBean).getAutoRenewalTrackingStore();
        doReturn(visitEngine).when(autorenewalTrackingServiceBean).getVisitEngine();
    }

    @Test
    public void testTrackAutorenewalOperation() throws SQLException, InvalidParametersException, VisitEngineException {
        doReturn(visitResponse).when(visitEngine).buildVisit(any());
        doReturn(clientInterface).when(visitResponse).getClientInterface();
        doReturn(VISIT_ID).when(clientInterface).getId();

        AutorenewalTracking autorenewalTracking = autorenewalTrackingWithVisitInformation(VISIT_INFORMATION);
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        verify(visitEngine).buildVisit(visitInformationCaptor.capture());
        verify(autorenewalTracking).setInterfaceId(anyInt());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
        assertSame(visitInformationCaptor.getValue(), VISIT_INFORMATION);
        assertEquals(autorenewalTracking.getInterfaceId(), VISIT_ID);
    }

    @Test
    public void testTrackAutorenewalOperationVisitEngineException() throws SQLException, InvalidParametersException, VisitEngineException {
        doThrow(new InvalidParametersException("")).when(visitEngine).buildVisit(any());
        AutorenewalTracking autorenewalTracking = autorenewalTrackingWithVisitInformation(VISIT_INFORMATION);
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        verify(visitEngine).buildVisit(visitInformationCaptor.capture());
        verify(autorenewalTracking, never()).setInterfaceId(anyInt());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
        assertSame(visitInformationCaptor.getValue(), VISIT_INFORMATION);
    }

    @Test
    public void testTrackAutorenewalOperationWithoutInterfaceId() throws SQLException, InvalidParametersException, VisitEngineException {
        AutorenewalTracking autorenewalTracking = autorenewalTrackingWithVisitInformation(VISIT_INFORMATION);
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        verify(visitEngine).buildVisit(visitInformationCaptor.capture());
        verify(autorenewalTracking, never()).setInterfaceId(anyInt());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
        assertSame(visitInformationCaptor.getValue(), VISIT_INFORMATION);
    }

    @Test
    public void testTrackAutorenewalOperationWithoutVisitInformation() throws SQLException, InvalidParametersException, VisitEngineException {
        AutorenewalTracking autorenewalTracking = autorenewalTrackingWithVisitInformation("");
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        verify(visitEngine, never()).buildVisit(any());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
    }

    @Test
    public void testExceptionDuringTrackAutorenewalOperationWithoutVisitInformation() throws SQLException, InvalidParametersException, VisitEngineException {
        doThrow(new SQLException()).when(autorenewalTrackingStore).insertAutorenewalTracking(any(), any());
        AutorenewalTracking autorenewalTracking = autorenewalTrackingWithVisitInformation("");
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        verify(visitEngine, never()).buildVisit(any());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
    }

    private AutorenewalTracking autorenewalTrackingWithVisitInformation(String visitInformation) {
        AutorenewalTracking autorenewalTracking = spy(new AutorenewalTracking());
        doReturn(visitInformation).when(autorenewalTracking).getVisitInformation();
        doReturn(AutoRenewalOperation.DISABLE_AUTO_RENEW).when(autorenewalTracking).getAutoRenewalOperation();
        doReturn("").when(autorenewalTracking).getRequestedMethod();
        doReturn("").when(autorenewalTracking).getRequestedMethod();
        doReturn("").when(autorenewalTracking).getMembershipId();
        return autorenewalTracking;
    }
}
