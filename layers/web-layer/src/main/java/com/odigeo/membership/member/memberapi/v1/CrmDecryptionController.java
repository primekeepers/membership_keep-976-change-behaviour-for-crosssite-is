package com.odigeo.membership.member.memberapi.v1;

import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.v1.CRMDecryptionApi;

import java.io.UnsupportedEncodingException;

public class CrmDecryptionController extends AbstractController implements CRMDecryptionApi {

    @Override
    public String getCrmDecryption(String token) throws MembershipServiceException {
        try {
            return getCipher().decryptToken(token);
        } catch (UnsupportedEncodingException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

}
