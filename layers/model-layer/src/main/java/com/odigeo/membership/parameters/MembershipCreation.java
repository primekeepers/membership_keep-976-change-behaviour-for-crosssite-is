package com.odigeo.membership.parameters;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@SuppressWarnings("PMD.GodClass")
public class MembershipCreation {

    private MemberAccountCreation memberAccountCreation;
    private String website;
    private Long memberAccountId;
    private MemberStatus memberStatus;
    private MembershipRenewal autoRenewal;
    private BigDecimal balance;
    private LocalDateTime activationDate;
    private LocalDateTime expirationDate;
    private StatusAction statusAction;
    private BigDecimal subscriptionPrice;
    private BigDecimal renewalPrice;
    private Integer renewalDuration;
    private String productStatus;
    private String currencyCode;
    private MembershipType membershipType;
    private int monthsDuration;
    private TimeUnit durationTimeUnit;
    private Integer duration;
    private SourceType sourceType;
    private String recurringId;
    private UUID recurringCollectionId;
    private UserCreation userCreation;
    private Long feeContainerId;
    private final boolean shoppingBasketProduct;

    public MembershipCreation(MembershipCreationBuilder builder) {
        this.memberAccountCreation = Optional.ofNullable(builder.getMemberAccountCreationBuilder()).orElseGet(MemberAccountCreation::builder).build();
        this.website = builder.getWebsite();
        this.memberStatus = Optional.ofNullable(builder.getMemberStatus()).orElse(MemberStatus.PENDING_TO_ACTIVATE);
        this.autoRenewal = Optional.ofNullable(builder.getAutoRenewal()).orElse(MembershipRenewal.ENABLED);
        this.balance = builder.getBalance();
        this.activationDate = builder.getActivationDate();
        this.expirationDate = builder.getExpirationDate();
        this.statusAction = Optional.ofNullable(builder.getStatusAction()).orElse(StatusAction.CREATION);
        this.membershipType = Optional.ofNullable(builder.getMembershipType()).orElse(MembershipType.BASIC);
        this.monthsDuration = builder.getMonthsDuration();
        this.durationTimeUnit = Optional.ofNullable(builder.getDurationTimeUnit()).orElse(TimeUnit.MONTHS);
        this.duration = builder.getDuration();
        this.subscriptionPrice = builder.getSubscriptionPrice();
        this.renewalPrice = builder.getRenewalPrice();
        this.productStatus = builder.getProductStatus();
        this.currencyCode = builder.getCurrencyCode();
        this.sourceType = builder.getSourceType();
        this.memberAccountId = builder.getMemberAccountId();
        this.recurringId = builder.getRecurringId();
        this.recurringCollectionId = builder.getRecurringCollectionId();
        this.userCreation = builder.getUserCreation();
        this.renewalDuration = builder.getRenewalDuration();
        this.feeContainerId = builder.getFeeContainerId();
        this.shoppingBasketProduct = builder.isShoppingBasketProduct();
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(@NotNull MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
    }

    public MembershipRenewal getAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(MembershipRenewal autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public BigDecimal getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(BigDecimal subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public void setRenewalPrice(BigDecimal renewalPrice) {
        this.renewalPrice = renewalPrice;
    }

    public Integer getRenewalDuration() {
        return renewalDuration;
    }

    public void setRenewalDuration(Integer renewalDuration) {
        this.renewalDuration = renewalDuration;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public StatusAction getStatusAction() {
        return statusAction;
    }

    public void setStatusAction(StatusAction statusAction) {
        this.statusAction = statusAction;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public void setMonthsDuration(int monthsDuration) {
        this.monthsDuration = monthsDuration;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    public UUID getRecurringCollectionId() {
        return recurringCollectionId;
    }

    public void setRecurringCollectionId(UUID recurringCollectionId) {
        this.recurringCollectionId = recurringCollectionId;
    }

    public UserCreation getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(UserCreation userCreation) {
        this.userCreation = userCreation;
    }

    public MemberAccountCreation getMemberAccountCreation() {
        return memberAccountCreation;
    }

    public void setMemberAccountCreation(MemberAccountCreation memberAccountCreation) {
        this.memberAccountCreation = memberAccountCreation;
    }

    public Long getFeeContainerId() {
        return feeContainerId;
    }

    public void setFeeContainerId(Long feeContainerId) {
        this.feeContainerId = feeContainerId;
    }

    public TimeUnit getDurationTimeUnit() {
        return durationTimeUnit;
    }

    public void setDurationTimeUnit(TimeUnit durationTimeUnit) {
        this.durationTimeUnit = durationTimeUnit;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public boolean isShoppingBasketProduct() {
        return shoppingBasketProduct;
    }

}
