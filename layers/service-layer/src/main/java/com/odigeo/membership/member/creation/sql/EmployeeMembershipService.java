package com.odigeo.membership.member.creation.sql;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;

import static java.time.temporal.ChronoUnit.YEARS;

public class EmployeeMembershipService extends MembershipSQLServiceProvider {

    private static final int EMPLOYEE_MONTHS_DURATION = 360;
    private static final Function<LocalDateTime, LocalDateTime> PLUS_30_YEARS = startDate -> startDate.plus(30, YEARS);
    private final SearchService searchService;
    private final MembershipMessageSendingManager membershipMessageSendingManager;
    private final UpdateMembershipService updateMembershipService;

    @Inject
    public EmployeeMembershipService(SearchService searchService, MembershipMessageSendingManager membershipMessageSendingManager, UpdateMembershipService updateMembershipService) {
        this.searchService = searchService;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
        this.updateMembershipService = updateMembershipService;
    }

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        try {
            Preconditions.checkNotNull(membershipCreation.getUserCreation(), "UserCreation mandatory for employees membership creation");
            deactivateAlreadyActiveMemberships(retrieveActivatedMembershipByUser(membershipCreation, searchService));
            configureEmployeeMembership(membershipCreation);
            Long memberId = getMemberManager().createMember(dataSource, membershipCreation);
            storeMemberStatusAction(dataSource, memberId, StatusAction.INTERNAL_CREATION);
            membershipMessageSendingManager.sendMembershipIdToMembershipReporter(memberId);
            membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(membershipCreation.getUserCreation().getEmail(), membershipCreation, SubscriptionStatus.SUBSCRIBED);
            return memberId;
        } catch (DataAccessException | MissingElementException | ExistingRecurringException e) {
            throw new DataAccessRollbackException("Error creating membership for userId " + membershipCreation.getMemberAccountCreation().getUserId(), e);
        }
    }

    private void configureEmployeeMembership(MembershipCreation membershipCreation) {
        membershipCreation.setActivationDate(LocalDateTime.now());
        membershipCreation.setExpirationDate(PLUS_30_YEARS.apply(membershipCreation.getActivationDate()));
        membershipCreation.setMonthsDuration(EMPLOYEE_MONTHS_DURATION);
        membershipCreation.setSubscriptionPrice(BigDecimal.ZERO);
        membershipCreation.setAutoRenewal(MembershipRenewal.DISABLED);
        membershipCreation.setBalance(BigDecimal.ZERO);
    }

    private void deactivateAlreadyActiveMemberships(List<Membership> alreadyActiveMemberships) throws MissingElementException, DataAccessException, ExistingRecurringException {
        for (Membership membership : alreadyActiveMemberships) {
            UpdateMembership updateMembership = UpdateMembership.builder()
                    .membershipId(String.valueOf(membership.getId()))
                    .operation(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP.name())
                    .build();
            updateMembershipService.updateMembership(updateMembership);
        }
    }

    private MemberManager getMemberManager() {
        return ConfigurationEngine.getInstance(MemberManager.class);
    }
}
