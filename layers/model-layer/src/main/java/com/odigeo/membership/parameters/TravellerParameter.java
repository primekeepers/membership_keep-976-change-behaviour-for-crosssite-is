package com.odigeo.membership.parameters;

public class TravellerParameter {
    private String name;
    private String lastNames;

    public TravellerParameter() {
    }

    public TravellerParameter(String name, String lastNames) {
        this.name = name;
        this.lastNames = lastNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }
}
