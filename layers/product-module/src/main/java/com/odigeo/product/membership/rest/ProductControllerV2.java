package com.odigeo.product.membership.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.product.MembershipProductServiceV2;
import com.odigeo.product.membership.service.ProductServiceV2;
import com.odigeo.product.membership.service.ProductServiceV2New;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.CloseProductProviderRequest;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.payment.PaymentEntity;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import com.odigeo.product.v2.model.responses.SetPaymentDetailsResponse;
import org.apache.log4j.Logger;

public class ProductControllerV2 implements MembershipProductServiceV2 {

    private static final Logger logger = Logger.getLogger(ProductControllerV2.class);

    @Override
    public BookingApiMembershipInfo getBookingApiProductInfo(String productId) throws ProductException {
        return getProductService().getBookingApiProductInfo(productId);
    }

    @Override
    public Product getProduct(String productId) throws ProductException {
        return getProductService().getProduct(productId);
    }

    @Override
    public Product getProductDetails(String productId) throws ProductException {
        return getProductService().getProduct(productId);
    }

    @Override
    public SetPaymentDetailsResponse setPaymentDetails(String productId, long providerProductId, PaymentEntity paymentEntity) throws ProductException {
        logger.info("Call function: setPaymentDetails with the following parameters: id: " + productId + ", providerProductId: " + providerProductId + ", and paymentEntity: " + paymentEntity + ".");
        return null;
    }

    @Override
    public CloseProviderProductResponse closeProviderProduct(String productId, String providerProductId, CloseProductProviderRequest closeProductProviderRequest) throws ProductException {
        return getProductService().closeProviderProduct(productId, providerProductId, closeProductProviderRequest);
    }

    @Override
    public void rollbackTransactionProduct(String productId, String transactionProductId) throws ProductException {
        getProductService().rollbackTransactionProduct(productId, transactionProductId);
    }

    @Override
    public void cancelTransactionProduct(String productId, String transactionProductId) throws ProductException {
        getProductService().cancelTransactionProduct(productId, transactionProductId);
    }

    @Override
    public void commitTransactionProduct(String productId, String transactionProductId) throws ProductException {
        getProductService().commitTransactionProduct(productId, transactionProductId);
    }

    @Override
    public void suspendTransactionProduct(String productId, String transactionProductId) throws ProductException {
        getProductService().suspendTransactionProduct(productId, transactionProductId);
    }

    @Override
    public void resumeTransactionProduct(String productId, String transactionProductId) throws ProductException {
        getProductService().resumeTransactionProduct(productId, transactionProductId);
    }

    @Override
    public void saveProduct(String productId, boolean isPartial) throws ProductException {
        getProductService().saveProduct(productId, null, isPartial);
    }

    private ProductServiceV2 getProductService() {
        return ConfigurationEngine.getInstance(ProductServiceV2New.class);
    }
}
