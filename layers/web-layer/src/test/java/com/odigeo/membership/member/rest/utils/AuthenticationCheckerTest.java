package com.odigeo.membership.member.rest.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

public class AuthenticationCheckerTest {
    private static final String CLIENT = "client";
    private static final String KEY = "key";
    private static final String TEST_PATH = "/membership/membership/method/123";
    private static final String HMAC256 = "HmacSHA256";
    private AuthenticationChecker authenticationChecker;
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    private static final ZoneId ZONE_MAD = ZoneId.of("Europe/Madrid");
    @Mock
    private Properties properties;

    @BeforeMethod
    public void setUp() throws NoSuchAlgorithmException {
        openMocks(this);
        when(properties.getProperty(eq(CLIENT))).thenReturn(KEY);
        authenticationChecker = new AuthenticationChecker(properties);
    }

    @Test
    public void testDefaultConstructor() {
        AuthenticationChecker ac = new AuthenticationChecker();
        assertNotEquals(ac, authenticationChecker);
    }

    @Test
    public void testCheck() throws InvalidKeyException, NoSuchAlgorithmException {
        assertFalse(authenticationChecker.check(CLIENT, "wrongHash", TEST_PATH));
        assertFalse(authenticationChecker.check(StringUtils.EMPTY, StringUtils.EMPTY, TEST_PATH));
        assertTrue(authenticationChecker.check(CLIENT, generateHmacTest(), TEST_PATH));
    }

    @Test
    public void testEmptyKey() {
        when(properties.getProperty(eq(CLIENT))).thenReturn(StringUtils.EMPTY);
        assertFalse(authenticationChecker.check(CLIENT, StringUtils.EMPTY, TEST_PATH));
    }

    private String generateHmacTest() throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance(HMAC256);
        mac.init(new SecretKeySpec(KEY.getBytes(StandardCharsets.UTF_8), HMAC256));
        String input = CLIENT + TEST_PATH + DATE_TIME_FORMATTER.format(ZonedDateTime.now(ZONE_MAD));
        return Hex.encodeHexString(mac.doFinal(input.getBytes(StandardCharsets.UTF_8)));
    }
}