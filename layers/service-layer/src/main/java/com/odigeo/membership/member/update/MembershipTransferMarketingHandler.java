package com.odigeo.membership.member.update;

import com.google.inject.Inject;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.Membership;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class MembershipTransferMarketingHandler {

    private final UserApiManager userApiManager;
    private final MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;
    private final MembershipMessageSendingManager membershipMessageSendingManager;

    @Inject
    public MembershipTransferMarketingHandler(UserApiManager userApiManager,
                                              MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper,
                                              MembershipMessageSendingManager membershipMessageSendingManager) {
        this.userApiManager = userApiManager;
        this.memberStatusToCrmStatusMapper = memberStatusToCrmStatusMapper;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
    }

    public void subscribeNewUserAndUnsubscribeOldUser(long oldUserId, long newUserId,
                                                      List<Membership> newUsersActiveAndPtcMemberships,
                                                      Set<String> oldUsersActiveWebsites) {
        UserInfo newUserInfo = null;
        UserInfo oldUserInfo = null;
        for (Membership activeOrPtcMembership : newUsersActiveAndPtcMemberships) {
            newUserInfo = Optional.ofNullable(newUserInfo).orElse(userApiManager.getUserInfo(newUserId));
            final SubscriptionStatus subscriptionStatus = memberStatusToCrmStatusMapper.map(activeOrPtcMembership.getStatus());
            membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(newUserInfo, activeOrPtcMembership, subscriptionStatus);

            final boolean oldUserShouldBeUnsubscribed = !oldUsersActiveWebsites.contains(activeOrPtcMembership.getWebsite());
            if (oldUserShouldBeUnsubscribed) {
                oldUserInfo = Optional.ofNullable(oldUserInfo).orElse(userApiManager.getUserInfo(oldUserId));
                membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(oldUserInfo, activeOrPtcMembership, SubscriptionStatus.UNSUBSCRIBED);
            }
        }
    }
}
