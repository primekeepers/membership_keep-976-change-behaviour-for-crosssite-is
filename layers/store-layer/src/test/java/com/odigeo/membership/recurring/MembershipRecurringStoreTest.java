package com.odigeo.membership.recurring;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.DataNotFoundException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class MembershipRecurringStoreTest {

    private static final Long MEMBERSHIP_ID = 1L;
    private static final UUID RECURRING_COLLECTION_ID = UUID.fromString("bd50c770-5a1b-46be-93c1-6a9b1e6d62c7");
    private static final String RECURRING_ID = "ABC";

    private MembershipRecurringStore store;

    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    @BeforeMethod
    public void before() throws Exception {
        openMocks(this);
        mockDatabase();
        ConfigurationEngine.init();
        store = ConfigurationEngine.getInstance(MembershipRecurringStore.class);
    }

    @Test
    public void testGetMembershipRecurringOK() throws Exception {
        resultSetWithOneResult();
        resultSetValue(RECURRING_ID);
        assertEquals(store.getMembershipRecurring(dataSource, MEMBERSHIP_ID),RECURRING_ID);
        verify(preparedStatement).setString(anyInt(), anyString());
        verify(resultSet).getString(anyString());
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMembershipRecurringKO() throws Exception {
        resultSetWithNoResults();
        store.getMembershipRecurring(dataSource, 1L);
    }

    @Test
    public void testInsertRecurring() throws Exception {
        when(preparedStatement.execute()).thenReturn(true);
        store.insertMembershipRecurring(dataSource, MEMBERSHIP_ID, RECURRING_ID);
        verify(preparedStatement).setNull(anyInt(), anyInt());
        verify(preparedStatement, times(2)).setString(anyInt(), anyString());
    }

    @Test
    public void testInsertRecurringCollectionId() throws Exception {
        when(preparedStatement.execute()).thenReturn(true);
        store.insertMembershipRecurringCollectionId(dataSource, MEMBERSHIP_ID, RECURRING_COLLECTION_ID);
        verify(preparedStatement).setString(eq(1), eq(MEMBERSHIP_ID.toString()));
        verify(preparedStatement).setString(eq(2), eq(RECURRING_COLLECTION_ID.toString()));
    }

    private void resultSetValue(String recurringId) throws SQLException {
        when(resultSet.getString(any())).thenReturn(recurringId);
    }

    private void resultSetWithOneResult() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
    }

    private void resultSetWithNoResults() throws SQLException {
        when(resultSet.next()).thenReturn(false);
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

}
