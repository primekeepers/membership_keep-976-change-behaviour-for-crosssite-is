package com.odigeo.membership.functionals.membership;

import java.math.BigDecimal;

public class StatusVerifierWrapper {

    private final String productStatus;
    private final String membershipStatus;
    private final BigDecimal balance;
    private final String activationDate;
    private final String expirationDate;

    public StatusVerifierWrapper(String productStatus, String membershipStatus, BigDecimal balance, String activationDate, String expirationDate) {
        this.productStatus = productStatus;
        this.membershipStatus = membershipStatus;
        this.balance = balance;
        this.activationDate = activationDate;
        this.expirationDate = expirationDate;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public String getMembershipStatus() {
        return membershipStatus;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }
}
