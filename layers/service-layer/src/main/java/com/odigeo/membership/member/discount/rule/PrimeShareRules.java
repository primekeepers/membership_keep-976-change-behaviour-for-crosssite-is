package com.odigeo.membership.member.discount.rule;

import com.google.inject.Singleton;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;

import java.util.Collection;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

@Singleton
public class PrimeShareRules extends ApplyDiscountRules {

    private final BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> predicate;

    public PrimeShareRules() {
        this.predicate = primeShareRule().and(primeShareActiveInterfaceRule()).and(primeShareActiveOriginRule());
    }

    @Override
    protected BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> getPredicate() {
        return predicate;
    }

    @Override
    protected String getRuleId() {
        return RULES_PRIME_SHARE;
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> primeShareRule() {
        return (parameters, context) -> {
            if (isABActive(context, TestTokenUtils.PRIME_SHARE_FOR_ALL)) {
                return true;
            }
            if (isABActive(context, TestTokenUtils.PRIME_SHARE_ONLY_PLUS)) {
                Collection<Membership> memberships = findMembershipsEligible(context, MembershipType.PLUS).collect(Collectors.toList());
                if (!memberships.isEmpty()) {
                    updateMembershipsEligible(context, memberships);
                    return true;
                }
                addError(context, NotApplicationReason.MEMBERSHIP_INVALID);
                return false;
            }
            return false;
        };
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> primeShareActiveOriginRule() {
        return (parameters, context) -> {
            if (parameters.isMetasearch() && isABActive(context, TestTokenUtils.PRIME_SHARE_META_BLOCKED)) {
                addError(context, NotApplicationReason.ORIGIN_INVALID);
                return false;
            }
            return true;
        };
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> primeShareActiveInterfaceRule() {
        return (parameters, context) -> {
            if (!isNativeInterface(parameters) || isABActive(context, TestTokenUtils.PRIME_SHARE_NATIVE_FOR_ALL)) {
                return true;
            }

            Collection<Membership> memberships = findMembershipsEligible(context, MembershipType.PLUS).collect(Collectors.toList());
            if (!memberships.isEmpty() && isABActive(context, TestTokenUtils.PRIME_SHARE_NATIVE_ONLY_PLUS)) {
                updateMembershipsEligible(context, memberships);
                return true;
            }

            addError(context, NotApplicationReason.INTERFACE_INVALID);
            return false;
        };
    }

    protected boolean isNativeInterface(ApplyDiscountParameters parameters) {
        return parameters.getClientInterface().isNative();
    }
}
