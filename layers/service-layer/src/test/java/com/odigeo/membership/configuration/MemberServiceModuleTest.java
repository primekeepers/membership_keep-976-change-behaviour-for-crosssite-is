package com.odigeo.membership.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.payments.recurringcollection.contract.RecurringCollectionResource;
import com.google.inject.util.Providers;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.member.nosql.MembershipNoSQLRepository;
import com.odigeo.userprofiles.api.v2.UserApiServiceInternal;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class MemberServiceModuleTest extends AbstractContextFactory {

    @Test
    public void testServiceLocatorBinding() {
        ConfigurationEngine.init(new MemberServiceModule(),
                binder -> binder.bind(MembershipNoSQLManager.class).toProvider(Providers.of(null)),
                binder -> binder.bind(MembershipNoSQLRepository.class).toProvider(Providers.of(null)),
                binder -> binder.bind(UserApiServiceInternal.class).toProvider(Providers.of(null)),
                binder -> binder.bind(RecurringCollectionResource.class).toProvider(Providers.of(null))
        );
        ServiceLocator serviceLocator = ConfigurationEngine.getInstance(ServiceLocator.class);
        assertNotNull(serviceLocator);
    }
}