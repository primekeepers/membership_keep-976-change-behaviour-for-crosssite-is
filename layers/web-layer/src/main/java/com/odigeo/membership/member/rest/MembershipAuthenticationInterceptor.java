package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.annotations.Authenticated;
import com.odigeo.membership.exception.MembershipUnauthorizedException;
import com.odigeo.membership.member.rest.utils.AuthenticationChecker;
import org.jboss.resteasy.core.interception.PostMatchContainerRequestContext;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.BooleanUtils.isFalse;

@Provider
public class MembershipAuthenticationInterceptor implements ContainerRequestFilter {

    private static final String CLIENT_HEADER = "client";
    private static final String HASH_HEADER = "hash";
    private static final String MODULE_NAME_PREFIX = "/membership";

    private String getHeader(ContainerRequestContext httpRequest, String headerName) {
        if (nonNull(httpRequest.getHeaders()) && !httpRequest.getHeaders().isEmpty()) {
            String requestHeader = httpRequest.getHeaders().getFirst(headerName);
            if (nonNull(requestHeader)) {
                return requestHeader;
            }
        }
        throw new MembershipUnauthorizedException("Authentication header " + headerName + " not found");
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        PostMatchContainerRequestContext pmContext = (PostMatchContainerRequestContext) containerRequestContext;
        Method method = pmContext.getResourceMethod().getMethod();

        if (method.isAnnotationPresent(Authenticated.class)) {
            String client = getHeader(containerRequestContext, CLIENT_HEADER);
            String hashedMessage = getHeader(containerRequestContext, HASH_HEADER);
            AuthenticationChecker authenticationChecker = ConfigurationEngine.getInstance(AuthenticationChecker.class);
            if (isFalse(authenticationChecker.check(client, hashedMessage, MODULE_NAME_PREFIX + containerRequestContext.getUriInfo().getPath()))) {
                throw new MembershipUnauthorizedException("Client " + client + " not authorized");
            }
        }
    }
}
