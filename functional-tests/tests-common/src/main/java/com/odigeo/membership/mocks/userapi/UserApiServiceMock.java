package com.odigeo.membership.mocks.userapi;

import com.odigeo.userprofiles.api.mocks.v2.OperationSaveUserBuilder;
import com.odigeo.userprofiles.api.mocks.v2.UserBuilder;
import com.odigeo.userprofiles.api.mocks.v2.responses.UserRegisteredResponseBuilder;
import com.odigeo.userprofiles.api.v2.UserApiService;
import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.requests.ChangeEmailRequest;
import com.odigeo.userprofiles.api.v2.model.requests.FlightStatusRequest;
import com.odigeo.userprofiles.api.v2.model.requests.ForgottenPasswordRequest;
import com.odigeo.userprofiles.api.v2.model.requests.PasswordChangeRequest;
import com.odigeo.userprofiles.api.v2.model.requests.ThirdAppRequest;
import com.odigeo.userprofiles.api.v2.model.requests.UserCreditCardRequest;
import com.odigeo.userprofiles.api.v2.model.requests.UserSaveRequest;
import com.odigeo.userprofiles.api.v2.model.requests.UserSearchRequest;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.UserCredentials;
import com.odigeo.userprofiles.api.v2.model.responses.FlightStatusResponse;
import com.odigeo.userprofiles.api.v2.model.responses.UserRegisteredResponse;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.ExceptionCodeEnum;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.StatusUserException;

import java.util.Random;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class UserApiServiceMock implements UserApiService {

    private static final String MOCK_NOT_IMPLEMENTED = "Mock not implemented";
    private static final String REGISTERED_EMAIL_PREFIX = "registered";
    private static final String DELETED_EMAIL_PREFIX = "deleted";
    private final Random random = new Random();

    @Override
    public String ping() {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User getUser(UserCredentials credentials) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User saveUser(User request) throws RemoteException, InvalidValidationException, InvalidCredentialsException, StatusUserException {
        OperationSaveUserBuilder operationSaveUserBuilder = new OperationSaveUserBuilder(new UserBuilder(random));
        if (isNotBlank(request.getEmail()) && request.getEmail().startsWith(REGISTERED_EMAIL_PREFIX)) {
            operationSaveUserBuilder.setExceptionCodeEnum(new ExceptionCodeEnum(("STA_000")));
        } else {
            operationSaveUserBuilder.setExceptionCodeEnum(null);
            operationSaveUserBuilder.getUserBuilder().setEmail(request.getEmail());
        }
        return operationSaveUserBuilder.build();
    }

    @Override
    public User updateUser(UserSaveRequest request) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User setUserPassword(PasswordChangeRequest request) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User activateUser(UserCredentials credentials) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public boolean requestForgottenPassword(ForgottenPasswordRequest forgottenPasswordRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User changeUserEmail(ChangeEmailRequest changeEmailRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public boolean deleteUser(UserCredentials deleteUserRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User manageCreditCard(UserCreditCardRequest userCreditCardRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User manageSearch(UserSearchRequest userSearchRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FlightStatusResponse putFlightStatusId(FlightStatusRequest flightStatusRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FlightStatusResponse getFlightStatusId(FlightStatusRequest flightStatusRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public UserRegisteredResponse isUserRegistered(ThirdAppRequest thirdAppRequest) {
        UserRegisteredResponseBuilder userRegisteredResponseBuilder = new UserRegisteredResponseBuilder(random);
        if (isNotBlank(thirdAppRequest.getEmail()) && thirdAppRequest.getEmail().startsWith(REGISTERED_EMAIL_PREFIX)) {
            userRegisteredResponseBuilder.setRegistered(true);
            userRegisteredResponseBuilder.setUserStatus(Status.ACTIVE);
            userRegisteredResponseBuilder.setId(257L);
        } else if (isNotBlank(thirdAppRequest.getEmail()) && thirdAppRequest.getEmail().startsWith(DELETED_EMAIL_PREFIX)) {
            userRegisteredResponseBuilder.setUserStatus(Status.DELETED);
            userRegisteredResponseBuilder.setId(4011L);
        } else {
            userRegisteredResponseBuilder.setRegistered(false);
        }
        return userRegisteredResponseBuilder.build();
    }
}
