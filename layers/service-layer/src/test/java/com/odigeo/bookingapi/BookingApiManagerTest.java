package com.odigeo.bookingapi;

import com.edreams.base.DataAccessException;
import com.odigeo.bookingapi.v14.BadCredentialsException;
import com.odigeo.bookingapi.v14.BookingApiService;
import com.odigeo.bookingapi.v14.InvalidParametersException;
import com.odigeo.bookingapi.v14.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingsearchapi.BookingSearchApiConfiguration;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.mock.v1.response.BuilderException;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.membership.booking.SearchBookingsRequestBuilder;
import com.odigeo.membership.commons.test.MockServer;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class BookingApiManagerTest {

    private static final String ERROR_MESSAGE = "ERROR_MESSAGE";
    private static final long BOOKING_ID = 123;
    private static final String USER_ID = "USER_ID";
    private static final String PASSWORD = "PASSWORD";
    private static final String ERROR_MESSAGE_NOT_NULL = "BookingApiManager::read bookingId " + BOOKING_ID + " error message " + ERROR_MESSAGE;
    private static final long TEST_MEMBERSHIP_ID = 1L;
    private static final InvalidParametersException INVALID_PARAMETERS_EXCEPTION = new InvalidParametersException(StringUtils.EMPTY);
    private static final BadCredentialsException BAD_CREDENTIALS_EXCEPTION = new BadCredentialsException("BAD CREDENTIALS");

    @Mock
    private BookingSearchApiService bookingSearchApiService;
    @Mock
    private BookingApiService bookingApiService;
    @Mock
    private BookingApiConfiguration bookingApiConfiguration;
    @Mock
    private BookingSearchApiConfiguration bookingSearchApiConfiguration;
    @Mock
    private Appender mockAppender;
    @Mock
    private BookingDetail bookingDetail;
    @Mock
    private UpdateBookingRequest updateBookingRequest;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    private BookingApiManager bookingApiManagerMockServer;
    private BookingApiManager bookingApiManager;
    private SearchBookingsRequest searchBookingsRequest;

    @BeforeMethod
    public void setup() throws IOException {
        openMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        setUpMockServer();
        searchBookingsRequest = new SearchBookingsRequestBuilder().setMembershipId(TEST_MEMBERSHIP_ID).build();
        bookingApiManager = new BookingApiManager(bookingApiService, bookingApiConfiguration, bookingSearchApiService, bookingSearchApiConfiguration);
    }

    @AfterMethod
    public void tearDownLogger() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    private void setUpMockServer() throws IOException {
        MockServer mockServer = new MockServer(200, "{\"bookingDetail\":{\"bookingBasicInfo\":{\"id\":3412293570,\"website\":{\"code\":\"OPUK\",\"brand\":\"OP\"},\"clientBookingReferenceId\":\"3412293570\",\"marketingPortalCode\":\"S\",\"partner\":null,\"creationDate\":1520601972000,\"requestDate\":1520601972000,\"lastUpdate\":1520602877000,\"agent\":{\"firstName\":\"Website Opodo UK\",\"lastName\":null},\"invoicingLegalEntity\":{\"id\":1,\"code\":\"OLTD\",\"name\":\"Opodo Limited\",\"countryCode\":\"GB\"},\"userCollectionRate\":1.1115882,\"collectionCurrency\":\"GBP\",\"collectionToWebsiteCurrencyConversionRate\":1,\"siteVersion\":\"1.267.1\",\"dapiClientVersion\":\"OneFront-1.242.0\",\"serverIp\":\"10.2.34.140\",\"partnerWebsiteId\":null,\"userSessionId\":\"6979309B2EC59017E4BD21FDB02C417B\",\"searchId\":45847398499,\"visitId\":34231973420,\"metaSelectedItinerary\":false,\"ipInfo\":{\"countryConfidenceFactor\":99,\"userRegion\":\"BELFAST\"},\"marketingTrackingInfo\":{\"marketingChannelId\":10,\"utmCampaign\":\"s\",\"utmContent\":null,\"utmMedium\":\"crm\",\"utmSource\":\"sf\",\"utmTerm\":\"20180309_w10_OOUK_TopMarch_193713\",\"traUtmSource\":\"sf\",\"traUtmCampaign\":\"s\",\"traUtmMedium\":\"crm\",\"traUtmContent\":null,\"traUtmTerm\":\"20180309_w10_OOUK_TopMarch_193713\",\"searchMarketingChannelId\":10},\"dimensionPartitionMap\":{\"821\":1,\"820\":1,\"823\":1,\"822\":1,\"817\":3,\"816\":2,\"819\":2,\"818\":1,\"829\":1,\"828\":1,\"831\":2,\"830\":1,\"825\":1,\"824\":2,\"827\":1,\"826\":1,\"804\":1,\"805\":1,\"806\":1,\"807\":1,\"801\":2,\"561\":1,\"802\":2,\"803\":2,\"812\":2,\"813\":1,\"814\":1,\"815\":2,\"808\":1,\"809\":1,\"810\":2,\"811\":1,\"42\":2,\"774\":4,\"775\":1,\"772\":1,\"773\":2,\"770\":1,\"771\":2,\"768\":1,\"769\":2,\"541\":2,\"780\":1,\"781\":2,\"778\":1,\"779\":2,\"776\":2,\"777\":2,\"881\":1,\"880\":2,\"883\":1,\"882\":1,\"885\":1,\"884\":1,\"887\":1,\"886\":3,\"889\":1,\"888\":1,\"864\":3,\"865\":1,\"866\":1,\"867\":1,\"868\":1,\"81\":1,\"869\":4,\"870\":4,\"871\":1,\"321\":3,\"872\":1,\"873\":2,\"874\":1,\"875\":1,\"876\":1,\"877\":1,\"878\":2,\"879\":1,\"851\":1,\"850\":1,\"849\":1,\"848\":1,\"855\":1,\"854\":1,\"581\":1,\"853\":2,\"852\":2,\"583\":2,\"859\":2,\"858\":2,\"857\":2,\"856\":1,\"863\":3,\"862\":1,\"861\":2,\"860\":2,\"834\":1,\"835\":2,\"832\":2,\"833\":4,\"842\":2,\"601\":1,\"843\":1,\"841\":1,\"846\":2,\"847\":1,\"844\":1,\"845\":2,\"681\":3,\"701\":2,\"441\":2,\"641\":1,\"181\":1,\"747\":1,\"746\":1,\"745\":1,\"744\":2,\"751\":2,\"201\":1,\"750\":2,\"749\":1,\"748\":1,\"739\":1,\"738\":1,\"737\":2,\"736\":1,\"743\":3,\"742\":1,\"741\":1,\"740\":1,\"762\":1,\"763\":2,\"761\":2,\"766\":1,\"767\":1,\"764\":2,\"765\":2,\"754\":2,\"752\":2,\"753\":1,\"501\":2,\"728\":1,\"729\":1,\"730\":1,\"731\":1,\"732\":1,\"733\":1,\"734\":1,\"735\":2,\"721\":1,\"722\":1,\"723\":2,\"724\":1,\"725\":1,\"726\":3,\"727\":3},\"marketingPortalId\":\"25229\"},\"testBooking\":false,\"middleOffice\":\"AGM\",\"userIp\":\"90.217.123.182\",\"userSessionLocale\":\"en_GB\",\"bookingUserComment\":\"eligible for free cancellation including SH fees as a void within next 10 hours\",\"check1\":false,\"check2\":false,\"price\":{\"amount\":90.30,\"currency\":\"GBP\"},\"buyer\":{\"phoneNumber\":{\"countryCode\":\"GB\",\"number\":\"07868736247\"},\"alternativePhoneNumber\":null,\"mobilePhone\":null,\"name\":\"HARSHVARDHAN\",\"lastNames\":\"KHARE\",\"mail\":\"kharekelkar@hotmail.com\",\"buyerIdentificationType\":null,\"identification\":null,\"dateOfBirth\":null,\"cpf\":null,\"address\":\"52 Linden Avenue\",\"cityName\":\"Darlington\",\"stateName\":null,\"zipCode\":\"DL3 8PP\",\"country\":\"GB\",\"neighborhood\":null,\"idUser\":null},\"travellers\":[{\"travellerType\":\"ADULT\",\"title\":\"MR\",\"name\":\"HARSHVARDHAN\",\"firstLastName\":\"KHARE\",\"secondLastName\":null,\"travellerGender\":null,\"providerPassengerName\":\"KHARE/HARSHVARDHAN(ADT)\",\"dateOfBirth\":null,\"nationalityCountryCode\":null,\"countryCodeOfResidence\":null,\"identification\":null,\"identificationType\":null,\"identificationExpirationDate\":null,\"identificationIssueCountryCode\":null,\"localityCodeOfResidence\":null,\"baggageSelections\":[{\"segmentIndex\":0,\"pieces\":0,\"kilos\":0}],\"middleName\":\"\",\"meal\":\"STANDARD\",\"numPassenger\":1,\"frequentFlyerCards\":[],\"mobileTraveller\":null}],\"bookingProducts\":[{\"typeInfo\":\"itineraryBooking\",\"id\":3412293603,\"productType\":\"ITINERARY\",\"feeContainerId\":110942539,\"buyPath\":81,\"isMainSearch\":true,\"isMainSearchProduct\":\"T\",\"providerBookings\":[{\"id\":3412293572,\"providerPaymentCurrency\":\"GBP\",\"providerPrice\":85.30,\"bookingStatus\":\"CONTRACT\",\"providerMerchantAmount\":{\"amount\":0.00,\"currency\":\"GBP\"},\"providerPaymentDetails\":[{\"typeInfo\":\"providerPaymentDetail\",\"id\":82241675,\"seqNumber\":1,\"providerPaymentMethodType\":\"CREDITCARD\",\"amount\":85.30,\"currency\":\"GBP\",\"providerToCollectionExchangeRate\":1,\"merchant\":false,\"corporateCreditCardPayment\":false,\"paymentEntity\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"transactionDate\":1520601982000,\"paymentDetailType\":\"PAYMENT\",\"paymentDetailRequester\":\"BOOKING_ENGINE\"}],\"sourcingLegalEntity\":{\"id\":1,\"code\":\"OLTD\",\"name\":\"Opodo Limited\",\"countryCode\":\"GB\"},\"travellerNumbers\":[1],\"idProvider\":\"1A\",\"issuanceDate\":1520602857000,\"additionalParameters\":[],\"providerBookingItinerary\":[{\"ticketNumbers\":[{\"numPassenger\":1,\"number\":\"5328495982\",\"price\":85.3,\"status\":\"CONTRACT\",\"currency\":\"GBP\",\"airlineCode\":\"117\",\"type\":\"FLIGHT\"}],\"providerItineraryId\":1519827800,\"carrierPNR\":null,\"baggageInformation\":{\"includedBaggage\":[{\"travellerType\":\"ADULT\",\"includedBaggage\":[{\"segmentIndex\":0,\"sectionIndex\":0,\"numPieces\":0,\"numKilos\":-1}]}],\"selectedBaggage\":[]}}],\"numAdults\":1,\"numChildren\":0,\"numInfants\":0,\"resident\":false,\"pnr\":\"VDKPMF\",\"voidActionType\":null,\"segments\":[0],\"itineraryProviders\":[0],\"officeId\":\"LONOP38AW\",\"issuanceOfficeId\":\"LONOP3100\",\"residentValidations\":{},\"baggageProviderPrice\":0.00,\"validatingCarrier\":0,\"creationDate\":1520550000000,\"requestDate\":1520601972000,\"minimumCommission\":2.96,\"providerId\":null,\"providerAncillaryServiceSelectionItemList\":[]}],\"legend\":{\"carriers\":[{\"id\":0,\"code\":\"SK\",\"name\":\"Scandinavian Airlines\"}],\"locations\":[{\"geoNodeId\":414,\"cityName\":\"Copenhagen\",\"name\":\"Kastrup\",\"type\":\"Airport\",\"iataCode\":\"CPH\",\"cityIataCode\":\"CPH\",\"countryCode\":\"DK\",\"countryName\":\"Denmark\",\"timeZone\":\"Europe/Copenhagen\"},{\"geoNodeId\":1153,\"cityName\":\"Manchester\",\"name\":\"Manchester International\",\"type\":\"Airport\",\"iataCode\":\"MAN\",\"cityIataCode\":\"MAN\",\"countryCode\":\"GB\",\"countryName\":\"United Kingdom\",\"timeZone\":\"Europe/London\"}],\"sectionResults\":[{\"section\":{\"technicalStops\":[],\"id\":\"542\",\"from\":1153,\"departureDate\":1522864500000,\"departureTerminal\":null,\"to\":414,\"arrivalDate\":1522871100000,\"arrivalTerminal\":null,\"carrier\":0,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"cabinClassCode\":\"M\",\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"aircraft\":\"320\",\"baggageAllowanceType\":null,\"baggageAllowanceQuantity\":null,\"duration\":110,\"productType\":\"FLIGHT\"},\"id\":0}],\"segmentResults\":[{\"segment\":{\"sections\":[0],\"carrier\":0,\"duration\":110,\"seats\":null,\"timesUndefined\":false},\"id\":0}],\"fareInfoPaxResults\":[],\"itineraries\":{\"crossFarings\":[],\"hubs\":[],\"smartHubs\":[],\"owasrts\":[],\"hidings\":[],\"simples\":[{\"id\":0,\"provider\":\"1A\",\"sections\":[0],\"providerPrice\":85.30,\"adultFares\":{\"fare\":42.00,\"tax\":43.30},\"childFares\":null,\"infantFares\":null,\"price\":85.30,\"providerCurrency\":\"GBP\",\"flightType\":\"REG\",\"providerItineraryId\":1519827800,\"lightTicketing\":false}],\"splitItineraries\":[]}},\"bookingItinerary\":{\"type\":\"SIMPLE\",\"tripType\":\"ONE_WAY\",\"arrival\":{\"geoNodeId\":414,\"cityName\":\"Copenhagen\",\"name\":\"Kastrup\",\"type\":\"Airport\",\"iataCode\":\"CPH\",\"cityIataCode\":\"CPH\",\"countryCode\":\"DK\",\"countryName\":\"Denmark\",\"timeZone\":\"Europe/Copenhagen\"},\"departure\":{\"geoNodeId\":1153,\"cityName\":\"Manchester\",\"name\":\"Manchester International\",\"type\":\"Airport\",\"iataCode\":\"MAN\",\"cityIataCode\":\"MAN\",\"countryCode\":\"GB\",\"countryName\":\"United Kingdom\",\"timeZone\":\"Europe/London\"},\"arrivalDate\":1522871100000,\"departureDate\":1522864500000},\"seatPreferencesSelectionSet\":null,\"freeCancellationLimitTime\":1520640000000,\"searchType\":\"M\",\"mainSearch\":1,\"displayMinPrice\":100.37,\"idDisplayPos\":0,\"searchStrategyId\":1,\"searchPatternSuiteId\":12294,\"bookingItineraryId\":204931146}],\"collectionSummary\":{\"attempts\":[{\"collectionMethod\":{\"creditCardCollectionMethod\":{\"id\":2,\"collectionMethodType\":\"CREDITCARD\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"validMovements\":[{\"orderId\":\"3412293570\",\"code\":\"0\",\"errorType\":\"UNKNOWN\",\"errorDescription\":null,\"action\":\"AUTHORIZE\",\"status\":\"AUTHORIZED\",\"amount\":5,\"creationDate\":\"2018-03-09 14:26:19\",\"commerceId\":\"OAUK2\",\"terminalId\":null,\"transactionId\":\"3964502912\",\"transactionSubId\":null,\"transactionCost\":null,\"commerceStatus\":\"5\",\"commerceStatusSubcode\":null,\"accountHolder\":null,\"accountNumber\":null,\"bankName\":null,\"bankCity\":null,\"bankCountry\":null,\"paymentReference\":null,\"swiftCode\":null,\"iban\":null,\"commerceType\":null,\"sequenceNumber\":null,\"currency\":\"GBP\",\"collectionToBillingConversionRate\":null,\"collectionOptionAccount\":null,\"invalidatedBySequenceNumber\":null},{\"orderId\":\"3412293570\",\"code\":\"0\",\"errorType\":\"UNKNOWN\",\"errorDescription\":null,\"action\":\"CONFIRM\",\"status\":\"PAID\",\"amount\":5,\"creationDate\":\"2018-03-09 14:32:57\",\"commerceId\":\"OAUK2\",\"terminalId\":null,\"transactionId\":\"3964502912\",\"transactionSubId\":\"1\",\"transactionCost\":0.08,\"commerceStatus\":\"91\",\"commerceStatusSubcode\":null,\"accountHolder\":null,\"accountNumber\":null,\"bankName\":null,\"bankCity\":null,\"bankCountry\":null,\"paymentReference\":null,\"swiftCode\":null,\"iban\":null,\"commerceType\":null,\"sequenceNumber\":1,\"currency\":\"GBP\",\"collectionToBillingConversionRate\":1,\"collectionOptionAccount\":\"OPUKWEB\",\"invalidatedBySequenceNumber\":null}],\"invalidMovements\":[],\"creditCard\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"elvAccount\":null,\"cofinogaCard\":null,\"amount\":5,\"type\":\"CREDITCARD\",\"gatewayCollectionMethodType\":\"CREDITCARD\",\"collectionEntity\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"id\":323929148,\"collectionOptionType\":\"COMMERCE\",\"collectionLegalEntity\":{\"id\":1,\"code\":\"OLTD\",\"name\":\"Opodo Limited\",\"countryCode\":\"GB\"},\"billingCurrency\":\"GBP\",\"transactionId\":\"3412293570\",\"commerceType\":\"OGONE\"}],\"creationDate\":\"2018-03-09 14:26:19\",\"currency\":\"GBP\",\"collectedAmount\":5.00,\"lastCreditCard\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}}},\"deposit\":null,\"fraudBookingResultContainer\":{\"fraudTrackingItems\":[{\"id\":331031551,\"bookingId\":3412293570,\"data\":\"ACTIVE\",\"type\":\"MANUALFRAUD_DECISION\",\"active\":true},{\"id\":331031548,\"bookingId\":3412293570,\"data\":\"LOW_RISK_LEVEL\",\"type\":\"RISK_LEVEL\",\"active\":true},{\"id\":331031547,\"bookingId\":3412293570,\"data\":\"ACCEPT\",\"type\":\"FINAL_DECISION\",\"active\":true},{\"id\":331031552,\"bookingId\":3412293570,\"data\":\"SIFT_SCIENCE_SCORE: 1; SIFT_SCIENCE_LEVEL: ACCEPT\",\"type\":\"SIFT_SCIENCE_DECISION\",\"active\":true},{\"id\":331031549,\"bookingId\":3412293570,\"data\":\"ACCEPT active: true\",\"type\":\"CYBERSOURCE_DECISION\",\"active\":true},{\"id\":331031550,\"bookingId\":3412293570,\"data\":\"ULTRA_LOW score: -540 level: LOW\",\"type\":\"EDREAMS_DECISION\",\"active\":true}],\"fraudBookingDecision\":\"ACCEPT_LOW_RISK\",\"fraudBookingRiskLevel\":\"LOW_RISK_LEVEL\"},\"userAgentInfo\":{\"device\":\"SMARTPHONE\",\"os\":\"IOS\",\"osVersion\":\"10.3.3\",\"userAgentVersion\":\"10.0\",\"idUserAgent\":11492045,\"userAgentFamily\":\"SAFARI\"},\"bookingStatus\":\"CONTRACT\",\"totalFee\":5.00,\"marketingRevenue\":8.84,\"allFeeContainers\":[{\"id\":110942538,\"feeContainerType\":\"BOOKING\",\"totalAmount\":0.00,\"fees\":[{\"typeInfo\":\"fee\",\"id\":152253652,\"feeLabel\":\"COLLECTION_METHOD\",\"chargeable\":true,\"subCode\":null,\"amount\":0.00,\"currency\":\"GBP\",\"creationDate\":1520601982000,\"requester\":\"ONLINE\"}]},{\"id\":110942539,\"feeContainerType\":\"ITINERARY\",\"totalAmount\":5.00,\"fees\":[{\"typeInfo\":\"fee\",\"id\":152253607,\"feeLabel\":\"FIRST_DEFAULT\",\"chargeable\":true,\"subCode\":null,\"amount\":0.85,\"currency\":\"GBP\",\"creationDate\":1520601972000,\"requester\":\"ONLINE\"},{\"typeInfo\":\"fee\",\"id\":152253608,\"feeLabel\":\"FINAL_DEFAULT\",\"chargeable\":true,\"subCode\":null,\"amount\":4.15,\"currency\":\"GBP\",\"creationDate\":1520601972000,\"requester\":\"ONLINE\"}]}],\"collectionState\":\"COLLECTED\",\"buypath\":\"81\",\"bookingInterface\":\"ONE_FRONT_SMARTPHONE\",\"currencyCode\":\"GBP\",\"message\":\"\",\"invoicingSummary\":null,\"promotionalCodeContainer\":{\"promotionalCodes\":[]},\"transferState\":\"NEED_TRANSFER\",\"chargeableServiceBookings\":[],\"middleOfficeId\":null,\"allowsManualDeposit\":false,\"isWhiteLabel\":false,\"bookingEngineProcessFlow\":\"NORMAL\",\"fraudRiskScore\":-540,\"userId\":null,\"isManualFraud\":\"NORMAL\",\"dateTravelStart\":1522864500000,\"membershipPerks\":null},\"messages\":null}");
        int port = mockServer.connectServerToPort();

        MockServer mockSearchBookingServer = new MockServer(200, "{\"bookingDetail\":{\"bookingBasicInfo\":{\"id\":3412293570,\"website\":{\"code\":\"OPUK\",\"brand\":\"OP\"},\"clientBookingReferenceId\":\"3412293570\",\"marketingPortalCode\":\"S\",\"partner\":null,\"creationDate\":1520601972000,\"requestDate\":1520601972000,\"lastUpdate\":1520602877000,\"agent\":{\"firstName\":\"Website Opodo UK\",\"lastName\":null},\"invoicingLegalEntity\":{\"id\":1,\"code\":\"OLTD\",\"name\":\"Opodo Limited\",\"countryCode\":\"GB\"},\"userCollectionRate\":1.1115882,\"collectionCurrency\":\"GBP\",\"collectionToWebsiteCurrencyConversionRate\":1,\"siteVersion\":\"1.267.1\",\"dapiClientVersion\":\"OneFront-1.242.0\",\"serverIp\":\"10.2.34.140\",\"partnerWebsiteId\":null,\"userSessionId\":\"6979309B2EC59017E4BD21FDB02C417B\",\"searchId\":45847398499,\"visitId\":34231973420,\"metaSelectedItinerary\":false,\"ipInfo\":{\"countryConfidenceFactor\":99,\"userRegion\":\"BELFAST\"},\"marketingTrackingInfo\":{\"marketingChannelId\":10,\"utmCampaign\":\"s\",\"utmContent\":null,\"utmMedium\":\"crm\",\"utmSource\":\"sf\",\"utmTerm\":\"20180309_w10_OOUK_TopMarch_193713\",\"traUtmSource\":\"sf\",\"traUtmCampaign\":\"s\",\"traUtmMedium\":\"crm\",\"traUtmContent\":null,\"traUtmTerm\":\"20180309_w10_OOUK_TopMarch_193713\",\"searchMarketingChannelId\":10},\"dimensionPartitionMap\":{\"821\":1,\"820\":1,\"823\":1,\"822\":1,\"817\":3,\"816\":2,\"819\":2,\"818\":1,\"829\":1,\"828\":1,\"831\":2,\"830\":1,\"825\":1,\"824\":2,\"827\":1,\"826\":1,\"804\":1,\"805\":1,\"806\":1,\"807\":1,\"801\":2,\"561\":1,\"802\":2,\"803\":2,\"812\":2,\"813\":1,\"814\":1,\"815\":2,\"808\":1,\"809\":1,\"810\":2,\"811\":1,\"42\":2,\"774\":4,\"775\":1,\"772\":1,\"773\":2,\"770\":1,\"771\":2,\"768\":1,\"769\":2,\"541\":2,\"780\":1,\"781\":2,\"778\":1,\"779\":2,\"776\":2,\"777\":2,\"881\":1,\"880\":2,\"883\":1,\"882\":1,\"885\":1,\"884\":1,\"887\":1,\"886\":3,\"889\":1,\"888\":1,\"864\":3,\"865\":1,\"866\":1,\"867\":1,\"868\":1,\"81\":1,\"869\":4,\"870\":4,\"871\":1,\"321\":3,\"872\":1,\"873\":2,\"874\":1,\"875\":1,\"876\":1,\"877\":1,\"878\":2,\"879\":1,\"851\":1,\"850\":1,\"849\":1,\"848\":1,\"855\":1,\"854\":1,\"581\":1,\"853\":2,\"852\":2,\"583\":2,\"859\":2,\"858\":2,\"857\":2,\"856\":1,\"863\":3,\"862\":1,\"861\":2,\"860\":2,\"834\":1,\"835\":2,\"832\":2,\"833\":4,\"842\":2,\"601\":1,\"843\":1,\"841\":1,\"846\":2,\"847\":1,\"844\":1,\"845\":2,\"681\":3,\"701\":2,\"441\":2,\"641\":1,\"181\":1,\"747\":1,\"746\":1,\"745\":1,\"744\":2,\"751\":2,\"201\":1,\"750\":2,\"749\":1,\"748\":1,\"739\":1,\"738\":1,\"737\":2,\"736\":1,\"743\":3,\"742\":1,\"741\":1,\"740\":1,\"762\":1,\"763\":2,\"761\":2,\"766\":1,\"767\":1,\"764\":2,\"765\":2,\"754\":2,\"752\":2,\"753\":1,\"501\":2,\"728\":1,\"729\":1,\"730\":1,\"731\":1,\"732\":1,\"733\":1,\"734\":1,\"735\":2,\"721\":1,\"722\":1,\"723\":2,\"724\":1,\"725\":1,\"726\":3,\"727\":3},\"marketingPortalId\":\"25229\"},\"testBooking\":false,\"middleOffice\":\"AGM\",\"userIp\":\"90.217.123.182\",\"userSessionLocale\":\"en_GB\",\"bookingUserComment\":\"eligible for free cancellation including SH fees as a void within next 10 hours\",\"check1\":false,\"check2\":false,\"price\":{\"amount\":90.30,\"currency\":\"GBP\"},\"buyer\":{\"phoneNumber\":{\"countryCode\":\"GB\",\"number\":\"07868736247\"},\"alternativePhoneNumber\":null,\"mobilePhone\":null,\"name\":\"HARSHVARDHAN\",\"lastNames\":\"KHARE\",\"mail\":\"kharekelkar@hotmail.com\",\"buyerIdentificationType\":null,\"identification\":null,\"dateOfBirth\":null,\"cpf\":null,\"address\":\"52 Linden Avenue\",\"cityName\":\"Darlington\",\"stateName\":null,\"zipCode\":\"DL3 8PP\",\"country\":\"GB\",\"neighborhood\":null,\"idUser\":null},\"travellers\":[{\"travellerType\":\"ADULT\",\"title\":\"MR\",\"name\":\"HARSHVARDHAN\",\"firstLastName\":\"KHARE\",\"secondLastName\":null,\"travellerGender\":null,\"providerPassengerName\":\"KHARE/HARSHVARDHAN(ADT)\",\"dateOfBirth\":null,\"nationalityCountryCode\":null,\"countryCodeOfResidence\":null,\"identification\":null,\"identificationType\":null,\"identificationExpirationDate\":null,\"identificationIssueCountryCode\":null,\"localityCodeOfResidence\":null,\"baggageSelections\":[{\"segmentIndex\":0,\"pieces\":0,\"kilos\":0}],\"middleName\":\"\",\"meal\":\"STANDARD\",\"numPassenger\":1,\"frequentFlyerCards\":[],\"mobileTraveller\":null}],\"bookingProducts\":[{\"typeInfo\":\"itineraryBooking\",\"id\":3412293603,\"productType\":\"ITINERARY\",\"feeContainerId\":110942539,\"buyPath\":81,\"isMainSearch\":true,\"isMainSearchProduct\":\"T\",\"providerBookings\":[{\"id\":3412293572,\"providerPaymentCurrency\":\"GBP\",\"providerPrice\":85.30,\"bookingStatus\":\"CONTRACT\",\"providerMerchantAmount\":{\"amount\":0.00,\"currency\":\"GBP\"},\"providerPaymentDetails\":[{\"typeInfo\":\"providerPaymentDetail\",\"id\":82241675,\"seqNumber\":1,\"providerPaymentMethodType\":\"CREDITCARD\",\"amount\":85.30,\"currency\":\"GBP\",\"providerToCollectionExchangeRate\":1,\"merchant\":false,\"corporateCreditCardPayment\":false,\"paymentEntity\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"transactionDate\":1520601982000,\"paymentDetailType\":\"PAYMENT\",\"paymentDetailRequester\":\"BOOKING_ENGINE\"}],\"sourcingLegalEntity\":{\"id\":1,\"code\":\"OLTD\",\"name\":\"Opodo Limited\",\"countryCode\":\"GB\"},\"travellerNumbers\":[1],\"idProvider\":\"1A\",\"issuanceDate\":1520602857000,\"additionalParameters\":[],\"providerBookingItinerary\":[{\"ticketNumbers\":[{\"numPassenger\":1,\"number\":\"5328495982\",\"price\":85.3,\"status\":\"CONTRACT\",\"currency\":\"GBP\",\"airlineCode\":\"117\",\"type\":\"FLIGHT\"}],\"providerItineraryId\":1519827800,\"carrierPNR\":null,\"baggageInformation\":{\"includedBaggage\":[{\"travellerType\":\"ADULT\",\"includedBaggage\":[{\"segmentIndex\":0,\"sectionIndex\":0,\"numPieces\":0,\"numKilos\":-1}]}],\"selectedBaggage\":[]}}],\"numAdults\":1,\"numChildren\":0,\"numInfants\":0,\"resident\":false,\"pnr\":\"VDKPMF\",\"voidActionType\":null,\"segments\":[0],\"itineraryProviders\":[0],\"officeId\":\"LONOP38AW\",\"issuanceOfficeId\":\"LONOP3100\",\"residentValidations\":{},\"baggageProviderPrice\":0.00,\"validatingCarrier\":0,\"creationDate\":1520550000000,\"requestDate\":1520601972000,\"minimumCommission\":2.96,\"providerId\":null,\"providerAncillaryServiceSelectionItemList\":[]}],\"legend\":{\"carriers\":[{\"id\":0,\"code\":\"SK\",\"name\":\"Scandinavian Airlines\"}],\"locations\":[{\"geoNodeId\":414,\"cityName\":\"Copenhagen\",\"name\":\"Kastrup\",\"type\":\"Airport\",\"iataCode\":\"CPH\",\"cityIataCode\":\"CPH\",\"countryCode\":\"DK\",\"countryName\":\"Denmark\",\"timeZone\":\"Europe/Copenhagen\"},{\"geoNodeId\":1153,\"cityName\":\"Manchester\",\"name\":\"Manchester International\",\"type\":\"Airport\",\"iataCode\":\"MAN\",\"cityIataCode\":\"MAN\",\"countryCode\":\"GB\",\"countryName\":\"United Kingdom\",\"timeZone\":\"Europe/London\"}],\"sectionResults\":[{\"section\":{\"technicalStops\":[],\"id\":\"542\",\"from\":1153,\"departureDate\":1522864500000,\"departureTerminal\":null,\"to\":414,\"arrivalDate\":1522871100000,\"arrivalTerminal\":null,\"carrier\":0,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"cabinClassCode\":\"M\",\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"aircraft\":\"320\",\"baggageAllowanceType\":null,\"baggageAllowanceQuantity\":null,\"duration\":110,\"productType\":\"FLIGHT\"},\"id\":0}],\"segmentResults\":[{\"segment\":{\"sections\":[0],\"carrier\":0,\"duration\":110,\"seats\":null,\"timesUndefined\":false},\"id\":0}],\"fareInfoPaxResults\":[],\"itineraries\":{\"crossFarings\":[],\"hubs\":[],\"smartHubs\":[],\"owasrts\":[],\"hidings\":[],\"simples\":[{\"id\":0,\"provider\":\"1A\",\"sections\":[0],\"providerPrice\":85.30,\"adultFares\":{\"fare\":42.00,\"tax\":43.30},\"childFares\":null,\"infantFares\":null,\"price\":85.30,\"providerCurrency\":\"GBP\",\"flightType\":\"REG\",\"providerItineraryId\":1519827800,\"lightTicketing\":false}],\"splitItineraries\":[]}},\"bookingItinerary\":{\"type\":\"SIMPLE\",\"tripType\":\"ONE_WAY\",\"arrival\":{\"geoNodeId\":414,\"cityName\":\"Copenhagen\",\"name\":\"Kastrup\",\"type\":\"Airport\",\"iataCode\":\"CPH\",\"cityIataCode\":\"CPH\",\"countryCode\":\"DK\",\"countryName\":\"Denmark\",\"timeZone\":\"Europe/Copenhagen\"},\"departure\":{\"geoNodeId\":1153,\"cityName\":\"Manchester\",\"name\":\"Manchester International\",\"type\":\"Airport\",\"iataCode\":\"MAN\",\"cityIataCode\":\"MAN\",\"countryCode\":\"GB\",\"countryName\":\"United Kingdom\",\"timeZone\":\"Europe/London\"},\"arrivalDate\":1522871100000,\"departureDate\":1522864500000},\"seatPreferencesSelectionSet\":null,\"freeCancellationLimitTime\":1520640000000,\"searchType\":\"M\",\"mainSearch\":1,\"displayMinPrice\":100.37,\"idDisplayPos\":0,\"searchStrategyId\":1,\"searchPatternSuiteId\":12294,\"bookingItineraryId\":204931146}],\"collectionSummary\":{\"attempts\":[{\"collectionMethod\":{\"creditCardCollectionMethod\":{\"id\":2,\"collectionMethodType\":\"CREDITCARD\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"validMovements\":[{\"orderId\":\"3412293570\",\"code\":\"0\",\"errorType\":\"UNKNOWN\",\"errorDescription\":null,\"action\":\"AUTHORIZE\",\"status\":\"AUTHORIZED\",\"amount\":5,\"creationDate\":\"2018-03-09 14:26:19\",\"commerceId\":\"OAUK2\",\"terminalId\":null,\"transactionId\":\"3964502912\",\"transactionSubId\":null,\"transactionCost\":null,\"commerceStatus\":\"5\",\"commerceStatusSubcode\":null,\"accountHolder\":null,\"accountNumber\":null,\"bankName\":null,\"bankCity\":null,\"bankCountry\":null,\"paymentReference\":null,\"swiftCode\":null,\"iban\":null,\"commerceType\":null,\"sequenceNumber\":null,\"currency\":\"GBP\",\"collectionToBillingConversionRate\":null,\"collectionOptionAccount\":null,\"invalidatedBySequenceNumber\":null},{\"orderId\":\"3412293570\",\"code\":\"0\",\"errorType\":\"UNKNOWN\",\"errorDescription\":null,\"action\":\"CONFIRM\",\"status\":\"PAID\",\"amount\":5,\"creationDate\":\"2018-03-09 14:32:57\",\"commerceId\":\"OAUK2\",\"terminalId\":null,\"transactionId\":\"3964502912\",\"transactionSubId\":\"1\",\"transactionCost\":0.08,\"commerceStatus\":\"91\",\"commerceStatusSubcode\":null,\"accountHolder\":null,\"accountNumber\":null,\"bankName\":null,\"bankCity\":null,\"bankCountry\":null,\"paymentReference\":null,\"swiftCode\":null,\"iban\":null,\"commerceType\":null,\"sequenceNumber\":1,\"currency\":\"GBP\",\"collectionToBillingConversionRate\":1,\"collectionOptionAccount\":\"OPUKWEB\",\"invalidatedBySequenceNumber\":null}],\"invalidMovements\":[],\"creditCard\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"elvAccount\":null,\"cofinogaCard\":null,\"amount\":5,\"type\":\"CREDITCARD\",\"gatewayCollectionMethodType\":\"CREDITCARD\",\"collectionEntity\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}},\"id\":323929148,\"collectionOptionType\":\"COMMERCE\",\"collectionLegalEntity\":{\"id\":1,\"code\":\"OLTD\",\"name\":\"Opodo Limited\",\"countryCode\":\"GB\"},\"billingCurrency\":\"GBP\",\"transactionId\":\"3412293570\",\"commerceType\":\"OGONE\"}],\"creationDate\":\"2018-03-09 14:26:19\",\"currency\":\"GBP\",\"collectedAmount\":5.00,\"lastCreditCard\":{\"creditCard\":{\"id\":100477177,\"creditCardNumber\":\"5451404000026614\",\"cvv\":null,\"expirationMonth\":\"11\",\"expirationYear\":\"18\",\"expirationDate\":\"1118\",\"owner\":\"Mohini kelkar\",\"creditCardType\":{\"code\":\"CA\",\"name\":\"Master Card Credit\"}}}},\"deposit\":null,\"fraudBookingResultContainer\":{\"fraudTrackingItems\":[{\"id\":331031551,\"bookingId\":3412293570,\"data\":\"ACTIVE\",\"type\":\"MANUALFRAUD_DECISION\",\"active\":true},{\"id\":331031548,\"bookingId\":3412293570,\"data\":\"LOW_RISK_LEVEL\",\"type\":\"RISK_LEVEL\",\"active\":true},{\"id\":331031547,\"bookingId\":3412293570,\"data\":\"ACCEPT\",\"type\":\"FINAL_DECISION\",\"active\":true},{\"id\":331031552,\"bookingId\":3412293570,\"data\":\"SIFT_SCIENCE_SCORE: 1; SIFT_SCIENCE_LEVEL: ACCEPT\",\"type\":\"SIFT_SCIENCE_DECISION\",\"active\":true},{\"id\":331031549,\"bookingId\":3412293570,\"data\":\"ACCEPT active: true\",\"type\":\"CYBERSOURCE_DECISION\",\"active\":true},{\"id\":331031550,\"bookingId\":3412293570,\"data\":\"ULTRA_LOW score: -540 level: LOW\",\"type\":\"EDREAMS_DECISION\",\"active\":true}],\"fraudBookingDecision\":\"ACCEPT_LOW_RISK\",\"fraudBookingRiskLevel\":\"LOW_RISK_LEVEL\"},\"userAgentInfo\":{\"device\":\"SMARTPHONE\",\"os\":\"IOS\",\"osVersion\":\"10.3.3\",\"userAgentVersion\":\"10.0\",\"idUserAgent\":11492045,\"userAgentFamily\":\"SAFARI\"},\"bookingStatus\":\"CONTRACT\",\"totalFee\":5.00,\"marketingRevenue\":8.84,\"allFeeContainers\":[{\"id\":110942538,\"feeContainerType\":\"BOOKING\",\"totalAmount\":0.00,\"fees\":[{\"typeInfo\":\"fee\",\"id\":152253652,\"feeLabel\":\"COLLECTION_METHOD\",\"chargeable\":true,\"subCode\":null,\"amount\":0.00,\"currency\":\"GBP\",\"creationDate\":1520601982000,\"requester\":\"ONLINE\"}]},{\"id\":110942539,\"feeContainerType\":\"ITINERARY\",\"totalAmount\":5.00,\"fees\":[{\"typeInfo\":\"fee\",\"id\":152253607,\"feeLabel\":\"FIRST_DEFAULT\",\"chargeable\":true,\"subCode\":null,\"amount\":0.85,\"currency\":\"GBP\",\"creationDate\":1520601972000,\"requester\":\"ONLINE\"},{\"typeInfo\":\"fee\",\"id\":152253608,\"feeLabel\":\"FINAL_DEFAULT\",\"chargeable\":true,\"subCode\":null,\"amount\":4.15,\"currency\":\"GBP\",\"creationDate\":1520601972000,\"requester\":\"ONLINE\"}]}],\"collectionState\":\"COLLECTED\",\"buypath\":\"81\",\"bookingInterface\":\"ONE_FRONT_SMARTPHONE\",\"currencyCode\":\"GBP\",\"message\":\"\",\"invoicingSummary\":null,\"promotionalCodeContainer\":{\"promotionalCodes\":[]},\"transferState\":\"NEED_TRANSFER\",\"chargeableServiceBookings\":[],\"middleOfficeId\":null,\"allowsManualDeposit\":false,\"isWhiteLabel\":false,\"bookingEngineProcessFlow\":\"NORMAL\",\"fraudRiskScore\":-540,\"userId\":null,\"isManualFraud\":\"NORMAL\",\"dateTravelStart\":1522864500000,\"membershipPerks\":null},\"messages\":null}");
        int mockSearchBookingPort = mockSearchBookingServer.connectServerToPort();

        ServiceBuilder<BookingApiService> serviceBuilder = new ServiceBuilder<>(BookingApiService.class, "http://localhost:" + port, new SimpleRestErrorsHandler(BookingApiService.class));
        BookingApiService bookingApiService = serviceBuilder.build();

        ServiceBuilder<BookingSearchApiService> bookingSearchApiServiceServiceBuilder = new ServiceBuilder<>(BookingSearchApiService.class, "http://localhost:" + mockSearchBookingPort, new SimpleRestErrorsHandler(BookingApiService.class));
        BookingSearchApiService bookingSearchApiService = bookingSearchApiServiceServiceBuilder.build();

        bookingApiManagerMockServer = new BookingApiManager(bookingApiService, new BookingApiConfiguration(), bookingSearchApiService, bookingSearchApiConfiguration);
    }

    @Test
    public void testGetBookingMockServer() throws Exception {
        BookingDetail bookingDetail = bookingApiManagerMockServer.getBooking(BOOKING_ID);
        assertNotNull(bookingDetail);
    }

    @Test
    public void testGetBookingMockServiceErrorMessageNull() throws Exception {
        setUpBookingApiResponse();
        BookingDetail bookingDetail = bookingApiManager.getBooking(BOOKING_ID);
        assertNotNull(bookingDetail);
    }

    @Test
    public void testGetBookingMockServiceErrorMessageNotNull() throws Exception {
        setUpBookingApiResponseWithErrorMessage();
        BookingDetail bookingDetail = bookingApiManager.getBooking(BOOKING_ID);
        assertNull(bookingDetail);
        verifyLogs(ERROR_MESSAGE_NOT_NULL);
    }

    @Test
    public void testSearchBookingIdsByMembership() throws BuilderException, DataAccessException {
        setUpBookingApiConfigurationUser();
        setUpBookingApiConfigurationPassword();
        when(bookingSearchApiService.searchBookings(anyString(), anyString(), any(), any(SearchBookingsRequest.class))).thenReturn(buildSearchBookingsByMembershipId());
        SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse = bookingApiManager.searchBookings(searchBookingsRequest);
        List<BookingSummary> bookingIdsByMembership = searchBookingsPageResponse.getBookings();
        assertEquals(bookingIdsByMembership.size(), 1);
        assertEquals(bookingIdsByMembership.get(0).getBookingBasicInfo().getId(), BOOKING_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSearchBookingIdsByMembershipInvalidParametersException() throws DataAccessException {
        when(bookingSearchApiService.searchBookings(eq(null), eq(null), any(), any(SearchBookingsRequest.class))).thenThrow(INVALID_PARAMETERS_EXCEPTION);
        bookingApiManager.searchBookings(searchBookingsRequest);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSearchBookingIdsByMembershipBadCredentialsException() throws DataAccessException {
        when(bookingSearchApiService.searchBookings(eq(null), eq(null), any(), any(SearchBookingsRequest.class))).thenThrow(BAD_CREDENTIALS_EXCEPTION);
        bookingApiManager.searchBookings(searchBookingsRequest);
    }

    @Test(expectedExceptions = BookingApiException.class)
    public void testGetBookingInvalidParametersException() throws Exception {
        when(bookingApiService.getBooking(eq(null), eq(null), any(), anyLong())).thenThrow(INVALID_PARAMETERS_EXCEPTION);
        bookingApiManager.getBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = BookingApiException.class)
    public void testGetBookingBadCredentialsException() throws Exception {
        when(bookingApiService.getBooking(eq(null), eq(null), any(Locale.class), anyLong())).thenThrow(BAD_CREDENTIALS_EXCEPTION);
        bookingApiManager.getBooking(BOOKING_ID);
    }

    private SearchBookingsPageResponse<List<BookingSummary>> buildSearchBookingsByMembershipId() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        BookingSummaryBuilder bookingSummaryBuilder = new BookingSummaryBuilder();
        BookingSummary bookingSummary = bookingSummaryBuilder.build(new Random());
        bookingSummary.getBookingBasicInfo().setId(BOOKING_ID);
        SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        searchBookingsPageResponse.setBookings(Collections.singletonList(bookingSummary));
        return searchBookingsPageResponse;
    }

    private void verifyLogs(String logMessage) {
        verify(mockAppender, atLeastOnce()).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
        assertTrue(loggingEvents.stream()
                .anyMatch(loggingEvent -> loggingEvent.getRenderedMessage().contains(logMessage)));
    }

    @Test(expectedExceptions = BookingApiException.class)
    public void testGetBookingTimeoutException() throws Exception {
        when(bookingApiService.getBooking(eq(null), eq(null), any(), anyLong())).thenThrow(new UndeclaredThrowableException(new Throwable()));
        bookingApiManager.getBooking(1);
    }

    private void setUpBookingApiConfigurationUser() {
        when(bookingApiConfiguration.getUser()).thenReturn(USER_ID);
        when(bookingSearchApiConfiguration.getUser()).thenReturn(USER_ID);
    }

    private void setUpBookingApiConfigurationPassword() {
        when(bookingApiConfiguration.getPassword()).thenReturn(PASSWORD);
        when(bookingSearchApiConfiguration.getPassword()).thenReturn(PASSWORD);
    }

    private void setUpBookingApiResponse() {
        when((bookingApiService.getBooking(eq(null), eq(null), any(), anyLong()))).thenReturn(bookingDetail);
    }

    private void setUpBookingApiResponseWithErrorMessage() {
        when(bookingDetail.getErrorMessage()).thenReturn(ERROR_MESSAGE);
        setUpBookingApiResponse();
    }
}
