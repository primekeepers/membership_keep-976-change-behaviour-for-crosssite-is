package com.odigeo.membership.member.memberapi.v1.util;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.response.MembershipAccountInfo;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.util.DateParser;
import org.apache.commons.lang.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.Date;

public final class MemberUserAreaServiceMapping {

    private static final long BOOKING_ID_ZERO = 0L;

    private MemberUserAreaServiceMapping() {
    }

    public static MembershipInfo asMembershipInfoResponse(MemberAccount memberAccount, Long bookingDetailId, Date lastStatusModificationDate) {
        MembershipInfo membershipInfo = ConfigurationEngine.getInstance(MemberServiceMapping.class).asMembershipInfoResponse(memberAccount);
        membershipInfo.setLastStatusModificationDate(DateParser.toIsoDate(lastStatusModificationDate).orElse(StringUtils.EMPTY));
        membershipInfo.setBookingIdSubscription(bookingDetailId == null ? BOOKING_ID_ZERO : bookingDetailId);
        return membershipInfo;
    }

    public static MembershipAccountInfo asMemberAccountInfoResponse(final MemberAccount account) {
        MembershipAccountInfo membershipAccountInfo = new MembershipAccountInfo();
        membershipAccountInfo.setId(account.getId());
        membershipAccountInfo.setUserId(account.getUserId());
        membershipAccountInfo.setFirstName(account.getName());
        membershipAccountInfo.setLastName(account.getLastNames());
        membershipAccountInfo.setTimestamp(account.getTimestamp().format(DateTimeFormatter.ISO_DATE_TIME));
        return membershipAccountInfo;
    }
}
