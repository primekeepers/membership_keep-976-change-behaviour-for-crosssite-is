package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;

import static com.odigeo.membership.UpdateMembershipAction.DEACTIVATE_MEMBERSHIP;

public class RemindMeLaterOperation implements MembershipUpdateOperation {

    private final MemberService memberService;
    private final DataSource dataSource;
    private final MembershipStore membershipStore;
    private final MembershipMessageSendingManager membershipMessageSendingManager;

    public interface Factory extends MembershipUpdateOperationFactory {
        @Override
        RemindMeLaterOperation createOperation(DataSource dataSource);
    }

    @Inject
    public RemindMeLaterOperation(@Assisted DataSource dataSource,
                                  MemberService memberService,
                                  MembershipStore membershipStore,
                                  MembershipMessageSendingManager membershipMessageSendingManager) {
        this.dataSource = dataSource;
        this.memberService = memberService;
        this.membershipStore = membershipStore;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
    }

    @Override
    public boolean update(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        return setRemindMeLater(updateMembership);
    }

    public boolean setRemindMeLater(UpdateMembership updateMembership) throws DataAccessRollbackException {
        try {
            Membership membership = memberService.getMembershipById(Long.parseLong(updateMembership.getMembershipId()));
            if (membershipActivatedOrJustDeactivated(updateMembership, membership)) {
                boolean storageSuccessful = membershipStore.setRemindMeLater(dataSource, Long.parseLong(updateMembership.getMembershipId()), Optional.ofNullable(updateMembership.getRemindMeLater()).orElse(Boolean.TRUE));
                Membership updatedMembership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
                boolean kafkaMessageSent = membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(updatedMembership, SubscriptionStatus.SUBSCRIBED);
                return storageSuccessful && kafkaMessageSent;
            }
        } catch (MissingElementException | SQLException | DataAccessException e) {
            throw new DataAccessRollbackException("Error setting remind me later flag for for membershipId " + updateMembership.getMembershipId(), e);
        }
        return false;
    }

    private boolean membershipActivatedOrJustDeactivated(UpdateMembership updateMembership, Membership membership) {
        boolean activated = membership != null && membership.getIsActive();
        boolean justDeactivated = membership != null && membership.getStatus() == MemberStatus.DEACTIVATED && DEACTIVATE_MEMBERSHIP.name().equals(updateMembership.getOperation());
        return activated || justDeactivated;
    }
}
