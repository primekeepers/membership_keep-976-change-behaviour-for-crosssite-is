package com.odigeo.membership.member.monitoring;

import com.codahale.metrics.health.HealthCheck;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.robots.activator.MembershipActivationProducerConfiguration;

import java.net.Socket;
import java.util.StringTokenizer;

@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public class KafkaProducerHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        String endpoints = ConfigurationEngine.getInstance(MembershipActivationProducerConfiguration.class).getBrokerList();
        StringTokenizer tokenizer = new StringTokenizer(endpoints, ",");
        while (tokenizer.hasMoreTokens()) {
            String hostPort = tokenizer.nextToken();
            try (Socket s = new Socket(hostPort.substring(0, hostPort.indexOf(':')), Integer.parseInt(hostPort.substring(hostPort.indexOf(':') + 1)))) {
                return Result.healthy("Kafka is up (for writing) at least in one node");
            }
        }
        return Result.unhealthy("Kafka is down (for writing)");
    }
}
