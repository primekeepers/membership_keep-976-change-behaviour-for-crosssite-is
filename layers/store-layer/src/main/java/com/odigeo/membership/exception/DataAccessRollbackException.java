package com.odigeo.membership.exception;

import com.edreams.base.DataAccessException;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class DataAccessRollbackException extends DataAccessException {

    public DataAccessRollbackException(final String message, final Throwable th) {
        super(message, th);
    }

    public DataAccessRollbackException(final Throwable th) {
        super(th);
    }
}
