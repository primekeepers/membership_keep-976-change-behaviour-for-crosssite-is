package com.odigeo.membership.mocks.kafka.processors;

import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.MessageProcessor;

import java.util.List;

public abstract class AbstractKafkaMessageProcessor<T extends Message> implements MessageProcessor<T> {

    public abstract void onMessage(T message);

    public abstract List<T> getReceivedMessages();

    public abstract void resetMessageList();
}
