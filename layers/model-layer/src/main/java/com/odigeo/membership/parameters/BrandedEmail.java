package com.odigeo.membership.parameters;


public class BrandedEmail {


    private String email;
    private String brand;

    public BrandedEmail() {
    }

    public BrandedEmail(String email, String brand) {
        this.email = email;
        this.brand = brand;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
