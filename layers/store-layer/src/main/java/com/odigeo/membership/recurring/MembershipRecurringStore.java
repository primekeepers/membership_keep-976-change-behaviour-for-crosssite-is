package com.odigeo.membership.recurring;

import com.odigeo.membership.exception.DataNotFoundException;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

public class MembershipRecurringStore {

    private static final Logger LOGGER = Logger.getLogger(MembershipRecurringStore.class);

    private static final String ADD_MEMBERSHIP_RECURRING_SQL = "INSERT INTO GE_MEMBERSHIP_RECURRING(ID, MEMBERSHIP_ID, RECURRING_ID) VALUES (?, ?, ?) ";
    private static final String ADD_MEMBERSHIP_RECURRING_COLLECTION_SQL = "INSERT INTO GE_MEMBERSHIP_RECURRING_COLLECTION(MEMBERSHIP_ID, RECURRING_COLLECTION_ID) VALUES (?, ?) ";
    private static final String GET_MEMBERSHIP_RECURRING_SQL = "SELECT RECURRING_ID FROM GE_MEMBERSHIP_RECURRING WHERE MEMBERSHIP_ID = ? ";

    public void insertMembershipRecurring(DataSource dataSource, long membershipId, String recurringId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_MEMBERSHIP_RECURRING_SQL)) {

            preparedStatement.setNull(1, Types.VARCHAR);
            preparedStatement.setString(2, String.valueOf(membershipId));
            preparedStatement.setString(3, recurringId);
            preparedStatement.execute();
        }
    }

    public void insertMembershipRecurringCollectionId(DataSource dataSource, long membershipId, UUID recurringCollectionId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_MEMBERSHIP_RECURRING_COLLECTION_SQL)) {

            preparedStatement.setString(1, String.valueOf(membershipId));
            preparedStatement.setString(2, recurringCollectionId.toString());
            preparedStatement.execute();
        }
    }

    public String getMembershipRecurring(DataSource dataSource, long membershipId) throws SQLException, DataNotFoundException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBERSHIP_RECURRING_SQL)) {
            preparedStatement.setString(1, String.valueOf(membershipId));
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return rs.getString("RECURRING_ID");
                } else {
                    LOGGER.warn("Missing recurring for membershipId: " + membershipId);
                    throw new DataNotFoundException("Missing recurring for membershipId:" + membershipId);
                }
            }
        }
    }
}
