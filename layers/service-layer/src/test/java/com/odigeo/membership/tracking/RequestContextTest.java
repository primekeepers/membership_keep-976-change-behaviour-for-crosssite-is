package com.odigeo.membership.tracking;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.*;

public class RequestContextTest {
    static final String MODULE_INFO_HEADER = "odigeo-module-info";
    static final String MODULE_INFO_HEADER_CONTENT = "module:1.0";
    RequestContext requestContext;

    @BeforeMethod
    public void setUp() throws SQLException {
        openMocks(this);
        requestContext = spy(new RequestContext());
    }

    @Test
    public void testGetEMptyHeader() {
        Optional<String> header = requestContext.getHeader(MODULE_INFO_HEADER);
        assertFalse(header.isPresent());
    }

    @Test
    public void testGetHeader() {
        doReturn(Optional.of(MODULE_INFO_HEADER_CONTENT)).when(requestContext).getHeader(anyString());
        Optional<String> header = requestContext.getHeader(MODULE_INFO_HEADER);
        assertTrue(header.isPresent());
    }
}