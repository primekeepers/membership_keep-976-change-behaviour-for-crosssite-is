package com.odigeo.membership.functionals.jboss;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.ServerConfiguration;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Optional;

@ScenarioScoped
public class JbossServerWorld {

    private static final Logger LOGGER = Logger.getLogger(JbossServerWorld.class);
    private static final String DEFAULT_CONTEXT_ROOT = "/membership";
    private static final String ENGINEERING_PATH = "/engineering/";

    private final String contextRoot;

    public JbossServerWorld() {
        contextRoot = Optional.ofNullable(System.getProperty("contextRoot")).orElse(DEFAULT_CONTEXT_ROOT);
    }

    public void installScenario() {
        refresh();
    }

    private void refresh() {
        LOGGER.info("Start refresh jboss");
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            final String refreshUrl = "http://" + ConfigurationEngine.getInstance(ServerConfiguration.class).getMembershipServer() + contextRoot + ENGINEERING_PATH + "refresh";
            httpClient.execute(new HttpGet(refreshUrl));
        } catch (IOException e) {
            LOGGER.error("Error refreshing jboss: ", e);
        }
        LOGGER.info("End refresh jboss");
    }
}
