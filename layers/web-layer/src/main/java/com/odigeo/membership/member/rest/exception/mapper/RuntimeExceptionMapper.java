package com.odigeo.membership.member.rest.exception.mapper;

import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.member.rest.utils.MembershipServiceExceptionBean;

import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RuntimeExceptionMapper extends BaseExceptionMapper<RuntimeException> implements ExceptionMapper<RuntimeException> {

    @Override
    protected MembershipServiceExceptionBean beanToSend(RuntimeException exception) {
        return new MembershipServiceExceptionBean(MembershipExceptionMapper.map(exception), true);
    }
}
