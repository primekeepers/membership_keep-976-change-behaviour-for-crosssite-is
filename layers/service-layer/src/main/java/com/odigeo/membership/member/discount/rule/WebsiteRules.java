package com.odigeo.membership.member.discount.rule;

import com.google.inject.Singleton;
import com.odigeo.membership.Membership;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.enums.Interface;
import com.odigeo.membership.member.discount.ApplyDiscountContext;
import com.odigeo.membership.parameters.NotApplicationReason;
import com.odigeo.util.TestTokenUtils;

import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Singleton
public class WebsiteRules extends ApplyDiscountRules {

    private final BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> predicate;

    public WebsiteRules() {
        this.predicate = isUserWebsiteMembershipMatch();
    }

    @Override
    protected BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> getPredicate() {
        return predicate;
    }

    @Override
    protected String getRuleId() {
        return RULES_WEBSITE;
    }

    private BiPredicate<ApplyDiscountParameters, ApplyDiscountContext> isUserWebsiteMembershipMatch() {
        return (parameters, context) -> {
            if (isCrossSiteEnabled(context, parameters)) {
                return true;
            }
            List<Membership> memberships = findMembershipsEligible(context)
                    .filter(websiteRule(parameters.getWebsite())).collect(Collectors.toList());
            if (memberships.isEmpty()) {
                addError(context, NotApplicationReason.WEBSITE_INVALID);
                return false;
            } else {
                updateMembershipsEligible(context, memberships);
                return true;
            }
        };
    }

    private Predicate<Membership> websiteRule(String website) {
        return membership -> Optional.of(membership).map(Membership::getWebsite).filter(website::equalsIgnoreCase).isPresent();
    }

    private Boolean isCrossSiteEnabled(ApplyDiscountContext context, ApplyDiscountParameters parameters) {
        boolean isCrossSiteDesktopABEnabled = Interface.ONE_FRONT_DESKTOP.equals(parameters.getClientInterface())
                && isABActive(context, TestTokenUtils.PRIME_CROSS_SITE_DESKTOP);
        boolean isCrossSiteMobileABEnabled = Interface.ONE_FRONT_SMARTPHONE.equals(parameters.getClientInterface())
                && isABActive(context, TestTokenUtils.PRIME_CROSS_SITE_MOBILE);

        return isCrossSiteDesktopABEnabled || isCrossSiteMobileABEnabled;
    }

}
