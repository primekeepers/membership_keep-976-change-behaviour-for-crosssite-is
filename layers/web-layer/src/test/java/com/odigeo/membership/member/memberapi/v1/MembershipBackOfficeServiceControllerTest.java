package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.BackOfficeService;
import com.odigeo.membership.request.backoffice.WelcomeEmailRequest;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doThrow;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipBackOfficeServiceControllerTest {

    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long BOOKING_ID = 456L;
    private static final String EMAIL = "test@edreams.com";
    private static final DataAccessException DATA_ACCESS_EXCEPTION = new DataAccessException(StringUtils.EMPTY);
    private static final MissingElementException MISSING_ELEMENT_EXCEPTION = new MissingElementException(StringUtils.EMPTY);
    private static final PublishMessageException PUBLISH_MESSAGE_EXCEPTION = new PublishMessageException("PublishMessageException", new RuntimeException());

    @Mock
    private BackOfficeService backOfficeService;

    private MembershipBackOfficeServiceController membershipBackOfficeServiceController;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipBackOfficeServiceController = new MembershipBackOfficeServiceController();
    }

    private void configure(Binder binder) {
        binder.bind(BackOfficeService.class).toInstance(backOfficeService);
    }

    @Test
    public void testUpdateMembershipMarketingInfoMessageSent() throws InvalidParametersException {
        assertTrue(membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL));
    }

    @Test
    public void testUpdateMembershipMarketingInfoMessageNotSent() throws InvalidParametersException, MissingElementException,
            DataAccessException, PublishMessageException {
        // Given
        doThrow(PUBLISH_MESSAGE_EXCEPTION).when(backOfficeService).updateMembershipMarketingInfo(MEMBERSHIP_ID);

        assertFalse(membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL));
    }

    @Test
    public void testUpdateMembershipMarketingInfoReturnsFalseWhenMissingElementException() throws MissingElementException,
            DataAccessException, InvalidParametersException, PublishMessageException {
        // Given
        doThrow(MISSING_ELEMENT_EXCEPTION).when(backOfficeService).updateMembershipMarketingInfo(MEMBERSHIP_ID);

        assertFalse(membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL));
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testUpdateMembershipMarketingInfoReturnsFalseWhenDataAccessException()
            throws MissingElementException, DataAccessException, InvalidParametersException, PublishMessageException {
        // Given
        doThrow(DATA_ACCESS_EXCEPTION).when(backOfficeService).updateMembershipMarketingInfo(MEMBERSHIP_ID);

        membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL);
    }

    @Test
    public void testSendWelcomeEmailMissingElementException() throws MissingElementException, DataAccessException {
        WelcomeEmailRequest welcomeEmailRequest = new WelcomeEmailRequest();
        welcomeEmailRequest.setBookingId(BOOKING_ID);
        when(backOfficeService.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID)).thenThrow(new MissingElementException(""));

        Boolean response = membershipBackOfficeServiceController.sendWelcomeEmail(MEMBERSHIP_ID, welcomeEmailRequest);

        assertFalse(response);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testSendWelcomeEmailDataAccessException() throws MissingElementException, DataAccessException {
        WelcomeEmailRequest welcomeEmailRequest = new WelcomeEmailRequest();
        welcomeEmailRequest.setBookingId(BOOKING_ID);
        when(backOfficeService.sendWelcomeEmail(MEMBERSHIP_ID, BOOKING_ID)).thenThrow(new DataAccessException(""));

        membershipBackOfficeServiceController.sendWelcomeEmail(MEMBERSHIP_ID, welcomeEmailRequest);

    }

}
