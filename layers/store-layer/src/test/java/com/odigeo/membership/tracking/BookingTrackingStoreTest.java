package com.odigeo.membership.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.BookingTracking;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingTrackingStoreTest {

    public static final BigDecimal COST_FEE_AMOUNT = BigDecimal.ONE;
    public static final BigDecimal PERKS_FEE_AMOUNT = BigDecimal.ZERO;
    private static final long BOOKING_ID = 123L;
    private static final long MEMBERSHIP_ID = 234L;
    private static final LocalDateTime BOOKING_DATE = LocalDateTime.of(2018, 1, 2, 0, 0);
    private static final BigDecimal AVOID_FEE_AMOUNT = BigDecimal.TEN;
    private BookingTrackingStore store;

    @Mock
    private BookingTracking bookingTracking;
    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    @BeforeMethod
    public void before() throws Exception {
        MockitoAnnotations.openMocks(this);
        mockDatabase();
        ConfigurationEngine.init();
        setupBookingTracking();
        store = ConfigurationEngine.getInstance(BookingTrackingStore.class);
    }

    private void setupBookingTracking() {
        when(bookingTracking.getBookingDate()).thenReturn(BOOKING_DATE);
        when(bookingTracking.getBookingId()).thenReturn(BOOKING_ID);
        when(bookingTracking.getMembershipId()).thenReturn(MEMBERSHIP_ID);
        when(bookingTracking.getAvoidFeeAmount()).thenReturn(AVOID_FEE_AMOUNT);
        when(bookingTracking.getCostFeeAmount()).thenReturn(COST_FEE_AMOUNT);
        when(bookingTracking.getPerksAmount()).thenReturn(PERKS_FEE_AMOUNT);
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    @Test
    public void testTrackBookingSuccess() throws SQLException {
        setRowsUpdated(1);
        assertTrue(store.trackBooking(dataSource, bookingTracking));
    }

    @Test
    public void testTrackBookingFail() throws SQLException {
        setRowsUpdated(0);
        assertFalse(store.trackBooking(dataSource, bookingTracking));
    }

    @Test(expectedExceptions = SQLException.class)
    public void testTrackBookingException() throws SQLException {
        throwPreparedStatementException();
        store.trackBooking(dataSource, bookingTracking);
    }

    @Test
    public void testRemoveTrackingSuccess() throws SQLException {
        setRowsUpdated(1);
        assertTrue(store.removeTrackedBooking(dataSource, BOOKING_ID));
    }

    @Test
    public void testRemoveTrackingFail() throws SQLException {
        setRowsUpdated(0);
        assertFalse(store.removeTrackedBooking(dataSource, BOOKING_ID));
    }

    @Test(expectedExceptions = SQLException.class)
    public void testRemoveTrackingException() throws SQLException {
        throwPreparedStatementException();
        store.removeTrackedBooking(dataSource, BOOKING_ID);
    }

    @Test
    public void testBookingTrackedFound() throws SQLException {
        setRowsFound(true, true);
        assertTrue(store.isBookingTracked(dataSource, BOOKING_ID));
    }

    @Test
    public void testBookingTrackedNotFound() throws SQLException {
        setRowsFound(false, false);
        assertFalse(store.isBookingTracked(dataSource, BOOKING_ID));
    }

    @Test(expectedExceptions = SQLException.class)
    public void testBookingTrackedException() throws SQLException {
        throwPreparedStatementException();
        store.isBookingTracked(dataSource, BOOKING_ID);
    }

    @Test
    public void testBookingLimitReached() throws SQLException {
        setRowsFound(true, true);
        setMembershipRules(5L, 15, 1);
        setTotalBookingsFound(5);
        assertTrue(store.isBookingLimitReached(dataSource, MEMBERSHIP_ID));
    }

    @Test
    public void testBookingLimitNotReached() throws SQLException {
        setRowsFound(true, true);
        setMembershipRules(5L, 15, 1);
        setTotalBookingsFound(4);
        assertFalse(store.isBookingLimitReached(dataSource, MEMBERSHIP_ID));
    }

    @Test
    public void testBookingLimitNotDataFound() throws SQLException {
        setRowsFound(false, true);
        assertFalse(store.isBookingLimitReached(dataSource, MEMBERSHIP_ID));
    }

    @Test
    public void testBookingLimitNoBookingsDataFound() throws SQLException {
        setRowsFound(true, false);
        setMembershipRules(5L, 15, 1);
        assertFalse(store.isBookingLimitReached(dataSource, MEMBERSHIP_ID));
    }

    @Test
    public void testBookingLimitNoRulesNoBookingDataFound() throws SQLException {
        setRowsFound(true, false);
        assertFalse(store.isBookingLimitReached(dataSource, MEMBERSHIP_ID));
    }

    @Test
    public void testBookingLimitRulesNotActive() throws SQLException {
        setRowsFound(true, true);
        setMembershipRules(5L, 15, 0);
        setTotalBookingsFound(5);
        assertFalse(store.isBookingLimitReached(dataSource, MEMBERSHIP_ID));
    }

    @Test(expectedExceptions = SQLException.class)
    public void testBookingLimitRulesException() throws SQLException {
        throwPreparedStatementException();
        store.isBookingLimitReached(dataSource, MEMBERSHIP_ID);
    }

    @Test(expectedExceptions = SQLException.class)
    public void testBookingLimitBookingException() throws SQLException {
        setRowsFound(true, true);
        setMembershipRules(5L, 15, 1);
        throwOnSecondExecuteQueryException();
        store.isBookingLimitReached(dataSource, MEMBERSHIP_ID);
    }

    private void throwOnSecondExecuteQueryException() throws SQLException {
        when(preparedStatement.executeQuery()).thenReturn(resultSet).thenThrow(new SQLException());
    }

    private void throwPreparedStatementException() throws SQLException {
        doThrow(new SQLException()).when(preparedStatement).executeUpdate();
        doThrow(new SQLException()).when(preparedStatement).executeQuery();
    }

    private void setTotalBookingsFound(int value) throws SQLException {
        when(resultSet.getInt("TOTAL_BOOKINGS")).thenReturn(value);
    }

    private void setMembershipRules(long bookings, int days, int isActive) throws SQLException {
        when(resultSet.getLong("NUM_BOOKINGS")).thenReturn(bookings);
        when(resultSet.getInt("NUM_DAYS")).thenReturn(days);
        when(resultSet.getInt("IS_ACTIVE")).thenReturn(isActive);
    }

    private void setRowsFound(boolean existsRules, boolean existsBookings) throws SQLException {
        when(resultSet.next()).thenReturn(existsRules).thenReturn(existsBookings);
        setResultSetResult();
    }

    private void setResultSetResult() throws SQLException {
        when(resultSet.getLong(eq("MEMBERSHIP_ID"))).thenReturn(MEMBERSHIP_ID);
        when(resultSet.getLong(eq("BOOKING_ID"))).thenReturn(BOOKING_ID);
        when(resultSet.getTimestamp(eq("BOOKING_DATE"))).thenReturn(Timestamp.valueOf(BOOKING_DATE));
        when(resultSet.getBigDecimal(eq("AVOID_FEE_AMOUNT"))).thenReturn(AVOID_FEE_AMOUNT);
        when(resultSet.getBigDecimal(eq("COST_FEE_AMOUNT"))).thenReturn(COST_FEE_AMOUNT);
        when(resultSet.getBigDecimal(eq("PERKS_FEE_AMOUNT"))).thenReturn(PERKS_FEE_AMOUNT);
    }

    private void setRowsUpdated(int rows) throws SQLException {
        when(preparedStatement.executeUpdate()).thenReturn(rows);
    }

    @Test
    public void testGetBookingTrackedByMembershipId() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        setResultSetResult();
        Collection<BookingTracking> bookingTracking = store.getBookingTrackedByMembershipId(dataSource, MEMBERSHIP_ID);
        assertFalse(bookingTracking.isEmpty());
    }
}
