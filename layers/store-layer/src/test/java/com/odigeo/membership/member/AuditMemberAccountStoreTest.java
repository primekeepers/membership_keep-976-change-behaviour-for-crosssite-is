package com.odigeo.membership.member;

import com.odigeo.membership.AuditMemberAccount;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import static java.lang.Boolean.TRUE;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AuditMemberAccountStoreTest {

    private AuditMemberAccountStore auditMemberAccountStore;
    private DataSource dataSourceMock;
    private Connection connectionMock;

    @BeforeMethod
    public void setUp() throws Exception {
        auditMemberAccountStore = new AuditMemberAccountStore();
        initDataSourceConnectionMock();
    }

    @Test
    public void testInsertAuditMemberAccountIsCalledWithExpectedValues() throws Exception {
        //Given
        final String insertScript = "INSERT INTO GE_AUDIT_MEMBER_ACCOUNT(ID,MEMBER_ACCOUNT_ID,USER_ID,FIRST_NAME,LAST_NAME) VALUES (?, ?, ?, ?, ?) ";
        final String sequenceScript = "SELECT SEQ_GE_AUDIT_MEMBER_ACCOUNT_ID.nextval FROM dual ";
        final long generatedAuditId = generateNextSequenceValueFromScript(sequenceScript);
        final AuditMemberAccount auditMemberAccountData = AuditMemberAccount.builder()
                .memberAccountId(333L)
                .userId(93L)
                .name("test name")
                .lastName("last names")
                .build();

        final PreparedStatement insertPreparedStatement = mock(PreparedStatement.class);
        given(connectionMock.prepareStatement(insertScript)).willReturn(insertPreparedStatement);

        //When
        auditMemberAccountStore.insertAuditMemberAccount(dataSourceMock, auditMemberAccountData);

        //Then
        verify(insertPreparedStatement).setLong(1, generatedAuditId);
        verify(insertPreparedStatement).setLong(2, auditMemberAccountData.getMemberAccountId());
        verify(insertPreparedStatement).setLong(3, auditMemberAccountData.getUserId());
        verify(insertPreparedStatement).setString(4, auditMemberAccountData.getName());
        verify(insertPreparedStatement).setString(5, auditMemberAccountData.getLastName());
        verify(insertPreparedStatement).execute();
    }

    @Test
    public void testInsertAuditMemberAccountThrowsSQLException() {
        //Given
        final AuditMemberAccount auditMemberAccountData = AuditMemberAccount.builder().build();
        final Throwable expectedException = new SQLException("expected exception");
        try {
            given(connectionMock.prepareStatement(anyString())).willThrow(expectedException);
            //When
            auditMemberAccountStore.insertAuditMemberAccount(dataSourceMock, auditMemberAccountData);
        } catch (Exception ex) {
            //Then
            assertEquals(ex.getClass(), SQLException.class);
            assertEquals(ex, expectedException);
        }
    }

    @Test
    private void testEquals() {
        EqualsVerifier.forClass(AuditMemberAccount.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    private Long generateNextSequenceValueFromScript(final String sequenceScript) throws SQLException {
        final Long nextSequenceValue = new Random().nextLong();
        final PreparedStatement seqPreparedStatement = mock(PreparedStatement.class);
        final ResultSet seqResultSet = mock(ResultSet.class);

        given(connectionMock.prepareStatement(sequenceScript)).willReturn(seqPreparedStatement);
        when(seqPreparedStatement.executeQuery()).thenReturn(seqResultSet);
        when(seqResultSet.next()).thenReturn(TRUE);
        when(seqResultSet.getLong(1)).thenReturn(nextSequenceValue);

        return nextSequenceValue;
    }

    private void initDataSourceConnectionMock() throws SQLException {
        dataSourceMock = mock(DataSource.class);
        connectionMock = mock(Connection.class);
        given(dataSourceMock.getConnection()).willReturn(connectionMock);
    }
}