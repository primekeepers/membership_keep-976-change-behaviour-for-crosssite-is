package com.odigeo.membership.member.auth.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class MembershipAuthenticationException extends WebApplicationException {
    private final String message;
    private final Throwable cause;

    public MembershipAuthenticationException(String message, Throwable cause) {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new MembershipExceptionBean(MembershipAuthenticationException.class, message)).type("application/json").build());
        this.message = message;
        this.cause = cause;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
