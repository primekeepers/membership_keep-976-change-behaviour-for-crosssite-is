package com.odigeo.membership.parameters;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.TimeUnit;
import com.odigeo.membership.parameters.MemberAccountCreation.MemberAccountCreationBuilder;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class MembershipCreationBuilder {

    private static final int MONTHS_DURATION_DEFAULT = 12;

    private String website;
    private Long memberAccountId;
    private MemberStatus memberStatus;
    private MembershipRenewal autoRenewal;
    private BigDecimal balance;
    private LocalDateTime activationDate;
    private LocalDateTime expirationDate;
    private StatusAction statusAction;
    private MembershipType membershipType;
    private String monthsDuration;
    private TimeUnit durationTimeUnit;
    private Integer duration;
    private SourceType sourceType;
    private BigDecimal subscriptionPrice;
    private BigDecimal renewalPrice;
    private Integer renewalDuration;
    private String productStatus;
    private String currencyCode;
    private String recurringId;
    private UUID recurringCollectionId;
    private UserCreation userCreation;
    private MemberAccountCreationBuilder memberAccountCreationBuilder;
    private Long feeContainerId;
    private boolean shoppingBasketProduct;

    public TimeUnit getDurationTimeUnit() {
        return durationTimeUnit;
    }

    public Integer getDuration() {
        if (duration != null) {
            return duration;
        }
        return getMonthsDuration();
    }

    public MembershipCreationBuilder withDurationTimeUnit(TimeUnit durationTimeUnit) {
        this.durationTimeUnit = durationTimeUnit;
        return this;
    }

    public MembershipCreationBuilder withDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public int getMonthsDuration() {
        int monthsDurationValue;
        if (TimeUnit.MONTHS.equals(getDurationTimeUnit()) && Objects.nonNull(duration)) {
            monthsDurationValue = getDuration();
        } else {
            monthsDurationValue = StringUtils.isNotBlank(monthsDuration) ? Integer.parseInt(monthsDuration) : MONTHS_DURATION_DEFAULT;
        }
        return monthsDurationValue;
    }

    public String getWebsite() {
        return website;
    }

    public MembershipCreationBuilder withWebsite(String website) {
        this.website = website;
        return this;
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipCreationBuilder withMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public Integer getRenewalDuration() {
        return renewalDuration;
    }

    public MembershipCreationBuilder withRenewalDuration(Integer renewalDuration) {
        this.renewalDuration = renewalDuration;
        return this;
    }

    public MembershipCreationBuilder withMemberStatus(MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
        return this;
    }

    public MembershipRenewal getAutoRenewal() {
        return autoRenewal;
    }

    public MembershipCreationBuilder withAutoRenewal(MembershipRenewal autoRenewal) {
        this.autoRenewal = autoRenewal;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public MembershipCreationBuilder withBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public MembershipCreationBuilder withActivationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public MembershipCreationBuilder withExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public StatusAction getStatusAction() {
        return statusAction;
    }

    public MembershipCreationBuilder withStatusAction(StatusAction statusAction) {
        this.statusAction = statusAction;
        return this;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public MembershipCreationBuilder withMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public MembershipCreationBuilder withMonthsDuration(String monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public MembershipCreationBuilder withSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public BigDecimal getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public MembershipCreationBuilder withSubscriptionPrice(BigDecimal subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
        return this;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public MembershipCreationBuilder withRenewalPrice(BigDecimal renewalPrice) {
        this.renewalPrice = renewalPrice;
        return this;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public MembershipCreationBuilder withProductStatus(String productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public MembershipCreationBuilder withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public MembershipCreationBuilder withRecurringId(String recurringId) {
        this.recurringId = recurringId;
        return this;
    }

    public UUID getRecurringCollectionId() {
        return recurringCollectionId;
    }

    public MembershipCreationBuilder withRecurringCollectionId(UUID recurringCollectionId) {
        this.recurringCollectionId = recurringCollectionId;
        return this;
    }

    public UserCreation getUserCreation() {
        return userCreation;
    }

    public MembershipCreationBuilder withUserCreation(UserCreation userCreation) {
        this.userCreation = userCreation;
        return this;
    }

    public MembershipCreation build() {
        return new MembershipCreation(this);
    }

    public MemberAccountCreationBuilder getMemberAccountCreationBuilder() {
        return memberAccountCreationBuilder;
    }

    public MembershipCreationBuilder withMemberAccountCreationBuilder(MemberAccountCreationBuilder memberAccountCreationBuilder) {
        this.memberAccountCreationBuilder = memberAccountCreationBuilder;
        return this;
    }

    public Long getFeeContainerId() {
        return feeContainerId;
    }

    public MembershipCreationBuilder withFeeContainerId(Long feeContainerId) {
        this.feeContainerId = feeContainerId;
        return this;
    }

    public boolean isShoppingBasketProduct() {
        return shoppingBasketProduct;
    }

    public MembershipCreationBuilder withShoppingBasketProduct(boolean shoppingBasketProduct) {
        this.shoppingBasketProduct = shoppingBasketProduct;
        return this;
    }
}
