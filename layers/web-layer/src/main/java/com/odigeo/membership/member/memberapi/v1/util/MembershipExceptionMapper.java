package com.odigeo.membership.member.memberapi.v1.util;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.common.collect.ImmutableMap;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.AlreadyHasPrimeMembershipException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.MembershipConflictException;
import com.odigeo.membership.exception.MembershipForbiddenException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.MembershipPreconditionFailedException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.MembershipUnauthorizedException;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.member.auth.exception.MembershipAuthenticationException;

import java.util.Map;
import java.util.function.BiFunction;

public final class MembershipExceptionMapper {

    private static Map<Class<? extends Exception>, BiFunction<String, Throwable, MembershipServiceException>> exceptionMap =
            ImmutableMap.<Class<? extends Exception>, BiFunction<String, Throwable, MembershipServiceException>>builder()
                    .put(MissingElementException.class, MembershipNotFoundException::new)
                    .put(ActivatedMembershipException.class, MembershipPreconditionFailedException::new)
                    .put(ExistingRecurringException.class, MembershipForbiddenException::new)
                    .put(DataAccessException.class, MembershipInternalServerErrorException::new)
                    .put(MembershipAuthenticationException.class, MembershipUnauthorizedException::new)
                    .put(UserAccountNotFoundException.class, MembershipNotFoundException::new)
                    .put(AlreadyHasPrimeMembershipException.class, MembershipConflictException::new)
                    .build();

    private MembershipExceptionMapper() {
    }

    public static MembershipServiceException map(Exception exception) {
        return exceptionMap.getOrDefault(exception.getClass(), MembershipInternalServerErrorException::new)
                .apply(exception.getMessage(), exception.getCause());
    }
}
