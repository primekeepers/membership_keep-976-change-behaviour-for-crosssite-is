package com.odigeo.userapi;

import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.Source;
import com.odigeo.userprofiles.api.v2.model.Status;
import com.odigeo.userprofiles.api.v2.model.TrafficInterface;

import java.util.Date;

public class UserDTO {
    private Long id;
    private String email;
    private Source source;
    private String flightStatusId;
    private Brand brand;
    private TrafficInterface trafficInterface;
    private String website;
    private Status status;
    private Date creationDate;
    private Date lastModified;
    private String locale;
    private String marketingPortal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getFlightStatusId() {
        return flightStatusId;
    }

    public void setFlightStatusId(String flightStatusId) {
        this.flightStatusId = flightStatusId;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public TrafficInterface getTrafficInterface() {
        return trafficInterface;
    }

    public void setTrafficInterface(TrafficInterface trafficInterface) {
        this.trafficInterface = trafficInterface;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate != null ? new Date(creationDate.getTime()) : null;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = new Date(creationDate.getTime());
    }

    public Date getLastModified() {
        return lastModified != null ? new Date(lastModified.getTime()) : null;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = new Date(lastModified.getTime());
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getMarketingPortal() {
        return marketingPortal;
    }

    public void setMarketingPortal(String marketingPortal) {
        this.marketingPortal = marketingPortal;
    }

}
