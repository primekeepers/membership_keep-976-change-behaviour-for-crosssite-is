package com.odigeo.product.membership.rest;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.MembershipPrices;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.product.membership.service.ProductServiceV2;
import com.odigeo.product.membership.service.ProductServiceV2New;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.response.exception.MembershipProductServiceException;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.CloseProductProviderRequest;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Price;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.enums.ProductType;
import com.odigeo.product.v2.model.payment.CreditCardPaymentEntity;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.fail;

public class ProductControllerV2Test {

    private static final String PRODUCT_ID = "123";
    private static final String WEBSITE = "ES";
    private static final String TRANSACTION_PRODUCT_ID = "123";
    private static final String CURRENCY = "EUR";
    private static final String MEMBERSHIP_ID = "321";
    private static final long MEMBER_ACCOUNT_ID = 123L;
    private static final long PROVIDER_PRODUCT_ID = 321L;
    private static final LocalDateTime TODAY = LocalDateTime.now();
    private static final LocalDateTime NEXT_YEAR = TODAY.plusYears(1);
    private static final String CURRENCY_CODE = "USD";

    private static final Membership MEMBERSHIP = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID))
            .setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(TODAY)
            .setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID)
            .setProductStatus(ProductStatus.CONTRACT)
            .setMembershipPricesBuilder(MembershipPrices.builder().totalPrice(BigDecimal.ONE)
                    .currencyCode(CURRENCY_CODE)).build();
    private static final Membership MEMBERSHIP_INIT_STATUS = new MembershipBuilder()
            .setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(TODAY)
            .setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID)
            .setProductStatus(ProductStatus.INIT)
            .setMembershipPricesBuilder(MembershipPrices.builder().totalPrice(BigDecimal.ONE)
                    .currencyCode(CURRENCY_CODE)).build();
    private static final MembershipFee MEMBERSHIP_FEE = new MembershipFee(MEMBERSHIP_ID, BigDecimal.TEN, CURRENCY, "RENEWAL_FEE");
    private static final String PRODUCT_NOT_FOUND = "ProductNotFound";
    private static final ProductException PRODUCT_EXCEPTION = new ProductException(PRODUCT_ID, ProductType.MEMBERSHIP_RENEWAL, PRODUCT_NOT_FOUND);
    private static final String GET_BOOKING_API_PRODUCT_INFO = "GET_BOOKING_API_PRODUCT_INFO";
    private static final String GET_PRODUCT = "GET_PRODUCT";
    private static final String GET_PRODUCT_DETAILS = "GET_PRODUCT_DETAILS";
    private static final String ROLLBACK_TRANSACTION_PRODUCT = "ROLLBACK_TRANSACTION_PRODUCT";
    private static final String CANCEL_TRANSACTION_PRODUCT = "CANCEL_TRANSACTION_PRODUCT";
    private static final String COMMIT_TRANSACTION_PRODUCT = "COMMIT_TRANSACTION_PRODUCT";
    private static final String SUSPEND_TRANSACTION_PRODUCT = "SUSPEND_TRANSACTION_PRODUCT";
    private static final String RESUME_TRANSACTION_PRODUCT = "RESUME_TRANSACTION_PRODUCT";
    private static final String SAVE_PRODUCT = "SAVE_PRODUCT";
    private static final long FEE_ID = 2L;
    private static final String SUB_CODE = "AE10";
    private static final AbstractFee FEE = new FixFee(FEE_ID, FeeLabel.MARKUP_TAX, SUB_CODE,
            Currency.getInstance("USD"), null, BigDecimal.TEN, new Date());

    private ProductServiceV2 productService;

    @Mock
    private MemberService memberService;

    @Mock
    private UpdateMembershipService updateMembershipService;

    @Mock
    private MembershipProductService membershipProductService;

    @Mock
    private MembershipFeesService membershipFeesService;

    private ProductControllerV2 productController;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        productService = Mockito.spy(new ProductServiceV2New(memberService, membershipProductService, updateMembershipService, membershipFeesService));
        productController = new ProductControllerV2();
    }

    private void configure(Binder binder) {
        binder.bind(MemberService.class).toInstance(memberService);
        binder.bind(UpdateMembershipService.class).toInstance(updateMembershipService);
        binder.bind(MembershipProductService.class).toInstance(membershipProductService);
        binder.bind(MembershipFeesService.class).toInstance(membershipFeesService);
    }

    @Test
    public void testGetProductHappyPath() throws ProductException, DataAccessException, MissingElementException {
        when(membershipProductService.getMembershipFees(Long.parseLong(MEMBERSHIP_ID))).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        when(memberService.getMembershipById(Long.parseLong(MEMBERSHIP_ID))).thenReturn(MEMBERSHIP);
        when(membershipFeesService.fillProductFee(any(), any())).thenReturn((FixFee) FEE);
        Product product = productController.getProduct(MEMBERSHIP_ID);
        assertNotNull(product);
        Assert.assertEquals(product.getId(), MEMBERSHIP_ID);
    }

    @Test
    public void testGetBookingApiProductHappyPath() throws ProductException, DataAccessException, MissingElementException {
        when(membershipProductService.getMembershipFees(Long.parseLong(PRODUCT_ID))).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        when(membershipFeesService.fillProductFee(any(), any())).thenReturn((FixFee) FEE);
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenReturn(MEMBERSHIP);
        BookingApiMembershipInfo bookingApiMembershipInfo = productController.getBookingApiProductInfo(PRODUCT_ID);
        assertNotNull(bookingApiMembershipInfo);
        assertTrue(bookingApiMembershipInfo.isRenewal());
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).getProduct(anyString());
        productController.getProduct(null);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetBookingApiProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).getBookingApiProductInfo(anyString());
        productController.getBookingApiProductInfo(null);
    }

    @Test
    public void testCloseProviderProductResult() throws ProductException {
        final CloseProviderProductResponse response = productController.closeProviderProduct(PRODUCT_ID,
                TRANSACTION_PRODUCT_ID, new CloseProductProviderRequest());
        assertNotNull(response);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testRollbackProviderProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).rollbackTransactionProduct(anyString(), anyString());
        productController.rollbackTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void testCancelProviderProduct() throws ProductException {
        productController.cancelTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testCancelProviderProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).cancelTransactionProduct(anyString(), anyString());
        productController.cancelTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void testCommitProviderProduct() throws ProductException, MissingElementException, DataAccessException {
        doNothing().when(productService).commitTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenReturn(MEMBERSHIP_INIT_STATUS);
        productController.commitTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testCommitProviderProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).commitTransactionProduct(anyString(), anyString());
        productController.commitTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void testSuspendProviderProduct() throws ProductException {
        productController.suspendTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testSuspendProviderProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).suspendTransactionProduct(anyString(), anyString());
        productController.suspendTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void testResumeProviderProduct() throws ProductException {
        productController.resumeTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testResumeProviderProductWhenNotFound() throws ProductException {
        doThrow(PRODUCT_EXCEPTION).when(productService).resumeTransactionProduct(anyString(), anyString());
        productController.resumeTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void testSetPaymentDetails() throws ProductException {
        assertNull(productController.setPaymentDetails(PRODUCT_ID, PROVIDER_PRODUCT_ID, new CreditCardPaymentEntity()));
    }

    @Test
    public void testGetProductDetails() throws ProductException, DataAccessException, MissingElementException {
        when(membershipProductService.getMembershipFees(Long.parseLong(MEMBERSHIP_ID))).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        when(memberService.getMembershipById(Long.parseLong(MEMBERSHIP_ID))).thenReturn(MEMBERSHIP);
        when(membershipFeesService.fillProductFee(any(), any())).thenReturn((FixFee) FEE);
        Product product = productController.getProductDetails(MEMBERSHIP_ID);
        assertNotNull(product);
        assertEquals(product.getId(), MEMBERSHIP_ID);
    }

    @Test
    public void testSaveProduct() throws ProductException {
        doNothing().when(productService).saveProduct(PRODUCT_ID, null, true);
        productController.saveProduct(PRODUCT_ID, true);
    }

    @Test
    public void testGetBookingApiProductInfoWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).getBookingApiProductInfo(anyString());
        testException(GET_BOOKING_API_PRODUCT_INFO, null, 400);
    }

    @Test
    public void testGetBookingApiProductInfoWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).getBookingApiProductInfo(anyString());
        testException(GET_BOOKING_API_PRODUCT_INFO, StringUtils.EMPTY, 400);
    }

    @Test
    public void testGetProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).getProduct(anyString());
        testException(GET_PRODUCT, null, 400);
    }

    @Test
    public void testGetProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).getProduct(anyString());
        testException(GET_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testGetProductDetailsWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).getProduct(anyString());
        testException(GET_PRODUCT_DETAILS, null, 400);
    }

    @Test
    public void testGetProductDetailsWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).getProduct(anyString());
        testException(GET_PRODUCT_DETAILS, StringUtils.EMPTY, 400);
    }

    @Test
    public void testRollbackTransactionProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).rollbackTransactionProduct(anyString(), anyString());
        testException(ROLLBACK_TRANSACTION_PRODUCT, null, 400);
    }

    @Test
    public void testRollbackTransactionProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).rollbackTransactionProduct(anyString(), anyString());
        testException(ROLLBACK_TRANSACTION_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testCancelTransactionProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).cancelTransactionProduct(anyString(), anyString());
        testException(CANCEL_TRANSACTION_PRODUCT, null, 400);
    }

    @Test
    public void testCancelTransactionProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).cancelTransactionProduct(anyString(), anyString());
        testException(CANCEL_TRANSACTION_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testCommitTransactionProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).commitTransactionProduct(anyString(), anyString());
        testException(COMMIT_TRANSACTION_PRODUCT, null, 400);
    }

    @Test
    public void testCommitTransactionProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).commitTransactionProduct(anyString(), anyString());
        testException(COMMIT_TRANSACTION_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testSuspendTransactionProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).suspendTransactionProduct(anyString(), anyString());
        testException(SUSPEND_TRANSACTION_PRODUCT, null, 400);
    }

    @Test
    public void testSuspendTransactionProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).suspendTransactionProduct(anyString(), anyString());
        testException(SUSPEND_TRANSACTION_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testResumeTransactionProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).resumeTransactionProduct(anyString(), anyString());
        testException(RESUME_TRANSACTION_PRODUCT, null, 400);
    }

    @Test
    public void testResumeTransactionProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).resumeTransactionProduct(anyString(), anyString());
        testException(RESUME_TRANSACTION_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testSaveProductWithNullParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).saveProduct(anyString(), anyString(), anyBoolean());
        testException(SAVE_PRODUCT, null, 400);
    }

    @Test
    public void testSaveProductWithEmptyParameterThrowsException() throws ProductException {
        doCallRealMethod().when(productService).saveProduct(anyString(), anyString(), anyBoolean());
        testException(SAVE_PRODUCT, StringUtils.EMPTY, 400);
    }

    @Test
    public void testSaveProductWithNullParameterAndSaveTypeTrueThrowsException() throws ProductException {
        doCallRealMethod().when(productService).saveProduct(anyString(), anyString(), anyBoolean());
        testException(SAVE_PRODUCT, true, null, 400);
    }

    @Test
    public void testSaveProductWithEmptyParameterAndSaveTypeTrueThrowsException() throws ProductException {
        doCallRealMethod().when(productService).saveProduct(anyString(), anyString(), anyBoolean());
        testException(SAVE_PRODUCT, true, StringUtils.EMPTY, 400);
    }

    @Test
    public void testGetBookingApiProductInfoWithNonExistingProductThrowsException() throws MissingElementException, DataAccessException {
        doThrow(new DataNotFoundException("Can't find product")).when(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
        testException(GET_BOOKING_API_PRODUCT_INFO, PRODUCT_ID, 404);
    }

    @Test
    public void testGetProductWithNonExistingProductThrowsException() throws MissingElementException, DataAccessException {
        doThrow(new MissingElementException("Can't find product")).when(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
        testException(GET_PRODUCT, PRODUCT_ID, 404);
    }

    @Test
    public void testGetProductDetailsWithNonExistingProductThrowsException() throws MissingElementException, DataAccessException {
        doThrow(new MissingElementException("Can't find product")).when(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
        testException(GET_PRODUCT_DETAILS, PRODUCT_ID, 404);
    }

    @Test
    public void testCommitTransactionProductWithNonExistingProductThrowsException() throws ProductException, MissingElementException, DataAccessException {
        doCallRealMethod().when(productService).commitTransactionProduct(PRODUCT_ID, null);
        when(memberService.getMembershipById(anyLong())).thenThrow(new MissingElementException("Can't find product"));
        testException(COMMIT_TRANSACTION_PRODUCT, PRODUCT_ID, 404);
    }

    @Test
    public void testGetBookingApiProductInfoWithExternalFailureThrowsException() throws MissingElementException, DataAccessException {
        doThrow(new DataAccessException("Random failure accessing data")).when(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
        testException(GET_BOOKING_API_PRODUCT_INFO, PRODUCT_ID, 500);
    }

    @Test
    public void testGetProductWithExternalFailureThrowsException() throws MissingElementException, DataAccessException {
        doThrow(new DataAccessException("Random failure accessing data")).when(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
        testException(GET_PRODUCT, PRODUCT_ID, 404);
    }

    @Test
    public void testGetProductDetailsWithExternalFailureThrowsException() throws MissingElementException, DataAccessException {
        doThrow(new DataAccessException("Random failure accessing data")).when(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
        testException(GET_PRODUCT_DETAILS, PRODUCT_ID, 404);
    }

    @Test
    public void testCommitTransactionProductWithExternalFailureThrowsException() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyLong())).thenThrow(new DataAccessException("Random failure accessing data"));
        testException(COMMIT_TRANSACTION_PRODUCT, PRODUCT_ID, 500);
    }

    @Test
    public void testSaveProductWithExternalFailureThrowsException() throws ProductException, MissingElementException, DataAccessException {
        doCallRealMethod().when(productService).saveProduct(PRODUCT_ID, null, false);
        when(memberService.getMembershipById(anyLong())).thenThrow(new DataAccessException("Random failure accessing data"));
        testException(SAVE_PRODUCT, PRODUCT_ID, 500);
    }

    @Test
    public void testSaveProductWithExternalFailureThrowsMissingElementException() throws ProductException, MissingElementException, DataAccessException {
        doCallRealMethod().when(productService).saveProduct(PRODUCT_ID, null, false);
        when(memberService.getMembershipById(anyLong())).thenThrow(new MissingElementException("Data not found"));
        testException(SAVE_PRODUCT, PRODUCT_ID, 404);
    }

    @Test
    public void testSaveProductCompleteWithProductNotInContractThrowsException() throws ProductException, MissingElementException, DataAccessException {
        doCallRealMethod().when(productService).saveProduct(PRODUCT_ID, null, false);
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenReturn(MEMBERSHIP_INIT_STATUS);
        testException(SAVE_PRODUCT, PRODUCT_ID, 403);
    }

    @Test
    public void testCommitTransactionProductWithExistingProductInContractThrowsException() throws MissingElementException, DataAccessException, ProductException {
        doCallRealMethod().when(productService).commitTransactionProduct(PRODUCT_ID, null);
        when(memberService.getMembershipById(anyLong())).thenReturn(MEMBERSHIP);
        testException(COMMIT_TRANSACTION_PRODUCT, PRODUCT_ID, 403);
    }

    private void testException(String whichOperation, String productId, int expectedHttpCode) {
        testException(whichOperation, false, productId, expectedHttpCode);
    }

    private void testException(String whichOperation, boolean saveType, String productId, int expectedHttpCode) {
        try {
            executeOperation(whichOperation, saveType, productId);
            fail("Should have thrown an exception");
        } catch (ProductException e) {
            if (!(e instanceof MembershipProductServiceException)) {
                fail("Expected a MembershipProductServiceException, got another ProductException");
            }
            MembershipProductServiceException mpse = (MembershipProductServiceException) e;
            Assert.assertEquals(expectedHttpCode, mpse.getMembershipProductServiceExceptionType().getHttpCode());
        }
    }

    private static Fee mockFee(InvocationOnMock invocation) {
        Fee fee = new Fee();
        Price price = new Price();
        price.setAmount(BigDecimal.TEN);
        price.setCurrency(Currency.getInstance(CURRENCY));
        fee.setPrice(price);
        return fee;
    }

    private void executeOperation(String whichOperation, boolean saveType, String productId) throws ProductException {
        switch (whichOperation) {
            case GET_BOOKING_API_PRODUCT_INFO:
                productController.getBookingApiProductInfo(productId);
                break;
            case GET_PRODUCT:
                productController.getProduct(productId);
                break;
            case GET_PRODUCT_DETAILS:
                productController.getProductDetails(productId);
                break;
            case ROLLBACK_TRANSACTION_PRODUCT:
                productController.rollbackTransactionProduct(productId, null);
                break;
            case CANCEL_TRANSACTION_PRODUCT:
                productController.cancelTransactionProduct(productId, null);
                break;
            case COMMIT_TRANSACTION_PRODUCT:
                productController.commitTransactionProduct(productId, null);
                break;
            case SUSPEND_TRANSACTION_PRODUCT:
                productController.suspendTransactionProduct(productId, null);
                break;
            case RESUME_TRANSACTION_PRODUCT:
                productController.resumeTransactionProduct(productId, null);
                break;
            case SAVE_PRODUCT:
                productController.saveProduct(productId, saveType);
                break;
            default:
                fail("Unknown operation: " + whichOperation);
        }
    }
}
