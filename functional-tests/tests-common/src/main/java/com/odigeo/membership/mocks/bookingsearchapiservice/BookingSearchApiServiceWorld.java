package com.odigeo.membership.mocks.bookingsearchapiservice;

import com.google.inject.Inject;
import com.odigeo.membership.server.ServerStopException;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;

@ScenarioScoped
public class BookingSearchApiServiceWorld {

    private final BookingSearchApiServiceMock bookingSearchApiServiceMock;
    private final BookingSearchApiServiceHttpServer bookingApiServiceHttpServer;

    @Inject
    public BookingSearchApiServiceWorld(BookingSearchApiServiceMock bookingSearchApiServiceMock, BookingSearchApiServiceHttpServer bookingApiServiceHttpServer) {
        this.bookingSearchApiServiceMock = bookingSearchApiServiceMock;
        this.bookingApiServiceHttpServer = bookingApiServiceHttpServer;
    }

    public void install() throws ServerStopException {
        if (bookingApiServiceHttpServer.serverNotCreated()) {
            bookingApiServiceHttpServer.startServer();
        }
        bookingApiServiceHttpServer.addService(bookingSearchApiServiceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(bookingApiServiceHttpServer)) {
            bookingApiServiceHttpServer.clearServices();
        }
    }

    public BookingSearchApiServiceMock getBookingSearchApiServiceMock() {
        return bookingSearchApiServiceMock;
    }
}
