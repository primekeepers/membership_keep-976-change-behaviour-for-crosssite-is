package com.odigeo.membership.member;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.AuditMemberAccount;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.exception.DataNotFoundException;

import javax.sql.DataSource;
import java.sql.SQLException;

@Singleton
public class AuditMemberAccountManager {

    private final AuditMemberAccountStore auditMemberAccountStore;
    private final MemberAccountStore memberAccountStore;

    @Inject
    AuditMemberAccountManager(final AuditMemberAccountStore auditMemberAccountStore, final MemberAccountStore memberAccountStore) {
        this.auditMemberAccountStore = auditMemberAccountStore;
        this.memberAccountStore = memberAccountStore;
    }

    public void auditUpdatedMemberAccount(final DataSource dataSource, final Long memberAccountId) throws SQLException, DataNotFoundException {

        MemberAccount memberAccount = memberAccountStore.getMemberAccountById(dataSource, memberAccountId);
        auditMemberAccountStore.insertAuditMemberAccount(dataSource, AuditMemberAccount.builder()
                .memberAccountId(memberAccount.getId())
                .userId(memberAccount.getUserId())
                .name(memberAccount.getName())
                .lastName(memberAccount.getLastNames())
                .build());
    }

    void auditNewMemberAccount(final DataSource dataSource, final AuditMemberAccount auditMemberAccount) throws SQLException {
        auditMemberAccountStore.insertAuditMemberAccount(dataSource, auditMemberAccount);
    }
}
