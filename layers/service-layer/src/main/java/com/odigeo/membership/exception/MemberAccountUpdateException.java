package com.odigeo.membership.exception;

import com.edreams.base.BaseRuntimeException;

public class MemberAccountUpdateException extends BaseRuntimeException {
    public MemberAccountUpdateException(String message, Exception e) {
        super(message, e);
    }
}
