package com.odigeo.commons.monitoring.metrics;

public final class MetricsNames {

    public static final String METRICS_REGISTRY_NAME = "member-service";

    //METRICS NAMES
    public static final String TRACKED_BOOKINGS_NUMBER = "membership.trackedbookins";
    public static final String ACTIVATIONS_NUMBER = "membership.activationsactivated";
    public static final String RENEWAL_ACTIVATIONS_NUMBER = "membership.renewalactivationsnumber";
    public static final String RENEWAL_CONTRACT_NUMBER = "membership.renewalcontractnumber";
    public static final String METRICS_TIME_TABLE_NAME = "membership.time";
    public static final String FAILED_SUBSCRIPTION_MSG = "membership.subscriptionmessagefailure";
    public static final String SUCCESS_SUBSCRIPTION_MSG = "membership.subscriptionmessage";
    public static final String FAILED_UPDATE_MSG = "membership.updatemessagefailure";
    public static final String SUCCESS_UPDATE_MSG = "membership.updatemessage";
    public static final String FAILED_WELCOME_TO_PRIME_MSG = "membership.welcometoprimemessagefailure";
    public static final String SUCCESS_WELCOME_TO_PRIME_MSG = "membership.welcometoprimemessage";
    public static final String BOOKING_MEMBERSHIP_RECURRING_BY_WEBSITE = "membership.bookingmembershiprecurringbywebsite";
    public static final String FAIL_RECURRING = "membership.failrecurring";
    public static final String MEMORYSTORE_SUCCESS = "membership.memorystoresuccess";
    public static final String MEMORYSTORE_HIT_CACHE_SUCCESS = "membership.memorystore-hit-cache-success";
    public static final String MEMORYSTORE_HIT_CACHE_FAILURE = "membership.memorystore-hit-cache-failure";
    public static final String MEMORYSTORE_FAILURE = "membership.memorystorefailure";
    public static final String BASIC_FREE_CREATION = "membership.basic-free";
    public static final String PENDING_TO_COLLECT_CREATION = "membership.pending-to-collect";
    public static final String BASIC_CREATION = "membership.basic-creation";
    public static final String MEMBERSHIP_AUTORENEWAL = "membership.autorenewal";
    public static final String FAILED_CRM_MSG = "membership.crmmessagefailure";
    public static final String USER_ELIGIBLE_SUCCESS = "membership.free-trial-eligible-success";
    public static final String USER_ELIGIBLE_BLOCKED = "membership.free-trial-eligible-blocked";
    public static final String FIX_USER_ACCOUNT_STATUS_ONE_ATTEMPT = "membership.fix-user-account-status-one-attempt";
    //Tags
    public static final String ATTEMPT_NUMBER = "attempt";
    public static final String TRACKING_RETRY_NUMBER = "retry";
    public static final String OPERATION = "operation";
    public static final String WEBSITE = "website";
    public static final String ERROR_RECURRING = "errorRecurring";
    public static final String AUTORENEWAL_MEMBERSHIP_ID = "id";
    public static final String AUTORENEWAL_METHOD = "method";
    public static final String AUTORENEWAL_REQUESTER = "client_module";
    public static final String AUTORENEWAL_OPERATION = "operation";
    //Tag values
    public static final String OPERATION_STORE_REDIS = "membership-operation-store-redis";
    public static final String OPERATION_FETCH_REDIS = "membership-operation-fetch-redis";

    private MetricsNames() {
    }
}
