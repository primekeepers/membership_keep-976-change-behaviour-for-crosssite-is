package com.odigeo.messaging;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Singleton;
import com.odigeo.membership.Membership;
import com.odigeo.membership.member.AbstractServiceBean;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationMapper;
import com.odigeo.shoppingbasket.checkout.messages.ProductClosureRequested;
import org.apache.log4j.Logger;

import java.sql.SQLException;

@Singleton
public class MembershipProductClosureService extends AbstractServiceBean {

    private static final Logger LOGGER = Logger.getLogger(MembershipProductClosureService.class);

    public void closeMembership(ProductClosureRequested productClosureRequested) throws DataAccessException, MissingElementException {
        LOGGER.info("Membership closure request " + productClosureRequested.getProductReference().getReference());
        Membership membership = getMembershipNoSQLManager().get(productClosureRequested.getProductReference().getReference())
                .orElseThrow(() -> new MissingElementException("MembershipId not found on NoSQL database: " + productClosureRequested.getProductReference().getReference()));

        MembershipCreation request = MembershipCreationMapper.from(membership);
        try {
            long membershipId = getMembershipStore().createMember(dataSource, request);
            getMembershipRecurringStore().insertMembershipRecurringCollectionId(dataSource, membershipId, membership.getRecurringCollectionId());
        } catch (SQLException ex) {
            throw new DataAccessException("Could not create the membership with reference: "
                    + productClosureRequested.getProductReference().getReference(), ex);
        }
    }

}

