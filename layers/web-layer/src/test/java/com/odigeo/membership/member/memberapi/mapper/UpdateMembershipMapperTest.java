package com.odigeo.membership.member.memberapi.mapper;

import com.odigeo.membership.request.product.UpdateMembershipRequest;
import org.testng.annotations.Test;

public class UpdateMembershipMapperTest extends MapperTest<UpdateMembershipMapper> {

    @Override
    protected UpdateMembershipMapper getMapper() {
        return new UpdateMembershipMapperImpl();
    }

    @Test
    public void testMap() {
        test(UpdateMembershipRequest.class, getMapper()::map);
    }

}