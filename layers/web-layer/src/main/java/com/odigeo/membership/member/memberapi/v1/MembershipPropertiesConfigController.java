package com.odigeo.membership.member.memberapi.v1;

import com.odigeo.membership.member.memberapi.AbstractController;
import com.odigeo.membership.v1.MembershipPropertiesConfigService;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class MembershipPropertiesConfigController extends AbstractController implements MembershipPropertiesConfigService {

    @Override
    public Boolean enableConfigurationProperty(final String propertyKey) {
        return getPropertiesConfigurationService().updatePropertyValue(propertyKey, TRUE);
    }

    @Override
    public Boolean disableConfigurationProperty(final String propertyKey) {
        return getPropertiesConfigurationService().updatePropertyValue(propertyKey, FALSE);
    }

}
