package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.discount.ApplyDiscountParameters;
import com.odigeo.membership.discount.ApplyDiscountResult;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.product.FreeTrialCandidate;

public interface MembershipValidationService {
    Boolean applyMembership(MemberOnPassengerListParameter memberOnPassengerListParameter) throws
            MissingElementException, DataAccessException;

    Boolean isMembershipActiveOnWebsite(String siteId);

    Boolean isMembershipToBeRenewed(Long membershipId) throws MissingElementException, DataAccessException;

    boolean isEligibleForFreeTrial(FreeTrialCandidate freeTrialCandidate) throws DataAccessException;

    ApplyDiscountResult applyMembership(ApplyDiscountParameters applyDiscountParameters) throws DataAccessException;
}
