package com.odigeo.membership.configuration.services;

import com.google.inject.Binder;
import com.google.inject.internal.BindingBuilder;
import com.odigeo.membership.member.memberapi.mapper.MembershipMapper;
import com.odigeo.membership.member.memberapi.mapper.UpdateMembershipMapper;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.openMocks;

public class MapperModuleTest {

    @Mock
    private Binder builder;
    @Mock
    private BindingBuilder<MembershipMapper> membershipMapperBindingBuilder;
    @Mock
    private BindingBuilder<UpdateMembershipMapper> updateMembershipMapperBindingBuilder;

    private MapperModule mapperModule;

    @BeforeMethod
    public void beforeClass() {
        openMocks(this);
        mapperModule = new MapperModule();

    }

    @Test
    public void testConfigure() {
        Mockito.when(builder.bind(MembershipMapper.class)).thenReturn(membershipMapperBindingBuilder);
        Mockito.when(builder.bind(UpdateMembershipMapper.class)).thenReturn(updateMembershipMapperBindingBuilder);
        mapperModule.configure(builder);
    }
}