package com.odigeo.membership.member.update.operation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.update.UpdateMembershipObjectMother;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static com.odigeo.membership.member.update.UpdateMembershipObjectMother.anActivatedMembershipBuilder;
import static java.lang.Boolean.TRUE;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class RemindMeLaterOperationTest {
    private RemindMeLaterOperation remindMeLaterOperation;
    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;
    @Mock
    private MemberService memberService;
    @Mock
    private MembershipStore membershipStore;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        remindMeLaterOperation = new RemindMeLaterOperation(dataSource, memberService, membershipStore, membershipMessageSendingManager);
    }

    @DataProvider
    Object[][] remindMeLaterScenarios() {
        return new Object[][]{
                {anActivatedMembershipBuilder().build(), TRUE, TRUE, TRUE},
                {anActivatedMembershipBuilder().build(), Boolean.FALSE, Boolean.FALSE, Boolean.FALSE},
                {anActivatedMembershipBuilder().build(), Boolean.FALSE, TRUE, Boolean.FALSE},
                {anActivatedMembershipBuilder().build(), TRUE, Boolean.FALSE, Boolean.FALSE},
                {anActivatedMembershipBuilder().setStatus(MemberStatus.DEACTIVATED).build(), TRUE, TRUE, Boolean.FALSE},
        };
    }

    @DataProvider
    Object[][] exceptionsProvider() {
        return new Object[][]{
                {new MissingElementException(StringUtils.EMPTY)},
                {new SQLException()}
        };
    }

    @Test(dataProvider = "remindMeLaterScenarios")
    public void testUpdate(Membership membership, boolean isRemindMeLaterSaved, boolean isMessageSentToCrm, boolean expectedResult) throws DataAccessException, MissingElementException, SQLException, ExistingRecurringException {
        when(memberService.getMembershipById(eq(membership.getId()))).thenReturn(membership);
        when(memberService.getMembershipByIdWithMemberAccount(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.setRemindMeLater(eq(dataSource), eq(membership.getId()), eq(TRUE)))
                .thenReturn(isRemindMeLaterSaved);
        when(membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(eq(membership), eq(SubscriptionStatus.SUBSCRIBED)))
                .thenReturn(isMessageSentToCrm);
        boolean setResult = remindMeLaterOperation.update(UpdateMembershipObjectMother.anUpdateMembership());
        assertEquals(setResult, expectedResult);
    }

    @Test
    public void testUpdateJustDeactivateScenario() throws DataAccessException, MissingElementException, SQLException, ExistingRecurringException {
        Membership membership = anActivatedMembershipBuilder().setStatus(MemberStatus.DEACTIVATED).build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(memberService.getMembershipByIdWithMemberAccount(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.setRemindMeLater(eq(dataSource), eq(membership.getId()), eq(TRUE)))
                .thenReturn(TRUE);
        when(membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(eq(membership), eq(SubscriptionStatus.SUBSCRIBED)))
                .thenReturn(TRUE);
        boolean setResult = remindMeLaterOperation.update(UpdateMembershipObjectMother.aDeactivateUpdateMembership());
        assertTrue(setResult);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateMembershipNotFoundExceptions() throws DataAccessException, MissingElementException, ExistingRecurringException {
        Membership membership = anActivatedMembershipBuilder().setStatus(MemberStatus.DEACTIVATED).build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenThrow(new MissingElementException(StringUtils.EMPTY));
        remindMeLaterOperation.update(UpdateMembershipObjectMother.aDeactivateUpdateMembership());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateExceptions() throws DataAccessException, MissingElementException, SQLException, ExistingRecurringException {
        Membership membership = anActivatedMembershipBuilder().setStatus(MemberStatus.DEACTIVATED).build();
        when(memberService.getMembershipById(eq(membership.getId())))
                .thenReturn(membership);
        when(memberService.getMembershipByIdWithMemberAccount(eq(membership.getId())))
                .thenReturn(membership);
        when(membershipStore.setRemindMeLater(eq(dataSource), eq(membership.getId()), eq(TRUE)))
                .thenThrow(new SQLException());
        remindMeLaterOperation.update(UpdateMembershipObjectMother.aDeactivateUpdateMembership());
    }
}