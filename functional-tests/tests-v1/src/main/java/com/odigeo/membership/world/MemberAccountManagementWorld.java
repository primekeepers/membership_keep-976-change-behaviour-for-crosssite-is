package com.odigeo.membership.world;

import com.odigeo.membership.functionals.membership.CheckMemberOnPassengerListRequestBuilder;
import com.odigeo.membership.functionals.membership.TravellerParametersContainerBuilder;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.response.MembershipAccountInfo;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;
import java.util.stream.Collectors;

@ScenarioScoped
public class MemberAccountManagementWorld {

    private MembershipAccountInfo membershipAccountInfo;
    private Boolean membershipApplicable;
    private CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest;
    private CreateMembershipRequest createMembershipRequest;
    private boolean eligibleForFreeTrialResponse;


    public MemberAccountManagementWorld() {
        this.checkMemberOnPassengerListRequest = new CheckMemberOnPassengerListRequest();
    }

    public MembershipAccountInfo getMembershipAccountInfo() {
        return membershipAccountInfo;
    }

    public void setMembershipAccountInfo(final MembershipAccountInfo membershipAccountInfo) {
        this.membershipAccountInfo = membershipAccountInfo;
    }

    public void setCheckMemberOnPassengerListRequest(CheckMemberOnPassengerListRequestBuilder checkMemberOnPassengerListRequestBuilder) {
        this.checkMemberOnPassengerListRequest = checkMemberOnPassengerListRequestBuilder.build();
    }

    public CheckMemberOnPassengerListRequest getCheckMemberOnPassengerListRequest() {
        return this.checkMemberOnPassengerListRequest;
    }

    public void addPassengersToCheckMemberOnPassengerList(List<TravellerParametersContainerBuilder> travellerParametersContainerBuilderList) {
        checkMemberOnPassengerListRequest.setTravellerContainerList(
                travellerParametersContainerBuilderList
                        .stream()
                        .map(TravellerParametersContainerBuilder::build)
                        .collect(Collectors.toList()));
    }

    public Boolean getMembershipApplicable() {
        return this.membershipApplicable;
    }

    public void setMembershipApplicable(Boolean membershipApplicable) {
        this.membershipApplicable = membershipApplicable;
    }

    public void setCreateMembershipRequest(CreateMembershipRequest createMembershipRequest) {
        this.createMembershipRequest = createMembershipRequest;
    }

    public CreateMembershipRequest getCreateMembershipRequest() {
        return this.createMembershipRequest;
    }

    public void setEligibleForFreeTrialResponse(boolean eligibleForFreeTrialResponse) {
        this.eligibleForFreeTrialResponse = eligibleForFreeTrialResponse;
    }

    public boolean isEligibleForFreeTrialResponse() {
        return eligibleForFreeTrialResponse;
    }

}
