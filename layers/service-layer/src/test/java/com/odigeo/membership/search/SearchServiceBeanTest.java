package com.odigeo.membership.search;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.UserAccountNotFoundException;
import com.odigeo.membership.member.MemberAccountStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.parameters.search.MembershipSearchBuilder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SearchServiceBeanTest {
    private static final List<MemberAccount> MEMBER_ACCOUNTS = Collections.singletonList(new MemberAccount(123L, 321L, "name", "lastName"));
    private static final List<Membership> MEMBERSHIPS = Collections.singletonList(new MembershipBuilder().setMembershipType(MembershipType.BASIC).build());
    private static final MembershipSearch MEMBERSHIP_SEARCH = new MembershipSearchBuilder().membershipType("BASIC").build();
    private static final MemberAccountSearch MEMBER_ACCOUNT_SEARCH = new MemberAccountSearch.Builder().lastNames("lastName").withMembership(true).build();
    private static final Long USER_ID_TEST = 123L;
    private static final String WEBSITE_TEST = "ES";
    private static final Long TEST_MEMBERSHIP_ID = 1234L;
    private static final String EMAIL_TEST = "test@test.ut";
    public static final MembershipSearch MEMBERSHIP_SEARCH_BY_EMAIL = MembershipSearch.builder().email(EMAIL_TEST).website(WEBSITE_TEST).build();
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private MembershipStore membershipStore;

    @Mock
    private DataSource dataSource;
    @Mock
    private UserService userService;

    @InjectMocks
    private SearchServiceBean searchServiceBean = new SearchServiceBean();


    @BeforeMethod
    public void setUp() throws SQLException {
        openMocks(this);
    }

    @Test
    public void testSearchMemberAccounts() throws SQLException, DataAccessException {
        when(memberAccountStore.searchMemberAccounts(any(DataSource.class), eq(MEMBER_ACCOUNT_SEARCH))).thenReturn(MEMBER_ACCOUNTS);
        List<MemberAccount> memberAccounts = searchServiceBean.searchMemberAccounts(MEMBER_ACCOUNT_SEARCH);
        assertEquals(memberAccounts, MEMBER_ACCOUNTS);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSearchMemberAccountsException() throws SQLException, DataAccessException {
        when(memberAccountStore.searchMemberAccounts(any(DataSource.class), any(MemberAccountSearch.class))).thenThrow(new SQLException());
        searchServiceBean.searchMemberAccounts(MEMBER_ACCOUNT_SEARCH);
    }

    @Test
    public void testSearchMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any())).thenReturn(MEMBERSHIPS);
        List<Membership> memberships = searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH);
        assertEquals(memberships, MEMBERSHIPS);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSearchMembershipException() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), eq(MEMBERSHIP_SEARCH)))
                .thenThrow(new SQLException());
        searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH);
    }

    @Test
    public void testGetCurrentActivatedMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class)))
                .thenAnswer(invocation -> setCurrentMembershipSearchByStatus(invocation, MemberStatus.ACTIVATED));

        Optional<Membership> currentMembership = searchServiceBean.getCurrentMembership(USER_ID_TEST, WEBSITE_TEST);

        verify(membershipStore).searchMemberships(any(DataSource.class), any(MembershipSearch.class));
        assertEquals(currentMembership.map(Membership::getStatus).orElse(null), MemberStatus.ACTIVATED);
    }

    @Test
    public void testGetCurrentPendingToCollectMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class)))
                .thenAnswer(invocation -> setCurrentMembershipSearchByStatus(invocation, MemberStatus.PENDING_TO_COLLECT));

        Optional<Membership> currentMembership = searchServiceBean.getCurrentMembership(USER_ID_TEST, WEBSITE_TEST);

        verify(membershipStore, times(2)).searchMemberships(any(), any());
        assertEquals(currentMembership.map(Membership::getStatus).orElse(null), MemberStatus.PENDING_TO_COLLECT);
    }

    @Test
    public void testNoneCurrentCollectMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class)))
                .thenAnswer(invocation -> setCurrentMembershipSearchByStatus(invocation, MemberStatus.PENDING_TO_ACTIVATE));

        Optional<Membership> currentMembership = searchServiceBean.getCurrentMembership(USER_ID_TEST, WEBSITE_TEST);

        verify(membershipStore, times(2)).searchMemberships(any(), any());
        assertFalse(currentMembership.isPresent());
    }

    @Test
    public void testSearchMembershipsByEmail() throws UserAccountNotFoundException, DataAccessException, SQLException {
        when(userService.getUserIdBy(EMAIL_TEST, WEBSITE_TEST)).thenReturn(USER_ID_TEST);
        when(membershipStore.searchMemberships(eq(dataSource), any(MembershipSearch.class))).thenReturn(MEMBERSHIPS);
        List<Membership> memberships = searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH_BY_EMAIL);
        verify(userService).getUserIdBy(eq(EMAIL_TEST), eq(WEBSITE_TEST));
        assertEquals(memberships, MEMBERSHIPS);
    }

    @Test
    public void testSearchMembershipsByEmailUserNotFound() throws UserAccountNotFoundException, DataAccessException, SQLException {
        when(userService.getUserIdBy(EMAIL_TEST, WEBSITE_TEST)).thenThrow(UserAccountNotFoundException.class);
        when(membershipStore.searchMemberships(eq(dataSource), any(MembershipSearch.class))).thenReturn(MEMBERSHIPS);
        List<Membership> memberships = searchServiceBean.searchMemberships(MembershipSearch.builder().email(EMAIL_TEST).website(WEBSITE_TEST).build());
        verify(userService).getUserIdBy(eq(EMAIL_TEST), eq(WEBSITE_TEST));
        assertTrue(memberships.isEmpty());
    }

    @Test
    public void checkLazyConfigurationEngineCalls() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(eq(dataSource), eq(MEMBERSHIP_SEARCH))).thenReturn(MEMBERSHIPS);
        when(memberAccountStore.searchMemberAccounts(eq(dataSource), eq(MEMBER_ACCOUNT_SEARCH))).thenReturn(MEMBER_ACCOUNTS);
        assertFalse(searchServiceBean.searchMemberAccounts(MEMBER_ACCOUNT_SEARCH).isEmpty());
        assertFalse(searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH).isEmpty());
        assertFalse(searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH).isEmpty());
    }

    private List<com.odigeo.membership.Membership> setCurrentMembershipSearchByStatus(InvocationOnMock invocation, MemberStatus memberStatus) {
        MembershipSearch membershipSearch = (MembershipSearch) invocation.getArguments()[1];
        return Optional.of(membershipSearch.getValues())
                .filter(membershipSearch1 -> membershipSearch1.contains(memberStatus.name()))
                .map(valueList -> List.of(new MembershipBuilder().setId(TEST_MEMBERSHIP_ID).setStatus(memberStatus).build()))
                .orElseGet(List::of);
    }
}
