package com.odigeo.membership.mocks.bookingapiservice;

import com.odigeo.bookingapi.client.v14.BookingApiServiceMockImpl;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.ProductCategoryBooking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class BookingApiServiceMock extends BookingApiServiceMockImpl {

    private static final long ERRONEOUS_BOOKING_ID = 1L;

    private List<BookingDetail> bookingDetails;

    public BookingApiServiceMock() {
        bookingDetails = new ArrayList<>();
    }

    @Override
    public BookingDetail getBooking(String user, String password, Locale locale, long bookingId) {
        if (bookingId == ERRONEOUS_BOOKING_ID) {
            BookingDetail bookingWithError = new BookingDetail();
            bookingWithError.setErrorMessage("Mock was configured to return this booking with error");
            return bookingWithError;
        }
        Optional<BookingDetail> bookingDetailOpt = bookingDetails.stream().filter(isBookingDetailWithBookingId(bookingId)).findFirst();
        return bookingDetailOpt.orElse(new BookingDetail());
    }

    public void addBookingDetail(BookingDetail bookingDetail) {
        if (bookingDetails == null) {
            bookingDetails = new ArrayList<>();
        }
        bookingDetails.add(bookingDetail);
    }

    public void addBookingProducts(ProductCategoryBooking productCategoryBooking, Long... bookingId) {
        if (bookingDetails == null) {
            bookingDetails = new ArrayList<>();
        }
        bookingDetails.stream().filter(isBookingDetailWithBookingId(bookingId)).forEach(add(productCategoryBooking));
    }

    private Consumer<BookingDetail> add(ProductCategoryBooking productCategoryBooking) {
        return b -> {
            if (b.getBookingProducts() == null) {
                b.setBookingProducts(new ArrayList<>());
            }
            b.getBookingProducts().add(productCategoryBooking);
        };
    }

    private Predicate<BookingDetail> isBookingDetailWithBookingId(Long... bookingId) {
        return b -> Arrays.stream(bookingId).anyMatch(id -> id.equals(b.getBookingBasicInfo().getId()));
    }

    public void resetMocks() {
        bookingDetails.clear();
    }
}
