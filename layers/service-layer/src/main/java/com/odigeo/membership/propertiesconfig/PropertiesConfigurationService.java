package com.odigeo.membership.propertiesconfig;

public interface PropertiesConfigurationService {

    boolean isSendingIdsToKafkaActive();

    boolean isTransactionalWelcomeEmailActive();

    boolean updatePropertyValue(String key, boolean value);
}
