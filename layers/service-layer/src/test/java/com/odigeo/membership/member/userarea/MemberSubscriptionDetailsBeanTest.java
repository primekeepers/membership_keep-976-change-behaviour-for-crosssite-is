package com.odigeo.membership.member.userarea;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.bookingapi.v14.responses.CreditCard;
import com.odigeo.bookingapi.v14.responses.Money;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.PrimeBookingInformation;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.math.BigDecimal.TEN;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class MemberSubscriptionDetailsBeanTest {

    @Mock
    private BookingDetailInfoExtractor bookingDetailInfoExtractorMock;
    @Mock
    private MembershipBookingDetailsBean membershipBookingDetailsBeanMock;
    private MemberSubscriptionDetailsBean subscriptionDetailsBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        subscriptionDetailsBean = new MemberSubscriptionDetailsBean(bookingDetailInfoExtractorMock, membershipBookingDetailsBeanMock);
    }

    @Test
    public void testBookingDetailsSubscriptionFromMembershipReturnsEmptyWhenNoFoundElement() throws Exception {
        //Given
        final long membershipId = 22L;
        given(membershipBookingDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willReturn(Optional.empty());
        //When
        Optional<BookingDetail> foundBookingDetail = subscriptionDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
        //Then
        assertFalse(foundBookingDetail.isPresent());
    }

    @Test
    public void testBookingDetailsSubscriptionFromMembershipReturnsDelegateFoundElement() throws Exception {
        //Given
        final long membershipId = 435L;
        final BookingDetail expectedBookingDetail = new BookingDetail();
        given(membershipBookingDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willReturn(Optional.of(expectedBookingDetail));
        //When
        Optional<BookingDetail> foundBookingDetail = subscriptionDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
        //Then
        assertEquals(foundBookingDetail, Optional.of(expectedBookingDetail));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMemberSubscriptionDetailsHasExtractedInfoFromBookingDetails() throws Exception {
        //Given
        final long membershipId = 342L;
        final List<BookingDetail> membershipBookingDetails = singletonList(new BookingDetail());
        final BookingDetail membershipBookingDetail = new BookingDetail();
        final Money bookingsTotalSavings = MoneyOperation.buildMoney(TEN, "EUR");
        final List<PrimeBookingInformation> primeBookingsInfo = singletonList(new PrimeBookingInformation());
        final CreditCard subscriptionPaymentMethod = new CreditCard();
        subscriptionPaymentMethod.setCreditCardNumber("CC number 1234");
        final String subscriptionPaymentMethodType = "pay method type";
        given(membershipBookingDetailsBeanMock.getBookingDetailsByMembershipId(membershipId)).willReturn(membershipBookingDetails);
        given(membershipBookingDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willReturn(Optional.of(membershipBookingDetail));
        given(bookingDetailInfoExtractorMock.extractTotalSavingsFromBookingDetails(membershipBookingDetails)).willReturn(bookingsTotalSavings);
        given(bookingDetailInfoExtractorMock.extractMemberSubscriptionDetailsFromBookingDetails(membershipBookingDetails)).willReturn(primeBookingsInfo);
        final Function<BookingDetail, Optional<CreditCard>> mockPaymentMethodExtractor = mock(Function.class);
        given(bookingDetailInfoExtractorMock.extractSubscriptionPaymentMethodFromBookingDetail()).willReturn(mockPaymentMethodExtractor);
        given(mockPaymentMethodExtractor.apply(membershipBookingDetail)).willReturn(Optional.of(subscriptionPaymentMethod));
        final Function<BookingDetail, String> mockPaymentMethodTypeExtractor = mock(Function.class);
        given(bookingDetailInfoExtractorMock.extractPaymentMethodTypeFromBookingDetail()).willReturn(mockPaymentMethodTypeExtractor);
        given(mockPaymentMethodTypeExtractor.apply(membershipBookingDetail)).willReturn(subscriptionPaymentMethodType);
        //When
        MemberSubscriptionDetails memberSubscriptionDetails = subscriptionDetailsBean.getMemberSubscriptionDetails(membershipId);
        //Then
        assertEquals(memberSubscriptionDetails.getMembershipId().longValue(), membershipId);
        assertEquals(memberSubscriptionDetails.getTotalSavings(), bookingsTotalSavings);
        assertEquals(memberSubscriptionDetails.getPrimeBookingsInfo(), primeBookingsInfo);
        assertEquals(memberSubscriptionDetails.getSubscriptionPaymentMethod(), subscriptionPaymentMethod);
        assertEquals(memberSubscriptionDetails.getSubscriptionPaymentMethodType(), subscriptionPaymentMethodType);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMemberSubscriptionDetailsHasDefaultPaymentInfoWhenNoMembershipBookingDetailRetrieved() throws Exception {
        //Given
        final long membershipId = 6775L;
        given(membershipBookingDetailsBeanMock.getBookingDetailsByMembershipId(membershipId)).willReturn(emptyList());
        given(membershipBookingDetailsBeanMock.getBookingDetailSubscriptionFromMembershipId(membershipId)).willThrow(new BookingApiException("expected booking api exception"));
        final Function<BookingDetail, Optional<CreditCard>> mockPaymentMethodExtractor = mock(Function.class);
        given(bookingDetailInfoExtractorMock.extractSubscriptionPaymentMethodFromBookingDetail()).willReturn(mockPaymentMethodExtractor);
        final Function<BookingDetail, String> mockPaymentMethodTypeExtractor = mock(Function.class);
        given(bookingDetailInfoExtractorMock.extractPaymentMethodTypeFromBookingDetail()).willReturn(mockPaymentMethodTypeExtractor);
        //When
        MemberSubscriptionDetails memberSubscriptionDetails = subscriptionDetailsBean.getMemberSubscriptionDetails(membershipId);
        //Then
        final Gson gson = new GsonBuilder().create();
        assertEquals(memberSubscriptionDetails.getMembershipId().longValue(), membershipId);
        assertEquals(gson.toJson(memberSubscriptionDetails.getSubscriptionPaymentMethod()), gson.toJson(new CreditCard()));
        assertEquals(memberSubscriptionDetails.getSubscriptionPaymentMethodType(), EMPTY);
    }
}
